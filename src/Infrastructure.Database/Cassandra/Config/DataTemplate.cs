﻿using System.Xml.Serialization;

namespace Infrastructure.Database.Cassandra.Config
{
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot("DataTemplate", Namespace = "", IsNullable = false)]
    public class DataTemplate
    {
        /// <remarks/>
        [XmlArrayItem("CqlStatement", IsNullable = false)]
        public DataTemplateCqlStatement[] DataQuery { get; set; }

        /// <remarks/>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <remarks/>
        [XmlAttribute("description")]
        public string Description { get; set; }

        /// <remarks/>
        [XmlAttribute("defaultPackage")]
        public string DefaultPackage { get; set; }

        /// <remarks/>
        [XmlAttribute("version")]
        public decimal Version { get; set; }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public class DataTemplateCqlStatement
    {
        /// <remarks/>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <remarks/>
        [XmlText()]
        public string Value { get; set; }
    }
}
