﻿using Cassandra.Data.Linq;
using Cassandra.Mapping;

namespace Infrastructure.Database.Cassandra.Manager.Extensions
{
    public static class CqlBatchExtensions
    {
        public static void Execute(this ICqlBatch cqlBatch, CqlDelete cqlDelete)
        {
            cqlBatch.Execute(cqlDelete.QueryString, cqlDelete.QueryValues);
        }
    }
}
