﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cassandra;

namespace Infrastructure.Database.Cassandra.Manager.Extensions
{
    public static class PreparedStatementExtensions
    {
        public static BoundStatement BindParams(this PreparedStatement statement, params object[] objs)
        {
            var objects = new List<object>();
            foreach (var o in objs)
            {
                if (o.GetType().IsArray)
                {
                    objects.AddRange(((Array)o).Cast<object>());
                }
                else
                {
                    objects.Add(o);
                }
            }
            return statement.Bind(objects.ToArray());
        }

        public static BoundStatement BindParams(this PreparedStatement statement, object[] exceptions, params object[] objs)
        {
            var objects = new List<object>();
            foreach (var o in objs)
            {
                if (!exceptions.Contains(o) && o.GetType().IsArray)
                {
                    objects.AddRange(((Array)o).Cast<object>());
                }
                else
                {
                    objects.Add(o);
                }
            }
            return statement.Bind(objects.ToArray());
        }
    }
}
