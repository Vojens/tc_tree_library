﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using Cassandra.Data.Linq;
using Cassandra.Mapping;
using Infrastructure.ConnectionProvider;
using Infrastructure.Database.Cassandra.Manager.Util;

namespace Infrastructure.Database.Cassandra.Manager.Extensions
{
    public static class CqlExtensions
    {
        public static StatementOptions GetStatementOptions(this Cql cql)
        {
            return cql.Statement.GetStatementOptions();
        }

        public static StatementOptions GetStatementOptions(this string cqlStatement)
        {
            if (cqlStatement.StartsWith("select", true, CultureInfo.CurrentCulture))
            {
                return StatementOptions.Select;
            }
            if (cqlStatement.StartsWith("Insert", true, CultureInfo.CurrentCulture))
            {
                return StatementOptions.Upsert;
            }
            if (cqlStatement.StartsWith("update", true, CultureInfo.CurrentCulture))
            {
                return StatementOptions.Upsert;
            }
            if (cqlStatement.StartsWith("Delete", true, CultureInfo.CurrentCulture))
            {
                return StatementOptions.Delete;
            }
            throw new ArgumentException();
        }

        public static CqlQuery<TSource> ExplicitSelect<TSource>(this CqlQuery<TSource> source)// where TSource : IDto
        {
            return source;
            //Type type = typeof (TSource);
            //object exp;
            //if (CqlHelper.DtoDictionary.TryGetValue(type, out exp))
            //{
            //    Expression<Func<TSource, TSource>> expression = (Expression<Func<TSource, TSource>>) exp;
            //    return source.Select(expression);
            //}
            //throw new KeyNotFoundException(string.Format("Lambda Expression for type {0} is not defined", typeof(TSource)));
        }
    }
}
