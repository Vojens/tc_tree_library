﻿using System;
using System.Reflection;

namespace Infrastructure.Database.Cassandra.Manager
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ColumnDefinitionAttribute : Attribute
    {
        public MemberInfo MemberInfo { get; set; }
        public Type MemberInfoType { get; set; }
        public string ColumnName { get; set; }
        public Type ColumnType { get; set; }
        public bool Ignore { get; set; }
        public bool IsExplicitlyDefined { get; set; }
        public bool SecondaryIndex { get; set; }
        public bool IsCounter { get; set; }
        public bool IsStatic { get; set; }
    }
}
