﻿using System;

namespace Infrastructure.Database.Cassandra.Manager.Registry
{
    public interface IRegistry : IDisposable
    {
        void Configure();
    }
}
