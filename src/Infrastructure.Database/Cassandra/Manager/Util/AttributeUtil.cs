﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Caching;

namespace Infrastructure.Database.Cassandra.Manager.Util
{
    public static class AttributeUtil
    {
        public static PropertyInfo GetPropertyInfo<T>(Expression<Func<T>> propertyLambda)
        {
            MemberExpression exp;

            if (propertyLambda.Body is UnaryExpression)
            {
                var unExp = (UnaryExpression)propertyLambda.Body;
                if (unExp.Operand is MemberExpression)
                {
                    exp = (MemberExpression)unExp.Operand;
                }
                else
                    throw new ArgumentException();
            }
            else if (propertyLambda.Body is MemberExpression)
            {
                exp = (MemberExpression)propertyLambda.Body;
            }
            else
            {
                throw new ArgumentException(string.Format("Expression '{0}' doesn't refers to a valid property.", propertyLambda));
            }
            return (PropertyInfo)exp.Member;
        }

        public static object[] GetCustomAttributes<T>(Expression<Func<T>> propertyLambda, Type attributeType)
        {
            var info = GetPropertyInfo(propertyLambda);
            return attributeType == null ? info.GetCustomAttributes(true) : info.GetCustomAttributes(attributeType, true);
        }

        public static object GetCustomAttribute<T>(Expression<Func<T>> propertyLambda, Type attributeType)
        {
            try
            {
                string name;
                if (propertyLambda.Body is MemberExpression)
                {
                    var expression = (MemberExpression)propertyLambda.Body;
                    name = (expression.Member.DeclaringType != null
                        ? expression.Member.DeclaringType.FullName
                        : string.Empty) + "." + expression.Member.Name;
                    
                }
                else if (propertyLambda.Body is UnaryExpression)
                {
                    var expression = (UnaryExpression) propertyLambda.Body;
                    var propExpression = (MemberExpression)expression.Operand;
                    //var type = propExpression.Member.GetType();
                    name = (propExpression.Member.DeclaringType != null
                        ? propExpression.Member.DeclaringType.FullName
                        : string.Empty) + "." + propExpression.Member.Name;
                }
                else
                {
                    throw new NotSupportedException(
                        "Types other than MemberExpression and Unary Expressions are not supported yet.");
                }
                
                
                ObjectCache cache = MemoryCache.Default;
                    var customAttribute = cache[name];
                if (customAttribute != null) 
                    return customAttribute;
                var policy = new CacheItemPolicy();
                customAttribute = DoGetCustomAttribute(propertyLambda, attributeType);
                cache.Set(name, customAttribute, policy);
                return customAttribute;
            }
            catch (Exception)
            {
                // ignored
            }
            return null;

        }

        public static object DoGetCustomAttribute<T>(Expression<Func<T>> propertyLambda, Type attributeType)
        {
            var info = GetPropertyInfo(propertyLambda);
            return info.GetCustomAttribute(attributeType, true);
        }

        public static object Get<T>(Expression<Func<T>> propertyLambda, Type attributeType, string propertyName)
        {
            object attribute = GetCustomAttribute(propertyLambda, attributeType);
            var info = attributeType.GetProperty(propertyName);
            return info.GetValue(attribute);
        }
    }
}
