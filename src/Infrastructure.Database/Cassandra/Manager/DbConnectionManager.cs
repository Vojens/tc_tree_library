﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Cassandra;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.ConnectionProvider;
using Infrastructure.Database.Cassandra.Config;


namespace Infrastructure.Database.Cassandra.Manager
{
    public class DbConnectionManager : IDisposable
    {
        private static readonly Logger Logger = new Logger(typeof (DbConnectionManager));
        private readonly object ProviderLock = new Object();
        private IConnectionProvider _connectionProvider;
        public Metadata Metadata { get; private set; }
        private readonly Regex _multipleSpacesRegex = new Regex("[ ]{2,}", RegexOptions.Compiled);   

        private static DataTemplate _dataTemplate;

        public DbConnectionManager(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public DbConnectionManager(IConnectionProvider connectionProvider, DataTemplate dataTemplate)
        {
            _connectionProvider = connectionProvider;
            _dataTemplate = dataTemplate;
        }

        public async Task Initialize()
        {
            await InitDataTemplateAsync().ConfigureAwait(false);
            _dataTemplate = null;   //to free up memory on heap
        }

        private static DataTemplate CqlQueries
        {
            get
            {
                return _dataTemplate;
            }
            set { _dataTemplate = value; }
        }

        public void Dispose()
        {
            lock (ProviderLock)
            {
                Contract.Requires<NullReferenceException>(_connectionProvider != null, "connection provider is null");
                try
                {
                    _connectionProvider.Dispose();
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                    throw;
                }
                _connectionProvider = null;
            }
        }

        public ISessionAdapter Session()
        {
            var session = _connectionProvider == null ? null : _connectionProvider.Session;

            if (session == null)
            {
                Logger.Warning("WARNING: ConnectionManager.getConnection() " +
                                  "failed to obtain a connection.");
            }

            return session;
        }

        public void SetConnectionProvider(IConnectionProvider provider)
        {
            lock (ProviderLock)
            {
                if (_connectionProvider != null)
                {
                    _connectionProvider.Dispose();
                    _connectionProvider = null;
                }
                _connectionProvider = provider;
                _connectionProvider.Connect();
                // Now, get a connection to determine meta data.
                //ISessionAdapter session = null;
                try
                {
                    //session = _connectionProvider.Session;
                    Metadata = _connectionProvider.Metadata;
                    _connectionProvider.Session.Cluster.Metadata.HostsEvent += Metadata_HostsEvent;
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                    throw;
                }
            }
        }


        private async Task InitDataTemplateAsync()
        {
            try
            {
//                Parallel.ForEach(CqlQueries.DataQuery, async (dataTemplateCqlStatement) =>
//                {
//                    try
//                    {
//                        //PreparedStatementDictionary.TryAdd(dataTemplateCqlStatement.Name, await GetPreparedStatementAsync(dataTemplateCqlStatement.Name));
//                        var prepStmt = await Session().PrepareAsync(dataTemplateCqlStatement.Name, dataTemplateCqlStatement.Value).ConfigureAwait(false);
//                        PreparedStatementDictionary[dataTemplateCqlStatement.Name] = prepStmt;
//                    }
//                    catch (Exception e)
//                    {
//                        Logger.Error(
//                            string.Format("Could not prepare statement name '{0}' with cql query {1}",
//                                dataTemplateCqlStatement.Name, dataTemplateCqlStatement.Value), e);
//                        throw;
//                    }
//                });

#if DEBUG
                var keys = CqlQueries.DataQuery.GroupBy(x => x.Name.Trim().ToUpper())
                    .Where(g => g.Count() > 1)
                    .Select(y => new {Name = y.Key, Count = y.Count()})
                    .ToList();

                var values = CqlQueries.DataQuery.GroupBy(x => x.Value.Trim().ToUpper())
                    .Where(g => g.Count() > 1)
                    .Select(y => new { Value = y.Key, Count = y.Count()})
                    .ToList();
#endif
                var tasks = CqlQueries.DataQuery.GroupBy(x => x.Name).Select(x => x.First()).Select(PrepareCqlStatementAsync).ToList();
                await tasks.Await().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        private async Task PrepareCqlStatementAsync(DataTemplateCqlStatement dataQuery)
        {
            try
            {
                var dataQueryValue = string.Join(Environment.NewLine, dataQuery.Value.Replace("\r"," ").Replace("\t", " ").Split(new[] { Environment.NewLine, "\n", "\r" }, StringSplitOptions.None).Select(l => l.TrimStart(' ').TrimEnd(' '))).Replace("\n", " ").Replace("\r", " ").TrimStart().TrimEnd();
                dataQueryValue = _multipleSpacesRegex.Replace(dataQueryValue, " ");
                await Session().PrepareAsync(dataQuery.Name, dataQueryValue).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logger.Error(
                    string.Format("Could not prepare statement name '{0}' with cql query {1}",
                        dataQuery.Name, dataQuery.Value), e);
                throw;
            }
        }

        public PreparedStatement GetPreparedStatementAsync(string statementKey)
        {
            Debug.Assert(statementKey != null);
            PreparedStatement prepStmt;
            var isAvailable = ((SessionAdapter)Session()).PreparedStatementCache.TryGetValue(statementKey, out prepStmt);

            if (isAvailable)
                return prepStmt;

            var errorMsg = "The CQL Query whose key is " + statementKey +
                                 " cannot be found in the configurations.";
            Logger.Error(errorMsg);
            throw new KeyNotFoundException(errorMsg);
//            if (_dataTemplate == null)
//            {
//                await InitDataTemplateAsync();
//                
//            }
//
//            //var testCqls = from cql in _dataTemplate.DataQuery where cql == null || cql.Name == null select " there is null";
//            foreach (var dataTemplateCqlStatement in _dataTemplate.DataQuery)
//            {
//                if (dataTemplateCqlStatement == null || dataTemplateCqlStatement.Name == null || dataTemplateCqlStatement.Value == null)
//                {
//                    Logger.Error("There exists null in data template");
//                }
//            }
//            //Logger.Error(testCqls.GetEnumerator().Current);
//            var cqls = from cql in _dataTemplate.DataQuery where cql.Name.Equals(statementKey) select cql.Value;
//            var cqlQuery = cqls.FirstOrDefault();
//            if (EqualityComparer<string>.Default.Equals(cqlQuery, default(string)))
//            {
//                var errorMsg = "The CQL Query whose key is " + statementKey +
//                                 " cannot be found in the configurations.";
//                Logger.Error(errorMsg);
//                throw new KeyNotFoundException(errorMsg);
//            }
//            prepStmt = await Session().PrepareAsync(statementKey, cqlQuery);
//            PreparedStatementDictionary[statementKey] = prepStmt;
//            return prepStmt;
        }

        public string GetCQlQuery(string name)
        {
            //Preconditions.CheckNotNull(name, "name of cql query cannot be null");
            var dataTemplateCqlStatement = _dataTemplate.DataQuery.FirstOrDefault(x => x.Name == name);
            if (dataTemplateCqlStatement != null)
            {
                return dataTemplateCqlStatement.Value;
            }
            else
            {
                Logger.Warning("There exists no data query in the template");
                return null;
            }
        }

        internal static void Metadata_HostsEvent(object sender, HostsEventArgs e)
        {
            Logger.Warning(string.Format("Node {0} has a Host Event of \"{1}\"", e.Address, e.What));
        }
    }
}