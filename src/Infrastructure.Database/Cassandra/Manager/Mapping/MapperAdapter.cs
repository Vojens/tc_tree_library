﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra;
using Cassandra.Mapping;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.Common.Util;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.ConnectionProvider.Util;
using Infrastructure.Database.Cassandra.Manager.Extensions;

namespace Infrastructure.Database.Cassandra.Manager.Mapping
{
    public class MapperAdapter : IMapper
    {
        private readonly IConnectionProvider _connectionProvider;
        public IMapper MapperAdaptee
        {
            get { return _mapperAdaptee; }
        }

        private readonly IMapper _mapperAdaptee;
        private readonly int? _batchSizeThreshold;
        private readonly int? _batchCountThreshold;

        public MapperAdapter(ISession session, MappingConfiguration config)
        {
            _batchSizeThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchSizeThreshold;
            _batchCountThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchCountThreshold;
            _mapperAdaptee = new Mapper(session, config);
        }

        public MapperAdapter(IConnectionProvider connectionProvider, MappingConfiguration config)
        {
            _connectionProvider = connectionProvider;
            _batchSizeThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchSizeThreshold;
            _batchCountThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchCountThreshold;
            _mapperAdaptee = new Mapper(_connectionProvider.Session.SessionAdaptee, config);
        }

        public MapperAdapter(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
            _batchSizeThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchSizeThreshold;
            _batchCountThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchCountThreshold;
            _mapperAdaptee = new Mapper(_connectionProvider.Session.SessionAdaptee);
        }

        /// <summary>
        /// Creates a new instance of the mapper using <see cref="MappingConfiguration.Global"/> mapping definitions.
        /// </summary>
        public MapperAdapter(ISession session)
        {
            _batchSizeThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchSizeThreshold;
            _batchCountThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchCountThreshold;
            _mapperAdaptee = new Mapper(session);
        }

        public MapperAdapter()
        {
            _batchSizeThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchSizeThreshold;
            _batchCountThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchCountThreshold;
            _mapperAdaptee = new Mapper(_connectionProvider.Session.SessionAdaptee);
        }

        public Task<IEnumerable<T>> FetchAsync<T>(CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.RangeScanSelect);
            }
            return _mapperAdaptee.FetchAsync<T>(queryOptions);
        }

        public Task<IEnumerable<T>> FetchAsync<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.FetchAsync<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.RangeScanSelect)));
        }

        public Task<IEnumerable<T>> FetchAsync<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.RangeScanSelect));
            return _mapperAdaptee.FetchAsync<T>(cql);
        }

        public Task<IPage<T>> FetchPageAsync<T>(Cql cql)
        {
            return _mapperAdaptee.FetchPageAsync<T>(cql);
        }

        public Task<IPage<T>> FetchPageAsync<T>(CqlQueryOptions queryOptions = null)
        {
            return _mapperAdaptee.FetchPageAsync<T>(queryOptions);
        }

        public Task<IPage<T>> FetchPageAsync<T>(int pageSize, byte[] pagingState, string query, object[] args)
        {
            return _mapperAdaptee.FetchPageAsync<T>(pageSize, pagingState, query, args);
        }

        public Task<T> SingleAsync<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.SingleAsync<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select)));
        }

        public Task<T> SingleAsync<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select));
            return _mapperAdaptee.SingleAsync<T>(cql);
        }

        public Task<T> SingleOrDefaultAsync<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.SingleOrDefaultAsync<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select)));
        }

        public Task<T> SingleOrDefaultAsync<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select));
            return _mapperAdaptee.SingleOrDefaultAsync<T>(cql);
        }

        public Task<T> FirstAsync<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.FirstAsync<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select)));
        }

        public Task<T> FirstAsync<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select));
            return _mapperAdaptee.FirstAsync<T>(cql);
        }

        public Task<T> FirstOrDefaultAsync<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.FirstOrDefaultAsync<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select)));
        }

        public Task<T> FirstOrDefaultAsync<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select));
            return _mapperAdaptee.FirstOrDefaultAsync<T>(cql);
        }

        public Task InsertAsync<T>(T poco, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert);
            }
            return _mapperAdaptee.InsertAsync(poco, queryOptions);
        }

        public Task InsertAsync<T>(T poco, bool insertNulls, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert);
            }
            return _mapperAdaptee.InsertAsync(poco, insertNulls, queryOptions);
        }

        public Task InsertAsync<T>(T poco, bool insertNulls, int? ttl, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert);
            }
            return _mapperAdaptee.InsertAsync(poco, insertNulls, ttl, queryOptions);
        }

        public Task UpdateAsync<T>(T poco, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert);
            }
            return _mapperAdaptee.UpdateAsync(poco, queryOptions);
        }

        public Task UpdateAsync<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.UpdateAsync<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert)));
        }

        public Task UpdateAsync<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert));
            return _mapperAdaptee.UpdateAsync<T>(cql);
        }

        public Task DeleteAsync<T>(T poco, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Delete);
            }
            return _mapperAdaptee.DeleteAsync(poco, queryOptions);
        }

        public Task DeleteAsync<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.DeleteAsync<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Delete)));
        }

        public Task DeleteAsync<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Delete));
            return _mapperAdaptee.DeleteAsync<T>(cql);
        }

        public Task ExecuteAsync(string cql, params object[] args)
        {
            return _mapperAdaptee.ExecuteAsync(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(cql.GetStatementOptions())));
        }

        public Task ExecuteAsync(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(cql.GetStatementOptions()));
            return _mapperAdaptee.ExecuteAsync(cql);
        }

        public IEnumerable<T> Fetch<T>(CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Select);
            }
            return _mapperAdaptee.Fetch<T>(queryOptions);
        }

        public IEnumerable<T> Fetch<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.Fetch<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.RangeScanSelect)));
        }

        public IEnumerable<T> Fetch<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.RangeScanSelect));
            return _mapperAdaptee.Fetch<T>(cql);
        }

        public IPage<T> FetchPage<T>(Cql cql)
        {
            return _mapperAdaptee.FetchPage<T>(cql);
        }

        public IPage<T> FetchPage<T>(CqlQueryOptions queryOptions = null)
        {
            return _mapperAdaptee.FetchPage<T>(queryOptions);
        }

        public IPage<T> FetchPage<T>(int pageSize, byte[] pagingState, string query, object[] args)
        {
            return _mapperAdaptee.FetchPage<T>(pageSize, pagingState, query, args);
        }

        public T Single<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.Single<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select)));
        }

        public T Single<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select));
            return _mapperAdaptee.Single<T>(cql);
        }

        public T SingleOrDefault<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.SingleOrDefault<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select)));
        }

        public T SingleOrDefault<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select));
            return _mapperAdaptee.SingleOrDefault<T>(cql);
        }

        public T First<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.First<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select)));
        }

        public T First<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select));
            return _mapperAdaptee.First<T>(cql);
        }

        public T FirstOrDefault<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.FirstOrDefault<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select)));
        }

        public T FirstOrDefault<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Select));
            return _mapperAdaptee.FirstOrDefault<T>(cql);
        }

        public void Insert<T>(T poco, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert);
            }
            _mapperAdaptee.Insert(poco, queryOptions);
        }

        public void Insert<T>(T poco, bool insertNulls, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert);
            }
            _mapperAdaptee.Insert(poco, insertNulls, queryOptions);
        }

        public void Insert<T>(T poco, bool insertNulls, int? ttl, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert);
            }
            _mapperAdaptee.Insert(poco, insertNulls, ttl, queryOptions);
        }

        public void Update<T>(T poco, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert);
            }
            _mapperAdaptee.Update(poco, queryOptions);
        }

        public void Update<T>(string cql, params object[] args)
        {
            _mapperAdaptee.Update<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert)));
        }

        public void Update<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert));
            _mapperAdaptee.Update<T>(cql);
        }

        public void Delete<T>(T poco, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Delete);
            }
            _mapperAdaptee.Delete(poco, queryOptions);
        }

        public void Delete<T>(string cql, params object[] args)
        {
            _mapperAdaptee.Delete<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Delete)));
        }

        public void Delete<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Delete));
            _mapperAdaptee.Delete<T>(cql);
        }

        public void Execute(string cql, params object[] args)
        {
            _mapperAdaptee.Execute(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(cql.GetStatementOptions())));
        }

        public void Execute(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(cql.GetStatementOptions()));
            _mapperAdaptee.Execute(cql);
        }

        public ICqlBatch CreateBatch()
        {
            return new CqlBatchAdapter(_mapperAdaptee.CreateBatch(), _connectionProvider);
        }

        public ICqlBatch CreateBatch(BatchType batchType)
        {
            return new CqlBatchAdapter(_mapperAdaptee.CreateBatch(batchType), _connectionProvider);
        }

        public void Execute(ICqlBatch batch)
        {
            _mapperAdaptee.Execute(batch);
        }

        public Task ExecuteAsync(IEnumerable<Cql> cqls)
        {
            var cqlBatch = CreateBatch();
            foreach (var cql in cqls)
            {
                cqlBatch.Execute(cql);
            }
            return ExecuteAsync(cqlBatch);
        }

        public Task ExecuteAsync(ICqlBatch batch)
        {
            if (batch.Statements.Any())
            {
                var size = 0;
                var count = 0;
                var cqls = new List<Cql>();
                var tasks = new List<Task>();
                foreach (var cql in batch.Statements)
                {
                    cqls.Add(cql);
                    var sizeofArguments = cql.Arguments.Where(argument => argument != null).Sum(argument => TypeHelper.SizeOf(argument));
                    size += sizeofArguments;
                    if (!(size >= _batchSizeThreshold) && !(count >= _batchCountThreshold))
                        continue;
                    var cqlBatch = new CqlBatchAdapter(_mapperAdaptee.CreateBatch(batch.BatchType), _connectionProvider);
                    cqlBatch.Execute(cqls);
                    tasks.Add(_mapperAdaptee.ExecuteAsync(cqlBatch));
                    size = count = 0;
                    cqls = new List<Cql>();
                }
                if (cqls.Any())
                {
                    var cqlBatch = new CqlBatchAdapter(_mapperAdaptee.CreateBatch(batch.BatchType), _connectionProvider);
                    cqlBatch.Execute(cqls);
                    tasks.Add(_mapperAdaptee.ExecuteAsync(cqlBatch));
                }
                return tasks.Await();
            }
            return TaskHelper.Completed;
        }

        public TDatabase ConvertCqlArgument<TValue, TDatabase>(TValue value)
        {
            return _mapperAdaptee.ConvertCqlArgument<TValue, TDatabase>(value);
        }

        public AppliedInfo<T> DeleteIf<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.DeleteIf<T>(cql, args);
        }

        public AppliedInfo<T> DeleteIf<T>(Cql cql)
        {
            return _mapperAdaptee.DeleteIf<T>(cql);
        }

        public Task<AppliedInfo<T>> DeleteIfAsync<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.DeleteIfAsync<T>(cql, args);
        }

        public Task<AppliedInfo<T>> DeleteIfAsync<T>(Cql cql)
        {
            return _mapperAdaptee.DeleteIfAsync<T>(cql);
        }

        public Task<AppliedInfo<T>> InsertIfNotExistsAsync<T>(T poco, CqlQueryOptions queryOptions = null)
        {
            return _mapperAdaptee.InsertIfNotExistsAsync(poco, queryOptions);
        }

        public Task<AppliedInfo<T>> InsertIfNotExistsAsync<T>(T poco, bool insertNulls, CqlQueryOptions queryOptions = null)
        {
            return _mapperAdaptee.InsertIfNotExistsAsync(poco, insertNulls, queryOptions);
        }

        public Task<AppliedInfo<T>> InsertIfNotExistsAsync<T>(T poco, bool insertNulls, int? ttl, CqlQueryOptions queryOptions = null)
        {
            return _mapperAdaptee.InsertIfNotExistsAsync(poco, insertNulls, ttl, queryOptions);
        }

        public AppliedInfo<T> InsertIfNotExists<T>(T poco, CqlQueryOptions queryOptions = null)
        {
            return _mapperAdaptee.InsertIfNotExists(poco, queryOptions);
        }

        public AppliedInfo<T> InsertIfNotExists<T>(T poco, bool insertNulls, CqlQueryOptions queryOptions = null)
        {
            return _mapperAdaptee.InsertIfNotExists(poco, insertNulls, queryOptions);
        }

        public AppliedInfo<T> InsertIfNotExists<T>(T poco, bool insertNulls, int? ttl, CqlQueryOptions queryOptions = null)
        {
            return _mapperAdaptee.InsertIfNotExists(poco, insertNulls, ttl, queryOptions);
        }

        public AppliedInfo<T> UpdateIf<T>(Cql cql)
        {
            return _mapperAdaptee.UpdateIf<T>(cql);
        }

        public AppliedInfo<T> UpdateIf<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.UpdateIf<T>(cql, args);
        }

        public Task<AppliedInfo<T>> UpdateIfAsync<T>(Cql cql)
        {
            return _mapperAdaptee.UpdateIfAsync<T>(cql);
        }

        public Task<AppliedInfo<T>> UpdateIfAsync<T>(string cql, params object[] args)
        {
            return _mapperAdaptee.UpdateIfAsync<T>(cql, args);
        }

        public Task<AppliedInfo<T>> ExecuteConditionalAsync<T>(ICqlBatch batch)
        {
            return _mapperAdaptee.ExecuteConditionalAsync<T>(batch);
        }

        public AppliedInfo<T> ExecuteConditional<T>(ICqlBatch batch)
        {
            return _mapperAdaptee.ExecuteConditional<T>(batch);
        }
    }

    public static class CqlExtensions
    {
        public static async Task<BoundStatement> ToBoundStatement(this Cql cql, IConnectionProvider connectionProvider)
        {
            PreparedStatement preparedStatementDecorator = await connectionProvider.Session.PrepareAsync(cql.Statement).ConfigureAwait(false);
            var boundStatementDecorator = preparedStatementDecorator.BindParams(cql.Arguments);
            return boundStatementDecorator;
        }

        public static async Task<BoundStatement> ToBoundStatement(this Cql cql, StatementOptions statementOptions, IConnectionProvider connectionProvider)
        {
            var preparedStatementDecorator = await connectionProvider.Session.PrepareAsync(cql.Statement).ConfigureAwait(false);
            var boundStatementDecorator = preparedStatementDecorator.BindParams(cql.Arguments);
            return boundStatementDecorator.Configure(statementOptions, connectionProvider);
        }

        public static async Task<BatchStatement> ToBatchStatement(this IEnumerable<Cql> cqls,
            IConnectionProvider connectionProvider, BatchStatement batchStatement)
        {
            var tasks = cqls.Select(cql => cql.ToBoundStatement(connectionProvider)).ToList();

            foreach (var task in tasks.Interleaved())
            {
                batchStatement.Add(await (await task.ConfigureAwait(false)).ConfigureAwait(false));
            }
            return batchStatement;
        }
    }
}
