﻿using System.Collections.Generic;
using Cassandra;
using Cassandra.Mapping;
using Infrastructure.ConnectionProvider;
using Infrastructure.Database.Cassandra.Manager.Extensions;

namespace Infrastructure.Database.Cassandra.Manager.Mapping
{
    public class CqlBatchAdapter : ICqlBatch
    {
        private readonly ICqlBatch _cqlBatch;
        private readonly IConnectionProvider _connectionProvider;

        public CqlBatchAdapter(ICqlBatch cqlBatch, IConnectionProvider connectionProvider)
        {
            _cqlBatch = cqlBatch;
            _connectionProvider = connectionProvider;
        }

        public void Insert<T>(T poco, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert);
            }
            _cqlBatch.Insert<T>(poco, queryOptions);
        }

        public void Insert<T>(T poco, bool insertNulls, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert);
            }
            _cqlBatch.Insert(poco, insertNulls, queryOptions);
        }

        public void Insert<T>(T poco, bool insertNulls, int? ttl, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert);
            }
            _cqlBatch.Insert(poco, insertNulls, ttl, queryOptions);
        }


        public void Update<T>(T poco, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert);
            }
            _cqlBatch.Update(poco, queryOptions);
        }

        public void Update<T>(string cql, params object[] args)
        {
            _cqlBatch.Update<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert)));
        }

        public void Update<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Upsert));
            _cqlBatch.Update<T>(cql);
        }

        public void Delete<T>(T poco, CqlQueryOptions queryOptions = null)
        {
            if (queryOptions == null)
            {
                queryOptions = _connectionProvider.GetCqlQueryOptions(StatementOptions.Delete);
            }
            _cqlBatch.Delete(poco, queryOptions);
        }

        public void Delete<T>(string cql, params object[] args)
        {
            _cqlBatch.Delete<T>(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Delete)));
        }

        public void Delete<T>(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(StatementOptions.Delete));
            _cqlBatch.Delete<T>(cql);
        }

        public void Execute(string cql, params object[] args)
        {
            _cqlBatch.Execute(Cql.New(cql, args).WithOptions(o => _connectionProvider.GetCqlQueryOptions(cql.GetStatementOptions())));
        }

        public void Execute(Cql cql)
        {
            cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(cql.GetStatementOptions()));
            _cqlBatch.Execute(cql);
        }

        public void Execute(IEnumerable<Cql> cqls)
        {
            //Preconditions.CheckNotNull(cqls);
            foreach (var cql in cqls)
            {
                cql.WithOptions(o => _connectionProvider.GetCqlQueryOptions(cql.GetStatementOptions()));
                Execute(cql);
            }
        }
        public TDatabase ConvertCqlArgument<TValue, TDatabase>(TValue value)
        {
            return _cqlBatch.ConvertCqlArgument<TValue, TDatabase>(value);
        }

        public void InsertIfNotExists<T>(T poco, CqlQueryOptions queryOptions = null)
        {
            _cqlBatch.InsertIfNotExists(poco, queryOptions);
        }

        public void InsertIfNotExists<T>(T poco, bool insertNulls, int? ttl, CqlQueryOptions queryOptions = null)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Cql> Statements
        {
            get { return _cqlBatch.Statements; }
        }

        public BatchType BatchType
        {
            get { return _cqlBatch.BatchType; }
            private set { }
        }
    }
}
