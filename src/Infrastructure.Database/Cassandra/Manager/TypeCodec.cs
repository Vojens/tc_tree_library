﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Cassandra;

namespace Infrastructure.Database.Cassandra.Manager
{
    internal delegate object DecodeHandler(int protocolVersion, IColumnInfo typeInfo, byte[] buffer, Type cSharpType);
    internal delegate byte[] EncodeHandler(int protocolVersion, IColumnInfo typeInfo, object value);

    /// <summary>
    /// Contains the methods handle serialization and deserialization from Cassandra types to CLR types
    /// </summary>
    public static class TypeCodec
    {
        private const string ListTypeName = "org.apache.cassandra.db.marshal.ListType";
        private const string SetTypeName = "org.apache.cassandra.db.marshal.SetType";
        private const string MapTypeName = "org.apache.cassandra.db.marshal.MapType";
        private const string UdtTypeName = "org.apache.cassandra.db.marshal.UserType";
        private const string TupleTypeName = "org.apache.cassandra.db.marshal.TupleType";
        public const string ReversedTypeName = "org.apache.cassandra.db.marshal.ReversedType";
        public const string CompositeTypeName = "org.apache.cassandra.db.marshal.CompositeType";
        private static readonly DateTimeOffset UnixStart = new DateTimeOffset(1970, 1, 1, 0, 0, 0, 0, TimeSpan.Zero);
        private static readonly ConcurrentDictionary<string, UdtMap> UdtMapsByName = new ConcurrentDictionary<string, UdtMap>();
        private static readonly ConcurrentDictionary<Type, UdtMap> UdtMapsByClrType = new ConcurrentDictionary<Type, UdtMap>();

        private static readonly Dictionary<ColumnTypeCode, DefaultTypeFromCqlTypeDelegate> DefaultTypes = new Dictionary<ColumnTypeCode, DefaultTypeFromCqlTypeDelegate>()
        {
            {ColumnTypeCode.Ascii,        GetDefaultTypeFromAscii},
            {ColumnTypeCode.Bigint,       GetDefaultTypeFromBigint},
            {ColumnTypeCode.Blob,         GetDefaultTypeFromBlob},
            {ColumnTypeCode.Boolean,      GetDefaultTypeFromBoolean},
            {ColumnTypeCode.Counter,      GetDefaultTypeFromCounter},
//            {ColumnTypeCode.Custom,       GetDefaultTypeFromCustom},
            {ColumnTypeCode.Double,       GetDefaultTypeFromDouble},
            {ColumnTypeCode.Float,        GetDefaultTypeFromFloat},
            {ColumnTypeCode.Int,          GetDefaultTypeFromInt},
            {ColumnTypeCode.Text,         GetDefaultTypeFromText},
            {ColumnTypeCode.Timestamp,    GetDefaultTypeFromTimestamp},
            {ColumnTypeCode.Uuid,         GetDefaultTypeFromUuid},
            {ColumnTypeCode.Varchar,      GetDefaultTypeFromVarchar},
            {ColumnTypeCode.Timeuuid,     GetDefaultTypeFromTimeuuid},
            {ColumnTypeCode.Inet,         GetDefaultTypeFromInet},
//            {ColumnTypeCode.List,         GetDefaultTypeFromList},
            {ColumnTypeCode.Map,          GetDefaultTypeFromMap},
            {ColumnTypeCode.Set,          GetDefaultTypeFromSet},
            {ColumnTypeCode.Decimal,      GetDefaultTypeFromDecimal},
            {ColumnTypeCode.Varint,       GetDefaultTypeFromVarint},
//            {ColumnTypeCode.Udt,          GetDefaultTypeFromUdt},
            {ColumnTypeCode.Tuple,        GetDefaultTypeFromTuple}
        };

        /// <summary>
        /// Default single (no collection types) cql type per CLR type
        /// </summary>
        private readonly static Dictionary<Type, ColumnTypeCode> DefaultSingleCqlTypes = new Dictionary<Type, ColumnTypeCode>()
        {
            { typeof(string), ColumnTypeCode.Text },
            { typeof(long), ColumnTypeCode.Bigint },
            { typeof(byte[]), ColumnTypeCode.Blob },
            { typeof(bool), ColumnTypeCode.Boolean },
            { typeof(double), ColumnTypeCode.Double },
            { typeof(float), ColumnTypeCode.Float },
            { typeof(IPAddress), ColumnTypeCode.Inet },
            { typeof(int), ColumnTypeCode.Int },
            { typeof(DateTimeOffset), ColumnTypeCode.Timestamp },
            { typeof(DateTime), ColumnTypeCode.Timestamp },
            { typeof(Guid), ColumnTypeCode.Uuid },
            { typeof(TimeUuid), ColumnTypeCode.Timeuuid },
            { TypeAdapters.DecimalTypeAdapter.GetDataType(), ColumnTypeCode.Decimal },
            { TypeAdapters.VarIntTypeAdapter.GetDataType(), ColumnTypeCode.Varint }
        };

        private static readonly Dictionary<string, ColumnTypeCode> SingleTypeNames = new Dictionary<string, ColumnTypeCode>()
        {
            {"org.apache.cassandra.db.marshal.UTF8Type", ColumnTypeCode.Varchar},
            {"org.apache.cassandra.db.marshal.AsciiType", ColumnTypeCode.Ascii},
            {"org.apache.cassandra.db.marshal.UUIDType", ColumnTypeCode.Uuid},
            {"org.apache.cassandra.db.marshal.TimeUUIDType", ColumnTypeCode.Timeuuid},
            {"org.apache.cassandra.db.marshal.Int32Type", ColumnTypeCode.Int},
            {"org.apache.cassandra.db.marshal.BytesType", ColumnTypeCode.Blob},
            {"org.apache.cassandra.db.marshal.FloatType", ColumnTypeCode.Float},
            {"org.apache.cassandra.db.marshal.DoubleType", ColumnTypeCode.Double},
            {"org.apache.cassandra.db.marshal.BooleanType", ColumnTypeCode.Boolean},
            {"org.apache.cassandra.db.marshal.InetAddressType", ColumnTypeCode.Inet},
            {"org.apache.cassandra.db.marshal.DateType", ColumnTypeCode.Timestamp},
            {"org.apache.cassandra.db.marshal.TimestampType", ColumnTypeCode.Timestamp},
            {"org.apache.cassandra.db.marshal.LongType", ColumnTypeCode.Bigint},
            {"org.apache.cassandra.db.marshal.DecimalType", ColumnTypeCode.Decimal},
            {"org.apache.cassandra.db.marshal.IntegerType", ColumnTypeCode.Varint},
            {"org.apache.cassandra.db.marshal.CounterColumnType", ColumnTypeCode.Counter}
        };

        private static readonly int SingleTypeNamesLength = SingleTypeNames.Keys.OrderByDescending(k => k.Length).First().Length;

        internal static byte[] GuidShuffle(byte[] b)
        {
            return new[] { b[3], b[2], b[1], b[0], b[5], b[4], b[7], b[6], b[8], b[9], b[10], b[11], b[12], b[13], b[14], b[15] };
        }

        internal static int BytesToInt32(byte[] buffer, int idx)
        {
            return (buffer[idx] << 24)
                   | (buffer[idx + 1] << 16 & 0xFF0000)
                   | (buffer[idx + 2] << 8 & 0xFF00)
                   | (buffer[idx + 3] & 0xFF);
        }

        //private static ushort BytesToUInt16(byte[] buffer, int idx)
        //{
        //    return (ushort)((buffer[idx] << 8) | (buffer[idx + 1] & 0xFF));
        //}

        private static byte[] Int32ToBytes(int value)
        {
            return new[]
            {
                (byte) ((value & 0xFF000000) >> 24),
                (byte) ((value & 0xFF0000) >> 16),
                (byte) ((value & 0xFF00) >> 8),
                (byte) (value & 0xFF)
            };
        }

        private static byte[] Int64ToBytes(long value)
        {
            return new[]
            {
                (byte) (((ulong) value & 0xFF00000000000000) >> 56),
                (byte) ((value & 0xFF000000000000) >> 48),
                (byte) ((value & 0xFF0000000000) >> 40),
                (byte) ((value & 0xFF00000000) >> 32),
                (byte) ((value & 0xFF000000) >> 24),
                (byte) ((value & 0xFF0000) >> 16),
                (byte) ((value & 0xFF00) >> 8),
                (byte) (value & 0xFF)
            };
        }

        private static long BytesToInt64(byte[] buffer, int idx)
        {
            return (long)(
                              (((ulong)buffer[idx] << 56) & 0xFF00000000000000)
                              | (((ulong)buffer[idx + 1] << 48) & 0xFF000000000000)
                              | (((ulong)buffer[idx + 2] << 40) & 0xFF0000000000)
                              | (((ulong)buffer[idx + 3] << 32) & 0xFF00000000)
                              | (((ulong)buffer[idx + 4] << 24) & 0xFF000000)
                              | (((ulong)buffer[idx + 5] << 16) & 0xFF0000)
                              | (((ulong)buffer[idx + 6] << 8) & 0xFF00)
                              | (((ulong)buffer[idx + 7]) & 0xFF)
                          );
        }

        //private static byte[] Int16ToBytes(short value)
        //{
        //    return new[] { (byte)((value & 0xFF00) >> 8), (byte)(value & 0xFF) };
        //}

        private static DateTimeOffset BytesToDateTimeOffset(byte[] buffer, int idx)
        {
            return UnixStart.AddMilliseconds(BytesToInt64(buffer, 0));
        }

        public static byte[] DateTimeOffsetToBytes(DateTimeOffset dt)
        {
            return Int64ToBytes(Convert.ToInt64(Math.Floor((dt - UnixStart).TotalMilliseconds)));
        }

        public static TimeSpan ToUnixTime(DateTimeOffset value)
        {
            return value - UnixStart;
        }

        public static Type GetDefaultTypeFromCqlType(ColumnTypeCode typeCode, IColumnInfo typeInfo)
        {
            DefaultTypeFromCqlTypeDelegate clrTypeHandler;
            if (!DefaultTypes.TryGetValue(typeCode, out clrTypeHandler))
            {
                throw new ArgumentException("No handler defined for type " + typeCode);
            }
            return clrTypeHandler(typeInfo);
        }

        internal static void CheckArgument(Type t, object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException();
            }
            if (!t.IsInstanceOfType(value))
            {
                throw new InvalidTypeException("value", value.GetType().FullName, new object[] { t.FullName });
            }
        }

        internal static void CheckArgument<T>(object value)
        {
            if (value == null)
                throw new ArgumentNullException();
            if (!(value is T))
                throw new InvalidTypeException("value", value.GetType().FullName, new object[] { typeof(T).FullName });
        }

        private static void CheckArgument<T1, T2>(object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException();
            }
            if (!(value is T1 || value is T2))
            {
                throw new InvalidTypeException("value", value.GetType().FullName, new object[] { typeof(T1).FullName, typeof(T2).FullName });
            }
        }

        public static object DecodeAscii(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            return Encoding.ASCII.GetString(value);
        }

        private static Type GetDefaultTypeFromAscii(IColumnInfo typeInfo)
        {
            return typeof(string);
        }

        public static byte[] EncodeAscii(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<string>(value);
            return Encoding.ASCII.GetBytes((string)value);
        }

        public static object DecodeBlob(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            return value;
        }

        private static Type GetDefaultTypeFromBlob(IColumnInfo typeInfo)
        {
            return typeof(byte[]);
        }

        public static byte[] EncodeBlob(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<byte[]>(value);
            return (byte[])value;
        }

        public static object DecodeBigint(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            return BytesToInt64(value, 0);
        }

        private static Type GetDefaultTypeFromBigint(IColumnInfo typeInfo)
        {
            return typeof(long);
        }

        public static byte[] EncodeBigint(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<long>(value);
            return Int64ToBytes((long)value);
        }

        public static object DecodeUuid(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            return new Guid(GuidShuffle(value));
        }

        private static Type GetDefaultTypeFromUuid(IColumnInfo typeInfo)
        {
            return typeof(Guid);
        }

        public static byte[] EncodeUuid(object value)
        {
            CheckArgument<Guid>(value);
            return GuidShuffle(((Guid)value).ToByteArray());
        }

        public static object DecodeVarint(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            var buffer = (byte[])value.Clone();
            Array.Reverse(buffer);
            return TypeAdapters.VarIntTypeAdapter.ConvertFrom(buffer);
        }

        private static Type GetDefaultTypeFromVarint(IColumnInfo typeInfo)
        {
            return TypeAdapters.VarIntTypeAdapter.GetDataType();
        }

        public static byte[] EncodeVarint(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            var ret = TypeAdapters.VarIntTypeAdapter.ConvertTo(value);
            Array.Reverse(ret);
            return ret;
        }

        ///// <summary>
        ///// Decodes length for collection types depending on the protocol version
        ///// </summary>
        //private static int DecodeCollectionLength(int protocolVersion, byte[] buffer, ref int index)
        //{
        //    int result;
        //    if (protocolVersion < 3)
        //    {
        //        //length is a short
        //        result = BytesToUInt16(buffer, index);
        //        index += 2;
        //    }
        //    else
        //    {
        //        //length is expressed in int
        //        result = BytesToInt32(buffer, index);
        //        index += 4;
        //    }
        //    return result;
        //}

        private static Type GetDefaultTypeFromSet(IColumnInfo typeInfo)
        {
            if (typeInfo == null)
            {
                throw new ArgumentNullException("typeInfo");
            }
            if (!(typeInfo is SetColumnInfo))
            {
                throw new InvalidTypeException("Expected SetColumnInfo, obtained " + typeInfo.GetType());
            }
            var innerTypeCode = (typeInfo as SetColumnInfo).KeyTypeCode;
            var innerTypeInfo = (typeInfo as SetColumnInfo).KeyTypeInfo;
            var valueType = GetDefaultTypeFromCqlType(innerTypeCode, innerTypeInfo);
            var openType = typeof(IEnumerable<>);
            return openType.MakeGenericType(valueType);
        }

        public static object DecodeTimestamp(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            if (cSharpType == null || cSharpType == typeof(object) || cSharpType == typeof(DateTimeOffset))
            {
                return BytesToDateTimeOffset(value, 0);
            }
            return BytesToDateTimeOffset(value, 0).DateTime;
        }

        private static Type GetDefaultTypeFromTimestamp(IColumnInfo typeInfo)
        {
            return typeof(DateTimeOffset);
        }

        public static byte[] EncodeTimestamp(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<DateTimeOffset, DateTime>(value);
            if (value is DateTimeOffset)
                return DateTimeOffsetToBytes((DateTimeOffset)value);
            var dt = (DateTime)value;
            // need to treat "Unspecified" as UTC (+0) not the default behavior of DateTimeOffset which treats as Local Timezone
            // because we are about to do math against EPOCH which must align with UTC. 
            // If we don't, then the value saved will be shifted by the local timezone when retrieved back out as DateTime.
            return DateTimeOffsetToBytes(dt.Kind == DateTimeKind.Unspecified
                                             ? new DateTimeOffset(dt, TimeSpan.Zero)
                                             : new DateTimeOffset(dt));
        }

        public static object DecodeTimeuuid(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            var decodedValue = new Guid(GuidShuffle(value));
            if (cSharpType == typeof(TimeUuid))
            {
                return (TimeUuid)decodedValue;
            }
            return decodedValue;
        }

        private static Type GetDefaultTypeFromTimeuuid(IColumnInfo typeInfo)
        {
            return typeof(Guid);
        }

        public static byte[] EncodeTimeuuid(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            if (value is TimeUuid)
            {
                value = ((TimeUuid)value).ToGuid();
            }
            CheckArgument<Guid>(value);
            return GuidShuffle(((Guid)value).ToByteArray());
        }

        private static Type GetDefaultTypeFromMap(IColumnInfo typeInfo)
        {
            if (typeInfo == null)
            {
                throw new ArgumentNullException("typeInfo");
            }
            if (!(typeInfo is MapColumnInfo))
            {
                throw new InvalidTypeException("Expected MapColumnInfo, obtained " + typeInfo.GetType());
            }
            var keyTypecode = (typeInfo as MapColumnInfo).KeyTypeCode;
            var keyTypeinfo = (typeInfo as MapColumnInfo).KeyTypeInfo;
            var valueTypecode = (typeInfo as MapColumnInfo).ValueTypeCode;
            var valueTypeinfo = (typeInfo as MapColumnInfo).ValueTypeInfo;
            var keyType = GetDefaultTypeFromCqlType(keyTypecode, keyTypeinfo);
            var valueType = GetDefaultTypeFromCqlType(valueTypecode, valueTypeinfo);

            var openType = typeof(IDictionary<,>);
            return openType.MakeGenericType(keyType, valueType);
        }

        public static object DecodeText(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            return Encoding.UTF8.GetString(value);
        }

        private static Type GetDefaultTypeFromText(IColumnInfo typeInfo)
        {
            return typeof(string);
        }

        public static byte[] EncodeText(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<string>(value);
            return Encoding.UTF8.GetBytes((string)value);
        }

        public static object DecodeVarchar(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            return Encoding.UTF8.GetString(value);
        }

        private static Type GetDefaultTypeFromVarchar(IColumnInfo typeInfo)
        {
            return typeof(string);
        }

        public static byte[] EncodeVarchar(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<string>(value);
            return Encoding.UTF8.GetBytes((string)value);
        }

        //private static byte[] EncodeBufferList(List<byte[]> bufferList, int bufferLength)
        //{
        //    //Add the necessary bytes length per each [bytes]
        //    bufferLength += bufferList.Count * 4;
        //    var result = new byte[bufferLength];
        //    var index = 0;
        //    foreach (var buf in bufferList)
        //    {
        //        var bufferItemLength = Int32ToBytes(buf != null ? buf.Length : -1);
        //        Buffer.BlockCopy(bufferItemLength, 0, result, index, bufferItemLength.Length);
        //        index += bufferItemLength.Length;
        //        if (buf == null)
        //        {
        //            continue;
        //        }
        //        Buffer.BlockCopy(buf, 0, result, index, buf.Length);
        //        index += buf.Length;
        //    }
        //    return result;
        //}

        ///// <summary>
        ///// Uses 2 or 4 bytes to represent the length in bytes
        ///// </summary>
        //private static byte[] EncodeCollectionLength(int protocolVersion, int value)
        //{
        //    if (protocolVersion < 3)
        //    {
        //        return Int16ToBytes((short)value);
        //    }
        //    return Int32ToBytes(value);
        //}

        public static object DecodeInet(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            if (value.Length == 4 || value.Length == 16)
            {
                return new IPAddress(value);
            }
            throw new DriverInternalError("Invalid length of Inet Addr");
        }

        private static Type GetDefaultTypeFromInet(IColumnInfo typeInfo)
        {
            return typeof(IPAddress);
        }

        public static byte[] EncodeInet(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<IPAddress>(value);
            return (value as IPAddress).GetAddressBytes();
        }

        public static object DecodeCounter(int protocolVersion, IColumnInfo typeInfo, byte[] buffer, Type cSharpType)
        {
            return BytesToInt64(buffer, 0);
        }

        private static Type GetDefaultTypeFromCounter(IColumnInfo typeInfo)
        {
            return typeof(long);
        }

        public static byte[] EncodeCounter(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<long>(value);
            return Int64ToBytes((long)value);
        }

        public static object DecodeDouble(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            var buffer = (byte[])value.Clone();
            Array.Reverse(buffer);
            return BitConverter.ToDouble(buffer, 0);
        }

        private static Type GetDefaultTypeFromDouble(IColumnInfo typeInfo)
        {
            return typeof(double);
        }

        public static byte[] EncodeDouble(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<double>(value);
            byte[] ret = BitConverter.GetBytes((double)value);
            Array.Reverse(ret);
            return ret;
        }

        public static object DecodeInt(int protocolVersion, IColumnInfo typeInfo, byte[] buffer, Type cSharpType)
        {
            return BytesToInt32(buffer, 0);
        }

        private static Type GetDefaultTypeFromInt(IColumnInfo typeInfo)
        {
            return typeof(int);
        }

        public static byte[] EncodeInt(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<int>(value);
            return Int32ToBytes((int)value);
        }

        public static object DecodeFloat(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            var buffer = (byte[])value.Clone();
            Array.Reverse(buffer);
            return BitConverter.ToSingle(buffer, 0);
        }

        private static Type GetDefaultTypeFromFloat(IColumnInfo typeInfo)
        {
            return typeof(float);
        }

        public static byte[] EncodeFloat(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<float>(value);
            byte[] ret = BitConverter.GetBytes((float)value);
            Array.Reverse(ret);
            return ret;
        }

        private static Type GetDefaultTypeFromTuple(IColumnInfo typeInfo)
        {
            if (typeInfo == null)
            {
                throw new ArgumentNullException("typeInfo");
            }
            if (!(typeInfo is TupleColumnInfo))
            {
                throw new ArgumentException("Expected TupleColumnInfo typeInfo, obtained " + typeInfo.GetType());
            }
            var tupleInfo = (TupleColumnInfo)typeInfo;
            Type genericTupleType;
            switch (tupleInfo.Elements.Count)
            {
                case 1:
                    genericTupleType = typeof(Tuple<>);
                    break;
                case 2:
                    genericTupleType = typeof(Tuple<,>);
                    break;
                case 3:
                    genericTupleType = typeof(Tuple<,,>);
                    break;
                case 4:
                    genericTupleType = typeof(Tuple<,,,>);
                    break;
                case 5:
                    genericTupleType = typeof(Tuple<,,,,>);
                    break;
                case 6:
                    genericTupleType = typeof(Tuple<,,,,,>);
                    break;
                case 7:
                    genericTupleType = typeof(Tuple<,,,,,,>);
                    break;
                default:
                    return typeof(byte[]);
            }

            return genericTupleType.MakeGenericType(tupleInfo.Elements.Select(s => GetDefaultTypeFromCqlType(s.TypeCode, s.TypeInfo)).ToArray());
        }

        public static byte[] EncodeCustom(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<byte[]>(value);
            return (byte[])value;
        }

        public static object DecodeBoolean(int protocolVersion, IColumnInfo typeInfo, byte[] buffer, Type cSharpType)
        {
            return buffer[0] == 1;
        }

        private static Type GetDefaultTypeFromBoolean(IColumnInfo typeInfo)
        {
            return typeof(bool);
        }

        public static byte[] EncodeBoolean(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            CheckArgument<bool>(value);
            var buffer = new byte[1];
            buffer[0] = ((bool)value) ? (byte)0x01 : (byte)0x00;
            return buffer;
        }

        public static object DecodeDecimal(int protocolVersion, IColumnInfo typeInfo, byte[] value, Type cSharpType)
        {
            var buffer = (byte[])value.Clone();
            return TypeAdapters.DecimalTypeAdapter.ConvertFrom(buffer);
        }

        private static Type GetDefaultTypeFromDecimal(IColumnInfo typeInfo)
        {
            return TypeAdapters.DecimalTypeAdapter.GetDataType();
        }

        public static byte[] EncodeDecimal(int protocolVersion, IColumnInfo typeInfo, object value)
        {
            byte[] ret = TypeAdapters.DecimalTypeAdapter.ConvertTo(value);
            return ret;
        }

        private delegate Type DefaultTypeFromCqlTypeDelegate(IColumnInfo typeInfo);

        /// <summary>
        /// Parses a given Cassandra type name to get the data type information
        /// </summary>
        /// <exception cref="ArgumentException" />
        internal static ColumnDesc ParseDataType(string typeName, int startIndex = 0, int length = 0)
        {
            var dataType = new ColumnDesc();
            if (length == 0)
            {
                length = typeName.Length;
            }
            if (length > ReversedTypeName.Length && typeName.Substring(startIndex, ReversedTypeName.Length) == ReversedTypeName)
            {
                //We don't care if the clustering order is reversed
                startIndex += ReversedTypeName.Length + 1;
                length -= ReversedTypeName.Length + 2;
            }
            //Quick check if its a single type
            if (length <= SingleTypeNamesLength)
            {
                ColumnTypeCode typeCode;
                if (startIndex > 0)
                {
                    typeName = typeName.Substring(startIndex, length);
                }
                if (SingleTypeNames.TryGetValue(typeName, out typeCode))
                {
                    dataType.TypeCode = typeCode;
                    return dataType;
                }
                throw GetTypeException(typeName);
            }
            if (typeName.Substring(startIndex, ListTypeName.Length) == ListTypeName)
            {
                //Its a list
                //org.apache.cassandra.db.marshal.ListType(innerType)
                //move cursor across the name and bypass the parenthesis
                startIndex += ListTypeName.Length + 1;
                length -= ListTypeName.Length + 2;
                var innerTypes = ParseParams(typeName, startIndex, length);
                if (innerTypes.Count != 1)
                {
                    throw GetTypeException(typeName);
                }
                dataType.TypeCode = ColumnTypeCode.List;
                var subType = ParseDataType(innerTypes[0]);
                dataType.TypeInfo = new ListColumnInfo()
                {
                    ValueTypeCode = subType.TypeCode,
                    ValueTypeInfo = subType.TypeInfo
                };
                return dataType;
            }
            if (typeName.Substring(startIndex, SetTypeName.Length) == SetTypeName)
            {
                //Its a Set
                //org.apache.cassandra.db.marshal.SetType(innerType)
                //move cursor across the name and bypass the parenthesis
                startIndex += SetTypeName.Length + 1;
                length -= SetTypeName.Length + 2;
                var innerTypes = ParseParams(typeName, startIndex, length);
                if (innerTypes.Count != 1)
                {
                    throw GetTypeException(typeName);
                }
                dataType.TypeCode = ColumnTypeCode.Set;
                var subType = ParseDataType(innerTypes[0]);
                dataType.TypeInfo = new SetColumnInfo()
                {
                    KeyTypeCode = subType.TypeCode,
                    KeyTypeInfo = subType.TypeInfo
                };
                return dataType;
            }
            if (typeName.Substring(startIndex, MapTypeName.Length) == MapTypeName)
            {
                //org.apache.cassandra.db.marshal.MapType(keyType,valueType)
                //move cursor across the name and bypass the parenthesis
                startIndex += MapTypeName.Length + 1;
                length -= MapTypeName.Length + 2;
                var innerTypes = ParseParams(typeName, startIndex, length);
                //It should contain the key and value types
                if (innerTypes.Count != 2)
                {
                    throw GetTypeException(typeName);
                }
                dataType.TypeCode = ColumnTypeCode.Map;
                var keyType = ParseDataType(innerTypes[0]);
                var valueType = ParseDataType(innerTypes[1]);
                dataType.TypeInfo = new MapColumnInfo()
                {
                    KeyTypeCode = keyType.TypeCode,
                    KeyTypeInfo = keyType.TypeInfo,
                    ValueTypeCode = valueType.TypeCode,
                    ValueTypeInfo = valueType.TypeInfo
                };
                return dataType;
            }
            if (typeName.Substring(startIndex, UdtTypeName.Length) == UdtTypeName)
            {
                //move cursor across the name and bypass the parenthesis
                startIndex += UdtTypeName.Length + 1;
                length -= UdtTypeName.Length + 2;
                var udtParams = ParseParams(typeName, startIndex, length);
                if (udtParams.Count < 2)
                {
                    //It should contain at least the keyspace, name of the udt and a type
                    throw GetTypeException(typeName);
                }
                dataType.TypeCode = ColumnTypeCode.Udt;
                dataType.Keyspace = udtParams[0];
                dataType.Name = HexToUtf8(udtParams[1]);
                var udtInfo = new UdtColumnInfo(dataType.Keyspace + "." + dataType.Name);
                for (var i = 2; i < udtParams.Count; i++)
                {
                    var p = udtParams[i];
                    var separatorIndex = p.IndexOf(':');
                    var c = ParseDataType(p, separatorIndex + 1, p.Length - (separatorIndex + 1));
                    c.Name = HexToUtf8(p.Substring(0, separatorIndex));
                    udtInfo.Fields.Add(c);
                }
                dataType.TypeInfo = udtInfo;
                return dataType;
            }
            if (typeName.Substring(startIndex, TupleTypeName.Length) == TupleTypeName)
            {
                //move cursor across the name and bypass the parenthesis
                startIndex += TupleTypeName.Length + 1;
                length -= TupleTypeName.Length + 2;
                var tupleParams = ParseParams(typeName, startIndex, length);
                if (tupleParams.Count < 1)
                {
                    //It should contain at least the keyspace, name of the udt and a type
                    throw GetTypeException(typeName);
                }
                dataType.TypeCode = ColumnTypeCode.Tuple;
                var tupleInfo = new TupleColumnInfo();
                foreach (var subTypeName in tupleParams)
                {
                    tupleInfo.Elements.Add(ParseDataType(subTypeName));
                }
                dataType.TypeInfo = tupleInfo;
                return dataType;
            }
            throw GetTypeException(typeName);
        }

        /// <summary>
        /// Converts a hex string to utf8 string
        /// </summary>
        private static string HexToUtf8(string hexString)
        {
            var bytes = Enumerable.Range(0, hexString.Length)
                 .Where(x => x % 2 == 0)
                 .Select(x => Convert.ToByte(hexString.Substring(x, 2), 16))
                 .ToArray();
            return Encoding.UTF8.GetString(bytes);
        }

        /// <summary>
        /// Parses comma delimited type parameters
        /// </summary>
        /// <returns></returns>
        private static List<string> ParseParams(string value, int startIndex, int length)
        {
            var types = new List<string>();
            var paramStart = startIndex;
            var level = 0;
            for (var i = startIndex; i < startIndex + length; i++)
            {
                var c = value[i];
                if (c == '(')
                {
                    level++;
                }
                if (c == ')')
                {
                    level--;
                }
                if (level == 0 && c == ',')
                {
                    types.Add(value.Substring(paramStart, i - paramStart));
                    paramStart = i + 1;
                }
            }
            //Add the last one
            types.Add(value.Substring(paramStart, length - (paramStart - startIndex)));
            return types;
        }

        private static Exception GetTypeException(string typeName)
        {
            return new ArgumentException(String.Format("Not a valid type {0}", typeName));
        }

        ///// <summary>
        ///// Sets a Udt map for a given Udt name
        ///// </summary>
        ///// <param name="name">Fully qualified udt name case sensitive (keyspace.udtName)</param>
        ///// <param name="map"></param>
        //        public static void SetUdtMap(string name, UdtMap map)
        //        {
        //            UdtMapsByName.AddOrUpdate(name, map, (k, oldValue) => map);
        //            UdtMapsByClrType.AddOrUpdate(map.NetType, map, (k, oldValue) => map);
        //        }

        /// <summary>
        /// Gets a UdtMap by fully qualified name.
        /// </summary>
        /// <param name="name">keyspace.udtName</param>
        /// <returns>Null if not found</returns>
        public static UdtMap GetUdtMap(string name)
        {
            UdtMap map;
            UdtMapsByName.TryGetValue(name, out map);
            return map;
        }

        /// <summary>
        /// Gets a UdtMap by fully qualified name.
        /// </summary>
        /// <returns>Null if not found</returns>
        public static UdtMap GetUdtMap(Type type)
        {
            UdtMap map;
            UdtMapsByClrType.TryGetValue(type, out map);
            return map;
        }

        /// <summary>
        /// Performs a lightweight validation to determine if the source type and target type matches.
        /// It isn't more invasive to support crazy uses of the driver, like direct inputs of blobs and all that. (backward compatibility)
        /// </summary>
        public static bool IsAssignableFrom(CqlColumn column, object value)
        {
            if (value == null || value is byte[])
            {
                return true;
            }
            var type = value.GetType();
            ColumnTypeCode cqlType;
            if (DefaultSingleCqlTypes.TryGetValue(type, out cqlType))
            {
                //Its a single type, if the types match -> go ahead
                if (cqlType == column.TypeCode) return true;
                //Only int32 and blobs are valid cql ints
                if (column.TypeCode == ColumnTypeCode.Int) return false;
                //Only double, longs and blobs are valid cql double
                if (column.TypeCode == ColumnTypeCode.Double && !(value is Int64)) return false;
                //The rest of the single values are not evaluated
                return true;
            }
            if (column.TypeCode == ColumnTypeCode.List || column.TypeCode == ColumnTypeCode.Set)
            {
                return value is IEnumerable;
            }
            if (column.TypeCode == ColumnTypeCode.Map)
            {
                return value is IDictionary;
            }
            return true;
        }
    }
}
