﻿using System;
using System.Linq.Expressions;
using Infrastructure.Common.Extensions.Reflection;

namespace Infrastructure.Database.Cassandra.Manager
{
    public class CqlFactory<T>
    {
        public T Value { get; private set; }
        public string Cql { get; private set; }
        public CqlFactory(T value, string cql)
        {
            Value = value;
            Cql = cql;
        }

        public CqlFactory<T> Map<TResult>(Expression<Func<T, TResult>> expression, bool isBindColumnName = true)
        {
            var fieldInfo = ReflectOn<T>.GetProperty(expression);
            fieldInfo.GetValue(Value);
            return this;
        }

        public static string CreateCql(string basicCql, string tableName)
        {
            //tableName.
            return null;
        }
    }
}
