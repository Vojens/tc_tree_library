﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure.ACD.Business;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Model.Paging;
using Model.ETP.Protocol.Replication.Enumerations;
using Model.Replication;
using Model.ETP.Protocol.Replication;

namespace Infrastructure.Graph.Service
{
    public interface IGraphService
    {
        ICompositeAclService AclService { get; }

        Task<bool> Exists(Guid resourceId);

        Task<bool> Exists(string resourceUri);

        Task<bool> HasChildren(string resourceUri);

        Task<bool> HasChildren(string resourceUri, string resourceType);

        Task<ResourceInternal> Get(Guid resourceId, FetchFlags fetch = FetchFlags.None);

        Task<List<KeyValuePair<string, HashSet<PropertyInternal>>>> GetProperties(Guid resourceId);

        IObservable<PropertyInternal> GetProperties(Guid resourceId, string groupName);

        Task<PropertyInternal> GetProperty(Guid resourceId, string groupName, string key);

        Task<ResourceInternal> Get(string resourceUri, FetchFlags fetch = FetchFlags.None);

        Task<bool> TryGet(Guid resourceId, Ref<ResourceInternal> @ref, FetchFlags fetch = FetchFlags.None, bool includeDeleted = false);

        Task<bool> TryGetByCondition(PropertyCondition condition, Ref<ResourceInternal> @ref);
        Task<bool> TryGetByCondition(MetadataCondition condition, Ref<ResourceInternal> @ref);
        Task<bool> TryGetByMetadataCondition(Condition condition, Ref<ResourceInternal> @ref);

        Task<bool> TryGet(string resourceUri, Ref<ResourceInternal> @ref, FetchFlags fetch = FetchFlags.None);

        Task<bool> TryGet(Guid resourceId, Ref<string> @ref);

        Task Add(ResourceInternal resource, bool skipPermCheck = false, object resourceContent = null);

        Task AddAuditTrail(ResourceAudit resource);

        Task AddProperty(Guid resourceId, IDictionary<string, HashSet<PropertyInternal>> properties);

        Task Add(IEnumerable<ResourceInternal> resources, bool skipPermCheck = false, Dictionary<string, object> resourceContentsDict = null);

        Task Update(ResourceInternal resource);

        Task UpdateProperties(ResourceInternal resource);

        Task UpdateLastUpdateDetails(Guid resourceId, DateTimeOffset timeStamp, Guid userId, string userName);

        Task MarkAs(Guid resourceId, bool isDeleted);

        Task<ResourceInternal> Delete(Guid resourceId);

        Task DeleteProperties(Guid id);

        Task DeleteProperties(Guid resourceId, string groupName);

        Task DeleteProperty(Guid resourceId,string groupName, string key);

        Task Delete(string resourceUri);

        Task<IList<ResourceInternal>> CascadeDelete(Guid resourceId);

        Task CascadeDelete(string resourceUri);

        Task<IList<ResourceInternal>> CascadeDeleteWithResult(string resourceUri);

        Task<IList<ResourceInternal>> CascadeDeleteWithResult(Guid resourceId);

        IObservable<ResourceInternal> ReadSelf(Guid resourceId, string resourceType = null,
            FetchFlags fetch = FetchFlags.None, bool includeDeleted = false);

        IObservable<ResourceInternal> ReadSelfWithoutPermission(string resourceUri, string resourceType = null,
            FetchFlags fetch = FetchFlags.None);

        [Obsolete("Please do not use this unless absolutely necessary")]
        IObservable<ResourceInternal> ReadSimpleResourceWithoutPermission(string resourceUri);

        [Obsolete("Please do not use this unless absolutely necessary")]
        IObservable<ResourceInternal> ReadSimpleResource(string resourceUri);
        
        [Obsolete("Please do not use this unless absolutely necessary")]
        ResourceInternal CqlReadSimpleResourceWithoutPermission(string resourceUri);

        [Obsolete("Please do not use this unless absolutely necessary")]
        Task<ResourceInternal> CqlReadSimpleResource(string resourceUri);

        [Obsolete("Please do not use this unless absolutely necessary")]
        IObservable<ResourceInternal> ReadSimpleDescendants(string resourceUri, string resourceType = null,
            short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null);

        [Obsolete("Please do not use this unless absolutely necessary")]
        IObservable<ResourceInternal> ReadSimpleDescendantsWithoutPermission(string resourceUri,
            string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null);
      
        IObservable<ResourceInternal> ReadSelf(string resourceUri, string resourceType = null,
            FetchFlags fetch = FetchFlags.None);

        IObservable<ResourceInternal> ReadDescendants(Guid resourceId, string resourceType = null,
            FetchFlags fetch = FetchFlags.None, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null);

        IObservable<ResourceInternal> ReadDescendants(string resourceUri, string resourceType = null,
            FetchFlags fetch = FetchFlags.None, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null);

        IObservable<ResourceInternal> ReadInResources(Guid resourceId, short startCd = short.MinValue, short endCd = -1,
            FetchFlags fetch = FetchFlags.None, params Condition[] conditions);
      
        //Task<PagedResource> ReadPagedDescendants(string resourceUri, string resourceType = null, short startCd = 1,
        //    short endCd = 1, FetchFlags fetch = FetchFlags.None, QueryOptions query = null);

        Task<PagedResult<ResourceInternal>> ReadDescendantsPaged(string resourceUri, string resourceType = null, short startCd = 1, short endCd = 1, FetchFlags fetch = FetchFlags.None, QueryOptions query = null, bool skipAclCheck = false);

        Task<PagedResult<ResourceInternal>> ReadDescendantsPaged(string[] resourceUris, string resourceType = null, short startCd = 1, short endCd = 1, FetchFlags fetch = FetchFlags.None, QueryOptions query = null, bool skipAclCheck = false);

        Task<PagedResult<ResourceInternal>> ReadRecentDescendantsPaged(string resourceUri, int noOfDays, string resourceType = null,
            short startCd = 1, short endCd = 1, FetchFlags fetch = FetchFlags.None, QueryOptions query = null);

        Task<PagedResult<ResourceInternal>> SearchDescendantsPagedWithProperties(string resourceUri,
            string resourceType = null, short startCd = 1, short endCd = 1, FetchFlags fetch = FetchFlags.None,
            QueryOptions query = null, List<Condition> metadataConditions = null, List<Condition> propertyConditions = null);

        Task<PagedResult<ResourceInternal>> SearchRecentDescendantsPagedWithProperties(string resourceUri, int noOfDays,
            string resourceType = null, short startCd = 1, short endCd = 1, FetchFlags fetch = FetchFlags.None,
            QueryOptions query = null, List<Condition> metadataConditions = null, List<Condition> propertyConditions = null);

        Task<PagedResult<ResourceAudit>> ReadAuditPaged(string resourceUri, QueryOptions query, short startCd = 1, short endCd = 1);

        Task<string> GetResourceUriById(Guid resourceId);        
        Task<ResourceInternal> GetCql(Guid id, FetchFlags fetch = FetchFlags.None);
        Task<ResourceInternal> GetCql(string uri, FetchFlags fetch = FetchFlags.None);
        Task AddReplicationEvent(ReplicationEventInternal replicationEvent);
        IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, sbyte status);

        IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, List<sbyte> status);

        IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, sbyte status, int limit);

        IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, sbyte status, DateTimeOffset since, int limit);

        IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, List<sbyte> status, DateTimeOffset lastTimestamp, int limit);

        IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, List<sbyte> status, params string[] resourceType);

        IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, List<sbyte> status, string resourceName, params string[] resourceType);

        IObservable<ResourceEventInternal> GetResourceEvents(DateTimeOffset fromDt, DateTimeOffset toDt);

        void HandleReplicationEvent(Guid resourceId, string version, string uniqueIdentifier, DateTimeOffset timestamp, ObjectTypes objectType, ActionTypes actionType, Guid parentResourceId, Guid ruleId, string oldName=null);

        void PublishRuleContentsUpdated(UpdatedSeaContents updatedSeaContents, UpdatedSecurityContents securityContents);

        void PublishRuleContentsUpdated(ReplicationRuleUpdated ruleUpdated);

        //void PublishRuleUpdated(ReplicationRule rule);
        void PublishRuleUpdated(TestReplicationRule rule);

        void PublishResourceHandled(ResourceHandledEvent resourceHandled);
        Task<ResourceAudit> GetAudit(Guid resourceId, Guid auditId);
    }
}
