using System;
using Infrastructure.Graph.Model;
using Model.ETP.Protocol.Replication.Enumerations;
using Model.Replication;
using Model.ETP.Protocol.Replication;

namespace Infrastructure.Graph.Service.Eventing
{
    internal class NullEventPublisher : IEventPublisher
    {
        public void PublishAddResource(Tuple<ResourceInternal, string, byte[]> resource, string ascendantUri, string source, Action<Exception> onError = null)
        {
            
        }

        public void PublishAddResource(ResourceInternal resource, string source, Action<Exception> onError = null, object resourceContent = null)
        {
            
        }

        public void PublishUpdateResource(ResourceInternal resource, string source, Action<Exception> onError = null)
        {
            
        }

        public void PublishDeleteResource(ResourceInternal resource, string source, Action<Exception> onError = null)
        {
            
        }

        public void Start()
        {
            
        }

        public void Stop()
        {
            
        }

        public void PublishReplicationResourceEvent(HD.Model.Replication.ResourceEvent resourceEvent, Action<Exception> onError = null)
        {
        }

        public void PublishRuleContentsUpdated(UpdatedSeaContents updatedSeaContents, UpdatedSecurityContents securityContents)
        {

        }

        public void PublishRuleContentsUpdated(ReplicationRuleUpdated ruleUpdated)
        {

        }
        
        //public void PublishRuleUpdated(ReplicationRule rule)
        public void PublishRuleUpdated(TestReplicationRule rule)
        {

        }

        public void PublishResourceHandled(ResourceHandledEvent resourceHandled)
        {
            
        }
    }
}