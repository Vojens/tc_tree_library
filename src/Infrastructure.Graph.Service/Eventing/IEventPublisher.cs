using System;
using Infrastructure.Graph.Model;
using Model.ETP.Protocol.Replication.Enumerations;
using Model.Replication;
using Model.ETP.Protocol.Replication;

namespace Infrastructure.Graph.Service.Eventing
{
    public interface IEventPublisher
    {
        void PublishAddResource(Tuple<ResourceInternal, string, byte[]> resource, string ascendantUri, string source, Action<Exception> onError = null);
        void PublishAddResource(ResourceInternal resource, string source, Action<Exception> onError = null, object resourceContent = null);
        void PublishUpdateResource(ResourceInternal resource, string source, Action<Exception> onError = null);
        void PublishDeleteResource(ResourceInternal resource, string source, Action<Exception> onError = null);
        void PublishReplicationResourceEvent(ResourceEvent resourceEvent, Action<Exception> onError = null);
        void PublishRuleContentsUpdated(UpdatedSeaContents updatedSeaContents, UpdatedSecurityContents securityContents);
        //void PublishRuleUpdated(ReplicationRule rule);
        void PublishRuleUpdated(TestReplicationRule rule);
        void PublishResourceHandled(ResourceHandledEvent resourceHandled);
        void PublishRuleContentsUpdated(ReplicationRuleUpdated ruleUpdated);
        void Start();
        void Stop();
    }
}