using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cassandra;
using Newtonsoft.Json;
using Datatypes.Object;
using ESB;
using Event;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Extensions.Type;
using Infrastructure.Common.IoC;
using Infrastructure.Common.Util;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Service.Extensions;
using Model.ETP.Protocol.Replication.Enumerations;
using Model.Replication;
using Serialization.Messages;
using Energistics.Datatypes;
using Model.ETP.Protocol.Replication;

namespace Infrastructure.Graph.Service.Eventing
{
    internal class RabbitMqPublisher : IEventPublisher
    {
        private readonly IEventBus _bus;
        private readonly string _staticResourceExchange = ConfigurationManager.AppSettings["staticResourceExchangeName"];
        private readonly string _seaHashedResource = (ConfigurationManager.AppSettings["ReplicationExchange"] ?? "Replication") + "_SEA_RESOURCE_CH";
        private readonly string _securityHashedExchange = (ConfigurationManager.AppSettings["ReplicationExchange"] ?? "Replication") + "_Security_CH";
        private readonly string _replicationControlEventsExchange = (ConfigurationManager.AppSettings["ReplicationExchange"] ?? "Replication") + "_ControlEvents";
        private const string HashExchangeType = "x-consistent-hash";

        public RabbitMqPublisher()
        {
            _bus = EventBus.Instance;
        }

        public void PublishAddResource(Tuple<ResourceInternal, string, byte[]> resource, string ascendantUri, string source, Action<Exception> onError = null)
        {
            var Resource = resource.Item1.ToResource();
            Resource.ContentEncoding = resource.Item2;
            Resource.Data = resource.Item3;
            _bus.PublishResourceCreated(_staticResourceExchange, Resource, source, onError: onError);

        }

        private static PVEvent CreateEvent(Datatypes.Object.Resource resource, string source, EventType eventType)
        {

            var tag = string.Format("{0}.{1}", resource.GetTag(), eventType);

            var Event = new PVEvent
            {
                ResourceId = new Guid(resource.ResourceMetadata.Uuid),
                Id = Guid.NewGuid(),
                Tag = tag,
                Source = source,
                TimeStamp = DateTime.Now.ToDateTime(),
                ContentType = "",
                ContentEncoding = "",
                Body = new byte[0],
                Extensions = new Dictionary<string, string>(),
                Resource = resource
            };
            return Event;
        }


        public void PublishAddResource(ResourceInternal resource, string source, Action<Exception> onError = null, object resourceContent = null)
        {
            var Resource = resourceContent == null ? resource.ToResource() : resource.ToResource(resourceContent);
            var pvEvent = CreateEvent(Resource, source, EventType.created);
            _bus.PublishResourceEvent(_staticResourceExchange, Resource.ResourceMetadata.Uri, EventType.created.ToString(), pvEvent, onError: onError);
        }

        public void PublishAddResource(ResourceInternal resource, string ascendantUri, string source, Action<Exception> onError = null)
        {
            var Resource = resource.ToResource();

            if (resource.Data != null)
            {
                var data = resource.Data.Content;
                var contentEncoding = resource.Data.ContentEncoding;

                //if (resourceXml.IsBetterToGzip())
                //{
                //    contentEncoding = "gzip";
                //    data = data.Gzip();
                //}

                Resource.ContentEncoding = contentEncoding;
                Resource.Data = data;
            }

            _bus.PublishResourceCreated(_staticResourceExchange, Resource, source, onError: onError);
        }

        public void PublishUpdateResource(ResourceInternal resource, string source, Action<Exception> onError = null)
        {
            var Resource = resource.ToResource();
            if (resource.Data != null)
            {
                var data = resource.Data.Content;
                var contentEncoding = resource.Data.ContentEncoding;

#warning Enable IsBetterToGzip
                //if (resourceXml.IsBetterToGzip())
                //{
                //    contentEncoding = "gzip";
                //    data = data.Gzip();
                //}

                Resource.ContentEncoding = contentEncoding;
                Resource.Data = data;
            }
            var pvEvent = CreateEvent(Resource, source, EventType.updated);

            _bus.PublishResourceEvent(_staticResourceExchange, resource.ResourceMetadata.Uri, EventType.updated.ToString(), pvEvent, onError: onError);
        }

        public void PublishDeleteResource(ResourceInternal resource, string source, Action<Exception> onError = null)
        {
            var Resource = resource.ToResource();
            var pvEvent = CreateEvent(Resource, source, EventType.deleted);
            _bus.PublishResourceEvent(_staticResourceExchange, resource.ResourceMetadata.Uri, EventType.deleted.ToString(), pvEvent, onError: onError);
        }


        public void PublishReplicationResourceEvent(ResourceEvent resEvent,
            Action<Exception> onError = null)
        {
            switch (resEvent.ObjectType)
            {
                case ObjectTypes.User:
                case ObjectTypes.Group:
                case ObjectTypes.Company:
                    _bus.PublishRealtime(_securityHashedExchange, resEvent, routingKey: Guid.NewGuid().ToString(), exchangeType: HashExchangeType);
                    break;
                case ObjectTypes.Set:
                case ObjectTypes.Element:
                case ObjectTypes.Attachment:
                    _bus.PublishRealtime(_seaHashedResource, resEvent, routingKey: Guid.NewGuid().ToString(), exchangeType: HashExchangeType);
                    break;
            }
        }

        //public void PublishRuleUpdated(ReplicationRule rule)
        public void PublishRuleUpdated(TestReplicationRule rule)
        {
            _bus.PublishRealtime(_replicationControlEventsExchange, rule, routingKey: rule.RuleId.ToString());
        }

        public void PublishResourceHandled(ResourceHandledEvent resourceHandled)
        {
            _bus.PublishRealtime(_replicationControlEventsExchange, resourceHandled);
        }

        public void PublishRuleContentsUpdated(ReplicationRuleUpdated ruleUpdated)
        {
            _bus.PublishRealtime(_seaHashedResource, ruleUpdated, routingKey: Guid.NewGuid().ToString(), exchangeType: HashExchangeType);

            _bus.PublishRealtime(_securityHashedExchange, ruleUpdated, routingKey: Guid.NewGuid().ToString(), exchangeType: HashExchangeType);
        }

        public void PublishRuleContentsUpdated(UpdatedSeaContents updatedSeaContents, UpdatedSecurityContents securityContents)
        {
            _bus.PublishRealtime(_seaHashedResource, updatedSeaContents, routingKey: Guid.NewGuid().ToString(), exchangeType: HashExchangeType);

            _bus.PublishRealtime(_securityHashedExchange, securityContents, routingKey: Guid.NewGuid().ToString(), exchangeType: HashExchangeType);
        }

        public void Start()
        {
            if (_bus.IsConnectionOpened) return;
            //_bus.Open();
        }

        public void Stop()
        {
            if (!_bus.IsConnectionOpened) return;
            // _bus.Close();
        }
    }
}