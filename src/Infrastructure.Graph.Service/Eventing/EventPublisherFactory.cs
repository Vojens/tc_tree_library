using System.Configuration;

namespace Infrastructure.Graph.Service.Eventing
{
    public static class EventPublisherFactory
    {
        private static IEventPublisher _publisher;

        static EventPublisherFactory()
        {
            var eventPublisher = ConfigurationManager.AppSettings["EnableEventPublisher"];

            if (string.IsNullOrWhiteSpace(eventPublisher) || bool.Parse(eventPublisher) == false)
                _publisher = new NullEventPublisher();
            else
                _publisher = new RabbitMqPublisher();
        }

        public static IEventPublisher Create()
        {
            return _publisher;
        }
    }
}