﻿using System;

namespace Infrastructure.Graph.Service
{
    public static class Global
    {
        internal const string ConfigType = "Graph";
        public static Guid RootId = Guid.Parse("06073a9c-9cef-4f07-9180-2e54e0fa6416");
        public static string RootUri = ".";
    }
}
