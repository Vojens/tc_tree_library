using LightInject;
using Infrastructure.ConnectionProvider;
using Infrastructure.Common.IoC;
using Infrastructure.Graph.Service.Eventing;

namespace Infrastructure.Graph.Service.Registry
{
    public class GraphRegistry : ICompositionRoot
    {
        public void Compose(IServiceRegistry serviceRegistry)
        {
            var connectionProvider = new CassandraConnectionProvider(Global.ConfigType);
            connectionProvider.Connect();

            serviceRegistry.Register<IConnectionProvider>(factory => connectionProvider, new PerContainerLifetime());

            serviceRegistry.Register(factory => EventPublisherFactory.Create(), new PerContainerLifetime());
            var resourceService = new ResourceService(connectionProvider);
            serviceRegistry.Register<ResourceService>(factory => resourceService, new PerContainerLifetime());
        }

        public static void Init()
        {
            ComponentRegistry.RegisterFrom<GraphRegistry>();
        }
    }
}