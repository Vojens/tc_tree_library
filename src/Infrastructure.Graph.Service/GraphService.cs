using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Infrastructure.ACD.Business;
using Infrastructure.ACD.Entities;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.IoC;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Model.Paging;
using Infrastructure.Graph.Service.Eventing;
using Model.ETP.Protocol.Replication.Enumerations;
using Infrastructure.Graph.Service.Core.Helper;
using Model.Replication;
using System.Threading.Tasks.Dataflow;
using Model.ETP.Protocol.Replication;

namespace Infrastructure.Graph.Service
{
    public class GraphService : IGraphService
    {
        private readonly IDomainInfo _domainInfo;
        private readonly IGraphClientInfo _clientInfo;
        private readonly ResourceService _resourceService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICompositeAclService _aclService;
        private readonly ActionBlock<ResourceEventInternal> _replicationResourceEventHandler;

        public GraphService(IDomainInfo domainInfo, IGraphClientInfo clientInfo)
        {
            _domainInfo = domainInfo;
            _clientInfo = clientInfo;
            _resourceService = ComponentRegistry.Resolve<ResourceService>();
            _eventPublisher = EventPublisherFactory.Create();
            _aclService = new CompositeAclService(clientInfo);

            _replicationResourceEventHandler = new ActionBlock<ResourceEventInternal>(async resourceEventInternal =>
            {
                await _resourceService.AddResourceEvent(resourceEventInternal).ConfigureAwait(false);
            });
        }

        public ICompositeAclService AclService
        {
            get { return new CompositeAclService(_clientInfo); }
        }

        public Task<bool> Exists(Guid resourceId)
        {
            return _resourceService.Exists(resourceId);
        }

        public Task<bool> Exists(string resourceUri)
        {
            return _resourceService.Exists(resourceUri);
        }

        public Task<bool> HasChildren(string resourceUri)
        {
            return _resourceService.HasChildren(resourceUri);
        }

        public async Task<bool> HasChildren(string resourceUri, string[] resourceTypes)
        {
            if (resourceTypes.Length > 1)
            {
                if (await _resourceService.HasChildren(resourceUri, string.Join(",", resourceTypes), _clientInfo.UserIdentities, Permissions.Read).ConfigureAwait(false))
                {
                    return true;
                }
            }
            else if (resourceTypes.Length == 1)
            {
                if (await HasChildren(resourceUri, resourceTypes[0]).ConfigureAwait(false))
                {
                    return true;
                }
            }
            return false;
        }

        public Task<bool> HasChildren(string resourceUri, string resourceType)
        {
            return _resourceService.HasChildren(resourceUri, resourceType, _clientInfo.UserIdentities, Permissions.Read);
        }

        public async Task<ResourceInternal> Get(Guid resourceId, FetchFlags fetch = FetchFlags.None)
        {
            var refResource = new Ref<ResourceInternal>();
            await TryGetCql(resourceId, refResource, fetch).ConfigureAwait(false);
            return refResource.Value;
        }

        public async Task<List<KeyValuePair<string, HashSet<PropertyInternal>>>> GetProperties(Guid resourceId)
        {
            return await _resourceService.GetProperties(resourceId).ConfigureAwait(false);
        }

        public IObservable<PropertyInternal> GetProperties(Guid resourceId, string groupName)
        {
            return _resourceService.GetProperties(resourceId, groupName);
        }

        public async Task<PropertyInternal> GetProperty(Guid resourceId, string groupName, string key)
        {
            return await _resourceService.GetProperty(resourceId, groupName, key).ConfigureAwait(false);
        }

        public async Task<ResourceInternal> Get(string resourceUri, FetchFlags fetch = FetchFlags.None)
        {
            var refResource = new Ref<ResourceInternal>();
            await TryGetCql(resourceUri, refResource, fetch).ConfigureAwait(false);
            return refResource.Value;
        }

        public Task<Guid> GetId(string resourceUri)
        {
            return _resourceService.GetId(resourceUri);
        }

        public async Task<bool> TryGet(Guid resourceId, Ref<ResourceInternal> @ref, FetchFlags fetch = FetchFlags.None, bool includeDeleted = false)
        {
            if(await TryGetCql(resourceId, @ref, fetch, includeDeleted).ConfigureAwait(false))
            {
                return true;
            }
            return false;
        }

        public async Task<bool> TryGetByCondition(PropertyCondition condition, Ref<ResourceInternal> @ref)
        {
            var resourceId = await _resourceService.GetResourceIdFromProperty(condition).ConfigureAwait(false);
            return await TryGet(resourceId, @ref, FetchFlags.Links | FetchFlags.Properties).ConfigureAwait(false);
        }

        public async Task<bool> TryGetByCondition(MetadataCondition condition, Ref<ResourceInternal> @ref)
        {
            var resourceId = await _resourceService.GetResourceIdFromMetadata(condition, _clientInfo.UserIdentities).ConfigureAwait(false);
            if (Guid.Empty.Equals(resourceId)) return false;
            return await TryGet(resourceId, @ref, FetchFlags.Links | FetchFlags.Properties).ConfigureAwait(false);
        }

        public async Task<bool> TryGetByMetadataCondition(Condition condition, Ref<ResourceInternal> @ref)
        {
            var resourceId = await _resourceService.GetResourceIdFromMetadata(condition, _clientInfo.UserIdentities).ConfigureAwait(false);
            if (Guid.Empty.Equals(resourceId)) return false;
            return await TryGet(resourceId, @ref, FetchFlags.Links | FetchFlags.Properties).ConfigureAwait(false);
        }

        public async Task<bool> TryGet(string resourceUri, Ref<ResourceInternal> @ref, FetchFlags fetch = FetchFlags.None)
        {
            if (await TryGetCql(resourceUri, @ref, fetch).ConfigureAwait(false))
            {
                return true;
            }
            return false;
        }

        public async Task<bool> TryGetCql(string resourceUri, Ref<ResourceInternal> @ref, FetchFlags fetch = FetchFlags.None, bool includeDeleted = false)
        {
            var refId = new Ref<Guid>();
            if (await _resourceService.TryGetId(resourceUri, refId).ConfigureAwait(false))
            {
                if (await AclService.PermissionService.HasPermission(refId.Value, _clientInfo.UserIdentities, Permissions.Read).ConfigureAwait(false))
                {
                    if (await _resourceService.TryGet(refId.Value, @ref, fetch, includeDeleted).ConfigureAwait(false))
                    {
                        return true;
                    }
                }
                return false;
            }
            return false;
        }

        public async Task<bool> TryGetCql(Guid id, Ref<ResourceInternal> @ref, FetchFlags fetch = FetchFlags.None, bool includeDeleted = false)
        {
            if (await AclService.PermissionService.HasPermission(id, _clientInfo.UserIdentities, Permissions.Read).ConfigureAwait(false))
            {
                if(await _resourceService.TryGet(id, @ref, fetch, includeDeleted).ConfigureAwait(false))
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> TryGet(Guid resourceId, Ref<string> @ref)
        {
            var resources = await ReadSelf(resourceId).AsAsyncList().ConfigureAwait(false);

            if (!resources.Any()) return false;

            @ref.Value = resources.First().ResourceMetadata.Uri;
            return true;
        }

        public async Task Add(ResourceInternal resource, bool skipPermCheck = false, object resourceContent = null)
        {
            if (resource.Parents.Count == 1)
            {
                await AddWithSingleParent(resource, skipPermCheck, resourceContent).ConfigureAwait(false);
            }
            else
            {
                await AddWithMultiParent(resource, skipPermCheck, resourceContent).ConfigureAwait(false);
            }
        }

        private async Task AddWithSingleParent(ResourceInternal resource, bool skipPermCheck = false, object resourceContent = null)
        {
            //TODO: Domain should handle parentid
            Guid parentId;
            if (resource.Parents.First().Id == Guid.Empty)
            {
                if (resource.Parents.First().Uri == ".")
                {
                    parentId = Global.RootId;
                }
                else
                {
                    parentId = await GetId(resource.Parents.First().Uri).ConfigureAwait(false);
                }
            }
            else
                parentId = resource.Parents.First().Id;

            if (!skipPermCheck)
            {
                if (!await AclService.PermissionService.HasCreatePermission(parentId, _clientInfo.UserIdentities, resource.ResourceMetadata.Type).ConfigureAwait(false))
                {
                    throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
                }
            }
            await _resourceService.Add(resource).ConfigureAwait(false);

            var distest = resource.ResourceMetadata.ResourceSystem == "SEA" ? (string.IsNullOrEmpty(resource.ResourceMetadata.Description) ? resource.ResourceMetadata.Uri : resource.ResourceMetadata.Description) : resource.ResourceMetadata.Uri;

            await AclService.ResourceCreateService.Handle(new Dictionary<string, object>{{"ResourceParentId",parentId},
                {"DisplayText", distest},
                {"ResourceId",resource.ResourceMetadata.Id},
                {"ResourceName",resource.ResourceMetadata.Name},
                {"ResourceType",resource.ResourceMetadata.Type},
                {"ResourceSet",resource.ResourceMetadata.ResourceSystem},
                {"ResourceWorkflow",resource.ResourceMetadata.Workflow},
                {"ResourceWorkflowState",resource.ResourceMetadata  .WorkflowState},
                {"ResourceCreatedUserId", resource.ResourceMetadata.CreationUserId}}).ConfigureAwait(false);

            _eventPublisher.PublishAddResource(resource, _domainInfo.Source, resourceContent:resourceContent);
        }

        public void HandleReplicationEvent(Guid resourceId, string version, string uniqueId,
            DateTimeOffset timestamp, ObjectTypes objectType, ActionTypes actionType, Guid parentResourceId, Guid ruleId, string oldName=null)
        {
            try
            {
                var helper = new ResourceEventsBucketHelperService();
                var bucket = helper.CalculateBucketFromCreationTime(timestamp);

                var resourceEventInternal = new ResourceEventInternal
                {
                    ResourceId = resourceId,
                    Bucket = bucket,
                    CreationTime = Cassandra.TimeUuid.NewId(timestamp),
                    ObjectType = (short)objectType,
                    ActionType = (short)actionType,
                    Version = version,
                    UniqueIdentifier = uniqueId,
                    ParentResourceId = parentResourceId,
                    RuleId = ruleId,
                    OldName = oldName
                };

                _replicationResourceEventHandler.Post(resourceEventInternal);

                var resourceEvent = new ResourceEvent
                {
                    ActionType = actionType,
                    ObjectType = objectType,
                    ResourceId = resourceId,
                    Timestamp = timestamp,
                    Version = version,
                    UniqueIdentifier = uniqueId,
                    ParentResourceId = parentResourceId,
                    RuleId = ruleId,
                    OldName=oldName
                };

                _eventPublisher.PublishReplicationResourceEvent(resourceEvent);
            }
            catch (Exception)
            {
                //_logger.Info("Failed to backup event for resource {1} to ResourceExchange :  {0}", ex.Message, pvEvent.ResourceId);
            }
        }

        //public void PublishRuleUpdated(ReplicationRule rule)
        public void PublishRuleUpdated(TestReplicationRule rule)
        {
            _eventPublisher.PublishRuleUpdated(rule);
        }

        public void PublishResourceHandled(ResourceHandledEvent resourceHandled)
        {
            _eventPublisher.PublishResourceHandled(resourceHandled);
        }

        public void PublishRuleContentsUpdated(UpdatedSeaContents updatedSeaContents, UpdatedSecurityContents securityContents)
        {
            _eventPublisher.PublishRuleContentsUpdated(updatedSeaContents, securityContents);
        }

        public void PublishRuleContentsUpdated(ReplicationRuleUpdated ruleUpdated)
        {
            _eventPublisher.PublishRuleContentsUpdated(ruleUpdated);
        }

        private async Task AddWithMultiParent(ResourceInternal resource, bool skipPermCheck = false, object resourceContent = null)
        {
            //TODO: Domain should handle parentid
            var parentIds = new List<Guid>();
            foreach (var parent in resource.Parents)
            {
                if (parent.Id == Guid.Empty)
                {
                    if (parent.Uri == ".")
                    {
                        parentIds.Add(Global.RootId);
                    }
                    else
                    {
                        var parentId = await GetId(parent.Uri).ConfigureAwait(false);
                        parentIds.Add(parentId);
                    }
                }
                else
                {
                    parentIds.Add(parent.Id);
                }
            }

            if (!skipPermCheck)
            {
                if (!await AclService.PermissionService.HasCreatePermission(parentIds, _clientInfo.UserIdentities, resource.ResourceMetadata.Type).ConfigureAwait(false))
                {
                    throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
                }
            }
            await _resourceService.Add(resource).ConfigureAwait(false);

            var distest = resource.ResourceMetadata.ResourceSystem == "SEA" ? (string.IsNullOrEmpty(resource.ResourceMetadata.Description) ? resource.ResourceMetadata.Uri : resource.ResourceMetadata.Description) : resource.ResourceMetadata.Uri;

            await AclService.ResourceCreateService.Handle(new Dictionary<string, object>{{"ResourceParentIds", parentIds},
                {"DisplayText", distest},
                {"ResourceId",resource.ResourceMetadata.Id},
                {"ResourceName",resource.ResourceMetadata.Name},
                {"ResourceType",resource.ResourceMetadata.Type},
                {"ResourceSet",resource.ResourceMetadata.ResourceSystem},
                {"ResourceWorkflow",resource.ResourceMetadata.Workflow},
                {"ResourceWorkflowState",resource.ResourceMetadata  .WorkflowState},
                {"ResourceCreatedUserId", resource.ResourceMetadata.CreationUserId}}, false).ConfigureAwait(false);

            _eventPublisher.PublishAddResource(resource, _domainInfo.Source, resourceContent:resourceContent);
        }

        public async Task AddWithoutPublish(ResourceInternal resource, bool skipPermCheck = false)
        {
            if (resource.Parents.Count == 1)
            {
                await AddWithoutPublishSingleParent(resource, skipPermCheck).ConfigureAwait(false);
            }
            else
            {
                await AddWithoutPublishMultiParent(resource, skipPermCheck).ConfigureAwait(false);
            }
        }

        private async Task AddWithoutPublishSingleParent(ResourceInternal resource, bool skipPermCheck = false)
        {
            //TODO: Domain should handle parentid
            Guid parentId;
            if (resource.Parents.First().Uri == ".")
            {
                parentId = Global.RootId;
            }
            else
            {
                parentId = await GetId(resource.Parents.First().Uri).ConfigureAwait(false);
            }

            if (!skipPermCheck)
            {
                if (!await AclService.PermissionService.HasCreatePermission(parentId, _clientInfo.UserIdentities, resource.ResourceMetadata.Type).ConfigureAwait(false))
                {
                    throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
                }
            }
            await _resourceService.Add(resource).ConfigureAwait(false);

            var distest = resource.ResourceMetadata.ResourceSystem == "SEA" ? (string.IsNullOrEmpty(resource.ResourceMetadata.Description) ? resource.ResourceMetadata.Uri : resource.ResourceMetadata.Description) : resource.ResourceMetadata.Uri;

            await AclService.ResourceCreateService.Handle(new Dictionary<string, object>{{"ResourceParentId",parentId},
                {"DisplayText", distest},
                {"ResourceId",resource.ResourceMetadata.Id},
                {"ResourceName",resource.ResourceMetadata.Name},
                {"ResourceType",resource.ResourceMetadata.Type},
                {"ResourceSet",resource.ResourceMetadata.ResourceSystem},
                {"ResourceWorkflow",resource.ResourceMetadata.Workflow},
                {"ResourceWorkflowState",resource.ResourceMetadata  .WorkflowState},
                {"ResourceCreatedUserId", resource.ResourceMetadata.CreationUserId}}).ConfigureAwait(false);
        }

        private async Task AddWithoutPublishMultiParent(ResourceInternal resource, bool skipPermCheck = false)
        {
            var parentIds = new List<Guid>();
            foreach (var parent in resource.Parents)
            {
                if (parent.Id != Guid.Empty) continue;
                if (parent.Uri == ".")
                {
                    parentIds.Add(Global.RootId);
                    parent.Id = Global.RootId;
                }
                else
                {
                    var parentId = await GetId(parent.Uri).ConfigureAwait(false);
                    parentIds.Add(parentId);
                    parent.Id = parentId;
                }
            }

            if (!skipPermCheck)
            {
                if (!await AclService.PermissionService.HasCreatePermission(parentIds, _clientInfo.UserIdentities, resource.ResourceMetadata.Type).ConfigureAwait(false))
                {
                    throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
                }
            }
            await _resourceService.Add(resource).ConfigureAwait(false);

            var distest = resource.ResourceMetadata.ResourceSystem == "SEA" ? (string.IsNullOrEmpty(resource.ResourceMetadata.Description) ? resource.ResourceMetadata.Uri : resource.ResourceMetadata.Description) : resource.ResourceMetadata.Uri;

            await AclService.ResourceCreateService.Handle(new Dictionary<string, object>{{"ResourceParentIds", parentIds},
                {"DisplayText", distest},
                {"ResourceId",resource.ResourceMetadata.Id},
                {"ResourceName",resource.ResourceMetadata.Name},
                {"ResourceType",resource.ResourceMetadata.Type},
                {"ResourceSet",resource.ResourceMetadata.ResourceSystem},
                {"ResourceWorkflow",resource.ResourceMetadata.Workflow},
                {"ResourceWorkflowState",resource.ResourceMetadata  .WorkflowState},
                {"ResourceCreatedUserId", resource.ResourceMetadata.CreationUserId}}, false).ConfigureAwait(false);
        }

        public Task AddAuditTrail(ResourceAudit auditEntry)
        {
            return _resourceService.AddAudits(auditEntry);
        }

        public Task AddProperty(Guid resourceId, IDictionary<string, HashSet<PropertyInternal>> properties)
        {
            return _resourceService.AddProperties(resourceId, properties);
        }

        //TODO Handle multiparent for bulk resource add
        public async Task Add(IEnumerable<ResourceInternal> resources, bool skipPermCheck = false, Dictionary<string, object> resourceContentsDict = null)
        {
            if (resources == null)
            {
                return;
            }
            var resourceUriToIdMaps = new Dictionary<string, Guid>();
            if (!skipPermCheck)
            {
                foreach (var resource in resources)
                {
                    if (!resourceUriToIdMaps.ContainsKey(resource.Parents.First().Uri))
                        resourceUriToIdMaps.Add(resource.Parents.First().Uri, await GetId(resource.Parents.First().Uri).ConfigureAwait(false));
                    if (!await AclService.PermissionService.HasCreatePermission(resourceUriToIdMaps[resource.Parents.First().Uri], _clientInfo.UserIdentities, resource.ResourceMetadata.Type).ConfigureAwait(false))
                    {
                        throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
                    }
                }
            }
            await _resourceService.Add(resources)
            .ContinueWith(async _ =>
            {
                var tasks = new List<Task>();
                foreach (var resource in resources)
                {
                    //TODO: Domain should handle parentid

                    Guid parentId;
                    if (resource.Parents.First().Uri == ".")
                    {
                        parentId = Global.RootId;
                    }
                    else
                    {

                        parentId = await GetId(resource.Parents.First().Uri).ConfigureAwait(false);

                    }
                    var distest = resource.ResourceMetadata.ResourceSystem == "SEA" ? (string.IsNullOrEmpty(resource.ResourceMetadata.Description) ? resource.ResourceMetadata.Uri : resource.ResourceMetadata.Description) : resource.ResourceMetadata.Uri;
                    await AclService.ResourceCreateService.Handle(new Dictionary<string, object>{{"ResourceParentId",parentId},
                {"DisplayText", distest},
                {"ResourceId",resource.ResourceMetadata.Id},
                {"ResourceName",resource.ResourceMetadata.Name},
                {"ResourceType",resource.ResourceMetadata.Type},
                {"ResourceSet",resource.ResourceMetadata.ResourceSystem},
                {"ResourceWorkflow",resource.ResourceMetadata.Workflow},
                {"ResourceWorkflowState",resource.ResourceMetadata  .WorkflowState},
                {"ResourceCreatedUserId", resource.ResourceMetadata.CreationUserId}}).ConfigureAwait(false);
                }
                await Task.WhenAll(tasks.ToArray()).ConfigureAwait(false);
            })
            .ContinueWith(_ =>
            {
                foreach (var resource in resources)
                {
                    object resourceContent = null;

                    if (resourceContentsDict != null)
                    {
                        resourceContentsDict.TryGetValue(resource.ResourceMetadata.Uri, out resourceContent);
                    }

                    _eventPublisher.PublishAddResource(resource, _domainInfo.Source, resourceContent: resourceContent);
                }
            }).ConfigureAwait(false);
        }

        public async Task Update(ResourceInternal resource)
        {
            if (!await AclService.PermissionService.HasPermission(resource.ResourceMetadata.Id, _clientInfo.UserIdentities, Permissions.Edit).ConfigureAwait(false))
            {
                throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
            }
            await _resourceService.UpdateResource(resource).ContinueWith(
            t =>
            {               
                if ((t.IsFaulted || t.IsCanceled) && t.Exception != null)
                    throw t.Exception.InnerException;
                else if (t.IsCompleted)
                    _eventPublisher.PublishUpdateResource(resource, _domainInfo.Source);
            }).ConfigureAwait(false);
        }

        public async Task MarkAs(Guid resourceId, bool isDeleted)
        {
            if (!await AclService.PermissionService.HasPermission(resourceId, _clientInfo.UserIdentities, Permissions.Delete).ConfigureAwait(false))
            {
                throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
            }

            var resources = await ReadSelf(resourceId, null, FetchFlags.None, true).AsAsyncList().ConfigureAwait(false);

            if (resources.Any())
            {
                var resource = resources.FirstOrDefault();
                if (resource != null)
                {
                    resource.ResourceMetadata.IsMarkedDeleted = isDeleted;
                    await _resourceService.UpdateResource(resource).ContinueWith(_ =>
                    {
                        _eventPublisher.PublishUpdateResource(resource, _domainInfo.Source);
                    }).ConfigureAwait(false);
                }
            }
        }

        public Task UpdateProperties(ResourceInternal resource)
        {
            return _resourceService.UpdateProperties(resource);
        }

        public Task UpdateLastUpdateDetails(Guid resourceId, DateTimeOffset timeStamp, Guid userId, string userName)
        {
            return _resourceService.PutLastUpdateTimestampAndUserInfo(resourceId, timeStamp, userId, userName);
        }

        public async Task<ResourceInternal> Delete(Guid resourceId)
        {
            if (resourceId == Global.RootId)
            {
                throw new InvalidOperationException("You have no permission to delete this resource");
            }
            if (!await AclService.PermissionService.HasPermission(resourceId, _clientInfo.UserIdentities, Permissions.Delete).ConfigureAwait(false))
            {
                throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
            }
            var resources = await ReadSelf(resourceId).AsAsyncList().ConfigureAwait(false);
            if (resources.Any())
            {
                var firstOrDefault = resources.FirstOrDefault();
                if (firstOrDefault != null)
                {
                    await _resourceService.Delete(firstOrDefault.ResourceMetadata.Uri, resourceId).ConfigureAwait(false);
                    await AclService.DeleteAll(resourceId).ConfigureAwait(false);
                    _eventPublisher.PublishDeleteResource(firstOrDefault, _domainInfo.Source);
                }
                return firstOrDefault;
            }
            return null;
        }

        public Task DeleteProperties(Guid id)
        {
            return _resourceService.DeleteProperties(id);
        }

        public Task DeleteProperties(Guid resourceId, string groupName)
        {
            return _resourceService.DeleteProperties(resourceId, groupName);
        }

        public Task DeleteProperty(Guid resourceId, string groupName, string key)
        {
            return _resourceService.DeleteProperty(resourceId, groupName, key);
        }

        public async Task Delete(string resourceUri)
        {
            if (resourceUri == Global.RootUri)
            {
                throw new InvalidOperationException("You have no permission to delete this resource");
            }
            if (!await AclService.PermissionService.HasPermission(resourceUri, _clientInfo.UserIdentities, Permissions.Delete).ConfigureAwait(false))
            {
                throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
            }
            var resources = await ReadSelf(resourceUri).AsAsyncList().ConfigureAwait(false);
            if (resources.Any())
            {
                var firstOrDefault = resources.FirstOrDefault();
                if (firstOrDefault != null)
                {
                    await _resourceService.Delete(resourceUri, firstOrDefault.ResourceMetadata.Id).ConfigureAwait(false);
                    await AclService.DeleteAll(firstOrDefault.ResourceMetadata.Id).ConfigureAwait(false);
                    _eventPublisher.PublishDeleteResource(firstOrDefault, _domainInfo.Source);
                }
            }
        }

        public async Task<IList<ResourceInternal>> CascadeDelete(Guid resourceId)
        {
            if (resourceId == Global.RootId)
            {
                throw new InvalidOperationException("You have no permission to delete this resource");
            }
            if (!await AclService.PermissionService.HasPermission(resourceId, _clientInfo.UserIdentities, Permissions.Delete).ConfigureAwait(false))
            {
                throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
            }
            var deletedResources = new List<ResourceInternal>();
            var resources = await ReadSelf(resourceId).AsAsyncList().ConfigureAwait(false);
            if (resources.Any())
            {
                var firstOrDefault = resources.FirstOrDefault();
                if (firstOrDefault != null)
                {
                    var resourceMetadataInternals = await _resourceService.CascadeDelete(firstOrDefault.ResourceMetadata.Uri).ConfigureAwait(false);
                    foreach (var resourceMetadataInternal in resourceMetadataInternals)
                    {
                        var resourceInternal = new ResourceInternal
                        {
                            ResourceMetadata = resourceMetadataInternal
                        };
                        _eventPublisher.PublishDeleteResource(resourceInternal, _domainInfo.Source);
                        deletedResources.Add(resourceInternal);
                        await AclService.DeleteAll(resourceMetadataInternal.Id).ConfigureAwait(false);
                    }
                }
            }
            return deletedResources;
        }

        public async Task CascadeDelete(string resourceUri)
        {
            if (resourceUri == Global.RootUri)
            {
                throw new InvalidOperationException("You have no permission to delete this resource");
            }
            if (!await AclService.PermissionService.HasPermission(resourceUri, _clientInfo.UserIdentities, Permissions.Delete).ConfigureAwait(false))
            {
                throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
            }
            var resourceMetadataInternals = await _resourceService.CascadeDelete(resourceUri).ConfigureAwait(false);
            foreach (var resourceMetadataInternal in resourceMetadataInternals)
            {
                var resourceInternal = new ResourceInternal
                {
                    ResourceMetadata = resourceMetadataInternal
                };
                _eventPublisher.PublishDeleteResource(resourceInternal, _domainInfo.Source);
                await AclService.DeleteAll(resourceMetadataInternal.Id).ConfigureAwait(false);
            }
        }

        public async Task<IList<ResourceInternal>> CascadeDeleteWithResult(string resourceUri)
        {
            if (resourceUri == Global.RootUri)
            {
                throw new InvalidOperationException("You have no permission to delete this resource");
            }
            if (!await AclService.PermissionService.HasPermission(resourceUri, _clientInfo.UserIdentities, Permissions.Delete).ConfigureAwait(false))
            {
                throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
            }
            //var result = await ReadDescendants(resourceUri).AsAsyncList().ConfigureAwait(false);
            var resources = await _resourceService.CascadeDelete(resourceUri).ConfigureAwait(false);
            var result = resources.Select(resourceMetadataInternal => new ResourceInternal
            {
                ResourceMetadata = resourceMetadataInternal
            }).ToList();
             var currentResource = resources.Find(x => x.Uri == resourceUri);
            _eventPublisher.PublishDeleteResource(new ResourceInternal { ResourceMetadata = currentResource },
                _domainInfo.Source);
            await AclService.DeleteAll(currentResource.Id).ConfigureAwait(false);
            foreach (var resource in result)
            {
                _eventPublisher.PublishDeleteResource(resource, _domainInfo.Source);
                await AclService.DeleteAll(resource.ResourceMetadata.Id).ConfigureAwait(false);
            }
            return result;
        }

        public async Task<IList<ResourceInternal>> CascadeDeleteWithoutPublish(string resourceUri)
        {
            if (resourceUri == Global.RootUri)
            {
                throw new InvalidOperationException("You have no permission to delete this resource");
            }
            if (!await AclService.PermissionService.HasPermission(resourceUri, _clientInfo.UserIdentities, Permissions.Delete).ConfigureAwait(false))
            {
                throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
            }
            //var result = await ReadDescendants(resourceUri).AsAsyncList().ConfigureAwait(false);
            var resources = await _resourceService.CascadeDelete(resourceUri).ConfigureAwait(false);
            var currentResource = resources.Find(x => x.Uri == resourceUri);
            await AclService.DeleteAll(currentResource.Id).ConfigureAwait(false);
            var result = resources.Select(resourceMetadataInternal => new ResourceInternal
            {
                ResourceMetadata = resourceMetadataInternal
            }).ToList();
            foreach (var res in result)
            {
                await AclService.DeleteAll(res.ResourceMetadata.Id).ConfigureAwait(false);
            }
            return result;
        }

        public async Task<IList<ResourceInternal>> CascadeDeleteWithResult(Guid resourceId)
        {
            if (resourceId == Global.RootId)
            {
                throw new InvalidOperationException("You have no permission to delete this resource");
            }
            if (!await AclService.PermissionService.HasPermission(resourceId, _clientInfo.UserIdentities, Permissions.Delete).ConfigureAwait(false))
            {
                throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
            }
            var result = await ReadDescendants(resourceId).AsAsyncList().ConfigureAwait(false);
            var resources = await ReadSelf(resourceId).AsAsyncList().ConfigureAwait(false);
            var resource = resources.First();
            await _resourceService.CascadeDelete(resource.ResourceMetadata.Uri).ConfigureAwait(false);
            _eventPublisher.PublishDeleteResource(resource, _domainInfo.Source);
            await AclService.DeleteAll(resource.ResourceMetadata.Id).ConfigureAwait(false);
            foreach (var res in result)
            {
                _eventPublisher.PublishDeleteResource(res, _domainInfo.Source);
                await AclService.DeleteAll(res.ResourceMetadata.Id).ConfigureAwait(false);
            }
            return result;
        }

        public IObservable<ResourceInternal> ReadDescendants(string resourceUri, string resourceType = null, FetchFlags fetch = FetchFlags.None, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            return _resourceService.Read(resourceUri, startCd, endCd, _clientInfo.UserIdentities, resourceType, fetch);
        }

        public IObservable<ResourceInternal> ReadDescendants(Guid resourceId, string resourceType = null, FetchFlags fetch = FetchFlags.None, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            return _resourceService.Read(resourceId, startCd, endCd, _clientInfo.UserIdentities, resourceType, fetch);
        }

        [Obsolete("Please do not use this unless absolutely necessary")]
        public IObservable<ResourceInternal> CqlReadDescendantsWithoutPermission(string resourceUri, string resourceType = null, FetchFlags fetch = FetchFlags.None, short cd = 1)
        {
            return _resourceService.CqlReadWithoutPermission(resourceUri,cd, resourceType, fetch);
        }

        [Obsolete("Please do not use this unless absolutely necessary")]
        public IObservable<ResourceInternal> ReadDescendantsWithoutPermission(string resourceUri, string resourceType = null, FetchFlags fetch = FetchFlags.None, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            return _resourceService.ReadWithoutPermission(resourceUri, startCd, endCd, resourceType, fetch);
        }

        [Obsolete("Please do not use this unless absolutely necessary")]
        public IObservable<ResourceInternal> ReadSelfWithoutPermission(string resourceUri, string resourceType = null, FetchFlags fetch = FetchFlags.None)
        {
            return _resourceService.ReadWithoutPermission(resourceUri, resourceType, fetch);
        }

        [Obsolete("Please do not use this unless absolutely necessary")]
        public IObservable<ResourceInternal> ReadSimpleResourceWithoutPermission(string resourceUri)
        {
            return _resourceService.ReadSimpleWithoutPermission(resourceUri);
        }

        [Obsolete("Please do not use this unless absolutely necessary")]
        public IObservable<ResourceInternal> ReadSimpleResource(string resourceUri)
        {
            return _resourceService.ReadSimple(resourceUri, _clientInfo.UserIdentities);
        }

        [Obsolete("Please do not use this unless absolutely necessary")]
        public ResourceInternal CqlReadSimpleResourceWithoutPermission(string resourceUri)
        {
            return _resourceService.CqlReadWithoutPermission(resourceUri, 0).ToEnumerable().FirstOrDefault();
        }

        [Obsolete("Please do not use this unless absolutely necessary")]
        public async Task<ResourceInternal> CqlReadSimpleResource(string resourceUri)
        {
            var resource = _resourceService.CqlReadWithoutPermission(resourceUri, 0).ToEnumerable().FirstOrDefault();
            if (resource == null) return null;
            if (!await AclService.PermissionService.HasPermission(resource.ResourceMetadata.Id, _clientInfo.UserIdentities, Permissions.Read).ConfigureAwait(false))
                return null;

            return resource;
        }

        [Obsolete("Please do not use this unless absolutely necessary")]
        public IObservable<ResourceInternal> ReadSimpleDescendants(string resourceUri, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            return _resourceService.ReadSimple(resourceUri, startCd, endCd, _clientInfo.UserIdentities, resourceType);
        }

        [Obsolete("Please do not use this unless absolutely necessary")]
        public IObservable<ResourceInternal> ReadSimpleDescendantsWithoutPermission(string resourceUri, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            return _resourceService.ReadSimpleWithoutPermission(resourceUri, startCd, endCd, resourceType);
        }

        public IObservable<ResourceInternal> ReadSelf(string resourceUri, string resourceType = null, FetchFlags fetch = FetchFlags.None)
        {
            return _resourceService.Read(resourceUri, _clientInfo.UserIdentities, resourceType, fetch);
        }

        public IObservable<ResourceInternal> ReadSelf(Guid resourceId, string resourceType = null, FetchFlags fetch = FetchFlags.None, bool includeDeleted = false)
        {
            return _resourceService.Read(resourceId, _clientInfo.UserIdentities, resourceType, fetch, includeDeleted);
        }

        public IObservable<ResourceInternal> ReadInResources(Guid resourceId, short startCd = short.MinValue, short endCd = -1, FetchFlags fetch = FetchFlags.None, params Condition[] conditions)
        {
            return _resourceService.ReadIn(resourceId, _clientInfo.UserIdentities, startCd, endCd, fetch, conditions);
        }

        //public async Task<PagedResource> ReadPagedDescendants(string resourceUri, string resourceType = null, short startCd = 1, short endCd = 1, FetchFlags fetch = FetchFlags.None, QueryOptions query = null)
        //{
        //    //var testType = "Set";
        //    //var testUri = "./sea/set(aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa)";
        //    //var testFlag = FetchFlags.Properties;
        //    //var testQueryOption = new QueryOptions
        //    //{
        //    //    PageNumber = 1, PageSize = 10, SortBy = SortBy.Name, SortOrder = SortOrder.Ascending,
        //    //    Filters = new Condition[]
        //    //    {
        //    //        new AndCondition<MetadataCondition>
        //    //        {
        //    //            Conditions = new[]
        //    //            {
        //    //                new MetadataCondition(MetadataCondition.Keys.IsSystem, bool.FalseString),
        //    //                new MetadataCondition(MetadataCondition.Keys.IsHidden, bool.FalseString)
        //    //            }
        //    //        },
        //    //        new NotCondition<MetadataCondition>(new MetadataCondition(MetadataCondition.Keys.Type, "Element")),
        //    //        new PropetyCondition("SetFields","Attachments","List"), 
        //    //    }
        //    //};

        //    // paging need to be handled properly
        //    var pagedResult = await _resourceService.ReadPaged(resourceUri, _clientInfo.UserIdentities, Permissions.Read, resourceType, startCd, endCd, fetch, query).AsAsyncList().ConfigureAwait(false);
        //    var pagedResource = new PagedResource
        //    {
        //        Data = pagedResult.ToList(),
        //        Paging = new PagingInfo()
        //    };
        //    return pagedResource;
        //}

        public Task<PagedResult<ResourceInternal>> ReadDescendantsPaged(string resourceUri, string resourceType = null, short startCd = 1, short endCd = 1, FetchFlags fetch = FetchFlags.None, QueryOptions query = null, bool skipAclCheck = false)
        {
            return _resourceService.ReadDescendantsPaged(resourceUri, _clientInfo.UserIdentities, Permissions.Read, resourceType, startCd, endCd, fetch, query, skipAclCheck);
        }

        public Task<PagedResult<ResourceInternal>> ReadDescendantsPaged(string[] resourceUris, string resourceType = null, short startCd = 1, short endCd = 1, FetchFlags fetch = FetchFlags.None, QueryOptions query = null, bool skipAclCheck = false)
        {
            return _resourceService.ReadDescendantsPaged(resourceUris, _clientInfo.UserIdentities, Permissions.Read, resourceType, startCd, endCd, fetch, query, skipAclCheck);
        }

        public Task<PagedResult<ResourceInternal>> ReadRecentDescendantsPaged(string resourceUri, int noOfDays, string resourceType = null, short startCd = 1, short endCd = 1, FetchFlags fetch = FetchFlags.None, QueryOptions query = null)
        {
            return _resourceService.ReadRecentDescendantsPaged(resourceUri, _clientInfo.UserIdentities, noOfDays, Permissions.Read, resourceType, startCd, endCd, fetch, query);
        }

        public Task<PagedResult<ResourceInternal>> SearchDescendantsPagedWithProperties(string resourceUri,
          string resourceType = null, short startCd = 1, short endCd = 1, FetchFlags fetch = FetchFlags.None,
          QueryOptions query = null, List<Condition> metadataConditions = null, List<Condition> propertyConditions = null)
        {
            return _resourceService.SearchDescendantsPagedWithProperties(resourceUri, _clientInfo.UserIdentities, Permissions.Read, resourceType, startCd, endCd, fetch, query, metadataConditions, propertyConditions);
        }

        public Task<PagedResult<ResourceInternal>> SearchRecentDescendantsPagedWithProperties(string resourceUri, int noOfDays,
          string resourceType = null, short startCd = 1, short endCd = 1, FetchFlags fetch = FetchFlags.None,
          QueryOptions query = null, List<Condition> metadataConditions = null, List<Condition> propertyConditions = null)
        {
            return _resourceService.SearchRecentDescendantsPagedWithProperties(resourceUri, _clientInfo.UserIdentities, noOfDays, Permissions.Read, resourceType, startCd, endCd, fetch, query, metadataConditions, propertyConditions);
        }

        public Task<PagedResult<ResourceAudit>> ReadAuditPaged(string resourceUri, QueryOptions query, short startCd = 1, short endCd = 1)
        {
            return _resourceService.ReadAuditsPaged(resourceUri, _clientInfo.UserIdentities, Permissions.Read, startCd, endCd, query);
        }

        public async Task<string> GetResourceUriById(Guid resourceId)
        {
            var resources = await ReadSelf(resourceId).AsAsyncList().ConfigureAwait(false);

            if (!resources.Any()) return string.Empty;

            return resources.First().ResourceMetadata.Uri;
        }        
        
        public async Task<ResourceInternal> GetCql(Guid id, FetchFlags fetch = FetchFlags.None)
        {
            var metadata = await _resourceService.GetMetadata(id).ConfigureAwait(false);

            if (!await AclService.PermissionService.HasPermission(metadata.Id, _clientInfo.UserIdentities,
                Permissions.Read).ConfigureAwait(false))
                throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");

            var resource = await _resourceService.FetchAsync(metadata, fetch).ConfigureAwait(false);
            return resource;
        }

        public async Task<ResourceInternal> GetCql(string uri, FetchFlags fetch = FetchFlags.None)
        {
            var id = await _resourceService.GetId(uri).ConfigureAwait(false);
            var metadata = await _resourceService.GetMetadata(id).ConfigureAwait(false);

            if (!await AclService.PermissionService.HasPermission(metadata.Id, _clientInfo.UserIdentities,
                Permissions.Read).ConfigureAwait(false))
                throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");

            var resource = await _resourceService.FetchAsync(metadata, fetch).ConfigureAwait(false);
            return resource;
        }


        public Task AddReplicationEvent(ReplicationEventInternal replicationEvent)
        {
            return _resourceService.AddReplicationEvent(replicationEvent);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, List<sbyte> status, DateTimeOffset lastTimestamp, int limit)
        {
            return _resourceService.ReadReplicationEvents(ruleId, status, lastTimestamp, limit);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, List<sbyte> status, params string[] resourceType)
        {
            return _resourceService.ReadReplicationEvents(ruleId, status, resourceType);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, List<sbyte> status, string resourceName, params string[] resourceType)
        {
            return _resourceService.ReadReplicationEvents(ruleId, status, resourceName, resourceType);
        }

        public IObservable<ResourceEventInternal> GetResourceEvents(DateTimeOffset fromDt, DateTimeOffset toDt)
        {
            return _resourceService.GetResourceEvents(fromDt, toDt);
        }
        
        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, sbyte status)
        {
            return _resourceService.ReadReplicationEvents(ruleId, status);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, List<sbyte> status)
        {
            return _resourceService.ReadReplicationEvents(ruleId, status);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, sbyte status, int limit)
        {
            return _resourceService.ReadReplicationEvents(ruleId, status, limit);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, sbyte status, DateTimeOffset since, int limit)
        {
            return _resourceService.ReadReplicationEvents(ruleId, status, since, limit);
        }

        public Task<ResourceAudit> GetAudit(Guid resourceId, Guid auditId)
        {
            return _resourceService.GetAudit(resourceId, auditId);
        }
    }
}