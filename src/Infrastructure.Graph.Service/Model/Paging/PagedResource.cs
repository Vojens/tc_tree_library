using System.Collections.Generic;
using Infrastructure.Graph.Model;

namespace Infrastructure.Graph.Service.Model.Paging
{
    public class PagedResource
    {
        public PagingInfo Paging { get; set; }
        public List<ResourceInternal> Data { get; set; }
    }
}