﻿using System;
using System.Collections.Generic;
using .Datatypes.Object;
using Infrastructure.Common.Extensions;
using Infrastructure.Graph.Model;
using Model.Extensions;
using .Serialization.Messages;
using .WITSML.Datatypes;
using ResourceMetadata = Infrastructure.Graph.Model.ResourceMetadataInternal;

namespace Infrastructure.Graph.Service.Extensions
{
    public static class ResourceExtensions
    {
        //private static readonly AvroBinarySerializer Serializer = new AvroBinarySerializer();

        public static Resource ToResource(this ResourceInternal resource)
        {
            //var data = Serialize(resource.ResourceMetadata);
            string contentEncoding = string.Empty;

            byte[] data = new byte[] { };
            if (resource.Data != null)
            {
                data = resource.Data.Content;

                if (resource.Data.ContentEncoding != null)
                    contentEncoding = resource.Data.ContentEncoding;
            }

            return resource.ToResource(data, contentEncoding);
        }

        public static Resource ToResource(this ResourceInternal resource, object content, string contentEncoding = "")
        {
            //var data = Serialize(resource.ResourceMetadata);

            var lastChanged = resource.ResourceMetadata.LastUpdateTimestamp;
            byte[] data = new byte[] { };
            if (resource.Data != null)
            {
                if (resource.Data.ModifyTimestamp > lastChanged)
                {
                    lastChanged = resource.Data.ModifyTimestamp;
                }

                data = (byte[]) content;
            }

            if (lastChanged.IsNullOrDefault())
            {
                lastChanged = DateTimeOffset.UtcNow;
            }

            var plinkResource = new Resource
            {
                ContentEncoding = contentEncoding,
                ContentType = "avro/binary",
                Data = data,
                Linkrefs = new Dictionary<string, string>(),
                ResourceMetadata = new Datatypes.Object.ResourceMetadata
                {
                    ChannelSubscribable = true,
                    CustomData = new Dictionary<string, string>(),
                    HasChildren = 0,
                    LastChanged = lastChanged.ToDateTime(),
                    Name = resource.ResourceMetadata.Name,
                    Namespace = "",
                    ObjectType = GetObjectType(resource.ResourceMetadata),
                    ResourceType = resource.ResourceMetadata.Type,
                    Uri = resource.ResourceMetadata.Uri,
                    Uuid = resource.ResourceMetadata.Id.ToString(),
                    Version = "1.4.1.1",
                    CreationUserId = resource.ResourceMetadata.CreationUserId.ToString(),
                    CreationUserName = resource.ResourceMetadata.CreationUserName,
                    CreationTime = resource.ResourceMetadata.CreationTimestamp.ToDateTime(),
                    LastUpdateUserId = resource.ResourceMetadata.LastUpdateUserId.ToString(),
                    LastUpdateUserName = resource.ResourceMetadata.LastUpdateUserName,
                    LastUpdateTime = resource.ResourceMetadata.LastUpdateTimestamp.ToDateTime(),
                    IsMarkedDeleted = resource.ResourceMetadata.IsMarkedDeleted
                }
            };

            plinkResource.ResourceMetadata.CustomData.Add("ReplicationRuleId", resource.ReplicationRuleId.ToString());
            plinkResource.ResourceMetadata.CustomData.Add("TriggeredBy", resource.ResourceMetadata.CreationUserId.ToString());
            return plinkResource;
        }

        private static Tuple<string, byte[]> Serialize(ResourceMetadata resourceMetadata)
        {
            // if channel then serialize the channel metadata and attach 
            // todo This is for spark processing

            if (IsChannelResource(resourceMetadata))
            {
                // TODO: need to revisit

                //bool isVirtualLog = resource.ChannelMetadata.ChannelType == ChronosChannelType.Alias ||
                //                     resource.ChannelMetadata.ChannelType == ChronosChannelType.Synthetic;

                //return Tuple.Create(string.Empty,
                //    (byte[])Serializer.Serialize(resource.ChannelMetadata.ToTrajectoryUri().ToChannelMetadata(isVirtualLog)));
            }

            return Tuple.Create(string.Empty, new byte[0]);
        }

        private static bool IsChannelResource(ResourceMetadata resourceMetadata)
        {
            return resourceMetadata.Type == "channel" || resourceMetadata.Type == "trajectorychannel";
        }

        private static string GetObjectType(this ResourceMetadata resourceMetadata)
        {
            return resourceMetadata.Type == "channel" ? typeof(ChannelMetadata).FullName : string.Empty;
        }
    }
}
