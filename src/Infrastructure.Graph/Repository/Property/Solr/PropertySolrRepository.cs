﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Property.Solr.Cql;
using Infrastructure.Common;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Repository.Property.Solr
{
    internal class PropertySolrRepository : SolrRepository
    {
        internal PropertySolrCqlRepository CqlRepository;
        public PropertySolrRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new PropertySolrCqlRepository(ConnectionProvider, SchemaContainer);
        }

        public IObservable<PropertySolrEntry> Read(Guid id)
        {
            return CqlRepository.Read(id).Read(ConnectionProvider, Global.SolrConsistencyLevel);
        }

        public IObservable<PropertySolrEntry> Read(Guid id, string groupName)
        {
            return CqlRepository.Read(id, groupName).Read(ConnectionProvider, Global.SolrConsistencyLevel);
        }

        public async Task<PropertySolrEntry> Read(Guid id, string groupName, string key)
        {
            var cqlQuerySingleElement = CqlRepository.Read(id, groupName, key);
            cqlQuerySingleElement.Configure(StatementOptions.Select, ConnectionProvider);
            cqlQuerySingleElement.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var propertySolrEntry = await cqlQuerySingleElement.ExecuteAsync().ConfigureAwait(false);
            return propertySolrEntry;
        }

        public IObservable<PropertySolrEntry> Read(List<Guid> ids)
        {
            return CqlRepository.Read(ids).Read(ConnectionProvider, Global.SolrConsistencyLevel);
        }

        public async Task<bool> TryGet(Condition condition, Ref<PropertySolrEntry> @ref)
        {
            var cqlQuerySingleElement = CqlRepository.Get(condition);
            cqlQuerySingleElement.Configure(StatementOptions.Select, ConnectionProvider);
            cqlQuerySingleElement.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var resourceMetadataSolrEntry = await cqlQuerySingleElement.ExecuteAsync().ConfigureAwait(false);
            if (resourceMetadataSolrEntry.IsNullOrDefault())
            {
                return false;
            }
            @ref.Value = resourceMetadataSolrEntry;
            return true;
        }
        
        public CqlQuery<PropertySolrEntry> Search(Condition condition)
        {
            var query = condition.BuidQuery();
            var solrQuery = QueryBuilder.Build(query, new SolrQueryOptions());
            var propertySolrEntry = SchemaContainer.GetTable<PropertySolrEntry>().Where(x => x.SolrQuery == solrQuery);
            return propertySolrEntry;
        }
    }
}
