﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Mapping;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Repository.Property.Solr.Cql
{
    internal class PropertySolrCqlRepository : SolrRepository
    {
        private readonly SchemaMap<PropertySolrEntry> _propertySolrEntryDefinition;
        private readonly SchemaMap<ResourceMetadataSolrEntry> _resourceMetadataSolrEntry;
        private readonly SchemaMap<AscendantsByResourceSolrEntry> _ascendantsResourceSolrEntry;

        public PropertySolrCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            _propertySolrEntryDefinition = (SchemaMap<PropertySolrEntry>)schemaContainer.GetDefinition<PropertySolrEntry>();
            _resourceMetadataSolrEntry = (SchemaMap<ResourceMetadataSolrEntry>)schemaContainer.GetDefinition<ResourceMetadataSolrEntry>();
            _ascendantsResourceSolrEntry = (SchemaMap<AscendantsByResourceSolrEntry>)schemaContainer.GetDefinition<AscendantsByResourceSolrEntry>();
        }

        public CqlQuery<PropertySolrEntry> Read(Guid id)
        {
            var key = _propertySolrEntryDefinition.GetColumnName(x => x.Key);
            var name = _propertySolrEntryDefinition.GetColumnName(x => x.Name);
            var outId = _propertySolrEntryDefinition.GetColumnName(x => x.OutId);

            var query = new SolrQueryByField(outId, id.ToString());
            var queryOptions = new SolrQueryOptions { OrderBy = new[] { new SortOrder(name, Order.ASC), new SortOrder(key, Order.ASC) } };
            var solrQuery = QueryBuilder.Build(query, queryOptions);

            return SchemaContainer.GetTable<PropertySolrEntry>().Where(x => x.SolrQuery == solrQuery);
        }

        public CqlQuery<PropertySolrEntry> Read(Guid id, string groupName)
        {
            var key = _propertySolrEntryDefinition.GetColumnName(x => x.Key);
            var name = _propertySolrEntryDefinition.GetColumnName(x => x.Name);
            var outId = _propertySolrEntryDefinition.GetColumnName(x => x.OutId);

            AbstractSolrQuery query = new SolrQueryByField(outId, id.ToString());
            query = query && new SolrQueryByField(name, groupName);
            var queryOptions = new SolrQueryOptions { OrderBy = new[] { new SortOrder(name, Order.ASC), new SortOrder(key, Order.ASC) } };
            var solrQuery = QueryBuilder.Build(query, queryOptions);

            return SchemaContainer.GetTable<PropertySolrEntry>().Where(x => x.SolrQuery == solrQuery);
        }

        public CqlQuerySingleElement<PropertySolrEntry> Read(Guid id, string groupName, string key)
        {
            var keyCol = _propertySolrEntryDefinition.GetColumnName(x => x.Key);
            var name = _propertySolrEntryDefinition.GetColumnName(x => x.Name);
            var outId = _propertySolrEntryDefinition.GetColumnName(x => x.OutId);

            AbstractSolrQuery query = new SolrQueryByField(outId, id.ToString());
            query = query && new SolrQueryByField(name, groupName);
            query = query && new SolrQueryByField(keyCol, key);
            var queryOptions = new SolrQueryOptions { OrderBy = new[] { new SortOrder(name, Order.ASC), new SortOrder(keyCol, Order.ASC) } };
            var solrQuery = QueryBuilder.Build(query, queryOptions);

            return SchemaContainer.GetTable<PropertySolrEntry>().Where(x => x.SolrQuery == solrQuery).FirstOrDefault();
        }

        public CqlQuery<PropertySolrEntry> Read(List<Guid> ids)
        {
            var metaType = _resourceMetadataSolrEntry.GetColumnName(x => x.Type);

            var propKey = _propertySolrEntryDefinition.GetColumnName(x => x.Key);
            var propName = _propertySolrEntryDefinition.GetColumnName(x => x.Name);
            var propOutId = _propertySolrEntryDefinition.GetColumnName(x => x.OutId);


            var ascResUri = _ascendantsResourceSolrEntry.GetColumnName(x => x.AscendantUri);
            var resCd = _ascendantsResourceSolrEntry.GetColumnName(x => x.CurveDistance);

            var query = new SolrQueryInList(propOutId, ids.Select(x => x.ToString()));
            var queryOptions = new SolrQueryOptions { OrderBy = new[] { new SortOrder(propName, Order.ASC), new SortOrder(propKey, Order.ASC) } };
            var solrQuery = QueryBuilder.Build(query, queryOptions);

            return SchemaContainer.GetTable<PropertySolrEntry>().Where(x => x.SolrQuery == solrQuery);
        }

        public CqlQuerySingleElement<PropertySolrEntry> Get(Condition condition)
        {
            var query = condition.BuidQuery();
            var queryOptions = new SolrQueryOptions();
            var solrQuery = QueryBuilder.Build(query, queryOptions);
            return SchemaContainer.GetTable<PropertySolrEntry>().Where(x => x.SolrQuery == solrQuery).FirstOrDefault();
        }
    }
}
