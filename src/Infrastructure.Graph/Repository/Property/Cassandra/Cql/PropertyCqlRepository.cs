﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Cassandra.Data.Linq;
using Cassandra.Mapping;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Repository.Property.Cassandra.Cql
{
    internal class PropertyCqlRepository : CassandraRepository
    {
        public PropertyCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
        }

        public CqlInsert<PropertyEntry> Add(PropertyEntry entry)
        {
            return SchemaContainer.GetTable<PropertyEntry>().Insert(entry, false);
        }

        public ICqlBatch Add(IEnumerable<PropertyEntry> entries)
        {
            var cqlBatch = DbMapper.CreateBatch();
            foreach (var propertyEntry in entries)
            {
                cqlBatch.Insert(propertyEntry, false);  //TODO specify cql query 
                //cqlBatch.Execute(Add(propertyEntry));
            }
            return cqlBatch;
        }

        public CqlQuery<PropertyEntry> Read(Guid id)
        {
            return SchemaContainer.GetTable<PropertyEntry>().Where(c => c.OutId == id && c.InId == id);
        }

        //public CqlQuery<PropertySolrEntry> Read(List<Guid> outIds)
        //{
        //    var solrQuery = string.Empty;
        //    for (var i = 0; i < outIds.Count; i++)
        //    {
        //        solrQuery = solrQuery + string.Format(@" outid: {0}", outIds[i]);
        //    }
        //    return SchemaContainer.GetTable<PropertySolrEntry>().Where(x => x.SolrQuery == solrQuery);
        //}

        public CqlQuery<PropertyEntry> Read(Guid id, string groupName)
        {
            return SchemaContainer.GetTable<PropertyEntry>().Where(c => c.OutId == id && c.InId == id && c.Name == groupName);
        }

//        public CqlQuery<PropertyEntry> Read(Guid id, IOrderedEnumerable<Tuple<string, string>> groupKeys)
//        {
//            var propertyEntries = SchemaContainer.GetTable<PropertyEntry>().Where(c => c.OutId == id && c.InId == id && groupKeys.Any(x => x.Item1 == c.Name) && );
//        }

        public CqlQuery<PropertyEntry> Get(Guid id, string groupName, string key)
        {
            return SchemaContainer.GetTable<PropertyEntry>().Where(c => c.OutId == id && c.InId == id && c.Name == groupName && c.Key == key);
        }

        public CqlDelete Delete(Guid id)
        {
            return SchemaContainer.GetTable<PropertyEntry>().Where(c => c.OutId == id && c.InId == id).Delete();
        }

        public CqlDelete Delete(Guid id, string groupName)
        {
            return SchemaContainer.GetTable<PropertyEntry>().Where(c => c.OutId == id && c.InId == id && c.Name == groupName).Delete();
        }

        public CqlDelete Delete(Guid id, string groupName, string key)
        {
            return SchemaContainer.GetTable<PropertyEntry>().Where(c => c.OutId == id && c.InId == id && c.Name == groupName && c.Key == key).Delete();
        }

        public CqlDelete DeleteIfExists(Guid id, string groupName, string key)
        {
            return Delete(id, groupName, key).IfExists();
        }

        public CqlUpdate Update(Guid id, Expression<Func<PropertyEntry, bool>> selector)
        {
            return SchemaContainer.GetTable<PropertyEntry>()
                    .Where(c => c.OutId == id && c.InId == id).Select(c => selector)
                .Update();
        }
    }
}
