﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using Cassandra.Mapping;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Common.Extensions;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Property.Cassandra.Cql;
using CqlQueryExtensions = Infrastructure.ConnectionProvider.Extensions.CqlQueryExtensions;

namespace Infrastructure.Graph.Repository.Property.Cassandra
{
    internal class PropertyCassandraRepository : CassandraRepository
    {
        internal PropertyCqlRepository CqlRepository;
        public PropertyCassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new PropertyCqlRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(PropertyEntry entry)
        {
            return CqlRepository.Add(entry).ConfigureAndExecuteAsync(ConnectionProvider);
        }

        public Task Add(IEnumerable<PropertyEntry> entries)
        {
            return DbMapper.ExecuteAsync(CqlRepository.Add(entries));
        }

        public IObservable<PropertyEntry> Read(Guid id)
        {
            return Observable.Create<PropertyEntry>(async o =>
            {
                try
                {
                    var enumerable = await DbMapper.FetchAsync<PropertyEntry>("WHERE outid = ? AND inid = ?", id, id).ConfigureAwait(false);

                    foreach (var entry in enumerable)
                    {
                        o.OnNext(entry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        //public IObservable<PropertySolrEntry> Read(List<Guid> outIds)
        //{
        //    return CqlQueryExtensions.Read(CqlRepository.Read(outIds), ConnectionProvider);
        //}

        public IObservable<PropertyEntry> Read(Guid id, string groupName)
        {
            return Observable.Create<PropertyEntry>(async o =>
            {
                try
                {
                    var enumerable = await DbMapper.FetchAsync<PropertyEntry>("WHERE outid = ? AND inid = ? AND nm = ?", id, id, groupName).ConfigureAwait(false);

                    foreach (var entry in enumerable)
                    {
                        o.OnNext(entry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

//        public IObservable<PropertyEntry> Read(Guid id, IOrderedEnumerable<Tuple<string, string>> groupKeys)
//        {
//            
//        }

        public async Task<PropertyEntry> Get(Guid id, string groupName, string key)
        {
            var propertyEntry = await DbMapper.FirstOrDefaultAsync<PropertyEntry>("WHERE outid = ? AND inid = ? AND nm = ? AND key = ?", id, id, groupName, key).ConfigureAwait(false);
            if (propertyEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("property entry with id {0}, groupName {1} and key{2} is not found", id, groupName, key));
            }
            return propertyEntry;
        }

        public Task Delete(Guid id)
        {
            return DbMapper.DeleteAsync<PropertyEntry>("WHERE outid = ? AND inid = ?", id, id);
        }

        public Task Delete(Guid id, string groupName)
        {
            return DbMapper.DeleteAsync<PropertyEntry>("WHERE outid = ? AND inid = ? AND nm = ?", id, id, groupName);
        }

        public Task Delete(Guid id, string groupName, string key)
        {
            return DbMapper.DeleteAsync<PropertyEntry>("WHERE outid = ? AND inid = ? AND nm = ? AND key = ?", id, id, groupName, key);
        }

        public Task<AppliedInfo<PropertyEntry>> DeleteIfExists(Guid id, string groupName, string key)
        {
            //            var propertyEntries = SchemaContainer.GetTable<PropertyEntry>()
            //                .Where(c => c.OutId == id && c.InId == id && c.Name == groupName && c.Key == key);
            return
                DbMapper.DeleteIfAsync<PropertyEntry>(
                    global::Cassandra.Mapping.Cql.New("WHERE outid = ? AND inid = ? AND nm = ? AND key = ?", id, id,
                        groupName, key));
            //            var rowSet = await CqlRepository.DeleteIfExists(id, groupName, key).ExecuteAsync();
            //            rowSet.
            //            return CqlRepository.DeleteIfExists(id, groupName, key).ConfigureAndExecuteAsync(ConnectionProvider);
        }

        public Task Update(Guid id, Expression<Func<PropertyEntry, bool>> selector)
        {
            return CqlRepository.Update(id, selector).Configure(StatementOptions.Upsert, ConnectionProvider).ExecuteAsync();
        }
    }
}
