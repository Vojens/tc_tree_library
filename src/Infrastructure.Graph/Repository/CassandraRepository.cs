using Cassandra.Mapping;
using Infrastructure.ConnectionProvider;
using Infrastructure.Database.Cassandra.Manager.Mapping;

namespace Infrastructure.Graph.Repository
{
    internal abstract class CassandraRepository
    {
        protected IConnectionProvider ConnectionProvider { get; set; }
        private readonly IMapper _dbMapper;
        private readonly SchemaContainer _schemaContainer;
        protected internal IMapper DbMapper
        {
            get { return _dbMapper; }
        }

        protected internal SchemaContainer SchemaContainer
        {
            get { return _schemaContainer; }
        }
        protected CassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
        {
            ConnectionProvider = connectionProvider;
            _schemaContainer = schemaContainer;
            _dbMapper = new MapperAdapter(ConnectionProvider, _schemaContainer.MappingConfiguration);

        }
    }
}