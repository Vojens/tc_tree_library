using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Mapping;
using Infrastructure.Graph.Solr;

namespace Infrastructure.Graph.Repository
{
    internal abstract class SolrRepository : CassandraRepository
    {
        private static readonly ISolrQueryBuilder SolrQueryBuilder = new SolrQueryBuilder();


        protected ISolrQueryBuilder QueryBuilder
        {
            get { return SolrQueryBuilder; }

        }
        protected SolrRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            KeySpace = ConnectionProvider.Session.Keyspace;
        }

        protected ISchemaDefinition Definition<TPoco>()
        {
            return SchemaContainer.GetDefinition<TPoco>();
        }

        protected string KeySpace { get; private set; }
    }
}