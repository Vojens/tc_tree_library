﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Common.Extensions;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Audit.Cassandra.Cql;
using Infrastructure.Graph.Repository.ResourceExchange.Cassandra.Cql;
using System.Reactive.Linq;
using System.Reactive.Disposables;
using Infrastructure.Graph.Service.Core.Helper;
using System.Linq;
using System.Collections;
using Cassandra;

namespace Infrastructure.Graph.Repository.ResourceExchange.Cassandra
{
    internal class ResourceEventCassandraRepository : CassandraRepository
    {
        protected ResourceEventCqlRepository CqlRepository;
        public ResourceEventCassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new ResourceEventCqlRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(ResourceEventEntry entry)
        {
            return CqlRepository.Add(entry).ConfigureAndExecuteAsync(ConnectionProvider);
        }

        public async Task<ResourceEventEntry> Get(Guid resourceId,DateTimeOffset bucket)
        {
            var resourceExchangeEntry = await DbMapper.FirstOrDefaultAsync<ResourceEventEntry>("WHERE resid = ? AND bucket = ?", resourceId, bucket).ConfigureAwait(false);
            if (resourceExchangeEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("resoure exchange data with id {0} is not found", resourceId));
            }
            return resourceExchangeEntry;
        }

        public IObservable<ResourceEventViewEntry> Get(DateTimeOffset fromTime, DateTimeOffset toTime)
        {
            return Observable.Create<ResourceEventViewEntry>(async o =>
            {
                try
                {
                    ResourceEventsBucketHelperService helper = new ResourceEventsBucketHelperService();
                    var bucket = helper.CalculateBucketFromCreationTime(fromTime);

                    do
                    {
                       var fromUUID = TimeUuid.NewId(fromTime);
                       var toUUID = TimeUuid.NewId(toTime);

                       var resourceEventViewEntries = await DbMapper.FetchAsync<ResourceEventViewEntry>("WHERE bucket = ? AND ctime > ? AND ctime < ?", bucket, fromUUID, toUUID).ConfigureAwait(false);
                       foreach (var resEvent in resourceEventViewEntries)
                       {
                           o.OnNext(resEvent);
                       }

                       bucket = helper.GetNextBucket(bucket);

                    } while (bucket < toTime);

                    
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }
     }
}
