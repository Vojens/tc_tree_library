﻿using System;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;
using Cassandra;

namespace Infrastructure.Graph.Repository.ResourceExchange.Cassandra.Cql
{
    internal class ResourceEventCqlRepository : CassandraRepository
    {
        public ResourceEventCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
        }

        public CqlInsert<ResourceEventEntry> Add(ResourceEventEntry entry)
        {
            return SchemaContainer.GetTable<ResourceEventEntry>().Insert(entry, false);
        }

        public CqlQuerySingleElement<ResourceEventEntry> Get(Guid resourceId, DateTimeOffset bucket)
        {
            return SchemaContainer.GetTable<ResourceEventEntry>().FirstOrDefault(c => c.ResourceId == resourceId && c.Bucket == bucket);
        }

        public CqlQuery<ResourceEventViewEntry> Read(DateTimeOffset bucket, DateTimeOffset fromTime, DateTimeOffset toTime)
        {
            var fromUUID = TimeUuid.NewId(fromTime);
            var toUUID = TimeUuid.NewId(toTime);
            return
                SchemaContainer.GetTable<ResourceEventViewEntry>()
                    .Where(c => c.Bucket == bucket && c.CreationTime.CompareTo(fromUUID) > 0 && c.CreationTime.CompareTo(toUUID) < 0);
        }
    }
}
