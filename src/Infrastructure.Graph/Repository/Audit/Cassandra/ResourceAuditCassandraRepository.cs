﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Cassandra;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Audit.Cassandra.Cql;
using Infrastructure.Graph.Repository.Metadata.Cassandra.Cql;

namespace Infrastructure.Graph.Repository.Audit.Cassandra
{
    internal class ResourceAuditCassandraRepository : CassandraRepository
    {
        protected ResourceAudtiCqlRepository CqlRepository;
        public ResourceAuditCassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new ResourceAudtiCqlRepository(ConnectionProvider, SchemaContainer);
        }
                
        public Task Add(ResourceAuditEntry entry)
        {
            var oldData = entry.OldData;
            var newData = entry.NewData;

            entry.OldData = string.Empty;
            entry.NewData = string.Empty;

            var tasks = new List<Task>();
            var task1 = CqlRepository.Add(entry).ConfigureAndExecuteAsync(ConnectionProvider);
            tasks.Add(task1);
            if (string.IsNullOrEmpty(oldData) == false || string.IsNullOrEmpty(newData) == false)
            {
                var task2 = CqlRepository.Add(new ResourceAuditDataEntry
                {
                    Id = entry.Id,
                    NewData = newData,
                    OldData = oldData
                }).ConfigureAndExecuteAsync(ConnectionProvider);
                tasks.Add(task2);
            }

            return Task.WhenAll(tasks.ToArray());
        }

        public async Task<ResourceAuditEntry> Get(Guid resourceId, Guid id)
        {
            var auditEntry = await DbMapper.FirstOrDefaultAsync<ResourceAuditEntry>("WHERE outid = ? AND inid = ? AND id = ?", resourceId, resourceId, id).ConfigureAwait(false);
            var auditDataEntry = await DbMapper.FirstOrDefaultAsync<ResourceAuditDataEntry>("WHERE id = ?", id).ConfigureAwait(false);
            if (auditEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("resoure metadata with id {0} is not found", id));
            }
            if (auditDataEntry != null)
            {
                auditEntry.OldData = auditDataEntry.OldData;
                auditEntry.NewData = auditDataEntry.NewData;
            }
            return auditEntry;
        }

    }
}
