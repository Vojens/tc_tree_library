﻿using System;
using Cassandra;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Repository.Audit.Cassandra.Cql
{
    internal class ResourceAudtiCqlRepository : CassandraRepository
    {
        public ResourceAudtiCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
        }

        public CqlInsert<ResourceAuditEntry> Add(ResourceAuditEntry entry)
        {
            return SchemaContainer.GetTable<ResourceAuditEntry>().Insert(entry, false);
        }

        public CqlInsert<ResourceAuditDataEntry> Add(ResourceAuditDataEntry entry)
        {
            return SchemaContainer.GetTable<ResourceAuditDataEntry>().Insert(entry, false);
        }

        public CqlQuerySingleElement<ResourceAuditEntry> Get(Guid resourceId, Guid id)
        {
            return SchemaContainer.GetTable<ResourceAuditEntry>().FirstOrDefault(c => c.OutId == resourceId && c.InId == resourceId && c.Id == id);
        }

        public CqlQuerySingleElement<ResourceAuditDataEntry> GetData(Guid id)
        {
            return SchemaContainer.GetTable<ResourceAuditDataEntry>().FirstOrDefault(c => c.Id == id);
        }

    }
}
