﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.ACD.Business;
using Infrastructure.ACD.Entities;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Mapping;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Solr.Query;
using SolrSortOrder = Infrastructure.Graph.Solr.Query.SortOrder;

namespace Infrastructure.Graph.Repository.Audit.Solr.Cql
{
    internal class ResourceAuditCqlRepository : SolrRepository
    {
        private readonly SchemaMap<AscendantsByResourceSolrEntry> _ascendantsResourceSolrEntry;
        private readonly SchemaMap<ResourceAuditSolrEntry> _resourceAuditSolrEntry;
        private readonly SchemaMap<PropertySolrEntry> _propertySolrEntryDefinition;

        public ResourceAuditCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            _propertySolrEntryDefinition = (SchemaMap<PropertySolrEntry>)schemaContainer.GetDefinition<PropertySolrEntry>();
            _ascendantsResourceSolrEntry = (SchemaMap<AscendantsByResourceSolrEntry>)schemaContainer.GetDefinition<AscendantsByResourceSolrEntry>();
            _resourceAuditSolrEntry = (SchemaMap<ResourceAuditSolrEntry>)schemaContainer.GetDefinition<ResourceAuditSolrEntry>();
        }

        public Tuple<CqlScalar<long>, CqlQuery<ResourceAuditSolrEntry>> ReadPaged(string resourceUri, List<Guid> useridentities, Permissions perm = Permissions.Read, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            int? start = null;
            AbstractSolrQuery basicQuery = new SolrQuery(string.Empty);
            var filterQueries = new List<FilterQuery>();
            ISolrQuery propertyFilterCondition = null;

            if (query != null)
            {
                if (query.Filters != null)
                {
                    var metaQuries = query.Filters.Where(x => x.Category == ConditionCategory.Audit);
                    foreach (var condition in metaQuries)
                    {
                        basicQuery = basicQuery && condition.BuidQuery();
                    }

                    var propertyQuries = query.Filters.Where(x => x.Category == ConditionCategory.Property).ToList();
                    if (propertyQuries.Any())
                    {
                        propertyFilterCondition =
                            new SolrMultipleCriteriaQuery(propertyQuries.Select(x => x.BuidQuery()),
                                AbstractSolrQuery.Operator.AND);
                    }
                }
                start = Math.Max(0, (query.PageNumber - 1) * query.PageSize);

            }


            if (!(startCd == 0 && endCd == 0))
            {
                var resCd = _ascendantsResourceSolrEntry.GetColumnName(x => x.CurveDistance);
                var ascResUri = _ascendantsResourceSolrEntry.GetColumnName(x => x.AscendantUri);

                var toCd = GetCurveDistanceTo(startCd);
                var fromCd = GetCurveDistanceFrom(endCd);

                var filterByUri = new FilterQuery(string.Format("{0}.{1}", KeySpace, _ascendantsResourceSolrEntry.GetTableName),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryByField(ascResUri, resourceUri) { EscapeValue = true, Quoted = false },
                        new SolrQueryByRange<short?>(resCd, fromCd, toCd)));

                filterQueries.Add(filterByUri);
            }

            if (propertyFilterCondition != null)
            {
                filterQueries.Add(new FilterQuery(string.Format("{0}.{1}", KeySpace, _propertySolrEntryDefinition.GetTableName),
                        propertyFilterCondition));
            }

            if (!PermissionService.IsWhitelisted(useridentities))
            {
                var identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                var permission = "perm";//_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter = new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                        new SolrQueryByField(permission, Convert.ToString((int)perm))));
                filterQueries.Add(acdFilter);
            }

            var sortOrder = GetSortOrder(query);
            var solrQueryTotal = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), null, sortOrder);
            var solrQueryWithStart = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), start, sortOrder);

            var totalCountQuery = SchemaContainer.GetTable<ResourceAuditSolrEntry>().Where(x => x.SolrQuery == solrQueryTotal).Count();
            var resultQuery = SchemaContainer.GetTable<ResourceAuditSolrEntry>().Where(x => x.SolrQuery == solrQueryWithStart);
            return Tuple.Create(totalCountQuery, resultQuery);
        }

        private short? GetCurveDistanceFrom(short cd)
        {
            short negate = -1;
            if (cd != short.MaxValue)
            {
                return (short)(Math.Abs(cd) * (negate));
            }
            return null;
        }

        private short? GetCurveDistanceTo(short cd)
        {
            short negate = -1;
            return ((short)(Math.Abs(cd) * (negate)));
        }


        private SolrSortOrder GetSortOrder(QueryOptions query)
        {
            if (query == null) return null;
            string sortBy;
            switch (query.SortBy)
            {
                case SortBy.Custom:
                    sortBy = GetTableColumnName(query.SortByStr);
                    break;
                case SortBy.CreatedDate:
                    sortBy = _resourceAuditSolrEntry.GetColumnName(x => x.TimeStamp);
                    break;
                case SortBy.Type:
                    sortBy = _resourceAuditSolrEntry.GetColumnName(x => x.ResourceType);
                    break;
                default:
                    sortBy = _resourceAuditSolrEntry.GetColumnName(x => x.TimeStamp);
                    break;
            }
            return new SolrSortOrder(sortBy, query.SortOrder == Model.SortOrder.Ascending ? Order.ASC : Order.DESC);
        }

        private string GetTableColumnName(string sortByField)
        {
            string columnName;
            if (sortByField.Equals("Action"))
            {
                columnName = _resourceAuditSolrEntry.GetColumnName(x => x.Action);
            }
            else if (sortByField.Equals("UserName"))
            {
                columnName = _resourceAuditSolrEntry.GetColumnName(x => x.UserName);
            }
            else
            {
                columnName = _resourceAuditSolrEntry.GetColumnName(x => x.TimeStamp);
            }


            return columnName;
        }
    }
}
