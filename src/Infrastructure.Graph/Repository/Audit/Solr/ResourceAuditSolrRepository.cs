﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.ACD.Entities;
using Infrastructure.Common;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Repository.Audit.Solr.Cql;
using Infrastructure.Common.Extensions;
using System.Threading.Tasks;
using Infrastructure.Graph.Model.Paging;

namespace Infrastructure.Graph.Repository.Audit.Solr
{
    internal class ResourceAuditSolrRepository : CassandraRepository
    {
        internal ResourceAuditCqlRepository CqlRepository;

        public ResourceAuditSolrRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new ResourceAuditCqlRepository(ConnectionProvider, SchemaContainer);
        }

        public async Task<PagedResult<ResourceAudit>> ReadPaged(string resourceUri, List<Guid> useridentities, Permissions perm = Permissions.Read, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            var tupleQuery = CqlRepository.ReadPaged(resourceUri, useridentities, perm, startCd, endCd, query);

            var cqlQueryResult = tupleQuery.Item2;
            var cqlQueryTotalCount = tupleQuery.Item1;

            cqlQueryTotalCount.Configure(StatementOptions.Select, ConnectionProvider);
            cqlQueryTotalCount.SetConsistencyLevel(Global.SolrConsistencyLevel);

            var totalCountTask = cqlQueryTotalCount.ExecuteAsync();
            var resultItemsTask = CqlQueryExtensions.ReadLimit(cqlQueryResult, ConnectionProvider, Global.SolrConsistencyLevel,
                    query != null ? query.PageSize : 500).Select(AutoMapper.Mapper.Map<ResourceAudit>).AsAsyncList();

            await Task.WhenAll(totalCountTask, resultItemsTask).ConfigureAwait(false);

            var result = await resultItemsTask.ConfigureAwait(false);
            var totalCount = (int)await totalCountTask.ConfigureAwait(false);
            var pageNo = query != null ? query.PageNumber : 0;
            var queryPageSize = query == null ? 0 : query.PageSize;

            var pagedResult = new PagedResult<ResourceAudit>(result, pageNo, queryPageSize, totalCount);
            return pagedResult;

        }
    }
}
