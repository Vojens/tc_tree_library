using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Data.Cassandra.Cql;

namespace Infrastructure.Graph.Repository.Data.Cassandra
{
    internal class ResourceDataSizeCassandraRepository : CassandraRepository
    {
        protected internal ResourceDataSizeCqlRepository CqlRepository;
        public ResourceDataSizeCassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new ResourceDataSizeCqlRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(ResourceDataSizeEntry entry)
        {
            return CqlRepository.Add(entry).ConfigureAndExecuteAsync(ConnectionProvider);
        }

        public Task Delete(Guid id)
        {
            return DbMapper.DeleteAsync<ResourceDataSizeEntry>("WHERE id = ?", id);
        }

        public async Task<ResourceDataSizeEntry> Get(Guid id)
        {
            var resourceMetadataEntry = await DbMapper.FirstOrDefaultAsync<ResourceDataSizeEntry>("WHERE id = ?", id).ConfigureAwait(false);
            if (resourceMetadataEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("resoure data size with id {0} is not found", id));
            }
            return resourceMetadataEntry;
        }

        public async Task<bool> TryGet(Guid id, Ref<ResourceDataSizeEntry> @ref)
        {
            var resourceMetadataEntry = await DbMapper.FirstOrDefaultAsync<ResourceDataSizeEntry>("WHERE id = ?", id).ConfigureAwait(false);
            if (resourceMetadataEntry.IsNullOrDefault())
            {
                return false;
            }
            @ref.Value = resourceMetadataEntry;
            return true;
        }
    }
}