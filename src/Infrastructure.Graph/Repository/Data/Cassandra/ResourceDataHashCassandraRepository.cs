﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Data.Cassandra.Cql;

namespace Infrastructure.Graph.Repository.Data.Cassandra
{
    internal class ResourceDataHashCassandraRepository : CassandraRepository
    {
        protected internal ResourceDataHashCqlRepository CqlRepository;
        public ResourceDataHashCassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new ResourceDataHashCqlRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(ResourceDataHashEntry entry)
        {
            return CqlRepository.Add(entry).ConfigureAndExecuteAsync(ConnectionProvider);
        }

        public Task Delete(Guid id)
        {
            return DbMapper.DeleteAsync<ResourceDataHashEntry>("WHERE id = ?", id);
        }

        public async Task<ResourceDataHashEntry> Get(Guid id)
        {
            var resourceMetadataEntry = await DbMapper.FirstOrDefaultAsync<ResourceDataHashEntry>("WHERE id = ?", id).ConfigureAwait(false);
            if (resourceMetadataEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("resoure data hash with id {0} is not found", id));
            }
            return resourceMetadataEntry;
        }

        public async Task<bool> TryGet(Guid id, Ref<ResourceDataHashEntry> @ref)
        {
            var resourceMetadataEntry = await DbMapper.FirstOrDefaultAsync<ResourceDataHashEntry>("WHERE id = ?", id).ConfigureAwait(false);
            if (resourceMetadataEntry.IsNullOrDefault())
            {
                return false;
            }
            @ref.Value = resourceMetadataEntry;
            return true;
        }
    }
}
