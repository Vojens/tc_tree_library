using System;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Repository.Data.Cassandra.Cql
{
    internal class ResourceDataSizeCqlRepository : CassandraRepository
    {
        public ResourceDataSizeCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
        }

        public CqlInsert<ResourceDataSizeEntry> Add(ResourceDataSizeEntry entry)
        {
            return SchemaContainer.GetTable<ResourceDataSizeEntry>().Insert(entry);
        }

        public CqlQuerySingleElement<ResourceDataSizeEntry> Get(Guid id)
        {
            return SchemaContainer.GetTable<ResourceDataSizeEntry>().Where(c => c.Id == id).FirstOrDefault();
        }

        public CqlDelete Delete(Guid id)
        {
            return SchemaContainer.GetTable<ResourceDataSizeEntry>().Where(c => c.Id == id).Delete();
        }
    }
}