﻿using System;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Repository.Data.Cassandra.Cql
{
    internal class ResourceDataHashCqlRepository : CassandraRepository
    {
        public ResourceDataHashCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
        }

        public CqlInsert<ResourceDataHashEntry> Add(ResourceDataHashEntry entry)
        {
            return SchemaContainer.GetTable<ResourceDataHashEntry>().Insert(entry, false);
        }

        public CqlQuerySingleElement<ResourceDataHashEntry> Get(Guid id)
        {
            return SchemaContainer.GetTable<ResourceDataHashEntry>().Where(c => c.Id == id).FirstOrDefault();
        }

        public CqlDelete Delete(Guid id)
        {
            return SchemaContainer.GetTable<ResourceDataHashEntry>().Where(c => c.Id == id).Delete();
        }
    }
}
