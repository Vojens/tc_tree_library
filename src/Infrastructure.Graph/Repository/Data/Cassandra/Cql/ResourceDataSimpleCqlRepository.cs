using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Repository.Data.Cassandra.Cql
{
    internal class ResourceDataSimpleCqlRepository : CassandraRepository
    {
        public ResourceDataSimpleCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
        }

        public CqlInsert<ResourceDataSimpleEntry> Add(ResourceDataSimpleEntry entry)
        {
            return SchemaContainer.GetTable<ResourceDataSimpleEntry>().Insert(entry, false);
        }

        public CqlQuerySingleElement<ResourceDataSimpleEntry> Get(Guid id)
        {
            return SchemaContainer.GetTable<ResourceDataSimpleEntry>().Where(c => c.Id == id).FirstOrDefault();
        }

        public CqlQuery<ResourceDataSimpleEntry> Get(List<Guid> ids)
        {
            var result= SchemaContainer.GetTable<ResourceDataSimpleEntry>().Where(c => ids.Contains(c.Id));
            return result;
        }

        public CqlDelete Delete(Guid id)
        {
            return SchemaContainer.GetTable<ResourceDataSimpleEntry>().Where(c => c.Id == id).Delete();
        }

        public CqlUpdate Update(Guid id, Expression<Func<ResourceDataSimpleEntry, bool>> selector)
        {
            return SchemaContainer.GetTable<ResourceDataSimpleEntry>()
                .Where(c => c.Id == id)
                .Select(c => selector)
                .Update();
        }
    }
}