﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Data.Cassandra.Cql;

namespace Infrastructure.Graph.Repository.Data.Cassandra
{
    internal class ResourceDataSimpleCassandraRepository : CassandraRepository
    {
        protected internal ResourceDataSimpleCqlRepository CqlRepository;
        public ResourceDataSimpleCassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new ResourceDataSimpleCqlRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(ResourceDataSimpleEntry entry)
        {
            return CqlRepository.Add(entry).ConfigureAndExecuteAsync(ConnectionProvider);
        }

        public Task Delete(Guid id)
        {
            return DbMapper.DeleteAsync<ResourceDataSimpleEntry>("WHERE id = ?", id);
        }

        public Task Update(Guid id, Expression<Func<ResourceDataSimpleEntry, bool>> selector)
        {
            return CqlRepository.Update(id, selector).Configure(StatementOptions.Upsert, ConnectionProvider).ExecuteAsync();
        }

        public async Task<ResourceDataSimpleEntry> Get(Guid id)
        {
            var resourceMetadataEntry = await DbMapper.FirstOrDefaultAsync<ResourceDataSimpleEntry>("WHERE id = ?", id).ConfigureAwait(false);
            if (resourceMetadataEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("resoure data simple with id {0} is not found", id));
            }
            return resourceMetadataEntry;
        }

        public Task<ResourceDataSimpleEntry> TryGet(Guid id)
        {
            return DbMapper.FirstOrDefaultAsync<ResourceDataSimpleEntry>("WHERE id = ?", id);
        }


        public IObservable<ResourceDataSimpleEntry> Get(List<Guid> ids)
        {
            return Observable.Create<ResourceDataSimpleEntry>(async o =>
            {
                try
                {
                    if (ids != null)
                    {
                        var tasks = ids.Select(TryGet).ToList();
                        tasks.ToAwaitObservable().Where(x => x != null).Subscribe(o);
                    }
                    else
                    {
                        o.OnCompleted();
                    }
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }
        public async Task<bool> TryGet(Guid id, Ref<ResourceDataSimpleEntry> @ref)
        {
            var resourceMetadataEntry = await DbMapper.FirstOrDefaultAsync<ResourceDataSimpleEntry>("WHERE id = ?", id).ConfigureAwait(false);
            if (resourceMetadataEntry.IsNullOrDefault())
            {
                return false;
            }
            @ref.Value = resourceMetadataEntry;
            return true;
        }
    }
}