﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Repository.Replication.Cassandra.Cql
{
    internal class ReplicationEventCqlRepository : CassandraRepository
    {
        private static readonly int Ttl;

        static ReplicationEventCqlRepository()
        {
            var ttl = ConfigurationManager.AppSettings["replicationEventTTL"];
            if (!int.TryParse(ttl, out Ttl))
            {
                Ttl = 86400;
            }
        }

        public ReplicationEventCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
        }

        public CqlInsert<ReplicationEventEntry> Add(ReplicationEventEntry entry)
        {
            var replicationEventEntries = SchemaContainer.GetTable<ReplicationEventEntry>();
            var cqlInsert = replicationEventEntries.Insert(entry, false);
            cqlInsert.SetTTL(Ttl);
            return cqlInsert;
        }

        public CqlQuery<ReplicationEventEntry> Read(Guid ruleId, sbyte status)
        {
            return
                SchemaContainer.GetTable<ReplicationEventEntry>()
                    .Where(c => c.RuleId == ruleId && status == c.Status);
        }

        public CqlQuery<ReplicationEventEntry> Read(Guid ruleId, List<sbyte> status)
        {
            return
                SchemaContainer.GetTable<ReplicationEventEntry>()
                    .Where(c => c.RuleId == ruleId && status.Contains(c.Status));
        }

        public CqlQuery<ReplicationEventEntry> Read(Guid ruleId, sbyte status, DateTimeOffset since)
        {
            return
                SchemaContainer.GetTable<ReplicationEventEntry>()
                    .Where(c => c.RuleId == ruleId && c.Status == status && c.Timestamp > since);
        }

        public CqlQuery<ReplicationEventEntry> Read(Guid ruleId, List<sbyte> status, DateTimeOffset lastTimestamp)
        {
            return
                SchemaContainer.GetTable<ReplicationEventEntry>()
                    .Where(c => c.RuleId == ruleId && status.Contains(c.Status) && c.Timestamp < lastTimestamp);
        }

        public CqlQuery<ReplicationEventViewEntry> Read(Guid ruleId, List<sbyte> status, string resourceType)
        {
            return
                SchemaContainer.GetTable<ReplicationEventViewEntry>()
                    .Where(c => c.RuleId == ruleId && status.Contains(c.Status) && c.ResourceType == resourceType);
        }

        public CqlQuery<ReplicationEventViewEntry> Read(Guid ruleId, List<sbyte> status, string resourceType, string resourceName)
        {
            return
                SchemaContainer.GetTable<ReplicationEventViewEntry>()
                    .Where(c => c.RuleId == ruleId && status.Contains(c.Status) && c.ResourceType == resourceType && c.ResourceName == resourceName);
        }
    }
}
