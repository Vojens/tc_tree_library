﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Replication.Cassandra.Cql;

namespace Infrastructure.Graph.Repository.Replication.Cassandra
{
    internal class ReplicationEventCassandraRepository : CassandraRepository
    {
        protected ReplicationEventCqlRepository CqlRepository;

        public ReplicationEventCassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new ReplicationEventCqlRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(ReplicationEventEntry entry)
        {
            return CqlRepository.Add(entry).ConfigureAndExecuteAsync(ConnectionProvider);
        }

        public IObservable<ReplicationEventEntry> Read(Guid ruleId, sbyte status)
        {
            return Observable.Create<ReplicationEventEntry>(async o =>
            {
                try
                {
                    var replicationEventEntries = await DbMapper.FetchAsync<ReplicationEventEntry>("WHERE ruleid = ? AND status = ?", ruleId, status).ConfigureAwait(false);
                    foreach (var replicationEventEntry in replicationEventEntries)
                    {
                        o.OnNext(replicationEventEntry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ReplicationEventEntry> Read(Guid ruleId, List<sbyte> status)
        {
            return Observable.Create<ReplicationEventEntry>(async o =>
            {
                try
                {

                    var @params = new List<object> { ruleId };
                    status.ForEach(i => @params.Add(i));

                    var whereClause = string.Format("WHERE ruleid = ? AND status IN ({0})", string.Join(",", Enumerable.Repeat("?", status.Count)));
                    var replicationEventEntries = await DbMapper.FetchAsync<ReplicationEventEntry>(whereClause, @params.ToArray()).ConfigureAwait(false);
                    
                    foreach (var replicationEventEntry in replicationEventEntries)
                    {
                        o.OnNext(replicationEventEntry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ReplicationEventEntry> Read(Guid ruleId, sbyte status, int limit)
        {
            return Observable.Create<ReplicationEventEntry>(async o =>
            {
                try
                {
                    var replicationEventEntries = await DbMapper.FetchAsync<ReplicationEventEntry>("WHERE ruleid = ? AND status = ? LIMIT ?", ruleId, status, limit).ConfigureAwait(false);
                    foreach (var replicationEventEntry in replicationEventEntries)
                    {
                        o.OnNext(replicationEventEntry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ReplicationEventEntry> Read(Guid ruleId, sbyte status, DateTimeOffset since, int limit)
        {
            return Observable.Create<ReplicationEventEntry>(async o =>
            {
                try
                {
                    var replicationEventEntries = await DbMapper.FetchAsync<ReplicationEventEntry>("WHERE ruleid = ? AND status = ? AND dtime > ? LIMIT ?", ruleId, status, since, limit).ConfigureAwait(false);
                    foreach (var replicationEventEntry in replicationEventEntries)
                    {
                        o.OnNext(replicationEventEntry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ReplicationEventEntry> Read(Guid ruleId, List<sbyte> status, DateTimeOffset lastTimestamp, int limit)
        {
            return Observable.Create<ReplicationEventEntry>(async o =>
            {
                try
                {
                    if (limit > 0)
                    {

                        var @params = new List<object> { ruleId };
                        status.ForEach(i => @params.Add(i));
                        @params.Add(lastTimestamp);
                        @params.Add(limit);

                        var whereClause = string.Format("WHERE ruleid = ? AND status IN ({0}) AND dtime < ? LIMIT ?", string.Join(",", Enumerable.Repeat("?", status.Count)));
                        var replicationEventEntries = await DbMapper.FetchAsync<ReplicationEventEntry>(whereClause, @params.ToArray()).ConfigureAwait(false);


                        foreach (var replicationEventEntry in replicationEventEntries)
                        {
                            o.OnNext(replicationEventEntry);
                        }
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        private IObservable<ReplicationEventViewEntry> ReadViewEntries(Guid ruleId, List<sbyte> status, string resourceType)
        {
            return Observable.Create<ReplicationEventViewEntry>(async o =>
            {
                try
                {
                    var @params = new List<object> { ruleId };
                    status.ForEach(i => @params.Add(i));
                    @params.Add(resourceType);

                    var whereClause = string.Format("WHERE ruleid = ? AND status IN ({0}) AND restype = ?", string.Join(",", Enumerable.Repeat("?", status.Count)));
                    var replicationEventEntries = await DbMapper.FetchAsync<ReplicationEventViewEntry>(whereClause, @params.ToArray()).ConfigureAwait(false);
                    foreach (var replicationEventEntry in replicationEventEntries)
                    {
                        o.OnNext(replicationEventEntry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        private IObservable<ReplicationEventViewEntry> ReadViewEntries(Guid ruleId, List<sbyte> status, string resourceType, string name)
        {
            return Observable.Create<ReplicationEventViewEntry>(async o =>
            {
                try
                {
                    var @params = new List<object> { ruleId };
                    status.ForEach(i => @params.Add(i));
                    @params.Add(resourceType);
                    @params.Add(name);

                    var whereClause = string.Format("WHERE ruleid = ? AND status IN ({0}) AND restype = ? AND resname = ?", string.Join(",", Enumerable.Repeat("?", status.Count)));
                    var replicationEventEntries = await DbMapper.FetchAsync<ReplicationEventViewEntry>(whereClause, @params.ToArray()).ConfigureAwait(false);
                    foreach (var replicationEventEntry in replicationEventEntries)
                    {
                        o.OnNext(replicationEventEntry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }


        public IObservable<ReplicationEventViewEntry> Read(Guid ruleId, List<sbyte> status, params string[] resourceType)
        {
            return Observable.Create<ReplicationEventViewEntry>(async o =>
            {
                try
                {
                    IEnumerable<ReplicationEventViewEntry> replicationEventViewEntries;
                    if (resourceType.Length == 1)
                    {
                        replicationEventViewEntries = await ReadViewEntries(ruleId, status, resourceType[0]).ToAsyncList().ConfigureAwait(false);
                    }
                    else
                    {
                        var tasks = resourceType.Select(resType => ReadViewEntries(ruleId, status, resType).ToAsyncList());
                        var results = await Task.WhenAll(tasks).ConfigureAwait(false);
                        replicationEventViewEntries = results.SelectMany(x => x.ToArray()).OrderByDescending(y => y.Timestamp).ToList();
                    }
                    foreach (var replicationEventViewEntry in replicationEventViewEntries)
                    {
                        o.OnNext(replicationEventViewEntry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ReplicationEventViewEntry> Read(Guid ruleId, List<sbyte> status, string resourceName, params string[] resourceType)
        {
            return Observable.Create<ReplicationEventViewEntry>(async o =>
            {
                try
                {
                    IEnumerable<ReplicationEventViewEntry> replicationEventViewEntries;
                    if (resourceType.Length == 1)
                    {
                        replicationEventViewEntries = await ReadViewEntries(ruleId, status, resourceType[0], resourceName).ToAsyncList().ConfigureAwait(false);
                    }
                    else
                    {
                        var tasks = resourceType.Select(resType => ReadViewEntries(ruleId, status, resType, resourceName).ToAsyncList());
                        var results = await Task.WhenAll(tasks).ConfigureAwait(false);
                        replicationEventViewEntries = results.SelectMany(x => x.ToArray()).OrderByDescending(y => y.Timestamp).ToList();
                    }
                    foreach (var replicationEventViewEntry in replicationEventViewEntries)
                    {
                        o.OnNext(replicationEventViewEntry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }
    }
}
