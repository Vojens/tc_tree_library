﻿using System;
using System.Linq.Expressions;
using Cassandra;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Repository.Metadata.Cassandra.Cql
{
    internal class ResourceMetadataCqlRepository : CassandraRepository
    {
        public ResourceMetadataCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
        }

        public CqlInsert<ResourceMetadataEntry> Add(ResourceMetadataEntry entry)
        {
            return SchemaContainer.GetTable<ResourceMetadataEntry>().Insert(entry, false);
        }

        public CqlQuerySingleElement<ResourceMetadataEntry> Get(Guid id)
        {
            return SchemaContainer.GetTable<ResourceMetadataEntry>().Where(c => c.OutId == id && c.InId == id).FirstOrDefault();
        }

        public CqlDelete Delete(Guid id)
        {
            return SchemaContainer.GetTable<ResourceMetadataEntry>().Where(c => c.OutId == id && c.InId == id).Delete();
        }

//        public CqlUpdate Update(Guid id, Expression<Func<ResourceMetadataEntry, bool>> selector)
//        {
//            return SchemaContainer.GetTable<ResourceMetadataEntry>()
//                .Where(c => c.OutId == id && c.InId == id)
//                .Select(c => selector)
//                .Update();
//        }
//
//        public CqlUpdate Update(Guid id, Expression<Func<ResourceMetadataEntry, ResourceMetadataEntry>> selector)
//        {
//            return SchemaContainer.GetTable<ResourceMetadataEntry>()
//                .Where(c => c.OutId == id && c.InId == id)
//                .Select(selector)
//                .Update();
//        }

        public CqlUpdate UpdateChangeTimestamp(Guid id, DateTimeOffset timestamp, LocalTime time)
        {
            return SchemaContainer.GetTable<ResourceMetadataEntry>()
                .Where(c => c.OutId == id && c.InId == id)
                .Select(c => new ResourceMetadataEntry(){ChangeTimestamp = timestamp, ChangeTime = time})
                .Update();
        }

        public CqlUpdate UpdateChangeTimestampAndUserInfo(Guid id, DateTimeOffset timestamp, LocalTime time, Guid userId, string userName)
        {
            return SchemaContainer.GetTable<ResourceMetadataEntry>()
                .Where(c => c.OutId == id && c.InId == id)
                .Select(c => new ResourceMetadataEntry() { ChangeTimestamp = timestamp, ChangeTime = time, ChangeUserId = userId, ChangeUserName = userName })
                .Update();
        }
    }
}
