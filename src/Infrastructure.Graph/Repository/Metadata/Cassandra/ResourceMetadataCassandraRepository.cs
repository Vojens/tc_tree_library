﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Cassandra;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Metadata.Cassandra.Cql;

namespace Infrastructure.Graph.Repository.Metadata.Cassandra
{
    internal class ResourceMetadataCassandraRepository : CassandraRepository
    {
        protected ResourceMetadataCqlRepository CqlRepository;
        public ResourceMetadataCassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new ResourceMetadataCqlRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(ResourceMetadataEntry entry)
        {
            return CqlRepository.Add(entry).ConfigureAndExecuteAsync(ConnectionProvider);
        }

        public Task Delete(Guid id)
        {
            return DbMapper.DeleteAsync<ResourceMetadataEntry>("WHERE outid = ? AND inid = ?", id, id);
        }

//        public Task Update(Guid id, Expression<Func<ResourceMetadataEntry, bool>> selector)
//        {
//            return CqlRepository.Update(id, selector).Configure(StatementOptions.Upsert, ConnectionProvider).ExecuteAsync();
//        }
//
//        public Task Update(Guid id, Expression<Func<ResourceMetadataEntry, ResourceMetadataEntry>> selector)
//        {
//            return CqlRepository.Update(id, selector).Configure(StatementOptions.Upsert, ConnectionProvider).ExecuteAsync();
//        }

        public async Task<ResourceMetadataEntry> Get(Guid id)
        {
            var resourceMetadataEntry = await DbMapper.FirstOrDefaultAsync<ResourceMetadataEntry>("WHERE outid = ? AND inid = ?", id, id).ConfigureAwait(false);
            if (resourceMetadataEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("resoure metadata with id {0} is not found", id));
            }
            return resourceMetadataEntry;
        }

        public async Task<bool> TryGet(Guid id, Ref<ResourceMetadataEntry> @ref)
        {
            var resourceMetadataEntry = await DbMapper.FirstOrDefaultAsync<ResourceMetadataEntry>("WHERE outid = ? AND inid = ?", id, id).ConfigureAwait(false);
            if (resourceMetadataEntry.IsNullOrDefault())
            {
                return false;
            }
            @ref.Value = resourceMetadataEntry;
            return true;
        }

        public Task UpdateChangeTimestamp(Guid id, DateTimeOffset timestamp, LocalTime time)
        {
            return DbMapper.UpdateAsync<ResourceMetadataEntry>("SET ctime = ?, ctime_t = ?  WHERE outid = ? AND inid = ?", timestamp, time, id, id);
        }

        public Task UpdateChangeTimestampAndUserInfo(Guid id, DateTimeOffset timestamp, LocalTime time, Guid userId, string userName)
        {
            return DbMapper.UpdateAsync<ResourceMetadataEntry>("SET ctime = ?, ctime_t = ?, cusrid = ?, cusrnm = ?  WHERE outid = ? AND inid = ?", timestamp, time, userId, userName, id, id);
        }
    }
}
