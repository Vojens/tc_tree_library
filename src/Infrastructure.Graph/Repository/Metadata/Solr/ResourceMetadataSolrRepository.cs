﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Cassandra;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.ACD.Entities;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Metadata.Solr.Cql;
using Infrastructure.Common;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Model.Paging;
using QueryOptions = Infrastructure.Graph.Model.QueryOptions;

namespace Infrastructure.Graph.Repository.Metadata.Solr
{
    internal class ResourceMetadataSolrRepository : CassandraRepository
    {
        internal ResourceMetadataSolrCqlRepository CqlRepository;
        public ResourceMetadataSolrRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new ResourceMetadataSolrCqlRepository(ConnectionProvider, SchemaContainer);
        }

        public async Task<ResourceMetadataSolrEntry> Get(string uri)
        {
            var cqlQuerySingleElement = CqlRepository.Get(uri);
            cqlQuerySingleElement.Configure(StatementOptions.Select, ConnectionProvider);
            cqlQuerySingleElement.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var resourceMetadataSolrEntry = await cqlQuerySingleElement.ExecuteAsync().ConfigureAwait(false);
            if (resourceMetadataSolrEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("resoure metadata with uri {0} is not found", uri));
            }
            return resourceMetadataSolrEntry;
        }

        public async Task<bool> TryGet(string uri, Ref<ResourceMetadataSolrEntry> @ref)
        {
            var cqlQuerySingleElement = CqlRepository.Get(uri);
            cqlQuerySingleElement.Configure(StatementOptions.Select, ConnectionProvider);
            cqlQuerySingleElement.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var resourceMetadataEntry = await CqlRepository.Get(uri).ConfigureAndExecuteAsync(ConnectionProvider).ConfigureAwait(false);
            if (resourceMetadataEntry.IsNullOrDefault())
            {
                return false;
            }
            @ref.Value = resourceMetadataEntry;
            return true;
        }

        public IObservable<ResourceMetadataSolrEntry> Get(List<Guid> ids)
        {
            return CqlQueryExtensions.Read(CqlRepository.Get(ids), ConnectionProvider, Global.SolrConsistencyLevel);
        }

        public IObservable<ResourceMetadataSolrEntry> ReadPaged(string resourceUri, List<Guid> useridentities, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, Model.QueryOptions query = null)
        {
            int limit = 500;
            if (query != null) limit = query.PageSize;
            return CqlQueryExtensions.ReadLimit(CqlRepository.ReadPaged(resourceUri, useridentities, perm, resourceType, startCd, endCd, query), ConnectionProvider, Global.SolrConsistencyLevel, limit);
        }

        public async Task<PagedResult<ResourceMetadataSolrEntry>> ReadDescendantsPaged(List<Guid> useridentities, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, Model.QueryOptions query = null,bool skipAclCheck=false, params string[] resourceUris)
        {
            var tupleQuery = CqlRepository.ReadDescendantsPaged(useridentities, perm, resourceType, startCd, endCd, query, skipAclCheck, resourceUris);

            var cqlQueryResult = tupleQuery.Item2;
            var cqlQueryTotalCount = tupleQuery.Item1;

            cqlQueryTotalCount.Configure(StatementOptions.Select, ConnectionProvider);
            cqlQueryTotalCount.SetConsistencyLevel(Global.SolrConsistencyLevel);

            var totalCountTask = cqlQueryTotalCount.ExecuteAsync();
            var resultItemsTask = CqlQueryExtensions.ReadLimit(cqlQueryResult, ConnectionProvider, Global.SolrConsistencyLevel, query != null ? query.PageSize : 500).AsAsyncList();

            await Task.WhenAll(totalCountTask, resultItemsTask).ConfigureAwait(false);

            var result = await resultItemsTask.ConfigureAwait(false);
            var totalCount = (int)await totalCountTask.ConfigureAwait(false);
            var pageNo = query != null ? query.PageNumber : 0;
            var queryPageSize = query == null ? 0 : query.PageSize;
            var pagedResult = new PagedResult<ResourceMetadataSolrEntry>(result, pageNo, queryPageSize, totalCount);
            return pagedResult;
        }
     

        public async Task<PagedResult<ResourceMetadataSolrEntry>> ReadRecentDescendantsPaged(string resourceUri, List<Guid> useridentities, int noOfDays, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, Model.QueryOptions query = null)
        {
            var tupleQuery = CqlRepository.ReadRecentDescendantsPaged(resourceUri, useridentities, noOfDays, perm, resourceType, startCd, endCd, query);

            var cqlQueryResult = tupleQuery.Item2;
            var cqlQueryTotalCount = tupleQuery.Item1;

            cqlQueryTotalCount.Configure(StatementOptions.Select, ConnectionProvider);
            cqlQueryTotalCount.SetConsistencyLevel(Global.SolrConsistencyLevel);

            var totalCountTask = cqlQueryTotalCount.ExecuteAsync();
            var resultItemsTask = CqlQueryExtensions.ReadLimit(cqlQueryResult, ConnectionProvider, Global.SolrConsistencyLevel, query != null ? query.PageSize : 500).AsAsyncList();

            await Task.WhenAll(totalCountTask, resultItemsTask).ConfigureAwait(false);

            var result = await resultItemsTask.ConfigureAwait(false);
            var totalCount = (int)await totalCountTask.ConfigureAwait(false);
            var pageNo = query != null ? query.PageNumber : 0;
            var queryPageSize = query == null ? 0 : query.PageSize;

            var pagedResult = new PagedResult<ResourceMetadataSolrEntry>(result, pageNo, queryPageSize, totalCount);
            return pagedResult;
        }

        public async Task<bool> SolrReadHasChildren(string uri, string resourceType, List<Guid> useridentities, Permissions perm = Permissions.Read)
        {
            var cqlQuerySingleElement = CqlRepository.ReadHasChildren(uri, resourceType, useridentities, perm);
            cqlQuerySingleElement.Configure(StatementOptions.Select, ConnectionProvider);
            cqlQuerySingleElement.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var resourceMetadataSolrEntry = await cqlQuerySingleElement.ExecuteAsync().ConfigureAwait(false);
            return !resourceMetadataSolrEntry.IsNullOrDefault();
        }

        public async Task<ResourceMetadataSolrEntry> ReadMetadataByCondition(Condition[] conditions, List<Guid> useridentities, Permissions perm = Permissions.Read)
        {
            var cqlQuerySingleElement = CqlRepository.ReadMetaDataByCondition(conditions,useridentities, perm);
            cqlQuerySingleElement.Configure(StatementOptions.Select, ConnectionProvider);
            cqlQuerySingleElement.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var resourceMetadataSolrEntry = await cqlQuerySingleElement.ExecuteAsync().ConfigureAwait(false);
            return resourceMetadataSolrEntry;
        }

        public IObservable<ResourceMetadataSolrEntry> SolrReadWithIds(string resourceUri, List<Guid> resourceIds, List<Guid> useridentities, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, Model.QueryOptions query = null)
        {
            return CqlRepository.ReadWithIds(resourceUri, resourceIds, useridentities, perm, resourceType, startCd, endCd, query).ReadLimit(ConnectionProvider, Global.SolrConsistencyLevel, int.MaxValue);
        }

        public IObservable<ResourceMetadataSolrEntry> SolrReadRecentWithIds(string resourceUri, List<Guid> resourceIds, List<Guid> useridentities, int noOfDays, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, Model.QueryOptions query = null)
        {
            return CqlRepository.ReadRecentWithIds(resourceUri, resourceIds, useridentities, noOfDays, perm, resourceType, startCd, endCd, query).ReadLimit(ConnectionProvider, Global.SolrConsistencyLevel);
        }
    }
}
