﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using Newtonsoft.Json;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Mapping;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Solr.Query;
using SolrSortOrder = Infrastructure.Graph.Solr.Query.SortOrder;
using SortOrder = Infrastructure.Graph.Model.SortOrder;
using Infrastructure.ACD.Business;
using Infrastructure.ACD.Entities;

namespace Infrastructure.Graph.Repository.Metadata.Solr.Cql
{
    internal class ResourceMetadataSolrCqlRepository : SolrRepository
    {
        private readonly SchemaMap<PropertySolrEntry> _propertySolrEntryDefinition;
        private readonly SchemaMap<ResourceMetadataSolrEntry> _resourceMetadataSolrEntry;
        private readonly SchemaMap<AscendantsByResourceSolrEntry> _ascendantsResourceSolrEntry;
        //private readonly SchemaMap<AcdSetEntry> _acdSetSolrEntry;

        public ResourceMetadataSolrCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            _propertySolrEntryDefinition = (SchemaMap<PropertySolrEntry>)schemaContainer.GetDefinition<PropertySolrEntry>();
            _resourceMetadataSolrEntry = (SchemaMap<ResourceMetadataSolrEntry>)schemaContainer.GetDefinition<ResourceMetadataSolrEntry>();
            _ascendantsResourceSolrEntry = (SchemaMap<AscendantsByResourceSolrEntry>)schemaContainer.GetDefinition<AscendantsByResourceSolrEntry>();
            //_acdSetSolrEntry = (SchemaMap<AcdSetEntry>)schemaContainer.GetDefinition<AcdSetEntry>();
        }

        public CqlQuerySingleElement<ResourceMetadataSolrEntry> Get(string uri)
        {
            var solrQuery = JsonConvert.SerializeObject(new
            {
                q = string.Format(@"uri:""{0}""", uri)
                //df = "text",
                //paging = "driver"
            });
            //var solrQuery = string.Format(@"""uri"":""{0}""", uri);
            //var solrQuery = string.Format(@"uri:{0}", uri);
            return SchemaContainer.GetTable<ResourceMetadataSolrEntry>().Where(x => x.SolrQuery == solrQuery).Select(x => new ResourceMetadataSolrEntry
            { OutId = x.OutId, InId = x.InId, Name = x.Name, Uri = x.Uri, Type = x.Type, SchemaType = x.SchemaType, Schema = x.Schema, CreationUserId = x.CreationUserId, CreationUserName = x.CreationUserName, CreationTime = x.CreationTime, CreationTimestamp = x.CreationTimestamp, ChangeUserId = x.ChangeUserId, ChangeUserName = x.ChangeUserName, ChangeTime = x.ChangeTime, ChangeTimestamp = x.ChangeTimestamp, Version = x.Version, Description = x.Description, Keywords = x.Keywords, Workflow = x.Workflow, WorkflowState = x.WorkflowState, IsMarkedDeleted = x.IsMarkedDeleted, IsHidden = x.IsHidden, IsReadOnly = x.IsReadOnly, IsSystem = x.IsSystem, ContentType = x.ContentType, StorageType = x.StorageType, StorageId = x.StorageId, StorageUri = x.StorageUri, ResourceSystem = x.ResourceSystem, SolrQuery = x.SolrQuery }).FirstOrDefault();
        }

        public CqlQuerySingleElement<ResourceMetadataSolrEntry> ReadMetaDataByCondition(Condition[] conditions, List<Guid> useridentities, Permissions perm = Permissions.Read)
        {
            AbstractSolrQuery basicQuery = new SolrQuery("");
            if (conditions == null || !conditions.Any(x => x.HasKey(MetadataCondition.Keys.IsDeleted.ToString())))
            {
                var metaIsDeleted = _resourceMetadataSolrEntry.GetColumnName(x => x.IsMarkedDeleted);
                basicQuery = new SolrQueryByField(metaIsDeleted, bool.FalseString);
            }
            if (conditions != null)
            {
                foreach (var condition in conditions)
                {
                    basicQuery = basicQuery && condition.BuidQuery();
                }
            }

            var filterQueries = new List<FilterQuery>();

            if (!PermissionService.IsWhitelisted(useridentities))
            {
                var identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                var permission = "perm";//_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter = new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                        new SolrQueryByField(permission, Convert.ToString((int)perm))));
                filterQueries.Add(acdFilter);
            }
            var solrQuery = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), null);

            return SchemaContainer.GetTable<ResourceMetadataSolrEntry>().Where(x => x.SolrQuery == solrQuery).Select(x => new ResourceMetadataSolrEntry
            { OutId = x.OutId, InId = x.InId, Name = x.Name, Uri = x.Uri, Type = x.Type, SchemaType = x.SchemaType, Schema = x.Schema, CreationUserId = x.CreationUserId, CreationUserName = x.CreationUserName, CreationTime = x.CreationTime, CreationTimestamp = x.CreationTimestamp, ChangeUserId = x.ChangeUserId, ChangeUserName = x.ChangeUserName, ChangeTime = x.ChangeTime, ChangeTimestamp = x.ChangeTimestamp, Version = x.Version, Description = x.Description, Keywords = x.Keywords, Workflow = x.Workflow, WorkflowState = x.WorkflowState, IsMarkedDeleted = x.IsMarkedDeleted, IsHidden = x.IsHidden, IsReadOnly = x.IsReadOnly, IsSystem = x.IsSystem, ContentType = x.ContentType, StorageType = x.StorageType, StorageId = x.StorageId, StorageUri = x.StorageUri, ResourceSystem = x.ResourceSystem, SolrQuery = x.SolrQuery }).FirstOrDefault();
        }

        public CqlQuery<ResourceMetadataSolrEntry> Get(List<Guid> ids)
        {
            var solrQuery = string.Empty;
            for (var i = 0; i < ids.Count; i++)
            {
                solrQuery = solrQuery + string.Format(@" outid: {0}", ids[i]);
            }
            var query = JsonConvert.SerializeObject(new
            {
                q = solrQuery,
                paging = "driver"
            });
            return SchemaContainer.GetTable<ResourceMetadataSolrEntry>().Where(x => x.SolrQuery == query).Select(x => new ResourceMetadataSolrEntry
            { OutId = x.OutId, InId = x.InId, Name = x.Name, Uri = x.Uri, Type = x.Type, SchemaType = x.SchemaType, Schema = x.Schema, CreationUserId = x.CreationUserId, CreationUserName = x.CreationUserName, CreationTime = x.CreationTime, CreationTimestamp = x.CreationTimestamp, ChangeUserId = x.ChangeUserId, ChangeUserName = x.ChangeUserName, ChangeTime = x.ChangeTime, ChangeTimestamp = x.ChangeTimestamp, Version = x.Version, Description = x.Description, Keywords = x.Keywords, Workflow = x.Workflow, WorkflowState = x.WorkflowState, IsMarkedDeleted = x.IsMarkedDeleted, IsHidden = x.IsHidden, IsReadOnly = x.IsReadOnly, IsSystem = x.IsSystem, ContentType = x.ContentType, StorageType = x.StorageType, StorageId = x.StorageId, StorageUri = x.StorageUri, ResourceSystem = x.ResourceSystem, SolrQuery = x.SolrQuery }); 
        }

        public CqlQuerySingleElement<ResourceMetadataSolrEntry> ReadHasChildren(string uri, string resourceType, List<Guid> useridentities, Permissions perm = Permissions.Read)
        {
            var metaIsDeleted = _resourceMetadataSolrEntry.GetColumnName(x => x.IsMarkedDeleted);
            var resCd = _ascendantsResourceSolrEntry.GetColumnName(x => x.CurveDistance);
            var ascResUri = _ascendantsResourceSolrEntry.GetColumnName(x => x.AscendantUri);

            AbstractSolrQuery basicQuery = new SolrQueryByField(metaIsDeleted, bool.FalseString);
            if (!string.IsNullOrWhiteSpace(resourceType))
            {
                var metaType = _resourceMetadataSolrEntry.GetColumnName(x => x.Type);
                resourceType.Split(',');
                if(resourceType.Length >1)
                    basicQuery = basicQuery && new SolrQueryInList(metaType, resourceType.Split(','));
                else
                {
                    basicQuery = basicQuery && new SolrQueryByField(metaType, resourceType);
                }
            }
            
            var filterQueries = new List<FilterQuery>();

            var filterByUri = new FilterQuery(string.Format("{0}.{1}", KeySpace, _ascendantsResourceSolrEntry.GetTableName),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryByField(ascResUri, uri) { EscapeValue = true, Quoted = false },
                        new SolrQueryByField(resCd, "-1")));

            filterQueries.Add(filterByUri);

            if (!PermissionService.IsWhitelisted(useridentities))
            {
                var identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                var permission = "perm";//_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter = new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                        new SolrQueryByField(permission, Convert.ToString((int)perm))));
                filterQueries.Add(acdFilter);
            }
            var solrQuery = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), null);

            return SchemaContainer.GetTable<ResourceMetadataSolrEntry>().Where(x => x.SolrQuery == solrQuery).Select(x => new ResourceMetadataSolrEntry
            { OutId = x.OutId, InId = x.InId, Name = x.Name, Uri = x.Uri, Type = x.Type, SchemaType = x.SchemaType, Schema = x.Schema, CreationUserId = x.CreationUserId, CreationUserName = x.CreationUserName, CreationTime = x.CreationTime, CreationTimestamp = x.CreationTimestamp, ChangeUserId = x.ChangeUserId, ChangeUserName = x.ChangeUserName, ChangeTime = x.ChangeTime, ChangeTimestamp = x.ChangeTimestamp, Version = x.Version, Description = x.Description, Keywords = x.Keywords, Workflow = x.Workflow, WorkflowState = x.WorkflowState, IsMarkedDeleted = x.IsMarkedDeleted, IsHidden = x.IsHidden, IsReadOnly = x.IsReadOnly, IsSystem = x.IsSystem, ContentType = x.ContentType, StorageType = x.StorageType, StorageId = x.StorageId, StorageUri = x.StorageUri, ResourceSystem = x.ResourceSystem, SolrQuery = x.SolrQuery }).FirstOrDefault();
        }

        public CqlQuery<ResourceMetadataSolrEntry> ReadPaged(string resourceUri, List<Guid> useridentities, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            var resCd = _ascendantsResourceSolrEntry.GetColumnName(x => x.CurveDistance);
            var ascResUri = _ascendantsResourceSolrEntry.GetColumnName(x => x.AscendantUri);

            var sortOrder = GetSortOrder(query);
            var toCd = GetCurveDistanceTo(startCd);
            var fromCd = GetCurveDistanceFrom(endCd);

            AbstractSolrQuery basicQuery = new SolrQuery("");
            if (query == null || query.Filters == null || !query.Filters.Any(x => x.HasKey(MetadataCondition.Keys.IsDeleted.ToString())))
            {
                var metaIsDeleted = _resourceMetadataSolrEntry.GetColumnName(x => x.IsMarkedDeleted);
                basicQuery = new SolrQueryByField(metaIsDeleted, bool.FalseString);
            }
            if (!string.IsNullOrWhiteSpace(resourceType))
            {
                var metaType = _resourceMetadataSolrEntry.GetColumnName(x => x.Type);
                basicQuery = basicQuery && new SolrQueryByField(metaType, resourceType);
            }

            ISolrQuery propertyFilterCondition = null;
            int? start = null;
            if (query != null)
            {
                if (query.Filters != null)
                {
                    var metaQuries = query.Filters.Where(x => x.Category == ConditionCategory.Metadata);
                    foreach (var condition in metaQuries)
                    {
                        basicQuery = basicQuery && condition.BuidQuery();
                    }
                    var propertyQuries = query.Filters.Where(x => x.Category == ConditionCategory.Property).ToList();
                    if (propertyQuries.Any())
                    {
                        propertyFilterCondition = new SolrMultipleCriteriaQuery(propertyQuries.Select(x => x.BuidQuery()), AbstractSolrQuery.Operator.AND);
                    }
                }
                start = Math.Max(0, (query.PageNumber - 1) * query.PageSize);
            }

            var filterQueries = new List<FilterQuery>();

            var filterByUri = new FilterQuery(string.Format("{0}.{1}", KeySpace, _ascendantsResourceSolrEntry.GetTableName),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryByField(ascResUri, resourceUri) { EscapeValue = true, Quoted = false },
                        new SolrQueryByRange<short?>(resCd, fromCd, toCd)));

            filterQueries.Add(filterByUri);

            if (propertyFilterCondition != null)
            {
                filterQueries.Add(new FilterQuery(string.Format("{0}.{1}", KeySpace, _propertySolrEntryDefinition.GetTableName),
                        propertyFilterCondition));
            }
            if (!PermissionService.IsWhitelisted(useridentities))
            {
                var identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                var permission ="perm";//_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter = new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                        new SolrQueryByField(permission, Convert.ToString((int)perm))));
                filterQueries.Add(acdFilter);
            }
            var solrQuery = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), start, sortOrder);
            //Console.WriteLine(solrQuery);

            return SchemaContainer.GetTable<ResourceMetadataSolrEntry>().Where(x => x.SolrQuery == solrQuery).Select(x => new ResourceMetadataSolrEntry
            { OutId = x.OutId, InId = x.InId, Name = x.Name, Uri = x.Uri, Type = x.Type, SchemaType = x.SchemaType, Schema = x.Schema, CreationUserId = x.CreationUserId, CreationUserName = x.CreationUserName, CreationTime = x.CreationTime, CreationTimestamp = x.CreationTimestamp, ChangeUserId = x.ChangeUserId, ChangeUserName = x.ChangeUserName, ChangeTime = x.ChangeTime, ChangeTimestamp = x.ChangeTimestamp, Version = x.Version, Description = x.Description, Keywords = x.Keywords, Workflow = x.Workflow, WorkflowState = x.WorkflowState, IsMarkedDeleted = x.IsMarkedDeleted, IsHidden = x.IsHidden, IsReadOnly = x.IsReadOnly, IsSystem = x.IsSystem, ContentType = x.ContentType, StorageType = x.StorageType, StorageId = x.StorageId, StorageUri = x.StorageUri, ResourceSystem = x.ResourceSystem, SolrQuery = x.SolrQuery }); 
        }

        public Tuple<CqlScalar<long>, CqlQuery<ResourceMetadataSolrEntry>> ReadDescendantsPaged(List<Guid> useridentities, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null, bool skipAclCheck = false, params string[] resourceUris)
        {
            //var metaIsDeleted = _resourceMetadataSolrEntry.GetColumnName(x => x.IsMarkedDeleted);
            var resCd = _ascendantsResourceSolrEntry.GetColumnName(x => x.CurveDistance);
            var ascResUri = _ascendantsResourceSolrEntry.GetColumnName(x => x.AscendantUri);

            var sortOrder = GetSortOrder(query);
            var toCd = GetCurveDistanceTo(startCd);
            var fromCd = GetCurveDistanceFrom(endCd);

            AbstractSolrQuery basicQuery = new SolrQuery("");
            if (query == null || query.Filters == null || !query.Filters.Any(x => x.HasKey(MetadataCondition.Keys.IsDeleted.ToString())))
            {
                var metaIsDeleted = _resourceMetadataSolrEntry.GetColumnName(x => x.IsMarkedDeleted);
                basicQuery = new SolrQueryByField(metaIsDeleted, bool.FalseString);
            }
            if (!string.IsNullOrWhiteSpace(resourceType))
            {
                var metaType = _resourceMetadataSolrEntry.GetColumnName(x => x.Type);
                basicQuery = basicQuery && new SolrQueryByField(metaType, resourceType);
            }
            

            ISolrQuery propertyFilterCondition = null;
            int? start = null;
            if (query != null)
            {
                if (query.Filters != null)
                {
                    var metaQuries = query.Filters.Where(x => x.Category == ConditionCategory.Metadata);
                    foreach (var condition in metaQuries)
                    {
                        basicQuery = basicQuery && condition.BuidQuery();
                    }
                    var propertyQuries = query.Filters.Where(x => x.Category == ConditionCategory.Property).ToList();
                    if (propertyQuries.Any())
                    {
                        propertyFilterCondition = new SolrMultipleCriteriaQuery(propertyQuries.Select(x => x.BuidQuery()), AbstractSolrQuery.Operator.AND);
                    }
                }

                start = Math.Max(0, (query.PageNumber - 1) * query.PageSize);
            }

            var filterQueries = new List<FilterQuery>();

            var filterByUri = new FilterQuery(string.Format("{0}.{1}", KeySpace, _ascendantsResourceSolrEntry.GetTableName),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        //new SolrQueryByField(ascResUri, resourceUri) { EscapeValue = true, Quoted = false },
                        new SolrQueryInList(ascResUri, resourceUris),
                        new SolrQueryByRange<short?>(resCd, fromCd, toCd)));

            filterQueries.Add(filterByUri);

            if (propertyFilterCondition != null)
            {
                filterQueries.Add(new FilterQuery(string.Format("{0}.{1}", KeySpace, _propertySolrEntryDefinition.GetTableName),
                        propertyFilterCondition));
            }

            var shouldJoinWithAcdTable = !skipAclCheck && !PermissionService.IsWhitelisted(useridentities);
            if (shouldJoinWithAcdTable)
            {
                var identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                var permission = "perm";//_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter = new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                        new SolrQueryByField(permission, Convert.ToString((int)perm))));
                filterQueries.Add(acdFilter);
            }

            var solrQueryTotal = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), null, sortOrder);
            var solrQueryWithStart = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), start, sortOrder);

            var totalCountQuery = SchemaContainer.GetTable<ResourceMetadataSolrEntry>().Where(x => x.SolrQuery == solrQueryTotal).Count();
            var resultQuery =
                SchemaContainer.GetTable<ResourceMetadataSolrEntry>()
                    .Where(x => x.SolrQuery == solrQueryWithStart)
                    .Select(x => new ResourceMetadataSolrEntry
                    {
                        OutId = x.OutId,
                        InId = x.InId,
                        Name = x.Name,
                        Uri = x.Uri,
                        Type = x.Type,
                        SchemaType = x.SchemaType,
                        Schema = x.Schema,
                        CreationUserId = x.CreationUserId,
                        CreationUserName = x.CreationUserName,
                        CreationTime = x.CreationTime,
                        CreationTimestamp = x.CreationTimestamp,
                        ChangeUserId = x.ChangeUserId,
                        ChangeUserName = x.ChangeUserName,
                        ChangeTime = x.ChangeTime,
                        ChangeTimestamp = x.ChangeTimestamp,
                        Version = x.Version,
                        Description = x.Description,
                        Keywords = x.Keywords,
                        Workflow = x.Workflow,
                        WorkflowState = x.WorkflowState,
                        IsMarkedDeleted = x.IsMarkedDeleted,
                        IsHidden = x.IsHidden,
                        IsReadOnly = x.IsReadOnly,
                        IsSystem = x.IsSystem,
                        ContentType = x.ContentType,
                        StorageType = x.StorageType,
                        StorageId = x.StorageId,
                        StorageUri = x.StorageUri,
                        ResourceSystem = x.ResourceSystem,
                        SolrQuery = x.SolrQuery
                    });
             return Tuple.Create(totalCountQuery, resultQuery);
        }

        public Tuple<CqlScalar<long>, CqlQuery<ResourceMetadataSolrEntry>> ReadRecentDescendantsPaged(string resourceUri, List<Guid> useridentities, int noOfDays, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            //var metaIsDeleted = _resourceMetadataSolrEntry.GetColumnName(x => x.IsMarkedDeleted);
            var createdDate = _resourceMetadataSolrEntry.GetColumnName(x => x.CreationTimestamp);
            var resCd = _ascendantsResourceSolrEntry.GetColumnName(x => x.CurveDistance);
            var ascResUri = _ascendantsResourceSolrEntry.GetColumnName(x => x.AscendantUri);

            var sortOrder = GetSortOrder(query);

            var toCd = GetCurveDistanceTo(startCd);
            var fromCd = GetCurveDistanceFrom(endCd);

            var toDate = DateTimeOffset.UtcNow.Date.AddDays(1).AddTicks(-1);
            var fromDate = toDate.AddDays(-noOfDays).Date;

            AbstractSolrQuery basicQuery = new SolrQuery("");
            if (query == null || query.Filters == null || !query.Filters.Any(x => x.HasKey(MetadataCondition.Keys.IsDeleted.ToString())))
            {
                var metaIsDeleted = _resourceMetadataSolrEntry.GetColumnName(x => x.IsMarkedDeleted);
                basicQuery = new SolrQueryByField(metaIsDeleted, bool.FalseString);
            }
            basicQuery = basicQuery && new SolrQueryByStringRange<string>(createdDate, fromDate.ToString("s") + "Z", toDate.ToString("s") + "Z");
            if (!string.IsNullOrWhiteSpace(resourceType))
            {
                var metaType = _resourceMetadataSolrEntry.GetColumnName(x => x.Type);
                basicQuery = basicQuery && new SolrQueryByField(metaType, resourceType);
            }

            ISolrQuery propertyFilterCondition = null;
            int? start = null;
            if (query != null)
            {
                if (query.Filters != null)
                {
                    var metaQuries = query.Filters.Where(x => x.Category == ConditionCategory.Metadata);
                    foreach (var condition in metaQuries)
                    {
                        basicQuery = basicQuery && condition.BuidQuery();
                    }
                    var propertyQuries = query.Filters.Where(x => x.Category == ConditionCategory.Property).ToList();
                    if (propertyQuries.Any())
                    {
                        propertyFilterCondition = new SolrMultipleCriteriaQuery(propertyQuries.Select(x => x.BuidQuery()), AbstractSolrQuery.Operator.AND);
                    }
                }
                start = Math.Max(0, (query.PageNumber - 1) * query.PageSize);
            }

            var filterQueries = new List<FilterQuery>();

            var filterByUri = new FilterQuery(string.Format("{0}.{1}", KeySpace, _ascendantsResourceSolrEntry.GetTableName),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryByField(ascResUri, resourceUri) { EscapeValue = true, Quoted = false },
                        new SolrQueryByRange<short?>(resCd, fromCd, toCd)));

            filterQueries.Add(filterByUri);

            if (propertyFilterCondition != null)
            {
                filterQueries.Add(new FilterQuery(string.Format("{0}.{1}", KeySpace, _propertySolrEntryDefinition.GetTableName),
                        propertyFilterCondition));
            }
            if (!PermissionService.IsWhitelisted(useridentities))
            {
                var identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                var permission = "perm";//_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter = new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                        new SolrQueryByField(permission, Convert.ToString((int)perm))));
                filterQueries.Add(acdFilter);
            }
            var solrQueryTotal = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), null, sortOrder);
            var solrQueryWithStart = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), start,sortOrder);

            var totalCountQuery = SchemaContainer.GetTable<ResourceMetadataSolrEntry>().Where(x => x.SolrQuery == solrQueryTotal).Count();
            var resultQuery =
                SchemaContainer.GetTable<ResourceMetadataSolrEntry>()
                    .Where(x => x.SolrQuery == solrQueryWithStart)
                    .Select(x => new ResourceMetadataSolrEntry
                    {
                        OutId = x.OutId,
                        InId = x.InId,
                        Name = x.Name,
                        Uri = x.Uri,
                        Type = x.Type,
                        SchemaType = x.SchemaType,
                        Schema = x.Schema,
                        CreationUserId = x.CreationUserId,
                        CreationUserName = x.CreationUserName,
                        CreationTime = x.CreationTime,
                        CreationTimestamp = x.CreationTimestamp,
                        ChangeUserId = x.ChangeUserId,
                        ChangeUserName = x.ChangeUserName,
                        ChangeTime = x.ChangeTime,
                        ChangeTimestamp = x.ChangeTimestamp,
                        Version = x.Version,
                        Description = x.Description,
                        Keywords = x.Keywords,
                        Workflow = x.Workflow,
                        WorkflowState = x.WorkflowState,
                        IsMarkedDeleted = x.IsMarkedDeleted,
                        IsHidden = x.IsHidden,
                        IsReadOnly = x.IsReadOnly,
                        IsSystem = x.IsSystem,
                        ContentType = x.ContentType,
                        StorageType = x.StorageType,
                        StorageId = x.StorageId,
                        StorageUri = x.StorageUri,
                        ResourceSystem = x.ResourceSystem,
                        SolrQuery = x.SolrQuery
                    });
            return Tuple.Create(totalCountQuery, resultQuery);
        }

        public CqlQuery<ResourceMetadataSolrEntry> ReadWithIds(string resourceUri, List<Guid> outIds, List<Guid> useridentities, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            var resCd = _ascendantsResourceSolrEntry.GetColumnName(x => x.CurveDistance);
            var ascResUri = _ascendantsResourceSolrEntry.GetColumnName(x => x.AscendantUri);
            var resOutId = _resourceMetadataSolrEntry.GetColumnName(x => x.OutId);
            
            var toCd = GetCurveDistanceTo(startCd);
            var fromCd = GetCurveDistanceFrom(endCd);

            AbstractSolrQuery basicQuery = new SolrQuery("");
            if (query == null || query.Filters == null || !query.Filters.Any(x => x.HasKey(MetadataCondition.Keys.IsDeleted.ToString())))
            {
                var metaIsDeleted = _resourceMetadataSolrEntry.GetColumnName(x => x.IsMarkedDeleted);
                basicQuery = new SolrQueryByField(metaIsDeleted, bool.FalseString);
            }
            if (!string.IsNullOrWhiteSpace(resourceType))
            {
                var metaType = _resourceMetadataSolrEntry.GetColumnName(x => x.Type);
                basicQuery = basicQuery && new SolrQueryByField(metaType, resourceType);
            }

            if(outIds.Any())
            {
                basicQuery = basicQuery && new SolrQueryInList(resOutId, outIds.Select(x => x.ToString()));
            }

            if (query != null && query.Filters != null)
            {
                var metaQuries = query.Filters.Where(x => x.Category == ConditionCategory.Metadata);
                basicQuery = metaQuries.Aggregate(basicQuery, (current, condition) => current && condition.BuidQuery());
            }

            var filterQueries = new List<FilterQuery>();

            var filterByUri = new FilterQuery(string.Format("{0}.{1}", KeySpace, _ascendantsResourceSolrEntry.GetTableName),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryByField(ascResUri, resourceUri) { EscapeValue = true, Quoted = false },
                        new SolrQueryByRange<short?>(resCd, fromCd, toCd)));

            filterQueries.Add(filterByUri);

            if (!PermissionService.IsWhitelisted(useridentities))
            {
                const string identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                const string permission = "perm"; //_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter = new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                        new SolrQueryByField(permission, Convert.ToString((int)perm))));
                filterQueries.Add(acdFilter);
            }
            var solrQuery = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), 0);
            //Console.WriteLine(solrQuery);

            var resultQuery =
                SchemaContainer.GetTable<ResourceMetadataSolrEntry>()
                    .Where(x => x.SolrQuery == solrQuery)
                    .Select(x => new ResourceMetadataSolrEntry
                    {
                        OutId = x.OutId,
                        InId = x.InId,
                        Name = x.Name,
                        Uri = x.Uri,
                        Type = x.Type,
                        SchemaType = x.SchemaType,
                        Schema = x.Schema,
                        CreationUserId = x.CreationUserId,
                        CreationUserName = x.CreationUserName,
                        CreationTime = x.CreationTime,
                        CreationTimestamp = x.CreationTimestamp,
                        ChangeUserId = x.ChangeUserId,
                        ChangeUserName = x.ChangeUserName,
                        ChangeTime = x.ChangeTime,
                        ChangeTimestamp = x.ChangeTimestamp,
                        Version = x.Version,
                        Description = x.Description,
                        Keywords = x.Keywords,
                        Workflow = x.Workflow,
                        WorkflowState = x.WorkflowState,
                        IsMarkedDeleted = x.IsMarkedDeleted,
                        IsHidden = x.IsHidden,
                        IsReadOnly = x.IsReadOnly,
                        IsSystem = x.IsSystem,
                        ContentType = x.ContentType,
                        StorageType = x.StorageType,
                        StorageId = x.StorageId,
                        StorageUri = x.StorageUri,
                        ResourceSystem = x.ResourceSystem,
                        SolrQuery = x.SolrQuery
                    });
            return resultQuery;
        }

        public CqlQuery<ResourceMetadataSolrEntry> ReadRecentWithIds(string resourceUri, List<Guid> outIds, List<Guid> useridentities, int noOfDays, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            var resCd = _ascendantsResourceSolrEntry.GetColumnName(x => x.CurveDistance);
            var ascResUri = _ascendantsResourceSolrEntry.GetColumnName(x => x.AscendantUri);
            var resOutId = _resourceMetadataSolrEntry.GetColumnName(x => x.OutId);
            var createdDate = _resourceMetadataSolrEntry.GetColumnName(x => x.CreationTimestamp);

            var toCd = GetCurveDistanceTo(startCd);
            var fromCd = GetCurveDistanceFrom(endCd);

            AbstractSolrQuery basicQuery = new SolrQuery("");
            if (query == null || query.Filters == null || !query.Filters.Any(x => x.HasKey(MetadataCondition.Keys.IsDeleted.ToString())))
            {
                var metaIsDeleted = _resourceMetadataSolrEntry.GetColumnName(x => x.IsMarkedDeleted);
                basicQuery = new SolrQueryByField(metaIsDeleted, bool.FalseString);
            }
            var toDate = DateTimeOffset.UtcNow.Date.AddDays(1).AddTicks(-1);
            var fromDate = toDate.AddDays(-noOfDays).Date;

            basicQuery = basicQuery && new SolrQueryByStringRange<string>(createdDate, fromDate.ToString("s") + "Z", toDate.ToString("s") + "Z");
            if (!string.IsNullOrWhiteSpace(resourceType))
            {
                var metaType = _resourceMetadataSolrEntry.GetColumnName(x => x.Type);
                basicQuery = basicQuery && new SolrQueryByField(metaType, resourceType);
            }

            if (outIds.Any())
            {
                basicQuery = basicQuery && new SolrQueryInList(resOutId, outIds.Select(x => x.ToString()));
            }

            if (query != null && query.Filters != null)
            {
                var metaQuries = query.Filters.Where(x => x.Category == ConditionCategory.Metadata);
                basicQuery = metaQuries.Aggregate(basicQuery, (current, condition) => current && condition.BuidQuery());
            }

            var filterQueries = new List<FilterQuery>();

            var filterByUri = new FilterQuery(string.Format("{0}.{1}", KeySpace, _ascendantsResourceSolrEntry.GetTableName),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryByField(ascResUri, resourceUri) { EscapeValue = true, Quoted = false },
                        new SolrQueryByRange<short?>(resCd, fromCd, toCd)));

            filterQueries.Add(filterByUri);

            if (!PermissionService.IsWhitelisted(useridentities))
            {
                const string identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                const string permission = "perm"; //_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter = new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                        new SolrQueryByField(permission, Convert.ToString((int)perm))));
                filterQueries.Add(acdFilter);
            }
            var solrQuery = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), 0);
            //Console.WriteLine(solrQuery);

            var resultQuery =
                 SchemaContainer.GetTable<ResourceMetadataSolrEntry>()
                     .Where(x => x.SolrQuery == solrQuery)
                     .Select(x => new ResourceMetadataSolrEntry
                     {
                         OutId = x.OutId,
                         InId = x.InId,
                         Name = x.Name,
                         Uri = x.Uri,
                         Type = x.Type,
                         SchemaType = x.SchemaType,
                         Schema = x.Schema,
                         CreationUserId = x.CreationUserId,
                         CreationUserName = x.CreationUserName,
                         CreationTime = x.CreationTime,
                         CreationTimestamp = x.CreationTimestamp,
                         ChangeUserId = x.ChangeUserId,
                         ChangeUserName = x.ChangeUserName,
                         ChangeTime = x.ChangeTime,
                         ChangeTimestamp = x.ChangeTimestamp,
                         Version = x.Version,
                         Description = x.Description,
                         Keywords = x.Keywords,
                         Workflow = x.Workflow,
                         WorkflowState = x.WorkflowState,
                         IsMarkedDeleted = x.IsMarkedDeleted,
                         IsHidden = x.IsHidden,
                         IsReadOnly = x.IsReadOnly,
                         IsSystem = x.IsSystem,
                         ContentType = x.ContentType,
                         StorageType = x.StorageType,
                         StorageId = x.StorageId,
                         StorageUri = x.StorageUri,
                         ResourceSystem = x.ResourceSystem,
                         SolrQuery = x.SolrQuery
                     });
            return resultQuery;
      
        }
       
        private static short? GetCurveDistanceFrom(short cd)
        {
            short negate = -1;
            if (cd != short.MaxValue)
            {
                return (short)(Math.Abs(cd) * negate);
            }
            return null;
        }

        private static short? GetCurveDistanceTo(short cd)
        {
            short negate = -1;
            return (short)(Math.Abs(cd) * negate);
        }

        private SolrSortOrder GetSortOrder(QueryOptions query)
        {
            if (query == null) return null;
            string sortBy;
            switch (query.SortBy)
            {
                case SortBy.Name:
                    sortBy = _resourceMetadataSolrEntry.GetColumnName(x => x.Name);
                    break;
                case SortBy.Name_Sort:
                    sortBy = _resourceMetadataSolrEntry.GetColumnName(x => x.Name_Sort);
                    break;
                case SortBy.CreatedDate:
                    sortBy = _resourceMetadataSolrEntry.GetColumnName(x => x.CreationTimestamp);
                    break;
                case SortBy.LastUpdatedDate:
                    sortBy = _resourceMetadataSolrEntry.GetColumnName(x => x.ChangeTimestamp);
                    break;
                case SortBy.Type:
                    sortBy = _resourceMetadataSolrEntry.GetColumnName(x => x.Type);
                    break;
                default:
                {
                    sortBy = _resourceMetadataSolrEntry.GetColumnName(x => x.Name);
                    query.SortBy=SortBy.Name;
                    break;
                }
            }
            return new SolrSortOrder(sortBy, query.SortOrder == SortOrder.Ascending ? Order.ASC : Order.DESC);
        }
    }
}
