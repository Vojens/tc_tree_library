﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Resource.Cassandra.Cql;

namespace Infrastructure.Graph.Repository.Resource.Cassandra
{
    internal class HardlinkCounterCassandraRepository : CassandraRepository
    {
        protected HardlinkCounterCqlRepository CqlRepository;
        public HardlinkCounterCassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new HardlinkCounterCqlRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Update(HardLinkCounterEntry entry)
        {
            var cqlUpdate = (CqlUpdate)CqlRepository.Update(entry).Configure(StatementOptions.Upsert, ConnectionProvider).SetIdempotence(false);
            return cqlUpdate.ExecuteAsync();
        }

        public async Task<HardLinkCounterEntry> Get(Guid resourceId)
        {
            var hardLinkCounterEntry = await CqlRepository.Get(resourceId).FirstOrDefault().ConfigureAndExecuteAsync(ConnectionProvider).ConfigureAwait(false);
            if (hardLinkCounterEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(String.Format("hardlinkcounter with entry id {0} is not found", resourceId));
            }
            return hardLinkCounterEntry;
        }

        public async Task<bool> TryGet(Guid resourceId, Ref<HardLinkCounterEntry> @ref)
        {
            var hardLinkCounterEntry = await CqlRepository.Get(resourceId).FirstOrDefault().ConfigureAndExecuteAsync(ConnectionProvider).ConfigureAwait(false);
            if (hardLinkCounterEntry.IsNullOrDefault())
            {
                return false;
            }
            @ref.Value = hardLinkCounterEntry;
            return true;
        }
    }
}
