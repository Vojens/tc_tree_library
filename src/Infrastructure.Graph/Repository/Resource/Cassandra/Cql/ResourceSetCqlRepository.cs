using System;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Repository.Resource.Cassandra.Cql
{
    internal class ResourceSetCqlRepository : CassandraRepository
    {
        public ResourceSetCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {

        }

        public CqlInsert<ResourceSetEntry> Add(ResourceSetEntry resourceSetEntry)
        {
            var resourceSetEntries = SchemaContainer.GetTable<ResourceSetEntry>();//new Table<ResourceSetEntry>(ConnectionProvider.Session.SessionAdaptee);
            var cqlInsert = resourceSetEntries.Insert(resourceSetEntry, false);
            return cqlInsert;
        }

        public CqlInsert<ResourceSetObjectEntry> Add(ResourceSetObjectEntry resourceSetEntry)
        {
            return SchemaContainer.GetTable<ResourceSetObjectEntry>().Insert(resourceSetEntry, false);
        }

        public CqlQuery<ResourcesByCurveDistanceEntry> Read(string outUri, short curveDistance, int curveDistanceBucket)
        {
            return SchemaContainer.GetTable<ResourcesByCurveDistanceEntry>()
                .Where(
                    x =>
                        x.OutUri == outUri && x.CurveDistance == curveDistance &&
                        x.CurveDistanceBucket == curveDistanceBucket);
        }

        public CqlQuery<ResourcesByCurveDistanceEntry> Get(string uri)
        {
            short curveDistance = 0;
            return SchemaContainer.GetTable<ResourcesByCurveDistanceEntry>()
                .Where(c => c.OutUri == uri && c.CurveDistance == curveDistance && c.CurveDistanceBucket == 0);
        }

        public CqlQuerySingleElement<ResourcesByCurveDistanceEntry> GetFirstOrDefault(string uri)
        {
            const short curveDistance = 0;
            var resourcesByCurveDistanceEntries = SchemaContainer.GetTable<ResourcesByCurveDistanceEntry>();
            var byCurveDistanceEntries = resourcesByCurveDistanceEntries.Where(
                c =>
                    c.OutUri == uri && c.CurveDistance == curveDistance &&
                    c.CurveDistanceBucket == 0);
            return
                byCurveDistanceEntries.FirstOrDefault();
        }

        public CqlQuery<ResourceSetEntry> Get(Guid id)
        {
            const short curveDistance = 0;
            return Get(id, curveDistance);
        }


        //not for public usage. it won't give you what you think it would
        private CqlQuery<ResourceSetEntry> Get(Guid id, short curveDistance)
        {
            return
                SchemaContainer.GetTable<ResourceSetEntry>()
                    .Where(c => c.OutId == id && c.InId == id && c.CurveDistance == curveDistance);
        }

        //        public CqlDelete Delete(ResourceSetEntry entry)
        //        {
        //            SchemaContainer.GetTable<ResourceSetEntry>().Select(c => c).Where(c => c.OutId == entry.OutId && c.InId == entry.InId && )
        //        }
    }
}