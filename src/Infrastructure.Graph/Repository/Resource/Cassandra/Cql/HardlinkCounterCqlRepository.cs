﻿using System;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Repository.Resource.Cassandra.Cql
{
    internal class HardlinkCounterCqlRepository : CassandraRepository
    {
        public HardlinkCounterCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
        }

        public CqlUpdate Update(HardLinkCounterEntry entry)
        {
            return SchemaContainer.GetTable<HardLinkCounterEntry>()
                .Where(c => c.ResourceId == entry.ResourceId)
                .Select(c => new HardLinkCounterEntry { Counter = entry.Counter }).Update();
        }

        public CqlQuery<HardLinkCounterEntry> Get(Guid resourceId)
        {
            return SchemaContainer.GetTable<HardLinkCounterEntry>().Where(c => c.ResourceId == resourceId);
        }
    }
}
