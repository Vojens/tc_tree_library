using System;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Repository.Resource.Cassandra.Cql
{
    internal class ResourceSetCounterCqlRepository : CassandraRepository
    {
        public ResourceSetCounterCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
        }

        public CqlUpdate Update(ResourceSetCounterEntry entry)
        {
            var cqlUpdate = SchemaContainer.GetTable<ResourceSetCounterEntry>()
                .Where(c => c.OutId == entry.OutId && c.InId == entry.InId && c.CurveDistance == entry.CurveDistance)
                .Select(c => new ResourceSetCounterEntry { Counter = entry.Counter })
                .Update();
            return cqlUpdate;
        }

        public CqlQuery<ResourceSetCounterEntry> Read(Guid id)
        {
            return SchemaContainer.GetTable<ResourceSetCounterEntry>()
                .Where(c => c.OutId == id && c.InId == id)
                .Select(c => new ResourceSetCounterEntry
                {
                    OutId = id,
                    InId = id,
                    CurveDistance = c.CurveDistance,
                    Counter = c.Counter
                });
        }
    }
}