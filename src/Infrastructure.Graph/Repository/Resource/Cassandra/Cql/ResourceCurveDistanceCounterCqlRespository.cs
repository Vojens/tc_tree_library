﻿using System;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Repository.Resource.Cassandra.Cql
{
    internal class ResourceCurveDistanceCounterCqlRespository : CassandraRepository
    {
        public ResourceCurveDistanceCounterCqlRespository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
        }

        public CqlUpdate Update(ResourceCruveDistanceCounterEntry entry)
        {

            return SchemaContainer.GetTable<ResourceCruveDistanceCounterEntry>()
                .Where(
                    c => c.OutUri == entry.OutUri && c.CurveDistance == entry.CurveDistance && c.Bucket == entry.Bucket)
                .Select(c => new ResourceCruveDistanceCounterEntry { Counter = entry.Counter })
                .Update();
        }

        public CqlQuery<ResourceCruveDistanceCounterEntry> Read(string outUri, short curveDistance)
        {
            return SchemaContainer.GetTable<ResourceCruveDistanceCounterEntry>()
                .Where(c => c.OutUri == outUri && c.CurveDistance == curveDistance)
                .Select(c => new ResourceCruveDistanceCounterEntry
                {
                    OutUri = outUri,
                    CurveDistance = curveDistance,
                    Bucket = c.Bucket,
                    Counter = c.Counter
                });
        }
    }
}
