﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Repository.Resource.Cassandra.Cql
{
    internal class AscendantsByResourceCqlRepository : CassandraRepository
    {
        public AscendantsByResourceCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer) : base(connectionProvider, schemaContainer)
        {
        }

        public CqlInsert<AscendantsByResourceEntry> Add(AscendantsByResourceEntry entry)
        {
            var resourceSetEntries = SchemaContainer.GetTable<AscendantsByResourceEntry>();
            var cqlInsert = resourceSetEntries.Insert(entry, false);
            return cqlInsert;
        }

        public CqlQuery<AscendantsByResourceEntry> Read(Guid outId, Guid inId)
        {
            return SchemaContainer.GetTable<AscendantsByResourceEntry>()
                .Where(
                    x =>
                        x.OutId == outId && x.InId == inId);
        }

        public CqlQuery<AscendantsByResourceEntry> Read(Guid outId, Guid inId, short curveDistance)
        {
            return SchemaContainer.GetTable<AscendantsByResourceEntry>()
                .Where(
                    c =>
                        c.OutId == outId && c.InId == inId && c.CurveDistance == curveDistance);
        }
    }
}
