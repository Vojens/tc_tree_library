﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Resource.Cassandra.Cql;

namespace Infrastructure.Graph.Repository.Resource.Cassandra
{
    internal class AscendantsByResourceCassandraRepository : CassandraRepository
    {
        internal AscendantsByResourceCqlRepository CqlRepository;
        public AscendantsByResourceCassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer) : base(connectionProvider, schemaContainer)
        {
            CqlRepository = new AscendantsByResourceCqlRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(AscendantsByResourceEntry entry)
        {
            return CqlRepository.Add(entry).ConfigureAndExecuteAsync(ConnectionProvider);
        }

        public IObservable<AscendantsByResourceEntry> Read(Guid outId, Guid inId)
        {
            return Observable.Create<AscendantsByResourceEntry>(async o =>
            {
                try
                {
                    var enumerable = await DbMapper.FetchAsync<AscendantsByResourceEntry>("WHERE outid = ? AND inid = ?", outId, inId).ConfigureAwait(false);

                    foreach (var entry in enumerable)
                    {
                        o.OnNext(entry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AscendantsByResourceEntry> Read(Guid outId, Guid inId, short curveDistance)
        {
            return Observable.Create<AscendantsByResourceEntry>(async o =>
            {
                try
                {
                    var enumerable = await DbMapper.FetchAsync<AscendantsByResourceEntry>("WHERE outid = ? AND inid = ? AND cd = ?", outId, inId, curveDistance).ConfigureAwait(false);

                    foreach (var entry in enumerable)
                    {
                        o.OnNext(entry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public Task Delete(Guid outId, Guid inId)
        {
            return DbMapper.DeleteAsync<AscendantsByResourceEntry>("WHERE outid = ? AND inid = ?", outId, inId);
        }

        public Task Delete(Guid outId, Guid inId, short curveDistance, Guid ascendantId)
        {
            return DbMapper.DeleteAsync<AscendantsByResourceEntry>("WHERE outid = ? AND inid = ? AND cd = ? AND aid = ?", outId, inId, curveDistance, ascendantId);
        }

        public Task Delete(Guid outId, Guid inId, IEnumerable<Tuple<short, Guid>> list)
        {
            var cqlBatch = DbMapper.CreateBatch();
            foreach (var tuple in list)
            {
                cqlBatch.Delete<AscendantsByResourceEntry>("WHERE outid = ? AND inid = ? AND cd = ? AND aid = ?", outId, inId, tuple.Item1, tuple.Item2);
            }
            return DbMapper.ExecuteAsync(cqlBatch);
        }
    }
}
