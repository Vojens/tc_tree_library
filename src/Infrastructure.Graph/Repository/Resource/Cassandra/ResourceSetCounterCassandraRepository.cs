using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Resource.Cassandra.Cql;
using CqlQueryExtensions = Infrastructure.ConnectionProvider.Extensions.CqlQueryExtensions;

namespace Infrastructure.Graph.Repository.Resource.Cassandra
{
    internal class ResourceSetCounterCassandraRepository : CassandraRepository
    {
        protected ResourceSetCounterCqlRepository CqlRespository { get; set; }

        public ResourceSetCounterCassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRespository = new ResourceSetCounterCqlRepository(connectionProvider, schemaContainer);
        }

        public Task Update(ResourceSetCounterEntry entry)
        {
            CqlUpdate cqlUpdate = (CqlUpdate)CqlRespository.Update(entry).Configure(StatementOptions.Upsert, ConnectionProvider).SetIdempotence(false);
            return cqlUpdate.ExecuteAsync();
        }

        public IObservable<ResourceSetCounterEntry> Read(Guid id)
        {
            return Observable.Create<ResourceSetCounterEntry>(async o =>
            {
                try
                {
                    var enumerable = await DbMapper.FetchAsync<ResourceSetCounterEntry>("WHERE outid = ? AND inid = ?", id, id).ConfigureAwait(false);

                    foreach (var entry in enumerable)
                    {
                        o.OnNext(entry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public async Task<ResourceSetCounterEntry> Get(Guid outId, Guid inId, short curveDistance)
        {
            var resourceSetCounterEntry = await DbMapper.FirstOrDefaultAsync<ResourceSetCounterEntry>("WHERE outid = ? AND inid = ? AND cd = ?", outId, inId, curveDistance).ConfigureAwait(false);
            if (resourceSetCounterEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("ResourceSetCounterEntry with outid {0} inid {1} cd {2} is not found", outId, inId, curveDistance));
            }
            return resourceSetCounterEntry;
        }

        public async Task<bool> TryGet(Guid outId, Guid inId, short curveDistance, Ref<ResourceSetCounterEntry> @ref)
        {
            var resourceSetCounterEntry = await DbMapper.FirstOrDefaultAsync<ResourceSetCounterEntry>("WHERE outid = ? AND inid = ? AND cd = ?", outId, inId, curveDistance).ConfigureAwait(false);
            if (resourceSetCounterEntry.IsNullOrDefault())
            {
                return false;
            }
            @ref.Value = resourceSetCounterEntry;
            return true;
        }
    }
}