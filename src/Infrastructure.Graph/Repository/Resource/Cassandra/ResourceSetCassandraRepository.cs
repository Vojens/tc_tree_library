using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Resource.Cassandra.Cql;

namespace Infrastructure.Graph.Repository.Resource.Cassandra
{
    internal class ResourceSetCassandraRepository : CassandraRepository
    {
        private readonly ResourceSetCqlRepository _cqlRepository;
        public ResourceSetCassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            _cqlRepository = new ResourceSetCqlRepository(connectionProvider, schemaContainer);
        }

        public Task Add(ResourceSetEntry entry)
        {
            return _cqlRepository.Add(entry).ConfigureAndExecuteAsync(ConnectionProvider);
        }

        public Task Add(ResourceSetObjectEntry entry)
        {
            return _cqlRepository.Add(entry).ConfigureAndExecuteAsync(ConnectionProvider);
        }

        public IObservable<ResourcesByCurveDistanceEntry> Read(string outUri, short curveDistance, int curveDistanceBucket)
        {
            return Observable.Create<ResourcesByCurveDistanceEntry>(async o =>
            {
                try
                {
                    var resourcesByCurveDistanceEntries = await
                        DbMapper.FetchAsync<ResourcesByCurveDistanceEntry>("WHERE outuri = ? AND cd = ? AND cd_bkt = ?", outUri, curveDistance, curveDistanceBucket)
                            .ConfigureAwait(false);
                    foreach (var resourcesByCurveDistanceEntry in resourcesByCurveDistanceEntries)
                    {
                        o.OnNext(resourcesByCurveDistanceEntry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ResourceSetEntry> Read(Guid outId, Guid inId)
        {
            return Observable.Create<ResourceSetEntry>(async o =>
            {
                try
                {
                    var resourceSetEntries = await
                        DbMapper.FetchAsync<ResourceSetEntry>("WHERE outid = ? AND inid = ?", outId, inId)
                            .ConfigureAwait(false);
                    foreach (var resourceSetEntry in resourceSetEntries)
                    {
                        o.OnNext(resourceSetEntry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }


        public async Task<ResourcesByCurveDistanceEntry> Get(string uri)
        {
            var resourcesByCurveDistanceEntry = await DbMapper.FirstOrDefaultAsync<ResourcesByCurveDistanceEntry>("WHERE outuri = ? AND cd = 0 AND cd_bkt = 0", uri).ConfigureAwait(false);
            if (resourcesByCurveDistanceEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("ResourcesByCurveDistanceEntry with uri {0} is not found", uri));
            }
            return resourcesByCurveDistanceEntry;
        }

        public async Task<bool> TryGet(string uri, Ref<ResourcesByCurveDistanceEntry> resourcesByCurveDistanceEntry)
        {
            try
            {
                //CqlQuerySingleElement<ResourcesByCurveDistanceEntry> cqlQuerySingleElement = _cqlRepository.GetFirstOrDefault(uri);
                //                var fetchedEntry = await cqlQuerySingleElement.ExecuteAsync().ConfigureAwait(false);
                var fetchedEntry = await DbMapper.FirstOrDefaultAsync<ResourcesByCurveDistanceEntry>("WHERE outuri = ? AND cd = ? AND cd_bkt = ?", uri, (short)0,
                    0).ConfigureAwait(false);
                if (fetchedEntry.IsNullOrDefault())
                {
                    return false;
                }
                resourcesByCurveDistanceEntry.Value = fetchedEntry;
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public async Task<Guid> GetId(string uri)
        {
            var resourcesByCurveDistanceEntry = await DbMapper.SingleOrDefaultAsync<ResourcesByCurveDistanceEntry>("WHERE outuri = ? AND cd = ?", uri, 0)
                .ConfigureAwait(false);
            if (resourcesByCurveDistanceEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("ResourcesByCurveDistanceEntry with uri {0} is not found", uri));
            }
            return resourcesByCurveDistanceEntry.InId;
        }

        public async Task<bool> TryGetId(string uri, Ref<Guid> refId)
        {
            var resourcesByCurveDistanceEntry = await DbMapper.FirstOrDefaultAsync<ResourcesByCurveDistanceEntry>("WHERE outuri = ? AND cd = 0 AND cd_bkt = 0", uri).ConfigureAwait(false);
            if (!resourcesByCurveDistanceEntry.IsNullOrDefault())
            {
                refId.Value = resourcesByCurveDistanceEntry.InId;
                return true;
            }
            return false;
        }

        public async Task<ResourceSetEntry> Get(Guid id)
        {
            var resourcesByCurveDistanceEntry = await DbMapper.FirstOrDefaultAsync<ResourceSetEntry>("WHERE outid = ? AND inid = ? AND cd = 0", id).ConfigureAwait(false);
            if (resourcesByCurveDistanceEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("ResourcesByCurveDistanceEntry with id {0} is not found", id));
            }
            return resourcesByCurveDistanceEntry;
        }

        public async Task<bool> TryGet(Guid id, Ref<ResourceSetEntry> resourcesByCurveDistanceEntry)
        {
            var fetchedEntry = await DbMapper.FirstOrDefaultAsync<ResourceSetEntry>("WHERE outid = ? AND inid = ? AND cd = 0", id, id).ConfigureAwait(false);
            if (fetchedEntry.IsNullOrDefault())
            {
                return false;
            }
            resourcesByCurveDistanceEntry.Value = fetchedEntry;
            return true;
        }

        public Task Delete(ResourceSetEntry entry)
        {
            try
            {
                return DbMapper.DeleteAsync<ResourceSetEntry>("WHERE outid = ? AND inid = ? AND cd = ? AND outuri = ? AND inuri = ?", entry.OutId, entry.InId, entry.CurveDistance, entry.OutUri, entry.InUri);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<ResourceSetEntry> Get(Guid outId, Guid inId, short curveDistance, string outUri, string inUri)
        {
            var resourceSetEntry = await DbMapper.FirstOrDefaultAsync<ResourceSetEntry>(
                "WHERE outid = ? AND inid = ? AND cd = ? outuri = ? AND inuri = ?", outId, inId, curveDistance, outUri,
                inUri).ConfigureAwait(false);
            if (resourceSetEntry.IsNullOrDefault())
            {
                throw new KeyNotFoundException(string.Format("ResourceSetEntry with outid = {0} AND inid = {1} AND cd = {2} outuri = {3} AND inuri = {4} is not found", outId, inId, curveDistance, outUri, inUri));
            }
            return resourceSetEntry;
        }

        public async Task<bool> TryGet(Guid outId, Guid inId, short curveDistance, string outUri, string inUri, Ref<ResourceSetEntry> @ref)
        {
            try
            {
                var resourceSetEntry = await DbMapper.FirstOrDefaultAsync<ResourceSetEntry>(
                    "WHERE outid = ? AND inid = ? AND cd = ? AND outuri = ? AND inuri = ?", outId, inId, curveDistance, outUri,
                    inUri).ConfigureAwait(false);
                if (resourceSetEntry.IsNullOrDefault())
                {
                    return false;
                }
                @ref.Value = resourceSetEntry;
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}