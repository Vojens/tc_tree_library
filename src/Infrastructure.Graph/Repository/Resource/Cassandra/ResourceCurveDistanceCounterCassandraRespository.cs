using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Resource.Cassandra.Cql;

namespace Infrastructure.Graph.Repository.Resource.Cassandra
{
    internal class ResourceCurveDistanceCounterCassandraRespository : CassandraRepository
    {
        protected ResourceCurveDistanceCounterCqlRespository CqlRespository;
        public ResourceCurveDistanceCounterCassandraRespository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            CqlRespository = new ResourceCurveDistanceCounterCqlRespository(connectionProvider, schemaContainer);
        }

        public Task Update(ResourceCruveDistanceCounterEntry entry)
        {
            try
            {
                var cqlUpdate = CqlRespository.Update(entry).Configure(StatementOptions.Upsert, ConnectionProvider);
                cqlUpdate.SetIdempotence(false);
                return cqlUpdate.ExecuteAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IObservable<ResourceCruveDistanceCounterEntry> Read(string outUri, short curveDistance)
        {
            return Observable.Create<ResourceCruveDistanceCounterEntry>(async o =>
            {
                try
                {
                    var enumerable = await DbMapper.FetchAsync<ResourceCruveDistanceCounterEntry>("WHERE outuri = ? AND cd = ?", outUri, curveDistance).ConfigureAwait(false);

                    foreach (var entry in enumerable)
                    {
                        o.OnNext(entry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }
    }
}