﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra;
using Cassandra.Data.Linq;
using Cassandra.Mapping;
using Infrastructure.ConnectionProvider;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Util;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Exceptions;

namespace Infrastructure.Graph.Repository.Resource.Cassandra
{
    class DistributedRowLock : CassandraRepository
    {
        public DistributedRowLock(IConnectionProvider connectionProvider, SchemaContainer schemaContainer) : base(connectionProvider, schemaContainer)
        {
        }

        public ColumnPrefixDistributedRowLock New(string key)
        {
            return new ColumnPrefixDistributedRowLock(key, ConnectionProvider, SchemaContainer);
        }
    }

    class ColumnPrefixDistributedRowLock : CassandraRepository
    {
        private LockEntry _lockEntry;
        public ConsistencyLevel ConsistencyLevel { get; private set; }
        public int? Ttl { get; private set; }
        public static readonly int LockTimeoutInSeconds = 60;
        public int Timeout { get; private set; }
        public IRetryPolicy RetryPolicy { get; private set; }
        public bool FailOnStaleLock { get; private set; }
        public bool FailOnBusyLock { get; private set; }
        private int _retryCount = 0;
        public int RetryPlocy { get { return _retryCount; } private set { _retryCount = value; } }
        public static readonly String DefaultLockPrefix = "_LOCK_";
        public String Prefix { get; private set; }
        public String Key { get; private set; }
        private String _lockKey = null;
        private string _lockMode = null;
        public string LockMode { get { return _lockMode; } private set { _lockMode = value; } }
        public ColumnPrefixDistributedRowLock(string key, IConnectionProvider connectionProvider, SchemaContainer schemaContainer) : base(connectionProvider, schemaContainer)
        {
            Key = key;
            Timeout = LockTimeoutInSeconds;
            Prefix = DefaultLockPrefix;
        }

        public ColumnPrefixDistributedRowLock WithConsistencyLevel(ConsistencyLevel consistencyLevel)
        {
            ConsistencyLevel = consistencyLevel;
            return this;
        }

        public ColumnPrefixDistributedRowLock WithTtl(int ttl)
        {
            Ttl = ttl;
            return this;
        }

        public ColumnPrefixDistributedRowLock WithTtl(TimeSpan timeSpan)
        {
            Ttl = Convert.ToInt32(Math.Ceiling(timeSpan.TotalSeconds));
            return this;
        }

        public ColumnPrefixDistributedRowLock WithTimeout(int timeout)
        {
            Timeout = timeout;
            return this;
        }

        public ColumnPrefixDistributedRowLock WithTimeout(TimeSpan timeSpan)
        {
            Timeout = Convert.ToInt32(Math.Ceiling(timeSpan.TotalSeconds));
            return this;
        }

        public ColumnPrefixDistributedRowLock WithBackoff(IRetryPolicy retryPolicy)
        {
            RetryPolicy = retryPolicy;
            return this;
        }

        public ColumnPrefixDistributedRowLock WithFailOnStaleLock(bool failOnStaleLock)
        {
            FailOnStaleLock = failOnStaleLock;
            return this;
        }

        public ColumnPrefixDistributedRowLock WithFailOnBusyLock(bool failOnBusyLock)
        {
            FailOnBusyLock = failOnBusyLock;
            return this;
        }

        public ColumnPrefixDistributedRowLock WithPrefix(String prefix) {
            Prefix = prefix;
            return this;
        }
        
        public ColumnPrefixDistributedRowLock WithMode(string lockMode)
        {
            LockMode = lockMode;
            return this;
        }

        public async Task Aquire()
        {
            Preconditions.CheckArgument(!Ttl.HasValue || Ttl.Value < Timeout, string.Format("Timeout {0} must be less than TTL {1}", Timeout, Ttl.Value));
            _retryCount = 0;
            while (true)
            {
                try
                {
                    var curTimeMicros = DateTime.UtcNow;
                    var cqlInsert = FillMutation(
                        SchemaContainer.GetTable<LockEntry>(), 
                        ConsistencyLevel, 
                        ConsistencyLevel.Serial, 
                        curTimeMicros,
                        LockMode,
                        Ttl);

                    AppliedInfo<LockEntry> rowSet = await cqlInsert.ExecuteAsync().ConfigureAwait(false);

                    VerifyLock(curTimeMicros, rowSet);
                    return;
                }
                catch (BusyLockException e)
                {
//                    release();
//                    if (!retry.allowRetry())
                        throw e;
                    //_retryCount++;
                }
            }
        }

        protected CqlConditionalCommand<LockEntry> FillMutation(Table<LockEntry> table, ConsistencyLevel consistencyLevel,
            ConsistencyLevel serialConsistencyLevel, DateTime curTimeMicros, string lockMode, int? ttl)
        {
            _lockEntry = new LockEntry();
            if (_lockKey == null)
            {
                _lockKey = (Prefix ?? "") + Key;
            }
            else
            {
                if (_lockKey != (Prefix ?? "") + Key)
                    throw new InvalidOperationException("Can't change prefix or key after acquiring the lock");
            }
            _lockEntry.Key = _lockKey;
            _lockEntry.LockId = TimeUuid.NewId(curTimeMicros);
            _lockEntry.Mode = lockMode;

            var cqlInsert = table.Insert(_lockEntry).IfNotExists();
            cqlInsert.SetConsistencyLevel(consistencyLevel);
            cqlInsert.SetSerialConsistencyLevel(serialConsistencyLevel);
            if (Ttl.HasValue)
                cqlInsert.SetTTL(Ttl.Value);

            return cqlInsert;
        }


        public Task Release()
        {
            if (_lockEntry != null)
            {
                var deleteCql = FillReleaseMutation(SchemaContainer.GetTable<LockEntry>(), ConsistencyLevel);
                return deleteCql.ExecuteAsync();
            }
            return TaskHelper.Completed;
        }

        public CqlDelete FillReleaseMutation(Table<LockEntry> table, ConsistencyLevel consistencyLevel)
        {
            var cqlDelete = table.Where(t => t.Key == _lockEntry.Key).Delete();
            cqlDelete.SetConsistencyLevel(consistencyLevel);
            return cqlDelete;
        }

        protected bool VerifyLock(DateTime curTimeMicros, AppliedInfo<LockEntry> appliedInfo)
        {
            if (appliedInfo.Applied)
            {
                return true;
            }

            var lockDateTimeOffset = appliedInfo.Existing.LockId.GetDate();
            var duration = curTimeMicros.Subtract(lockDateTimeOffset.DateTime);
            if (duration.TotalSeconds > Timeout)
            {
                if (FailOnStaleLock)
                {
                    throw new StaleLockException(string.Format("Stale lock on row '{0}'.  Manual cleanup requried.", appliedInfo.Existing));
                }
            }
            if (FailOnBusyLock)
            {
                throw new BusyLockException(string.Format("Lock already acquired for row '{0}'", appliedInfo.Existing));
            }

            return false;
        }

    }
}
