﻿using System;
using System.Collections.Generic;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Resource.Solr.Cql;
using Infrastructure.Common;
using Infrastructure.Graph.Model.Filter;

namespace Infrastructure.Graph.Repository.Resource.Solr
{
    internal class ResourceSetSolrRepository : CassandraRepository
    {
        internal ResourceSetSolrCqlRepository ResourceSolrCqlRepository;
        public ResourceSetSolrRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer) : base(connectionProvider, schemaContainer)
        {
            ResourceSolrCqlRepository = new ResourceSetSolrCqlRepository(connectionProvider, schemaContainer);
        }

//        public IObservable<ResourceSetSolrEntry> Read(string outUri, short? startCd, short? endCd)
//        {
//            //todo validation startcd >= endcd
//            var resourceSetSolrEntries = ResourceSolrCqlRepository.Read(outUri, startCd, endCd);
//            resourceSetSolrEntries.Configure(StatementOptions.RangeScanSelect, ConnectionProvider);
//            resourceSetSolrEntries.SetConsistencyLevel(ConsistencyLevel.One);
//            return CqlQueryExtensions.Read(resourceSetSolrEntries, ConnectionProvider);
//        }


        //public IObservable<ResourceSetSolrEntry> Read(string outUri)
        //{
        //    return CqlQueryExtensions.Read(ResourceSolrCqlRepository.Read(outUri), ConnectionProvider, Global.SolrConsistencyLevel);
        //}

        //public IObservable<ResourceSetSolrEntry> Read(string outUri, string type)
        //{
        //    return CqlQueryExtensions.Read(ResourceSolrCqlRepository.Read(outUri, type), ConnectionProvider, Global.SolrConsistencyLevel);
        //}

        //public IObservable<ResourceSetSolrEntry> Read(string outUri, short cd)
        //{
        //    return CqlQueryExtensions.Read(ResourceSolrCqlRepository.Read(outUri, cd), ConnectionProvider, Global.SolrConsistencyLevel);
        //}

        //public IObservable<ResourceSetSolrEntry> Read(string outUri, string type, short cd)
        //{
        //    return CqlQueryExtensions.Read(ResourceSolrCqlRepository.Read(outUri, type, cd), ConnectionProvider, Global.SolrConsistencyLevel);
        //}

        //public IObservable<ResourceSetSolrEntry> ReadUpTo(string outUri, string type, short cd)
        //{
        //    return CqlQueryExtensions.Read(ResourceSolrCqlRepository.ReadUpTo(outUri, type, cd), ConnectionProvider, Global.SolrConsistencyLevel);
        //}

        //public IObservable<ResourceSetSolrEntry> ReadFrom(string outUri, string type, short cd)
        //{
        //    return CqlQueryExtensions.Read(ResourceSolrCqlRepository.ReadFrom(outUri, type, cd), ConnectionProvider, Global.SolrConsistencyLevel);
        //}

        //public IObservable<ResourceSetSolrEntry> ReadWindow(string outUri, string type, short startCd, short endCd)
        //{
        //    return CqlQueryExtensions.Read(ResourceSolrCqlRepository.ReadWindow(outUri, type, startCd, endCd), ConnectionProvider, Global.SolrConsistencyLevel);
        //}

        //public IObservable<ResourceSetSolrEntry> ReadUpTo(string outUri, short cd)
        //{
        //    return CqlQueryExtensions.Read(ResourceSolrCqlRepository.ReadUpTo(outUri, cd), ConnectionProvider, Global.SolrConsistencyLevel);
        //}

        //public IObservable<ResourceSetSolrEntry> ReadFrom(string outUri, short cd)
        //{
        //    return CqlQueryExtensions.Read(ResourceSolrCqlRepository.ReadFrom(outUri, cd), ConnectionProvider, Global.SolrConsistencyLevel);
        //}

        //public IObservable<ResourceSetSolrEntry> ReadWindow(string outUri, short startCd, short endCd)
        //{
        //    return CqlQueryExtensions.Read(ResourceSolrCqlRepository.ReadWindow(outUri, startCd, endCd), ConnectionProvider, Global.SolrConsistencyLevel);
        //}

        public IObservable<ResourceSetSolrEntry> Read(Guid id, List<Guid> useridentities, string type = null)
        {
            return ResourceSolrCqlRepository.Read(id, useridentities, type).Read(ConnectionProvider, Global.SolrConsistencyLevel);
        }

        public IObservable<ResourceSetSolrEntry> Read(string outUri, List<Guid> useridentities, string type = null)
        {
            return ResourceSolrCqlRepository.Read(outUri, useridentities, type).Read(ConnectionProvider, Global.SolrConsistencyLevel);
        }

        public IObservable<ResourceSetSolrEntry> ReadWithoutPermission(string outUri, string type = null)
        {
            return ResourceSolrCqlRepository.ReadWithoutPermission(outUri, type).Read(ConnectionProvider, Global.SolrConsistencyLevel);
        }

    }
}
