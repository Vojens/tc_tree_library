﻿using System;
using System.Collections.Generic;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository.Resource.Solr.Cql;
using Infrastructure.Common;
using Infrastructure.Graph.Model.Filter;

namespace Infrastructure.Graph.Repository.Resource.Solr
{
    internal class AscendantsByResourceSolrRepository : CassandraRepository
    {
        internal AscendantsByResourceSolrCqlRepository ResourceSolrCqlRepository;
        public AscendantsByResourceSolrRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            ResourceSolrCqlRepository = new AscendantsByResourceSolrCqlRepository(connectionProvider, schemaContainer);
        }

        public IObservable<AscendantsByResourceSolrEntry> ReadWindowAll(string outUri, short startCd, short endCd, List<Guid> useridentities, string type = null)
        {
            return ResourceSolrCqlRepository.ReadWindowAll(outUri, startCd, endCd, useridentities, type).Read(ConnectionProvider, Global.SolrConsistencyLevel);
        }

        public IObservable<AscendantsByResourceSolrEntry> ReadWindowAll(Guid outId, short startCd, short endCd, List<Guid> useridentities, string type = null)
        {
            return ResourceSolrCqlRepository.ReadWindowAll(outId, startCd, endCd, useridentities, type).Read(ConnectionProvider, Global.SolrConsistencyLevel);
        }

        public IObservable<AscendantsByResourceSolrEntry> ReadWindowAllWithoutPermission(string outUri, short startCd, short endCd, string type = null)
        {
            return ResourceSolrCqlRepository.ReadWindowAllWithoutPermission(outUri, startCd, endCd, type).Read(ConnectionProvider, Global.SolrConsistencyLevel);
        }

        public IObservable<AscendantsByResourceSolrEntry> ReadIn(Guid resId, short startCd, short endCd, List<Guid> useridentities, params Condition[] conditions)
        {
            return ResourceSolrCqlRepository.ReadIn(resId, startCd, endCd, useridentities, conditions).Read(ConnectionProvider, Global.SolrConsistencyLevel);
        }

    }
}
