﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.ACD.Business;
using Infrastructure.ACD.Entities;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Mapping;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Repository.Resource.Solr.Cql
{
    internal class ResourceSetSolrCqlRepository : SolrRepository
    {
        private readonly SchemaMap<ResourceSetSolrEntry> _resourceSetSolrEntry;

        public ResourceSetSolrCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            _resourceSetSolrEntry =
                (SchemaMap<ResourceSetSolrEntry>) schemaContainer.GetDefinition<ResourceSetSolrEntry>();
        }

        //TODO Do not use these commented methods without implementing whitelist respect :)
        //public CqlQuery<ResourceSetSolrEntry> Read(string outUri)
        //{
        //    const string solrQueryFormat = "outuri:{0}";
        //    var solrQuery = JsonConvert.SerializeObject(new
        //    {
        //        q = string.Format(solrQueryFormat, outUri),
        //        paging = "driver"
        //    });
        //    return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        //}

        //public CqlQuery<ResourceSetSolrEntry> Read(string outUri, string type)
        //{
        //    const string solrQueryFormat = "outuri:{0} typ:{1}";
        //    var solrQuery = JsonConvert.SerializeObject(new
        //    {
        //        q = string.Format(solrQueryFormat, outUri, type),
        //        paging = "driver"
        //    });
        //    return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        //}

        //public CqlQuery<ResourceSetSolrEntry> Read(string outUri, short cd)
        //{
        //    const string solrQueryFormat = "outuri:{0} cd:{1}";
        //    var solrQuery = JsonConvert.SerializeObject(new
        //    {
        //        q = string.Format(solrQueryFormat, outUri, cd),
        //        paging = "driver"
        //    });
        //    return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        //}

        //public CqlQuery<ResourceSetSolrEntry> Read(string outUri, string type, short cd)
        //{
        //    const string solrQueryFormat = "outuri:{0} typ:{1} cd:{2}";
        //    var solrQuery = JsonConvert.SerializeObject(new
        //    {
        //        q = string.Format(solrQueryFormat, outUri, type, cd),
        //        paging = "driver"
        //    });
        //    return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        //}

        //public CqlQuery<ResourceSetSolrEntry> ReadUpTo(string outUri, string type, short cd)
        //{
        //    const string solrQueryFormat = "outuri:{0} typ:{1} cd:[* TO {2}]";
        //    var solrQuery = JsonConvert.SerializeObject(new
        //    {
        //        q = string.Format(solrQueryFormat, outUri, type, cd),
        //        paging = "driver"
        //    });
        //    return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        //}

        //public CqlQuery<ResourceSetSolrEntry> ReadFrom(string outUri, string type, short cd)
        //{
        //    const string solrQueryFormat = "outuri:{0} typ:{1} cd:[{2} TO *]";
        //    var solrQuery = JsonConvert.SerializeObject(new
        //    {
        //        q = string.Format(solrQueryFormat, outUri, type, cd),
        //        paging = "driver"
        //    });
        //    return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        //}

        //public CqlQuery<ResourceSetSolrEntry> ReadWindow(string outUri, string type, short startCd, short endCd)
        //{
        //    const string solrQueryFormat = "outuri:{0} typ:{1} cd:[{2} TO {3}]";
        //    var solrQuery = JsonConvert.SerializeObject(new
        //    {
        //        q = string.Format(solrQueryFormat, outUri, type, startCd, endCd),
        //        paging = "driver"
        //    });
        //    return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        //}

        //public CqlQuery<ResourceSetSolrEntry> ReadUpTo(string outUri, short cd)
        //{
        //    const string solrQueryFormat = "outuri:{0} cd:[* TO {1}]";
        //    var solrQuery = JsonConvert.SerializeObject(new
        //    {
        //        q = string.Format(solrQueryFormat, outUri, cd),
        //        paging = "driver"
        //    });
        //    return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        //}

        //public CqlQuery<ResourceSetSolrEntry> ReadFrom(string outUri, short cd)
        //{
        //    outUri = "\"" + outUri + "\"";
        //    const string solrQueryFormat = "outuri:{0} AND cd:[{1} TO *]";
        //    var solrQuery = JsonConvert.SerializeObject(new
        //    {
        //        q = string.Format(solrQueryFormat, outUri, cd),
        //        paging = "driver"
        //    });
        //    return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        //}

        //public CqlQuery<ResourceSetSolrEntry> ReadWindow(string outUri, short startCd, short endCd)
        //{
        //    const string solrQueryFormat = "outuri:{0} cd:[{1} TO {2}]";
        //    var solrQuery = JsonConvert.SerializeObject(new
        //    {
        //        q = string.Format(solrQueryFormat, outUri, startCd, endCd),
        //        paging = "driver"
        //    });
        //    return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        //}
        public CqlQuery<ResourceSetSolrEntry> ReadWithoutPermission(string outUri, string type = null)
        {
            var resCd = _resourceSetSolrEntry.GetColumnName(x => x.CurveDistance);
            var outuri = _resourceSetSolrEntry.GetColumnName(x => x.OutUri);

            AbstractSolrQuery basicQuery = new SolrQueryByField(outuri, outUri) {EscapeValue = true, Quoted = false};
            basicQuery = basicQuery && new SolrQueryByRange<short?>(resCd, 0, 0);
            if (!string.IsNullOrWhiteSpace(type))
            {
                var resType = _resourceSetSolrEntry.GetColumnName(x => x.Type);
                basicQuery = basicQuery && new SolrQueryByField(resType, type);
            }
            var solrQuery = QueryBuilder.Build(basicQuery, null);
            return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        }
              
       
        public CqlQuery<ResourceSetSolrEntry> Read(string outUri, List<Guid> useridentities, string type = null)
        {
            var resCd = _resourceSetSolrEntry.GetColumnName(x => x.CurveDistance);
            var outuri = _resourceSetSolrEntry.GetColumnName(x => x.OutUri);

            AbstractSolrQuery basicQuery = new SolrQueryByField(outuri, outUri) {EscapeValue = true, Quoted = false};
            basicQuery = basicQuery && new SolrQueryByRange<short?>(resCd, 0, 0);
            if (!string.IsNullOrWhiteSpace(type))
            {
                var resType = _resourceSetSolrEntry.GetColumnName(x => x.Type);
                basicQuery = basicQuery && new SolrQueryByField(resType, type);
            }
            string solrQuery;
            if (!PermissionService.IsWhitelisted(useridentities))
            {
                var filterQueries = new List<FilterQuery>();
                const string identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                const string permission = "perm"; //_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter =
                    new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                        SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                            new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                            new SolrQueryByField(permission, Convert.ToString((int) Permissions.Read))));
                filterQueries.Add(acdFilter);
                solrQuery = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), null);
            }
            else
            {
                solrQuery = QueryBuilder.Build(basicQuery, null);
            }

            return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        }
        
        public CqlQuery<ResourceSetSolrEntry> Read(Guid id, List<Guid> useridentities, string type = null)
        {
            var resCd = _resourceSetSolrEntry.GetColumnName(x => x.CurveDistance);
            var outId = _resourceSetSolrEntry.GetColumnName(x => x.OutId);

            AbstractSolrQuery basicQuery = new SolrQueryByField(outId, id.ToString());
            basicQuery = basicQuery && new SolrQueryByRange<short?>(resCd, 0, 0);
            if (!string.IsNullOrWhiteSpace(type))
            {
                var resType = _resourceSetSolrEntry.GetColumnName(x => x.Type);
                basicQuery = basicQuery && new SolrQueryByField(resType, type);
            }
            string solrQuery;
            if (!PermissionService.IsWhitelisted(useridentities))
            {
                var filterQueries = new List<FilterQuery>();
                const string identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                const string permission = "perm"; //_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter =
                    new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                        SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                            new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                            new SolrQueryByField(permission, Convert.ToString((int)Permissions.Read))));
                filterQueries.Add(acdFilter);
                solrQuery = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), null);
            }
            else
            {
                solrQuery = QueryBuilder.Build(basicQuery, null);
            }

            return SchemaContainer.GetTable<ResourceSetSolrEntry>().Where(c => c.SoleQuery == solrQuery);
        }
    }
}
