﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using Infrastructure.ACD.Business;
using Infrastructure.ACD.Entities;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Mapping;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Repository.Resource.Solr.Cql
{
    internal class AscendantsByResourceSolrCqlRepository : SolrRepository
    {
        private readonly SchemaMap<AscendantsByResourceSolrEntry> _ascendantsByResourceSolrEntry;
        public AscendantsByResourceSolrCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {
            _ascendantsByResourceSolrEntry = (SchemaMap<AscendantsByResourceSolrEntry>)schemaContainer.GetDefinition<AscendantsByResourceSolrEntry>();
        }

        public CqlQuery<AscendantsByResourceSolrEntry> ReadWindowAll(string outUri, short startCd, short endCd, List<Guid> useridentities, string type = null)
        {
            var resCd = _ascendantsByResourceSolrEntry.GetColumnName(x => x.CurveDistance);
            var outuri = _ascendantsByResourceSolrEntry.GetColumnName(x => x.AscendantUri);

            var toCd = GetCurveDistanceTo(startCd);
            var fromCd = GetCurveDistanceFrom(endCd);

            AbstractSolrQuery basicQuery = new SolrQueryByField(outuri, outUri) { EscapeValue = true, Quoted = false };
            basicQuery = basicQuery && new SolrQueryByRange<short?>(resCd, fromCd, toCd);

            if (!string.IsNullOrWhiteSpace(type))
            {
                var resType = _ascendantsByResourceSolrEntry.GetColumnName(x => x.Type);
                basicQuery = basicQuery && new SolrQueryByField(resType, type);
            }
            string solrQuery;
            if (!PermissionService.IsWhitelisted(useridentities))
            {
                var filterQueries = new List<FilterQuery>();
                const string identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                const string permission = "perm"; //_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter = new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                        new SolrQueryByField(permission, Convert.ToString((int)Permissions.Read))));
                filterQueries.Add(acdFilter);
                solrQuery = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), null);
            }
            else
            {
                solrQuery = QueryBuilder.Build(basicQuery, null);
            }

            return SchemaContainer.GetTable<AscendantsByResourceSolrEntry>().Where(c => c.SolrQuery == solrQuery);
        }

        public CqlQuery<AscendantsByResourceSolrEntry> ReadWindowAll(Guid outId, short startCd, short endCd, List<Guid> useridentities, string type = null)
        {
            var resCd = _ascendantsByResourceSolrEntry.GetColumnName(x => x.CurveDistance);
            var resId = _ascendantsByResourceSolrEntry.GetColumnName(x => x.AscendantId);

            var toCd = GetCurveDistanceTo(startCd);
            var fromCd = GetCurveDistanceFrom(endCd);

            AbstractSolrQuery basicQuery = new SolrQueryByField(resId, outId.ToString());
            basicQuery = basicQuery && new SolrQueryByRange<short?>(resCd, fromCd, toCd);

            if (!string.IsNullOrWhiteSpace(type))
            {
                var resType = _ascendantsByResourceSolrEntry.GetColumnName(x => x.Type);
                basicQuery = basicQuery && new SolrQueryByField(resType, type);
            }
            string solrQuery;
            if (!PermissionService.IsWhitelisted(useridentities))
            {
                var filterQueries = new List<FilterQuery>();
                const string identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                const string permission = "perm"; //_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter = new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                    SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                        new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                        new SolrQueryByField(permission, Convert.ToString((int)Permissions.Read))));
                filterQueries.Add(acdFilter);
                solrQuery = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), null);
            }
            else
            {
                solrQuery = QueryBuilder.Build(basicQuery, null);
            }

            return SchemaContainer.GetTable<AscendantsByResourceSolrEntry>().Where(c => c.SolrQuery == solrQuery);
        }
        
        public CqlQuery<AscendantsByResourceSolrEntry> ReadWindowAllWithoutPermission(string outUri, short startCd, short endCd, string type = null)
        {
            var resCd = _ascendantsByResourceSolrEntry.GetColumnName(x => x.CurveDistance);
            var outuri = _ascendantsByResourceSolrEntry.GetColumnName(x => x.AscendantUri);

            var toCd = GetCurveDistanceTo(startCd);
            var fromCd = GetCurveDistanceFrom(endCd);

            AbstractSolrQuery basicQuery = new SolrQueryByField(outuri, outUri) { EscapeValue = true, Quoted = false };
            basicQuery = basicQuery && new SolrQueryByRange<short?>(resCd, fromCd, toCd);

            if (!string.IsNullOrWhiteSpace(type))
            {
                var resType = _ascendantsByResourceSolrEntry.GetColumnName(x => x.Type);
                basicQuery = basicQuery && new SolrQueryByField(resType, type);
            }
            var solrQuery = QueryBuilder.Build(basicQuery, null);

            return SchemaContainer.GetTable<AscendantsByResourceSolrEntry>().Where(c => c.SolrQuery == solrQuery);
        }

        public CqlQuery<AscendantsByResourceSolrEntry> ReadIn(Guid resId, short startCd, short endCd, List<Guid> useridentities,
          params Condition[] conditions)
        {
            var resCd = _ascendantsByResourceSolrEntry.GetColumnName(x => x.CurveDistance);
            var outId = _ascendantsByResourceSolrEntry.GetColumnName(x => x.OutId);

            AbstractSolrQuery basicQuery = new SolrQueryByField(outId, resId.ToString());
            basicQuery = basicQuery && new SolrQueryByRange<short?>(resCd, startCd, endCd);

            if (conditions != null && conditions.Any())
            {
                foreach (var condition in conditions)
                {
                    basicQuery = basicQuery && condition.BuidQuery();
                }
            }

            string solrQuery;
            if (!PermissionService.IsWhitelisted(useridentities))
            {
                var filterQueries = new List<FilterQuery>();
                const string identityId = "idenid"; //_acdSetSolrEntry.GetColumnName(x => x.PrincipalId);
                const string permission = "perm"; //_acdSetSolrEntry.GetColumnName(x => x.Permission);
                var acdFilter =
                    new FilterQuery(string.Format("{0}.{1}", KeySpace, "acd_set" /*_acdSetSolrEntry.GetTableName*/),
                        SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                            new SolrQueryInList(identityId, useridentities.Select(x => x.ToString())),
                            new SolrQueryByField(permission, Convert.ToString((int)Permissions.Read))));
                filterQueries.Add(acdFilter);
                solrQuery = QueryBuilder.Build(basicQuery, filterQueries.ToArray(), null);
            }
            else
            {
                solrQuery = QueryBuilder.Build(basicQuery, null);
            }

            return SchemaContainer.GetTable<AscendantsByResourceSolrEntry>().Where(c => c.SolrQuery == solrQuery);
        }


        private static short? GetCurveDistanceFrom(short cd)
        {
            const short negate = -1;
            if (cd != short.MaxValue)
            {
                return (short)(Math.Abs(cd) * negate);
            }
            return null;
        }

        private static short? GetCurveDistanceTo(short cd)
        {
            const short negate = -1;
            return (short)(Math.Abs(cd) * negate);
        }
    }
}
