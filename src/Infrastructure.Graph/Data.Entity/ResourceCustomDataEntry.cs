using System;
using Cassandra;

namespace Infrastructure.Graph.Data.Entity
{
    internal class ResourceCustomDataEntry
    {
        public Guid InId { get; set; }
        public Guid OutId { get; set; }
        public string ResourceSystem { get; set; }
        public string Name { get; set; }
        public TimeUuid KeyId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public short Type { get; set; }
    }
}