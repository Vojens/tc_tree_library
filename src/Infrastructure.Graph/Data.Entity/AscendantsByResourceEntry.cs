﻿using System;

namespace Infrastructure.Graph.Data.Entity
{
    internal class AscendantsByResourceEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public short CurveDistance { get; set; }
        public Guid AscendantId { get; set; }
        public string AscendantUri { get; set; }
        public string Type { get; set; }
    }

    internal class AscendantsByResourceSolrEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public short CurveDistance { get; set; }
        public Guid AscendantId { get; set; }
        public string AscendantUri { get; set; }
        public string SolrQuery { get; set; }
        public string Type { get; set; }
    }
}
