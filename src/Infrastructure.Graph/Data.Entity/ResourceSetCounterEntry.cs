using System;

namespace Infrastructure.Graph.Data.Entity
{
    internal class ResourceSetCounterEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public short CurveDistance { get; set; }
        public long Counter { get; set; }
    }
}