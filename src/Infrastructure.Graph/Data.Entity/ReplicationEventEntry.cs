﻿using System;

namespace Infrastructure.Graph.Data.Entity
{
    internal class ReplicationEventEntry
    {
        public Guid RuleId { get; set; }
        public sbyte Status { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string ResourceType { get; set; }
        public string ResourceName { get; set; }
        public Guid ResourceId { get; set; }
        public string Data { get; set; }
    }

    internal class ReplicationEventViewEntry
    {
        public Guid RuleId { get; set; }
        public sbyte Status { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string ResourceType { get; set; }
        public string ResourceName { get; set; }
        public Guid ResourceId { get; set; }
        public string Data { get; set; }
    }
}