namespace Infrastructure.Graph.Data.Entity
{
    internal class ResourceCruveDistanceCounterEntry
    {
        public string OutUri { get; set; }
        public short CurveDistance { get; set; }
        public int Bucket { get; set; }
        public long Counter { get; set; }
    }
}