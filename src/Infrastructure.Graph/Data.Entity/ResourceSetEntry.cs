﻿using System;

namespace Infrastructure.Graph.Data.Entity
{
    internal class ResourceSetEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public string OutUri { get; set; }
        public string InUri { get; set; }
        public short CurveDistance { get; set; }
        public short StorageType { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string ResourceSystem { get; set; }
        public int CurveDistanceBucket { get; set; }
    }

    internal class ResourceSetObjectEntry
    {
        public object OutId { get; set; }
        public object InId { get; set; }
        public object OutUri { get; set; }
        public object InUri { get; set; }
        public object CurveDistance { get; set; }
        public object StorageType { get; set; }
        public object Name { get; set; }
        public object Type { get; set; }
        public object ResourceSystem { get; set; }
        public object CurveDistanceBucket { get; set; }
    }

    internal class ResourcesByCurveDistanceEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public string OutUri { get; set; }
        public string InUri { get; set; }
        public short CurveDistance { get; set; }
        public short StorageType { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string ResourceSystem { get; set; }
        public int CurveDistanceBucket { get; set; }
    }

    internal class ResourceSetSolrEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public string OutUri { get; set; }
        public string InUri { get; set; }
        public short CurveDistance { get; set; }
        public short StorageType { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string ResourceSystem { get; set; }
        public int CurveDistanceBucket { get; set; }
        public string SoleQuery { get; set; }
    }
}