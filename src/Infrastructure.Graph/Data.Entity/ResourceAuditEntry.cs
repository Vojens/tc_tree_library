﻿using System;
using System.Collections.Generic;
using Cassandra;

namespace Infrastructure.Graph.Data.Entity
{
    internal class ResourceAuditEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public string Action { get; set; }
        public IDictionary<string, string> AdditionalInfo { get; set; }
        public Guid AdditionalResourceId { get; set; }
        public string Comment { get; set; }
        public Guid Id { get; set; }
        public string NewData { get; set; }
        public string NewType { get; set; }
        public string OldData { get; set; }
        public string OldType { get; set; }
        public string ResourceType { get; set; }
        public string ResourceSchema{ get; set; }
        public DateTimeOffset TimeStamp{ get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
    }

    internal class ResourceAuditSolrEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public string Action { get; set; }
        public IDictionary<string, string> AdditionalInfo { get; set; }
        public Guid AdditionalResourceId { get; set; }
        public string Comment { get; set; }
        public Guid Id { get; set; }
        public string NewData { get; set; }
        public string NewType { get; set; }
        public string OldData { get; set; }
        public string OldType { get; set; }
        public string ResourceType { get; set; }
        public string ResourceSchema { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string SolrQuery { get; set; }
    }

    internal class ResourceAuditDataEntry
    {
        public Guid Id { get; set; }
        public string NewData { get; set; }
        public string OldData { get; set; }
    }

    //internal class ResourceAuditsSolrEntry
    //{
    //    public Guid OutId { get; set; }
    //    public Guid InId { get; set; }
    //    public string Uri { get; set; }
    //    public string Name { get; set; }
    //    public string Type { get; set; } 
    //    public string SchemaType { get; set; }
    //    public byte[] Schema { get; set; }

    //    public Guid CreationUserId { get; set; }
    //    public string CreationUserName { get; set; }
    //    public DateTimeOffset CreationTimestamp { get; set; }
    //    public LocalTime CreationTime { get; set; }

    //    public Guid ChangeUserId { get; set; }
    //    public string ChangeUserName { get; set; }
    //    public DateTimeOffset ChangeTimestamp { get; set; }
    //    public LocalTime ChangeTime { get; set; }

    //    public string Version { get; set; }
    //    public string Description { get; set; }
    //    public string Keywords { get; set; }
    //    public IDictionary<string, string> WorkflowSteps { get; set; }
    //    public bool IsMarkedDeleted { get; set; }
    //    public bool IsHidden { get; set; }
    //    public bool IsReadOnly { get; set; }
    //    public bool IsSystem { get; set; }
    //    //        public int HasChildren { get; set; }
    //    public string ContentType { get; set; }
    //    public short StorageType { get; set; }
    //    public Guid StorageId { get; set; }
    //    public string StorageUri { get; set; }
    //    public string ResourceSystem { get; set; }
    //    public string SolrQuery { get; set; }
    //}
}