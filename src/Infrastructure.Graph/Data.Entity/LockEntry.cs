﻿using Cassandra;

namespace Infrastructure.Graph.Data.Entity
{
    internal class LockEntry
    {
        public string Key { get; set; }
        public TimeUuid LockId { get; set; }
        public string Mode { get; set; }
    }

    internal static class LockMode
    {
        public static readonly string ProtectedRead = "PR";
        public static readonly string ProtectedWrite = "PW";
        public static readonly string ProtectedDelete = "PD";
    }
}