using System;

namespace Infrastructure.Graph.Data.Entity
{
    internal class HardLinkCounterEntry
    {
        public Guid ResourceId { get; set; }
        public long Counter { get; set; }
    }
}