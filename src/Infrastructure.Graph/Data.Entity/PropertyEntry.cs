using System;

namespace Infrastructure.Graph.Data.Entity
{
    internal class PropertyEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public string Type { get; set; }
        public double? DoubleValue { get; set; }
        public int? IntValue { get; set; }
        public string Value { get; set; }
    }

    internal class PropertySolrEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public string Type { get; set; }
        public double? DoubleValue { get; set; }
        public int? IntValue { get; set; }
        public string Value { get; set; }
        public string SolrQuery { get; set; }
    }
}