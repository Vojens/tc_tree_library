﻿using System;

namespace Infrastructure.Graph.Data.Entity
{
    internal class ResourceDataHashEntry
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public byte[] Hash { get; set; }
        public string ResourceSystem { get; set; }
    }

    internal class ResourceDataSizeEntry
    {
        public Guid Id { get; set; }
        public long Size { get; set; }
    }

    internal class ResourceDataSimpleEntry
    {
        public Guid Id { get; set; }
        public string Compression { get; set; }
        public string Encoding { get; set; }
        public string Type { get; set; }
        public byte[] Data { get; set; }
        public string ResourceSystem { get; set; }
        public DateTimeOffset MTime { get; set; }
    }
}
