using System;
using System.Net;
using Cassandra;
using System.Collections.Generic;

namespace Infrastructure.Graph.Data.Entity
{
    internal class ResourceEventEntry
    {
        public Guid ResourceId { get; set; }
        public DateTimeOffset Bucket { get; set; }
        public TimeUuid CreationTime { get; set; }
        public short ObjectType { get; set; }
        public short ActionType { get; set; }
        public string Version { get; set; }
        public string UniqueIdentifier { get; set; }
        public Guid ParentResourceId { get; set; }
        public Guid RuleId { get; set; }
        public string OldName { get; set; }
    }

    internal class ResourceEventViewEntry
    {
        public Guid ResourceId { get; set; }
        public DateTimeOffset Bucket { get; set; }
        public TimeUuid CreationTime { get; set; }
        public short ObjectType { get; set; }
        public short ActionType { get; set; }
        public string Version { get; set; }
        public string UniqueIdentifier { get; set; }
        public Guid ParentResourceId { get; set; }
        public Guid RuleId { get; set; }
        public string OldName { get; set; }
    }
}

