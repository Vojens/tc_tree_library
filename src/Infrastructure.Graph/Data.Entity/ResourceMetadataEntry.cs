﻿using System;
using System.Collections.Generic;
using Cassandra;

namespace Infrastructure.Graph.Data.Entity
{
    internal class ResourceMetadataEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public string Uri { get; set; }
        public string Name { get; set; }
        public string Type { get; set; } 
        public string SchemaType { get; set; }
        public byte[] Schema { get; set; }

        public Guid CreationUserId { get; set; }
        public string CreationUserName { get; set; }
        public DateTimeOffset CreationTimestamp { get; set; }
        public LocalTime CreationTime { get; set; }

        public Guid ChangeUserId { get; set; }
        public string ChangeUserName { get; set; }
        public DateTimeOffset ChangeTimestamp { get; set; }
        public LocalTime ChangeTime { get; set; }

        public string Version { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string Workflow { get; set; }
        public short WorkflowState { get; set; }
        public bool IsMarkedDeleted { get; set; }
        public bool IsHidden { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsSystem { get; set; }
        //        public int HasChildren { get; set; }
        public string ContentType { get; set; }
        public short StorageType { get; set; }
        public Guid StorageId { get; set; }
        public string StorageUri { get; set; }
        public string ResourceSystem { get; set; }
    }

    internal class ResourceMetadataSolrEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public string Uri { get; set; }
        public string Name { get; set; }
        public string Name_Sort { get; set; }
        public string Type { get; set; } 
        public string SchemaType { get; set; }
        public byte[] Schema { get; set; }

        public Guid CreationUserId { get; set; }
        public string CreationUserName { get; set; }
        public DateTimeOffset CreationTimestamp { get; set; }
        public LocalTime CreationTime { get; set; }

        public Guid ChangeUserId { get; set; }
        public string ChangeUserName { get; set; }
        public DateTimeOffset ChangeTimestamp { get; set; }
        public LocalTime ChangeTime { get; set; }

        public string Version { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string Workflow { get; set; }
        public short WorkflowState { get; set; }
        public bool IsMarkedDeleted { get; set; }
        public bool IsHidden { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsSystem { get; set; }
        //        public int HasChildren { get; set; }
        public string ContentType { get; set; }
        public short StorageType { get; set; }
        public Guid StorageId { get; set; }
        public string StorageUri { get; set; }
        public string ResourceSystem { get; set; }
        public string SolrQuery { get; set; }
    }
}