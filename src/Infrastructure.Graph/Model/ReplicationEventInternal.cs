using System;

namespace Infrastructure.Graph.Model
{
    public class ReplicationEventInternal
    {
        public Guid RuleId { get; set; }
        public sbyte Status { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string ResourceType { get; set; }
        public string ResourceName { get; set; }
        public Guid ResourceId { get; set; }
        public string Data { get; set; }
    }
}