﻿namespace Infrastructure.Graph.Model
{
    public enum EventType { created, updated, deleted }
}
