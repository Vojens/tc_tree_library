﻿using System.Collections.Generic;

namespace Infrastructure.Graph.Model
{
    public class PropertyInternal
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }

        private sealed class KeyEqualityComparer : IEqualityComparer<PropertyInternal>
        {
            public bool Equals(PropertyInternal x, PropertyInternal y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return string.Equals(x.Key, y.Key);
            }

            public int GetHashCode(PropertyInternal obj)
            {
                return (obj.Key != null ? obj.Key.GetHashCode() : 0);
            }
        }

        private static readonly IEqualityComparer<PropertyInternal> KeyComparerInstance = new KeyEqualityComparer();

        public static IEqualityComparer<PropertyInternal> KeyComparer
        {
            get { return KeyComparerInstance; }
        }
    }
}