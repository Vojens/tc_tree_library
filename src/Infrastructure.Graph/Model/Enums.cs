﻿using System;

namespace Infrastructure.Graph.Model
{
    [Flags]
    public enum FetchFlags  //should merge with FetchFlags?
    {
        None = 1,
        Properties = 2,
        Links = 4,
        Data = 8
    }
}
