using System;
using System.Net;
using System.Text;
using Cassandra;
using Infrastructure.Common.Util;
using System.Collections.Generic;

namespace Infrastructure.Graph.Model
{
    public class ResourceEventInternal
    {
        public Guid ResourceId { get; set; }
        public DateTimeOffset Bucket { get; set; }
        public TimeUuid CreationTime { get; set; }
        public short ObjectType { get; set; }
        public short ActionType { get; set; }
        public string Version { get; set; }
        public string UniqueIdentifier { get; set; }
        public Guid ParentResourceId { get; set; }
        public Guid RuleId { get; set; }
        public string OldName { get; set; }
    }
}