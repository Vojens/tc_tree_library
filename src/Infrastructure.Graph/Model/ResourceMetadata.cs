using System;
using System.Collections.Generic;

namespace Infrastructure.Graph.Model
{
    public class ResourceMetadataInternal
    {
        public Guid Id { get; set; }            //[required] [unique] [indexed]
        public string Uri { get; set; }         //[required] [unique] [indexed]
        public string Name { get; set; }        //[required] [indexed]
        public string Type { get; set; }        //[requieed] [indexed]
        public Guid CreationUserId { get; set; }    //Originator ID
        public string CreationUserName { get; set; }    //Originator name
        public DateTimeOffset CreationTimestamp { get; set; }
        public Guid LastUpdateUserId { get; set; }      //for both metadata and data //need to have a better name.. 
        public string LastUpdateUserName { get; set; }    //for both metadata and data //need to have a better name.. 
        public DateTimeOffset LastUpdateTimestamp { get; set; } //for the metadata
        public string Version { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }            //[indexed]
        public string Workflow { get; set; }
        public short WorkflowState { get; set; }
        public bool IsMarkedDeleted { get; set; }    //[indexed]
        public bool IsHidden { get; set; }          //[indexed]
        public bool IsReadOnly { get; set; }
        public bool IsSystem { get; set; }
        public int HasChildren { get; set; }
        public string ContentType { get; set; }          //[optional]
        public StorageType StorageType { get; set; }    //required if Data is not empty
        public Guid StorageId { get; set; }             //required if StorageType == Hardlink
        public string StorageUri { get; set; }          //required if StorageType == SoftLink || NAS
        public string ResourceSystem { get; set; }
        public string SchemaType { get; set; }
        public byte[] Schema { get; set; }

        private sealed class IdUriEqualityComparer : IEqualityComparer<ResourceMetadataInternal>
        {
            public bool Equals(ResourceMetadataInternal x, ResourceMetadataInternal y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x.Id.Equals(y.Id) && string.Equals(x.Uri, y.Uri);
            }

            public int GetHashCode(ResourceMetadataInternal obj)
            {
                unchecked
                {
                    return (obj.Id.GetHashCode() * 397) ^ (obj.Uri != null ? obj.Uri.GetHashCode() : 0);
                }
            }
        }

        private static readonly IEqualityComparer<ResourceMetadataInternal> IdUriComparerInstance = new IdUriEqualityComparer();

        public static IEqualityComparer<ResourceMetadataInternal> IdUriComparer
        {
            get { return IdUriComparerInstance; }
        }
    }

    public enum StorageType
    {
        Empty = 0,
        Simple = 1,
        SoftLink = 2,
        HardLink = 3,
        NAS = 4
    }
    public enum ResourceSetType
    {
        WITSML = 0,
        SEA = 1,
        PetroVue = 2,
        Generic =3,
        Configurations=4
    }
}