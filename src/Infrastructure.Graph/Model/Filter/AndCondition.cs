﻿using System.Linq;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Model.Filter
{
    public sealed class AndCondition<T> : Condition where T : Condition, ISpecificCondition
    {
        public T[] Conditions { get; set; }

        internal override AbstractSolrQuery BuidQuery()
        {
            if (Conditions == null || Conditions.Length == 0)
                return null;

            var query = new SolrMultipleCriteriaQuery(Conditions.Select(x => x.BuidQuery()), AbstractSolrQuery.Operator.AND);
            return query;
        }

        public override ConditionCategory Category
        {
            get { return ((Conditions != null && Conditions.Length > 0) ? Conditions[0].Category : ConditionCategory.None); }
        }

        internal override bool HasKey(string key)
        {
            if (Conditions == null || Conditions.Length == 0)
                return false;

            return Conditions.Any(x => x.HasKey(key));
        }
    }
}