using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Model.Filter
{
    public class AuditCondition : Condition, ISpecificCondition
    {
        private static readonly Dictionary<Keys, string> KeyMapping;

        private bool _isExact = true;

        public bool IsExact
        {
            get { return _isExact; }
            set { _isExact = value; }
        }

        static AuditCondition()
        {
            // TODO need to map based on the schema map.
            KeyMapping = new Dictionary<Keys, string>
            {
                {Keys.AuditId, "id"},
                {Keys.Action, "action"},
                {Keys.AddlInfo, "addlinfo"},
                {Keys.AddlResId, "addlresid"},
                {Keys.ResourceId, "outid"},
                {Keys.ResourceSystem, "rs"},
                {Keys.UserName, "usrnm"},
                {Keys.ResType, "restyp"},
                {Keys.Comment, "cmnt"}
            };
        }

        public AuditCondition(Keys key, string value)
        {
            Key = KeyMapping[key];
            Value = value;
        }

        internal override AbstractSolrQuery BuidQuery()
        {
            if (string.IsNullOrWhiteSpace(Key) || string.IsNullOrWhiteSpace(Value))
                return null;

            var query = new SolrQueryByField(Key, IsExact ? Value : "*" + Value + "*") { Quoted = IsExact };
            return query;
        }

        public override ConditionCategory Category
        {
            get { return ConditionCategory.Audit; }
        }

        internal override bool HasKey(string key)
        {
            if (string.IsNullOrEmpty(key)) return false;
            foreach (var keyPair in KeyMapping)
            {
                if (key.Equals(keyPair.Key.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    if (keyPair.Value.Equals(Key))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public enum Keys
        {
            Action,
            AuditId,
            AddlInfo,
            AddlResId,
            ResourceId,
            ResourceSystem,
            UserName,
            ResType,
            Comment
        }
    }
}