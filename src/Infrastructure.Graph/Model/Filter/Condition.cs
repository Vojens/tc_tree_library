﻿using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Model.Filter
{
    public enum ConditionCategory
    {
        None,
        Resource,
        Metadata,
        Property,
        Audit
    }

    public interface ISpecificCondition
    {
        ConditionCategory Category { get; }
    }

    public abstract class Condition
    {
        public string Key { get; set; }
        public string Value { get; set; }

        internal abstract AbstractSolrQuery BuidQuery();

        public abstract ConditionCategory Category { get; }

        internal abstract bool HasKey(string key);
    }
}
