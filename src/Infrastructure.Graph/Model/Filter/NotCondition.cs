﻿using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Model.Filter
{
    public class NotCondition<T> : Condition where T : Condition, ISpecificCondition
    {
        private readonly ConditionCategory _category;
        private readonly Condition _condition;

        public NotCondition(Condition condition)
        {
            _condition = condition;
            Key = _condition.Key;
            Value = _condition.Value;
            _category = _condition.Category;
        }

        internal override AbstractSolrQuery BuidQuery()
        {
            return new SolrNotQuery(new SolrQueryByField(Key, Value));
        }

        public override ConditionCategory Category
        {
            get { return _category; }
        }

        internal override bool HasKey(string key)
        {
            if (_condition == null) return false;
            return _condition.HasKey(key);
        }
    }
}