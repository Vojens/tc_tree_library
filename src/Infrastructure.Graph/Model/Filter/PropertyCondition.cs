using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Model.Filter
{
    public class PropertyCondition : Condition, ISpecificCondition
    {
        private const string NameString = "nm";
        private const string KeyString = "key";
        private const string ValueString = "val";
        private const string ValueInt = "valint";
        private const string ValueDouble = "valdbl";
        private readonly string _name;
        private readonly string _type;
        private readonly bool _escapespace = true;

        public PropertyCondition(string name, string key, string value, string type)
        {
            _name = name;
            Key = key;
            Value = value;
            _type = type;
        }

        public PropertyCondition(string name, string key, string value, bool escapespace, string type)
        {
            _name = name;
            Key = key;
            Value = value;
            _type = type;
            _escapespace = escapespace;
        }

        public PropertyCondition(string name, string key, string value)
        {
            _name = name;
            Key = key;
            Value = value;
        }

        public PropertyCondition(string name, string key, string value, bool escapespace)
        {
            _name = name;
            Key = key;
            Value = value;
            _escapespace = escapespace;
        }

        internal override AbstractSolrQuery BuidQuery()
        {
            if (string.IsNullOrWhiteSpace(_name) || string.IsNullOrWhiteSpace(Key) || string.IsNullOrWhiteSpace(Value))
                return null;
            var charReplace = true;
            var value = ValueString;
            if (!string.IsNullOrWhiteSpace(_type))
            {
                switch (_type.ToLower())
                {
                    case "uuid":
                    case "uri":
                        charReplace = false;
                        break;
                    case "int":
                        value = ValueInt;
                        break;
                    case "double":
                        value = ValueDouble;
                        break;
                    default:
                        value = ValueString;
                        break;
                }
            }


            var query = SolrMultipleCriteriaQuery.Create(AbstractSolrQuery.Operator.AND,
                new SolrQueryByField(NameString, _name), new SolrQueryByField(KeyString, Key),
                new SolrQueryByField(value, Value) { EscapeSpace = _escapespace, CharReplace = charReplace});
            return query;
        }

        public override ConditionCategory Category
        {
            get { return ConditionCategory.Property; }
        }

        internal override bool HasKey(string key)
        {
            return Key.Equals(key);
        }
    }
}