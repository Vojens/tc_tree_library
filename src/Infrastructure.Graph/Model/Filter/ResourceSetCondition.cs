using System;
using System.Collections.Generic;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Model.Filter
{
    public class ResourceSetCondition : Condition, ISpecificCondition
    {
        private static readonly Dictionary<Keys, string> KeyMapping;

        private bool _isExact = true;

        public bool IsExact
        {
            get { return _isExact; }
            set { _isExact = value; }
        }

        static ResourceSetCondition()
        {
            // TODO need to map based on the schema map.
            KeyMapping = new Dictionary<Keys, string>
            {
                {Keys.OutId, "outid"},
                {Keys.InUri, "inuri"},
                {Keys.Name, "nm"},
                {Keys.Type, "typ"}
            };
        }

        public ResourceSetCondition(Keys key, string value)
        {
            Key = KeyMapping[key];
            Value = value;
        }

        internal override AbstractSolrQuery BuidQuery()
        {
            if (string.IsNullOrWhiteSpace(Key) || string.IsNullOrWhiteSpace(Value))
                return null;

            var query = new SolrQueryByField(Key, IsExact ? Value : "*" + Value + "*") { Quoted = IsExact };
            return query;
        }

        public override ConditionCategory Category
        {
            get { return ConditionCategory.Resource; }
        }

        internal override bool HasKey(string key)
        {
            if (string.IsNullOrEmpty(key)) return false;
            foreach (var keyPair in KeyMapping)
            {
                if (key.Equals(keyPair.Key.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    if (keyPair.Value.Equals(Key))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public enum Keys
        {
            OutId,
            InUri,
            Name,
            Type
        }
    }
}