using System;
using System.Collections.Generic;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Model.Filter
{
    public class MetadataRangeCondition : Condition, ISpecificCondition
    {
        private static readonly Dictionary<Keys, string> KeyMapping;

        public string From { get; private set; }

        public string To { get; private set; }

        public bool InclusiveFrom { get; private set; }

        public bool InclusiveTo { get; private set; }

        static MetadataRangeCondition()
        {
            // TODO need to map based on the schema map.
            KeyMapping = new Dictionary<Keys, string>
            {
                {Keys.CreationTimestamp, "crtime"},
                {Keys.LastUpdateTimestamp, "ctime"},
            };
        }

        public MetadataRangeCondition(Keys key, string from, string to, bool inclusive = true)
            : this(key, from, to, inclusive, inclusive) { }

        public MetadataRangeCondition(Keys key, string from, string to, bool inclusiveFrom, bool inclusiveTo)
        {
            Key = KeyMapping[key];
            From = from;
            To = to;
            InclusiveFrom = inclusiveFrom;
            InclusiveTo = inclusiveTo;
        }

        internal override AbstractSolrQuery BuidQuery()
        {
            if (string.IsNullOrWhiteSpace(Key) || string.IsNullOrWhiteSpace(From) || string.IsNullOrWhiteSpace(To))
                return null;
            var query = new SolrQueryByStringRange<string>(Key, From, To, InclusiveFrom, InclusiveTo);
            return query;
        }

        public override ConditionCategory Category
        {
            get { return ConditionCategory.Metadata; }
        }

        internal override bool HasKey(string key)
        {
            if (string.IsNullOrEmpty(key)) return false;
            foreach (var keyPair in KeyMapping)
            {
                if (key.Equals(keyPair.Key.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    if (keyPair.Value.Equals(Key))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public enum  Keys
        {
            CreationTimestamp,
            LastUpdateTimestamp
        }
    }
}