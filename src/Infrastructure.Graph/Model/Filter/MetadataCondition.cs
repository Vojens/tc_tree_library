﻿using System;
using System.Collections.Generic;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Model.Filter
{
    public class MetadataCondition : Condition, ISpecificCondition
    {
        private static readonly Dictionary<Keys, string> KeyMapping;

        private bool _isExact = true;

        public bool IsExact
        {
            get { return _isExact; }
            set { _isExact = value; }
        }

        static MetadataCondition()
        {
            // TODO need to map based on the schema map.
            KeyMapping = new Dictionary<Keys, string>
            {
                {Keys.Id, "outid"},
                {Keys.Uri, "uri"},
                {Keys.Name, "nm"},
                {Keys.Name_Sort, "nm_sort"},
                {Keys.Type, "typ"},
                {Keys.IsDeleted, "isdel"},
                {Keys.IsHidden, "ishid"},
                {Keys.IsSystem, "issys"},
                {Keys.IsReadOnly, "isreadonly"},
                {Keys.CreationUserName, "crusrnm"},
                {Keys.LastUpdateUserName, "cusrnm"},
                {Keys.CreationTimestamp, "crtime"},
                {Keys.LastUpdateTimestamp, "ctime"},
                {Keys.ContentType, "conttyp"},
                {Keys.Description, "descr"}
            };
        }

        public MetadataCondition(Keys key, string value)
        {
            Key = KeyMapping[key];
            Value = value;
        }

        internal override AbstractSolrQuery BuidQuery()
        {
            if (string.IsNullOrWhiteSpace(Key) || string.IsNullOrWhiteSpace(Value))
                return null;
            
            var query = new SolrQueryByField(Key, IsExact ? Value : "*" + Value + "*") { Quoted = IsExact };
            return query;
        }

        public override ConditionCategory Category
        {
            get { return ConditionCategory.Metadata; }
        }

        internal override bool HasKey(string key)
        {
            if (string.IsNullOrEmpty(key)) return false;
            foreach (var keyPair in KeyMapping)
            {
                if (key.Equals(keyPair.Key.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    if (keyPair.Value.Equals(Key))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public enum Keys
        {
            Id,
            Uri,
            Name,
            Name_Sort,
            Type,
            ContentType,
            IsDeleted,
            IsHidden,
            IsSystem,
            IsReadOnly,
            CreationUserName,
            LastUpdateUserName,
            CreationTimestamp,
            LastUpdateTimestamp,
            Description

        }
    }
}