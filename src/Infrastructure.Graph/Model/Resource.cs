﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Graph.Model
{
    public class ResourceInternal
    {
        public ResourceMetadataInternal ResourceMetadata { get; set; }    //[required]
        public IDictionary<string, HashSet<PropertyInternal>> Properties { get; set; }   //[optional]
        public ResourceDataInternal Data { get; set; }  //[optional]
        public HashSet<ResourceLinkInternal> Parents { get; set; }     //read only //nullable // ignore on write
        /// <summary>
        /// Used by replication service to identify which rule is creating this resource.
        /// Empty if the resouce was not created by replication service.
        /// </summary>
        public Guid ReplicationRuleId{ get; set; }     
    }
}
