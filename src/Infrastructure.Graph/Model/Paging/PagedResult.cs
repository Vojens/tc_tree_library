﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Graph.Model.Paging
{
    public class PagedResult<T>
    {
        public Paging Paging { get; set; }
        public IList<T> Data { get; set; }

        public PagedResult(IEnumerable<T> items, int pageNo, int queryPageSize, int totalRecordCount)
        {
            Data = new List<T>(items);
            var pageSize = items.Any() ? Math.Min(items.Count(), queryPageSize) : queryPageSize;

            var pageCount = totalRecordCount > 0 && queryPageSize > 0 ? totalRecordCount / queryPageSize + (totalRecordCount % queryPageSize > 0 ? 1 : 0) : 0;
            Paging = new Paging
            {
                Page = pageNo,
                PageSize = pageSize,
                TotalCount = totalRecordCount,
                PageCount = pageCount
            };
        }

        public PagedResult(IEnumerable<T> items, Paging paging)
        {
            Data = new List<T>(items);
            Paging = paging;
        }
    }
}