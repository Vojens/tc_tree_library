﻿using Infrastructure.Graph.Model.Filter;

namespace Infrastructure.Graph.Model
{
    public enum SortBy
    {
        Name,
        Name_Sort,
        CreatedDate,
        LastUpdatedDate,
        Type,
        Custom,
        None
    }

    public enum SortOrder
    {
        Ascending,
        Descending
    }

    public class QueryOptions
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public SortBy SortBy { get; set; }
        public string SortByStr { get; set; }
        public SortOrder SortOrder { get; set; }
        public Condition[] Filters { get; set; }
    }
}