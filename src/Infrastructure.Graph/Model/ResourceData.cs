﻿using System;
using Infrastructure.Common.Util;

namespace Infrastructure.Graph.Model
{
    public class ResourceDataInternal //TODO change to ResourceData later
    {
        public DateTimeOffset ModifyTimestamp { get; set; }
        public string ContentEncoding { get; set; }
        public byte[] Content { get; set; }

        /// <summary>
        /// Set the content to a string, updating the Content and ContentEncoding properties.
        /// </summary>
        /// <param name="text">The text string</param>
        public void SetContent(string text)
        {
            string encoding;
            byte[] bytes = ContentEncoder.Encode(text, out encoding);

            Content = bytes;
            ContentEncoding = encoding;
        }

        /// <summary>
        /// Get the content as a string. Handles ContentEncoding.
        /// </summary>
        /// <returns>The content as a string</returns>
        public string GetContentAsString()
        {
            return ContentEncoder.DecodeString(Content, ContentEncoding);
        }
    }
}