﻿using System;

namespace Infrastructure.Graph.Model.Internal
{
    internal class Edge : IEquatable<Edge>
    {
        public override string ToString()
        {
            return string.Format("Out: {0}, In: {1}", Out, In);
        }

        public bool Equals(Edge other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(Out, other.Out) && Equals(In, other.In);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Edge)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Out != null ? Out.GetHashCode() : 0) * 397) ^ (In != null ? In.GetHashCode() : 0);
            }
        }

        public static bool operator ==(Edge left, Edge right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Edge left, Edge right)
        {
            return !Equals(left, right);
        }

        public Identifier Out { get; set; }
        public Identifier In { get; set; }

        public static Edge CreateEdge(ResourceLinkInternal @out, ResourceLinkInternal @in)
        {
            return new Edge
            {
                Out = Identifier.FromResourceLink(@out),
                In = Identifier.FromResourceLink(@in)
            };
        }
    }
}