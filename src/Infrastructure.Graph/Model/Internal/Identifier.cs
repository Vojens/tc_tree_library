using System;
using System.Collections.Generic;
using Infrastructure.Common.Extensions;

namespace Infrastructure.Graph.Model.Internal
{
    internal class Identifier : IEquatable<Identifier>
    {
        public override string ToString()
        {
            return string.Format("Id: {0}, Uri: {1}", Id, Uri);
        }

        public bool Equals(Identifier other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id.Equals(other.Id) && string.Equals(Uri, other.Uri);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Identifier)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id.GetHashCode() * 397) ^ (Uri != null ? Uri.GetHashCode() : 0);
            }
        }

        public static bool operator ==(Identifier left, Identifier right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Identifier left, Identifier right)
        {
            return !Equals(left, right);
        }

        private sealed class IdUriEqualityComparer : IEqualityComparer<Identifier>
        {
            public bool Equals(Identifier x, Identifier y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x.Id.Equals(y.Id) && string.Equals(x.Uri, y.Uri);
            }

            public int GetHashCode(Identifier obj)
            {
                unchecked
                {
                    return (obj.Id.GetHashCode() * 397) ^ (obj.Uri != null ? obj.Uri.GetHashCode() : 0);
                }
            }
        }

        private static readonly IEqualityComparer<Identifier> IdUriComparerInstance = new IdUriEqualityComparer();

        public static IEqualityComparer<Identifier> IdUriComparer
        {
            get { return IdUriComparerInstance; }
        }

        public Guid Id { get; set; }
        public string Uri { get; set; }

        public static Identifier FromResourceLink(ResourceLinkInternal link)
        {
            Preconditions.CheckNotNull(link, "resourcelink cannot be null");
            return new Identifier
            {
                Id = link.Id,
                Uri = link.Uri
            };
        }
    }
}