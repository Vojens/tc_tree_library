﻿using System.Collections.Generic;

namespace Infrastructure.Graph.Model.Internal
{
    internal class Graph
    {
        public HashSet<ResourceInternal> Vertices { get; set; }
        public HashSet<Edge> Edges { get; set; }
    }
}
