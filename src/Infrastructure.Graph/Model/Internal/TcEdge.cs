namespace Infrastructure.Graph.Model.Internal
{
    internal class TcEdge : Edge
    {
        public int CurveDistance { get; set; }
    }
}