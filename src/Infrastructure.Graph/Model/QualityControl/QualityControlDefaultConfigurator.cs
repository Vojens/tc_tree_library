using Infrastructure.Common.IoC;

namespace Infrastructure.Graph.Model.QualityControl
{
    public static class QualityControlDefaultConfigurator
    {
        public static void Configure()
        {
            ComponentRegistry.Register<PropertyValidator>();
        }
    }
}