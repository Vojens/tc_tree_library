﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using Infrastructure.Common;

namespace Infrastructure.Graph.Model.QualityControl
{
    internal class PropertyValidator : AbstractValidator<PropertyInternal>
    {
        public PropertyValidator()
        {
            RuleFor(property => property.Key).NotNull().NotEmpty().WithMessage("key cannot be null or empty");
            RuleFor(property => property.Value).NotEmpty().When(property => Global.SupportedPropertyTypes.Contains(property.Type));
            RuleFor(property => property.Value).Must(IsInt).When(property => property.Type == _intType).WithMessage("Value {PropertyValue} is not of type int");
            RuleFor(property => property.Value).Must(IsDouble).When(property => property.Type == _doubleType).WithMessage("Value {PropertyValue} is not of type double");
        }

        private string _intType = Global.SupportedPropertyTypesDictionary[typeof(int)];
        private string _doubleType = Global.SupportedPropertyTypesDictionary[typeof(double)];


        private bool IsInt(string value)
        {
            int i;
            return Int32.TryParse(value, out i);
        }

        private bool IsDouble(string value)
        {
            double d;
            return Double.TryParse(value, out d);
        }
    }

    internal class PropertyDictionaryValidator : AbstractValidator<IDictionary<String, HashSet<PropertyInternal>>>
    {
        public PropertyDictionaryValidator()
        {
            RuleFor(dictionary => dictionary.Values).SetCollectionValidator(new PropertyHashSetValidator());
        }
    }


    internal class PropertyHashSetValidator : AbstractValidator<HashSet<PropertyInternal>>
    {
        public PropertyHashSetValidator()
        {
            RuleFor(hashSet => hashSet)
                .Must(hashSet => hashSet.Distinct(PropertyInternal.KeyComparer).Count() == hashSet.Count)
                .WithMessage(
                    "Properties are not distinct. Properties of the same group cannot have multiple entries with the same key");
            RuleForEach(hashset => hashset).SetValidator(new PropertyValidator());
            //RuleFor(hashSet => hashSet).SetCollectionValidator(new PropertyValidator()).WithName("Property");
        }
    }
}
