﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Infrastructure.Graph.Model.QualityControl
{
    internal class ResourceDataValidator : AbstractValidator<ResourceDataInternal>
    {
        public ResourceDataValidator()
        {
            RuleFor(data => data).NotNull().NotEmpty();
            RuleFor(data => data.ContentEncoding).NotNull();
            RuleFor(data => data.Content).NotNull();
        }
    }
}
