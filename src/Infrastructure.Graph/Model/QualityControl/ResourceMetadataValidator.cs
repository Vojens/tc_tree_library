using FluentValidation;

namespace Infrastructure.Graph.Model.QualityControl
{
    internal class ResourceMetadataValidator : AbstractValidator<ResourceMetadataInternal>
    {
        public ResourceMetadataValidator()
        {
            RuleFor(metadata => metadata).NotNull().NotEmpty();
            RuleFor(metadata => metadata.Id).NotEmpty().WithMessage("resource id cannot be empty");
            RuleFor(metadata => metadata.Uri).NotNull().NotEmpty().WithMessage("resource uri cannot be null or empty");
            RuleFor(metadata => metadata.Name).NotNull().WithMessage("Name cannot be null");
            RuleFor(metadata => metadata.Type).NotNull().NotEmpty().WithMessage("Type cannot be null or empty");
        }
    }
}