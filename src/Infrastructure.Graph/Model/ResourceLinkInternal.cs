﻿using System;

namespace Infrastructure.Graph.Model
{
    public class ResourceLinkInternal
    {
        public string Uri { get; set; }
        public Guid Id { get; set; }
    }
}