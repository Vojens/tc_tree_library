using System;
using System.Collections.Generic;

namespace Infrastructure.Graph.Model
{

    public class ResourceAudit
    {
        public Guid ResourceId { get; set; }
        public string Action { get; set; }
        public IDictionary<string, string> AdditionalInfo { get; set; }
        public Guid AdditionalResourceId { get; set; }
        public string Comment { get; set; }
        public Guid Id { get; set; }
        public string NewData { get; set; }
        public string NewType { get; set; }
        public string OldData { get; set; }
        public string OldType { get; set; }
        public string ResourceType { get; set; }
        public string ResourceSet { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
    }
}