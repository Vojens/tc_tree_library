﻿namespace Infrastructure.Graph
{
    public static class Constants
    {
        public static readonly int CURVE_DISTANCE_BUCKET_SIZE = 10000;
        public static readonly int LOCK_TTL = 180;
        public static readonly int LOCK_TIMEOUT = 360;
    }
}
