﻿using System;
using System.Collections.Generic;
using Infrastructure.Graph.Exceptions;

namespace Infrastructure.Graph
{
    public class InstanceLocker
    {
        private static readonly Dictionary<string, string> Locks = new Dictionary<string, string>();
        private static readonly object Lock = new object();

        public static void EnterLock(string key,Exception ex)
        {
            lock (Lock)
            {
                if (Locks.ContainsKey(key))
                {
                    throw ex;
                }
                Locks.Add(key, key);
            }
        }

        public static void ExitLock(string key)
        {
            lock (Lock)
            {
                if (Locks.ContainsKey(key))
                {
                    Locks.Remove(key);
                }
            }
        }
    }
}
