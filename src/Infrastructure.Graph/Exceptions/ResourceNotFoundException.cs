﻿using System;

namespace Infrastructure.Graph.Exceptions
{
    [Serializable]
    public class ResourceNotFoundException : NotFoundException
    {
        private const string UriException = "Resource with Uri {0} not found in the database";
        public ResourceNotFoundException(string uri) : base(string.Format(UriException, uri))
        {
            
        }

        private const string IdException = "Resource with id {0} not found in the database";
        public ResourceNotFoundException(Guid id)
            : base(string.Format(IdException, id))
        {

        }
    }

    [Serializable]
    public class ResourceMetaDataNotFoundException : NotFoundException
    {
        private const string UriException = "Resource MetaData with Uri {0} not found in the database";
        private const string IdException = "Resource MetaData with id {0} not found in the database";
        
        public ResourceMetaDataNotFoundException(string uri)
            : base(string.Format(UriException, uri))
        {
        }

        public ResourceMetaDataNotFoundException(Guid id)
            : base(string.Format(IdException, id))
        {
        }
    }
}
