﻿using System;

namespace Infrastructure.Graph.Exceptions
{
    public class ResourceAlreadyExistsException : InvalidOperationException
    {
        private const string UriException = "Resource with same uri {0} already exists in the database";
        public ResourceAlreadyExistsException(string uri)
            : base(string.Format(UriException, uri))
        {

        }

        private const string IdException = "Resource with same id {0} already exists in the database";
        public ResourceAlreadyExistsException(Guid id)
            : base(string.Format(IdException, id))
        {

        }
    }
}
