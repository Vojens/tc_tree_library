﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Graph.Exceptions
{
    public class StaleLockException : SystemException
    {
        public StaleLockException()
        {
        }

        public StaleLockException(string message)
            : base(message)
        {
        }

        public StaleLockException(string format, params object[] args)
            : base(string.Format(format, args))
        {
        }

        public StaleLockException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public StaleLockException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException)
        {
        }

        public StaleLockException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
