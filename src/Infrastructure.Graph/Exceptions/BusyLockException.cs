﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Graph.Exceptions
{
    public class BusyLockException : SystemException
    {
        public BusyLockException()
        {
        }

        public BusyLockException(string message)
            : base(message)
        {
        }

        public BusyLockException(string format, params object[] args)
            : base(string.Format(format, args))
        {
        }

        public BusyLockException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public BusyLockException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException)
        {
        }

        public BusyLockException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
