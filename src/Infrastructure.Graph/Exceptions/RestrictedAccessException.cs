﻿using System;
using System.Runtime.Serialization;

namespace Infrastructure.Graph.Exceptions
{
    public class RestrictedAccessException : SystemException
    {
        public RestrictedAccessException()
        {
        }

        public RestrictedAccessException(string message)
            : base(message)
        {
        }

        public RestrictedAccessException(string format, params object[] args)
            : base(string.Format(format, args))
        {
        }

        public RestrictedAccessException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public RestrictedAccessException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException)
        {
        }

        protected RestrictedAccessException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}