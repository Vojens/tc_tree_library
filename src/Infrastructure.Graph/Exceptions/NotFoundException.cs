﻿using System;
using System.Runtime.Serialization;

namespace Infrastructure.Graph.Exceptions
{
    [Serializable]
    public abstract class NotFoundException : SystemException
    {
        protected NotFoundException()
        {
        }

        protected NotFoundException(string message)
            : base(message)
        {
        }

        protected NotFoundException(string format, params object[] args)
            : base(string.Format(format, args))
        {
        }

        protected NotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected NotFoundException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException)
        {
        }

        protected NotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
