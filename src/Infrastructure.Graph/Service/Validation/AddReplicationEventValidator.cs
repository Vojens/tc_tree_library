using FluentValidation;
using Infrastructure.Graph.Model;

namespace Infrastructure.Graph.Service.Validation
{
    internal class AddReplicationEventValidator : AbstractValidator<ReplicationEventInternal>
    {
        public AddReplicationEventValidator()
        {
            RuleFor(entry => entry).NotNull().NotEmpty();
            RuleFor(entry => entry.RuleId).NotEmpty().WithMessage("RuleId cannot be empty");
            //why the below is needed when Status is sbyte
            //RuleFor(entry => entry.Status).NotNull().NotEmpty().WithMessage("Status cannot be null or empty");
            RuleFor(entry => entry.Timestamp).NotNull().NotEmpty().WithMessage("Timestamp cannot be null or empty");
            RuleFor(entry => entry.ResourceType).NotNull().NotEmpty().WithMessage("ResourceType cannot be null or empty");
            RuleFor(entry => entry.ResourceName).NotNull().NotEmpty().WithMessage("ResourceName cannot be null or empty");
            // Resource Id is empty if a deleted object from classic comes to HD in intitial stream
            //RuleFor(entry => entry.ResourceId).NotEmpty().WithMessage("ResourceId cannot be empty");
            RuleFor(entry => entry.Data).NotNull().NotEmpty().WithMessage("Data cannot be null or empty");
        }
    }
}