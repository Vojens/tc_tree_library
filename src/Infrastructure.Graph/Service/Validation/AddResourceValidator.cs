using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.QualityControl;
using Infrastructure.Graph.Service.Composite;

namespace Infrastructure.Graph.Service.Validation
{
    internal class AddResourcesValidator : AbstractValidator<IEnumerable<ResourceInternal>>
    {
        internal readonly ResourceSetCompositeService ResourceSetCompositeService;
        private readonly AddResourceValidator _addResourceValidator;
        public AddResourcesValidator(ResourceSetCompositeService resourceSetCompositeService)
        {
            ResourceSetCompositeService = resourceSetCompositeService;
            _addResourceValidator = new AddResourceValidator(ResourceSetCompositeService);
        }

        public override async Task<ValidationResult> ValidateAsync(ValidationContext<IEnumerable<ResourceInternal>> context, CancellationToken cancellation = new CancellationToken())
        {
            var instance = context.InstanceToValidate;
            var failures = new List<ValidationFailure>();

            if (instance == null || !instance.Any())
            {
                failures.Add(new ValidationFailure("resource list",
                    "resource list cannot be null or empty"));
            }
            else
            {
                if (instance.Select(x => x.ResourceMetadata).Distinct(ResourceMetadataInternal.IdUriComparer).Count() !=
                    instance.Count())
                {
                    failures.Add(new ValidationFailure("resource list",
                    "resources are not distinct. One or more resource share the same id/uri"));
                }

                var tasks = new ConcurrentBag<Task<ValidationResult>>();
                Parallel.ForEach(instance, item =>
                {
                    tasks.Add(_addResourceValidator.ValidateAsync(item));
                });

                foreach (var task in tasks.Interleaved())
                {
                    var validationResult = (await (await task.ConfigureAwait(false)).ConfigureAwait(false));
                    failures.AddRange(validationResult.Errors);
                }
            }

            return new ValidationResult(failures);
        }
    }

    internal class AddResourceValidator : AbstractValidator<ResourceInternal>
    {
        internal readonly ResourceSetCompositeService ResourceSetCompositeService;
        public AddResourceValidator(ResourceSetCompositeService resourceSetCompositeService)
        {
            ResourceSetCompositeService = resourceSetCompositeService;
            RuleFor(resource => resource.ResourceMetadata).SetValidator(new ResourceMetadataValidator());
            RuleFor(resource => resource.Data).SetValidator(new ResourceDataValidator());
            RuleFor(resource => resource.ResourceMetadata.Id).MustAsync((id, cancel) => ResourceSetCompositeService.NotExists(id)).WithMessage("another resource with the same id {PropertyValue} already exists");
            RuleFor(resource => resource.ResourceMetadata.Uri).MustAsync((uri, cancel) => ResourceSetCompositeService.NotExists(uri)).WithMessage("another resource with the same uri {PropertyValue} already exists");
            RuleFor(resource => resource.Properties)
                .SetValidator(new PropertyDictionaryValidator())
                .When(resource => resource.Properties != null);
            RuleFor(resource => resource.ResourceMetadata.StorageId).NotEmpty()
                .When(resource => resource.ResourceMetadata.StorageType == StorageType.HardLink);
            RuleFor(resource => resource.ResourceMetadata)
                .MustAsync((metadata, cancel) => ResourceSetCompositeService.Exists(metadata.StorageId))
                .When(resource => resource.ResourceMetadata.StorageType == StorageType.HardLink);
            RuleFor(resource => resource.ResourceMetadata.StorageUri).NotNull().NotEmpty()
                .When(
                    resource =>
                        resource.ResourceMetadata.StorageType == StorageType.SoftLink ||
                        resource.ResourceMetadata.StorageType == StorageType.NAS);
            RuleFor(resource => resource.Data)
                .NotNull()
                .NotEmpty()
                .When(resource => resource.ResourceMetadata.StorageType != StorageType.Empty);
        }
    }
}