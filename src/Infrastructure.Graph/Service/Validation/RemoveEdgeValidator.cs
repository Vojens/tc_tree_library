using FluentValidation;
using Infrastructure.Graph.Model.Internal;
using Infrastructure.Graph.Service.Composite;

namespace Infrastructure.Graph.Service.Validation
{
    internal class RemoveEdgeValidator : AbstractValidator<Edge>
    {
        private readonly ResourceSetCompositeService _resourceSetCompositeService;
        public RemoveEdgeValidator(ResourceSetCompositeService resourceSetCompositeService)
        {
            _resourceSetCompositeService = resourceSetCompositeService;
            RuleFor(edge => edge).NotNull().NotEmpty().WithMessage("edge cannot be null or empty");
            RuleFor(edge => edge.Out).NotNull();
            RuleFor(edge => edge.Out.Id).NotEmpty();
            RuleFor(edge => edge.Out.Uri).NotNull().NotEmpty();
            RuleFor(edge => edge.In).NotNull();
            RuleFor(edge => edge.In.Id).NotEmpty();
            RuleFor(edge => edge.In.Uri).NotNull().NotEmpty();
            RuleFor(edge => edge)
                .Must(edge => edge.In != edge.Out)
                .WithMessage("cannot remove a link/edge to the same resource");
            RuleFor(edge => edge.Out).MustAsync((@out, cancel) => _resourceSetCompositeService.Exists(@out.Uri, @out.Id)).WithMessage("out resource {PropertyValue} does not exist");
            RuleFor(edge => edge.In).MustAsync((@in, cancel) => _resourceSetCompositeService.Exists(@in.Uri, @in.Id)).WithMessage("in resource {PropertyValue} does not exist");
            RuleFor(edge => edge.In).MustAsync((@in, cancel) => _resourceSetCompositeService.HasMultipleParents(@in.Uri)).WithMessage("in resource {PropertyValue} only has one parent. delete the child {PropertyValue} instead");
        }
    }
}