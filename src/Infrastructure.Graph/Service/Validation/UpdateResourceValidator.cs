﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.QualityControl;
using Infrastructure.Graph.Service.Composite;

namespace Infrastructure.Graph.Service.Validation
{
    internal class UpdateResourceValidator : AbstractValidator<ResourceInternal>
    {
        internal readonly ResourceSetCompositeService ResourceSetCompositeService;
        public UpdateResourceValidator(ResourceSetCompositeService resourceSetCompositeService)
        {
            ResourceSetCompositeService = resourceSetCompositeService;
            RuleSet("data", () =>
            {
                RuleFor(resource => resource.Data).NotNull();
            });
            RuleSet("properties", () =>
            {
                RuleFor(resource => resource.Properties).SetValidator(new PropertyDictionaryValidator()).NotNull();
            });
            ExecuteCommonRules();
        }


        private void ExecuteCommonRules()
        {
            RuleFor(resource => resource).NotNull().NotEmpty();
            RuleFor(resource => resource.ResourceMetadata).SetValidator(new ResourceMetadataValidator());
            RuleFor(resource => resource.ResourceMetadata.Id).MustAsync((id, cancel) => ResourceSetCompositeService.Exists(id)).WithMessage("resource id {PropertyValue} does not exist");
            RuleFor(resource => resource.ResourceMetadata.Uri).MustAsync((uri, cancel) => ResourceSetCompositeService.Exists(uri)).WithMessage("resource uri {PropertyValue} does not exist");
        }
    }
}
