﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reactive.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cassandra;
using Infrastructure.ConnectionProvider;
using Infrastructure.ACD.Entities;
using Infrastructure.Common.Extensions.DateTime;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Dto;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Model.Paging;
using Infrastructure.Graph.Repository;
using Infrastructure.Graph.Repository.Metadata.Cassandra;
using Infrastructure.Graph.Repository.Metadata.Solr;
using QueryOptions = Infrastructure.Graph.Model.QueryOptions;
using ResourceMetadataEntry = Infrastructure.Graph.Data.Entity.ResourceMetadataEntry;

namespace Infrastructure.Graph.Service.Core
{
    internal class ResourceMetadataCoreService
    {
        protected IConnectionProvider ConnectionProvider;
        protected SchemaContainer SchemaContainer;
        internal ResourceMetadataCassandraRepository ResourceMetadataCassandraRepository;
        internal ResourceMetadataSolrRepository ResourceMetadataSolrRepository;
        public ResourceMetadataCoreService(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
        {
            ConnectionProvider = connectionProvider;
            SchemaContainer = schemaContainer;
            ResourceMetadataCassandraRepository = new ResourceMetadataCassandraRepository(ConnectionProvider, SchemaContainer);
            ResourceMetadataSolrRepository = new ResourceMetadataSolrRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(ResourceMetadataInternal entry)
        {
            var resourceMetadataEntry = Mapper.Map<ResourceMetadataEntry>(entry);
            return ResourceMetadataCassandraRepository.Add(resourceMetadataEntry);
        }

        public Task Delete(Guid id)
        {
            return ResourceMetadataCassandraRepository.Delete(id);
        }
        //TODO look into this

//        public Task Update(Guid id, Expression<Func<ResourceMetadataInternal, bool>> selector)
//        {
//            var mappedSelector = selector.RemapForType<ResourceMetadataInternal, ResourceMetadataEntry, bool>();
//            return ResourceMetadataCassandraRepository.Update(id, mappedSelector);
//        }
//
//        public Task Update(Guid id, ResourceMetadataInternal resourceMetadataInternal)
//        {
//            var resourceMetadataEntry = AutoMapper.Mapper.Map<ResourceMetadataEntry>(resourceMetadataInternal);
//            Expression<Func<ResourceMetadataEntry, ResourceMetadataEntry>> func = x => resourceMetadataEntry;
//            return ResourceMetadataCassandraRepository.Update(id, func);
//        }

        public Task UpdateChangeTimestamp(Guid id)
        {
            return UpdateChangeTimestamp(id, DateTimeOffset.UtcNow);
        }

        public Task UpdateChangeTimestamp(Guid id, DateTimeOffset timestamp)
        {
            return ResourceMetadataCassandraRepository.UpdateChangeTimestamp(id, timestamp, timestamp.ToLocalCassandraTime());
        }

        public Task UpdateChangeTimestampAndUserInfo(Guid id, DateTimeOffset timestamp, Guid userId, string userName)
        {
            return ResourceMetadataCassandraRepository.UpdateChangeTimestampAndUserInfo(id, timestamp, timestamp.ToLocalCassandraTime(), userId, userName);
        }

        public async Task<ResourceMetadataInternal> Get(Guid id)
        {
            var resourceMetadataEntry = await ResourceMetadataCassandraRepository.Get(id).ConfigureAwait(false);
            //include the rest of the stuff
            return Mapper.Map<ResourceMetadataInternal>(resourceMetadataEntry);
        }

        public async Task<bool> TryGet(Guid id, Ref<ResourceMetadataInternal> @ref)
        {
            var internalRef = new Ref<ResourceMetadataEntry>();
            if (await ResourceMetadataCassandraRepository.TryGet(id, internalRef).ConfigureAwait(false))
            {
                @ref.Value = Mapper.Map<ResourceMetadataInternal>(internalRef.Value);
                return true;
            }
            return false;
        }

        public async Task<ResourceMetadataInternal> Get(string uri)
        {
            var resourceMetadataEntry = await ResourceMetadataSolrRepository.Get(uri).ConfigureAwait(false);
            return Mapper.Map<ResourceMetadataInternal>(resourceMetadataEntry);
        }

        public async Task<bool> TryGet(string uri, Ref<ResourceMetadataInternal> @ref)
        {
            var internalRef = new Ref<ResourceMetadataSolrEntry>();
            if (await ResourceMetadataSolrRepository.TryGet(uri, internalRef).ConfigureAwait(false))
            {
                @ref.Value = Mapper.Map<ResourceMetadataInternal>(internalRef.Value);
                return true;
            }
            return false;
        }

        public IObservable<ResourceMetadataInternal> Get(List<Guid> ids)
        {
            return ResourceMetadataSolrRepository.Get(ids).Select(Mapper.Map<ResourceMetadataInternal>);
        }

        public IObservable<ResourceMetadataInternal> SolrReadPaged(string resourceUri, List<Guid> useridentities, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            return ResourceMetadataSolrRepository.ReadPaged(resourceUri, useridentities, perm, resourceType, startCd, endCd, query).Select(Mapper.Map<ResourceMetadataInternal>);
        }

        public Task<PagedResult<ResourceMetadataSolrEntry>> SolrReadDescendantsPaged(string resourceUri, List<Guid> useridentities, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null,bool skipAclCheck=false)
        {
            return ResourceMetadataSolrRepository.ReadDescendantsPaged(useridentities, perm, resourceType, startCd, endCd, query, skipAclCheck, resourceUri);
        }

        public Task<PagedResult<ResourceMetadataSolrEntry>> SolrReadDescendantsPaged(string[] resourceUris, List<Guid> useridentities, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null,bool skipAclCheck=false)
        {
            return ResourceMetadataSolrRepository.ReadDescendantsPaged(useridentities, perm, resourceType, startCd, endCd, query, skipAclCheck, resourceUris);
        }

        public Task<PagedResult<ResourceMetadataSolrEntry>> ReadRecentDescendantsPaged(string resourceUri, List<Guid> useridentities, int noOfDays, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            return ResourceMetadataSolrRepository.ReadRecentDescendantsPaged(resourceUri, useridentities, noOfDays, perm, resourceType, startCd, endCd, query);
        }

        public Task<bool> SolrReadHasChildren(string uri, string resourceType, List<Guid> useridentities, Permissions perm = Permissions.Read)
        {
            return ResourceMetadataSolrRepository.SolrReadHasChildren(uri, resourceType, useridentities, perm);
        }

        public Task<ResourceMetadataSolrEntry> ReadMetadataByCondition(Condition[] conditions , List<Guid> useridentities, Permissions perm = Permissions.Read)
        {
            return ResourceMetadataSolrRepository.ReadMetadataByCondition(conditions, useridentities, perm);
        }

        public IObservable<ResourceMetadataInternal> SolrReadWithIds(string resourceUri, List<Guid> reosurceIds, List<Guid> useridentities, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            return ResourceMetadataSolrRepository.SolrReadWithIds(resourceUri, reosurceIds, useridentities, perm, resourceType, startCd, endCd, query).Select(Mapper.Map<ResourceMetadataInternal>);
        }

        public IObservable<ResourceMetadataInternal> SolrReadRecentWithIds(string resourceUri, List<Guid> reosurceIds, List<Guid> useridentities, int noOfDays, Permissions perm = Permissions.Read, string resourceType = null, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            return ResourceMetadataSolrRepository.SolrReadRecentWithIds(resourceUri, reosurceIds, useridentities, noOfDays, perm, resourceType, startCd, endCd, query).Select(Mapper.Map<ResourceMetadataInternal>);
        }
    }
}
