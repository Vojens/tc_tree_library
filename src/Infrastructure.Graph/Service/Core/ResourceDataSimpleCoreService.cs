﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Infrastructure.ConnectionProvider;
using Infrastructure.Common.Compressor;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.Common.Extensions.Hash;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Mapping;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Repository;
using Infrastructure.Graph.Repository.Data.Cassandra;
using Infrastructure.Common;
using Infrastructure.Graph.Repository.Resource.Cassandra;
using Infrastructure.Common.Model;

namespace Infrastructure.Graph.Service.Core
{
    internal class ResourceDataSimpleCoreService
    {
        protected IConnectionProvider ConnectionProvider;
        protected SchemaContainer SchemaContainer;
        internal ResourceDataSimpleCassandraRepository ResourceDataSimpleCassandraRepository;
        internal ResourceDataHashCassandraRepository ResourceDataHashCassandraRepository;
        internal ResourceDataSizeCassandraRepository ResourceDataSizeCassandraRepository;
        public ResourceDataSimpleCoreService(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
        {
            ConnectionProvider = connectionProvider;
            SchemaContainer = schemaContainer;
            ResourceDataSimpleCassandraRepository = new ResourceDataSimpleCassandraRepository(ConnectionProvider, SchemaContainer);
            ResourceDataHashCassandraRepository = new ResourceDataHashCassandraRepository(ConnectionProvider, SchemaContainer);
            ResourceDataSizeCassandraRepository = new ResourceDataSizeCassandraRepository(ConnectionProvider, SchemaContainer);
        }

        public async Task Add(Guid id, ResourceDataInternal resourceData, string resourceSystem, string type)
        {
            var content = resourceData.Content;
            var hashAsync = await content.GetHashAsync(HashAlgorithmType.CityHash, 128).ConfigureAwait(false);  //hash of the actual resource regardless of the compression
            var compression = "";
            if (resourceData.Content.Length > Global.CompressionMinSizeInBytes)
            {
                compression = CompressionStringType.ToCompressionStringType(Global.DefaultCompressionType);
                content = await content.CompressAsync(Global.DefaultCompressionType).ConfigureAwait(false);
            }

            var resourceDataSimpleEntry = new ResourceDataSimpleEntry
            {
                Id = id,
                Compression = compression,
                Data = content,
                Encoding = resourceData.ContentEncoding,
                MTime = DateTimeOffset.UtcNow,
                ResourceSystem = resourceSystem,
                Type = type
            };

            var resourceDataHashEntry = new ResourceDataHashEntry
            {
                Id = id,
                Type = Global.DefaultHashName,
                Hash = null,
                ResourceSystem = resourceSystem
            };

            var resourceDataSizeEntry = new ResourceDataSizeEntry
            {
                Id = id,
                Size = content.LongLength
            };

            var tasks = new List<Task>();
            tasks.Add(ResourceDataSizeCassandraRepository.Add(resourceDataSizeEntry));
            tasks.Add(ResourceDataSimpleCassandraRepository.Add(resourceDataSimpleEntry));

            //tasks.Add(hashAsync.ContinueWith(async c =>
            //{
            //    resourceDataHashEntry.Hash = await c.ConfigureAwait(false);
            //    await ResourceDataHashCassandraRepository.Add(resourceDataHashEntry).ConfigureAwait(false);
            //}));


            await tasks.Await().ConfigureAwait(false);

            resourceDataHashEntry.Hash = hashAsync;
            await ResourceDataHashCassandraRepository.Add(resourceDataHashEntry).ConfigureAwait(false);
        }

        public async Task<ResourceDataInternal> Get(Guid id)
        {
            var resourceDataSimpleEntry = await ResourceDataSimpleCassandraRepository.Get(id).ConfigureAwait(false);
            var resourceData = new ResourceDataInternal
            {
                Content = resourceDataSimpleEntry.Data,
                ContentEncoding = resourceDataSimpleEntry.Encoding,
                ModifyTimestamp = resourceDataSimpleEntry.MTime
            };
            var compressionType = CompressionStringType.ToCompressionType(resourceDataSimpleEntry.Compression);
            if (compressionType != CompressionType.None)
            {
                resourceData.Content = await resourceDataSimpleEntry.Data.DecompressAsync(compressionType).ConfigureAwait(false);
            }
            return resourceData;
        }

        public async Task<ResourceDataInternal> TryGet(Guid id)
        {
            var resourceDataSimpleEntry = await ResourceDataSimpleCassandraRepository.TryGet(id).ConfigureAwait(false);
            if (resourceDataSimpleEntry == null) return null;

            var resourceData = new ResourceDataInternal
            {
                Content = resourceDataSimpleEntry.Data,
                ContentEncoding = resourceDataSimpleEntry.Encoding,
                ModifyTimestamp = resourceDataSimpleEntry.MTime
            };
            var compressionType = CompressionStringType.ToCompressionType(resourceDataSimpleEntry.Compression);
            if (compressionType != CompressionType.None)
            {
                resourceData.Content = await resourceDataSimpleEntry.Data.DecompressAsync(compressionType).ConfigureAwait(false);
            }
            return resourceData;
        }

        public IObservable<KeyValuePair<Guid, ResourceDataInternal>> Get(List<Guid> ids)
        {
            return Observable.Create<KeyValuePair<Guid, ResourceDataInternal>>(async o =>
            {
                try
                {
                    var resourceDataSimpleEntries = await ResourceDataSimpleCassandraRepository.Get(ids).AsAsyncList().ConfigureAwait(false);
                    foreach (var resourceDataSimpleEntry in resourceDataSimpleEntries)
                    {
                        var resourceData = new ResourceDataInternal
                        {
                            Content = resourceDataSimpleEntry.Data,
                            ContentEncoding = resourceDataSimpleEntry.Encoding,
                            ModifyTimestamp = resourceDataSimpleEntry.MTime
                        };
                        var compressionType = CompressionStringType.ToCompressionType(resourceDataSimpleEntry.Compression);
                        if (compressionType != CompressionType.None)
                        {
                            resourceData.Content = await resourceDataSimpleEntry.Data.DecompressAsync(compressionType).ConfigureAwait(false);
                        }
                        o.OnNext(new KeyValuePair<Guid, ResourceDataInternal>(resourceDataSimpleEntry.Id, resourceData));
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }
        
        public async Task Update1(Guid id, ResourceDataInternal resourceData, string resourceSystem, string type)
        {
            var resourceDataHashEntry = await ResourceDataHashCassandraRepository.Get(id).ConfigureAwait(false);
            if (resourceDataHashEntry.Type != Global.DefaultHashName)
            {
                throw new NotSupportedException(string.Format("hash of type {0} for id {1} is not supported", resourceDataHashEntry.Type, id));
            }
            var hash = await resourceData.Content.GetHashAsync(HashAlgorithmType.CityHash, 128).ConfigureAwait(false);
            if (hash.SequenceEqual(resourceDataHashEntry.Hash))
            {
                return;
            }
            await Add(id, resourceData, resourceSystem, type).ConfigureAwait(false);    //in this case hash will be calculated twice.. but compressin might play a rule here
        }
        public async Task Update(Guid id, ResourceDataInternal resourceData, string resourceSystem, string type)
        {
            var refResData = new Ref<ResourceDataHashEntry>();
            if (await ResourceDataHashCassandraRepository.TryGet(id, refResData).ConfigureAwait(false))
            {
                var resourceDataHashEntry = refResData.Value;
                if (resourceDataHashEntry.Type != Global.DefaultHashName)
                {
                    throw new NotSupportedException(string.Format("hash of type {0} for id {1} is not supported", resourceDataHashEntry.Type, id));
                }
                var hash = await resourceData.Content.GetHashAsync(HashAlgorithmType.CityHash, 128).ConfigureAwait(false);
                if (hash.SequenceEqual(resourceDataHashEntry.Hash))
                {
                    return;
                }
            }
            await Add(id, resourceData, resourceSystem, type).ConfigureAwait(false);    //in this case hash will be calculated twice.. but compressin might play a rule here
        }

        public Task Delete(Guid id)
        {
            var tasks = new List<Task>
            {
                ResourceDataSimpleCassandraRepository.Delete(id),
                ResourceDataHashCassandraRepository.Delete(id),
                ResourceDataSizeCassandraRepository.Delete(id)
            };
            return tasks.Await();
        }
    }
}
