﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Infrastructure.ConnectionProvider;
using Infrastructure.ACD.Entities;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.Paging;
using Infrastructure.Graph.Repository;
using Infrastructure.Graph.Repository.Audit.Cassandra;
using Infrastructure.Graph.Repository.Audit.Solr;

namespace Infrastructure.Graph.Service.Core
{
    internal class ResourceAuditCoreService
    {
        protected IConnectionProvider ConnectionProvider;
        protected SchemaContainer SchemaContainer;
        internal ResourceAuditCassandraRepository ResourceAuditCassandraRepository;
        internal ResourceAuditSolrRepository ResourceAuditSolrRepository;

        public ResourceAuditCoreService(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
        {
            ConnectionProvider = connectionProvider;
            SchemaContainer = schemaContainer;
            ResourceAuditCassandraRepository = new ResourceAuditCassandraRepository(ConnectionProvider, SchemaContainer);
            ResourceAuditSolrRepository = new ResourceAuditSolrRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(ResourceAudit entry)
        {
            var resourceAuditEntry = Mapper.Map<ResourceAuditEntry>(entry);
            return ResourceAuditCassandraRepository.Add(resourceAuditEntry);
        }

        public async Task<ResourceAudit> Get(Guid resourceId, Guid id)
        {
            var resourceAuditEntry = await ResourceAuditCassandraRepository.Get(resourceId, id).ConfigureAwait(false);
            //include the rest of the stuff
            return Mapper.Map<ResourceAudit>(resourceAuditEntry);
        }

        public Task<PagedResult<ResourceAudit>> SolrReadPaged(string resourceUri, List<Guid> useridentities, Permissions perm = Permissions.Read, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            return ResourceAuditSolrRepository.ReadPaged(resourceUri, useridentities, perm, startCd, endCd, query);
        }
    }
}
