﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Infrastructure.ConnectionProvider;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Model.Internal;
using Infrastructure.Graph.Repository;
using Infrastructure.Graph.Repository.Resource.Cassandra;
using Infrastructure.Graph.Repository.Resource.Solr;
using Infrastructure.Graph.Service.Core.Helper;

namespace Infrastructure.Graph.Service.Core
{
    internal class ResourceSetCoreService
    {
        protected IConnectionProvider ConnectionProvider;
        protected SchemaContainer SchemaContainer;
        internal ResourceSetCassandraRepository ResourceSetCassandraRepository;
        internal ResourceSetCounterCassandraRepository ResourceSetCounterCassandraRepository;
        internal ResourceCurveDistanceCounterCassandraRespository ResourceCurveDistanceCounterCassandraRespository;
        internal ResourceSetSolrRepository ResourceSetSolrRepository;
        internal AscendantsByResourceSolrRepository AscendantsByResourceSolrRepository;
        internal AscendantsByResourceCassandraRepository AscendantsByResourceCassandraRepository;

        public ResourceSetCoreService(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
        {
            ConnectionProvider = connectionProvider;
            SchemaContainer = schemaContainer;
            ResourceSetCassandraRepository = new ResourceSetCassandraRepository(ConnectionProvider, SchemaContainer);
            ResourceCurveDistanceCounterCassandraRespository = new ResourceCurveDistanceCounterCassandraRespository(ConnectionProvider, SchemaContainer);
            ResourceSetCounterCassandraRepository = new ResourceSetCounterCassandraRepository(ConnectionProvider, SchemaContainer);
            ResourceSetSolrRepository = new ResourceSetSolrRepository(ConnectionProvider, SchemaContainer);
            AscendantsByResourceSolrRepository = new AscendantsByResourceSolrRepository(ConnectionProvider, SchemaContainer);
            AscendantsByResourceCassandraRepository = new AscendantsByResourceCassandraRepository(ConnectionProvider, SchemaContainer);
        }

        public Task AddVertex(ResourceSetEntry entry)
        {
            //TODO check if ids/uris are not null/empty
            if (entry.OutUri != entry.InUri || entry.OutId != entry.InId)
            {
                throw new ArgumentException(
                    "this is not a valid resource that you want to add. In/Out Ids/Uris are different");
            }
            var resourceSetEntry = Mapper.Map<ResourceSetEntry>(entry);
            resourceSetEntry.CurveDistance = 0;
            resourceSetEntry.CurveDistanceBucket = 0;

            var tasks = new List<Task>();

            var resourceCruveDistanceCounterEntry = new ResourceCruveDistanceCounterEntry
            {
                OutUri = resourceSetEntry.OutUri,
                CurveDistance = 0,
                Bucket = 0,
                Counter = 1
            };

            tasks.Add(ResourceSetCounterCassandraRepository.Update(new ResourceSetCounterEntry
            {
                OutId = resourceSetEntry.OutId,
                InId = resourceSetEntry.OutId,
                CurveDistance = 0,
                Counter = 1
            }));
            //            if ((StorageType)entry.StorageType == StorageType.HardLink)
            //            {
            //                HardlinkCounterCassandraRepository.Update(new HardLinkCounterEntry()
            //                {
            //                    ResourceId = entry.,
            //                    Counter = 1
            //                })
            //            }
            tasks.Add(ResourceCurveDistanceCounterCassandraRespository.Update(resourceCruveDistanceCounterEntry));
            tasks.Add(ResourceSetCassandraRepository.Add(resourceSetEntry));


            var abre = new AscendantsByResourceEntry
            {
                OutId = resourceSetEntry.OutId,
                InId = resourceSetEntry.OutId,
                CurveDistance = 0,
                AscendantId = resourceSetEntry.InId,
                AscendantUri = resourceSetEntry.InUri,
                Type = resourceSetEntry.Type
            };
            tasks.Add(AscendantsByResourceCassandraRepository.Add(abre));

            return Task.WhenAll(tasks);
        }

        public Task AddBidirectionalEdge(ResourceSetEntry from, ResourceSetEntry to, short curveDistance)
        {
            var tasks = new List<Task>
            {
                AddUnidirectionalEdge(from,to, curveDistance),
                AddUnidirectionalEdge(to, from, (short)-curveDistance) //the negative soab
            };

            return Task.WhenAll(tasks);
        }

        public async Task AddUnidirectionalEdge(ResourceSetEntry from, ResourceSetEntry to, short curveDistance)
        {
            try
            {
                var tcEdge = new TcEdge
                {
                    Out = new Identifier { Id = @from.OutId, Uri = @from.OutUri },
                    In = new Identifier { Id = to.InId, Uri = to.InUri },
                    CurveDistance = curveDistance
                };
                var curveDistanceBucketHelperService = new CurveDistanceBucketHelperService(tcEdge, ConnectionProvider, SchemaContainer);
                var bucket = await curveDistanceBucketHelperService.FirstAsync().ConfigureAwait(false);
                var outToIn = Mapper.Map<ResourceSetEntry>(to);
                outToIn.OutUri = from.OutUri;
                outToIn.OutId = from.OutId;
                outToIn.CurveDistance = curveDistance;
                outToIn.CurveDistanceBucket = bucket;
                var tasks = new List<Task>();
                if (curveDistance >= 0)
                {
                    var rsc = new ResourceSetCounterEntry
                    {
                        OutId = @from.OutId,
                        InId = @to.InId,
                        CurveDistance = curveDistance,
                        Counter = 1
                    };
                    tasks.Add(ResourceSetCounterCassandraRepository.Update(rsc));
                }
                else// if (curveDistance < 0)
                {
                    var abre = new AscendantsByResourceEntry
                    {
                        OutId = from.OutId,
                        InId = from.OutId,
                        CurveDistance = curveDistance,
                        AscendantId = to.InId,
                        AscendantUri = to.InUri,
                        Type = from.Type
                    };
                    tasks.Add(AscendantsByResourceCassandraRepository.Add(abre));
                }

                tasks.Add(ResourceSetCassandraRepository.Add(outToIn));
                tasks.Add(curveDistanceBucketHelperService.UpdateCounter());

                await Task.WhenAll(tasks).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                throw e;
            }

        }


        public IObservable<ResourcesByCurveDistanceEntry> Read(string outUri, short curveDistance)
        {
            return ResourceCurveDistanceCounterCassandraRespository.Read(outUri, curveDistance)
                .Where(x => x.Counter > 0)
                .Select(x => ResourceSetCassandraRepository.Read(outUri, curveDistance, x.Bucket))
                .Merge();
        }

        internal IObservable<ResourcesByCurveDistanceEntry> ReadUpToRoot(string uri)
        {
            //TODO read root, read uri, read rootid to thisid, find max cd, fire from cd = 1 to cd = MAX_CD - 1
            return Observable.Create<ResourcesByCurveDistanceEntry>(async o =>
            {
                try
                {
                    short distance = 0;
                    bool isRootFound = false;
                    bool stillHasData;
                    do
                    {
                        var byCurveDistanceEntries = Read(uri, distance--).ToEnumerable();
                        stillHasData = false;
                        foreach (var resourcesByCurveDistanceEntry in byCurveDistanceEntries)
                        {
                            stillHasData = true;
                            o.OnNext(resourcesByCurveDistanceEntry);
                            if (resourcesByCurveDistanceEntry.InUri == ".")
                                isRootFound = true;
                        }
                    } while (!isRootFound && stillHasData);
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }
        
        public Task<bool> Exists(string uri)
        {
            return ResourceSetCassandraRepository.TryGet(uri, new Ref<ResourcesByCurveDistanceEntry>());
        }

        public Task<bool> Exists(Guid id)
        {
            return ResourceSetCassandraRepository.TryGet(id, new Ref<ResourceSetEntry>());
        }

        public Task<bool> Exists(string uri, Guid id)
        {
            return ResourceSetCassandraRepository.TryGet(id, id, 0, uri, uri, new Ref<ResourceSetEntry>());
        }

        public Task<bool> EdgeExists(Identifier @out, Identifier @in)
        {
            return ResourceSetCassandraRepository.TryGet(@out.Id, @in.Id, 1, @out.Uri, @in.Uri, new Ref<ResourceSetEntry>());
        }

        public Task<ResourceSetEntry> Get(Guid id)
        {
            return ResourceSetCassandraRepository.Get(id);
        }

        public async Task<ResourceSetEntry> Get(string uri)
        {
            var resourcesByCurveDistanceEntry = await ResourceSetCassandraRepository.Get(uri).ConfigureAwait(false);
            return Mapper.Map<ResourceSetEntry>(resourcesByCurveDistanceEntry);
        }

        public Task<Guid> GetId(string uri)
        {
            return ResourceSetCassandraRepository.GetId(uri);
        }

        public Task<bool> TryGetId(string uri, Ref<Guid> refId)
        {
            return ResourceSetCassandraRepository.TryGetId(uri, refId);
        }

        private Task DecrementSetCounter(Guid outId, Guid inId, short curveDistance, long counter = -1)
        {
            return ResourceSetCounterCassandraRepository.Update(new ResourceSetCounterEntry
            {
                OutId = outId,
                InId = inId,
                CurveDistance = curveDistance,
                Counter = counter
            });
        }

        private Task DecrementCurveDistanceCounter(string outUri, short curveDistance, int bucket, long counter = -1)
        {
            return ResourceCurveDistanceCounterCassandraRespository.Update(new ResourceCruveDistanceCounterEntry
            {
                OutUri = outUri,
                CurveDistance = curveDistance,
                Bucket = bucket,
                Counter = counter
            });
        }

        public async Task<bool> Delete(ResourceSetEntry entry)
        {
            var resourceSetCounterEntryRef = new Ref<ResourceSetCounterEntry>();
            if (entry.CurveDistance != 0)   //if not pointer to itself
            {
                bool resourceSetCounterEntryExists;
                if (entry.CurveDistance > 0)
                {
                    resourceSetCounterEntryExists = await
                        ResourceSetCounterCassandraRepository.TryGet(entry.OutId, entry.InId,
                            entry.CurveDistance,
                            resourceSetCounterEntryRef)
                            .ConfigureAwait(false);
                }
                else
                {
                    resourceSetCounterEntryExists = await
                        ResourceSetCounterCassandraRepository.TryGet(entry.InId, entry.OutId,
                            Math.Abs(entry.CurveDistance),
                            resourceSetCounterEntryRef)
                            .ConfigureAwait(false);
                }

                if (resourceSetCounterEntryExists)  //I AM THE GREATEST - M.A.
                {
                    if (resourceSetCounterEntryRef.Value.Counter > 1)   //there are other resources referencing this, so just decrement the counter, and leave it alone
                    {
                        await DecrementSetCounter(entry.OutId, entry.InId, entry.CurveDistance).ConfigureAwait(false);
                        return false;
                    }
                }
                else
                {
                    //shouldn't get here anyways, but if it did, then there is no resource so just return
                    return false;
                }
            }


            await DeletePrivate(entry).ConfigureAwait(false);
            return true;
        }


        public async Task<bool> DeletePair(ResourceSetEntry downEntry, ResourceSetEntry upEntry)
        {
            if (!(downEntry.CurveDistance > 0 && upEntry.CurveDistance < 0))
            {
                throw new ArgumentException("curve distance is not correct");
            }

            if (downEntry.CurveDistance != -upEntry.CurveDistance)
            {
                throw new ArgumentException("curve distances of the pair is not compatible");
            }

            var resourceSetCounterEntryRef = new Ref<ResourceSetCounterEntry>();
            var resourceSetCounterEntryExists = await
                ResourceSetCounterCassandraRepository.TryGet(downEntry.OutId, downEntry.InId,
                    downEntry.CurveDistance,
                    resourceSetCounterEntryRef)
                    .ConfigureAwait(false);

            var tasks = new List<Task>();
            if (resourceSetCounterEntryExists)  //I AM THE GREATEST - M.A.
            {
                if (resourceSetCounterEntryRef.Value.Counter > 1)   //there are other resources referencing this, so just decrement the counter, and leave it alone
                {
                    tasks.Add(DecrementSetCounter(downEntry.OutId, downEntry.InId, downEntry.CurveDistance));
                    tasks.Add(DecrementSetCounter(upEntry.OutId, upEntry.InId, upEntry.CurveDistance));
                    await tasks.Await().ConfigureAwait(false);
                    return false;
                }
            }
            else
            {
                //shouldn't get here anyways, but if it did, then there is no resource so just return
                return false;
            }


            tasks.Add(DeletePrivate(upEntry));
            tasks.Add(DeletePrivate(downEntry));
            await tasks.Await().ConfigureAwait(false);
            return true;
        }

        private async Task DeletePrivate(ResourceSetEntry entry)
        {
            var tasks = new List<Task>();
            var resourceSetCounterEntries = await
                    ResourceSetCassandraRepository.Read(entry.OutId, entry.InId)
                        .ToAsyncList()
                        .ConfigureAwait(false);
            int bucket = 0;
            if (entry.CurveDistance != 0)
            {
                var @ref = new Ref<ResourceSetEntry>();
                if (await
                    TryGet(entry.OutId, entry.InId, entry.CurveDistance, entry.OutUri, entry.InUri, @ref)
                        .ConfigureAwait(false))
                {
                    bucket = @ref.Value.CurveDistanceBucket;
                }
            }

            if (entry.CurveDistance > 0)
            {

                if (!resourceSetCounterEntries.Any())
                {
                    tasks.Add(ResourceSetCassandraRepository.Delete(entry));
                }
                tasks.Add(DecrementSetCounter(entry.OutId, entry.InId, entry.CurveDistance));
            }
            tasks.Add(ResourceSetCassandraRepository.Delete(entry));
            tasks.Add(DecrementCurveDistanceCounter(entry.OutUri, entry.CurveDistance, bucket));
            await tasks.Await().ConfigureAwait(false);
        }

        internal Task<ResourceSetEntry> Get(Guid outId, Guid inId, short curveDistance, string outUri, string inUri)
        {
            return ResourceSetCassandraRepository.Get(outId, inId, curveDistance, outUri, inUri);
        }

        internal Task<bool> TryGet(Guid outId, Guid inId, short curveDistance, string outUri, string inUri,
            Ref<ResourceSetEntry> @ref)
        {
            return ResourceSetCassandraRepository.TryGet(outId, inId, curveDistance, outUri, inUri, @ref);
        }

        public async Task<bool> HasChildren(string uri)
        {
            var resourceCruveDistanceCounterEntries = await ResourceCurveDistanceCounterCassandraRespository.Read(uri, 1).ToAsyncList().ConfigureAwait(false);
            return resourceCruveDistanceCounterEntries.Any(x => x.Counter > 0);
        }


        //public IObservable<ResourceSetEntry> SolrRead(string outUri)
        //{
        //    return ResourceSetSolrRepository.Read(outUri).Select(Mapper.Map<ResourceSetEntry>);
        //}

        //public IObservable<ResourceSetEntry> SolrRead(string outUri, string type)
        //{
        //    return ResourceSetSolrRepository.Read(outUri, type).Select(Mapper.Map<ResourceSetEntry>);
        //}

        //public IObservable<ResourceSetEntry> SolrRead(string outUri, short cd)
        //{
        //    return ResourceSetSolrRepository.Read(outUri, cd).Select(Mapper.Map<ResourceSetEntry>);
        //}

        //public IObservable<ResourceSetEntry> SolrRead(string outUri, string type, short cd)
        //{
        //    return ResourceSetSolrRepository.Read(outUri, type, cd).Select(Mapper.Map<ResourceSetEntry>);
        //}

        //public IObservable<ResourceSetEntry> SolrReadUpTo(string outUri, string type, short cd)
        //{
        //    return ResourceSetSolrRepository.ReadUpTo(outUri, type, cd).Select(Mapper.Map<ResourceSetEntry>);
        //}

        //public IObservable<ResourceSetEntry> SolrReadFrom(string outUri, string type, short cd)
        //{
        //    return ResourceSetSolrRepository.ReadFrom(outUri, type, cd).Select(Mapper.Map<ResourceSetEntry>);
        //}

        //public IObservable<ResourceSetEntry> SolrReadWindow(string outUri, string type, short startCd, short endCd)
        //{
        //    return ResourceSetSolrRepository.ReadWindow(outUri, type, startCd, endCd).Select(Mapper.Map<ResourceSetEntry>);
        //}

        //public IObservable<ResourceSetEntry> SolrReadUpTo(string outUri, short cd)
        //{
        //    return ResourceSetSolrRepository.ReadUpTo(outUri, cd).Select(Mapper.Map<ResourceSetEntry>);
        //}

        //public IObservable<ResourceSetEntry> SolrReadFrom(string outUri, short cd)
        //{
        //    return ResourceSetSolrRepository.ReadFrom(outUri, cd).Select(Mapper.Map<ResourceSetEntry>);
        //}

        //public IObservable<ResourceSetEntry> SolrReadWindow(string outUri, short startCd, short endCd)
        //{
        //    return ResourceSetSolrRepository.ReadWindow(outUri, startCd, endCd).Select(Mapper.Map<ResourceSetEntry>);
        //}

        public IObservable<ResourceSetEntry> SolrReadWithoutPermission(string outUri, string type = null)
        {
            return ResourceSetSolrRepository.ReadWithoutPermission(outUri, type).Select(Mapper.Map<ResourceSetEntry>);
        }

        public IObservable<ResourceSetEntry> SolrRead(string outUri, List<Guid> useridentities, string type = null)
        {
            return ResourceSetSolrRepository.Read(outUri, useridentities, type).Select(Mapper.Map<ResourceSetEntry>);
        }

        public IObservable<ResourceSetEntry> SolrRead(Guid id, List<Guid> useridentities, string type = null)
        {
            return ResourceSetSolrRepository.Read(id, useridentities, type).Select(Mapper.Map<ResourceSetEntry>);
        }

        public IObservable<AscendantsByResourceEntry> SolrReadWindowAll(string outUri, short startCd, short endCd, List<Guid> useridentities, string resourceType = null)
        {
            return AscendantsByResourceSolrRepository.ReadWindowAll(outUri, startCd, endCd, useridentities, resourceType).Select(Mapper.Map<AscendantsByResourceEntry>);
        }

        public IObservable<AscendantsByResourceEntry> SolrReadWindowAll(Guid outId, short startCd, short endCd, List<Guid> useridentities, string resourceType = null)
        {
            return AscendantsByResourceSolrRepository.ReadWindowAll(outId, startCd, endCd, useridentities, resourceType).Select(Mapper.Map<AscendantsByResourceEntry>);
        }

        public IObservable<AscendantsByResourceEntry> SolrReadIn(Guid resId, short startCd, short endCd, List<Guid> useridentities, params Condition[] conditions)
        {
            return AscendantsByResourceSolrRepository.ReadIn(resId, startCd, endCd, useridentities, conditions).Select(Mapper.Map<AscendantsByResourceEntry>);
        }
        
        public IObservable<AscendantsByResourceEntry> SolrReadWindowAllWithoutPermission(string outUri, short startCd, short endCd, string resourceType = null)
        {
            return AscendantsByResourceSolrRepository.ReadWindowAllWithoutPermission(outUri, startCd, endCd, resourceType).Select(Mapper.Map<AscendantsByResourceEntry>);
        }

    }
}
