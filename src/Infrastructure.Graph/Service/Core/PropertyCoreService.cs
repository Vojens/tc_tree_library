﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.Common.Util;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Repository;
using Infrastructure.Graph.Repository.Property.Cassandra;
using Infrastructure.Graph.Repository.Property.Solr;
using Infrastructure.Common;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Model.Filter;

namespace Infrastructure.Graph.Service.Core
{
    internal class PropertyCoreService
    {
        protected IConnectionProvider ConnectionProvider;
        protected SchemaContainer SchemaContainer;
        internal PropertyCassandraRepository PropertyCassandraRepository;
        internal PropertySolrRepository PropertySolrRepository;
        public PropertyCoreService(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
        {
            ConnectionProvider = connectionProvider;
            SchemaContainer = schemaContainer;
            PropertyCassandraRepository = new PropertyCassandraRepository(ConnectionProvider, SchemaContainer);
            PropertySolrRepository = new PropertySolrRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(Guid resourceId, IDictionary<string, HashSet<PropertyInternal>> properties)
        {
            var entries = new HashSet<PropertyEntry>();
            if (properties == null)
            {
                return TaskHelper.Completed;
            }

            foreach (var propertyGroup in properties)
            {
                var groupName = propertyGroup.Key;
                foreach (var property in propertyGroup.Value)
                {
                    var propertyEntry = new PropertyEntry
                    {
                        OutId = resourceId,
                        InId = resourceId,
                        Name = groupName,
                        Key = property.Key,
                        Type = property.Type
                    };
                    if (Global.SupportedPropertyTypesDictionary[typeof(double)] == property.Type)
                    {
                        double d;
                        if (Double.TryParse(property.Value, out d))
                        {
                            propertyEntry.DoubleValue = d;
                        }
                    }
                    else if (Global.SupportedPropertyTypesDictionary[typeof(int)] == property.Type)
                    {
                        int i;
                        if (Int32.TryParse(property.Value, out i))
                        {
                            propertyEntry.IntValue = i;
                        }
                    }
                    else
                    {
                        propertyEntry.Value = property.Value;
                    }
                    entries.Add(propertyEntry);
                }
            }
            return PropertyCassandraRepository.Add(entries);
        }

        public async Task<Guid> GetIdFromProperty(Condition filter)
        {
            var refentry = new Ref<PropertySolrEntry>();
            if (await PropertySolrRepository.TryGet(filter, refentry).ConfigureAwait(false))
                return refentry.Value.InId;
            else
                return Guid.Empty;
        }

        public IObservable<KeyValuePair<string, HashSet<PropertyInternal>>> Read(Guid id)
        {
            return Observable.Create<KeyValuePair<string, HashSet<PropertyInternal>>>(async o =>
            {
                try
                {
                    var propertyEntries = PropertySolrRepository.Read(id).ToEnumerable();
                    string currentGroupName = null;
                    var hashSet = new HashSet<PropertyInternal>();
                    foreach (var propertyEntry in propertyEntries)
                    {
                        var property = new PropertyInternal();
                        property.Key = propertyEntry.Key;
                        property.Type = propertyEntry.Type;
                        if (propertyEntry.IntValue.HasValue)
                        {
                            property.Value = propertyEntry.IntValue.Value.ToString();
                        }
                        else if (propertyEntry.DoubleValue.HasValue)
                        {
                            property.Value = propertyEntry.DoubleValue.Value.ToString();
                        }
                        else
                        {
                            property.Value = propertyEntry.Value;
                        }
                        if (currentGroupName != null && propertyEntry.Name != currentGroupName)
                        {
                            o.OnNext(new KeyValuePair<string, HashSet<PropertyInternal>>(currentGroupName, hashSet));
                            hashSet = new HashSet<PropertyInternal>();
                        }
                        hashSet.Add(property);
                        currentGroupName = propertyEntry.Name;
                    }
                    if (currentGroupName != null && hashSet.Any())
                    {
                        o.OnNext(new KeyValuePair<string, HashSet<PropertyInternal>>(currentGroupName, hashSet));
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<PropertyInternal> Read(Guid id, string groupName)
        {
            return Observable.Create<PropertyInternal>(async o =>
            {
                try
                {
                    var propertyEntries = PropertySolrRepository.Read(id, groupName).ToEnumerable();
                    foreach (var propertyEntry in propertyEntries)
                    {
                        var property = new PropertyInternal
                        {
                            Key = propertyEntry.Key,
                            Type = propertyEntry.Type
                        };
                        if (propertyEntry.IntValue.HasValue)
                        {
                            property.Value = propertyEntry.IntValue.Value.ToString();
                        }
                        else if (propertyEntry.DoubleValue.HasValue)
                        {
                            property.Value = propertyEntry.DoubleValue.Value.ToString();
                        }
                        else
                        {
                            property.Value = propertyEntry.Value;
                        }
                        o.OnNext(property);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public async Task<PropertyInternal> Read(Guid id, string groupName, string key)
        {
            var propertyEntry = await PropertySolrRepository.Read(id, groupName, key).ConfigureAwait(false);

            if (propertyEntry == null) return null;

            var property = new PropertyInternal
            {
                Key = propertyEntry.Key,
                Type = propertyEntry.Type
            };
            if (propertyEntry.IntValue.HasValue)
            {
                property.Value = propertyEntry.IntValue.Value.ToString();
            }
            else if (propertyEntry.DoubleValue.HasValue)
            {
                property.Value = propertyEntry.DoubleValue.Value.ToString();
            }
            else
            {
                property.Value = propertyEntry.Value;
            }
            return property;
        }

        public IObservable<KeyValuePair<Guid, Dictionary<string, HashSet<PropertyInternal>>>> Read(List<Guid> outIds)
        {
            return Observable.Create<KeyValuePair<Guid, Dictionary<string, HashSet<PropertyInternal>>>>(async o =>
            {
                try
                {
                    var items = await PropertySolrRepository.Read(outIds).ToAsyncList().ConfigureAwait(false);
                    var groupedPropertyEntries = items.GroupBy(x => x.InId).Select(grp => new { grp.Key, Items = grp.ToList() });

                    foreach (var groupedPropertyEntry in groupedPropertyEntries)
                    {
                        var propertyEntries = groupedPropertyEntry.Items;
                        string currentGroupName = null;
                        var hashSet = new HashSet<PropertyInternal>();
                        var dictionary = new Dictionary<string, HashSet<PropertyInternal>>();
                        foreach (var propertyEntry in propertyEntries)
                        {
                            var property = new PropertyInternal
                            {
                                Key = propertyEntry.Key,
                                Type = propertyEntry.Type
                            };
                            if (propertyEntry.IntValue.HasValue)
                            {
                                property.Value = propertyEntry.IntValue.Value.ToString();
                            }
                            else if (propertyEntry.DoubleValue.HasValue)
                            {
                                property.Value = propertyEntry.DoubleValue.Value.ToString();
                            }
                            else
                            {
                                property.Value = propertyEntry.Value;
                            }
                            if (currentGroupName != null && propertyEntry.Name != currentGroupName)
                            {
                                dictionary.Add(currentGroupName, hashSet);
                                hashSet = new HashSet<PropertyInternal>();
                            }
                            hashSet.Add(property);
                            currentGroupName = propertyEntry.Name;
                        }
                        if (currentGroupName != null && hashSet.Any())
                        {
                            dictionary.Add(currentGroupName, hashSet);
                        }
                        o.OnNext(new KeyValuePair<Guid, Dictionary<string, HashSet<PropertyInternal>>>(groupedPropertyEntry.Key, dictionary));
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        //public IObservable<PropertyEntry> Read(Guid id, string groupName)
        //{
        //    return PropertyCassandraRepository.Read(id, groupName);
        //}

        public Task<PropertyEntry> Get(Guid id, string groupName, string key)
        {
            return PropertyCassandraRepository.Get(id, groupName, key);
        }

        public IObservable<KeyValuePair<string, HashSet<PropertyInternal>>> Get(Guid id)
        {
            return Observable.Create<KeyValuePair<string, HashSet<PropertyInternal>>>(async o =>
            {
                try
                {
                    var propertyEntries = PropertyCassandraRepository.Read(id).ToEnumerable();
                    string currentGroupName = null;
                    var hashSet = new HashSet<PropertyInternal>();
                    foreach (var propertyEntry in propertyEntries)
                    {
                        var property = new PropertyInternal
                        {
                            Key = propertyEntry.Key,
                            Type = propertyEntry.Type
                        };
                        if (propertyEntry.IntValue.HasValue)
                        {
                            property.Value = propertyEntry.IntValue.Value.ToString();
                        }
                        else if (propertyEntry.DoubleValue.HasValue)
                        {
                            property.Value = propertyEntry.DoubleValue.Value.ToString();
                        }
                        else
                        {
                            property.Value = propertyEntry.Value;
                        }
                        if (currentGroupName != null && propertyEntry.Name != currentGroupName)
                        {
                            o.OnNext(new KeyValuePair<string, HashSet<PropertyInternal>>(currentGroupName, hashSet));
                            hashSet = new HashSet<PropertyInternal>();
                        }
                        hashSet.Add(property);
                        currentGroupName = propertyEntry.Name;
                    }
                    if (currentGroupName != null && hashSet.Any())
                    {
                        o.OnNext(new KeyValuePair<string, HashSet<PropertyInternal>>(currentGroupName, hashSet));
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public Task Delete(Guid id)
        {
            return PropertyCassandraRepository.Delete(id);
        }

        public Task Delete(Guid id, string groupName)
        {
            return PropertyCassandraRepository.Delete(id, groupName);
        }

        public Task Delete(Guid id, string groupName, string key)
        {
            return PropertyCassandraRepository.Delete(id, groupName, key);
        }

        public async Task<bool> DeleteIfExists(Guid id, string groupName, string key)
        {
            var appliedInfo = await PropertyCassandraRepository.DeleteIfExists(id, groupName, key).ConfigureAwait(false);
            return appliedInfo.Applied;
        }

        public Task Update(Guid id, Expression<Func<PropertyEntry, bool>> selector)
        {
            return PropertyCassandraRepository.Update(id, selector);
        }

        public IObservable<PropertySolrEntry> Search(Condition condition)
        {
            return PropertySolrRepository.Search(condition).ReadLimit(ConnectionProvider, Global.SolrConsistencyLevel, int.MaxValue);
        }
    }
}
