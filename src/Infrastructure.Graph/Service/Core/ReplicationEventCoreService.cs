using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Infrastructure.ConnectionProvider;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Repository;
using Infrastructure.Graph.Repository.Replication.Cassandra;

namespace Infrastructure.Graph.Service.Core
{
    internal class ReplicationEventCoreService
    {
        protected IConnectionProvider ConnectionProvider;
        protected SchemaContainer SchemaContainer;
        internal ReplicationEventCassandraRepository ReplicationEventCassandraRepository;

        public ReplicationEventCoreService(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
        {
            ConnectionProvider = connectionProvider;
            SchemaContainer = schemaContainer;
            ReplicationEventCassandraRepository = new ReplicationEventCassandraRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(ReplicationEventInternal entry)
        {
            var resourceMetadataEntry = Mapper.Map<ReplicationEventEntry>(entry);
            return ReplicationEventCassandraRepository.Add(resourceMetadataEntry);
        }

        public IObservable<ReplicationEventInternal> Read(Guid ruleId, sbyte status)
        {
            return ReplicationEventCassandraRepository.Read(ruleId, status).Select(Mapper.Map<ReplicationEventInternal>);
        }

        public IObservable<ReplicationEventInternal> Read(Guid ruleId, List<sbyte> status)
        {
            return ReplicationEventCassandraRepository.Read(ruleId, status).Select(Mapper.Map<ReplicationEventInternal>);
        }

        public IObservable<ReplicationEventInternal> Read(Guid ruleId, sbyte status, int limit)
        {
            return ReplicationEventCassandraRepository.Read(ruleId, status, limit).Select(Mapper.Map<ReplicationEventInternal>);
        }

        public IObservable<ReplicationEventInternal> Read(Guid ruleId, sbyte status, DateTimeOffset since, int limit)
        {
            return ReplicationEventCassandraRepository.Read(ruleId, status, since, limit).Select(Mapper.Map<ReplicationEventInternal>);
        }

        public IObservable<ReplicationEventInternal> Read(Guid ruleId, List<sbyte> status, DateTimeOffset lastTimestamp, int limit)
        {
            return ReplicationEventCassandraRepository.Read(ruleId, status, lastTimestamp, limit).Select(Mapper.Map<ReplicationEventInternal>);
        }

        public IObservable<ReplicationEventInternal> Read(Guid ruleId, List<sbyte> status, params string[] resourceType)
        {
            return ReplicationEventCassandraRepository.Read(ruleId, status, resourceType).Select(Mapper.Map<ReplicationEventInternal>);
        }

        public IObservable<ReplicationEventInternal> Read(Guid ruleId, List<sbyte> status, string resourceName, params string[] resourceType)
        {
            return ReplicationEventCassandraRepository.Read(ruleId, status, resourceName, resourceType).Select(Mapper.Map<ReplicationEventInternal>);
        }
    }
}