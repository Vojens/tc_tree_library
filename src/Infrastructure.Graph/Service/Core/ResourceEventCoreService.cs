﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Infrastructure.ConnectionProvider;
using Infrastructure.ACD.Entities;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.Paging;
using Infrastructure.Graph.Repository;
using Infrastructure.Graph.Repository.ResourceExchange.Cassandra;
using System.Reactive.Linq;

namespace Infrastructure.Graph.Service.Core
{
    internal class ResourceEventCoreService
    {
        protected IConnectionProvider ConnectionProvider;
        protected SchemaContainer SchemaContainer;
        internal ResourceEventCassandraRepository ResourceEventCassandraRepository;

        public ResourceEventCoreService(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
        {
            ConnectionProvider = connectionProvider;
            SchemaContainer = schemaContainer;
            ResourceEventCassandraRepository = new ResourceEventCassandraRepository(ConnectionProvider, SchemaContainer);
        }

        public Task Add(ResourceEventInternal entry)
        {
            var resourceEventEntry = Mapper.Map<ResourceEventEntry>(entry);
            return ResourceEventCassandraRepository.Add(resourceEventEntry);
        }

        public async Task<ResourceEventInternal> Get(Guid id,DateTimeOffset bucket)
        {
            var resourceExchangeEntry = await ResourceEventCassandraRepository.Get(id,bucket).ConfigureAwait(false);
            //include the rest of the stuff
            return Mapper.Map<ResourceEventInternal>(resourceExchangeEntry);
        }

        public IObservable<ResourceEventInternal> Get(DateTimeOffset fromDt, DateTimeOffset toDt)
        {
            return ResourceEventCassandraRepository.Get(fromDt, toDt).Select(Mapper.Map<ResourceEventInternal>);
        }
    }
}
