﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Graph.Service.Core.Helper
{
    public class ResourceEventsBucketHelperService
    {
        private int BucketSizeInDays = 7;

        public DateTimeOffset CalculateBucketFromCreationTime(DateTimeOffset eventCreationTime)
        {
            var elapsedSpan = new TimeSpan(eventCreationTime.Ticks);
            TimeSpan bucketTimeSpan = TimeSpan.FromDays(BucketSizeInDays);
            var mod = -(elapsedSpan.Ticks % bucketTimeSpan.Ticks);
            var bucket = eventCreationTime.AddTicks(mod);
            return bucket;
        }

        public DateTimeOffset GetNextBucket(DateTimeOffset bucket)
        {
            return bucket.AddDays(BucketSizeInDays);
        }
    }
}
