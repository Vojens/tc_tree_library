﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Infrastructure.ConnectionProvider;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model.Internal;
using Infrastructure.Graph.Repository;
using Infrastructure.Graph.Repository.Resource.Cassandra;

namespace Infrastructure.Graph.Service.Core.Helper
{
    internal class CurveDistanceBucketHelperService
    {
        private readonly ConcurrentDictionary<int, long> _distanceCounterDic = new ConcurrentDictionary<int, long>();
        protected readonly IConnectionProvider ConnectionProvider;
        protected readonly ResourceCurveDistanceCounterCassandraRespository CassandraRepository;
        protected readonly TcEdge TcEdge;
        public CurveDistanceBucketHelperService(TcEdge tcEdge, IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
        {
            ConnectionProvider = connectionProvider;
            TcEdge = tcEdge;
            CassandraRepository = new ResourceCurveDistanceCounterCassandraRespository(ConnectionProvider, schemaContainer);
        }

        public IObservable<int> CreateDistanceObservable(int? max = null)
        {
            return Observable.Create<int>(async o =>
            {
                try
                {
                    var distanceCounters = await CassandraRepository.Read(TcEdge.Out.Uri,
                        (short)(TcEdge.CurveDistance + 1)).ToAsyncList().ConfigureAwait(false);

                    ResourceCruveDistanceCounterEntry firstCounterAvailable = null, lastCounter = null;
                    var isFound = false;
                    long counter;


                    foreach (var distanceCounter in distanceCounters)
                    {
                        lastCounter = distanceCounter;
                        if (distanceCounter.Counter < Constants.CURVE_DISTANCE_BUCKET_SIZE)
                        {
                            isFound = true;
                            firstCounterAvailable = distanceCounter;
                            break;
                        }
                    }
                    int currentBucket;
                    if (isFound)
                    {
                        currentBucket = firstCounterAvailable.Bucket;
                        counter = firstCounterAvailable.Counter + 1;
                    }
                    else if (lastCounter == null) //there exists nothing;
                    {
                        currentBucket = 0;
                        counter = 1;
                    }
                    else
                    {
                        currentBucket = lastCounter.Bucket + 1;
                        counter = 1;
                    }

                    long tempCounter;
                    _distanceCounterDic[currentBucket] = 1;

                    o.OnNext(currentBucket);

                    if (max.HasValue)
                    {
                        for (var i = 0; i < max.Value; i++)
                        {
                            if (++counter > Constants.CURVE_DISTANCE_BUCKET_SIZE)
                            {
                                currentBucket++;
                                counter = 1;
                            }

                            _distanceCounterDic[currentBucket] = _distanceCounterDic.TryGetValue(currentBucket,
                                out tempCounter)
                                ? ++tempCounter
                                : 1;

                            o.OnNext(currentBucket);
                        }
                        o.OnCompleted();
                    }
                    else
                    {
                        while (true)
                        {
                            if (++counter > Constants.CURVE_DISTANCE_BUCKET_SIZE)
                            {
                                currentBucket++;
                                counter = 1;
                            }

                            _distanceCounterDic[currentBucket] = _distanceCounterDic.TryGetValue(currentBucket, out tempCounter)
                                ? ++tempCounter
                                : 1;

                            o.OnNext(currentBucket);

                        }
                    }

                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
            });

        }

        public async Task<int> FirstAsync()
        {
            var distanceCounters = await CassandraRepository.Read(TcEdge.Out.Uri,
                (short)(TcEdge.CurveDistance)).ToAsyncList().ConfigureAwait(false);

            ResourceCruveDistanceCounterEntry firstCounterAvailable = null, lastCounter = null;
            var isFound = false;
            long counter;


            foreach (var distanceCounter in distanceCounters)
            {
                lastCounter = distanceCounter;
                if (distanceCounter.Counter < Constants.CURVE_DISTANCE_BUCKET_SIZE)
                {
                    isFound = true;
                    firstCounterAvailable = distanceCounter;
                    break;
                }
            }
            int currentBucket;
            if (isFound)
            {
                currentBucket = firstCounterAvailable.Bucket;
            }
            else if (lastCounter == null) //there exists nothing;
            {
                currentBucket = 0;
            }
            else
            {
                currentBucket = lastCounter.Bucket + 1;
            }

            _distanceCounterDic[currentBucket] = 1;
            return currentBucket;
        }

        public Task UpdateCounter()
        {
            var outVertex = TcEdge;
            var lastCounter = _distanceCounterDic.LastOrDefault();
            var tasks = new List<Task>();
            foreach (var tempDistanceCounter in _distanceCounterDic)
            {
                var distanceCounter = new ResourceCruveDistanceCounterEntry
                {
                    OutUri = outVertex.Out.Uri,
                    CurveDistance = (short)(outVertex.CurveDistance),   //TODO is this one plus 1 or not?.. 
                    Bucket = tempDistanceCounter.Key,
                    Counter = 1 //(tempDistanceCounter.Equals(lastCounter) ? tempDistanceCounter.Value - 1 : tempDistanceCounter.Value) //TODO please check with the old value in here
                };
                if (distanceCounter.Counter != 0)
                    tasks.Add(CassandraRepository.Update(distanceCounter));
            }

            //TODO do it in a counter batch
            //            if (statements.Any())
            //                await
            //                    ConnectionProvider.Session.ExecuteBatchAsync(statements,
            //                        (new BatchStatement()).SetBatchType(BatchType.Counter)).ConfigureAwait(false);
            return tasks.Await();
        }
    }
}
