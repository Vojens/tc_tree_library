﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Cassandra;
using Cassandra.Mapping;
using FluentValidation;
using Infrastructure.ConnectionProvider;
using Infrastructure.ConnectionProvider.Config;
using Infrastructure.Graph.Exceptions;
using Infrastructure.ACD.Entities;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Dto;
using Infrastructure.Graph.Exceptions;
using Infrastructure.Graph.Mapping.Cassandra;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Model.Internal;
using Infrastructure.Graph.Model.Paging;
using Infrastructure.Graph.Repository;
using Infrastructure.Graph.Repository.Resource.Cassandra;
using Infrastructure.Graph.Service.Composite;
using Infrastructure.Graph.Service.Composite.Helper;
using Infrastructure.Graph.Service.Core;
using Infrastructure.Graph.Service.Validation;
using QueryOptions = Infrastructure.Graph.Model.QueryOptions;
using SortOrder = Infrastructure.Graph.Model.SortOrder;

namespace Infrastructure.Graph.Service
{
    public class ResourceService
    {
        internal readonly ResourceSetCompositeService ResourceSetCompositeService;
        internal readonly ResourceMetadataCoreService ResourceMetadataCoreService;
        internal readonly ResourceDataSimpleCoreService ResourceDataSimpleCoreService;
        internal readonly PropertyCoreService PropertyCoreService;
        internal readonly ResourceAuditCoreService ResourceAuditCoreService;
        internal readonly ResourceEventCoreService ResourceEventCoreService;
        internal readonly ReplicationEventCoreService ReplicationEventCoreService;
        internal readonly DistributedRowLock DistributedRowLock;
        internal readonly IConnectionProvider ConnectionProvider;
        internal readonly SchemaContainer SchemaContainer;
        internal readonly AddResourceValidator AddResourceValidator;
        internal readonly AddResourcesValidator AddResourcesValidator;
        internal readonly AddReplicationEventValidator AddReplicationEventValidator;
        internal readonly UpdateResourceValidator UpdateResourceValidator;

        internal static Dictionary<Guid, Dictionary<string, HashSet<PropertyInternal>>> EmptyGroupedProperties = new Dictionary<Guid, Dictionary<string, HashSet<PropertyInternal>>>();
        internal static Dictionary<string, HashSet<PropertyInternal>> EmptyGroupedProperty = new Dictionary<string, HashSet<PropertyInternal>>();
        internal static TaskCompletionSource<ResourceMetadataInternal> ResourceMetadataInternalEmptyTask = new TaskCompletionSource<ResourceMetadataInternal>();
        internal static TaskCompletionSource<ResourceDataInternal> ResourceDataInternalEmptyTask = new TaskCompletionSource<ResourceDataInternal>();
        internal static TaskCompletionSource<IList<KeyValuePair<Guid, ResourceDataInternal>>> ResourceDatasInternalEmptyTask = new TaskCompletionSource<IList<KeyValuePair<Guid, ResourceDataInternal>>>();
        internal static TaskCompletionSource<List<KeyValuePair<string, HashSet<PropertyInternal>>>> ResourcePropertiesInternalEmptyTask = new TaskCompletionSource<List<KeyValuePair<string, HashSet<PropertyInternal>>>>();
        internal static TaskCompletionSource<List<KeyValuePair<Guid, Dictionary<string, HashSet<PropertyInternal>>>>> ResourcesPropertiesInternalEmptyTask = new TaskCompletionSource<List<KeyValuePair<Guid, Dictionary<string, HashSet<PropertyInternal>>>>>();
        internal static TaskCompletionSource<List<ResourceSetEntry>> ParentsEmptyTask = new TaskCompletionSource<List<ResourceSetEntry>>();
        static ResourceService()
        {
            DtoDefaultConfigurator.Configure();
            ResourceMetadataInternalEmptyTask.SetResult(null);
            ResourceDataInternalEmptyTask.SetResult(null);
            ResourcePropertiesInternalEmptyTask.SetResult(null);
            ResourcesPropertiesInternalEmptyTask.SetResult(null);
            ResourceDatasInternalEmptyTask.SetResult(null);
            ParentsEmptyTask.SetResult(null);
        }

        public ResourceService(IConnectionProvider connectionProvider)
        {
            ConnectionProvider = connectionProvider;

            var mappingConfiguration = new MappingConfiguration();
            SchemaContainer = new SchemaContainer(ConnectionProvider, mappingConfiguration);
            CassandraMappingsDefaultConfigurator.Configure(SchemaContainer);

            ResourceSetCompositeService = new ResourceSetCompositeService(ConnectionProvider, SchemaContainer);
            ResourceMetadataCoreService = new ResourceMetadataCoreService(ConnectionProvider, SchemaContainer);
            ResourceDataSimpleCoreService = new ResourceDataSimpleCoreService(ConnectionProvider, SchemaContainer);
            PropertyCoreService = new PropertyCoreService(ConnectionProvider, SchemaContainer);
            ResourceAuditCoreService = new ResourceAuditCoreService(ConnectionProvider, SchemaContainer);
            ResourceEventCoreService = new ResourceEventCoreService(ConnectionProvider, SchemaContainer);
            ReplicationEventCoreService = new ReplicationEventCoreService(ConnectionProvider, SchemaContainer);
            DistributedRowLock = new DistributedRowLock(ConnectionProvider, SchemaContainer);
            AddResourceValidator = new AddResourceValidator(ResourceSetCompositeService);
            AddResourcesValidator = new AddResourcesValidator(ResourceSetCompositeService);
            AddReplicationEventValidator = new AddReplicationEventValidator();
            UpdateResourceValidator = new UpdateResourceValidator(ResourceSetCompositeService);
        }

        public Task<bool> Exists(string resourceUri)
        {
            return ResourceSetCompositeService.Exists(resourceUri);
        }

        public Task<bool> Exists(Guid resourceid)
        {
            return ResourceSetCompositeService.Exists(resourceid);
        }

        public Task<bool> HasChildren(string resourceUri)
        {
            return ResourceSetCompositeService.HasChildren(resourceUri);
        }

        public Task<bool> HasChildren(string resourceUri, string resourceType, List<Guid> useridentities, Permissions perm = Permissions.Read)
        {
            return ResourceMetadataCoreService.SolrReadHasChildren(resourceUri, resourceType, useridentities, perm);
        }

        public async Task Add(ResourceInternal resource)
        {
            var @lock = DistributedRowLock.New(resource.ResourceMetadata.Uri);
            try
            {
                await @lock
                    .WithBackoff(DowngradingConsistencyRetryPolicy.Instance)
                    .WithConsistencyLevel(ConnectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Upsert.ConsistencyLevel ?? ConsistencyLevel.EachQuorum)
                    .WithFailOnBusyLock(true)
                    .WithFailOnStaleLock(true)
                    .WithTtl(Constants.LOCK_TTL)
                    .WithTimeout(Constants.LOCK_TIMEOUT)
                    .WithMode(LockMode.ProtectedWrite)
                    .Aquire()
                    .ConfigureAwait(false);

                //TODO validations
                var utcNow = DateTimeOffset.UtcNow;

                await AddResourceValidator.ValidateAndThrowAsync(resource).ConfigureAwait(false);

                if (resource.ResourceMetadata.CreationTimestamp == DateTimeOffset.MinValue)
                    resource.ResourceMetadata.CreationTimestamp = utcNow;

                if (resource.Data != null)
                {
                    resource.Data.ModifyTimestamp = utcNow;
                }

                var resourceSetAndEdges = ResourceSetCompositeServiceHelper.GetResourceSetAndEdges(resource);
                var resourceToParentsDic = await ResourceSetCompositeService.GetResourceToParentDic(
                    new HashSet<ResourceSetEntry> { resourceSetAndEdges.Key }, resourceSetAndEdges.Value)
                    .ConfigureAwait(false);

                //if (resource.ResourceMetadata.Uri != "." && resource.ResourceMetadata.Uri != "./witsml" &&
                //    resource.ResourceMetadata.Uri != "./witsml_changelogs" && resource.ResourceMetadata.Uri != "./sea")
                //{
                //validations for parents now
                var expcetions = (from keyValuePair in resourceToParentsDic
                                  where keyValuePair.Value == null || !keyValuePair.Value.Any()
                                  select
                                      new KeyNotFoundException(string.Format("parent of resource id {0} does not exist",
                                          keyValuePair.Key))).Cast<Exception>().ToList();
                if (expcetions.Any())
                    throw new AggregateException(expcetions);
                //}

                var tasks = new List<Task>
                {
                    ResourceSetCompositeService.Add(new HashSet<ResourceSetEntry> {resourceSetAndEdges.Key},
                        resourceSetAndEdges.Value, resourceToParentsDic),
                    ResourceMetadataCoreService.Add(resource.ResourceMetadata)
                };
                if (resource.Data != null)
                    tasks.Add(ResourceDataSimpleCoreService.Add(resource.ResourceMetadata.Id, resource.Data, resource.ResourceMetadata.ResourceSystem, resource.ResourceMetadata.Type));
                if (resource.Properties != null)
                    tasks.Add(PropertyCoreService.Add(resource.ResourceMetadata.Id, resource.Properties));
                await tasks.Await().ConfigureAwait(false);

                try
                {
                    await @lock.Release().ConfigureAwait(false);
                }
                catch (Exception) { }
            }
            catch (BusyLockException e)
            {
                throw new ResourceAlreadyExistsException(e.Message);
            }
            catch (StaleLockException e)
            {
                throw new ResourceAlreadyExistsException(e.Message);
            }
            //TODO: Lock - come back after VS upgrade
            //finally
            //{
            //    try
            //    {
            //        await @lock.Release().ConfigureAwait(false);
            //    }
            //    catch (Exception){}
            //}
        }

        public async Task Add(IEnumerable<ResourceInternal> resources)
        {
            var lockedResources = new List<string>();
            var locks = resources.GroupBy(x => x.ResourceMetadata.Uri).Select(x => x.First()).Select(x => DistributedRowLock.New(x.ResourceMetadata.Uri)).ToList();
            try
            {
                var lockTasks = locks.Select(@lock =>

                    @lock
                        .WithBackoff(DowngradingConsistencyRetryPolicy.Instance)
                        .WithConsistencyLevel(ConnectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Upsert.ConsistencyLevel ?? ConsistencyLevel.EachQuorum)
                        .WithFailOnBusyLock(true)
                        .WithFailOnStaleLock(true)
                        .WithTtl(Constants.LOCK_TTL)
                        .WithTimeout(Constants.LOCK_TIMEOUT)
                        .WithMode(LockMode.ProtectedWrite)
                        .Aquire()
                    );

                await lockTasks.Await().ConfigureAwait(false);

                var utcNow = DateTimeOffset.UtcNow;

                foreach (var resource in resources)
                {
                    lockedResources.Add(resource.ResourceMetadata.Uri);
                    if (resource.ResourceMetadata.CreationTimestamp == DateTimeOffset.MinValue)
                        resource.ResourceMetadata.CreationTimestamp = utcNow;
                    if (resource.Data != null)
                    {
                        resource.Data.ModifyTimestamp = utcNow;
                    }
                }
                await AddResourcesValidator.ValidateAndThrowAsync(resources).ConfigureAwait(false);

                var tasks = new List<Task>();


                Dictionary<ResourceSetEntry, HashSet<Edge>> dic =
                    resources.Select(resource => ResourceSetCompositeServiceHelper.GetResourceSetAndEdges(resource))
                        .ToDictionary(keyValuePair => keyValuePair.Key, keyValuePair => keyValuePair.Value);
                var vertices = new HashSet<ResourceSetEntry>(dic.Keys);
                var edges = dic.Select(x => x.Value).Aggregate((hashSet, n1) => new HashSet<Edge>(hashSet.Union(n1)));

                var resourceToParentsDic =
                    await ResourceSetCompositeService.GetResourceToParentDic(vertices, edges).ConfigureAwait(false);

                //validations for parents now
                var expcetions = (from keyValuePair in resourceToParentsDic
                                  where keyValuePair.Value == null || !keyValuePair.Value.Any()
                                  select
                                      new KeyNotFoundException(string.Format("parent of resource id {0} does not exist",
                                          keyValuePair.Key))).Cast<Exception>().ToList();
                if (expcetions.Any())
                    throw new AggregateException(expcetions);

                tasks.Add(ResourceSetCompositeService.Add(vertices, edges, resourceToParentsDic));
                foreach (var resource in resources)
                {
                    tasks.Add(ResourceMetadataCoreService.Add(resource.ResourceMetadata));
                    tasks.Add(ResourceDataSimpleCoreService.Add(resource.ResourceMetadata.Id, resource.Data,
                        resource.ResourceMetadata.ResourceSystem, resource.ResourceMetadata.Type));
                    tasks.Add(PropertyCoreService.Add(resource.ResourceMetadata.Id, resource.Properties));
                }
                await tasks.Await().ConfigureAwait(false);

                try
                {
                    await locks.Select(@lock => @lock.Release()).ToList().Await().ConfigureAwait(false);
                }
                catch (Exception)
                {
                }
            }
            catch (BusyLockException e)
            {
                throw new ResourceAlreadyExistsException(e.Message);
            }
            catch (StaleLockException e)
            {
                throw new ResourceAlreadyExistsException(e.Message);
            }
            //TODO: Lock - come back after VS upgrade
            //finally
            //{
            //    try
            //    {
            //        locks.Select(@lock => @lock.Release()).ToList().Await();
            //    }
            //    catch (Exception)
            //    {
            //    }
            //}
        }

        public async Task<ResourceInternal> Get(Guid id)
        {

            var metadataTask = ResourceMetadataCoreService.Get(id);
            var dataTask = ResourceDataSimpleCoreService.Get(id);
            var readPropertiesAsync = PropertyCoreService.Read(id).ToAsyncList();
            ResourceMetadataInternal metadata = await metadataTask.ConfigureAwait(false);
            var resourceSetTask = ResourceSetCompositeService.ReadDirectParents(metadata.Uri);

            ResourceDataInternal data = await dataTask.ConfigureAwait(false);
            var parentResourceSets = await resourceSetTask.ToAsyncList().ConfigureAwait(false);
            List<KeyValuePair<string, HashSet<PropertyInternal>>> properties = await readPropertiesAsync.ConfigureAwait(false);
            var parents = new HashSet<ResourceLinkInternal>();
            foreach (var parentResourceSet in parentResourceSets)
            {
                parents.Add(new ResourceLinkInternal
                {
                    Id = parentResourceSet.InId,
                    Uri = parentResourceSet.InUri
                });
            }
            var resource = new ResourceInternal
            {
                Parents = parents,
                Data = data,
                ResourceMetadata = metadata,
                Properties = properties.ToDictionary(x => x.Key, x => x.Value)
            };
            return resource;
        }

        public async Task<ResourceInternal> Get(Guid id, FetchFlags flag)
        {
            var metadataTask = ResourceMetadataCoreService.Get(id);

            var dataTask = ResourceDataInternalEmptyTask.Task;
            var readPropertiesAsync = ResourcePropertiesInternalEmptyTask.Task;

            if (flag.HasFlag(FetchFlags.Data))
                dataTask = ResourceDataSimpleCoreService.Get(id);

            if (flag.HasFlag(FetchFlags.Properties))
                readPropertiesAsync = PropertyCoreService.Read(id).ToAsyncList();

            await Task.WhenAll(metadataTask, dataTask, readPropertiesAsync).ConfigureAwait(false);

            var metadata = await metadataTask.ConfigureAwait(false);

            var data = await dataTask.ConfigureAwait(false);
            var properties = await readPropertiesAsync.ConfigureAwait(false);

            var parents = new HashSet<ResourceLinkInternal>();
            if (flag.HasFlag(FetchFlags.Links))
            {
                var parentResourceSets = await ResourceSetCompositeService.ReadDirectParents(metadata.Uri).ToAsyncList().ConfigureAwait(false);
                foreach (var parentResourceSet in parentResourceSets)
                {
                    parents.Add(new ResourceLinkInternal
                    {
                        Id = parentResourceSet.InId,
                        Uri = parentResourceSet.InUri
                    });
                }
            }
            var resource = new ResourceInternal
            {
                Parents = parents,
                Data = data,
                ResourceMetadata = metadata,
                Properties = properties != null ? properties.ToDictionary(x => x.Key, x => x.Value) : EmptyGroupedProperty
            };
            return resource;
        }

        public async Task<bool> TryGet(Guid id, Ref<ResourceInternal> @ref, FetchFlags flag, bool includeDeleted)
        {
            var refMetadata = new Ref<ResourceMetadataInternal>();
            if(await ResourceMetadataCoreService.TryGet(id, refMetadata).ConfigureAwait(false))
            {
                if (!includeDeleted && refMetadata.Value.IsMarkedDeleted)
                {
                    return false;
                }

                var dataTask = ResourceDataInternalEmptyTask.Task;
                var readPropertiesAsync = ResourcePropertiesInternalEmptyTask.Task;

                if (flag.HasFlag(FetchFlags.Data))
                    dataTask = ResourceDataSimpleCoreService.TryGet(id);

                if (flag.HasFlag(FetchFlags.Properties))
                    readPropertiesAsync = PropertyCoreService.Read(id).ToAsyncList();

                await Task.WhenAll(dataTask, readPropertiesAsync).ConfigureAwait(false);
                
                var data = await dataTask.ConfigureAwait(false);
                var properties = await readPropertiesAsync.ConfigureAwait(false);

                var parents = new HashSet<ResourceLinkInternal>();
                if (flag.HasFlag(FetchFlags.Links))
                {
                    var parentResourceSets = await ResourceSetCompositeService.ReadDirectParents(refMetadata.Value.Uri).ToAsyncList().ConfigureAwait(false);
                    foreach (var parentResourceSet in parentResourceSets)
                    {
                        parents.Add(new ResourceLinkInternal
                        {
                            Id = parentResourceSet.InId,
                            Uri = parentResourceSet.InUri
                        });
                    }
                }
                @ref.Value = new ResourceInternal
                {
                    Parents = parents,
                    Data = data,
                    ResourceMetadata = refMetadata.Value,
                    Properties = properties != null ? properties.ToDictionary(x => x.Key, x => x.Value) : EmptyGroupedProperty
                };
                return true;
            }
            return false;            
        }

        public Task<bool> TryGetId(string uri,Ref<Guid> refId)
        {
            return ResourceSetCompositeService.TryGetId(uri, refId);
        }

        public async Task<bool> TryGet(Guid id, Ref<ResourceInternal> @ref)
        {
            if (await ResourceSetCompositeService.Exists(id).ConfigureAwait(false))
            {
                var metadataTask = ResourceMetadataCoreService.Get(id);
                var dataTask = ResourceDataSimpleCoreService.Get(id);
                var readPropertiesAsync = PropertyCoreService.Read(id).ToAsyncList();
                ResourceMetadataInternal metadata = await metadataTask.ConfigureAwait(false);
                var resourceSetTask = ResourceSetCompositeService.ReadDirectParents(metadata.Uri);

                ResourceDataInternal data = await dataTask.ConfigureAwait(false);
                var parentResourceSets = await resourceSetTask.ToAsyncList().ConfigureAwait(false);
                List<KeyValuePair<string, HashSet<PropertyInternal>>> properties =
                    await readPropertiesAsync.ConfigureAwait(false);
                var parents = new HashSet<ResourceLinkInternal>();
                foreach (var parentResourceSet in parentResourceSets)
                {
                    parents.Add(new ResourceLinkInternal
                    {
                        Id = parentResourceSet.InId,
                        Uri = parentResourceSet.InUri
                    });
                }
                var resource = new ResourceInternal
                {
                    Parents = parents,
                    Data = data,
                    ResourceMetadata = metadata,
                    Properties = properties.ToDictionary(x => x.Key, x => x.Value)
                };
                @ref.Value = resource;
                return true;
            }
            return false;
        }

        public async Task<ResourceMetadataInternal> Delete(string uri, Guid id)
        {

            var notExistsTask = ResourceSetCompositeService.NotExists(uri, id);
            var hasChildrenTask = ResourceSetCompositeService.HasChildren(uri);

            if (await notExistsTask.ConfigureAwait(false))
            {
                throw new KeyNotFoundException(string.Format("resource with id {0} and uri {1} does not exist", id, uri));
            }

            if (await hasChildrenTask.ConfigureAwait(false))
            {
                throw new InvalidOperationException(string.Format("cannot delete resource  uri {0} id {1} that has children. Use cascade delete", uri, id));
            }

            return await DeleteInternal(uri, id).ConfigureAwait(false);
        }

        internal async Task<ResourceMetadataInternal> DeleteInternal(string uri, Guid id)
        {
            var @lock = DistributedRowLock.New(uri);
            try
            {
                await @lock
                    .WithBackoff(DowngradingConsistencyRetryPolicy.Instance)
                    .WithConsistencyLevel(ConnectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Delete.ConsistencyLevel ?? ConsistencyLevel.EachQuorum)
                    .WithFailOnBusyLock(true)
                    .WithFailOnStaleLock(true)
                    .WithTtl(Constants.LOCK_TTL)
                    .WithTimeout(Constants.LOCK_TIMEOUT)
                    .WithMode(LockMode.ProtectedDelete)
                    .Aquire()
                    .ConfigureAwait(false);

                var deleteTask = ResourceSetCompositeService.Delete(uri);
                var tasks = new List<Task>
                {
                    deleteTask,
                    ResourceMetadataCoreService.Delete(id),
                    ResourceDataSimpleCoreService.Delete(id),
                    PropertyCoreService.Delete(id)
                };
                await tasks.Await().ConfigureAwait(false);
                var resourceSetEntry = await deleteTask.ConfigureAwait(false);


                try
                {
                    await @lock.Release().ConfigureAwait(false);
                }
                catch (Exception) { }

                return new ResourceMetadataInternal()
                {
                    Id = resourceSetEntry.InId,
                    Uri = resourceSetEntry.InUri,
                    Type = resourceSetEntry.Type,
                    Name = resourceSetEntry.Name
                };
            }
            catch (BusyLockException e)
            {
                throw new ResourceNotFoundException(e.Message);
            }
            catch (StaleLockException e)
            {
                throw new ResourceNotFoundException(e.Message);
            }
            //TODO: Lock - come back after VS upgrade
            //finally
            //{
            //    try
            //    {
            //        @lock.Release();
            //    }
            //    catch (Exception) { }
            //}
        }

        public async Task<List<ResourceMetadataInternal>> CascadeDelete(string uri)
        {
            var rowLocks = new List<ColumnPrefixDistributedRowLock>();
            //var @lock = DistributedRowLock.New(uri);
            //rowLocks.Add(@lock);
            try
            {
                //await @lock
                //    .WithBackoff(DowngradingConsistencyRetryPolicy.Instance)
                //    .WithConsistencyLevel(ConsistencyLevel.EachQuorum)
                //    .WithFailOnBusyLock(true)
                //    .WithFailOnStaleLock(true)
                //    .WithTtl(Constants.LOCK_TTL)
                //    .WithTimeout(Constants.LOCK_TIMEOUT)
                //    .WithMode(LockMode.ProtectedDelete)
                //    .Aquire()
                //    .ConfigureAwait(false);

                var result = await GetResources(uri).ConfigureAwait(false);
                var resourcesToDelete = result.Item1;
                var lockTasks = new List<Task>();

                foreach (var resource in resourcesToDelete)
                {
                    var disLock = DistributedRowLock.New(resource.InUri);
                    lockTasks.Add(disLock
                    .WithBackoff(DowngradingConsistencyRetryPolicy.Instance)
                    .WithConsistencyLevel(ConnectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Delete.ConsistencyLevel ?? ConsistencyLevel.EachQuorum)
                    .WithFailOnBusyLock(true)
                    .WithFailOnStaleLock(true)
                    .WithTtl(Constants.LOCK_TTL)
                    .WithTimeout(Constants.LOCK_TIMEOUT)
                    .WithMode(LockMode.ProtectedDelete)
                    .Aquire());
                    rowLocks.Add(disLock);
                }
                await lockTasks.Await().ConfigureAwait(false);

                await ResourceSetCompositeService.CascadeDelete(uri, resourcesToDelete, result.Item2).ConfigureAwait(false);
                var tasks = new List<Task>();
                foreach (var resourcesByCurveDistanceEntry in resourcesToDelete)
                {
                    tasks.Add(ResourceMetadataCoreService.Delete(resourcesByCurveDistanceEntry.InId));
                    tasks.Add(ResourceDataSimpleCoreService.Delete(resourcesByCurveDistanceEntry.InId));
                    tasks.Add(PropertyCoreService.Delete(resourcesByCurveDistanceEntry.InId));
                }
                await tasks.Await().ConfigureAwait(false);

                try
                {
                    await rowLocks.Select(row => row.Release()).ToList().Await().ConfigureAwait(false);
                }
                catch (Exception) { }

                return
                    resourcesToDelete.Select(
                        resourcesByCurveDistanceEntry => new ResourceMetadataInternal()
                        {
                            Id = resourcesByCurveDistanceEntry.InId,
                            Uri = resourcesByCurveDistanceEntry.InUri,
                            Type = resourcesByCurveDistanceEntry.Type,
                            Name = resourcesByCurveDistanceEntry.Name
                        }).ToList();
            }
            catch (BusyLockException e)
            {
                throw new ResourceNotFoundException(e.Message);
            }
            catch (StaleLockException e)
            {
                throw new ResourceNotFoundException(e.Message);
            }
            //TODO: Lock - come back after VS upgrade
            //finally
            //{
            //    try
            //    {
            //        rowLocks.Select(row => row.Release()).ToList().Await();
            //    }
            //    catch (Exception) { }
            //}
        }

        private async Task<Tuple<List<ResourcesByCurveDistanceEntry>, List<Task<KeyValuePair<ResourcesByCurveDistanceEntry, List<ResourcesByCurveDistanceEntry>>>>>> GetResources(string uri)
        {
            short distance = 0;
            bool isDone = false;
            var fetchTasks =
                new List<Task<KeyValuePair<ResourcesByCurveDistanceEntry, List<ResourcesByCurveDistanceEntry>>>>();
            var deletedResources = new List<ResourcesByCurveDistanceEntry>();
            do
            {
                List<ResourcesByCurveDistanceEntry> resourcesByCurveDistanceEntries =
                    await ResourceSetCompositeService.ReadCurveDistanceEntries(uri, distance++).ConfigureAwait(false);
                if (!resourcesByCurveDistanceEntries.Any())
                {
                    isDone = true;
                }
                else
                {
                    deletedResources.AddRange(resourcesByCurveDistanceEntries);
                    fetchTasks.AddRange(resourcesByCurveDistanceEntries.Select(ResourceSetCompositeService.FetchUntilRoot));
                }
            } while (!isDone);
            return new Tuple<List<ResourcesByCurveDistanceEntry>, List<Task<KeyValuePair<ResourcesByCurveDistanceEntry, List<ResourcesByCurveDistanceEntry>>>>>(deletedResources, fetchTasks);
        }

        public Task AddLink(ResourceLinkInternal @out, ResourceLinkInternal @in)
        {
            return ResourceSetCompositeService.AddEdge(Edge.CreateEdge(@out, @in));
        }

        public Task DeleteLink(ResourceLinkInternal @out, ResourceLinkInternal @in)
        {
            return ResourceSetCompositeService.RemoveEdge(Edge.CreateEdge(@out, @in));
        }

        public async Task UpdateData(ResourceInternal resource)
        {
            await UpdateResourceValidator.ValidateAndThrowAsync(resource, "data").ConfigureAwait(false);
            await ResourceDataSimpleCoreService.Update(resource.ResourceMetadata.Id, resource.Data,
                resource.ResourceMetadata.ResourceSystem, resource.ResourceMetadata.Type).ConfigureAwait(false);
        }

        public async Task UpdateMetadata(ResourceInternal resource)
        {
            await UpdateResourceValidator.ValidateAndThrowAsync(resource).ConfigureAwait(false);
            var metadata = resource.ResourceMetadata;
            var fetchedMetadata = await ResourceMetadataCoreService.Get(resource.ResourceMetadata.Id).ConfigureAwait(false);
            if (fetchedMetadata.StorageType != metadata.StorageType)
            {
                throw new InvalidOperationException("cannot change the resource type");
            }
            await ResourceMetadataCoreService.Add(metadata).ConfigureAwait(false);
        }

        public async Task UpdateResource(ResourceInternal resource)
        {
            if (resource.ResourceMetadata != null)
                await UpdateMetadata(resource).ConfigureAwait(false);
            if (resource.Properties != null)
                await UpdateProperties(resource).ConfigureAwait(false);
            if (resource.Data != null)
                await UpdateData(resource).ConfigureAwait(false);
        }

        private IObservable<ResourceInternal> FetchAsync(List<ResourceInternal> entries, FetchFlags fetch = FetchFlags.Data | FetchFlags.Links | FetchFlags.Properties)
        {
            return Observable.Create<ResourceInternal>(async o =>
            {
                try
                {
                    var outIds = entries.Select(x => x.ResourceMetadata.Id).ToList();
                    var propertiesTask = ResourcesPropertiesInternalEmptyTask.Task;
                    var datasTask = ResourceDatasInternalEmptyTask.Task;

                    if (fetch.HasFlag(FetchFlags.Properties))
                        propertiesTask = PropertyCoreService.Read(outIds).ToAsyncList();
                    if ((fetch & FetchFlags.Data) != 0)
                        datasTask = ResourceDataSimpleCoreService.Get(outIds).AsAsyncList();

                    await Task.WhenAll(propertiesTask, datasTask).ConfigureAwait(false);

                    var propertiesGroup = await propertiesTask.ConfigureAwait(false);
                    var allResourcesProperties = propertiesGroup != null ? propertiesGroup.ToDictionary(x => x.Key, x => x.Value) : EmptyGroupedProperties;
                    var datas = await datasTask.ConfigureAwait(false);
                    var allDatas = datas != null ? datas.ToDictionary(x => x.Key, x => x.Value) : new Dictionary<Guid, ResourceDataInternal>();
                    var shouldFetchParentLinks = (fetch & FetchFlags.Links) != 0;

                    foreach (var entry in entries)
                    {
                        if (allDatas.ContainsKey(entry.ResourceMetadata.Id))
                        {
                            entry.Data = allDatas[entry.ResourceMetadata.Id];
                        }

                        if (allResourcesProperties.ContainsKey(entry.ResourceMetadata.Id))
                        {
                            entry.Properties = allResourcesProperties[entry.ResourceMetadata.Id].ToDictionary(x => x.Key, x => x.Value);
                        }
                        if (shouldFetchParentLinks)
                        {
                            entry.Parents = await GetParentLinks(entry.ResourceMetadata.Uri).ConfigureAwait(false);
                        }

                        o.OnNext(entry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        private IObservable<ResourceInternal> FetchAsync(List<Guid> ids, FetchFlags fetch = FetchFlags.Data | FetchFlags.Links | FetchFlags.Properties)
        {
            return Observable.Create<ResourceInternal>(async o =>
            {
                try
                {
                    var metadatasTask = ResourceMetadataCoreService.Get(ids).ToAsyncList();
                    var propertiesTask = ResourcesPropertiesInternalEmptyTask.Task;
                    var datasTask = ResourceDatasInternalEmptyTask.Task;

                    if (fetch.HasFlag(FetchFlags.Properties))
                        propertiesTask = PropertyCoreService.Read(ids).ToAsyncList();
                    if ((fetch & FetchFlags.Data) != 0)
                        datasTask = ResourceDataSimpleCoreService.Get(ids).AsAsyncList();

                    await Task.WhenAll(metadatasTask, propertiesTask, datasTask).ConfigureAwait(false);

                    var metadatas = await metadatasTask.ConfigureAwait(false);
                    var propertiesGroup = await propertiesTask.ConfigureAwait(false);
                    var allResourcesProperties = propertiesGroup != null ? propertiesGroup.ToDictionary(x => x.Key, x => x.Value) : EmptyGroupedProperties;
                    var datas = await datasTask.ConfigureAwait(false);
                    var allDatas = datas != null ? datas.ToDictionary(x => x.Key, x => x.Value) : new Dictionary<Guid, ResourceDataInternal>();
                    var shouldFetchParentLinks = (fetch & FetchFlags.Links) != 0;
                    foreach (var metadata in metadatas)
                    {
                        var resource = new ResourceInternal
                        {
                            ResourceMetadata = metadata
                        };

                        if (allDatas.ContainsKey(metadata.Id))
                        {
                            resource.Data = allDatas[metadata.Id];
                        }

                        if (allResourcesProperties.ContainsKey(metadata.Id))
                        {
                            resource.Properties = allResourcesProperties[metadata.Id].ToDictionary(x => x.Key, x => x.Value);
                        }

                        if (shouldFetchParentLinks)
                        {
                            resource.Parents = await GetParentLinks(metadata.Uri).ConfigureAwait(false);
                        }

                        o.OnNext(resource);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public async Task<ResourceInternal> FetchAsync(ResourceMetadataInternal resourceMetadata,
            FetchFlags fetch = FetchFlags.Data | FetchFlags.Links | FetchFlags.Properties)
        {
            var resource = new ResourceInternal
            {
                ResourceMetadata = resourceMetadata
            };

            if ((fetch & FetchFlags.Data) != 0)
            {
                resource.Data = await ResourceDataSimpleCoreService.TryGet(resourceMetadata.Id).ConfigureAwait(false);
            }

            if (fetch.HasFlag(FetchFlags.Properties))
            {
                var propertiesGroup = await PropertyCoreService.Get(resourceMetadata.Id).AsAsyncList().ConfigureAwait(false);
                resource.Properties = propertiesGroup != null ? propertiesGroup.ToDictionary(x => x.Key, x => x.Value) : EmptyGroupedProperty;
            }

            if ((fetch & FetchFlags.Links) != 0)
            {
                resource.Parents = await GetParentLinks(resourceMetadata.Uri).ConfigureAwait(false);
            }
            return resource;
        }

        private async Task<HashSet<ResourceLinkInternal>> GetParentLinks(string resourceUri)
        {
            var parents = await ResourceSetCompositeService.ReadDirectParents(resourceUri).ToAsyncList().ConfigureAwait(false);
            if (parents != null)
            {
                return new HashSet<ResourceLinkInternal>(parents.Select(x => new ResourceLinkInternal()
                {
                    Id = x.InId,
                    Uri = x.InUri
                }));
            }
            return new HashSet<ResourceLinkInternal>();
        }

        public IObservable<ResourceInternal> ReadSimple(string uri, short startCd, short endCd, List<Guid> useridentities, string resourceType = null)
        {
            return ResourceSetCompositeService.SolrReadWindowAll(uri, startCd, endCd, useridentities, resourceType)
                .Select(x => new ResourceInternal
                {
                    ResourceMetadata = new ResourceMetadataInternal { Id = x.InId, Type = x.Type }
                });
        }

        public IObservable<ResourceInternal> ReadSimpleWithoutPermission(string uri, short startCd, short endCd, string resourceType = null)
        {
            return ResourceSetCompositeService.SolrReadWindowAllWithoutPermission(uri, startCd, endCd, resourceType)
                .Select(x => new ResourceInternal
                {
                    ResourceMetadata = new ResourceMetadataInternal { Id = x.InId, Type = x.Type }
                });
        }

        public IObservable<ResourceInternal> Read(string uri, short startCd, short endCd, List<Guid> useridentities, string resourceType = null, FetchFlags fetch = FetchFlags.None)
        {
            return Observable.Create<ResourceInternal>(async o =>
            {
                try
                {
                    var resourceSetEntries = ResourceSetCompositeService.SolrReadWindowAll(uri, startCd, endCd, useridentities, resourceType).ToEnumerable();
                    var splitResources = resourceSetEntries.ToList().ChunkBy(100);
                    var observables = splitResources.Select(resourceSetEntry => FetchAsync(resourceSetEntry.Select(x => x.InId).ToList(), fetch));
                    // Tune further, move this flag into fetchasync
                    observables.Merge().Where(x => x.ResourceMetadata.IsMarkedDeleted == false).Subscribe(o);
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ResourceInternal> CqlReadWithoutPermission(string uri, short cd, string resourceType = null,
            FetchFlags fetch = FetchFlags.None)
        {
            return Observable.Create<ResourceInternal>(async o =>
            {
                try
                {
                    var obs = ResourceSetCompositeService.Read(uri, cd);

                    var entriesObs = resourceType.IsNullOrDefault() ? obs : 
                        obs.Where(x => x.Type.Equals(resourceType, StringComparison.OrdinalIgnoreCase));

                    entriesObs.Select(x => new ResourceInternal
                    {
                        ResourceMetadata = new ResourceMetadataInternal { Id = x.InId, Type = x.Type, Uri = x.InUri, Name = x.Name }
                    }).Subscribe(o);
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }
        

        public IObservable<ResourceInternal> ReadWithoutPermission(string uri, short startCd, short endCd, string resourceType = null, FetchFlags fetch = FetchFlags.None)
        {
            return Observable.Create<ResourceInternal>(async o =>
            {
                try
                {
                    var resourceSetEntries = ResourceSetCompositeService.SolrReadWindowAllWithoutPermission(uri, startCd, endCd, resourceType).ToEnumerable();
                    var splitResources = resourceSetEntries.ToList().ChunkBy(100);
                    var observables = splitResources.Select(resourceSetEntry => FetchAsync(resourceSetEntry.Select(x => x.InId).ToList(), fetch));
                    // Tune further, move this flag into fetchasync
                    observables.Merge().Where(x => x.ResourceMetadata.IsMarkedDeleted == false).Subscribe(o);
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ResourceInternal> ReadWithoutPermission(string uri, string type = null, FetchFlags fetch = FetchFlags.None)
        {
            return Observable.Create<ResourceInternal>(async o =>
            {
                try
                {
                    var resourceSetEntries = ResourceSetCompositeService.SolrReadWithoutPermission(uri, type).ToEnumerable();
                    var splitResources = resourceSetEntries.ToList().ChunkBy(100);
                    var observables = splitResources.Select(resourceSetEntry => FetchAsync(resourceSetEntry.Select(x => x.InId).ToList(), fetch));
                    // Tune further, move this flag into fetchasync
                    observables.Merge().Where(x => x.ResourceMetadata.IsMarkedDeleted == false).Subscribe(o);
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ResourceInternal> ReadSimpleWithoutPermission(string uri)
        {
            return ResourceSetCompositeService.SolrReadWithoutPermission(uri, null)
                .Select(x => new ResourceInternal
                {
                    ResourceMetadata = new ResourceMetadataInternal { Id = x.InId, Uri = x.OutUri, Type = x.Type, Name = x.Name }
                });
        }

        public IObservable<ResourceInternal> ReadSimple(string uri, List<Guid> useridentities)
        {
            return ResourceSetCompositeService.SolrRead(uri, useridentities, null)
                .Select(x => new ResourceInternal
                {
                    ResourceMetadata = new ResourceMetadataInternal { Id = x.InId, Uri = x.OutUri, Type = x.Type, Name = x.Name }
                });
        }

        public IObservable<ResourceInternal> Read(string uri, List<Guid> useridentities, string type = null, FetchFlags fetch = FetchFlags.None)
        {
            return Observable.Create<ResourceInternal>(async o =>
            {
                try
                {
                    var resourceSetEntries = ResourceSetCompositeService.SolrRead(uri, useridentities, type).ToEnumerable();
                    var splitResources = resourceSetEntries.ToList().ChunkBy(100);
                    var observables = splitResources.Select(resourceSetEntry => FetchAsync(resourceSetEntry.Select(x => x.InId).ToList(), fetch));
                    // Tune further, move this flag into fetchasync
                    observables.Merge().Where(x => x.ResourceMetadata.IsMarkedDeleted == false).Subscribe(o);
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ResourceInternal> Read(Guid id, List<Guid> useridentities, string type = null, FetchFlags fetch = FetchFlags.None, bool includeDeleted = false)
        {
            return Observable.Create<ResourceInternal>(async o =>
            {
                try
                {
                    var resourceSetEntries = ResourceSetCompositeService.SolrRead(id, useridentities, type).ToEnumerable();
                    var splitResources = resourceSetEntries.ToList().ChunkBy(100);
                    var observables = splitResources.Select(resourceSetEntry => FetchAsync(resourceSetEntry.Select(x => x.InId).ToList(), fetch)).Merge();
                    // Tune further, move this flag into fetchasync
                    var obs = (!includeDeleted) ? observables.Where(x => x.ResourceMetadata.IsMarkedDeleted == false) : observables;
                    obs.Subscribe(o);
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ResourceInternal> Read(Guid id, short startCd, short endCd, List<Guid> useridentities, string resourceType = null, FetchFlags fetch = FetchFlags.None)
        {
            return Observable.Create<ResourceInternal>(async o =>
            {
                try
                {
                    var resourceSetEntries = ResourceSetCompositeService.SolrReadWindowAll(id, startCd, endCd, useridentities, resourceType).ToEnumerable();
                    var splitResources = resourceSetEntries.ToList().ChunkBy(100);
                    var observables = splitResources.Select(resourceSetEntry => FetchAsync(resourceSetEntry.Select(x => x.InId).ToList(), fetch));
                    // Tune further, move this flag into fetchasync
                    observables.Merge().Where(x => x.ResourceMetadata.IsMarkedDeleted == false).Subscribe(o);
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ResourceInternal> ReadIn(Guid resourceId, List<Guid> useridentities, short startCd = short.MinValue, short endCd = -1, FetchFlags fetch = FetchFlags.None, params Condition[] conditions)
        {
            return Observable.Create<ResourceInternal>(async o =>
            {
                try
                {
                    var resourceSetEntries = ResourceSetCompositeService.SolrReadIn(resourceId, startCd, endCd, useridentities, conditions).ToEnumerable();
                    var splitResources = resourceSetEntries.OrderBy(x => x.CurveDistance).ToList();
                    foreach (var resource in splitResources)
                    {
                        var resourceInternals = FetchAsync(new List<Guid> { resource.AscendantId }, fetch).ToEnumerable();
                        foreach (var @internal in resourceInternals)
                        {
                            o.OnNext(@internal);
                        }
                    }
                    o.OnCompleted();

                    //var resourceSetEntries = ResourceSetCompositeService.SolrReadIn(uri, startCd, endCd, useridentities, conditions).ToEnumerable();
                    //var splitResources = resourceSetEntries.OrderBy(x=>x.CurveDistance).ToList().ChunkBy(100);
                    //var observables = splitResources.Select(resourceSetEntry => FetchAsync(resourceSetEntry, fetch));
                    //observables.Merge().Subscribe(o);
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ResourceInternal> ReadPaged(string uri, List<Guid> useridentities, Permissions perm = Permissions.Read, string type = null, short startCd = 1, short endCd = short.MaxValue, FetchFlags fetch = FetchFlags.None, QueryOptions query = null)
        {
            return Observable.Create<ResourceInternal>(async o =>
            {
                try
                {
                    var resourceInternals = ResourceMetadataCoreService.SolrReadPaged(uri, useridentities, perm, type, startCd, endCd, query).Select(x => new ResourceInternal { ResourceMetadata = x }).ToEnumerable();
                    var splitResources = resourceInternals.ToList().ChunkBy(100);
                    splitResources.Select(resources => FetchAsync(resources, fetch)).Merge().Subscribe(o);
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public async Task<PagedResult<ResourceInternal>> ReadDescendantsPaged(string uri, List<Guid> useridentities, Permissions perm = Permissions.Read, string type = null, short startCd = 1, short endCd = short.MaxValue, FetchFlags fetch = FetchFlags.None, QueryOptions query = null, bool skipAclCheck = false)
        {
            var pagedResourceInternal = await ResourceMetadataCoreService.SolrReadDescendantsPaged(uri, useridentities, perm, type, startCd, endCd, query, skipAclCheck).ConfigureAwait(false);
            var splitResources = pagedResourceInternal.Data.Select(x => new ResourceInternal { ResourceMetadata = AutoMapper.Mapper.Map<ResourceMetadataInternal>(x) }).ToList().ChunkBy(100);
            var tasks = new Task<IList<ResourceInternal>>[splitResources.Count];
            for (var i = 0; i < tasks.Length; i++)
            {
                tasks[i] = FetchAsync(splitResources[i], fetch).AsAsyncList();
            }
            await tasks.Await().ConfigureAwait(false);

            var result = new List<ResourceInternal>();
            foreach (var task in tasks)
            {
                result.AddRange(await task.ConfigureAwait(false));
            }
            var pagedResult = new PagedResult<ResourceInternal>(result, pagedResourceInternal.Paging);
            return pagedResult;
        }

        public async Task<PagedResult<ResourceInternal>> ReadDescendantsPaged(string[] uris, List<Guid> useridentities, Permissions perm = Permissions.Read, string type = null, short startCd = 1, short endCd = short.MaxValue, FetchFlags fetch = FetchFlags.None, QueryOptions query = null, bool skipAclCheck = false)
        {
            var pagedResourceInternal = await ResourceMetadataCoreService.SolrReadDescendantsPaged(uris, useridentities, perm, type, startCd, endCd, query, skipAclCheck).ConfigureAwait(false);
            var splitResources = pagedResourceInternal.Data.Select(x => new ResourceInternal { ResourceMetadata = AutoMapper.Mapper.Map<ResourceMetadataInternal>(x) }).ToList().ChunkBy(100);

            var tasks = new Task<IList<ResourceInternal>>[splitResources.Count];
            for (var i = 0; i < tasks.Length; i++)
            {
                tasks[i] = FetchAsync(splitResources[i], fetch).AsAsyncList();
            }
            await tasks.Await().ConfigureAwait(false);

            var result = new List<ResourceInternal>();
            foreach (var task in tasks)
            {
                result.AddRange(await task.ConfigureAwait(false));
            }

            var pagedResult = new PagedResult<ResourceInternal>(result, pagedResourceInternal.Paging);
            return pagedResult;
        }

        public async Task<PagedResult<ResourceInternal>> ReadRecentDescendantsPaged(string uri, List<Guid> useridentities, int noOfDays, Permissions perm = Permissions.Read, string type = null, short startCd = 1, short endCd = short.MaxValue, FetchFlags fetch = FetchFlags.None, QueryOptions query = null)
        {
            var pagedResourceInternal = await ResourceMetadataCoreService.ReadRecentDescendantsPaged(uri, useridentities, noOfDays, perm, type, startCd, endCd, query).ConfigureAwait(false);
            var splitResources = pagedResourceInternal.Data.Select(x => new ResourceInternal { ResourceMetadata = AutoMapper.Mapper.Map<ResourceMetadataInternal>(x) }).ToList().ChunkBy(100);

            var tasks = new Task<IList<ResourceInternal>>[splitResources.Count];
            for (var i = 0; i < tasks.Length; i++)
            {
                tasks[i] = FetchAsync(splitResources[i], fetch).AsAsyncList();
            }
            await tasks.Await().ConfigureAwait(false);

            var result = new List<ResourceInternal>();
            foreach (var task in tasks)
            {
                result.AddRange(await task.ConfigureAwait(false));
            }

            var pagedResult = new PagedResult<ResourceInternal>(result, pagedResourceInternal.Paging);
            return pagedResult;
        }

        private static PagedResult<ResourceMetadataInternal> SortingAndPaging(List<ResourceMetadataInternal> resourceMetadatas, QueryOptions query)
        {
            if (resourceMetadatas == null || !resourceMetadatas.Any()) return null;
            switch (query.SortBy)
            {
                case SortBy.Name:
                    resourceMetadatas = query.SortOrder == SortOrder.Ascending
                        ? resourceMetadatas.OrderBy(e => e.Name).ToList()
                        : resourceMetadatas.OrderByDescending(e => e.Name).ToList();
                    break;
                case SortBy.CreatedDate:
                    resourceMetadatas = query.SortOrder == SortOrder.Ascending
                        ? resourceMetadatas.OrderBy(e => e.CreationTimestamp).ToList()
                        : resourceMetadatas.OrderByDescending(e => e.CreationTimestamp).ToList();
                    break;
                case SortBy.LastUpdatedDate:
                    resourceMetadatas = query.SortOrder == SortOrder.Ascending
                        ? resourceMetadatas.OrderBy(e => e.LastUpdateTimestamp).ToList()
                        : resourceMetadatas.OrderByDescending(e => e.LastUpdateTimestamp).ToList();
                    break;
                case SortBy.Type:
                    resourceMetadatas = query.SortOrder == SortOrder.Ascending
                        ? resourceMetadatas.OrderBy(e => e.Type).ToList()
                        : resourceMetadatas.OrderByDescending(e => e.Type).ToList();
                    break;
                default:
                    {
                        resourceMetadatas = query.SortOrder == SortOrder.Ascending
                            ? resourceMetadatas.OrderBy(e => e.Name).ToList()
                            : resourceMetadatas.OrderByDescending(e => e.Name).ToList();
                        break;
                    }
            }
            var totalCount = resourceMetadatas.Count;
            var page = Math.Max(1, query.PageNumber);
            var pageSize = Math.Max(1, query.PageSize);
            var skip = (page - 1) * pageSize;
            var pagedData = resourceMetadatas.Skip(skip).Take(pageSize);
            var pagedResult = new PagedResult<ResourceMetadataInternal>(pagedData, page, query.PageSize, totalCount);
            return pagedResult;
        }

        public async Task<PagedResult<ResourceInternal>> SearchRecentDescendantsPagedWithProperties(string uri, List<Guid> useridentities, int noOfDays, Permissions perm = Permissions.Read, string type = null, short startCd = 1, short endCd = short.MaxValue, FetchFlags fetch = FetchFlags.None, QueryOptions query = null, List<Condition> metadataConditions = null, List<Condition> propertyConditions = null)
        {
            var propIds = new List<Guid>();
            if (propertyConditions != null)
            {
                for (var i = 0; i < propertyConditions.Count; i++)
                {
                    var props = await PropertyCoreService.Search(propertyConditions[i]).ToAsyncList().ConfigureAwait(false);
                    propIds = i == 0 ? props.Select(x => x.OutId).ToList() : propIds.Intersect(props.Select(x => x.OutId).ToList()).ToList();
                }
            }
            var splitIds = propIds.ChunkBy(100);
            var metadatas = new List<ResourceMetadataInternal>();
            foreach (var ids in splitIds)
            {
                metadatas.AddRange(await ResourceMetadataCoreService.SolrReadRecentWithIds(uri, ids, useridentities, noOfDays, perm, type, startCd, endCd,
                        query).ToAsyncList().ConfigureAwait(false));
            }

            var pagedData = SortingAndPaging(metadatas, query);
            if (pagedData == null) return null;

            var splitResources = pagedData.Data.Select(x => new ResourceInternal { ResourceMetadata = AutoMapper.Mapper.Map<ResourceMetadataInternal>(x) })
                .ToList().ChunkBy(100);

            var tasks = new Task<IList<ResourceInternal>>[splitResources.Count];
            for (var i = 0; i < tasks.Length; i++)
            {
                tasks[i] = FetchAsync(splitResources[i], fetch).AsAsyncList();
            }
            await tasks.Await().ConfigureAwait(false);

            var result = new List<ResourceInternal>();
            foreach (var task in tasks)
            {
                result.AddRange(await task.ConfigureAwait(false));
            }

            var pagedResult = new PagedResult<ResourceInternal>(result, pagedData.Paging);
            return pagedResult;
        }

        public async Task<PagedResult<ResourceInternal>> SearchDescendantsPagedWithProperties(string uri, List<Guid> useridentities, Permissions perm = Permissions.Read, string type = null, short startCd = 1, short endCd = short.MaxValue, FetchFlags fetch = FetchFlags.None, QueryOptions query = null, List<Condition> metadataConditions = null, List<Condition> propertyConditions = null)
        {
            var propIds = new List<Guid>();
            if (propertyConditions != null)
            {
                for (var i = 0; i < propertyConditions.Count; i++)
                {
                    var props = await PropertyCoreService.Search(propertyConditions[i]).ToAsyncList().ConfigureAwait(false);
                    propIds = i == 0 ? props.Select(x => x.OutId).ToList() : propIds.Intersect(props.Select(x => x.OutId).ToList()).ToList();
                }
            }
            var splitIds = propIds.ChunkBy(100);
            var metadatas = new List<ResourceMetadataInternal>();
            foreach (var ids in splitIds)
            {
                if (metadataConditions != null && metadataConditions.Any())
                {
                    if (query == null)
                        query = new QueryOptions();
                    query.Filters = metadataConditions.ToArray();
                }

                metadatas.AddRange(await ResourceMetadataCoreService.SolrReadWithIds(uri, ids, useridentities, perm, type, startCd, endCd,
                        query).ToAsyncList().ConfigureAwait(false));
            }

            var pagedData = SortingAndPaging(metadatas, query);
            if (pagedData == null) return null;

            var splitResources = pagedData.Data.Select(x => new ResourceInternal { ResourceMetadata = AutoMapper.Mapper.Map<ResourceMetadataInternal>(x) })
                .ToList().ChunkBy(100);

            var tasks = new Task<IList<ResourceInternal>>[splitResources.Count];
            for (var i = 0; i < tasks.Length; i++)
            {
                tasks[i] = FetchAsync(splitResources[i], fetch).AsAsyncList();
            }
            await tasks.Await().ConfigureAwait(false);

            var result = new List<ResourceInternal>();
            foreach (var task in tasks)
            {
                result.AddRange(await task.ConfigureAwait(false));
            }

            var pagedResult = new PagedResult<ResourceInternal>(result, pagedData.Paging);
            return pagedResult;
        }

        public Task<ResourceMetadataInternal> GetMetadata(Guid id)
        {
            return ResourceMetadataCoreService.Get(id);
        }

        public Task<bool> TryGetMetadata(Guid id, Ref<ResourceMetadataInternal> @ref)
        {
            return ResourceMetadataCoreService.TryGet(id, @ref);
        }

        public Task<ResourceMetadataInternal> GetMetadata(string uri)
        {
            return ResourceMetadataCoreService.Get(uri);
        }

        public Task<bool> TryGetMetadata(string uri, Ref<ResourceMetadataInternal> @ref)
        {
            return ResourceMetadataCoreService.TryGet(uri, @ref);
        }

        public async Task<Guid> GetId(string uri)
        {
            var resEntry = await ResourceSetCompositeService.Read(uri, 0).AsAsyncList().ConfigureAwait(false);
            if (!resEntry.Any())
            {
                throw new ResourceNotFoundException(uri);
            }
            return resEntry.Single().InId;
        }

        public Task PutLastUpdateTimestamp(Guid id)
        {
            return ResourceMetadataCoreService.UpdateChangeTimestamp(id);
        }

        public Task PutLastUpdateTimestamp(Guid id, DateTimeOffset timestamp)
        {
            return ResourceMetadataCoreService.UpdateChangeTimestamp(id, timestamp);
        }

        public Task PutLastUpdateTimestampAndUserInfo(Guid id, DateTimeOffset timestamp, Guid userId, string userName)
        {
            return ResourceMetadataCoreService.UpdateChangeTimestampAndUserInfo(id, timestamp, userId, userName);
        }

        #region properties

        public Task AddProperties(Guid resourceId, IDictionary<string, HashSet<PropertyInternal>> properties)
        {
            return PropertyCoreService.Add(resourceId, properties);
        }

        public async Task<List<KeyValuePair<string, HashSet<PropertyInternal>>>> GetProperties(Guid id)
        {
            var readPropertiesAsync = PropertyCoreService.Read(id).ToAsyncList();
            var properties = await readPropertiesAsync.ConfigureAwait(false);
            return properties;
        }

        public IObservable<PropertyInternal> GetProperties(Guid id, string groupName)
        {
            return PropertyCoreService.Read(id, groupName);
        }

        public Task<PropertyInternal> GetProperty(Guid id, string groupName, string key)
        {
            return PropertyCoreService.Read(id, groupName, key);
        }

        public async Task<Guid> GetResourceIdFromProperty(Condition filter)
        {
            var resourceId = await PropertyCoreService.GetIdFromProperty(filter).ConfigureAwait(false);
            return resourceId;
        }

        public async Task<Guid> GetResourceIdFromMetadata(Condition filter, List<Guid> useridentities, Permissions perm = Permissions.Read)
        {
            var resourcemetadata = await ResourceMetadataCoreService.ReadMetadataByCondition(new[] { filter }, useridentities, perm).ConfigureAwait(false);
            return resourcemetadata != null ? resourcemetadata.InId : Guid.Empty;
        }

        public async Task UpdateProperties(ResourceInternal resource)
        {
            await UpdateResourceValidator.ValidateAndThrowAsync(resource, "properties").ConfigureAwait(false);
            await PropertyCoreService.Add(resource.ResourceMetadata.Id, resource.Properties).ConfigureAwait(false);
        }

        public Task DeleteProperties(Guid id)
        {
            return PropertyCoreService.Delete(id);
        }

        public Task DeleteProperties(Guid id, string groupName)
        {
            return PropertyCoreService.Delete(id, groupName);
        }


        public Task DeleteProperty(Guid id, string groupName, string key)
        {
            return PropertyCoreService.Delete(id, groupName, key);
        }

        #endregion

        #region Audits

        public Task AddAudits(ResourceAudit auditEntry)
        {
            return ResourceAuditCoreService.Add(auditEntry);
        }

        public Task<ResourceAudit> GetAudit(Guid resourceId, Guid auditId)
        {
            return ResourceAuditCoreService.Get(resourceId, auditId);
        }

        public Task<PagedResult<ResourceAudit>> ReadAuditsPaged(string uri, List<Guid> useridentities, Permissions perm = Permissions.Read, short startCd = 1, short endCd = short.MaxValue, QueryOptions query = null)
        {
            return ResourceAuditCoreService.SolrReadPaged(uri, useridentities, perm, startCd, endCd, query);
        }

        #endregion

        #region ResourceExchange

        public Task AddResourceEvent(ResourceEventInternal resourceEvent)
        {
            return ResourceEventCoreService.Add(resourceEvent);
        }

        public Task<ResourceEventInternal> GetResourceEvent(Guid auditId, DateTimeOffset bucket)
        {
            return ResourceEventCoreService.Get(auditId, bucket);
        }

        public IObservable<ResourceEventInternal> GetResourceEvents(DateTimeOffset fromDt, DateTimeOffset toDt)
        {
            return ResourceEventCoreService.Get(fromDt, toDt);
        }

        #endregion

        #region Replication Events

        public async Task AddReplicationEvent(ReplicationEventInternal replicationEvent)
        {
            await AddReplicationEventValidator.ValidateAndThrowAsync(replicationEvent).ConfigureAwait(false);
            await ReplicationEventCoreService.Add(replicationEvent).ConfigureAwait(false);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, sbyte status)
        {
            return ReplicationEventCoreService.Read(ruleId, status);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, List<sbyte> status)
        {
            return ReplicationEventCoreService.Read(ruleId, status);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, sbyte status, int limit)
        {
            return ReplicationEventCoreService.Read(ruleId, status, limit);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, sbyte status, DateTimeOffset since, int limit)
        {
            return ReplicationEventCoreService.Read(ruleId, status, since, limit);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, List<sbyte> status, DateTimeOffset lastTimestamp, int limit)
        {
            return ReplicationEventCoreService.Read(ruleId, status, lastTimestamp, limit);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, List<sbyte> status, params string[] resourceType)
        {
            return ReplicationEventCoreService.Read(ruleId, status, resourceType);
        }

        public IObservable<ReplicationEventInternal> ReadReplicationEvents(Guid ruleId, List<sbyte> status, string resourceName, params string[] resourceType)
        {
            return ReplicationEventCoreService.Read(ruleId, status, resourceName, resourceType);
        }

        #endregion
    }

    public static class ListExtensions
    {
        public static List<List<T>> ChunkBy<T>(this IList<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
    }

}
