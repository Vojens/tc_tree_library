﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.Internal;

namespace Infrastructure.Graph.Service.Composite.Helper
{
    internal static class ResourceSetCompositeServiceHelper
    {
        public static KeyValuePair<ResourceSetEntry, HashSet<Edge>> GetResourceSetAndEdges(ResourceInternal resource)
        {
            var edges = new HashSet<Edge>();
            foreach (var resourceLink in resource.Parents)
            {
                var edge = new Edge
                {
                    Out = new Identifier
                    {
                        Id = resourceLink.Id,
                        Uri = resourceLink.Uri
                    },
                    In = new Identifier
                    {
                        Id = resource.ResourceMetadata.Id,
                        Uri = resource.ResourceMetadata.Uri
                    }
                };
                edges.Add(edge);
            }
            var tasks = new List<Task>();

            var resourceSetEntry = new ResourceSetEntry
            {
                OutId = resource.ResourceMetadata.Id,
                InId = resource.ResourceMetadata.Id,
                OutUri = resource.ResourceMetadata.Uri,
                InUri = resource.ResourceMetadata.Uri,
                CurveDistance = 0,
                CurveDistanceBucket = 0,
                Name = resource.ResourceMetadata.Name,
                ResourceSystem = resource.ResourceMetadata.ResourceSystem,
                StorageType = (short)resource.ResourceMetadata.StorageType,
                Type = resource.ResourceMetadata.Type
            };
            return new KeyValuePair<ResourceSetEntry, HashSet<Edge>>(resourceSetEntry, edges);
        }
    }
}
