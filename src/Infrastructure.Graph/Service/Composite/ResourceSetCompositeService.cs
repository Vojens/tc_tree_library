﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Infrastructure.ConnectionProvider;
using Infrastructure.Common.Exceptions;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.Common.Model;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model.Filter;
using Infrastructure.Graph.Model.Internal;
using Infrastructure.Graph.Repository;
using Infrastructure.Graph.Repository.Resource.Cassandra;
using Infrastructure.Graph.Service.Core;
using Infrastructure.Graph.Service.Core.Helper;
using Infrastructure.Graph.Service.Validation;

namespace Infrastructure.Graph.Service.Composite
{
    internal class ResourceSetCompositeService
    {
        protected IConnectionProvider ConnectionProvider;
        protected SchemaContainer SchemaContainer;
        protected ResourceSetCoreService ResourceSetCoreService;
        private CurveDistanceBucketHelperService _curveDistanceBucketHelperService;
        private readonly AddEdgeValidator _addEdgeValidator;
        private readonly RemoveEdgeValidator _removeEdgeValidator;
        internal AscendantsByResourceCassandraRepository AscendantsByResourceCassandraRepository;
        public ResourceSetCompositeService(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
        {
            ConnectionProvider = connectionProvider;
            SchemaContainer = schemaContainer;
            ResourceSetCoreService = new ResourceSetCoreService(ConnectionProvider, SchemaContainer);
            _addEdgeValidator = new AddEdgeValidator(this);
            _removeEdgeValidator = new RemoveEdgeValidator(this);
        }


        internal Task Add(HashSet<ResourceSetEntry> vertices, HashSet<Edge> edges,
            Dictionary<Identifier, List<ResourcesByCurveDistanceEntry>> resourceToParentsDic)
        {
            //for each out, fetch resource_sets and all of their parents
            var edgeOutGroups = edges.GroupBy(x => x.Out).ToList();
            //construct TcEdge
            var tasks = new ConcurrentBag<Task>();
            foreach (var edgeOutGroup in edgeOutGroups)
            {
                var outIdentifier = edgeOutGroup.Key;
                var tcParentResources = resourceToParentsDic[outIdentifier];
                Parallel.ForEach(edgeOutGroup, edge =>
                //foreach (var edge in edgeOutGroup)
                {
                    var inIdentifier = edge.In;
                    foreach (var parent in tcParentResources.ToList())
                    {
                        var cd = (short)(Math.Abs(parent.CurveDistance) + 1); //Math.Abs haha got ya
                        var inEntry =
                            vertices.FirstOrDefault(x => x.InId == inIdentifier.Id || x.InUri == inIdentifier.Uri);
                        if (inEntry.IsNullOrDefault())
                        {
                            throw new ArgumentException(string.Format("there exists no vertex for id {0} and uri {1}",
                                inIdentifier.Id, inIdentifier.Uri));
                        }
                        var parentSetEntry = Mapper.Map<ResourceSetEntry>(parent);
                        //make sure it's the actual resource now, because internally 'out' is used rather than 'in'
                        parentSetEntry.OutId = parentSetEntry.InId;
                        parentSetEntry.OutUri = parentSetEntry.InUri;
                        tasks.Add(ResourceSetCoreService.AddBidirectionalEdge(parentSetEntry, inEntry, cd));
                    }


                });
            }

            //Add vertices
            foreach (var vertex in vertices)
            {
                tasks.Add(ResourceSetCoreService.AddVertex(vertex));
            }
            return tasks.Await();
        }

        public async Task Add(HashSet<ResourceSetEntry> vertices, HashSet<Edge> edges)
        {
            var resourceToParentsDic = await GetResourceToParentDic(vertices, edges).ConfigureAwait(false);
            await Add(vertices, edges, resourceToParentsDic).ConfigureAwait(false);
        }

        internal async Task<Dictionary<Identifier, List<ResourcesByCurveDistanceEntry>>> GetResourceToParentDic(
            HashSet<ResourceSetEntry> vertices, HashSet<Edge> edges)
        {
            //for each out, fetch resource_sets and all of their parents
            var resourceToParentsDic = new Dictionary<Identifier, List<ResourcesByCurveDistanceEntry>>();
            var edgeOutGroups = edges.GroupBy(x => x.Out).ToList();
            var keys = new List<Task<List<ResourcesByCurveDistanceEntry>>>();
            var values = new List<Identifier>();
            foreach (var edgeOutGroup in edgeOutGroups)
            {
                var outIdentifier = edgeOutGroup.Key;
                var uri = outIdentifier.Uri;
                keys.Add(ResourceSetCoreService.ReadUpToRoot(uri).ToAsyncList());
                values.Add(outIdentifier);
            }

            if (keys.Count != values.Count)
            {
                throw new ConcurrentModificationException(string.Format("key count: {0} != value count: {1}", keys.Count,
                    values.Count));
            }

            foreach (var awaitableTask in keys.Interleaved())
            {
                var task = await awaitableTask.ConfigureAwait(false);
                var taskIndex = keys.IndexOf(task);
                if (taskIndex < 0)
                {
                    throw new KeyNotFoundException("Task not found in the list");
                }
                var identifier = values[taskIndex];
                var parentResources = await task.ConfigureAwait(false);
                resourceToParentsDic.Add(identifier, parentResources);

                keys[taskIndex] = null;
                values[taskIndex] = null;
            }

            var nonEmptyValues = values.Where(x => !x.IsNullOrDefault()).ToList();
            if (nonEmptyValues.Count > 0)
            {
                throw new ConcurrentModificationException(
                    string.Format("Not all values are empty. There are {0} values left unprocessed- those are: {1}",
                        nonEmptyValues.Count, string.Join(",", nonEmptyValues)));
            }

            var nonEmptyKeyCount = keys.Select(x => !x.IsNullOrDefault()).Count(x => x);
            if (nonEmptyKeyCount > 0)
            {
                throw new ConcurrentModificationException(
                    string.Format("Not all keys are nulls. There are still {0} keys left", nonEmptyKeyCount));
            }
            return resourceToParentsDic;
        }

        public Task<ResourceSetEntry> Get(Guid id)
        {
            return ResourceSetCoreService.Get(id);
        }

        public Task<ResourceSetEntry> Get(string uri)
        {
            return ResourceSetCoreService.Get(uri);
        }

        public Task<Guid> GetId(string uri)
        {
            return ResourceSetCoreService.GetId(uri);
        }

        public Task<bool> TryGetId(string uri, Ref<Guid> refId)
        {
            return ResourceSetCoreService.TryGetId(uri, refId);
        }

        public IObservable<ResourceSetEntry> ReadDirectParents(string uri)
        {
            return Read(uri, -1);
        }

        public IObservable<ResourceSetEntry> Read(string uri, short distance)
        {
            return ResourceSetCoreService.Read(uri, distance).Select(x => Mapper.Map<ResourceSetEntry>(x));
        }

        public Task<bool> Exists(string uri)
        {
            return ResourceSetCoreService.Exists(uri);
        }

        public async Task<bool> NotExists(string uri)
        {
            return !await ResourceSetCoreService.Exists(uri).ConfigureAwait(false);
        }

        public Task<bool> Exists(Guid id)
        {
            return ResourceSetCoreService.Exists(id);
        }

        public async Task<bool> NotExists(Guid id)
        {
            return !await ResourceSetCoreService.Exists(id).ConfigureAwait(false);
        }

        public Task<bool> Exists(string uri, Guid id)
        {
            return ResourceSetCoreService.Exists(uri, id);
        }

        public async Task<bool> NotExists(string uri, Guid id)
        {
            var b = await Exists(uri, id).ConfigureAwait(false);
            return !b;
        }

        public Task<bool> EdgeExists(Guid outId, Guid inId, string outUri, string inUri)
        {
            var @out = new Identifier()
            {
                Id = outId,
                Uri = outUri
            };
            var @in = new Identifier()
            {
                Id = inId,
                Uri = inUri
            };
            return EdgeExists(@out, @in);
        }

        public Task<bool> EdgeExists(Identifier @out, Identifier @in)
        {
            return ResourceSetCoreService.EdgeExists(@out, @in);
        }

        public async Task<bool> EdgeNotExists(Guid outId, Guid inId, string outUri, string inUri)
        {
            var b = await EdgeExists(outId, inId, outUri, inUri).ConfigureAwait(false);
            return !b;
        }

        public async Task<bool> EdgeNotExists(Identifier @out, Identifier @in)
        {
            var b = await EdgeExists(@out, @in).ConfigureAwait(false);
            return !b;
        }

        public async Task<List<ResourcesByCurveDistanceEntry>> CascadeDelete(string uri)
        {
            short distance = 0;
            bool isDone = false;
            var fetchTasks =
                new List<Task<KeyValuePair<ResourcesByCurveDistanceEntry, List<ResourcesByCurveDistanceEntry>>>>();
            var deletedResources = new List<ResourcesByCurveDistanceEntry>();
            var upToRoot = ResourceSetCoreService.ReadUpToRoot(uri).ToAsyncList();
            do
            {
                List<ResourcesByCurveDistanceEntry> resourcesByCurveDistanceEntries =
                    await ResourceSetCoreService.Read(uri, distance++).ToAsyncList().ConfigureAwait(false);
                if (!resourcesByCurveDistanceEntries.Any())
                {
                    isDone = true;
                }
                else
                {
                    deletedResources.AddRange(resourcesByCurveDistanceEntries);
                    fetchTasks.AddRange(resourcesByCurveDistanceEntries.Select(FetchUntilRoot));
                }
            } while (!isDone);

            var upList = await upToRoot.ConfigureAwait(false);

            var tasks = new List<Task>();
            foreach (var task in fetchTasks.Interleaved())
            {
                var keyValuePair = await (await task.ConfigureAwait(false)).ConfigureAwait(false);
                var hasMultipleParents = keyValuePair.Value.GroupBy(x => x.CurveDistance).Any(x => x.ToList().Count > 1);
                var parents = keyValuePair.Value;
                if (hasMultipleParents)
                {
                    parents = new List<ResourcesByCurveDistanceEntry>();
                    foreach (var resourcesByCurveDistanceEntry in keyValuePair.Value)
                    {
                        var inUri = resourcesByCurveDistanceEntry.InUri;
                        if (upList.Any(x => x.InUri == inUri) || inUri == keyValuePair.Key.InUri)
                        {
                            parents.Add(resourcesByCurveDistanceEntry);
                        }
                    }
                }
                tasks.Add(DeleteInternal(keyValuePair.Key.InUri, parents, !hasMultipleParents));
            }
            await tasks.Await().ConfigureAwait(false);
            return deletedResources;
        }

        public async Task CascadeDelete(string uri, List<ResourcesByCurveDistanceEntry> deletedResources, List<Task<KeyValuePair<ResourcesByCurveDistanceEntry, List<ResourcesByCurveDistanceEntry>>>> fetchTasks)
        {
            var upToRoot = ResourceSetCoreService.ReadUpToRoot(uri).ToAsyncList();
            var upList = await upToRoot.ConfigureAwait(false);

            var tasks = new List<Task>();
            foreach (var task in fetchTasks.Interleaved())
            {
                var keyValuePair = await (await task.ConfigureAwait(false)).ConfigureAwait(false);
                var hasMultipleParents = keyValuePair.Value.GroupBy(x => x.CurveDistance).Any(x => x.ToList().Count > 1);
                var parents = keyValuePair.Value;
                if (hasMultipleParents)
                {
                    parents = new List<ResourcesByCurveDistanceEntry>();
                    foreach (var resourcesByCurveDistanceEntry in keyValuePair.Value)
                    {
                        var inUri = resourcesByCurveDistanceEntry.InUri;
                        if (upList.Any(x => x.InUri == inUri) || inUri == keyValuePair.Key.InUri)
                        {
                            parents.Add(resourcesByCurveDistanceEntry);
                        }
                    }
                }
                tasks.Add(DeleteInternal(keyValuePair.Key.InUri, parents, !hasMultipleParents));
            }
            await tasks.Await().ConfigureAwait(false);
        }

        public async Task<List<ResourcesByCurveDistanceEntry>> ReadCurveDistanceEntries(string uri, short distance)
        {
            var result = await ResourceSetCoreService.Read(uri, distance).ToAsyncList().ConfigureAwait(false);
            return result;
        }

        public async Task<KeyValuePair<ResourcesByCurveDistanceEntry, List<ResourcesByCurveDistanceEntry>>> FetchUntilRoot(ResourcesByCurveDistanceEntry entry)
        {
            var resourcesByCurveDistanceEntries = await ResourceSetCoreService.ReadUpToRoot(entry.InUri).ToAsyncList().ConfigureAwait(false);
            return new KeyValuePair<ResourcesByCurveDistanceEntry, List<ResourcesByCurveDistanceEntry>>(entry,
                resourcesByCurveDistanceEntries);
        }

        public async Task<ResourceSetEntry> Delete(string uri)
        {
            List<ResourcesByCurveDistanceEntry> resourcesByCurveDistanceEntries =
                await ResourceSetCoreService.ReadUpToRoot(uri).ToAsyncList().ConfigureAwait(false);
            await DeleteInternal(uri, resourcesByCurveDistanceEntries).ConfigureAwait(false);
            return
                Mapper.Map<ResourceSetEntry>(resourcesByCurveDistanceEntries.FirstOrDefault(x => x.CurveDistance == 0));
        }

        internal Task DeleteInternal(string uri, List<ResourcesByCurveDistanceEntry> listOfParents, bool deleteResource = true)
        {
            var bottomResourceByCurveEntry =
                listOfParents.FirstOrDefault(x => x.CurveDistance == 0 || x.InUri == uri);

            if (bottomResourceByCurveEntry.IsNullOrDefault())
            {
                throw new InvalidOperationException(string.Format("resource with uri {0} does not exist to be deleted", uri));
            }
            var goingDownList = new List<ResourceSetEntry>();
            var goingUpList = new List<ResourceSetEntry>();
            foreach (
                var byCurveDistanceEntry in listOfParents.Where(c => c != bottomResourceByCurveEntry))
               //var byCurveDistanceEntry in listOfParents)
            {
                var downEntry = Mapper.Map<ResourceSetEntry>(bottomResourceByCurveEntry);
                downEntry.OutUri = byCurveDistanceEntry.InUri;
                downEntry.OutId = byCurveDistanceEntry.InId;
                downEntry.CurveDistance = Math.Abs(byCurveDistanceEntry.CurveDistance);
                goingDownList.Add(downEntry);

                var upEntry = Mapper.Map<ResourceSetEntry>(byCurveDistanceEntry);
                upEntry.OutUri = bottomResourceByCurveEntry.OutUri;
                upEntry.OutId = bottomResourceByCurveEntry.OutId;

                goingUpList.Add(upEntry);
            }

            //zip the two lists
            var tuples = from firstEntry in goingDownList let resourceSetEntry = goingUpList
                .FirstOrDefault(x => Math.Abs(x.CurveDistance) == Math.Abs(firstEntry.CurveDistance) && x.InId == firstEntry.OutId) 
                         where !resourceSetEntry.IsNullOrDefault() 
                         select new Tuple<ResourceSetEntry, ResourceSetEntry>(firstEntry, resourceSetEntry);
            var tasks = tuples.Select(x => DeletePairInternal(x.Item1, x.Item2)).ToList();
            if (deleteResource)
            {
                var bottomResourceSetEntry = Mapper.Map<ResourceSetEntry>(bottomResourceByCurveEntry);
                tasks.Add(DeleteInternal(bottomResourceSetEntry, goingUpList));
            }
            return tasks.Await();
        }

        private async Task DeleteInternal(ResourceSetEntry entry, IEnumerable<ResourceSetEntry> ascendants)
        {
            var isDeleted = await ResourceSetCoreService.Delete(entry).ConfigureAwait(false);
            if (isDeleted)
            {
                await
                    ResourceSetCoreService.AscendantsByResourceCassandraRepository.Delete(
                        entry.InId, entry.InId).ConfigureAwait(false);
            }
            else
            {
                await
                    ResourceSetCoreService.AscendantsByResourceCassandraRepository.Delete(
                        entry.InId, entry.InId, ascendants.Where(x => x.CurveDistance < 0).Select(x => Tuple.Create<short, Guid>(x.CurveDistance, x.OutId))).ConfigureAwait(false);
            }
        }


        public async Task DeletePairInternal(ResourceSetEntry down, ResourceSetEntry up)
        {
            var isDeleted = await ResourceSetCoreService.DeletePair(down, up).ConfigureAwait(false);
            if (isDeleted && up.CurveDistance < 0)
            {
                await ResourceSetCoreService.AscendantsByResourceCassandraRepository
                    .Delete(up.OutId, up.OutId, up.CurveDistance, up.InId).ConfigureAwait(false);
            }
        }

        public Task<bool> HasChildren(string uri)
        {
            return ResourceSetCoreService.HasChildren(uri);
        }

        public async Task<bool> HasMultipleParents(string uri)
        {
            var resourcesByCurveDistanceEntries = await ResourceSetCoreService.Read(uri, -1).Take(2).ToAsyncList().ConfigureAwait(false);
            return resourcesByCurveDistanceEntries.Count > 1;
        }

        internal IObservable<ResourcesByCurveDistanceEntry> ReadDescendantsInternal(string uri)
        {

            return Observable.Create<ResourcesByCurveDistanceEntry>(async o =>
            {
                try
                {
                    short distance = 0;
                    bool isDone = false;
                    do
                    {
                        List<ResourcesByCurveDistanceEntry> resourcesByCurveDistanceEntries =
                            await ResourceSetCoreService.Read(uri, distance++).ToAsyncList().ConfigureAwait(false);
                        if (!resourcesByCurveDistanceEntries.Any())
                        {
                            isDone = true;
                        }
                        else
                        {
                            foreach (var resourcesByCurveDistanceEntry in resourcesByCurveDistanceEntries)
                            {
                                o.OnNext(resourcesByCurveDistanceEntry);
                            }
                        }
                    } while (!isDone);
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public async Task AddEdge(Edge edge)
        {
            await _addEdgeValidator.ValidateAndThrowAsync(edge).ConfigureAwait(false);
            var upListTask = ResourceSetCoreService.ReadUpToRoot(edge.Out.Uri).ToAsyncList();
            var downListTask = ReadDescendantsInternal(edge.In.Uri).ToAsyncList();
            await Task.WhenAll(upListTask, downListTask).ConfigureAwait(false);

            var upList = await upListTask.ConfigureAwait(false);
            var downList = await downListTask.ConfigureAwait(false);
            if (upList.Where(x => x.CurveDistance < 0).Intersect(downList.Where(x => x.CurveDistance > 0)).Any())
            {
                throw new InvalidOperationException("adding this link causes a cyclic graph");
            }
            var tasks = new List<Task>();
            foreach (var upEntry in upList)
            {
                var upEntrySet = Mapper.Map<ResourceSetEntry>(upEntry);
                upEntrySet.OutId = upEntrySet.InId;
                upEntrySet.OutUri = upEntrySet.InUri;
                foreach (var downEntry in downList)
                {
                    var inEntry = Mapper.Map<ResourceSetEntry>(downEntry);
                    inEntry.OutUri = inEntry.InUri;
                    inEntry.OutId = inEntry.InId;
                    inEntry.CurveDistance = (short)(Math.Abs(upEntry.CurveDistance) + 1 + downEntry.CurveDistance);
                    tasks.Add(ResourceSetCoreService.AddBidirectionalEdge(upEntrySet, inEntry, inEntry.CurveDistance));
                }
            }
            await tasks.Await().ConfigureAwait(false);

        }

        public async Task RemoveEdge(Edge edge)
        {
            await _removeEdgeValidator.ValidateAndThrowAsync(edge).ConfigureAwait(false);
            var outUpListTask = ResourceSetCoreService.ReadUpToRoot(edge.Out.Uri).ToAsyncList();
            var inUpListTask = ResourceSetCoreService.ReadUpToRoot(edge.In.Uri).ToAsyncList();
            var downListTask = ReadDescendantsInternal(edge.In.Uri).ToAsyncList();
            await Task.WhenAll(outUpListTask, inUpListTask, downListTask).ConfigureAwait(false);

            var outUpList = await outUpListTask.ConfigureAwait(false);
            var inUpList = await inUpListTask.ConfigureAwait(false);
            var downList = await downListTask.ConfigureAwait(false);

            if (!downList.Any())
            {
                //shouldn't get here since it's already validated above
                //maybe log
            }
            if (downList.Count > 1)
            {
                //shouldn't get here since it's already validated above
                //maybe log
            }

            if (!inUpList.Any(x => x.InUri == edge.Out.Uri && x.InId == edge.Out.Id))
            {
                throw new InvalidOperationException(string.Format("Edge {0} is not connected", edge));
            }

            if (!inUpList.Any(x => x.InUri == edge.Out.Uri && x.InId == edge.Out.Id && x.CurveDistance == -1))
            {
                throw new InvalidOperationException(string.Format("Edge {0} is connected, but not direct parent", edge));
            }

            var filteredList =
                inUpList.Where(x => outUpList.Any(y => y.InUri == x.InUri && y.InId == x.InId) || x.CurveDistance == 0).ToList();
            await DeleteInternal(edge.In.Uri, filteredList, false).ConfigureAwait(false);
        }


        //public IObservable<ResourceSetEntry> SolrRead(string outUri)
        //{
        //    return ResourceSetCoreService.SolrRead(outUri);
        //}

        //public IObservable<ResourceSetEntry> SolrRead(string outUri, string type)
        //{
        //    return ResourceSetCoreService.SolrRead(outUri, type);
        //}

        //public IObservable<ResourceSetEntry> SolrRead(string outUri, short cd)
        //{
        //    return ResourceSetCoreService.SolrRead(outUri, cd);
        //}

        //public IObservable<ResourceSetEntry> SolrRead(string outUri, string type, short cd)
        //{
        //    return ResourceSetCoreService.SolrRead(outUri, type, cd);
        //}

        //public IObservable<ResourceSetEntry> SolrReadUpTo(string outUri, string type, short cd)
        //{
        //    return ResourceSetCoreService.SolrReadUpTo(outUri, type, cd);
        //}

        //public IObservable<ResourceSetEntry> SolrReadFrom(string outUri, string type, short cd)
        //{
        //    return ResourceSetCoreService.SolrReadFrom(outUri, type, cd);
        //}

        //public IObservable<ResourceSetEntry> SolrReadWindow(string outUri, string type, short startCd, short endCd)
        //{
        //    return ResourceSetCoreService.SolrReadWindow(outUri, type, startCd, endCd);
        //}

        //public IObservable<ResourceSetEntry> SolrReadUpTo(string outUri, short cd)
        //{
        //    return ResourceSetCoreService.SolrReadUpTo(outUri, cd);
        //}

        //public IObservable<ResourceSetEntry> SolrReadFrom(string outUri, short cd)
        //{
        //    return ResourceSetCoreService.SolrReadFrom(outUri, cd);
        //}

        //public IObservable<ResourceSetEntry> SolrReadWindow(string outUri, short startCd, short endCd)
        //{
        //    return ResourceSetCoreService.SolrReadWindow(outUri, startCd, endCd);
        //}

        public IObservable<ResourceSetEntry> SolrReadWithoutPermission(string outUri, string type)
        {
            return ResourceSetCoreService.SolrReadWithoutPermission(outUri, type);
        }

        public IObservable<ResourceSetEntry> SolrRead(string outUri, List<Guid> useridentities, string type)
        {
            return ResourceSetCoreService.SolrRead(outUri, useridentities, type);
        }

        public IObservable<ResourceSetEntry> SolrRead(Guid id, List<Guid> useridentities, string type)
        {
            return ResourceSetCoreService.SolrRead(id, useridentities, type);
        }

        public IObservable<AscendantsByResourceEntry> SolrReadWindowAll(string outUri, short startCd, short endCd, List<Guid> useridentities, string resourceType = null)
        {
            return ResourceSetCoreService.SolrReadWindowAll(outUri, startCd, endCd, useridentities, resourceType);
        }

        public IObservable<AscendantsByResourceEntry> SolrReadWindowAll(Guid outId, short startCd, short endCd, List<Guid> useridentities, string resourceType = null)
        {
            return ResourceSetCoreService.SolrReadWindowAll(outId, startCd, endCd, useridentities, resourceType);
        }

        public IObservable<AscendantsByResourceEntry> SolrReadIn(Guid resId, short startCd, short endCd, List<Guid> useridentities, params Condition[] conditions)
        {
            return ResourceSetCoreService.SolrReadIn(resId, startCd, endCd, useridentities, conditions);
        }


        public IObservable<AscendantsByResourceEntry> SolrReadWindowAllWithoutPermission(string outUri, short startCd, short endCd, string resourceType = null)
        {
            return ResourceSetCoreService.SolrReadWindowAllWithoutPermission(outUri, startCd, endCd, resourceType);
        }
    }
}
