﻿using System;
using System.Threading.Tasks;
using System.Transactions;

namespace Infrastructure.Graph.Service.Transaction
{
    public static class TranscationExtensions
    {
        public static Task EnlistVolatileAsync(
            this System.Transactions.Transaction transaction,
            IEnlistmentNotification enlistmentNotification,
            EnlistmentOptions enlistmentOptions)
        {
            return Task.FromResult(transaction.EnlistVolatile(enlistmentNotification, enlistmentOptions));
        }
    }

    internal class AsyncManager : IEnlistmentNotification
    {
        private readonly Func<Task> _onCommit, _onRollBack, _inDoubt;
        private readonly TaskCompletionSource<object> _source;

        public AsyncManager(Func<Task> onCommit, Func<Task> onRollback, Func<Task> inDoubt)
        {
            _onCommit = onCommit;
            _onRollBack = onRollback;
            _inDoubt = inDoubt;
        }

        public void Prepare(PreparingEnlistment preparingEnlistment)
        {
            preparingEnlistment.Prepared();
        }

        public async void Commit(Enlistment enlistment)
        {
            await _onCommit().ConfigureAwait(false);
            enlistment.Done();
        }

        public async void Rollback(Enlistment enlistment)
        {
            await _onRollBack().ConfigureAwait(false);
            enlistment.Done();
        }

        public async void InDoubt(Enlistment enlistment)
        {
            await _inDoubt().ConfigureAwait(false);
            enlistment.Done();
        }
    }
}
