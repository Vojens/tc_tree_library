﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model;
using Infrastructure.Graph.Model.Internal;

namespace Infrastructure.Graph.Service.Transaction
{
    internal class AddResourceAsyncTransactional
    {
        private readonly ResourceService _service;
        public AddResourceAsyncTransactional(ResourceService service)
        {
            _service = service;
        }
        public Task AddAsync(ResourceInternal resource)
        {
            if (System.Transactions.Transaction.Current != null)
            {
                return System.Transactions.Transaction.Current.EnlistVolatileAsync(new AsyncManager(() => ExecuteAsyncInternal(resource), null, null), EnlistmentOptions.None);
            }
            return ExecuteAsyncInternal(resource);
        }

        private Task ExecuteAsyncInternal(ResourceInternal resource)
        {
            //TODO Move this logic down to ResourceSetCompositeService, and use mappers
            var edges = new HashSet<Edge>();
            foreach (var resourceLink in resource.Parents)
            {
                var edge = new Edge
                {
                    Out = new Identifier
                    {
                        Id = resourceLink.Id,
                        Uri = resourceLink.Uri
                    },
                    In = new Identifier
                    {
                        Id = resource.ResourceMetadata.Id,
                        Uri = resource.ResourceMetadata.Uri
                    }
                };
                edges.Add(edge);
            }
            var tasks = new List<Task>();

            var resourceSetEntry = new ResourceSetEntry
            {
                OutId = resource.ResourceMetadata.Id,
                InId = resource.ResourceMetadata.Id,
                OutUri = resource.ResourceMetadata.Uri,
                InUri = resource.ResourceMetadata.Uri,
                CurveDistance = 0,
                CurveDistanceBucket = 0,
                Name = resource.ResourceMetadata.Name,
                ResourceSystem = resource.ResourceMetadata.ResourceSystem,
                StorageType = (short)resource.ResourceMetadata.StorageType,
                Type = resource.ResourceMetadata.Type
            };
            tasks.Add(_service.ResourceSetCompositeService.Add(new HashSet<ResourceSetEntry> { resourceSetEntry }, edges));
            tasks.Add(_service.ResourceMetadataCoreService.Add(resource.ResourceMetadata));
            tasks.Add(_service.ResourceDataSimpleCoreService.Add(resource.ResourceMetadata.Id, resource.Data, resource.ResourceMetadata.ResourceSystem, resource.ResourceMetadata.Type));
            return tasks.Await();
        }
    }
}
