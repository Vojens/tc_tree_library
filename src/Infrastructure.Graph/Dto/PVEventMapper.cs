using AutoMapper;
using Event;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model;

namespace Infrastructure.Graph.Dto
{
    internal static class PVEventMapper
    {
        public static void Configure()
        {
            Mapper.CreateMap<PVEvent, PVEventInternal>();

            Mapper.CreateMap<Event, PVEvent>();

        }
    }
}