using AutoMapper;
using Infrastructure.Graph.Data.Entity;

namespace Infrastructure.Graph.Dto
{
    internal static class ResourceSetMapper
    {
        public static void Configure()
        {
            Mapper.CreateMap<ResourceSetEntry, ResourceSetEntry>();
            Mapper.CreateMap<ResourceSetEntry, ResourceSetObjectEntry>();
            Mapper.CreateMap<ResourceSetObjectEntry, ResourceSetEntry>();
            Mapper.CreateMap<ResourcesByCurveDistanceEntry, ResourcesByCurveDistanceEntry>();
            Mapper.CreateMap<ResourcesByCurveDistanceEntry, ResourceSetEntry>();
            Mapper.CreateMap<ResourceSetEntry, ResourcesByCurveDistanceEntry>();
            Mapper.CreateMap<ResourceSetEntry, ResourceSetSolrEntry>();
            Mapper.CreateMap<ResourceSetSolrEntry, ResourceSetEntry>();
            Mapper.CreateMap<AscendantsByResourceSolrEntry, AscendantsByResourceEntry>();
        }
    }
}