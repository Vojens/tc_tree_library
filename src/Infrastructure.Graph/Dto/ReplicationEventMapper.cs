using AutoMapper;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model;

namespace Infrastructure.Graph.Dto
{
    internal static class ReplicationEventMapper
    {
        public static void Configure()
        {
            Mapper.CreateMap<ReplicationEventInternal, ReplicationEventEntry>()
                .ForMember(m => m.RuleId, opt => opt.MapFrom(src => src.RuleId))
                .ForMember(m => m.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(m => m.Timestamp, opt => opt.MapFrom(src => src.Timestamp))
                .ForMember(m => m.ResourceType, opt => opt.MapFrom(src => src.ResourceType))
                .ForMember(m => m.ResourceName, opt => opt.MapFrom(src => src.ResourceName))
                .ForMember(m => m.ResourceId, opt => opt.MapFrom(src => src.ResourceId))
                .ForMember(m => m.Data, opt => opt.MapFrom(src => src.Data));

            Mapper.CreateMap<ReplicationEventEntry, ReplicationEventInternal>()
                .ForMember(m => m.RuleId, opt => opt.MapFrom(src => src.RuleId))
                .ForMember(m => m.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(m => m.Timestamp, opt => opt.MapFrom(src => src.Timestamp))
                .ForMember(m => m.ResourceType, opt => opt.MapFrom(src => src.ResourceType))
                .ForMember(m => m.ResourceName, opt => opt.MapFrom(src => src.ResourceName))
                .ForMember(m => m.ResourceId, opt => opt.MapFrom(src => src.ResourceId))
                .ForMember(m => m.Data, opt => opt.MapFrom(src => src.Data));

            Mapper.CreateMap<ReplicationEventInternal, ReplicationEventViewEntry>()
                .ForMember(m => m.RuleId, opt => opt.MapFrom(src => src.RuleId))
                .ForMember(m => m.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(m => m.Timestamp, opt => opt.MapFrom(src => src.Timestamp))
                .ForMember(m => m.ResourceType, opt => opt.MapFrom(src => src.ResourceType))
                .ForMember(m => m.ResourceName, opt => opt.MapFrom(src => src.ResourceName))
                .ForMember(m => m.ResourceId, opt => opt.MapFrom(src => src.ResourceId))
                .ForMember(m => m.Data, opt => opt.MapFrom(src => src.Data));

            Mapper.CreateMap<ReplicationEventViewEntry, ReplicationEventInternal>()
                .ForMember(m => m.RuleId, opt => opt.MapFrom(src => src.RuleId))
                .ForMember(m => m.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(m => m.Timestamp, opt => opt.MapFrom(src => src.Timestamp))
                .ForMember(m => m.ResourceType, opt => opt.MapFrom(src => src.ResourceType))
                .ForMember(m => m.ResourceName, opt => opt.MapFrom(src => src.ResourceName))
                .ForMember(m => m.ResourceId, opt => opt.MapFrom(src => src.ResourceId))
                .ForMember(m => m.Data, opt => opt.MapFrom(src => src.Data));
        }
    }
}