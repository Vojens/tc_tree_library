using AutoMapper;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model;

namespace Infrastructure.Graph.Dto
{
    internal static class ResourceEventMapper
    {
        public static void Configure()
        {
            Mapper.CreateMap<ResourceEventInternal, ResourceEventEntry>()                
                .ForMember(m => m.Bucket, opt => opt.MapFrom(src => src.Bucket))
                .ForMember(m => m.CreationTime, opt => opt.MapFrom(src => src.CreationTime))
                .ForMember(m => m.ActionType, opt => opt.MapFrom(src => src.ActionType))
                .ForMember(m => m.ObjectType, opt => opt.MapFrom(src => src.ObjectType))                
                .ForMember(m => m.ResourceId, opt => opt.MapFrom(src => src.ResourceId))
                .ForMember(m => m.Version, opt => opt.MapFrom(src => src.Version))
                .ForMember(m=> m.OldName, opt=> opt.MapFrom(src => src.OldName));

            Mapper.CreateMap<ResourceEventEntry, ResourceEventInternal>()
                .ForMember(m => m.Bucket, opt => opt.MapFrom(src => src.Bucket))
                .ForMember(m => m.CreationTime, opt => opt.MapFrom(src => src.CreationTime))
                .ForMember(m => m.ActionType, opt => opt.MapFrom(src => src.ActionType))
                .ForMember(m => m.ObjectType, opt => opt.MapFrom(src => src.ObjectType))                
                .ForMember(m => m.ResourceId, opt => opt.MapFrom(src => src.ResourceId))
                .ForMember(m => m.Version, opt => opt.MapFrom(src => src.Version))
                .ForMember(m => m.OldName, opt => opt.MapFrom(src => src.OldName));

            Mapper.CreateMap<ResourceEventInternal, ResourceEventViewEntry>()
                .ForMember(m => m.Bucket, opt => opt.MapFrom(src => src.Bucket))
                .ForMember(m => m.CreationTime, opt => opt.MapFrom(src => src.CreationTime))
                .ForMember(m => m.ActionType, opt => opt.MapFrom(src => src.ActionType))
                .ForMember(m => m.ObjectType, opt => opt.MapFrom(src => src.ObjectType))   
                .ForMember(m => m.ResourceId, opt => opt.MapFrom(src => src.ResourceId))
                .ForMember(m => m.Version, opt => opt.MapFrom(src => src.Version))
                .ForMember(m => m.OldName, opt => opt.MapFrom(src => src.OldName));

            Mapper.CreateMap<ResourceEventViewEntry, ResourceEventInternal>()
                .ForMember(m => m.Bucket, opt => opt.MapFrom(src => src.Bucket))
                .ForMember(m => m.CreationTime, opt => opt.MapFrom(src => src.CreationTime))
                .ForMember(m => m.ActionType, opt => opt.MapFrom(src => src.ActionType))
                .ForMember(m => m.ObjectType, opt => opt.MapFrom(src => src.ObjectType))   
                .ForMember(m => m.ResourceId, opt => opt.MapFrom(src => src.ResourceId))
                .ForMember(m => m.Version, opt => opt.MapFrom(src => src.Version))
                .ForMember(m => m.OldName, opt => opt.MapFrom(src => src.OldName));
        }
    }
}