using AutoMapper;
using Infrastructure.Common.Extensions.DateTime;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model;

namespace Infrastructure.Graph.Dto
{
    internal static class ResourceAuditMapper
    {
        public static void Configure()
        {
            Mapper.CreateMap<ResourceAudit, ResourceAuditEntry>()
                .ForMember(m => m.OutId, opt => opt.MapFrom(src => src.ResourceId))
                .ForMember(m => m.InId, opt => opt.MapFrom(src => src.ResourceId))
                .ForMember(m => m.Comment, opt => opt.MapFrom(src => src.Comment))
                .ForMember(m => m.NewData, opt => opt.MapFrom(src => src.NewData))
                .ForMember(m => m.OldData, opt => opt.MapFrom(src => src.OldData))
                .ForMember(m => m.NewType, opt => opt.MapFrom(src => src.NewType))
                .ForMember(m => m.OldType, opt => opt.MapFrom(src => src.OldType))
                .ForMember(m => m.TimeStamp, opt => opt.MapFrom(src => src.TimeStamp))
                .ForMember(m => m.Action, opt => opt.MapFrom(src => src.Action))
                .ForMember(m => m.AdditionalInfo, opt => opt.MapFrom(src => src.AdditionalInfo))
                .ForMember(m => m.AdditionalResourceId, opt => opt.MapFrom(src => src.AdditionalResourceId))
                .ForMember(m => m.ResourceSchema, opt => opt.MapFrom(src => src.ResourceSet))
                .ForMember(m => m.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(m => m.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(m => m.Id, opt => opt.MapFrom(src => src.Id));

            Mapper.CreateMap<ResourceAuditEntry, ResourceAudit>()
               .ForMember(m => m.ResourceId, opt => opt.MapFrom(src => src.InId))
               .ForMember(m => m.Comment, opt => opt.MapFrom(src => src.Comment))
               .ForMember(m => m.NewData, opt => opt.MapFrom(src => src.NewData))
               .ForMember(m => m.OldData, opt => opt.MapFrom(src => src.OldData))
               .ForMember(m => m.NewType, opt => opt.MapFrom(src => src.NewType))
               .ForMember(m => m.OldType, opt => opt.MapFrom(src => src.OldType))
               .ForMember(m => m.TimeStamp, opt => opt.MapFrom(src => src.TimeStamp))
               .ForMember(m => m.Action, opt => opt.MapFrom(src => src.Action))
               .ForMember(m => m.AdditionalInfo, opt => opt.MapFrom(src => src.AdditionalInfo))
               .ForMember(m => m.AdditionalResourceId, opt => opt.MapFrom(src => src.AdditionalResourceId))
               .ForMember(m => m.ResourceSet, opt => opt.MapFrom(src => src.ResourceSchema))
               .ForMember(m => m.UserId, opt => opt.MapFrom(src => src.UserId))
               .ForMember(m => m.UserName, opt => opt.MapFrom(src => src.UserName))
               .ForMember(m => m.Id, opt => opt.MapFrom(src => src.Id));

            Mapper.CreateMap<ResourceAuditSolrEntry, ResourceAudit>()
               .ForMember(m => m.ResourceId, opt => opt.MapFrom(src => src.InId))
               .ForMember(m => m.Comment, opt => opt.MapFrom(src => src.Comment))
               .ForMember(m => m.NewData, opt => opt.MapFrom(src => src.NewData))
               .ForMember(m => m.OldData, opt => opt.MapFrom(src => src.OldData))
               .ForMember(m => m.NewType, opt => opt.MapFrom(src => src.NewType))
               .ForMember(m => m.OldType, opt => opt.MapFrom(src => src.OldType))
               .ForMember(m => m.TimeStamp, opt => opt.MapFrom(src => src.TimeStamp))
               .ForMember(m => m.Action, opt => opt.MapFrom(src => src.Action))
               .ForMember(m => m.AdditionalInfo, opt => opt.MapFrom(src => src.AdditionalInfo))
               .ForMember(m => m.AdditionalResourceId, opt => opt.MapFrom(src => src.AdditionalResourceId))
               .ForMember(m => m.ResourceSet, opt => opt.MapFrom(src => src.ResourceSchema))
               .ForMember(m => m.UserId, opt => opt.MapFrom(src => src.UserId))
               .ForMember(m => m.UserName, opt => opt.MapFrom(src => src.UserName))
               .ForMember(m => m.Id, opt => opt.MapFrom(src => src.Id));
        }
    }
}