﻿namespace Infrastructure.Graph.Dto
{
    public static class DtoDefaultConfigurator
    {
        public static void Configure()
        {
            ResourceSetMapper.Configure();
            ResourceMetadataMapper.Configure();
            ResourceAuditMapper.Configure();
            ResourceEventMapper.Configure();
            PVEventMapper.Configure();
            ReplicationEventMapper.Configure();
        }
    }
}
