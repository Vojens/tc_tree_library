using AutoMapper;
using Infrastructure.Common.Extensions.DateTime;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Model;

namespace Infrastructure.Graph.Dto
{
    internal static class ResourceMetadataMapper
    {
        public static void Configure()
        {
            Mapper.CreateMap<ResourceMetadataInternal, ResourceMetadataEntry>()
                .ForMember(m => m.OutId, opt => opt.MapFrom(src => src.Id))
                .ForMember(m => m.InId, opt => opt.MapFrom(src => src.Id))
                .ForMember(m => m.Uri, opt => opt.MapFrom(src => src.Uri))
                .ForMember(m => m.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(m => m.Type, opt => opt.MapFrom(src => src.Type))
                .ForMember(m => m.SchemaType, opt => opt.MapFrom(src => src.SchemaType))
                .ForMember(m => m.Schema, opt => opt.MapFrom(src => src.Schema))
                .ForMember(m => m.CreationUserId, opt => opt.MapFrom(src => src.CreationUserId))
                .ForMember(m => m.CreationUserName, opt => opt.MapFrom(src => src.CreationUserName))
                .ForMember(m => m.CreationTimestamp, opt => opt.MapFrom(src => src.CreationTimestamp))
                .ForMember(m => m.CreationTime, opt => opt.MapFrom(src => src.CreationTimestamp.ToLocalCassandraTime()))
                .ForMember(m => m.ChangeUserId, opt => opt.MapFrom(src => src.LastUpdateUserId))
                .ForMember(m => m.ChangeUserName, opt => opt.MapFrom(src => src.LastUpdateUserName))
                .ForMember(m => m.ChangeTimestamp, opt => opt.MapFrom(src => src.LastUpdateTimestamp))
                .ForMember(m => m.ChangeTime, opt => opt.MapFrom(src => src.LastUpdateTimestamp.ToLocalCassandraTime()))
                .ForMember(m => m.Version, opt => opt.MapFrom(src => src.Version))
                .ForMember(m => m.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(m => m.Keywords, opt => opt.MapFrom(src => src.Keywords))
                .ForMember(m => m.Workflow, opt => opt.MapFrom(src => src.Workflow))
                .ForMember(m => m.WorkflowState, opt => opt.MapFrom(src => src.WorkflowState))
                .ForMember(m => m.IsMarkedDeleted, opt => opt.MapFrom(src => src.IsMarkedDeleted))
                .ForMember(m => m.IsHidden, opt => opt.MapFrom(src => src.IsHidden))
                .ForMember(m => m.IsReadOnly, opt => opt.MapFrom(src => src.IsReadOnly))
                .ForMember(m => m.IsSystem, opt => opt.MapFrom(src => src.IsSystem))
                .ForMember(m => m.ContentType, opt => opt.MapFrom(src => src.ContentType))
                .ForMember(m => m.StorageType, opt => opt.MapFrom(src => (short)src.StorageType))
                .ForMember(m => m.StorageId, opt => opt.MapFrom(src => src.StorageId))
                .ForMember(m => m.StorageUri, opt => opt.MapFrom(src => src.StorageUri))
                .ForMember(m => m.ResourceSystem, opt => opt.MapFrom(src => src.ResourceSystem));

            //TODO complete the rest of the stuff
            Mapper.CreateMap<ResourceMetadataEntry, ResourceMetadataInternal>()
                .ForMember(m => m.Id, opt => opt.MapFrom(src => src.OutId))
                .ForMember(m => m.Uri, opt => opt.MapFrom(src => src.Uri))
                .ForMember(m => m.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(m => m.Type, opt => opt.MapFrom(src => src.Type))
                .ForMember(m => m.SchemaType, opt => opt.MapFrom(src => src.SchemaType))
                .ForMember(m => m.Schema, opt => opt.MapFrom(src => src.Schema))
                .ForMember(m => m.CreationUserId, opt => opt.MapFrom(src => src.CreationUserId))
                .ForMember(m => m.CreationUserName, opt => opt.MapFrom(src => src.CreationUserName))
                .ForMember(m => m.CreationTimestamp, opt => opt.MapFrom(src => src.CreationTimestamp))
                .ForMember(m => m.LastUpdateUserId, opt => opt.MapFrom(src => src.ChangeUserId))
                .ForMember(m => m.LastUpdateUserName, opt => opt.MapFrom(src => src.ChangeUserName))
                .ForMember(m => m.LastUpdateTimestamp, opt => opt.MapFrom(src => src.ChangeTimestamp))
                .ForMember(m => m.Version, opt => opt.MapFrom(src => src.Version))
                .ForMember(m => m.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(m => m.Keywords, opt => opt.MapFrom(src => src.Keywords))
                .ForMember(m => m.Workflow, opt => opt.MapFrom(src => src.Workflow))
                .ForMember(m => m.WorkflowState, opt => opt.MapFrom(src => src.WorkflowState))
                .ForMember(m => m.IsMarkedDeleted, opt => opt.MapFrom(src => src.IsMarkedDeleted))
                .ForMember(m => m.IsHidden, opt => opt.MapFrom(src => src.IsHidden))
                .ForMember(m => m.IsReadOnly, opt => opt.MapFrom(src => src.IsReadOnly))
                .ForMember(m => m.IsSystem, opt => opt.MapFrom(src => src.IsSystem))
                .ForMember(m => m.ContentType, opt => opt.MapFrom(src => src.ContentType))
                .ForMember(m => m.StorageType, opt => opt.MapFrom(src => (StorageType)src.StorageType))
                .ForMember(m => m.StorageId, opt => opt.MapFrom(src => src.StorageId))
                .ForMember(m => m.StorageUri, opt => opt.MapFrom(src => src.StorageUri))
                .ForMember(m => m.ResourceSystem, opt => opt.MapFrom(src => src.ResourceSystem));

            Mapper.CreateMap<ResourceMetadataEntry, ResourceMetadataSolrEntry>();
            Mapper.CreateMap<ResourceMetadataSolrEntry, ResourceMetadataEntry>();

            Mapper.CreateMap<ResourceMetadataSolrEntry, ResourceMetadataInternal>()
                .ForMember(m => m.Id, opt => opt.MapFrom(src => src.OutId))
                .ForMember(m => m.Uri, opt => opt.MapFrom(src => src.Uri))
                .ForMember(m => m.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(m => m.Type, opt => opt.MapFrom(src => src.Type))
                .ForMember(m => m.SchemaType, opt => opt.MapFrom(src => src.SchemaType))
                .ForMember(m => m.Schema, opt => opt.MapFrom(src => src.Schema))
                .ForMember(m => m.CreationUserId, opt => opt.MapFrom(src => src.CreationUserId))
                .ForMember(m => m.CreationUserName, opt => opt.MapFrom(src => src.CreationUserName))
                .ForMember(m => m.CreationTimestamp, opt => opt.MapFrom(src => src.CreationTimestamp))
                .ForMember(m => m.LastUpdateUserId, opt => opt.MapFrom(src => src.ChangeUserId))
                .ForMember(m => m.LastUpdateUserName, opt => opt.MapFrom(src => src.ChangeUserName))
                .ForMember(m => m.LastUpdateTimestamp, opt => opt.MapFrom(src => src.ChangeTimestamp))
                .ForMember(m => m.Version, opt => opt.MapFrom(src => src.Version))
                .ForMember(m => m.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(m => m.Keywords, opt => opt.MapFrom(src => src.Keywords))
                .ForMember(m => m.Workflow, opt => opt.MapFrom(src => src.Workflow))
                .ForMember(m => m.WorkflowState, opt => opt.MapFrom(src => src.WorkflowState))
                .ForMember(m => m.IsMarkedDeleted, opt => opt.MapFrom(src => src.IsMarkedDeleted))
                .ForMember(m => m.IsHidden, opt => opt.MapFrom(src => src.IsHidden))
                .ForMember(m => m.IsReadOnly, opt => opt.MapFrom(src => src.IsReadOnly))
                .ForMember(m => m.IsSystem, opt => opt.MapFrom(src => src.IsSystem))
                .ForMember(m => m.ContentType, opt => opt.MapFrom(src => src.ContentType))
                .ForMember(m => m.StorageType, opt => opt.MapFrom(src => (StorageType) src.StorageType))
                .ForMember(m => m.StorageId, opt => opt.MapFrom(src => src.StorageId))
                .ForMember(m => m.StorageUri, opt => opt.MapFrom(src => src.StorageUri))
                .ForMember(m => m.ResourceSystem, opt => opt.MapFrom(src => src.ResourceSystem));
        }
    }
}