﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Infrastructure.Graph.Solr.FieldSerializers
{
    internal class StringFieldSerializer : ISolrFieldSerializer
    {
        public bool CanHandleType(Type t)
        {
            return t == typeof(string);
        }

        public IEnumerable<PropertyNode> Serialize(object obj)
        {
            if (obj == null)
                yield break;
            yield return new PropertyNode
            {
                FieldValue = obj.ToString()
            };
        }
    }
}