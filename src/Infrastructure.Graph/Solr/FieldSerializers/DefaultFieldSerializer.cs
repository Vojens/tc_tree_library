﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Graph.Solr.FieldSerializers
{
    internal class DefaultFieldSerializer : ISolrFieldSerializer
    {
        private readonly AggregateFieldSerializer _serializer;

        public DefaultFieldSerializer()
        {
            _serializer = new AggregateFieldSerializer(new ISolrFieldSerializer[] {
                new FormattableFieldSerializer(), new StringFieldSerializer()
            });
        }

        public bool CanHandleType(Type t)
        {
            return _serializer.CanHandleType(t);
        }

        public IEnumerable<PropertyNode> Serialize(object obj)
        {
            return _serializer.Serialize(obj);
        }
    }
}