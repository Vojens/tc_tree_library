﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Graph.Solr.FieldSerializers
{
    internal class PropertyNode
    {
        public string FieldValue { get; set; }

        public string FieldNameSuffix { get; set; }
    }

    internal interface ISolrFieldSerializer
    {
        bool CanHandleType(Type t);

        IEnumerable<PropertyNode> Serialize(object obj);
    }
}
