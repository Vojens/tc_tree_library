using System.Collections.Generic;

namespace Infrastructure.Graph.Solr.Query
{
    internal class SolrQueryInList : AbstractSolrQuery
    {
        private readonly string _fieldName;
        private readonly IEnumerable<string> _list;

        public SolrQueryInList(string fieldName, IEnumerable<string> list)
        {
            _fieldName = fieldName;
            _list = list;
        }

        public SolrQueryInList(string fieldName, params string[] values) : this(fieldName, (IEnumerable<string>)values) { }


        public string FieldName
        {
            get { return _fieldName; }
        }

        public IEnumerable<string> List
        {
            get { return _list; }
        }
    }
}