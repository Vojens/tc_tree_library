using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Graph.Solr.Query
{
    internal class LocalParams : Dictionary<string, string>
    {
        public LocalParams() { }

        public LocalParams(IDictionary<string, string> dictionary) : base(dictionary) { }

        public override string ToString()
        {
            if (Count == 0)
                return string.Empty;
            var sb = new StringBuilder();
            sb.Append("{!");
            sb.Append(string.Join(" ", this.Select(kv => string.Format("{0}={1}", kv.Key, Quote(kv.Value))).ToArray()));
            sb.Append("}");
            return sb.ToString();
        }

        private static string Quote(string v)
        {
            if (v == null)
                throw new ArgumentNullException("v");
            if (!v.Contains(" "))
                return v;
            return string.Format("'{0}'", v.Replace("'", "\\'"));
        }

        public static ISolrQuery operator +(LocalParams p, ISolrQuery q)
        {
            return new LocalParamsQuery(q, p);
        }
    }
}