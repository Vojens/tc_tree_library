﻿using System;
using System.Text.RegularExpressions;

namespace Infrastructure.Graph.Solr.Query
{
    internal enum Order
    {
        ASC,
        DESC
    }

    internal class SortOrder
    {
        private readonly string fieldName;
        private readonly Order order = Order.ASC;
        private static readonly Regex parseRx = new Regex("\\s+", RegexOptions.Compiled);

        public SortOrder(string fieldName)
        {
            this.fieldName = fieldName;
        }

        public SortOrder(string fieldName, Order order)
            : this(fieldName)
        {
            this.order = order;
        }

        public string FieldName
        {
            get { return fieldName; }
        }

        public Order Order
        {
            get { return order; }
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", FieldName, Order.ToString().ToLower());
        }

        public static SortOrder Parse(string s)
        {
            if (string.IsNullOrEmpty(s))
                return null;
            try
            {
                var tokens = parseRx.Split(s.Trim());
                string field = tokens[0];
                if (tokens.Length > 1)
                {
                    var o = (Order)Enum.Parse(typeof(Order), tokens[1].ToUpper());
                    return new SortOrder(field, o);
                }
                return new SortOrder(field);
            }
            catch (Exception e)
            {
                throw new ArgumentOutOfRangeException(s);
            }
        }

        public bool Equals(SortOrder other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            return Equals(other.fieldName, fieldName) && Equals(other.order, order);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != typeof(SortOrder))
            {
                return false;
            }
            return Equals((SortOrder)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((fieldName != null ? fieldName.GetHashCode() : 0) * 397) ^ order.GetHashCode();
            }
        }
    }
}