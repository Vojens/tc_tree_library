namespace Infrastructure.Graph.Solr.Query
{
    internal class SolrQueryByField : AbstractSolrQuery
    {
        private readonly string _fieldName;
        private readonly string _fieldValue;

        public SolrQueryByField(string fieldName, string fieldValue)
        {
            _fieldName = fieldName;
            _fieldValue = fieldValue;
            Quoted = true;
            EscapeSpace = true;
            CharReplace = true;
        }

        public bool Quoted { get; set; }

        public bool EscapeValue{ get; set; }

        public bool EscapeSpace { get; set; }

        public bool CharReplace { get; set; }

        public string FieldName
        {
            get { return _fieldName; }
        }

        public string FieldValue
        {
            get { return _fieldValue; }
        }
    }
}