using System.Collections.Generic;

namespace Infrastructure.Graph.Solr.Query
{
    internal class SolrNestedJoinQuery : AbstractSolrQuery
    {
        public SolrJoinQuery JoinQuery { get; private set; }

        public IEnumerable<SolrMagicQuery> MagicQueries { get; private set; }

        public SolrNestedJoinQuery(SolrJoinQuery joinQuery, IEnumerable<SolrMagicQuery> queries)
        {
            JoinQuery = joinQuery;
            MagicQueries = queries;
        }

        public static SolrNestedJoinQuery Create(SolrJoinQuery joinQuery, params SolrMagicQuery[] queries)
        {
            return Create(joinQuery, (IEnumerable<SolrMagicQuery>)queries);
        }

        public static SolrNestedJoinQuery Create(SolrJoinQuery joinQuery, IEnumerable<SolrMagicQuery> queries)
        {
            return new SolrNestedJoinQuery(joinQuery, queries);
        }
    }
}