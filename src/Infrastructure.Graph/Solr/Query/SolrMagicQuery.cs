namespace Infrastructure.Graph.Solr.Query
{
    internal class SolrMagicQuery : AbstractSolrQuery
    {
        public string FromIndex { get; private set; }

        public ISolrQuery Condition { get; private set; }

        public SolrMagicQuery(string fromIndex, ISolrQuery condition)
        {
            FromIndex = fromIndex;
            Condition = condition;
        }
    }
}