﻿using System.Collections.Generic;

namespace Infrastructure.Graph.Solr.Query
{
    internal class SolrQueryOptions
    {
        public int? Start { get; set; }

        //public int? Rows { get; set; }

        public ISolrQuery FilterQuery { get; set; }

        public ICollection<SortOrder> OrderBy { get; set; }

    }
}