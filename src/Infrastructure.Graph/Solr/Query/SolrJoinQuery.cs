﻿namespace Infrastructure.Graph.Solr.Query
{
    internal class SolrJoinQuery : AbstractSolrQuery
    {
        public string FromIndex { get; private set; }

        public ISolrQuery Condition { get; private set; }

        public SolrJoinQuery(string fromIndex, ISolrQuery condition)
        {
            FromIndex = fromIndex;
            Condition = condition;
        }
    }
}