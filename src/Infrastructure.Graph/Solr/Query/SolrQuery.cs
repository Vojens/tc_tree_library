﻿namespace Infrastructure.Graph.Solr.Query
{
    internal class SolrQuery : AbstractSolrQuery, ISelfSerializingQuery
    {
        private readonly string _query;

        public SolrQuery(string query)
        {
            _query = query;
        }

        public string Query
        {
            get { return _query; }
        }
        public static readonly AbstractSolrQuery All = new SolrQuery("*:*");
    }
}