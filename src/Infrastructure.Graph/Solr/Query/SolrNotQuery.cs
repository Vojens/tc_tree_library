namespace Infrastructure.Graph.Solr.Query
{
    internal class SolrNotQuery : AbstractSolrQuery
    {
        private readonly ISolrQuery _query;

        public SolrNotQuery(ISolrQuery q)
        {
            _query = q;
        }

        public ISolrQuery Query
        {
            get { return _query; }
        }
    }
}