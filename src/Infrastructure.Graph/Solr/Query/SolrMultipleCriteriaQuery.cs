using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Graph.Solr.Query
{
    internal class SolrMultipleCriteriaQuery : AbstractSolrQuery
    {
        private readonly string _oper;
        private readonly IEnumerable<ISolrQuery> _queries;

        public string Oper
        {
            get { return _oper; }
        }

        public IEnumerable<ISolrQuery> Queries
        {
            get { return _queries; }
        }

        public SolrMultipleCriteriaQuery(IEnumerable<ISolrQuery> queries) : this(queries, string.Empty) { }

        public SolrMultipleCriteriaQuery(IEnumerable<ISolrQuery> queries, string oper)
        {
            _queries = queries;
            _oper = oper;
        }

        public static SolrMultipleCriteriaQuery Create(string oper = "", params ISolrQuery[] queries)
        {
            return Create(queries, oper);
        }

        public static SolrMultipleCriteriaQuery Create<T>(IEnumerable<T> queries, string oper) where T : ISolrQuery
        {
            return new SolrMultipleCriteriaQuery(queries.Cast<ISolrQuery>(), oper);
        }
    }
}