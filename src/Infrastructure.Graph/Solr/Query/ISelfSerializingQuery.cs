﻿namespace Infrastructure.Graph.Solr.Query
{
    internal interface ISelfSerializingQuery : ISolrQuery
    {
        string Query { get; }
    }
}