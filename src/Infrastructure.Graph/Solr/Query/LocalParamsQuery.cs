namespace Infrastructure.Graph.Solr.Query
{
    internal class LocalParamsQuery : ISolrQuery
    {
        private readonly ISolrQuery _query;
        private readonly LocalParams _local;

        public LocalParamsQuery(ISolrQuery query, LocalParams local)
        {
            _query = query;
            _local = local;
        }

        public ISolrQuery Query
        {
            get { return _query; }
        }

        public LocalParams Local
        {
            get { return _local; }
        }
    }
}