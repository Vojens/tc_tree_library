namespace Infrastructure.Graph.Solr.Query
{
    internal class SolrQueryByStringRange<T> : AbstractSolrQuery, ISolrQueryByRange
    {
        private readonly string _fieldName;
        private readonly T _from;
        private readonly T _to;
        private readonly bool _inclusiveFrom;
        private readonly bool _inclusiveTo;

        public SolrQueryByStringRange(string fieldName, T from, T to) : this(fieldName, from, to, true) { }


        public SolrQueryByStringRange(string fieldName, T @from, T to, bool inclusive) : this(fieldName, from, to, inclusive, inclusive) { }


        public SolrQueryByStringRange(string fieldName, T @from, T to, bool inclusiveFrom, bool inclusiveTo)
        {
            _fieldName = fieldName;
            _from = from;
            _to = to;
            _inclusiveFrom = inclusiveFrom;
            _inclusiveTo = inclusiveTo;
        }

        public string FieldName
        {
            get { return _fieldName; }
        }

        public T From
        {
            get { return _from; }
        }

        object ISolrQueryByRange.From
        {
            get { return _from; }
        }

        public T To
        {
            get { return _to; }
        }

        object ISolrQueryByRange.To
        {
            get { return _to; }
        }

        public bool Inclusive
        {
            get { return _inclusiveFrom && _inclusiveTo; }
        }

        public bool InclusiveFrom
        {
            get { return _inclusiveFrom; }
        }

        public bool InclusiveTo
        {
            get { return _inclusiveTo; }
        }
    }
}