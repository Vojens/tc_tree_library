namespace Infrastructure.Graph.Solr.Query
{
    internal interface ISolrQueryByRange
    {
        string FieldName { get; }

        object From { get; }

        object To { get; }

        bool Inclusive { get; }

        bool InclusiveFrom { get; }

        bool InclusiveTo { get; }
    }
}