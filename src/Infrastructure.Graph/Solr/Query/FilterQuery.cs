﻿namespace Infrastructure.Graph.Solr.Query
{
    internal class FilterQuery
    {
        public string FromIndex { get; private set; }

        public ISolrQuery Condition { get; private set; }

        public FilterQuery(string fromIndex, ISolrQuery condition)
        {
            FromIndex = fromIndex;
            Condition = condition;
        }

        public SolrJoinQuery ToJoinQuery()
        {
            return new SolrJoinQuery(FromIndex, Condition);
        }

        public SolrMagicQuery ToMagicQuery()
        {
            return new SolrMagicQuery(FromIndex, Condition);
        }
    }
}