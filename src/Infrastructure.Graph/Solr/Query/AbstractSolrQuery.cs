﻿using System;

namespace Infrastructure.Graph.Solr.Query
{
    internal class AbstractSolrQuery : ISolrQuery
    {
        public AbstractSolrQuery Not()
        {
            return new SolrNotQuery(this);
        }

        internal class Operator
        {
            public const string OR = "OR";
            public const string AND = "AND";
        }

        public static AbstractSolrQuery operator &(AbstractSolrQuery a, AbstractSolrQuery b)
        {
            if (a == null) throw new ArgumentNullException("a");
            if (b == null) throw new ArgumentNullException("b");

            return new SolrMultipleCriteriaQuery(new[] { a, b }, Operator.AND);
        }

        public static AbstractSolrQuery operator |(AbstractSolrQuery a, AbstractSolrQuery b)
        {
            if (a == null) throw new ArgumentNullException("a");
            if (b == null) throw new ArgumentNullException("b");

            return new SolrMultipleCriteriaQuery(new[] { a, b }, Operator.OR);
        }

        public static AbstractSolrQuery operator +(AbstractSolrQuery a, AbstractSolrQuery b)
        {
            if (a == null) throw new ArgumentNullException("a");
            if (b == null) throw new ArgumentNullException("b");

            return new SolrMultipleCriteriaQuery(new[] { a, b });
        }

        public static AbstractSolrQuery operator -(AbstractSolrQuery a, AbstractSolrQuery b)
        {
            if (a == null) throw new ArgumentNullException("a");
            if (b == null) throw new ArgumentNullException("b");

            return new SolrMultipleCriteriaQuery(new[] { a, b.Not() });
        }

        public static AbstractSolrQuery operator !(AbstractSolrQuery a)
        {
            if (a == null) throw new ArgumentNullException("a");

            return a.Not();
        }

        public static bool operator false(AbstractSolrQuery a)
        {
            return false;
        }

        public static bool operator true(AbstractSolrQuery a)
        {
            return false;
        }
    }
}
