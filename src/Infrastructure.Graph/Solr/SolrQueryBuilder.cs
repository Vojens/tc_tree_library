﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Infrastructure.Graph.Solr.Query;
using Infrastructure.Graph.Solr.QuerySerializers;

namespace Infrastructure.Graph.Solr
{
    internal class SolrQueryBuilder : ISolrQueryBuilder
    {
        private readonly ISolrQuerySerializer _querySerializer;

        public SolrQueryBuilder()
        {
            _querySerializer = new DefaultQuerySerializer();
        }

        public string Build(ISolrQuery query, SolrQueryOptions options)
        {
            var parameters = GetQueryParameters(query, options).ToDictionary(x => x.Key, x => x.Value);
            var solrQuery = JsonConvert.SerializeObject(parameters);
            return solrQuery;
        }

        public string Build(ISolrQuery query, FilterQuery[] filterQueries, int? start, params SortOrder[] orderBy)
        {
            if (filterQueries != null && filterQueries.Length > 0)
            {
                if (filterQueries.Length > 1)
                {
                    return Build(query, new SolrQueryOptions
                    {
                        FilterQuery = SolrNestedJoinQuery.Create(filterQueries[0].ToJoinQuery(),
                            filterQueries.Skip(1).Select(x => x.ToMagicQuery())),
                        OrderBy = orderBy,
                        Start = start
                    });
                }
                return Build(query, new SolrQueryOptions { FilterQuery = filterQueries.First().ToJoinQuery(), OrderBy = orderBy, Start = start });
            }
            return Build(query, new SolrQueryOptions { OrderBy = orderBy, Start = start });
        }

        private IEnumerable<KeyValuePair<string, object>> GetQueryParameters(ISolrQuery query, SolrQueryOptions options)
        {
            yield return Create("q", _querySerializer.Serialize(query));

            if (options == null)
            {
                yield return Create("paging", "driver");
                yield break;
            }

            foreach (var p in GetCommonParameters(options))
            {
                yield return p;
            }

            if (options.OrderBy != null && options.OrderBy.Count > 0)
            {
                yield return Create("sort", string.Join(",", options.OrderBy.Where(x => x != null).Select(x => x.ToString())));
                yield return Create("useFieldCache", true);
            }
          
        }

        private IEnumerable<KeyValuePair<string, object>> GetCommonParameters(SolrQueryOptions options)
        {
            if (options == null) yield break;

            if (options.Start.HasValue)
            {
                yield return Create("start", options.Start.ToString());
            }
            else
            {
                yield return Create("paging", "driver");
            }

            //if (options.Rows.HasValue)
            //{
            //    yield return Create("rows", options.Rows.ToString());
            //}
            if (options.FilterQuery == null) yield break;

            yield return Create("fq", _querySerializer.Serialize(options.FilterQuery));
        }

        private KeyValuePair<string, object> Create(string key, object value)
        {
            return new KeyValuePair<string, object>(key, value);
        }
    }
}