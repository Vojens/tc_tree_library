using System.Collections.Generic;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Solr
{
    internal interface ISolrQueryBuilder
    {
        string Build(ISolrQuery query, SolrQueryOptions options);

        string Build(ISolrQuery query, FilterQuery[] filterQueries, int? start,params SortOrder[] orderBy);
    }
}