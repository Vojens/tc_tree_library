﻿using System.Linq;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Solr.QuerySerializers
{
    internal class QueryInListSerializer : SingleTypeQuerySerializer<SolrQueryInList>
    {
        private readonly ISolrQuerySerializer _serializer;
        private static readonly string Seperator = string.Format(" {0} ", AbstractSolrQuery.Operator.OR);

        public QueryInListSerializer(ISolrQuerySerializer serializer)
        {
            _serializer = serializer;
        }

        public override string Serialize(SolrQueryInList q)
        {
            if (string.IsNullOrEmpty(q.FieldName) || q.List == null || !q.List.Any())
                return null;

            var array = q.List.Select(x => "(" + QueryByFieldSerializer.Quote(x) + ")").ToArray();
            return "(" + _serializer.Serialize(new SolrQueryByField(QueryByFieldSerializer.EscapeSpaces(q.FieldName),
                       string.Join(Seperator, array)) { Quoted = false }) + ")";
        }
    }
}