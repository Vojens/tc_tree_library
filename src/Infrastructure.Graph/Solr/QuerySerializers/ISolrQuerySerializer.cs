﻿using System;

namespace Infrastructure.Graph.Solr.QuerySerializers
{
    internal interface ISolrQuerySerializer
    {
        bool CanHandleType(Type t);
        string Serialize(object q);
    }
}