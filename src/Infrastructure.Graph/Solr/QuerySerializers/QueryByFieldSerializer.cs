﻿using System;
using System.Text.RegularExpressions;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Solr.QuerySerializers
{
    internal class QueryByFieldSerializer : SingleTypeQuerySerializer<SolrQueryByField>
    {
        public override string Serialize(SolrQueryByField q)
        {
            if (q.FieldName == null || q.FieldValue == null)
            {
                return null;
            }
            return (q.Quoted ? string.Format("{0}:({1})", EscapeSpaces(q.FieldName), Quote(q.FieldValue, q.EscapeSpace, q.CharReplace))
                : (q.EscapeValue ? string.Format("{0}:\"{1}\"", q.FieldName, q.FieldValue)
                    : string.Format("{0}:({1})", q.FieldName, q.FieldValue)));
        }

        public static readonly Regex SpecialCharactersRx = new Regex("(\\+|\\-|\\&\\&|\\|\\||\\!|\\{|\\}|\\[|\\]|\\^|\\(|\\)|\\\"|\\~|\\:|\\;|\\\\|\\?|\\*|\\/)", RegexOptions.Compiled);

        public static string EscapeSpaces(string value)
        {
            return value.Replace(" ", @"\ ");
        }

        public static string Quote(string value, bool escapeSpace = true, bool specialCharReplace = true)
        {
            string r;
            if (specialCharReplace)
            {
                r = SpecialCharactersRx.Replace(value, "\\$1");
                if (escapeSpace && r.IndexOf(' ') != -1 || r == "")
                    r = string.Format("\"{0}\"", r);
            }
            else
            {
                r = string.Format("\"{0}\"", value);
            }
            return r;
        }
    }
}