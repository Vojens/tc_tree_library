using System;
using System.Linq;

namespace Infrastructure.Graph.Solr.QuerySerializers
{
    internal class AggregateQuerySerializer : ISolrQuerySerializer
    {
        private readonly ISolrQuerySerializer[] _serializers;

        public AggregateQuerySerializer(ISolrQuerySerializer[] serializers)
        {
            _serializers = serializers;
        }

        public bool CanHandleType(Type t)
        {
            return _serializers.Any(s => s.CanHandleType(t));
        }

        public string Serialize(object q)
        {
            if (q == null) return string.Empty;
            var t = q.GetType();
            foreach (var s in _serializers)
            {
                if (s.CanHandleType(t))
                {
                    return s.Serialize(q);
                }
            }
            throw new ApplicationException(string.Format("Couldn't serialize query '{0}' of type '{1}'", q, t));
        }
    }
}