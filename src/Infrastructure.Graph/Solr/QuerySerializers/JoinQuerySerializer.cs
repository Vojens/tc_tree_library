﻿using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Solr.QuerySerializers
{
    internal class JoinQuerySerializer : SingleTypeQuerySerializer<SolrJoinQuery>
    {
        private readonly ISolrQuerySerializer _serializer;

        public JoinQuerySerializer(ISolrQuerySerializer serializer)
        {
            _serializer = serializer;
        }

        public override string Serialize(SolrJoinQuery q)
        {
            if (string.IsNullOrEmpty(q.FromIndex) || q.Condition == null)
                return null;

            var local = new LocalParams { { "type", "join" }, { "fromIndex", q.FromIndex } };
            return local + _serializer.Serialize(q.Condition);
        }
    }
}