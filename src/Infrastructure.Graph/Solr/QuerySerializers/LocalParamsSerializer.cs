﻿using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Solr.QuerySerializers
{
    internal class LocalParamsSerializer : SingleTypeQuerySerializer<LocalParamsQuery>
    {
        private readonly ISolrQuerySerializer _serializer;

        public LocalParamsSerializer(ISolrQuerySerializer serializer)
        {
            _serializer = serializer;
        }

        public override string Serialize(LocalParamsQuery q)
        {
            return q.Local + _serializer.Serialize(q.Query);
        }
    }
}