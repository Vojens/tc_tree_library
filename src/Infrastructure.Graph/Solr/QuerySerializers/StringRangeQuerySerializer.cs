using System;
using System.Linq;
using Infrastructure.Graph.Solr.FieldSerializers;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Solr.QuerySerializers
{
    internal class StringRangeQuerySerializer<T> : ISolrQuerySerializer
    {
        private readonly ISolrFieldSerializer _fieldSerializer;

        public StringRangeQuerySerializer(ISolrFieldSerializer fieldSerializer)
        {
            _fieldSerializer = fieldSerializer;
        }

        public bool CanHandleType(Type t)
        {
            return t == typeof(T);
        }

        public static string BuildRange(string fieldName, string @from, string @to, bool inclusive)
        {
            return BuildRange(fieldName, @from, @to, inclusive, inclusive);
        }

        public static string BuildRange(string fieldName, string @from, string @to, bool inclusiveFrom, bool inclusiveTo)
        {
            return "$field:$ii$from TO $to$if"
                .Replace("$field", QueryByFieldSerializer.EscapeSpaces(fieldName))
                .Replace("$ii", inclusiveFrom ? "[" : "{")
                .Replace("$if", inclusiveTo ? "]" : "}")
                .Replace("$from", @from)
                .Replace("$to", to);
        }

        public string SerializeValue(object o)
        {
            if (o == null) return "*";
            return _fieldSerializer.Serialize(o).First().FieldValue;
        }

        public string Serialize(object q)
        {
            var query = (ISolrQueryByRange)q;
            return BuildRange(query.FieldName,
                SerializeValue(query.From),
                SerializeValue(query.To),
                query.InclusiveFrom,
                query.InclusiveTo
                );
        }
    }
}