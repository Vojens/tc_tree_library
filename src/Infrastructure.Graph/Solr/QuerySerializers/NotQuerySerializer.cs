﻿using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Solr.QuerySerializers
{
    internal class NotQuerySerializer : SingleTypeQuerySerializer<SolrNotQuery>
    {
        private readonly ISolrQuerySerializer _serializer;

        public NotQuerySerializer(ISolrQuerySerializer serializer)
        {
            _serializer = serializer;
        }

        public override string Serialize(SolrNotQuery q)
        {
            return "-" + _serializer.Serialize(q.Query);
        }
    }
}