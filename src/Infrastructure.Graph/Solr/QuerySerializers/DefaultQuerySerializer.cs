﻿using System;
using Infrastructure.Graph.Solr.FieldSerializers;

namespace Infrastructure.Graph.Solr.QuerySerializers
{
    internal class DefaultQuerySerializer : ISolrQuerySerializer
    {
        private readonly AggregateQuerySerializer _serializer;

        public DefaultQuerySerializer()
        {
            var fieldSerializer = new DefaultFieldSerializer();
            _serializer = new AggregateQuerySerializer(new ISolrQuerySerializer[] {
                new LocalParamsSerializer(this),
                new QueryByFieldSerializer(),
                new NotQuerySerializer(this),
                new QueryInListSerializer(this),
                new JoinQuerySerializer(this),
                new StringRangeQuerySerializer<string>(fieldSerializer),
                new RangeQuerySerializer(fieldSerializer),
                new MagicQuerySerializer(this), 
                new MultipleCriteriaQuerySerializer(this),
                new NestedJoinQuerySerializer(this), 
                new SelfSerializingQuerySerializer()
            });
        }

        public bool CanHandleType(Type t)
        {
            return _serializer.CanHandleType(t);
        }

        public string Serialize(object q)
        {
            return _serializer.Serialize(q);
        }
    }
}
