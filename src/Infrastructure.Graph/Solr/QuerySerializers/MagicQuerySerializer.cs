using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Solr.QuerySerializers
{
    internal class MagicQuerySerializer : SingleTypeQuerySerializer<SolrMagicQuery>
    {
        private readonly ISolrQuerySerializer _serializer;

        public MagicQuerySerializer(ISolrQuerySerializer serializer)
        {
            _serializer = serializer;
        }

        public override string Serialize(SolrMagicQuery q)
        {
            if (string.IsNullOrEmpty(q.FromIndex) || q.Condition == null)
                return null;

            var local = new LocalParams { { "type", "join" }, { "fromIndex", q.FromIndex } };
            return string.Format("_query_: \"{0}{1}\"", local, _serializer.Serialize(q.Condition));
        }
    }
}