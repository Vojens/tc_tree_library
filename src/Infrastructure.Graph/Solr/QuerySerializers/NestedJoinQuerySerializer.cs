﻿using System.Text;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Solr.QuerySerializers
{
    internal class NestedJoinQuerySerializer : SingleTypeQuerySerializer<SolrNestedJoinQuery>
    {
        private readonly ISolrQuerySerializer _serializer;

        public NestedJoinQuerySerializer(ISolrQuerySerializer serializer)
        {
            _serializer = serializer;
        }

        public override string Serialize(SolrNestedJoinQuery q)
        {
            if (q.JoinQuery == null || q.MagicQueries == null)
                return null;

            var queryBuilder = new StringBuilder();
            var join = _serializer.Serialize(q.JoinQuery);
            queryBuilder.Append(join);
            foreach (var query in q.MagicQueries)
            {
                if (query == null) continue;
                var sq = _serializer.Serialize(query);
                if (string.IsNullOrEmpty(sq)) continue;
                if (queryBuilder.Length > 0)
                {
                    queryBuilder.AppendFormat(" {0} ", AbstractSolrQuery.Operator.AND);
                }
                queryBuilder.Append(sq);
            }
            var queryString = queryBuilder.ToString();
            return queryString;
        }
    }
}