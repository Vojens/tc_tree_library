﻿using System;
using Infrastructure.Graph.Solr.Query;

namespace Infrastructure.Graph.Solr.QuerySerializers
{
    public class SelfSerializingQuerySerializer : ISolrQuerySerializer
    {
        public bool CanHandleType(Type t)
        {
            return typeof(ISelfSerializingQuery).IsAssignableFrom(t);
        }

        public string Serialize(object q)
        {
            var sq = (ISelfSerializingQuery)q;
            return sq.Query;
        }
    }
}