using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository;

namespace Infrastructure.Graph.Mapping.Cassandra
{
    internal class PropertyMappings : SchemaMappings
    {
        public PropertyMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<PropertyEntry>()
                    .TableName("property")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutId, k2 => k2.InId)
                    .ClusteringKey(c => c.Name)
                    .ClusteringKey(c => c.Key)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.Name, icpcp => icpcp.WithName("nm"))
                    .Column(icp => icp.Key, icpcp => icpcp.WithName("key"))
                    .Column(icp => icp.Value, icpcp => icpcp.WithName("val"))
                    .Column(icp => icp.DoubleValue, icpcp => icpcp.WithName("valdbl"))
                    .Column(icp => icp.IntValue, icpcp => icpcp.WithName("valint"))
                    .Column(icp => icp.Type, icpcp => icpcp.WithName("typ")));


            SchemaContainer.AddSchemaMap(
                For<PropertySolrEntry>()
                    .TableName("property")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutId, k2 => k2.InId)
                    .ClusteringKey(c => c.Name)
                    .ClusteringKey(c => c.Key)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.Name, icpcp => icpcp.WithName("nm"))
                    .Column(icp => icp.Key, icpcp => icpcp.WithName("key"))
                    .Column(icp => icp.Value, icpcp => icpcp.WithName("val"))
                    .Column(icp => icp.DoubleValue, icpcp => icpcp.WithName("valdbl"))
                    .Column(icp => icp.IntValue, icpcp => icpcp.WithName("valint"))
                    .Column(icp => icp.Type, icpcp => icpcp.WithName("typ"))
                    .Column(icp => icp.SolrQuery, icpcp => icpcp.WithName("solr_query")));
        }
    }
}