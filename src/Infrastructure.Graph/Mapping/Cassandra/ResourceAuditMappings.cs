using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository;

namespace Infrastructure.Graph.Mapping.Cassandra
{
    internal class ResourceAuditMappings : SchemaMappings
    {
        public ResourceAuditMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<ResourceAuditEntry>()
                    .TableName("resourcedata_simple_audit_log")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutId, k2 => k2.OutId)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.Action, icpcp => icpcp.WithName("action"))
                    .Column(icp => icp.AdditionalInfo, icpcp => icpcp.WithName("addlinfo"))
                    .Column(icp => icp.AdditionalResourceId, icpcp => icpcp.WithName("addlresid"))
                    .Column(icp => icp.Comment, icpcp => icpcp.WithName("cmnt"))
                    .Column(icp => icp.Id, icpcp => icpcp.WithName("id"))
                    .Column(icp => icp.NewData, icpcp => icpcp.WithName("newdat"))
                    .Column(icp => icp.OldData, icpcp => icpcp.WithName("olddat"))
                    .Column(icp => icp.NewType, icpcp => icpcp.WithName("newtyp"))
                    .Column(icp => icp.OldType, icpcp => icpcp.WithName("oldtyp"))
                    .Column(icp => icp.ResourceType, icpcp => icpcp.WithName("restyp"))
                    .Column(icp => icp.ResourceSchema, icpcp => icpcp.WithName("rs"))
                    .Column(icp => icp.TimeStamp, icpcp => icpcp.WithName("time"))
                    .Column(icp => icp.UserId, icpcp => icpcp.WithName("usrid"))
                    .Column(icp => icp.UserName, icpcp => icpcp.WithName("usrnm")));

            SchemaContainer.AddSchemaMap(
                For<ResourceAuditSolrEntry>()
                    .TableName("resourcedata_simple_audit_log")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutId, k2 => k2.OutId)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.Action, icpcp => icpcp.WithName("action"))
                    .Column(icp => icp.AdditionalInfo, icpcp => icpcp.WithName("addlinfo"))
                    .Column(icp => icp.AdditionalResourceId, icpcp => icpcp.WithName("addlresid"))
                    .Column(icp => icp.Comment, icpcp => icpcp.WithName("cmnt"))
                    .Column(icp => icp.Id, icpcp => icpcp.WithName("id"))
                    .Column(icp => icp.NewData, icpcp => icpcp.WithName("newdat"))
                    .Column(icp => icp.OldData, icpcp => icpcp.WithName("olddat"))
                    .Column(icp => icp.NewType, icpcp => icpcp.WithName("newtyp"))
                    .Column(icp => icp.OldType, icpcp => icpcp.WithName("oldtyp"))
                    .Column(icp => icp.ResourceType, icpcp => icpcp.WithName("restyp"))
                    .Column(icp => icp.ResourceSchema, icpcp => icpcp.WithName("rs"))
                    .Column(icp => icp.TimeStamp, icpcp => icpcp.WithName("time"))
                    .Column(icp => icp.UserId, icpcp => icpcp.WithName("usrid"))
                    .Column(icp => icp.UserName, icpcp => icpcp.WithName("usrnm"))
                    .Column(icp => icp.SolrQuery, icpcp => icpcp.WithName("solr_query")));

            SchemaContainer.AddSchemaMap(For<ResourceAuditDataEntry>()
                .TableName("resourcedata_simple_audit_log_data")
                .ExplicitColumns()
                .PartitionKey(k => k.Id)
                .Column(c => c.Id, c => c.WithName("id"))
                .Column(c => c.NewData, c => c.WithName("newdat"))
                .Column(c => c.OldData, c => c.WithName("olddat")));
        }
    }
}