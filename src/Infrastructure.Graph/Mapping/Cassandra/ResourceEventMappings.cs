using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository;

namespace Infrastructure.Graph.Mapping.Cassandra
{
    internal class ResourceEventMappings : SchemaMappings
    {
        public ResourceEventMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<ResourceEventEntry>()
                    .TableName("resource_events")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.ResourceId, k2 => k2.Bucket)
                    .ClusteringKey(k1 => k1.CreationTime)
                    .Column(icp => icp.Bucket, icpcp => icpcp.WithName("bucket"))
                    .Column(icp => icp.CreationTime, icpcp => icpcp.WithName("ctime"))
                    .Column(icp => icp.ResourceId, icpcp => icpcp.WithName("resid"))
                    .Column(icp => icp.ActionType, icpcp => icpcp.WithName("acttyp"))
                    .Column(icp => icp.ObjectType, icpcp => icpcp.WithName("restyp"))
                    .Column(icp => icp.Version, icpcp => icpcp.WithName("version"))
                    .Column(icp => icp.UniqueIdentifier, icpcp => icpcp.WithName("unqval"))
                    .Column(icp => icp.ParentResourceId, icpcp => icpcp.WithName("pid"))
                    .Column(icp => icp.RuleId, icpcp => icpcp.WithName("rid"))
                    .Column(icp => icp.OldName, icpcp => icpcp.WithName("oldnm"))
                    );

            SchemaContainer.AddSchemaMap(
                For<ResourceEventViewEntry>()
                    .TableName("resource_events_by_bucket")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.ResourceId, k2 => k2.Bucket)
                    .ClusteringKey(k1 => k1.CreationTime)
                    .Column(icp => icp.Bucket, icpcp => icpcp.WithName("bucket"))
                    .Column(icp => icp.CreationTime, icpcp => icpcp.WithName("ctime"))
                    .Column(icp => icp.ResourceId, icpcp => icpcp.WithName("resid"))
                    .Column(icp => icp.ActionType, icpcp => icpcp.WithName("acttyp"))
                    .Column(icp => icp.ObjectType, icpcp => icpcp.WithName("restyp"))
                    .Column(icp => icp.Version, icpcp => icpcp.WithName("version"))
                    .Column(icp => icp.UniqueIdentifier, icpcp => icpcp.WithName("unqval"))
                    .Column(icp => icp.ParentResourceId, icpcp => icpcp.WithName("pid"))
                    .Column(icp => icp.RuleId, icpcp => icpcp.WithName("rid"))
                    .Column(icp => icp.OldName, icpcp => icpcp.WithName("oldnm"))
                    );
        }
    }
}