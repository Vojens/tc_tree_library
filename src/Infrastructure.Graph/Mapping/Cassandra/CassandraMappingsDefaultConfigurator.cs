using Infrastructure.Graph.Repository;

namespace Infrastructure.Graph.Mapping.Cassandra
{
    internal class CassandraMappingsDefaultConfigurator
    {
        public static void Configure(SchemaContainer schemaContainer)
        {
            var resourceSetMappings = new ResourceSetMappings(schemaContainer);
            var resourceMetadataMappings = new ResourceMetadataMappings(schemaContainer);
            var resourceDataMappings = new ResourceDataMappings(schemaContainer);
            var resourceAuditMappings = new ResourceAuditMappings(schemaContainer);
            var resourceExchangeMappings = new ResourceEventMappings(schemaContainer);
            var propertyMappings = new PropertyMappings(schemaContainer);
            var resourceEventMappings = new ReplicationEventMappings(schemaContainer);
            var lockMappings = new LockMappings(schemaContainer);
        }
    }
}