﻿using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository;

namespace Infrastructure.Graph.Mapping.Cassandra
{
    internal class ResourceSetMappings : SchemaMappings
    {
        public ResourceSetMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<ResourceSetEntry>()
                    .TableName("resource_set")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutId, k2 => k2.OutId)
                    .ClusteringKey(c => c.CurveDistance)
                    .ClusteringKey(c => c.OutUri)
                    .ClusteringKey(c => c.InUri)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.CurveDistance, icpcp => icpcp.WithName("cd"))
                    .Column(icp => icp.OutUri, icpcp => icpcp.WithName("outuri"))
                    .Column(icp => icp.InUri, icpcp => icpcp.WithName("inuri"))
                    .Column(icp => icp.StorageType, icpcp => icpcp.WithName("strgtyp"))
                    .Column(icp => icp.Name, icpcp => icpcp.WithName("nm"))
                    .Column(icp => icp.Type, icpcp => icpcp.WithName("typ"))
                    .Column(icp => icp.ResourceSystem, icpcp => icpcp.WithName("rs"))
                    .Column(icp => icp.CurveDistanceBucket, icpcp => icpcp.WithName("cd_bkt")));

            SchemaContainer.AddSchemaMap(
                For<ResourceSetObjectEntry>()
                    .TableName("resource_set")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutId, k2 => k2.OutId)
                    .ClusteringKey(c => c.CurveDistance)
                    .ClusteringKey(c => c.OutUri)
                    .ClusteringKey(c => c.InUri)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.CurveDistance, icpcp => icpcp.WithName("cd"))
                    .Column(icp => icp.OutUri, icpcp => icpcp.WithName("outuri"))
                    .Column(icp => icp.InUri, icpcp => icpcp.WithName("inuri"))
                    .Column(icp => icp.StorageType, icpcp => icpcp.WithName("strgtyp"))
                    .Column(icp => icp.Name, icpcp => icpcp.WithName("nm"))
                    .Column(icp => icp.Type, icpcp => icpcp.WithName("typ"))
                    .Column(icp => icp.ResourceSystem, icpcp => icpcp.WithName("rs"))
                    .Column(icp => icp.CurveDistance, icpcp => icpcp.WithName("cd"))
                    .Column(icp => icp.CurveDistanceBucket, icpcp => icpcp.WithName("cd_bkt")));


            SchemaContainer.AddSchemaMap(
                For<ResourcesByCurveDistanceEntry>()
                    .TableName("resources_by_curve_distance")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutUri, k2 => k2.CurveDistance, k3 => k3.CurveDistanceBucket)
                    .ClusteringKey(c => c.InUri)
                    .ClusteringKey(c => c.OutId)
                    .ClusteringKey(c => c.InId)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.CurveDistance, icpcp => icpcp.WithName("cd"))
                    .Column(icp => icp.OutUri, icpcp => icpcp.WithName("outuri"))
                    .Column(icp => icp.InUri, icpcp => icpcp.WithName("inuri"))
                    .Column(icp => icp.StorageType, icpcp => icpcp.WithName("strgtyp"))
                    .Column(icp => icp.Name, icpcp => icpcp.WithName("nm"))
                    .Column(icp => icp.Type, icpcp => icpcp.WithName("typ"))
                    .Column(icp => icp.ResourceSystem, icpcp => icpcp.WithName("rs"))
                    .Column(icp => icp.CurveDistance, icpcp => icpcp.WithName("cd"))
                    .Column(icp => icp.CurveDistanceBucket, icpcp => icpcp.WithName("cd_bkt")));

            SchemaContainer.AddSchemaMap(
                For<ResourceCruveDistanceCounterEntry>()
                    .TableName("resource_curve_distance_counter")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutUri, k2 => k2.CurveDistance)
                    .ClusteringKey(c => c.Bucket)
                    .Column(icp => icp.OutUri, icpcp => icpcp.WithName("outuri"))
                    .Column(icp => icp.CurveDistance, icpcp => icpcp.WithName("cd"))
                    .Column(icp => icp.Bucket, icpcp => icpcp.WithName("bucket"))
                    .Column(icp => icp.Counter, icpcp => icpcp.WithName("count").AsCounter()));


            SchemaContainer.AddSchemaMap(
                For<ResourceSetCounterEntry>()
                    .TableName("resource_set_counter")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutId, k2 => k2.InId)
                    .ClusteringKey(c => c.CurveDistance)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.CurveDistance, icpcp => icpcp.WithName("cd"))
                    .Column(icp => icp.Counter, icpcp => icpcp.WithName("count").AsCounter()));

            SchemaContainer.AddSchemaMap(
                For<ResourceSetSolrEntry>()
                    .TableName("resource_set")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutId, k2 => k2.InId)
                    .ClusteringKey(c => c.CurveDistance)
                    .ClusteringKey(c => c.OutUri)
                    .ClusteringKey(c => c.InUri)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.CurveDistance, icpcp => icpcp.WithName("cd"))
                    .Column(icp => icp.OutUri, icpcp => icpcp.WithName("outuri"))
                    .Column(icp => icp.InUri, icpcp => icpcp.WithName("inuri"))
                    .Column(icp => icp.StorageType, icpcp => icpcp.WithName("strgtyp"))
                    .Column(icp => icp.Name, icpcp => icpcp.WithName("nm"))
                    .Column(icp => icp.Type, icpcp => icpcp.WithName("typ"))
                    .Column(icp => icp.ResourceSystem, icpcp => icpcp.WithName("rs"))
                    .Column(icp => icp.CurveDistance, icpcp => icpcp.WithName("cd"))
                    .Column(icp => icp.CurveDistanceBucket, icpcp => icpcp.WithName("cd_bkt"))
                    .Column(icp => icp.SoleQuery, icpcp => icpcp.WithName("solr_query")));

            SchemaContainer.AddSchemaMap(
                For<AscendantsByResourceEntry>()
                    .TableName("ascendants_by_resource")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutId, k2 => k2.InId)
                    .ClusteringKey(c => c.CurveDistance)
                    .ClusteringKey(c => c.AscendantId)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.CurveDistance, icpcp => icpcp.WithName("cd"))
                    .Column(icp => icp.AscendantId, icpcp => icpcp.WithName("aid"))
                    .Column(icp => icp.AscendantUri, icpcp => icpcp.WithName("auri"))
                    .Column(icp => icp.Type, icpcp => icpcp.WithName("typ")));

            SchemaContainer.AddSchemaMap(
                For<AscendantsByResourceSolrEntry>()
                    .TableName("ascendants_by_resource")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutId, k2 => k2.InId)
                    .ClusteringKey(c => c.CurveDistance)
                    .ClusteringKey(c => c.AscendantId)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.CurveDistance, icpcp => icpcp.WithName("cd"))
                    .Column(icp => icp.AscendantId, icpcp => icpcp.WithName("aid"))
                    .Column(icp => icp.AscendantUri, icpcp => icpcp.WithName("auri"))
                    .Column(icp => icp.Type, icpcp => icpcp.WithName("typ"))
                    .Column(icp => icp.SolrQuery, icpcp => icpcp.WithName("solr_query")));
        }
    }
}
