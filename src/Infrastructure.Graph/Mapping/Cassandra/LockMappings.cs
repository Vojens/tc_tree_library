﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository;

namespace Infrastructure.Graph.Mapping.Cassandra
{
    internal class LockMappings : SchemaMappings
    {

        public LockMappings(SchemaContainer schemaContainer) : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<LockEntry>()
                    .TableName("lock")
                    .ExplicitColumns()
//                    .PartitionKey(k1 => k1.Keyspace, k2 => k2.Table, k3 => k3.PartitionKeys)
//                    .Column(icp => icp.Keyspace, icpcp => icpcp.WithName("ks"))
//                    .Column(icp => icp.Table, icpcp => icpcp.WithName("tbl"))
//                    .Column(icp => icp.PartitionKeys, icpcp => icpcp.WithName("pk"))
//                    .Column(icp => icp.AquireTimeUuid, icpcp => icpcp.WithName("atud"))
//                    .Column(icp => icp.Operation, icpcp => icpcp.WithName("op").WithDbType<sbyte>())
//                    .Column(icp => icp.Ttl, icpcp => icpcp.WithName("ttl")));
                    .PartitionKey(k1 => k1.Key)
                    .Column(icp => icp.Key, icpcp => icpcp.WithName("key"))
                    .Column(icp => icp.LockId, icpcp => icpcp.WithName("lkid"))
                    .Column(icp => icp.Mode, icpcp => icpcp.WithName("mod")));
        }


    }
}
