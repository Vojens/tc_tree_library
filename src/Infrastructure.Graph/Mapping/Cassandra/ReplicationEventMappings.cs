using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository;

namespace Infrastructure.Graph.Mapping.Cassandra
{
    internal class ReplicationEventMappings : SchemaMappings
    {
        public ReplicationEventMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<ReplicationEventEntry>()
                    .TableName("replication_events")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.RuleId, k2 => k2.Status)
                    .ClusteringKey(c => c.Timestamp)
                    .ClusteringKey(c => c.ResourceType)
                    .ClusteringKey(c => c.ResourceName)
                    .ClusteringKey(c => c.ResourceId)
                    .Column(icp => icp.RuleId, icpcp => icpcp.WithName("ruleid"))
                    .Column(icp => icp.Status, icpcp => icpcp.WithName("status"))
                    .Column(icp => icp.Timestamp, icpcp => icpcp.WithName("dtime"))
                    .Column(icp => icp.ResourceType, icpcp => icpcp.WithName("restype"))
                    .Column(icp => icp.ResourceName, icpcp => icpcp.WithName("resname"))
                    .Column(icp => icp.ResourceId, icpcp => icpcp.WithName("resid"))
                    .Column(icp => icp.Data, icpcp => icpcp.WithName("data")));


            SchemaContainer.AddSchemaMap(
                For<ReplicationEventViewEntry>()
                    .TableName("replication_events_view")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.RuleId, k2 => k2.Status)
                    .ClusteringKey(c => c.ResourceType)
                    .ClusteringKey(c => c.ResourceName)
                    .ClusteringKey(c => c.ResourceId)
                    .ClusteringKey(c => c.Timestamp)
                    .Column(icp => icp.RuleId, icpcp => icpcp.WithName("ruleid"))
                    .Column(icp => icp.Status, icpcp => icpcp.WithName("status"))
                    .Column(icp => icp.Timestamp, icpcp => icpcp.WithName("dtime"))
                    .Column(icp => icp.ResourceType, icpcp => icpcp.WithName("restype"))
                    .Column(icp => icp.ResourceName, icpcp => icpcp.WithName("resname"))
                    .Column(icp => icp.ResourceId, icpcp => icpcp.WithName("resid"))
                    .Column(icp => icp.Data, icpcp => icpcp.WithName("data")));
        }
    }
}