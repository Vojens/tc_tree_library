using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository;

namespace Infrastructure.Graph.Mapping.Cassandra
{
    internal class ResourceDataMappings : SchemaMappings
    {
        public ResourceDataMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<ResourceDataSimpleEntry>()
                    .TableName("resourcedata_simple")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.Id)
                    .Column(icp => icp.Id, icpcp => icpcp.WithName("id"))
                    .Column(icp => icp.Compression, icpcp => icpcp.WithName("cmpr"))
                    .Column(icp => icp.Encoding, icpcp => icpcp.WithName("enc"))
                    .Column(icp => icp.Type, icpcp => icpcp.WithName("typ"))
                    .Column(icp => icp.Data, icpcp => icpcp.WithName("dat"))
                    .Column(icp => icp.ResourceSystem, icpcp => icpcp.WithName("rs"))
                    .Column(icp => icp.MTime, icpcp => icpcp.WithName("mtime")));

            SchemaContainer.AddSchemaMap(
                For<ResourceDataHashEntry>()
                    .TableName("resourcedata_hash")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.Id)
                    .Column(icp => icp.Id, icpcp => icpcp.WithName("id"))
                    .Column(icp => icp.Type, icpcp => icpcp.WithName("typ"))
                    .Column(icp => icp.Hash, icpcp => icpcp.WithName("hash"))
                    .Column(icp => icp.ResourceSystem, icpcp => icpcp.WithName("rs")));

            SchemaContainer.AddSchemaMap(
                For<ResourceDataSizeEntry>()
                    .TableName("resourcedata_size")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.Id)
                    .Column(icp => icp.Id, icpcp => icpcp.WithName("id"))
                    .Column(icp => icp.Size, icpcp => icpcp.WithName("size"))
                );
        }
    }
}