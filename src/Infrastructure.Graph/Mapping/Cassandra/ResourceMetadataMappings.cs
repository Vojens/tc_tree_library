using Infrastructure.Graph.Data.Entity;
using Infrastructure.Graph.Repository;

namespace Infrastructure.Graph.Mapping.Cassandra
{
    internal class ResourceMetadataMappings : SchemaMappings
    {
        public ResourceMetadataMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<ResourceMetadataEntry>()
                    .TableName("resource_metadata")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutId, k2 => k2.OutId)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.Uri, icpcp => icpcp.WithName("uri"))
                    .Column(icp => icp.Name, icpcp => icpcp.WithName("nm"))
                    .Column(icp => icp.Type, icpcp => icpcp.WithName("typ"))
                    .Column(icp => icp.ResourceSystem, icpcp => icpcp.WithName("rs"))
                    .Column(icp => icp.SchemaType, icpcp => icpcp.WithName("scmtyp"))
                    .Column(icp => icp.Schema, icpcp => icpcp.WithName("scm"))
                    .Column(icp => icp.CreationUserId, icpcp => icpcp.WithName("crusrid"))
                    .Column(icp => icp.CreationUserName, icpcp => icpcp.WithName("crusrnm"))
                    .Column(icp => icp.CreationTimestamp, icpcp => icpcp.WithName("crtime"))
                    .Column(icp => icp.CreationTime, icpcp => icpcp.WithName("crtime_t"))
                    .Column(icp => icp.ChangeUserId, icpcp => icpcp.WithName("cusrid"))
                    .Column(icp => icp.ChangeUserName, icpcp => icpcp.WithName("cusrnm"))
                    .Column(icp => icp.ChangeTimestamp, icpcp => icpcp.WithName("ctime"))
                    .Column(icp => icp.ChangeTime, icpcp => icpcp.WithName("ctime_t"))
                    .Column(icp => icp.Version, icpcp => icpcp.WithName("ver"))
                    .Column(icp => icp.Description, icpcp => icpcp.WithName("descr"))
                    .Column(icp => icp.Keywords, icpcp => icpcp.WithName("keywords"))
                    .Column(icp => icp.Workflow, icpcp => icpcp.WithName("wfnm"))
                    .Column(icp => icp.WorkflowState, icpcp => icpcp.WithName("wfstat"))
                    .Column(icp => icp.IsMarkedDeleted, icpcp => icpcp.WithName("isdel"))
                    .Column(icp => icp.IsHidden, icpcp => icpcp.WithName("ishid"))
                    .Column(icp => icp.IsReadOnly, icpcp => icpcp.WithName("isreadonly"))
                    .Column(icp => icp.IsSystem, icpcp => icpcp.WithName("issys"))
                    .Column(icp => icp.ContentType, icpcp => icpcp.WithName("conttyp"))
                    .Column(icp => icp.StorageType, icpcp => icpcp.WithName("strgtyp"))
                    .Column(icp => icp.StorageId, icpcp => icpcp.WithName("strgid"))
                    .Column(icp => icp.StorageUri, icpcp => icpcp.WithName("strguri")));

            SchemaContainer.AddSchemaMap(
                For<ResourceMetadataSolrEntry>()
                    .TableName("resource_metadata")
                    .ExplicitColumns()
                    .PartitionKey(k1 => k1.OutId, k2 => k2.OutId)
                    .Column(icp => icp.OutId, icpcp => icpcp.WithName("outid"))
                    .Column(icp => icp.InId, icpcp => icpcp.WithName("inid"))
                    .Column(icp => icp.Uri, icpcp => icpcp.WithName("uri"))
                    .Column(icp => icp.Name, icpcp => icpcp.WithName("nm"))
                    .Column(icp => icp.Name_Sort, icpcp => icpcp.WithName("nm_sort"))
                    .Column(icp => icp.Type, icpcp => icpcp.WithName("typ"))
                    .Column(icp => icp.SchemaType, icpcp => icpcp.WithName("scmtyp"))
                    .Column(icp => icp.Schema, icpcp => icpcp.WithName("scm"))
                    .Column(icp => icp.CreationUserId, icpcp => icpcp.WithName("crusrid"))
                    .Column(icp => icp.CreationUserName, icpcp => icpcp.WithName("crusrnm"))
                    .Column(icp => icp.CreationTimestamp, icpcp => icpcp.WithName("crtime"))
                    .Column(icp => icp.CreationTime, icpcp => icpcp.WithName("crtime_t"))
                    .Column(icp => icp.ChangeUserId, icpcp => icpcp.WithName("cusrid"))
                    .Column(icp => icp.ChangeUserName, icpcp => icpcp.WithName("cusrnm"))
                    .Column(icp => icp.ChangeTimestamp, icpcp => icpcp.WithName("ctime"))
                    .Column(icp => icp.ChangeTime, icpcp => icpcp.WithName("ctime_t"))
                    .Column(icp => icp.Version, icpcp => icpcp.WithName("ver"))
                    .Column(icp => icp.Description, icpcp => icpcp.WithName("descr"))
                    .Column(icp => icp.Keywords, icpcp => icpcp.WithName("keywords"))
                    .Column(icp => icp.Workflow, icpcp => icpcp.WithName("wfnm"))
                    .Column(icp => icp.WorkflowState, icpcp => icpcp.WithName("wfstat"))
                    .Column(icp => icp.IsMarkedDeleted, icpcp => icpcp.WithName("isdel"))
                    .Column(icp => icp.IsHidden, icpcp => icpcp.WithName("ishid"))
                    .Column(icp => icp.IsReadOnly, icpcp => icpcp.WithName("isreadonly"))
                    .Column(icp => icp.IsSystem, icpcp => icpcp.WithName("issys"))
                    .Column(icp => icp.ContentType, icpcp => icpcp.WithName("conttyp"))
                    .Column(icp => icp.StorageType, icpcp => icpcp.WithName("strgtyp"))
                    .Column(icp => icp.StorageId, icpcp => icpcp.WithName("strgid"))
                    .Column(icp => icp.StorageUri, icpcp => icpcp.WithName("strguri"))
                    .Column(icp => icp.ResourceSystem, icpcp => icpcp.WithName("rs"))
                    .Column(icp => icp.SolrQuery, icpcp => icpcp.WithName("solr_query")));
        }
    }
}