﻿using Infrastructure.Graph.Repository;

namespace Infrastructure.Graph.Mapping
{
    internal abstract class SchemaMappings
    {
        internal SchemaContainer SchemaContainer;

        protected SchemaMappings(SchemaContainer schemaContainer)
        {
            SchemaContainer = schemaContainer;
        }

        public SchemaMap<TProc> For<TProc>()
        {
            var schemaMap = new SchemaMap<TProc>();
            return schemaMap;
        }
    }
}