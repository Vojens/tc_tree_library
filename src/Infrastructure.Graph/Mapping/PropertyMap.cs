using System;
using System.Reflection;
using Cassandra.Mapping;

namespace Infrastructure.Graph.Mapping
{
    internal class PropertyMap : IPropertyDefinition
    {
        private readonly MemberInfo _memberInfo;
        private readonly Type _memberInfoType;
        private string _columnName;
        private Type _columnType;
        private bool _isIgnore;
        private readonly bool _isExplicitlyDefined;
        private bool _isSecondaryIndex;
        private bool _isCounter;
        private bool _isStatic;
        private bool _isDistance;


        public MemberInfo MemberInfo
        {
            get { return _memberInfo; }
        }

        public Type MemberInfoType
        {
            get { return _memberInfoType; }
        }

        public string ColumnName
        {
            get { return _columnName; }
        }

        public Type ColumnType
        {
            get { return _columnType; }
        }

        public bool IsIgnore
        {
            get { return _isIgnore; }
        }

        public bool IsExplicitlyDefined
        {
            get { return _isExplicitlyDefined; }
        }

        public bool IsSecondaryIndex
        {
            get { return _isSecondaryIndex; }
        }

        public bool IsCounter
        {
            get { return _isCounter; }
        }

        public bool IsStatic
        {
            get { return _isStatic; }
        }

        public bool IsDistance
        {
            get { return _isDistance; }
        }

        /// <summary>
        /// Creates a new ColumnMap for the property/field specified by the MemberInfo.
        /// </summary>
        public PropertyMap(MemberInfo memberInfo, Type memberInfoType, bool isExplicitlyDefined)
        {
            if (memberInfo == null) throw new ArgumentNullException("memberInfo");
            if (memberInfoType == null) throw new ArgumentNullException("memberInfoType");
            _memberInfo = memberInfo;
            _memberInfoType = memberInfoType;
            _isExplicitlyDefined = isExplicitlyDefined;
        }

        /// <summary>
        /// Tells the mapper to ignore this property/field when mapping.
        /// </summary>
        public PropertyMap Ignore()
        {
            _isIgnore = true;
            return this;
        }

        /// <summary>
        /// Tells the mapper to use the column name specified when mapping the property/field.
        /// </summary>
        public PropertyMap WithName(string columnName)
        {
            if (string.IsNullOrWhiteSpace(columnName)) throw new ArgumentNullException("columnName");

            _columnName = columnName;
            return this;
        }

        /// <summary>
        /// Tells the mapper to convert the data in the property or field to the Type specified when doing an INSERT or UPDATE (i.e. the
        /// column type in Cassandra).  (NOTE: This does NOT affect the Type when fetching/SELECTing data from the database.)
        /// </summary>
        public PropertyMap WithDbType(Type type)
        {
            if (type == null) throw new ArgumentNullException("type");

            _columnType = type;
            return this;
        }

        /// <summary>
        /// Tells the mapper to convert the data in the property or field to Type T when doing an INSERT or UPDATE (i.e. the
        /// column type in Cassandra).  (NOTE: This does NOT affect the Type when fetching/SELECTing data from the database.)
        /// </summary>
        public PropertyMap WithDbType<T>()
        {
            _columnType = typeof(T);
            return this;
        }

        /// <summary>
        /// Tells the mapper that this column is defined also as a secondary index
        /// </summary>
        /// <returns></returns>
        public PropertyMap WithSecondaryIndex()
        {
            _isSecondaryIndex = true;
            return this;
        }

        /// <summary>
        /// Tells the mapper that this is a counter column
        /// </summary>
        public PropertyMap AsCounter()
        {
            _isCounter = true;
            return this;
        }

        /// <summary>
        /// Tells the mapper that this is a static column
        /// </summary>
        public PropertyMap AsStatic()
        {
            _isStatic = true;
            return this;
        }

        public PropertyMap AsDistance()
        {
            _isDistance = true;
            return this;
        }

        public Action<ColumnMap> ToColumnMap()
        {
            var action = new Action<ColumnMap>(
                x =>
                {
                    if (_isCounter)
                    {
                        x.AsCounter();
                    }
                    if (_isStatic)
                    {
                        x.AsStatic();
                    }
                    if (_isIgnore)
                    {
                        x.Ignore();
                    }
                    if (_isSecondaryIndex)
                    {
                        x.WithSecondaryIndex();
                    }
                    if (!string.IsNullOrWhiteSpace(_columnName))
                    {
                        x.WithName(_columnName);
                    }
                    if (_columnType != null)
                    {
                        x.WithDbType(_columnType);
                    }

                });
            //            var cm = new ColumnMap(_memberInfo, _memberInfoType, _isExplicitlyDefined);
            //            if (_isIgnore) cm.Ignore();
            //            cm.WithName(_columnName);
            //            cm.WithDbType(_columnType);
            //            if (_isSecondaryIndex) cm.WithSecondaryIndex();
            //            if (_isCounter) cm.AsCounter();
            //            if (_isStatic) cm.AsStatic();
            //            return cm;
            return action;
        }
    }
}