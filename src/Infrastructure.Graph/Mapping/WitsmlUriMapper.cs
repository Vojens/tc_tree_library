﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Infrastructure.Graph.Mapping
{
    public class WitsmlUriMapper
    {
        public static string WitsmlRootUri
        {
            get
            {
                return @"./" + UriElements.Witsml;
            }
        }
        public static string MapUriToPath(string uri)
        {
            const string pattern = @"^\.\/" + UriElements.Witsml + @"/(.+)$";
            var rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            var matches = rgx.Matches(uri);
            var parts = matches[0].Groups[1].Value.Split('/');
            var path = new StringBuilder();
            for (var i = 0; i < parts.Length; i++)
            {
                if (parts.Length == 4 && i == parts.Length - 1)
                {
                    continue;
                }
                path.Append("\\" + ExtractUid(parts[i]));
            }
            return path.ToString();
        }

        public static string MapPathToUri(string path)
        {
            var parts = path.Split('\\');
            var uri = new StringBuilder(WitsmlRootUri);
            var elements = new[] { UriElements.Well, UriElements.Wellbore, UriElements.Log };
            for (var i = 0; i < parts.Length; i++)
            {
                uri.Append("/" + elements[i] + "(" + parts[i] + ")");
            }
            return uri.ToString();
        }

        public static class UriElements
        {
            public const string Witsml = "witsml";
            public const string Well = "well";
            public const string Wellbore = "wellbore";
            public const string Log = "log";
            public const string Channel = "channel";
            public const string Trajectory = "trajectory";
            public const string TrajectoryChannel = "trajectorychannel";
            public const string MudLog = "mudlog";
            //public const string MudLogChannel = "mudlogchannel";
            public const string VirtualLog = "virtuallog";
            public const string VirtualLogChannel = "virtuallogchannel";
            public const string VirtualLogSourceChannel = "virtuallogsourcechannel";
            public const string ChangeLog = "changeLog";
            public const string ChangeLogChannel = "changelogchannel";
            public const string WitsmlChangelogs = "witsml_changelogs";

        }

        public static string ExtractUid(string uriPart)
        {
            const string pattern = @"^(.+)\((.+)\)$";
            var rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            var matches = rgx.Matches(uriPart);
            return matches[0].Groups[2].Value;
        }

        public static string WitsmlChangelogUri
        {
            get
            {
                return @"./" + UriElements.WitsmlChangelogs;
            }
        }

        public static string GetResourceUri(string changeLogUri)
        {
            var resourceUri = changeLogUri.Replace(WitsmlChangelogUri, WitsmlRootUri);
            var index = resourceUri.LastIndexOf("/", StringComparison.Ordinal);
            return resourceUri.Substring(0, index);
        }

        public static string GetChangeLogUri(string resourceUri)
        {
            var changeLogUri = resourceUri.Replace(WitsmlRootUri, WitsmlChangelogUri);

            var indexStart = resourceUri.LastIndexOf("(", StringComparison.Ordinal) + 1;
            var indexEnd = resourceUri.LastIndexOf(")", StringComparison.Ordinal);

            var uid = resourceUri.Substring(indexStart, indexEnd - indexStart);

            return changeLogUri + "/changelog(" + uid + ")";
        }

        public static string GetChangeLogChannelUri(string headerUri)
        {
            return headerUri + "/changelogchannel";
        }
    }
}
