using System.Collections.Generic;
using Infrastructure.Common.Compressor;

namespace Infrastructure.Graph.Mapping
{
    internal class CompressionStringType
    {
        public static readonly string None = "";
        public static readonly string Lz4 = "LZ4";
        public static readonly string Snappy = "Snappy";
        public static readonly string GZip = "gzip";

        private static readonly Dictionary<string, CompressionType> StringToTypeDic = new Dictionary<string, CompressionType>();
        private static readonly Dictionary<CompressionType, string> TypeToStringDic = new Dictionary<CompressionType, string>();

        static CompressionStringType()
        {
            StringToTypeDic[None.Trim().ToLower()] = CompressionType.None;
            StringToTypeDic[Lz4.Trim().ToLower()] = CompressionType.Lz4;
            StringToTypeDic[Snappy.Trim().ToLower()] = CompressionType.Snappy;
            StringToTypeDic[GZip.Trim().ToLower()] = CompressionType.Gzip;

            TypeToStringDic[CompressionType.None] = None;
            TypeToStringDic[CompressionType.Lz4] = Lz4;
            TypeToStringDic[CompressionType.Snappy] = Snappy;
            TypeToStringDic[CompressionType.Gzip] = GZip;
        }

        public static string ToCompressionStringType(CompressionType compressionType)
        {
            return TypeToStringDic[compressionType];
        }

        public static CompressionType ToCompressionType(string compressionStringType)
        {
            return StringToTypeDic[compressionStringType.Trim().ToLower()];
        }
    }
}