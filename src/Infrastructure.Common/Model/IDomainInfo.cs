﻿using System.Collections.Generic;

namespace Infrastructure.Common.Model
{
    public interface IDomainInfo
    {
        string RootNode { get; set; }
        string Source { get; set; }
        IDictionary<string, string> Handlers { get; set; }
    }
}