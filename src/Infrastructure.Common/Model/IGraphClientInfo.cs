﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Common.Model
{
    public interface IGraphClientInfo
    {
        Guid UserId { get; }
        string Username { get; }
        List<Guid> GroupIds { get; }
        List<Guid> UserIdentities { get; }
        string Token { get; }
    }
}