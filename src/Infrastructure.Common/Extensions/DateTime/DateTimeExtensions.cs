﻿using System;

namespace Infrastructure.Common.Extensions.DateTime
{
    public static class DateTimeExtensions
    {
        public static DateTimeOffset ToDateTimeOffset(this long ticks)
        {
            DateTimeOffset dateTimeOffset = new System.DateTime(ticks, DateTimeKind.Utc);
            return dateTimeOffset;
        }

//        public static DateTimeOffset ToDateTimeOffset(this byte[] bytes)
//        {
//            return BitConverter.ToInt64(bytes, 0).ToDateTimeOffset();
//        }
//
//        public static DateTimeOffset ToBiDateTimeOffset(this byte[] bytes)
//        {
//            byte[] newBytes = bytes;
//            if (BitConverter.IsLittleEndian)
//                newBytes = bytes.Reverse().ToArray();
//            return BitConverter.ToInt64(newBytes, 0).ToDateTimeOffset();
//        }
        
    }
}
