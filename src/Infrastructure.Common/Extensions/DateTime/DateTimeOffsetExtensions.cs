﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Cassandra;

namespace Infrastructure.Common.Extensions.DateTime
{
    public static class DateTimeOffsetExtension
    {

        private const int TicksPerMicrosecond = 10;
        private const int NanosecondsPerTick = 100;

        private static readonly DateTimeOffset UtcEpoch = new DateTimeOffset(1970, 1, 1, 0, 0, 0, 0, TimeSpan.Zero);

        private static readonly Random _random = new Random();

        public static DateTimeOffset Random(this DateTimeOffset value, TimeSpan timeSpan)
        {
            var seconds = timeSpan.TotalSeconds * StaticRandom.Instance.NextDouble();

            // Alternatively: return value.AddSeconds(-seconds);
            var span = TimeSpan.FromSeconds(seconds);
            return value.Subtract(span);
        }

        public static DateTimeOffset Random(this DateTimeOffset value)
        {
            var timeSpan = new TimeSpan(0, _random.Next(0, 23), _random.Next(0, 59), _random.Next(0, 59),
                _random.Next(0, 999));
            var seconds = timeSpan.TotalSeconds * StaticRandom.Instance.NextDouble();

            // Alternatively: return value.AddSeconds(-seconds);
            var span = TimeSpan.FromSeconds(seconds);
            return value.Subtract(span);
        }

        

        public static DateTimeOffset Truncate(this DateTimeOffset dateTime, TimeSpan timeSpan)
        {
            return timeSpan == TimeSpan.Zero ? dateTime : dateTime.AddTicks(-(dateTime.Ticks % timeSpan.Ticks));
        }

        public static bool WithinExeclusively(this DateTimeOffset value, DateTimeOffset left, DateTimeOffset right)
        {
            return value.Truncate(TimeSpan.FromMilliseconds(1)) > left.Truncate(TimeSpan.FromMilliseconds(1)) && value.Truncate(TimeSpan.FromMilliseconds(1)) < right.Truncate(TimeSpan.FromMilliseconds(1));
        }

        public static bool WithinInclusively(this DateTimeOffset value, DateTimeOffset left, DateTimeOffset right)
        {
            return value.Truncate(TimeSpan.FromMilliseconds(1)) >= left.Truncate(TimeSpan.FromMilliseconds(1)) && value.Truncate(TimeSpan.FromMilliseconds(1)) <= right.Truncate(TimeSpan.FromMilliseconds(1));
        }

        public static DateTimeOffset Max(this DateTimeOffset first, DateTimeOffset second)
        {
            return (first > second ? first : second);
        }

        public static DateTimeOffset Min(this DateTimeOffset first, DateTimeOffset second)
        {
            return (first < second ? first : second);
        }

        public static LocalDate ToLocalCassandraDate(this DateTimeOffset dateTimeOffset)
        {
            return new LocalDate(dateTimeOffset.Year, dateTimeOffset.Month, dateTimeOffset.Day);
        }

        public static LocalTime ToLocalCassandraTime(this DateTimeOffset dateTimeOffset)
        {
            var midnight = new DateTimeOffset(dateTimeOffset.Year, dateTimeOffset.Month, dateTimeOffset.Day, 0, 0, 0, 0, dateTimeOffset.Offset);
            var today = dateTimeOffset - midnight;
            var todaysNanoSeconds = today.Nanoseconds();
            return new LocalTime(todaysNanoSeconds);
        }

        public static int Nanoseconds(this DateTimeOffset self)
        {
            return (int)(self.Ticks % TimeSpan.TicksPerMillisecond % TicksPerMicrosecond)
               * NanosecondsPerTick;
        }

        public static int Nanoseconds(this TimeSpan self)
        {
            return (int)(self.Ticks % TimeSpan.TicksPerMillisecond % TicksPerMicrosecond)
               * NanosecondsPerTick;
        }
    }
    internal static class StaticRandom
    {
        private static int _seed;

        private static readonly ThreadLocal<Random> ThreadLocal = new ThreadLocal<Random>
            (() => new Random(Interlocked.Increment(ref _seed)));

        static StaticRandom()
        {
            _seed = Environment.TickCount;
        }

        public static Random Instance { get { return ThreadLocal.Value; } }
    }
}
