﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Common.Extensions
{
    public static class ObservableExtensions
    {
        public static Task<IList<T>> AsAsyncList<T>(this IObservable<T> observable)
        {
            var taskCompletion = new TaskCompletionSource<IList<T>>();

            var list = new List<T>();

            observable.Subscribe(list.Add,
                taskCompletion.SetException,
                () => // on complete
                    taskCompletion.SetResult(list));

            return taskCompletion.Task;
        }

        public static IObservable<IList<T>> BufferUntil<T>(this IObservable<T> observable, Func<T, bool> predicate)
        {
            var singleSource = observable.Publish().RefCount();
            var trigger = singleSource.Where(predicate);
            return singleSource.Buffer(trigger);
        }
    }
}
