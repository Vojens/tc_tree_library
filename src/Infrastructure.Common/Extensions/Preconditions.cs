﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Common.Extensions
{
    public static class Preconditions
    {
        public static void CheckArgument(bool expression)
        {
            if (!expression)
            {
                throw new ArgumentException();
            }
        }

        public static void CheckArgument(bool expression, object errorMessage)
        {
            if (!expression)
            {
                throw new ArgumentException(errorMessage.ToString());
            }
        }

        public static void CheckArgument(bool expression,
            string errorMessageTemplate = null,
            params object[] errorMessageArgs)
        {
            if (!expression)
            {
                throw new ArgumentException(string.Format(errorMessageTemplate, errorMessageArgs));
            }
        }

        public static void CheckState(bool expression, object errorMessage = null)
        {
            if (!expression)
            {
                throw new InvalidOperationException(errorMessage != null ? errorMessage.ToString() : null);
            }
        }

        public static void CheckState(bool expression,
            string errorMessageTemplate,
            params object[] errorMessageArgs)
        {
            if (!expression)
            {
                throw new InvalidOperationException(string.Format(errorMessageTemplate, errorMessageArgs));
            }
        }

        public static T CheckNotNull<T>(T reference)
        {
            if (reference == null)
            {
                throw new NullReferenceException();
            }
            return reference;
        }

        public static T CheckNotNull<T>(T reference, object errorMessage)
        {
            if (reference == null)
            {
                throw new NullReferenceException(Convert.ToString(errorMessage));
            }
            return reference;
        }

        public static T CheckNotNull<T>(T reference,
            string errorMessageTemplate,
            params object[] errorMessageArgs)
        {
            if (reference == null)
            {
                // If either of these parameters is null, the right thing happens anyway
                throw new NullReferenceException(string.Format(errorMessageTemplate, errorMessageArgs));
            }
            return reference;
        }

        public static T CheckNeitherNullNorDefault<T>(T reference,
            string errorMessageTemplate,
            params object[] errorMessageArgs)
        {
            if (EqualityComparer<T>.Default.Equals(reference, default(T)))
            {
                // If either of these parameters is null, the right thing happens anyway
                throw new NullReferenceException(string.Format(errorMessageTemplate, errorMessageArgs));
            }
            return reference;
        }

        public static T CheckNeitherNullNorDefault<T>(T reference)
        {
            if (EqualityComparer<T>.Default.Equals(reference, default(T)))
            {
                // If either of these parameters is null, the right thing happens anyway
                throw new ArgumentNullException(string.Format("Object passed of type {0} cannot be null or default", typeof(T)));
            }
            return reference;
        }

        public static bool IsNull<T>(this T reference)
        {
            return reference == null;
        }

        public static bool IsNullOrDefault<T>(this T reference)
        {
            return EqualityComparer<T>.Default.Equals(reference, default(T));
        }

        public static int CheckElementIndex(int index, int size)
        {
            return CheckElementIndex(index, size, "index");
        }

        public static int CheckElementIndex(
            int index, int size, string desc)
        {
            // Carefully optimized for execution by hotspot (explanatory comment above)
            if (index < 0 || index >= size)
            {
                throw new IndexOutOfRangeException(BadElementIndex(index, size, desc));
            }
            return index;
        }

        private static string BadElementIndex(int index, int size, string desc)
        {
            if (index < 0)
            {
                return string.Format("{0} ({1}) must not be negative", desc, index);
            }
            if (size < 0)
            {
                throw new ArgumentException("negative size: " + size);
            } // index >= size
            return string.Format("{0} ({1}) must be less than size ({2})", desc, index, size);
        }

        public static int CheckPositionIndex(int index, int size)
        {
            return CheckPositionIndex(index, size, "index");
        }

        public static int CheckPositionIndex(int index, int size, string desc)
        {
            // Carefully optimized for execution by hotspot (explanatory comment above)
            if (index < 0 || index > size)
            {
                throw new IndexOutOfRangeException(BadPositionIndex(index, size, desc));
            }
            return index;
        }

        private static string BadPositionIndex(int index, int size, string desc)
        {
            if (index < 0)
            {
                return string.Format("{0} ({1}) must not be negative", desc, index);
            }
            if (size < 0)
            {
                throw new ArgumentException("negative size: " + size);
            } // index > size
            return string.Format("{0} ({1}) must not be greater than size ({2})", desc, index, size);
        }

        public static void CheckPositionIndexes(int start, int end, int size)
        {
            // Carefully optimized for execution by hotspot (explanatory comment above)
            if (start < 0 || end < start || end > size)
            {
                throw new IndexOutOfRangeException(BadPositionIndexes(start, end, size));
            }
        }

        private static string BadPositionIndexes(int start, int end, int size)
        {
            if (start < 0 || start > size)
            {
                return BadPositionIndex(start, size, "start index");
            }
            if (end < 0 || end > size)
            {
                return BadPositionIndex(end, size, "end index");
            }
            // end < start
            return string.Format("end index ({0}) must not be less than start index ({1})", end, start);
        }

        public static T CheckIfOfType<T>(object obj)
        {
            if (obj is T)
            {
                return (T)obj;
            }
            throw new ArgumentException(string.Format("Object passed is not of type {0}", typeof(T)));
        }

        public static void CheckNeitherNullNorEmpty<T>(IList<T> collection)
        {
            if (collection == null || collection.Count == 0)
            {
                throw new ArgumentException("collection is null or empty");
            }
        }

        public static void CheckNotEmpty<T>(IList<T> list, string message)
        {
            if (list.Count == 0)
            {
                throw new ArgumentException(message);
            }
        }

        public static void EnsureEmpty<T>(IList<T> list, string message)
        {
            if (list.Count != 0)
            {
                throw new ArgumentException(message);
            }
        }

        public static void EnsureNotEquals<T1,T2>(T1 t1, T2 t2)
        {
            if (t1.Equals(t2))
            {
                throw new ArgumentException("values should not be equal");
            }
        }
    }
}
