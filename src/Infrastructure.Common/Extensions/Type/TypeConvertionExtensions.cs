﻿using Microsoft.Hadoop.Avro;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Infrastructure.Common.Extensions.Type
{
    public static class TypeConvertionExtensions
    {
         //Convert an object to a byte array
        public static byte[] ToByteArray(this object obj)
        {
            if (obj == null)
                return null;
            if (obj is string)
            {
                return Encoding.UTF8.GetBytes((string) obj);
            }
            var bf = new BinaryFormatter();
            var ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        //public static object ByteToString(this byte[] obj)
        //{
        //    if (obj == null)
        //        return null;
        //    var bf = new BinaryFormatter();
        //    var ms = new MemoryStream(obj);
        //    return bf.Deserialize(ms);
        //    //return ms;
        //}

        public static Stream ToStream(this object obj)
        {
            if (obj == null)
                return null;
            var bf = new BinaryFormatter();
            var ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms;
        }

        public static T Deserialize<T>(this byte[] data)
        {
            using (MemoryStream ms = new MemoryStream(data))
            {
                IFormatter br = new BinaryFormatter();
                return (T)br.Deserialize(ms);
            }
        }
    }
}
