﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Common.Extensions.Asynchronous
{
    public static class TaskExtensions
    {
        public static Task<Task<T>>[] Interleaved<T>(this IEnumerable<Task<T>> tasks)
        {
            var inputTasks = tasks.ToList();

            var buckets = new TaskCompletionSource<Task<T>>[inputTasks.Count];
            var results = new Task<Task<T>>[buckets.Length];
            for (var i = 0; i < buckets.Length; i++)
            {
                buckets[i] = new TaskCompletionSource<Task<T>>();
                results[i] = buckets[i].Task;
            }

            var nextTaskIndex = -1;
            Action<Task<T>> continuation = completed =>
            {
                var bucket = buckets[Interlocked.Increment(ref nextTaskIndex)];
                bucket.TrySetResult(completed);
            };

            foreach (var inputTask in inputTasks)
                inputTask.ContinueWith(continuation, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);

            return results;
        }

        public static Task<Task>[] Interleaved(this IEnumerable<Task> tasks)
        {
            var inputTasks = tasks.ToList();

            var buckets = new TaskCompletionSource<Task>[inputTasks.Count];
            var results = new Task<Task>[buckets.Length];
            for (var i = 0; i < buckets.Length; i++)
            {
                buckets[i] = new TaskCompletionSource<Task>();
                results[i] = buckets[i].Task;
            }

            var nextTaskIndex = -1;
            Action<Task> continuation = completed =>
            {
                var bucket = buckets[Interlocked.Increment(ref nextTaskIndex)];
                bucket.TrySetResult(completed);
            };

            foreach (var inputTask in inputTasks)
                inputTask.ContinueWith(continuation, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);

            return results;
        }

        public static async Task Await<T>(IEnumerable<Task<T>> tasks)
        {
            foreach (var task in tasks.Interleaved())
            {
                await (await task.ConfigureAwait(false)).ConfigureAwait(false);
            }
        }

        public static IObservable<T> ToAwaitObservable<T>(this IEnumerable<Task<T>> tasks)
        {
            return Observable.Create<T>(async o =>
            {
                try
                {
                    foreach (var task in tasks.Interleaved())
                    {
                        o.OnNext(await (await task.ConfigureAwait(false)).ConfigureAwait(false));
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
            });
            
        }

        public static async Task Await(this IEnumerable<Task> tasks)
        {
            foreach (var task in tasks.Interleaved())
            {
                await (await task.ConfigureAwait(false)).ConfigureAwait(false);
            }
        }

        public static Task<List<T>> ToAsyncList<T>(this IObservable<T> observable)
        {
            var taskCompletion = new TaskCompletionSource<List<T>>();

            var list = new List<T>();

            observable.Subscribe(list.Add,
                taskCompletion.SetException,
                () => // on complete
                    taskCompletion.SetResult(list));

            return taskCompletion.Task;
        }
    }
}
