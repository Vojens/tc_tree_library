﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Common.Compressor;
using Infrastructure.Common.Extensions.DateTime;
using Microsoft.Hadoop.Avro;

namespace Infrastructure.Common.Extensions
{
    public static class ByteArrayExtensions
    {
        private static readonly ICompressor GzipCompressor = new GzipCompressor();
        private static readonly ICompressor Lz4Compressor = new LZ4Compressor();

        public static T ConvertTo<T>(this byte[] bytes)
        {
            var type = typeof(T);
            if (type == typeof(string))
            {
                return (T) (object) Encoding.UTF8.GetString(bytes);
            }

            if (BitConverter.IsLittleEndian)
                bytes = bytes.Reverse().ToArray();

            if (type == typeof(bool))
            {
                return (T)(object)BitConverter.ToBoolean(bytes, 0);
            }
            if (type == typeof(char))
            {
                return (T)(object)BitConverter.ToChar(bytes, 0);
            }
            if (type == typeof(double))
            {
                return (T)(object)BitConverter.ToDouble(bytes, 0);
            }
            if (type == typeof(short))
            {
                return (T)(object)BitConverter.ToInt16(bytes, 0);
            }
            if (type == typeof(int))
            {
                return (T)(object)BitConverter.ToInt32(bytes, 0);
            }
            if (type == typeof(long))
            {
                return (T)(object)BitConverter.ToInt64(bytes, 0);
            }
            if (type == typeof(ushort))
            {
                return (T)(object)BitConverter.ToUInt16(bytes, 0);
            }
            if (type == typeof(uint))
            {
                return (T)(object)BitConverter.ToUInt32(bytes, 0);
            }
            if (type == typeof(ulong))
            {
                return (T)(object)BitConverter.ToUInt64(bytes, 0);
            }
            if (type == typeof(DateTimeOffset))
            {
                return (T)(object)BitConverter.ToInt64(bytes, 0).ToDateTimeOffset();
            }
            throw new NotSupportedException();
        }

        public static byte[] Compress(this byte[] bytes, CompressionType compressionType)
        {
            switch (compressionType)
            {
                case CompressionType.Lz4:
                    return Lz4Compressor.Compress(bytes);
                case CompressionType.Gzip:
                    return GzipCompressor.Compress(bytes);
                default:
                    throw new NotSupportedException();
            }
        }

        public static Task<byte[]> CompressAsync(this byte[] bytes, CompressionType compressionType)
        {
            switch (compressionType)
            {
                case CompressionType.Lz4:
                    return Lz4Compressor.CompressAsync(bytes);
                case CompressionType.Gzip:
                    return GzipCompressor.CompressAsync(bytes);
                default:
                    throw new NotSupportedException();
            }
        }

        public static byte[] Decompress(this byte[] bytes, CompressionType compressionType)
        {
            switch (compressionType)
            {
                case CompressionType.Lz4:
                    return Lz4Compressor.Decompress(bytes);
                case CompressionType.Gzip:
                    return GzipCompressor.Decompress(bytes);
                default:
                    throw new NotSupportedException();
            }
        }

        public static Task<byte[]> DecompressAsync(this byte[] bytes, CompressionType compressionType)
        {
            switch (compressionType)
            {
                case CompressionType.Lz4:
                    return Lz4Compressor.DecompressAsync(bytes);
                case CompressionType.Gzip:
                    return GzipCompressor.DecompressAsync(bytes);
                default:
                    throw new NotSupportedException();
            }
        }


    }
}
