﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.Common.Extensions.Asynchronous;
using Infrastructure.Common.Util;

namespace Infrastructure.Common.Extensions.PubSub
{
    internal class Hub
    {
        internal class Handler
        {
            public Delegate Action { get; set; }
            public WeakReference Sender { get; set; }
            public System.Type Type { get; set; }
        }

        internal object Locker = new object();
        internal List<Handler> Handlers = new List<Handler>();

        internal ConcurrentDictionary<string, List<Handler>> Dic = new ConcurrentDictionary<string, List<Handler>>();


        public Task Publish<T>(object sender, T data = default( T ))
        {
            var handlerList = new List<Handler>(Handlers.Count);
            var handlersToRemoveList = new List<Handler>(Handlers.Count);

            foreach (var handler in Handlers)
            {
                if (!handler.Sender.IsAlive)
                {
                    handlersToRemoveList.Add(handler);
                }
                else if (handler.Type.IsAssignableFrom(typeof(T)))
                {
                    handlerList.Add(handler);
                }
            }

            foreach (var l in handlersToRemoveList)
            {
                Handlers.Remove(l);
            }

            var tasks = handlerList.Select(l => (Func<T, Task>)l.Action).Select(action => action(data)).ToList();
            return tasks.Await();
        }

        public void Subscribe<T>(object sender, Func<T, Task> handler)
        {
            var item = new Handler
            {
                Action = handler,
                Sender = new WeakReference(sender),
                Type = typeof(T)
            };

            lock (Locker)
            {
                Handlers.Add(item);
            }
        }

        public Task Publish<T>(object sender, string path, T data = default( T ), EventArgs eventArgs = null)
        {
            List<Task> tasks = null;
            List<Handler> handlerList;
            if (Dic.TryGetValue(path, out handlerList))
            {
                tasks = handlerList.Where(l => l.Type == typeof(T)).Select(l => (Func<T, EventArgs, Task>)l.Action).Select(action => action(data, eventArgs)).ToList();

            }
            return tasks != null ? tasks.Await() : TaskHelper.Completed;
        }

        public void Subscribe<T>(object sender, string path, Func<T, Task> handler)
        {
            var item = new Handler
            {
                Action = handler,
                Sender = new WeakReference(sender),
                Type = typeof(T)
            };

            lock (Locker)
            {
                List<Handler> handlerList;
                if (Dic.TryGetValue(path, out handlerList))
                {
                    handlerList.Add(item);
                }
                else
                {
                    handlerList = new List<Handler> { item };
                    Dic.TryAdd(path, handlerList);
                }
            }
        }

        public void Subscribe<T>(object sender, string path, Func<T, EventArgs, Task> handler)
        {
            var item = new Handler
            {
                Action = handler,
                Sender = new WeakReference(sender),
                Type = typeof(T)
            };

            lock (Locker)
            {
                List<Handler> handlerList;
                if (Dic.TryGetValue(path, out handlerList))
                {
                    handlerList.Add(item);
                }
                else
                {
                    handlerList = new List<Handler> { item };
                    Dic.TryAdd(path, handlerList);
                }
            }
        }

        public void Unsubscribe(object sender)
        {
            lock (Locker)
            {
                var query = Handlers.Where(a => !a.Sender.IsAlive ||
                                                       a.Sender.Target.Equals(sender));

                foreach (var h in query.ToList())
                {
                    Handlers.Remove(h);
                }
            }
        }

        public void Unsubscribe<T>(object sender, Func<T, Task> handler = null)
        {
            lock (Locker)
            {
                var query = Handlers
                    .Where(a => !a.Sender.IsAlive ||
                                 (a.Sender.Target.Equals(sender) && a.Type == typeof(T)));

                if (handler != null)
                {
                    query = query.Where(a => a.Action.Equals(handler));
                }

                foreach (var h in query.ToList())
                {
                    Handlers.Remove(h);
                }
            }
        }
    }
}
