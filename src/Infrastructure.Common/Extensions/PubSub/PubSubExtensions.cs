using System;
using System.Threading.Tasks;

namespace Infrastructure.Common.Extensions.PubSub
{
    public static class PubSubExtensions
    {
        private static readonly Hub Hub = new Hub();

        public static bool Exists<T>(this object obj)
        {
            foreach (var h in Hub.Handlers)
            {
                if (h.Sender.Target.ToString() == obj.ToString() &&
                    typeof(T) == h.Type)
                {
                    return true;
                }
            }
            return false;
        }

        public static Task Publish<T>(this object obj)
        {
            return Hub.Publish(obj, default(T));
        }

        public static Task Publish<T>(this object obj, T data)
        {
            return Hub.Publish(obj, data);
        }

        public static Task Publish<T>(this object obj, string path, T data = default( T ), EventArgs eventArgs = null)
        {
            return Hub.Publish<T>(obj, path, data, eventArgs);
        }

        public static void Subscribe<T>(this object obj, Func<T, Task> handler)
        {
            Hub.Subscribe(obj, handler);
        }

        public static void Subscribe<T>(this object obj, string path, Func<T, EventArgs, Task> handler)
        {
            Hub.Subscribe(obj, path, handler);
        }

        public static void Unsubscribe(this object obj)
        {
            Hub.Unsubscribe(obj);
        }

        public static void Unsubscribe<T>(this object obj)
        {
            Hub.Unsubscribe(obj, (Func<T, Task>)null);
        }

        public static void Unsubscribe<T>(this object obj, Func<T, Task> handler)
        {
            Hub.Unsubscribe(obj, handler);
        }
    }
}