﻿using System.Data.HashFunction;
using System.IO;
using System.Threading.Tasks;
using Infrastructure.Common.Extensions.Type;

namespace Infrastructure.Common.Extensions.Hash
{
    public static class HashExtensions
    {
        //        public async static Task<byte[]> GetHashAsync(this object obj, HashAlgorithmType hashAlgorithmType, int hashSize = 128)
        //        {
        //            switch (hashAlgorithmType)
        //            {
        //                case HashAlgorithmType.CityHash:
        //                    CityHash city = new CityHash(hashSize);
        //                    return city.ComputeHash(obj.ToStream());
        //                default:
        //                    return null;
        //            }
        //        }

        public static byte[] GetHash(this object obj, HashAlgorithmType hashAlgorithmType, int hashSize = 128)
        {
            return GetHash(obj.ToByteArray(), hashAlgorithmType, hashSize);
        }

        public static byte[] GetHash(this byte[] bytes, HashAlgorithmType hashAlgorithmType, int hashSize = 128)
        {
            switch (hashAlgorithmType)
            {
                case HashAlgorithmType.CityHash:
                    var city = new CityHash(hashSize);
                    var ms = new MemoryStream(bytes);
                    var result = city.ComputeHash(ms);
                    ms.Close();
                    return result;
                default:
                    return null;
            }
        }

        public static async Task<byte[]> GetHashAsync(this byte[] bytes, HashAlgorithmType hashAlgorithmType, int hashSize = 128)
        {
            switch (hashAlgorithmType)
            {
                case HashAlgorithmType.CityHash:
                    var city = new CityHash(hashSize);
                    var ms = new MemoryStream(bytes);
                    var result = await city.ComputeHashAsync(ms).ConfigureAwait(false);
                    ms.Close();
                    return result;
                default:
                    return null;
            }
        }
    }
    public enum HashAlgorithmType { CityHash = 1 }
}
