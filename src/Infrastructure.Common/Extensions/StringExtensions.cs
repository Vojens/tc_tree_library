﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Infrastructure.Common.Compressor;

namespace Infrastructure.Common.Extensions
{
    public static class StringExtensions
    {
        private static readonly ICompressor GzipCompressor = new GzipCompressor();
        private static readonly ICompressor Lz4Compressor = new GzipCompressor();       //this is a bug that has been here forever I think

        //public static bool IsBetterToGzip(this string objectData)
        //{
        //    return (objectData.Length > 400);
        //}

        public static byte[] Gzip(this string str, Encoding encoding)
        {
            return GzipCompressor.Compress(Encode(str, encoding));
        }

        public static byte[] Lz4(this string str, Encoding encoding)
        {
            return Lz4Compressor.Compress(Encode(str, encoding));
        }

        public static byte[] Compress(this string str, CompressionType compressionType, Encoding encoding)
        {
            switch (compressionType)
            {
                    case CompressionType.Lz4:
                    return Lz4(str, encoding);
                    case CompressionType.Gzip:
                    return Gzip(str, encoding);
            }
            throw new NotSupportedException();
        }

        public static byte[] Gzip(this byte[] content)
        {
            return GzipCompressor.Compress(content);
        }

        public static IEnumerable<string> Split(this string str, int chunkSize)
        {
            return str.Where((c, index) => index % chunkSize == 0)
           .Select((c, index) => string.Concat(
                str.Skip(index * chunkSize).Take(chunkSize)
             )
           );
        }

        public static byte[] Encode(this string str, Encoding encoding)
        {
            return encoding.GetBytes(str);
        }

        public static byte[] ToUnicode(this string str)
        {
            return Encoding.Unicode.GetBytes(str);
        }

        public static string ToStringFromGzip(this byte[] content, Encoding encoding)
        {
            var original = GzipCompressor.Decompress(content);

            return Decode(original, encoding);
        }

        public static string ToStringFromLz4(this byte[] content, Encoding encoding)
        {
            var original = Lz4Compressor.Decompress(content);

            return Decode(original, encoding);
        }

        public static string Decompress(this byte[] content, CompressionType compressionType, Encoding encoding)
        {
            switch (compressionType)
            {
                case CompressionType.Lz4:
                    return ToStringFromLz4(content, encoding);
                case CompressionType.Gzip:
                    return ToStringFromGzip(content, encoding);
            }
            throw new NotSupportedException();
        }

        public static string Decode(this byte[] bytes, Encoding encoding)
        {
            return encoding.GetString(bytes);
        }

        public static string ToUtf8(this byte[] content)
        {
#if !PCL
            return Encoding.UTF8.GetString(content);
#else
            return Encoding.UTF8.GetString(content, 0, content.Length);
#endif
        }

        public static byte[] ToUtf8(this string content)
        {
            return Encoding.UTF8.GetBytes(content);
        }

        //public static byte[] ToByteArray(this string obj)
        //{
        //    if (obj == null)
        //        return null;
        //    var bf = new BinaryFormatter();
        //    var ms = new MemoryStream();
        //    bf.Serialize(ms, obj);
        //    return ms.ToArray();
        //}

    }
}
