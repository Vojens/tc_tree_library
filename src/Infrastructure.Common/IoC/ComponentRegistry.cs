﻿using System;
using System.Collections.Generic;
using LightInject;

namespace Infrastructure.Common.IoC
{
    public static class ComponentRegistry
    {
        private static readonly LightInject.ServiceContainer _container = new LightInject.ServiceContainer();

        public static void Register<T>()
        {
            _container.Register<T>();
        }

        public static void RegisterFrom<T>() where T : ICompositionRoot, new()
        {
            _container.RegisterFrom<T>();
        }

        public static ServiceContainer Container { get { return _container; } }

        public static T Resolve<T>()
        {
            return _container.GetInstance<T>();
        }

        public static T Resolve<T>(string name)
        {
            return _container.GetInstance<T>(name);
        }

        public static T Resolve<T, TArg>(TArg arg) where T : class
        {
            return (T)_container.GetInstance(typeof(T), new object[1] { arg });
        }

        public static T Resolve<T, TArg1, TArg2>(TArg1 arg1, TArg2 arg2) where T : class
        {
            return (T)_container.GetInstance(typeof(T), new object[] { arg1, arg2 });
        }

        public static T Resolve<T, TArg1, TArg2,TArg3>(TArg1 arg1, TArg2 arg2,TArg3 arg3) where T : class
        {
            return (T)_container.GetInstance(typeof(T), new object[] { arg1, arg2, arg3 });
        }

        public static void RegisterConstructorDependency<TDependency>()
        {
            Container.RegisterConstructorDependency((factory, info, args) =>
            {
                foreach (var argument in args)
                {
                    if (argument is TDependency)
                    {
                        return (TDependency)argument;
                    }
                }
                return factory.GetInstance<TDependency>();
            });
        }
    }
}
