﻿namespace Infrastructure.Common.IoC
{
    public interface IExplicitArgs
    {
        T Resolve<T>();
        T Resolve<T>(string name);
        IExplicitArgs With<T>(T arg);
    }
}
