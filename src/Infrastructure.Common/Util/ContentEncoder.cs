﻿using System;
using System.Text;
using Infrastructure.Common.Compressor;

namespace Infrastructure.Common.Util
{
    /// <summary>
    /// Handles standard encoding and compression for resource content which would be either text or raw bytes.
    /// </summary>
    /// <remarks>
    /// UTF-8 is used for text.
    /// 
    /// The encoding parameter behaves as it does in HTML, specifying a compression method.
    /// 
    /// LZ4 is used when compression is needed, however decompression supports both LZ4 and GZIP.
    /// </remarks>
    public static class ContentEncoder
    {
        /// <summary>
        /// Data with a length exceeding this number is compressed.
        /// </summary>
        public static readonly int MaxUncompressedLength = 400;

        private static readonly GzipCompressor GzipCompressor = new GzipCompressor();
        private static readonly LZ4Compressor Lz4Compressor = new LZ4Compressor();

        /// <summary>
        /// Encode a string and compress it if its length exceeds MaxUncompressedLength.
        /// </summary>
        /// <param name="text">The text to be encoded.</param>
        /// <param name="encoding">The compression encoding that is selected. Will be an empty string if no encoding.</param>
        /// <returns>The encoded and potentially compressed string</returns>
        public static byte[] Encode(string text, out string encoding)
        {
            return Encode(Encoding.UTF8.GetBytes(text), out encoding);
        }

        /// <summary>
        /// Encode a byte array and compress it if its length exceeds MaxUncompressedLength.
        /// </summary>
        /// <param name="bytes">The byte array to be encoded.</param>
        /// <param name="encoding">The compression encoding that is selected. Will be an empty string if no encoding.</param>
        /// <returns>The encoded and potentially compressed byte array</returns>
        public static byte[] Encode(byte[] bytes, out string encoding)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");
            
            if (bytes.Length > MaxUncompressedLength)
            {
                // LZ4 is preferred for speed. Compressed size is less of a concern.
                encoding = "lz4";
                return Lz4Compressor.Compress(bytes);
            }
            else
            {
                encoding = "";
                return bytes;
            }
        }

        /// <summary>
        /// Decode a string with an optional compression encoding.
        /// </summary>
        /// <param name="data">The text to be decoded</param>
        /// <param name="encoding">The compression encoding that was selected, or an empty string if none.</param>
        /// <returns>The decoded string</returns>
        public static string DecodeString(byte[] data, string encoding)
        {
            return Encoding.UTF8.GetString(DecodeBytes(data, encoding));
        }

        /// <summary>
        /// Decode a byte array with an optional compression encoding.
        /// </summary>
        /// <param name="data">The byte array to be decoded</param>
        /// <param name="encoding">The compression encoding that was selected, or an empty string if none.</param>
        /// <returns>The decoded byte array</returns>
        public static byte[] DecodeBytes(byte[] data, string encoding)
        {
            if (data == null)
                throw new ArgumentNullException("data");
            if (encoding == null)
                throw new ArgumentNullException("encoding");

            if (encoding.Length == 0)
                return data;

            if (string.Equals(encoding, "lz4", StringComparison.OrdinalIgnoreCase))
                return Lz4Compressor.Decompress(data);

            if (string.Equals(encoding, "gzip", StringComparison.OrdinalIgnoreCase))
                return GzipCompressor.Decompress(data);

            throw new ArgumentException("unknown encoding: " + encoding, "encoding");
        }
    }
}
