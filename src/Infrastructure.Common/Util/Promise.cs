﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace Infrastructure.Common.Util
{
    public class Promise<T> : IDisposable
    {
        private readonly Subject<T> _subject;
        public Promise()
        {
            _subject = new Subject<T>();
        }

        public void Supply(T value)
        {
            _subject.OnNext(value);
        }

        public void Done()
        {
            _subject.OnCompleted();
        }

        public T First()
        {
            return _subject.ToEnumerable().First();
        }

        public IObservable<T> All()
        {
            return _subject;
        }

        public void Dispose()
        {
            _subject.Dispose();
        }
    }
}
