namespace Infrastructure.Common.Solr
{
    public interface ISolrOperationsCache
    {
        /// <summary>
        /// Gets the solr url.
        /// </summary>
        string SolrUrl { get; }

        /// <summary>
        /// Gets the number of documents return after start in query result.
        ///  eg: page size.
        /// </summary>
        int MaxRowsResult { get; }
    }
}