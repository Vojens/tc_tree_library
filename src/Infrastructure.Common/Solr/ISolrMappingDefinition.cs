﻿using System;
using System.Linq.Expressions;

namespace Infrastructure.Common.Solr
{
    public interface ISolrMappingDefinition
    {
        string GetCore<T>();

        string GetKeySpace<T>();

        string GetColumnName<T>(Expression<Func<T, object>> column);

        void MapKeySpace<T>(string keySpace);
    }
}