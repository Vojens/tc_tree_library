using System;
using System.Reflection;

namespace Infrastructure.Common.Solr
{
    public class SolrColumnMap : ISolrColumnMap
    {
        private string _columnName;
        private readonly MemberInfo _memberInfo;
        private readonly Type _memberInfoType;
        private readonly bool _isExplicitlyDefined;

        public string ColumnName
        {
            get { return _columnName; }
        }

        public MemberInfo MemberInfo
        {
            get { return _memberInfo; }
        }

        public SolrColumnMap(MemberInfo memberInfo, Type memberInfoType, bool isExplicitlyDefined)
        {
            if (memberInfo == null) throw new ArgumentNullException("memberInfo");
            if (memberInfoType == null) throw new ArgumentNullException("memberInfoType");
            _memberInfo = memberInfo;
            _memberInfoType = memberInfoType;
            _isExplicitlyDefined = isExplicitlyDefined;
        }

        public SolrColumnMap WithName(string columnName)
        {
            if (string.IsNullOrWhiteSpace(columnName)) throw new ArgumentNullException("columnName");

            _columnName = columnName;
            return this;
        }
    }
}