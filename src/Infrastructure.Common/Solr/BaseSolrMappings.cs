using System;
using System.Collections.Generic;

namespace Infrastructure.Common.Solr
{
    public abstract class BaseSolrMappings
    {
        internal Dictionary<Type, ISolrTypeDefinitionMap> Definitions;

        protected BaseSolrMappings()
        {
            Definitions = new Dictionary<Type, ISolrTypeDefinitionMap>();
        }

        public SolrTypeDefinitionMappings<TPoco> For<TPoco>()
        {
            ISolrTypeDefinitionMap typeDefinitionMap;
            if (Definitions.TryGetValue(typeof(TPoco), out typeDefinitionMap) == false)
            {
                typeDefinitionMap = new SolrTypeDefinitionMappings<TPoco>();
                Definitions.Add(typeof(TPoco), typeDefinitionMap);
            }
            return (SolrTypeDefinitionMappings<TPoco>)typeDefinitionMap;
        }
    }
}