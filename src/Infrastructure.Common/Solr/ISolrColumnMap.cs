namespace Infrastructure.Common.Solr
{
    public interface ISolrColumnMap
    {
        string ColumnName { get; }
        SolrColumnMap WithName(string columnName);
    }
}