﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Cassandra;
using HttpWebAdapters;
using HttpWebAdapters.Adapters;
using SolrNet;
using SolrNet.Exceptions;
using SolrNet.Impl;
using SolrNet.Utils;

namespace Infrastructure.Common.Solr
{
    public class LbSolrConnection : ISolrConnection
    {
        //private string serverURL;
        private string version = "2.2";

        /// <summary>
        /// HTTP cache implementation
        /// </summary>
        public ISolrCache Cache { get; set; }

        /// <summary>
        /// HTTP request factory
        /// </summary>
        public IHttpWebRequestFactory HttpWebRequestFactory { get; set; }


        public ILoadBalancingPolicy LoadBalancingPolicy { get; private set; }

        private string _genericSolrUri;
        private IEnumerator<Host> _hostEnumerator;
        private IEnumerable<Host> _hostEnumerable;
        private string _keyspace;
        private string _columnFamily;
        private readonly object _queryPlanLock = new object();

        /// <summary>
        /// Manages HTTP connection with Solr
        /// </summary>
        /// <param name="serverURL">URL to Solr</param>
        public LbSolrConnection(ILoadBalancingPolicy loadBalancingPolicy, string genericSolrUri, string keyspace, string columnFamily)
        {
            LoadBalancingPolicy = loadBalancingPolicy;
            _genericSolrUri = genericSolrUri;

            _hostEnumerable = LoadBalancingPolicy.NewQueryPlan("solr", null);
            _hostEnumerator = _hostEnumerable.GetEnumerator();

            _keyspace = keyspace;
            _columnFamily = columnFamily;
            Timeout = -1;
            Cache = new NullCache();
            HttpWebRequestFactory = new HttpWebRequestFactory();
        }

        private Host GetNextHost()
        {
            Host host = null;
            //Lock to handle multiple threads from multiple executions to get a new host
            lock (_queryPlanLock)
            {
                while (_hostEnumerator.MoveNext())
                {
                    var h = _hostEnumerator.Current;
                    if (h.IsUp)
                    {
                        host = h;
                        break;
                    }
                }
                if (host == null)
                {
                    _hostEnumerator = _hostEnumerable.GetEnumerator();
                    return GetNextHost();
                }
            }
            return host;
        }

        /// <summary>
        /// URL to Solr
        /// </summary>
        public string GetNewServerUrl() {
            var host = GetNextHost();
            var serverUri = string.Format(_genericSolrUri, host.Address.Address);
            return serverUri;
        }

        public string GetNewCoreUri()
        {
            var serverUri = GetNewServerUrl();
            var coreUri = string.Format("{0}{1}.{2}", serverUri, _keyspace, _columnFamily);
            return coreUri;   
        }

        /// <summary>
        /// Solr XML response syntax version
        /// </summary>
        public string Version {
            get { return version; }
            set { version = value; }
        }

        /// <summary>
        /// HTTP connection timeout
        /// </summary>
        public int Timeout { get; set; }

        public string Post(string relativeUrl, string s) {
            var bytes = Encoding.UTF8.GetBytes(s);
            using (var content = new MemoryStream(bytes))
                return PostStream(relativeUrl, "text/xml; charset=utf-8", content, null);
        }

        public string PostStream(string relativeUrl, string contentType, Stream content, IEnumerable<KeyValuePair<string, string>> parameters) {
            var u = new UriBuilder(GetNewCoreUri());
            u.Path += relativeUrl;
            var parametersList = parameters.ToList();
            u.Query = GetQuery(parametersList);

            var request = HttpWebRequestFactory.Create(u.Uri);
            request.Method = HttpWebRequestMethod.POST;
            request.KeepAlive = true;
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

            if (Timeout > 0) {
                request.ReadWriteTimeout = Timeout;
                request.Timeout = Timeout;
            }
            if (contentType != null)
                request.ContentType = contentType;

            request.ContentLength = content.Length;
            request.ProtocolVersion = HttpVersion.Version11;

            try {
                using (var postStream = request.GetRequestStream()) {
                    CopyTo(content, postStream);
                }
                return GetResponse(request).Data;
            } catch (WebException e) {
                var msg = e.Message;
                if (e.Response != null) {
                    var r = new HttpWebResponseAdapter(e.Response);
                    if (r.StatusCode == HttpStatusCode.NotFound || r.StatusCode == HttpStatusCode.ServiceUnavailable)
                    {
                        return Get(relativeUrl, parametersList);
                    }
                    using (var s = e.Response.GetResponseStream())
                    using (var sr = new StreamReader(s))
                        msg = sr.ReadToEnd();
                }
                throw new SolrConnectionException(msg, e, request.RequestUri.ToString());
            }
        }

        private static void CopyTo(Stream input, Stream output) {
            byte[] buffer = new byte[0x1000];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                output.Write(buffer, 0, read);
        }

        public string Get(string relativeUrl, IEnumerable<KeyValuePair<string, string>> parameters) {
            var u = new UriBuilder(GetNewCoreUri());
            u.Path += relativeUrl;
            var parametersList = parameters.ToList();
            u.Query = GetQuery(parametersList);

            var request = HttpWebRequestFactory.Create(u.Uri);
            request.Method = HttpWebRequestMethod.GET;
            request.KeepAlive = true;
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

            var cached = Cache[u.Uri.ToString()];
            if (cached != null) {
                request.Headers.Add(HttpRequestHeader.IfNoneMatch, cached.ETag);
            }
            if (Timeout > 0) {
                request.ReadWriteTimeout = Timeout;
                request.Timeout = Timeout;                
            }
            try {
                var response = GetResponse(request);
                if (response.ETag != null)
                    Cache.Add(new SolrCacheEntity(u.Uri.ToString(), response.ETag, response.Data));
                return response.Data;
            } catch (WebException e) {
                if (e.Response != null) {
                    using (e.Response) {
                        var r = new HttpWebResponseAdapter(e.Response);
                        if (r.StatusCode == HttpStatusCode.NotFound || r.StatusCode == HttpStatusCode.ServiceUnavailable)
                        {
                            return Get(relativeUrl, parametersList);
                        }
                        if (r.StatusCode == HttpStatusCode.NotModified) {
                            return cached.Data;
                        }
                        using (var s = e.Response.GetResponseStream())
                        using (var sr = new StreamReader(s)) {
                            throw new SolrConnectionException(sr.ReadToEnd(), e, u.Uri.ToString());
                        }
                    }
                }
                throw new SolrConnectionException(e, u.Uri.ToString());
            }
        }

        /// <summary>
        /// Gets the Query 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private string GetQuery(IEnumerable<KeyValuePair<string, string>> parameters) {
            var param = new List<KeyValuePair<string, string>>();
            if (parameters != null)
                param.AddRange(parameters);

            param.Add(KV.Create("version", version));
            return string.Join("&", param
                .Select(kv => KV.Create(HttpUtility.UrlEncode(kv.Key), HttpUtility.UrlEncode(kv.Value)))
                .Select(kv => string.Format("{0}={1}", kv.Key, kv.Value))
                .ToArray());
        }

        /// <summary>
        /// Gets http response, returns (etag, data)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private SolrResponse GetResponse(IHttpWebRequest request) {
            using (var response = request.GetResponse()) {
                var etag = response.Headers[HttpResponseHeader.ETag];
                var cacheControl = response.Headers[HttpResponseHeader.CacheControl];
                if (cacheControl != null && cacheControl.Contains("no-cache"))
                    etag = null; // avoid caching things marked as no-cache

                return new SolrResponse(etag, ReadResponseToString(response));
            }
        }

        /// <summary>
        /// Reads the full stream from the response and returns the content as stream,
        /// using the correct encoding.
        /// </summary>
        /// <param name="response">Web response from request to Solr</param>
        /// <returns></returns>
        private string ReadResponseToString(IHttpWebResponse response)
        {
            using (var responseStream = response.GetResponseStream())
				using (var reader = new StreamReader(responseStream, TryGetEncoding(response))) {
					return reader.ReadToEnd();
            }
        }

        private struct SolrResponse {
            public string ETag { get; private set; }
            public string Data { get; private set; }
            public SolrResponse(string eTag, string data) : this() {
                ETag = eTag;
                Data = data;
            }
        }

        private Encoding TryGetEncoding(IHttpWebResponse response) {
            try {
                return Encoding.GetEncoding(response.CharacterSet);
            } catch {
                return Encoding.UTF8;
            }
        }
    }
}
