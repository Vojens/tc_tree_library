using System;
using System.Collections.Generic;
using System.Reflection;
using SolrNet;

namespace Infrastructure.Common.Solr
{
    public interface ISolrTypeDefinitionMap
    {
        Type PocoType { get; }
        string SolrCore { get; }
        SolrFieldModel UniqueKey { get; }
        ISolrColumnMap GetColumnDefinition(MemberInfo memberInfo);
        IDictionary<string, SolrFieldModel> GetFields();
    }
}