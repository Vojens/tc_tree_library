using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using SolrNet;

namespace Infrastructure.Common.Solr
{
    public class SolrTypeDefinitionMappings<TPoco> : BaseSolrMappings, ISolrTypeDefinitionMap
    {
        private readonly Type _pocoType;
        private readonly Dictionary<string, SolrColumnMap> _columnMaps;
        private SolrFieldModel _uniqueKey;
        private string _solrCore;

        public SolrTypeDefinitionMappings()
        {
            _pocoType = typeof(TPoco);
            _columnMaps = new Dictionary<string, SolrColumnMap>();
        }

        public Type PocoType
        {
            get { return _pocoType; }
        }

        public string SolrCore
        {
            get { return _solrCore; }
        }

        public SolrFieldModel UniqueKey
        {
            get { return _uniqueKey; }
        }

        public SolrTypeDefinitionMappings<TPoco> CoreName(string coreName)
        {
            if (string.IsNullOrWhiteSpace(coreName)) throw new ArgumentNullException("coreName");

            _solrCore = coreName;
            return this;
        }

        public SolrTypeDefinitionMappings<TPoco> SetUniqueKey<TProp>(Expression<Func<TPoco, TProp>> column)
        {
            if (column == null) throw new ArgumentNullException("column");

            var memberInfo = GetPropertyOrField(column);

            var declaringType = memberInfo.DeclaringType ?? memberInfo.ReflectedType;

            var sColumnMap = _columnMaps.Where(kvp => kvp.Key == memberInfo.Name).Select(kvp => kvp.Value).FirstOrDefault();

            if (sColumnMap == null)
                throw new ArgumentException(string.Format("Property '{0}.{1}' not mapped. Please use Add() to typeDefinitionMap it first", declaringType, memberInfo.Name));

            _uniqueKey = new SolrFieldModel
            {
                FieldName = sColumnMap.ColumnName,
                Property = sColumnMap.MemberInfo as PropertyInfo,
                Boost = null
            };
            return this;
        }

        public ISolrColumnMap GetColumnDefinition(MemberInfo memberInfo)
        {
            return _columnMaps.Where(kvp => kvp.Key == memberInfo.Name).Select(kvp => kvp.Value).FirstOrDefault();
        }

        public IDictionary<string, SolrFieldModel> GetFields()
        {
            var fields = _columnMaps.ToDictionary(pair => pair.Key, pair => new SolrFieldModel
            {
                FieldName = pair.Value.ColumnName,
                Property = pair.Value.MemberInfo as PropertyInfo,
                Boost = null
            });
            return fields;
        }

        public SolrTypeDefinitionMappings<TPoco> Column<TProp>(Expression<Func<TPoco, TProp>> column, Action<SolrColumnMap> columnConfig)
        {
            if (column == null) throw new ArgumentNullException("column");
            if (columnConfig == null) throw new ArgumentNullException("columnConfig");

            var memberInfo = GetPropertyOrField(column);

            SolrColumnMap sColumnMap;
            if (_columnMaps.TryGetValue(memberInfo.Name, out sColumnMap) == false)
            {
                var memberInfoType = memberInfo as PropertyInfo != null ? ((PropertyInfo)memberInfo).PropertyType : ((FieldInfo)memberInfo).FieldType;

                sColumnMap = new SolrColumnMap(memberInfo, memberInfoType, true);
                _columnMaps[memberInfo.Name] = sColumnMap;
            }

            columnConfig(sColumnMap);
            return this;
        }

        public SolrTypeDefinitionMappings<TPoco> Column<TProp>(Expression<Func<TPoco, TProp>> column)
        {
            return Column(column, _ => { });
        }

        private string GetColumnName(MemberInfo memberInfo)
        {
            SolrColumnMap sColumnMap;
            if (_columnMaps.TryGetValue(memberInfo.Name, out sColumnMap))
            {
                return ((ISolrColumnMap)sColumnMap).ColumnName ?? memberInfo.Name;
            }
            return memberInfo.Name;
        }

        private MemberInfo GetPropertyOrField<TProp>(Expression<Func<TPoco, TProp>> expression)
        {
            var body = expression.Body;

            if (body.NodeType == ExpressionType.Convert)
                body = ((UnaryExpression)body).Operand;

            var memberExpression = body as MemberExpression;
            if (memberExpression == null || IsPropertyOrField(memberExpression.Member) == false)
                throw new ArgumentOutOfRangeException("expression", string.Format("Expression {0} is not a property or field.", expression));

            if (memberExpression.Member.ReflectedType != _pocoType && _pocoType.IsSubclassOf(memberExpression.Member.ReflectedType) == false)
            {
                throw new ArgumentOutOfRangeException("expression", string.Format("Expression {0} refers to a property or field that is not from type {1}", expression, _pocoType));
            }

            return memberExpression.Member;
        }

        private static bool IsPropertyOrField(MemberInfo memberInfo)
        {
            return memberInfo.MemberType == MemberTypes.Field || memberInfo.MemberType == MemberTypes.Property;
        }
    }
}