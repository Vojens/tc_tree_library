﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using SolrNet.Mapping;

namespace Infrastructure.Common.Solr
{
    public class SolrMappingManager : MappingManager, ISolrMappingDefinition
    {
        private static readonly SolrMappingManager GlobalInstance = new SolrMappingManager();
        private readonly IDictionary<Type, ISolrTypeDefinitionMap> _definitions;
        private readonly IDictionary<Type, string> _keyspaces;

        public SolrMappingManager()
        {
            _definitions = new Dictionary<Type, ISolrTypeDefinitionMap>();
            _keyspaces = new ConcurrentDictionary<Type, string>();
        }

        public static SolrMappingManager Global
        {
            get { return GlobalInstance; }
        }

        public void Define<T>() where T : BaseSolrMappings, new()
        {
            var mappings = new T();
            foreach (var def in mappings.Definitions)
            {
                _definitions.Add(def);
                foreach (var pair in def.Value.GetFields())
                {
                    Add(pair.Value.Property, pair.Value.FieldName);
                    if (def.Value.UniqueKey.FieldName == pair.Value.FieldName)
                    {
                        SetUniqueKey(pair.Value.Property);
                    }
                }
            }
        }

        public string GetCore<T>()
        {
            var @type = typeof(T);
            ISolrTypeDefinitionMap result;
            if (_definitions.TryGetValue(@type, out result))
            {
                return result.SolrCore;
            }
            throw new ArgumentException(string.Format("Type '{0}' not mapped", @type));
        }

        public string GetKeySpace<T>()
        {
            var @type = typeof(T);
            string result;
            if (_keyspaces.TryGetValue(@type, out result))
            {
                return result;
            }
            throw new ArgumentException(string.Format("Type '{0}' not mapped", @type));
        }

        public string GetColumnName<T>(Expression<Func<T, object>> column)
        {
            var @type = typeof(T);
            ISolrTypeDefinitionMap result;
            if (_definitions.TryGetValue(@type, out result))
            {
                var memberInfo = GetPropertyOrField(column, @type);
                var columnMap = result.GetColumnDefinition(memberInfo);

                return columnMap.ColumnName;
            }
            throw new ArgumentException(string.Format("Type '{0}' not mapped", @type));
        }

        public void MapKeySpace<T>(string keySpace)
        {
            _keyspaces.Add(typeof(T), keySpace);
        }

        private MemberInfo GetPropertyOrField<TPoco, TProp>(Expression<Func<TPoco, TProp>> expression, Type pocoType)
        {
            var body = expression.Body;

            if (body.NodeType == ExpressionType.Convert)
                body = ((UnaryExpression)body).Operand;

            var memberExpression = body as MemberExpression;
            if (memberExpression == null || IsPropertyOrField(memberExpression.Member) == false)
                throw new ArgumentOutOfRangeException("expression", string.Format("Expression {0} is not a property or field.", expression));

            if (memberExpression.Member.ReflectedType != pocoType && pocoType.IsSubclassOf(memberExpression.Member.ReflectedType) == false)
            {
                throw new ArgumentOutOfRangeException("expression", string.Format("Expression {0} refers to a property or field that is not from type {1}", expression, pocoType));
            }

            return memberExpression.Member;
        }

        private static bool IsPropertyOrField(MemberInfo memberInfo)
        {
            return memberInfo.MemberType == MemberTypes.Field || memberInfo.MemberType == MemberTypes.Property;
        }
    }
}
