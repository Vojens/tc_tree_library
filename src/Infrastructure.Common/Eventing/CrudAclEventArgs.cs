﻿using System;

namespace Infrastructure.Common.Eventing
{
    public class CrudAclEventArgs : EventArgs
    {
        public CrudAcl Crud { get; private set; }
        public CrudAclEventArgs(CrudAcl crud)
        {
            Crud = crud;
        }
    }
    public enum CrudAcl
    {
        Any = 0,
        Create = 1,
        Read = 2,
        Update = 3,
        Delete = 4,
        ApproveBuddy = 5,
        RejectBuddy = 6
    }
    
}
