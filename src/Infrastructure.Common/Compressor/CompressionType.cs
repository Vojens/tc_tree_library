﻿namespace Infrastructure.Common.Compressor
{
    public enum CompressionType
    {
        None = 1,
        Lz4 = 2,
        Snappy = 3,
        Gzip = 4
    }
}
