﻿using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading.Tasks;
using LZ4;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Extensions.Type;

namespace Infrastructure.Common.Compressor
{
    public class LZ4Compressor : ICompressor
    {
        public Stream DecompressToStream(Stream stream)
        {
            var buffer = ReadAllBytes(stream, 0);
            return DecompressToStream(buffer);
        }

        public Stream DecompressToStream(byte[] buffer)
        {
            using (var istream = buffer.ToStream())
            using (var zstream = new LZ4Stream(istream, CompressionMode.Decompress))
            using (var ostream = new MemoryStream())
            {
                zstream.CopyTo(ostream);
                return ostream;
            }

        }

        public byte[] Decompress(byte[] buffer)
        {
            using (var ms = new MemoryStream(buffer))
            {
                using (var zip = new LZ4Stream(ms, CompressionMode.Decompress))
                {
                    using (var target = new MemoryStream())
                    {
                        zip.CopyTo(target);

                        return target.ToArray();
                    }
                }
            }

        }

        public async Task<byte[]> DecompressAsync(byte[] buffer)
        {
            using (var ms = new MemoryStream(buffer))
            {
                using (var zip = new LZ4Stream(ms, CompressionMode.Decompress))
                {
                    using (var target = new MemoryStream())
                    {
                        await zip.CopyToAsync(target).ConfigureAwait(false);

                        return target.ToArray();
                    }
                }
            }
        }

        public byte[] Compress(object obj)
        {
            return Compress(obj.ToByteArray());
        }

        public byte[] Compress(string str, Encoding encoding)
        {
            return Compress(str.Encode(encoding));
        }

        public byte[] Compress(byte[] data)
        {
            using (var output = new MemoryStream())
            {
                using (var lzStream = new LZ4Stream(output, CompressionMode.Compress))
                {
                    lzStream.Write(data, 0, data.Length);
                }

                return output.ToArray();
            }
        }

        public async Task<byte[]> CompressAsync(byte[] data)
        {
            using (var output = new MemoryStream())
            {
                using (var lzStream = new LZ4Stream(output, CompressionMode.Compress))
                {
                    await lzStream.WriteAsync(data, 0, data.Length).ConfigureAwait(false);
                }

                return output.ToArray();
            }
        }

        public static byte[] ReadAllBytes(Stream stream, int position)
        {
            var buffer = new byte[stream.Length - position];
            stream.Position = position;
            stream.Read(buffer, position, buffer.Length - position);
            return buffer;
        }
    }
}
