﻿using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace Infrastructure.Common.Compressor
{
    public class GzipCompressor : ICompressor
    {
        public byte[] Decompress(byte[] data)
        {
            using (var ms = new MemoryStream(data))
            {
                using (var zip = new GZipStream(ms, CompressionMode.Decompress))
                {
                    using (var target = new MemoryStream())
                    {
                        zip.CopyTo(target);

                        return target.ToArray();
                    }
                }
            }
        }

        public Task<byte[]> DecompressAsync(byte[] data)
        {
            throw new System.NotImplementedException();
        }

        public byte[] Compress(byte[] data)
        {
            using (var input = new MemoryStream(data))
            using (var output = new MemoryStream())
            {
                using (var gzip = new GZipStream(output, CompressionMode.Compress, true))
                {
                    input.CopyTo(gzip);
                }

                return output.ToArray();
            }
        }

        public async Task<byte[]> CompressAsync(byte[] bytes)
        {
            using (var input = new MemoryStream(bytes))
            using (var output = new MemoryStream())
            {
                using (var gzip = new GZipStream(output, CompressionMode.Compress, true))
                {
                    await input.CopyToAsync(gzip).ConfigureAwait(false);
                }

                return output.ToArray();
            }
        }
    }
}