﻿using System.Threading.Tasks;

namespace Infrastructure.Common.Compressor
{
    public interface ICompressor
    {
        byte[] Compress(byte[] bytes);
        Task<byte[]> CompressAsync(byte[] bytes);
        byte[] Decompress(byte[] data);
        Task<byte[]> DecompressAsync(byte[] data);
    }
}