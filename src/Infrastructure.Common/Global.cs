﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Cassandra;
using CompressionType = Infrastructure.Common.Compressor.CompressionType;

namespace Infrastructure.Common
{
    public static class Global
    {
        private static Encoding _resourceDataEncoding = Encoding.Unicode;

        /// <exception cref="NotSupportedException" accessor="set">If encoding is not unicode or utf8.</exception>
        public static Encoding ResourceDataEncoding
        {
            get { return _resourceDataEncoding; }
            set
            {
                if (!Equals(value, Encoding.Unicode) && !Equals(value, Encoding.UTF8))
                {
                    throw new NotSupportedException(string.Format("Encoding other than {0} and {1} are not supported",
                        Encoding.Unicode, Encoding.UTF8));
                }
                _resourceDataEncoding = value;
            }
        }

        private static int _compressionMinSizeInBytes = 1000;

        public static int CompressionMinSizeInBytes
        {
            get { return _compressionMinSizeInBytes; }
            set { _compressionMinSizeInBytes = value; }
        }

        private static bool _isCompressionMinSizeEnabled = true;

        public static bool IsCompressionMinSizeEnabled
        {
            get { return _isCompressionMinSizeEnabled; }
            set { _isCompressionMinSizeEnabled = value; }
        }

        private static CompressionType _defaultCompressionType = CompressionType.Lz4;

        public static CompressionType DefaultCompressionType
        {
            get { return _defaultCompressionType; }
            set { _defaultCompressionType = value; }
        }

        private static string _defaultHashName = "CityHash128";
        public static string DefaultHashName
        {
            get { return _defaultHashName; }
            set { _defaultHashName = value; }
        }

        public static ConsistencyLevel SolrConsistencyLevel
        {
            get { return _solrConsistencyLevel; }
        }

        private static string _solrPaging = "driver";
        public static string SolrPaging
        {
            get { return _solrPaging; }
        }


        private static readonly IDictionary<Type, string> _supportedPropertyTypesDictionary = new Dictionary
            <Type, string>
        {
            {typeof (int), "int"},
            {typeof (double), "double"}
        };

        public static readonly IDictionary<Type, string> SupportedPropertyTypesDictionary =
            new ReadOnlyDictionary<Type, string>(_supportedPropertyTypesDictionary);


        public static HashSet<string> SupportedPropertyTypes = new HashSet<string>
        {
            SupportedPropertyTypesDictionary[typeof(int)],
            SupportedPropertyTypesDictionary[typeof(double)]
        };

        private static ConsistencyLevel _solrConsistencyLevel = ConsistencyLevel.One;

    }
}