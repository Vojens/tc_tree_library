﻿using System.Collections.Generic;
using Cassandra;

namespace Infrastructure.ConnectionProvider
{
    public class BatchStatementDecorator : BatchStatement
    {
        protected readonly List<Statement> _queries = new List<Statement>();

//        /// <summary>
//        /// Adds a new statement to this batch.
//        /// Note that statement can be any <c>Statement</c>. It is allowed to mix <see cref="SimpleStatement"/> and <see cref="BoundStatement"/> in the same <c>BatchStatement</c> in particular.
//        /// Please note that the options of the added <c>Statement</c> (all those defined directly by the Statement class: consistency level, fetch size, tracing, ...) will be ignored for the purpose of the execution of the Batch. Instead, the options used are the one of this <c>BatchStatement</c> object.
//        /// </summary>
//        /// <param name="statement">Statement to add to the batch</param>
//        /// <returns>The Batch statement</returns>
        public new BatchStatement Add(Statement statement)
        {
            _queries.Add(statement);
            var bsd = statement as BoundStatementDecorator;
            base.Add(bsd != null ? bsd.BoundStatement : statement);
            return this;
        }
    }
}
