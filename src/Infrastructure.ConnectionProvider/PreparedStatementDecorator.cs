﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cassandra;

namespace Infrastructure.ConnectionProvider
{
    public class PreparedStatementDecorator : PreparedStatement
    {
        private readonly PreparedStatement _preparedStatement;
        private readonly string _cqlQueryName, _cqlQuery;
        public PreparedStatementDecorator(PreparedStatement preparedStatement, string cqlQueryName, string cqlQuery)
        {
            _preparedStatement = preparedStatement;
            _cqlQueryName = cqlQueryName;
            _cqlQuery = cqlQuery;
        }

        public PreparedStatement PreparedStatement
        {
            get { return _preparedStatement; }
        }

        public string CqlQuery
        {
            get { return _cqlQuery; }
        }

        public new PreparedStatementDecorator SetConsistencyLevel(ConsistencyLevel consistency)
        {
            _preparedStatement.SetConsistencyLevel(consistency);
            return this;
        }

        /// <summary>
        ///  Gets metadata on the bounded variables of this prepared statement.
        /// </summary>
        public new RowSetMetadata Variables
        {
            get { return _preparedStatement.Variables; }
        }

        public new RoutingKey RoutingKey
        {
            get { return _preparedStatement.RoutingKey; }
        }

        public new ConsistencyLevel? ConsistencyLevel
        {
            get { return _preparedStatement.ConsistencyLevel; }
        }


        /// <summary>
        /// Set the routing key for this query.
        /// <para>
        /// The routing key is a hint for token aware load balancing policies but is never mandatory.
        /// This method allows you to manually provide a routing key for this query.
        /// </para>
        /// <para>
        /// If the partition key is composite, you should provide multiple routing key components.
        /// </para>
        /// </summary>
        /// <param name="routingKeyComponents"> the raw (binary) values to compose to
        ///  obtain the routing key. </param>
        /// <returns>this <c>PreparedStatement</c> object.  <see>Query#GetRoutingKey</see></returns>
        public new PreparedStatementDecorator SetRoutingKey(params RoutingKey[] routingKeyComponents)
        {
            _preparedStatement.SetRoutingKey(routingKeyComponents);
            return this;
        }

        /// <summary>
        /// Creates a new BoundStatement object and bind its variables to the provided
        /// values.
        /// <para>
        /// Specify the parameter values by the position of the markers in the query or by name, 
        /// using a single instance of an anonymous type, with property names as parameter names.
        /// </para>
        /// <para>
        /// Note that while no more <c>values</c> than bound variables can be provided, it is allowed to
        /// provide less <c>values</c> that there is variables.
        /// </para>
        /// </summary>
        /// <param name="objs"> the values to bind to the variables of the newly
        ///  created BoundStatement. </param>
        /// <returns>the newly created <c>BoundStatement</c> with its variables
        ///  bound to <c>values</c>. </returns>
        public BoundStatementDecorator BindParams(params object[] objs)
        {
            var objects = new List<object>();
            foreach (var o in objs)
            {
                if (o == null)
                {
                    objects.Add(null);
                }
                else if (o.GetType().IsArray)
                {
                    objects.AddRange(((Array)o).Cast<object>());
                }
                else
                {
                    objects.Add(o);
                }
            }
            var bs = new BoundStatementDecorator(this, _preparedStatement.Bind(objects.ToArray()));
            return bs;
        }

        public BoundStatementDecorator BindParams(object[] exceptions, params object[] objs)
        {
            var objects = new List<object>();
            foreach (var o in objs)
            {
                if (!exceptions.Contains(o) && o.GetType().IsArray)
                {
                    objects.AddRange(((Array)o).Cast<object>());
                }
                else
                {
                    objects.Add(o);
                }
            }
            var bs = new BoundStatementDecorator(this, _preparedStatement.Bind(objects.ToArray()));
            return bs;
        }

        /// <summary>
        /// Creates a new BoundStatement object and bind its variables to the provided
        /// values.
        /// <para>
        /// Specify the parameter values by the position of the markers in the query or by name, 
        /// using a single instance of an anonymous type, with property names as parameter names.
        /// </para>
        /// <para>
        /// Note that while no more <c>values</c> than bound variables can be provided, it is allowed to
        /// provide less <c>values</c> that there is variables.
        /// </para>
        /// </summary>
        /// <param name="values"> the values to bind to the variables of the newly
        ///  created BoundStatement. </param>
        /// <returns>the newly created <c>BoundStatement</c> with its variables
        ///  bound to <c>values</c>. </returns>
        public override BoundStatement Bind(params object[] values)
        {
            var bs = new BoundStatementDecorator(this, _preparedStatement.Bind(values));
            return bs;
        }

        /// <summary>
        /// For named query markers, it sets the parameter names that are part of the routing key.
        /// <para>
        /// Use this method ONLY if the parameter names are different from the partition key names.
        /// </para>
        /// </summary>
        /// <returns>this <c>PreparedStatement</c> object.</returns>
        public new PreparedStatementDecorator SetRoutingNames(params string[] names)
        {
            _preparedStatement.SetRoutingNames(names);
            return this;
        }


        public static implicit operator BoundStatementDecorator(PreparedStatementDecorator preparedStatementDecorator)
        {
            return new BoundStatementDecorator(preparedStatementDecorator);
        }
    }
}
