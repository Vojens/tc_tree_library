﻿namespace Infrastructure.ConnectionProvider.Config
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("DataTemplate", Namespace = "", IsNullable = false)]
    public partial class DataTemplate
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("CqlStatement", IsNullable = false)]
        public DataTemplateCqlStatement[] DataQuery { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("name")]
        public string Name { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("description")]
        public string Description { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("defaultPackage")]
        public string DefaultPackage { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("version")]
        public decimal Version { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DataTemplateCqlStatement
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("name")]
        public string Name { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value { get; set; }
    }
}
