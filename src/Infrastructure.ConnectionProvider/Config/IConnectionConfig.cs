﻿
namespace Infrastructure.ConnectionProvider.Config
{
    public interface IConnectionConfig
    {
        IClusterConfig Cluster { get; }
        IStatementOptions StatementOptions { get; }
    }
}
