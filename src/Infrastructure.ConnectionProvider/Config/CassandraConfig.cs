﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Serialization;
using Cassandra;
using Infrastructure.ConnectionProvider.Util;

namespace Infrastructure.ConnectionProvider.Config
{
    [Serializable]
    [XmlRoot("CassandraConfig")]
    public class CassandraConfig : IConnectionConfig
    {
        [XmlElement("Cluster", IsNullable = false)]
        public ClusterConfig ClusterConfig { get; set; }

        [XmlElement("StatementOptions", IsNullable = true)]
        public StatementOptions StatementOptionsValue { get; set; }
        private static readonly Logger Logger = new Logger(typeof(CassandraConfig));

        public static CassandraConfig CreateCassandraConfig(string fullConfigPath)
        {
            CassandraConfig cassandraConfig;
            try
                {
                    var serializer = new XmlSerializer(typeof(CassandraConfig));
                    if (!File.Exists(fullConfigPath))
                    {
                        throw new FileNotFoundException(fullConfigPath + " is neither found in Config file not base.");
                    }
                    using (StreamReader reader = new StreamReader(fullConfigPath))
                    {
                        cassandraConfig = (CassandraConfig)serializer.Deserialize(reader);
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                    throw;
                }
            return cassandraConfig;
        }

        [XmlIgnore]
        public IClusterConfig Cluster
        {
            get
            {
                return ClusterConfig;
            }
        }

        [XmlIgnore]
        public IStatementOptions StatementOptions
        {
            get
            {
                return StatementOptionsValue;
            }
        }

    }


    [Serializable]
    public class ClusterConfig : IClusterConfig
    {
        //private static readonly Logger _logger = new Logger(typeof(ClusterConfig));

        public static readonly string DefaultContactPoints = "localhost";
        public static readonly bool DefaultMetricsEnabled = true;
        public static readonly bool DefaultDeferredInitialization = false;
        public static readonly bool DefaultJmxReportingEnabled = true;
        public static readonly bool DefaultSslEnabled = false;
        public static readonly int DefaultPort = 9042;
        public static readonly string DefaultAuthProvider = "Cassandra.NoneAuthProvider, Cassandra";
        private string _keyspace;
        private int? _solrPageSize;
        private CompressionType? _compressionType;
        private List<string> _contactPoints = new List<string>();
        private ILoadBalancingPolicy _loadBalancingPolicy;
        private LoadBalancingPolicyConfig _loadBalancingPolicyConfig;
        private PoolingOptions _poolingOptions;
        private PoolingOptionsConfig _poolingOptionsConfig;
        private int? _port = DefaultPort;
        private IReconnectionPolicy _reconnectionPolicy;
        private ReconnectionPolicyConfig _reconnectionPolicyConfig; 
        private IRetryPolicy _retryPolicy;
        private RetryPolicyContainer _retryPolicycontainer;
        //private List<string> _shutdownScripts = new List<string>();
        private SocketOptions _socketOptions;
        private SocketOptionsConfig _socketOptionsConfig;
        private bool? _sslEnabled = DefaultSslEnabled;
        private SSLOptions _sslOptions; //TODO certificates
        private SslOptionsConfig _sslOptionsConfig;
        //private List<string> _startupScripts = new List<string>();
        private bool? _disableRowSetBuffering;
        private QueryOptionsConfig _queryOptionsConfig;
        private QueryOptions _queryOptions;
        private int? _queryTimeout;
        private AuthenticationConfig _authenticationConfig = new AuthenticationConfig() { Provider = DefaultAuthProvider };
        private IAuthProvider _authProvider = new NoneAuthProvider();


        [XmlElement("IsMultiNode", IsNullable = true)]
        public bool? IsMultiNode { get; set; }

        [XmlElement("DataCenterName", IsNullable = true)]
        public string DataCenterName { get; set; }

        //[XmlElementAttribute("ContactPoints", IsNullable = false)]
        [XmlArray("ContactPoints")]
        [XmlArrayItem("ContactPoint")]
        public List<string> ContactPoints
        {
            get { return _contactPoints; }
            set { _contactPoints = value; }
        }

        public void ResetContactPoints()
        {
            _contactPoints = new List<string>(){DefaultContactPoints};
        }

        [XmlElement("Port", Type = typeof (int))]
        public int? Port
        {
            get { return _port; }
            set { _port = value; }
        }

        [XmlElement("CompressionType", IsNullable = true)]
        public CompressionType? CompressionType
        {
            get { return _compressionType; }
            set { _compressionType = value; }
        }
        
        [XmlElement("DisableRowSetBuffering", IsNullable = true)]
        public bool? DisableRowSetBuffering
        {
            get { return _disableRowSetBuffering; }
            set { _disableRowSetBuffering = value; }
        }

        [XmlElement("Authentication", IsNullable = true)]
        public AuthenticationConfig AuthenticationConfig
        {
            get { return _authenticationConfig; }
            set
            {
                _authenticationConfig = value;
                var providerType = Type.GetType(_authenticationConfig.Provider);
                if (providerType == null)
                {
                    throw new ArgumentNullException(string.Format("Provider Type {0} is unknown", _authenticationConfig.Provider));
                }
                if (providerType == typeof(NoneAuthProvider))
                {
                    _authProvider = new NoneAuthProvider();
                }
                else if (providerType == typeof(PlainTextAuthProvider))
                {
                    var userName = _authenticationConfig.UserName;
                    if (string.IsNullOrEmpty(userName))
                    {
                        throw new ArgumentNullException(
                            string.Format(
                                "Username of the authentication is null/empty. the provider is PlainTextAuthProvider. If there is no authentication, then use {0}",
                                DefaultAuthProvider));
                    }
                    _authProvider = new PlainTextAuthProvider(userName, _authenticationConfig.Password);
                }
                else if (providerType == typeof(DseAuthProvider))
                {
                    IDseCredentialsResolver dseCredentialsResolver;
                    NetworkCredential networkCredential;
                    if (String.IsNullOrEmpty(_authenticationConfig.UserName) &&
                        String.IsNullOrEmpty(_authenticationConfig.Password) &&
                        String.IsNullOrEmpty(_authenticationConfig.Domain))
                    {
                        networkCredential = null;
                    }
                    else if (!String.IsNullOrEmpty(_authenticationConfig.UserName) &&
                             !String.IsNullOrEmpty(_authenticationConfig.Password) &&
                             String.IsNullOrEmpty(_authenticationConfig.Domain))
                    {
                        networkCredential = new NetworkCredential(_authenticationConfig.UserName,
                            _authenticationConfig.Password);
                    }
                    else if (!String.IsNullOrEmpty(_authenticationConfig.UserName) &&
                             !String.IsNullOrEmpty(_authenticationConfig.Password) &&
                             !String.IsNullOrEmpty(_authenticationConfig.Domain))
                    {
                        networkCredential = new NetworkCredential(_authenticationConfig.UserName,
                            _authenticationConfig.Password, _authenticationConfig.Domain);
                    }
                    else
                    {
                        throw new ArgumentException(string.Format("the arguements of {0} provided are not valid",
                            _authenticationConfig.Provider));
                    }

                    dseCredentialsResolver = new SimpleDseCredentialsResolver(networkCredential,
                        _authenticationConfig.Principal);
                    _authProvider = new DseAuthProvider(dseCredentialsResolver);
                }
                else
                {
                    var ctor = providerType.GetConstructors().First();
                    var createdActivator = Activators.GetActivator<IAuthProvider>(ctor);
                    //create an instance
                    _authProvider = createdActivator();
                }
            }
        }

        
        [XmlIgnore]
        public IAuthProvider AuthProvider {
            get { return _authProvider; }
            set { _authProvider = value; }
        }

        [XmlElement("QueryTimeout", IsNullable = true)]
        public int? QueryTimeout
        {
            get { return _queryTimeout; }
            set { _queryTimeout = value; }
        }

        [XmlIgnore]
        public PoolingOptions PoolingOptions
        {
            get { return _poolingOptions; }
            set { _poolingOptions = value; }
        }

        [XmlElement("PoolingOptions", IsNullable = true)]
        public PoolingOptionsConfig PoolingOptionsConfig
        {
            get { return _poolingOptionsConfig;}
            set
            {
                _poolingOptionsConfig = value;

                if (_poolingOptions == null)
                {
                    _poolingOptions = new PoolingOptions();
                }
                #region core connections
                if (_poolingOptionsConfig.CoreConnectionsForLocal.HasValue)
                    _poolingOptions.SetCoreConnectionsPerHost(HostDistance.Local,
                        _poolingOptionsConfig.CoreConnectionsForLocal.Value);

                if (_poolingOptionsConfig.CoreConnectionsForRemote.HasValue)
                    _poolingOptions.SetCoreConnectionsPerHost(HostDistance.Remote,
                        _poolingOptionsConfig.CoreConnectionsForRemote.Value);
                #endregion

                #region max connections
                if (_poolingOptionsConfig.MaxConnectionsForLocal.HasValue)
                    _poolingOptions.SetMaxConnectionsPerHost(HostDistance.Local,
                        _poolingOptionsConfig.MaxConnectionsForLocal.Value);

                if (_poolingOptionsConfig.MaxConnectionsForRemote.HasValue)
                    _poolingOptions.SetMaxConnectionsPerHost(HostDistance.Remote,
                        _poolingOptionsConfig.MaxConnectionsForRemote.Value);

                #endregion

                #region MinSimultaneousRequests
                if (_poolingOptionsConfig.MinSimultaneousRequestsForLocal.HasValue)
                    _poolingOptions.SetMinSimultaneousRequestsPerConnectionTreshold(HostDistance.Local,
                        _poolingOptionsConfig.MinSimultaneousRequestsForLocal.Value);

                if (_poolingOptionsConfig.MinSimultaneousRequestsForRemote.HasValue)
                    _poolingOptions.SetMinSimultaneousRequestsPerConnectionTreshold(HostDistance.Remote,
                        _poolingOptionsConfig.MinSimultaneousRequestsForRemote.Value);

                #endregion

                #region MaxSimultaneousRequests
                if (_poolingOptionsConfig.MaxSimultaneousRequestsForLocal.HasValue)
                    _poolingOptions.SetMaxSimultaneousRequestsPerConnectionTreshold(HostDistance.Local,
                        _poolingOptionsConfig.MaxSimultaneousRequestsForLocal.Value);

                if (_poolingOptionsConfig.MaxSimultaneousRequestsForRemote.HasValue)
                    _poolingOptions.SetMaxSimultaneousRequestsPerConnectionTreshold(HostDistance.Remote,
                        _poolingOptionsConfig.MaxSimultaneousRequestsForRemote.Value);

                #endregion

                #region HeartBeatInterval
                if (_poolingOptionsConfig.HeartBeatInterval.HasValue)
                    _poolingOptions.SetHeartBeatInterval(_poolingOptionsConfig.HeartBeatInterval.Value);
                #endregion
            }
        }


        [XmlIgnore]
        public SocketOptions SocketOptions
        {
            get { return _socketOptions; }
            set { _socketOptions = value; }
        }

        [XmlElement("SocketOptions", IsNullable = true)]
        public SocketOptionsConfig SocketOptionsConfig
        {
            get { return _socketOptionsConfig; }
            set
            {
                _socketOptionsConfig = value;

                if (_socketOptions == null)
                {
                    _socketOptions = new SocketOptions();
                }

                if (_socketOptionsConfig.ConnectTimeoutMillis.HasValue)
                    _socketOptions.SetConnectTimeoutMillis(_socketOptionsConfig.ConnectTimeoutMillis.Value);

                if (_socketOptionsConfig.KeepAlive.HasValue)
                    _socketOptions.SetKeepAlive(_socketOptionsConfig.KeepAlive.Value);

                if (_socketOptionsConfig.ReceiveBufferSize.HasValue)
                    _socketOptions.SetReceiveBufferSize(_socketOptionsConfig.ReceiveBufferSize.Value);

                if (_socketOptionsConfig.ReuseAddress.HasValue)
                    _socketOptions.SetReuseAddress(_socketOptionsConfig.ReuseAddress.Value);

                if (_socketOptionsConfig.SendBufferSize.HasValue)
                    _socketOptions.SetSendBufferSize(_socketOptionsConfig.SendBufferSize.Value);

                if (_socketOptionsConfig.SoLinger.HasValue)
                    _socketOptions.SetSoLinger(_socketOptionsConfig.SoLinger.Value);

                if (_socketOptionsConfig.TcpNoDelay.HasValue)
                    _socketOptions.SetTcpNoDelay(_socketOptionsConfig.TcpNoDelay.Value);

                if (_socketOptionsConfig.UseStreamMode.HasValue)
                    _socketOptions.SetStreamMode(_socketOptionsConfig.UseStreamMode.Value);

                if (_socketOptionsConfig.ReadTimeoutMillis.HasValue)
                    _socketOptions.SetReadTimeoutMillis(_socketOptionsConfig.ReadTimeoutMillis.Value);
                
                if (_socketOptionsConfig.DefunctReadTimeoutThreshold.HasValue)
                    _socketOptions.SetDefunctReadTimeoutThreshold(_socketOptionsConfig.DefunctReadTimeoutThreshold.Value);

            }
        }

        [XmlIgnore]
        public ILoadBalancingPolicy LoadBalancingPolicy
        {
            get
            {
                if (_loadBalancingPolicy != null)
                {
                    return _loadBalancingPolicy;
                }
                return LoadBalancingPolicyConfig == null ? null : _loadBalancingPolicy;
            }
            set { _loadBalancingPolicy = value; }
        }

        public static ILoadBalancingPolicy CreateLoadBalancingPolicy(LoadBalancingPolicyConfig loadBalancingPolicyConfig)
        {
            if (loadBalancingPolicyConfig == null)
            {
                throw new ArgumentNullException(string.Format("{0} cannot be null", (LoadBalancingPolicyConfig) null));
            }
            if (loadBalancingPolicyConfig is RoundRobinPolicyConfig)
            {
                return new RoundRobinPolicy();
            }
            if (loadBalancingPolicyConfig is DcAwareRoundRobinPolicyConfig)
            {
                var dcAwareRoundRobinPolicyConfig = loadBalancingPolicyConfig as DcAwareRoundRobinPolicyConfig;
                if (dcAwareRoundRobinPolicyConfig.LocalDc == null &&
                    !dcAwareRoundRobinPolicyConfig.UsedHostsPerRemoteDc.HasValue)
                {
                    return new DCAwareRoundRobinPolicy();
                }
                if (dcAwareRoundRobinPolicyConfig.LocalDc != null &&
                         !dcAwareRoundRobinPolicyConfig.UsedHostsPerRemoteDc.HasValue)
                {
                    return new DCAwareRoundRobinPolicy(dcAwareRoundRobinPolicyConfig.LocalDc);
                }
                if (dcAwareRoundRobinPolicyConfig.LocalDc != null &&
                         dcAwareRoundRobinPolicyConfig.UsedHostsPerRemoteDc.HasValue)
                {
                    return new DCAwareRoundRobinPolicy(dcAwareRoundRobinPolicyConfig.LocalDc,
                        dcAwareRoundRobinPolicyConfig.UsedHostsPerRemoteDc.Value);
                }

                string errMsg = string.Format("LoadBalancingPolicyConfig of type {0} is not supported", loadBalancingPolicyConfig.GetType());
                var notSupportedException = new NotSupportedException(errMsg);
                //_logger.Error(notSupportedException);
                throw notSupportedException;

            }
            if (loadBalancingPolicyConfig is RetryLoadBalancingPolicyConfig)
            {
                var retryLoadBalancingPolicy = loadBalancingPolicyConfig as RetryLoadBalancingPolicyConfig;
                var loadBalancingPolicy = CreateLoadBalancingPolicy(retryLoadBalancingPolicy.LoadBalancingPolicy);
                var reconnectionPolicy = CreateReconnectionPolicy(retryLoadBalancingPolicy.ReconnectionPolicyConfig);
                return new RetryLoadBalancingPolicy(loadBalancingPolicy, reconnectionPolicy);
            }
            if (loadBalancingPolicyConfig is TokenAwarePolicyConfig)
            {
                var tokenAwarePolicyConfig = loadBalancingPolicyConfig as TokenAwarePolicyConfig;
                if (tokenAwarePolicyConfig.ChildPolicy == null)
                {
                    throw new ArgumentNullException("TokenAwarePolicy needs to have a child balancing policy");
                }
                ILoadBalancingPolicy childLoadBalancyPolicy;
                try
                {
                    childLoadBalancyPolicy = CreateLoadBalancingPolicy(tokenAwarePolicyConfig.ChildPolicy);
                }
                catch (NotSupportedException)
                {
                    throw new NotSupportedException("The child load balancing policy of TokenAwarePolicyConfig is not supported");
                }
                
                return new TokenAwarePolicy(childLoadBalancyPolicy);
            }
            else
            {
                string errMsg = string.Format("The type of load balancing policy configured ({0})is not supported",loadBalancingPolicyConfig.GetType());
                var notSupportedException = new NotSupportedException(errMsg);
                //_logger.Error(notSupportedException);
                throw notSupportedException;
            }
        }

        public static IReconnectionPolicy CreateReconnectionPolicy(ReconnectionPolicyConfig reconnectionPolicyConfig)
        {
            if (reconnectionPolicyConfig is ConstantReconnectionPolicyConfig)
            {
                var constantReconnectionPolicyConfig = reconnectionPolicyConfig as ConstantReconnectionPolicyConfig;
                return new ConstantReconnectionPolicy(constantReconnectionPolicyConfig.ConstantDelayMs);
            }
            if (reconnectionPolicyConfig is ExponentialReconnectionPolicyConfig)
            {
                var exponentialReconnectionPolicyConfig = reconnectionPolicyConfig as ExponentialReconnectionPolicyConfig;
                return new ExponentialReconnectionPolicy(exponentialReconnectionPolicyConfig.BaseDelayMs, exponentialReconnectionPolicyConfig.MaxDelayMs);
            }
            if (reconnectionPolicyConfig is FixedReconnectionPolicyConfig)
            {
                var fixedReconnectionPolicyConfig = reconnectionPolicyConfig as FixedReconnectionPolicyConfig;
                return new FixedReconnectionPolicy(fixedReconnectionPolicyConfig.Delays);
            }
            return null;
        }

        [XmlElement(ElementName = "RoundRobinPolicy", Type = typeof (RoundRobinPolicyConfig))]
        [XmlElement(ElementName = "DCAwareRoundRobinPolicy", Type = typeof(DcAwareRoundRobinPolicyConfig))]
        [XmlElement(ElementName = "TokenAwarePolicy", Type = typeof(TokenAwarePolicyConfig))]
        [XmlElement(ElementName = "RetryLoadBalancingPolicy", Type = typeof(RetryLoadBalancingPolicyConfig))]
        public LoadBalancingPolicyConfig LoadBalancingPolicyConfig {
            get
            {
                return _loadBalancingPolicyConfig;
            }
            set
            {
                _loadBalancingPolicyConfig = value;
                if (_loadBalancingPolicyConfig != null)
                    _loadBalancingPolicy = CreateLoadBalancingPolicy(_loadBalancingPolicyConfig);
            }
        }

        public ReconnectionPolicyConfig ReconnectionPolicyConfig
        {
            get
            {
                return _reconnectionPolicyConfig;
            }
            set
            {
                _reconnectionPolicyConfig = value;
                if (_reconnectionPolicyConfig != null)
                    _reconnectionPolicy = CreateReconnectionPolicy(_reconnectionPolicyConfig);
            }
        }

        [XmlElement("SslEnabled", IsNullable = true)]
        public bool? SslEnabled
        {
            get { return _sslEnabled; }
            set { _sslEnabled = value; }
        }

        [XmlIgnore]
        public SSLOptions SslOptions
        {
            get { return _sslOptions; }
            set { _sslOptions = value; }
        }

        [XmlElement("SslOptions", IsNullable = true)]
        public SslOptionsConfig SslOptionsConfig
        {
            get { return _sslOptionsConfig;}
            set
            {
                _sslOptionsConfig = value;
                if (_sslOptionsConfig != null)
                {
                    if (_sslOptionsConfig.Certificates == null || !_sslOptionsConfig.Certificates.Any())
                    {
                        throw new InvalidOperationException("There needs to be atleast 1 certificate");
                    }

                    if (!_sslOptionsConfig.SslProtocols.HasValue &&
                        !_sslOptionsConfig.CheckCertificateRevocation.HasValue)
                    {
                        _sslOptions = new SSLOptions();
                    }else if (_sslOptionsConfig.SslProtocols.HasValue ^
                              _sslOptionsConfig.CheckCertificateRevocation.HasValue)
                    {
                        throw new ArgumentNullException(
                            "If CheckCertificateRevocation or SslProtocols in SslOptions is Set, the other one has to be Set as well");
                    }
                    else
                    {
                                                //Define the Callback function that will be used by the C# Driver to validate the certificate
                        RemoteCertificateValidationCallback callback = (s, cert, chain, policyErrors) =>
                        {
                            //A "real" certificate was given!
                            if (policyErrors == SslPolicyErrors.None)
                            {
                                return true;
                            }

                            //Since this is a "Global" and locally crated certificate the CN names will not match and the certificate chaining will be incorrect
                            //Policy Errors will be RemoteCertificateNameMismatch and RemoteCertificateChainErrors
                            //Can add some additional checking like the cert.IssuerName, etc.
                            if (policyErrors.HasFlag(SslPolicyErrors.RemoteCertificateChainErrors) &&
                                chain.ChainStatus.Length == 1 &&
                                chain.ChainStatus[0].Status == X509ChainStatusFlags.UntrustedRoot)
                            {
                                return true;
                            }

                            return false;
                        };
                        _sslOptions = new SSLOptions(_sslOptionsConfig.SslProtocols.Value, _sslOptionsConfig.CheckCertificateRevocation.Value, callback);
                    }
                    
                    var certs = new X509Certificate2Collection();
                    var basePath = ConfigurationManager.AppSettings["BasePath"];
                    foreach (var certConfig in _sslOptionsConfig.Certificates)
                    {
                        //Add the C* node certificate(s) plus their password. 
                        //If there were more than one certificate they can be added here!
                        X509Certificate2 cert;
                        if (!string.IsNullOrWhiteSpace(certConfig.FilePath))
                        {
                            var absolutePath = !string.IsNullOrEmpty(basePath) ? certConfig.FilePath.Replace("{BASEPATH}", basePath) : Path.GetFullPath(certConfig.FilePath);
                            if (!File.Exists(absolutePath))
                            {
                                throw new ArgumentException(string.Format("Certificate File {0} does not exist", absolutePath));
                            }
                            cert = string.IsNullOrWhiteSpace(certConfig.Password) ? new X509Certificate2(absolutePath) : new X509Certificate2(absolutePath, certConfig.Password);
                        }
                        else if (!string.IsNullOrWhiteSpace(certConfig.RawData))
                        {
                            cert = new X509Certificate2(Convert.FromBase64String(certConfig.RawData));
                        }
                        else
                        {
                            string certificateConfig;
                            try
                            {
                                var serializer = new XmlSerializer(typeof (CertificateConfig),
                                    new XmlRootAttribute("Certificate"));

                                using (StringWriter textWriter = new StringWriter())
                                {
                                    serializer.Serialize(textWriter, certConfig);
                                    certificateConfig = textWriter.ToString();
                                }
                            }
                            catch (Exception)
                            {
                                certificateConfig = "Error: failed to show the certificate config";
                            }

                            throw new InvalidOperationException(string.Format("Unable to load certificate with the following params {0}", certificateConfig));
                        }
                        certs.Add(cert);
                    }
                    SslOptions.SetCertificateCollection(certs);
                }
            }
        }

        public static bool ValidateServerCertificate(
              object sender,
              X509Certificate certificate,
              X509Chain chain,
              SslPolicyErrors sslPolicyErrors)
        {
            //A "real" certificate was given!
            if (sslPolicyErrors == SslPolicyErrors.None)
            {
                return true;
            }

            //Since this is a "Global" and locally crated certificate the CN names will not match and the certificate chaining will be incorrect
            //Policy Errors will be RemoteCertificateNameMismatch and RemoteCertificateChainErrors
            //Can add some additional checking like the cert.IssuerName, etc.
            //TODO revise later
            if (sslPolicyErrors.HasFlag(SslPolicyErrors.RemoteCertificateChainErrors) &&
                chain.ChainStatus.Length == 1 &&
                chain.ChainStatus[0].Status == X509ChainStatusFlags.UntrustedRoot)
            {
                return true;
            }
            //Recommend to log this since the driver will throw an Host Not Avaiable Exception when the cert fails!
            //_logger.Error(string.Format("Error while validating a certificate {0}", sslPolicyErrors));
            Console.WriteLine("Error while validating a certificate {0}", sslPolicyErrors);
            return false;
        }

        [XmlIgnore]
        public IReconnectionPolicy ReconnectionPolicy
        {
            get { return _reconnectionPolicy; }
            set { _reconnectionPolicy = value; }
        }

        [XmlIgnore]
        public IRetryPolicy RetryPolicy
        {
            get { return _retryPolicy; }
            set { _retryPolicy = value; }
        }

        public static IRetryPolicy CreateRetryPolicy(RetryPolicyConfig retryPolicyConfig)
        {
            if (retryPolicyConfig == null)
            {
                return new DefaultRetryPolicy();
            }
            if (retryPolicyConfig is DefaultRetryPolicyConfig)
            {
                return new DefaultRetryPolicy();
            }
            if (retryPolicyConfig is DowngradingConsistencyRetryPolicyConfig)
            {
                return DowngradingConsistencyRetryPolicy.Instance;
            }
            if (retryPolicyConfig is FallthroughRetryPolicyConfig)
            {
                return FallthroughRetryPolicy.Instance;
            }
            if (retryPolicyConfig is LoggingRetryPolicyConfig)
            {
                var loggingRetryPolicyConfig = (LoggingRetryPolicyConfig)retryPolicyConfig;
                return new LoggingRetryPolicy(CreateRetryPolicy(loggingRetryPolicyConfig.RetryPolicy));
            }
            string errMsg = string.Format("IRetryPolicy of type {0} is not supported", retryPolicyConfig.GetType());
            var notSupportedException = new NotSupportedException(errMsg);
            throw notSupportedException;
        }


        [XmlElement("RetryPolicy", IsNullable = true)]
        public RetryPolicyContainer RetryPolicyContainerValue
        {
            get
            {
                return _retryPolicycontainer;
            }
            set
            {
                _retryPolicycontainer = value;
                RetryPolicy = CreateRetryPolicy(value.RetryPolicyConfigValue);
            }
        }

        [XmlElement("Keyspace", IsNullable = false)]
        public string Keyspace
        {
            get { return _keyspace; }
            set { _keyspace = value; }
        }

        [XmlElement("SolrPageSize", IsNullable=true)]
        public int? SolrPageSize
        {
            get
            {
                if (_solrPageSize == null)
                {
                    return 1000;
                }
                return _solrPageSize;
            }
            set
            {
                _solrPageSize = value;
            }
        }

        public bool ShouldSerializePort()
        {
            return Port.HasValue;
        }

        public void ResetPort()
        {
            Port = DefaultPort;
        }

        public bool ShouldSerializeCompressionType()
        {
            return CompressionType.HasValue;
        }

        public bool ShouldSerializeSslEnabled()
        {
            return SslEnabled.HasValue;
        }

        [XmlElement("QueryOptions", IsNullable = true)]
        public QueryOptionsConfig QueryOptionsConfig
        {
            get
            { return _queryOptionsConfig; }
            set
            {
                _queryOptionsConfig = value;
                if (_queryOptionsConfig != null)
                {
                    _queryOptions = new QueryOptions();
                    if (_queryOptionsConfig.ConsistencyLevel.HasValue)
                        _queryOptions.SetConsistencyLevel(_queryOptionsConfig.ConsistencyLevel.Value);
                    if (_queryOptionsConfig.SerialConsistencyLevel.HasValue)
                        _queryOptions.SetSerialConsistencyLevel(_queryOptionsConfig.SerialConsistencyLevel.Value);
                    if (_queryOptionsConfig.PageSize.HasValue)
                        _queryOptions.SetPageSize(_queryOptionsConfig.PageSize.Value);
                }
            }
        }

        [XmlIgnore]
        public QueryOptions QueryOptions
        {
            get { return _queryOptions; }
            set { _queryOptions = value; }
        }


    }




    #region RetryPolicy
    [Serializable]
    public class RetryPolicyContainer
    {

        [XmlElement(ElementName = "DefaultRetryPolicy", Type = typeof(DefaultRetryPolicyConfig))]
        [XmlElement(ElementName = "DowngradingConsistencyRetryPolicy", Type = typeof(DowngradingConsistencyRetryPolicyConfig))]
        [XmlElement(ElementName = "FallthroughRetryPolicy", Type = typeof(FallthroughRetryPolicyConfig))]
        [XmlElement(ElementName = "LoggingRetryPolicy", Type = typeof(LoggingRetryPolicyConfig))]
        public RetryPolicyConfig RetryPolicyConfigValue { get; set; }
    }

    [Serializable]
    public class RetryPolicyConfig
    {

    }

    [Serializable]
    public class DefaultRetryPolicyConfig : RetryPolicyConfig
    {

    }

    [Serializable]
    public class DowngradingConsistencyRetryPolicyConfig : RetryPolicyConfig
    {

    }

    [Serializable]
    public class FallthroughRetryPolicyConfig : RetryPolicyConfig
    {

    }

    [Serializable]
    public class LoggingRetryPolicyConfig : RetryPolicyConfig
    {
        [XmlElement(ElementName = "DefaultRetryPolicy", Type = typeof(DefaultRetryPolicyConfig))]
        [XmlElement(ElementName = "DowngradingConsistencyRetryPolicy", Type = typeof(DowngradingConsistencyRetryPolicyConfig))]
        [XmlElement(ElementName = "FallthroughRetryPolicy", Type = typeof(FallthroughRetryPolicyConfig))]
        public RetryPolicyConfig RetryPolicy { get; set; }
    }

    #endregion

    #region LoadBalancingPolicy
    [Serializable]
    public class LoadBalancingPolicyConfig
    {

    }

    [Serializable]
    public class TokenAwarePolicyConfig : LoadBalancingPolicyConfig
    {
        [XmlElement(ElementName = "RoundRobinPolicy", Type = typeof(RoundRobinPolicyConfig))]
        [XmlElement(ElementName = "DCAwareRoundRobinPolicy", Type = typeof(DcAwareRoundRobinPolicyConfig))]
        public LoadBalancingPolicyConfig ChildPolicy { get; set; }
    }

    [Serializable]
    public class RetryLoadBalancingPolicyConfig : LoadBalancingPolicyConfig
    {

        [XmlElement(ElementName = "RoundRobinPolicy", Type = typeof(RoundRobinPolicyConfig))]
        [XmlElement(ElementName = "DCAwareRoundRobinPolicy", Type = typeof(DcAwareRoundRobinPolicyConfig))]
        [XmlElement(ElementName = "TokenAwarePolicy", Type = typeof(TokenAwarePolicyConfig))]
        public LoadBalancingPolicyConfig LoadBalancingPolicy { get; set; }

        [XmlElement(ElementName = "ConstanstReconnectionPolicy", Type = typeof(ConstantReconnectionPolicyConfig))]
        [XmlElement(ElementName = "ExponentialReconnectionPolicy", Type = typeof(ExponentialReconnectionPolicyConfig))]
        [XmlElement(ElementName = "FixedReconnectionPolicy", Type = typeof(FixedReconnectionPolicyConfig))]
        public ReconnectionPolicyConfig ReconnectionPolicyConfig { get; set; }
    }

    [Serializable]
    public class RoundRobinPolicyConfig : LoadBalancingPolicyConfig
    {


    }

    [Serializable]
    public class DcAwareRoundRobinPolicyConfig : LoadBalancingPolicyConfig
    {
        [XmlElement("LocalDc", Type = typeof(string), IsNullable = true)]
        public string LocalDc { get; set; }

        [XmlElement("UsedHostsPerRemoteDc", Type = typeof(int?), IsNullable = true)]
        public int? UsedHostsPerRemoteDc { get; set; }

        public bool UsedHostsPerRemoteDcPort()
        {
            return UsedHostsPerRemoteDc.HasValue;
        }
    }

    #endregion


    #region ReconnectionPolicy
    [Serializable]
    public class ReconnectionPolicyConfig
    {


    }

    [Serializable]
    public class ConstantReconnectionPolicyConfig : ReconnectionPolicyConfig
    {
        [XmlElement("ConstantDelayMs", Type = typeof(long), IsNullable = false)]
        public long ConstantDelayMs { get; set; }
    }

    [Serializable]
    public class ExponentialReconnectionPolicyConfig : ReconnectionPolicyConfig
    {
        [XmlElement("BaseDelayMs", Type = typeof(long), IsNullable = false)]
        public long BaseDelayMs { get; set; }

        [XmlElement("MaxDelayMs", Type = typeof(long), IsNullable = false)]
        public long MaxDelayMs { get; set; }
    }

    [Serializable]
    public class FixedReconnectionPolicyConfig : ReconnectionPolicyConfig
    {
        private string _delaysString;
        private long[] _delays;

        [XmlIgnore]
        public long[] Delays
        {
            get { return _delays; }
            set
            {
                _delays = value;
                _delaysString = string.Join(",", _delays);
            }
        }

        [XmlElement("Delays", Type = typeof(string), IsNullable = false)]
        public string DelaysString
        {
            get { return _delaysString; }
            set
            {
                _delaysString = value;
                _delays = (_delaysString ?? "").Split(',').Select(long.Parse).ToArray();
            }
        }

    }

    #endregion

    [Serializable]
    public class PoolingOptionsConfig
    {
        private int? _coreConnectionsForLocal;// = DefaultCorePoolLocal;
        private int? _coreConnectionsForRemote;// = DefaultCorePoolRemote;

        private int? _maxConnectionsForLocal;// = DefaultMaxPoolLocal;
        private int? _maxConnectionsForRemote;// = DefaultMaxPoolRemote;
        private int? _maxSimultaneousRequestsForLocal;// = DefaultMaxRequests;
        private int? _maxSimultaneousRequestsForRemote;// = DefaultMaxRequests;
        private int? _minSimultaneousRequestsForLocal;// = DefaultMinRequests;
        private int? _minSimultaneousRequestsForRemote;// = DefaultMinRequests;
        private int? _heartBeatInterval;

        [XmlElement("CoreConnectionsForLocal", IsNullable = true)]
        public int? CoreConnectionsForLocal
        {
            get { return _coreConnectionsForLocal; }
            set { _coreConnectionsForLocal = value; }
        }

        [XmlElement("CoreConnectionsForRemote", IsNullable = true)]
        public int? CoreConnectionsForRemote
        {
            get { return _coreConnectionsForRemote; }
            set { _coreConnectionsForRemote = value; }
        }

        [XmlElement("MaxConnectionsForLocal", IsNullable = true)]
        public int? MaxConnectionsForLocal
        {
            get { return _maxConnectionsForLocal; }
            set { _maxConnectionsForLocal = value; }
        }

        [XmlElement("MaxConnectionsForRemote", IsNullable = true)]
        public int? MaxConnectionsForRemote
        {
            get { return _maxConnectionsForRemote; }
            set { _maxConnectionsForRemote = value; }
        }

        [XmlElement("MaxSimultaneousRequestsForLocal", IsNullable = true)]
        public int? MaxSimultaneousRequestsForLocal
        {
            get { return _maxSimultaneousRequestsForLocal; }
            set { _maxSimultaneousRequestsForLocal = value; }
        }

        [XmlElement("MaxSimultaneousRequestsForRemote", IsNullable = true)]
        public int? MaxSimultaneousRequestsForRemote
        {
            get { return _maxSimultaneousRequestsForRemote; }
            set { _maxSimultaneousRequestsForRemote = value; }
        }

        [XmlElement("MinSimultaneousRequestsForLocal", IsNullable = true)]
        public int? MinSimultaneousRequestsForLocal
        {
            get { return _minSimultaneousRequestsForLocal; }
            set { _minSimultaneousRequestsForLocal = value; }
        }

        [XmlElement("MinSimultaneousRequestsForRemote", IsNullable = true)]
        public int? MinSimultaneousRequestsForRemote
        {
            get { return _minSimultaneousRequestsForRemote; }
            set { _minSimultaneousRequestsForRemote = value; }
        }

        [XmlElement("HeartBeatInterval", IsNullable = true)]
        public int? HeartBeatInterval
        {
            get { return _heartBeatInterval; }
            set { _heartBeatInterval = value; }
        }
    }


    [Serializable]
    public class SocketOptionsConfig
    {
        [XmlElement("ConnectTimeoutMillis", IsNullable = true)]
        public int? ConnectTimeoutMillis { get; set; }

        [XmlElement("KeepAlive", IsNullable = true)]
        public bool? KeepAlive { get; set; }

        [XmlElement("ReceiveBufferSize", IsNullable = true)]
        public int? ReceiveBufferSize { get; set; }

        [XmlElement("ReuseAddress", IsNullable = true)]
        public bool? ReuseAddress { get; set; }

        [XmlElement("SendBufferSize", IsNullable = true)]
        public int? SendBufferSize { get; set; }

        [XmlElement("SoLinger", IsNullable = true)]
        public int? SoLinger { get; set; }

        [XmlElement("TcpNoDelay", IsNullable = true)]
        public bool? TcpNoDelay { get; set; }

        [XmlElement("UseStreamMode", IsNullable = true)]
        public bool? UseStreamMode { get; set; }

        [XmlElement("ReadTimeoutMillis", IsNullable = true)]
        public int? ReadTimeoutMillis { get; set; }

        [XmlElement("DefunctReadTimeoutThreshold", IsNullable = true)]
        public int? DefunctReadTimeoutThreshold { get; set; }
    }

    [Serializable]
    public class SslOptionsConfig
    {
        private SslProtocols? _sslProtocol;
        private bool? _checkCertificateRevocation;
        private List<CertificateConfig> _certificateConfigs = new List<CertificateConfig>();

        [XmlElement("SslProtocols", IsNullable = true)]
        public SslProtocols? SslProtocols
        {
            get { return _sslProtocol; }
            set { _sslProtocol = value; }
        }

        [XmlElement("CheckCertificateRevocation", IsNullable = true)]
        public bool? CheckCertificateRevocation
        {
            get { return _checkCertificateRevocation; }
            set { _checkCertificateRevocation = value; }
        }
        [XmlArray("Certificates")]
        [XmlArrayItem("Certificate")]
        public List<CertificateConfig> Certificates
        {
            get { return _certificateConfigs; }
            set { _certificateConfigs = value; }
        }
        }

    [Serializable]
    public class AuthenticationConfig
    {
        [XmlAttribute("provider")]
        public string Provider { get; set; }

        [XmlElement("Username", IsNullable = true)]
        public string UserName { get; set; }

        [XmlElement("Password", IsNullable = true)]
        public string Password { get; set; }

        [XmlElement("Domain", IsNullable = true)]
        public string Domain { get; set; }

        [XmlElement("Principal", IsNullable = true)]
        public string Principal { get; set; }
    }

    [Serializable]
    public class CertificateConfig
    {
        [XmlElement("FilePath", IsNullable = true)]
        public string FilePath { get; set; }

        [XmlElement("Password", IsNullable = true)]
        public string Password { get; set; }

        [XmlElement("RawData", IsNullable = true)]
        public string RawData { get; set; }

    }

    [Serializable]
    public class QueryOptionsConfig
    {
        [XmlElement("ConsistencyLevel", IsNullable = true)]
        public ConsistencyLevel? ConsistencyLevel { get; set; }

        [XmlElement("SerialConsistencyLevel", IsNullable = true)]
        public ConsistencyLevel? SerialConsistencyLevel { get; set; }

        [XmlElement("PageSize", Type = typeof(int?), IsNullable = true)]
        public int? PageSize { get; set; }
    }

    #region StatementOptions
    [Serializable]
    public class StatementOptions : IStatementOptions
    {

        [XmlElement("DataManipulation", IsNullable = true)]
        public DataManipulation DataManipulation { get; set; }

        [XmlElement("Queries", IsNullable = true)]
        public Queries Queries { get; set; }
    }

    [Serializable]

    public class DataManipulation
    {
        [XmlElement("Upsert", IsNullable = true)]
        public DataManipulationOption Upsert { get; set; }

        [XmlElement("Delete", IsNullable = true)]
        public DataManipulationOption Delete { get; set; }

        [XmlElement("Batch", IsNullable = true)]
        public BatchOption Batch { get; set; }
    }

    [Serializable]
    public class Queries
    {
        [XmlElement("Select", IsNullable = true)]
        public StatementOption Select { get; set; }

        [XmlElement("RangeScanSelect", IsNullable = true)]
        public StatementOption RangeScanSelect { get; set; }
    }

    [Serializable]
    public class DataManipulationOption
    {
        [XmlElement("Timestamp", Type = typeof(DateTimeKind?), IsNullable = true)]
        public DateTimeKind? DateTimeOffset { get; set; }

        [XmlElement("EnableTracing", IsNullable = true)]
        public bool? EnableTracing { get; set; }

        [XmlElement("ConsistencyLevel", IsNullable = true)]

        public ConsistencyLevel? ConsistencyLevel { get; set; }

        [XmlIgnore]
        private RetryPolicyConfig _retryPolicyConfigValue;

        [XmlElement(ElementName = "DefaultRetryPolicy", Type = typeof(DefaultRetryPolicyConfig))]
        [XmlElement(ElementName = "DowngradingConsistencyRetryPolicy", Type = typeof(DowngradingConsistencyRetryPolicyConfig))]
        [XmlElement(ElementName = "FallthroughRetryPolicy", Type = typeof(FallthroughRetryPolicyConfig))]
        [XmlElement(ElementName = "LoggingRetryPolicy", Type = typeof(LoggingRetryPolicyConfig))]
        public RetryPolicyConfig RetryPolicyConfigValue
        {
            get { return _retryPolicyConfigValue; }
            set
            {
                _retryPolicyConfigValue = value;
                RetryPolicy = ClusterConfig.CreateRetryPolicy(_retryPolicyConfigValue);
            }}

        [XmlIgnore]
        public IRetryPolicy RetryPolicy { get; set; }

    }


    [Serializable]
    public class BatchOption : DataManipulationOption
    {
        private const int DefaultBatchSizeThreshold = 5012;
        private const int DefaultBatchCountThreshold = 1000;

        private int? _batchSizeThreshold;
        private int? _batchCountThreshold;

        [XmlElement("BatchType", Type = typeof(BatchType?), IsNullable = true)]
        public BatchType? BatchType { get; set; }

        [XmlElement("BatchSizeThreshold", Type = typeof(int))]
        public int? BatchSizeThreshold
        {
            get
            {
                return !_batchSizeThreshold.HasValue ? DefaultBatchSizeThreshold : _batchSizeThreshold;
            }
            set { _batchSizeThreshold = value; }
        }

        [XmlElement("BatchCountThreshold", Type = typeof(int))]
        public int? BatchCountThreshold
        {
            get
            {
                return !_batchCountThreshold.HasValue ? DefaultBatchCountThreshold : _batchCountThreshold;
            }
            set { _batchCountThreshold = value; }
        }
    }

    [Serializable]
    public class StatementOption : QueryOptionsConfig
    {
        [XmlElement("Timestamp", Type = typeof(DateTimeKind?), IsNullable = true)]
        public DateTimeKind? DateTimeOffset { get; set; }

        [XmlElement("EnableTracing", IsNullable = true)]
        public bool? EnableTracing { get; set; }

        [XmlIgnore]
        private RetryPolicyConfig _retryPolicyConfigValue;

        [XmlElement(ElementName = "DefaultRetryPolicy", Type = typeof(DefaultRetryPolicyConfig))]
        [XmlElement(ElementName = "DowngradingConsistencyRetryPolicy", Type = typeof(DowngradingConsistencyRetryPolicyConfig))]
        [XmlElement(ElementName = "FallthroughRetryPolicy", Type = typeof(FallthroughRetryPolicyConfig))]
        [XmlElement(ElementName = "LoggingRetryPolicy", Type = typeof(LoggingRetryPolicyConfig))]
        public RetryPolicyConfig RetryPolicyConfigValue
        {
            get { return _retryPolicyConfigValue; }
            set
            {
                _retryPolicyConfigValue = value;
                RetryPolicy = ClusterConfig.CreateRetryPolicy(_retryPolicyConfigValue);
            }
        }

        [XmlIgnore]
        public IRetryPolicy RetryPolicy { get; set; }
    }
    #endregion
}