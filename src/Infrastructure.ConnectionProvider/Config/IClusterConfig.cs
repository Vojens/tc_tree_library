﻿using System.Collections.Generic;
using Cassandra;

namespace Infrastructure.ConnectionProvider.Config
{
    public interface IClusterConfig
    {
        bool? IsMultiNode { get; set; }
        string DataCenterName { get; set; }
        List<string> ContactPoints { get; set; }
        int? Port { get; set; }
        CompressionType? CompressionType { get; set; }
        bool? DisableRowSetBuffering { get; set; }
        PoolingOptions PoolingOptions { get; set; }
        PoolingOptionsConfig PoolingOptionsConfig { get; set; }
        SocketOptions SocketOptions { get; set; }
        SocketOptionsConfig SocketOptionsConfig { get; set; }
        ILoadBalancingPolicy LoadBalancingPolicy { get; set; }
        LoadBalancingPolicyConfig LoadBalancingPolicyConfig { get; set; }
        ReconnectionPolicyConfig ReconnectionPolicyConfig { get; set; }
        bool? SslEnabled { get; set; }
        SSLOptions SslOptions { get; set; }
        SslOptionsConfig SslOptionsConfig { get; set; }
        IReconnectionPolicy ReconnectionPolicy { get; set; }
        IRetryPolicy RetryPolicy { get; set; }
        RetryPolicyContainer RetryPolicyContainerValue { get; set; }
        string Keyspace { get; set; }
        QueryOptionsConfig QueryOptionsConfig { get; set; }
        QueryOptions QueryOptions { get; set; }
        bool ShouldSerializePort();
        bool ShouldSerializeCompressionType();
        bool ShouldSerializeSslEnabled();
        int? QueryTimeout { get; set; }
        int? SolrPageSize { get; set; }
        AuthenticationConfig AuthenticationConfig { get; set; }
        IAuthProvider AuthProvider { get; set; }
    }
}
