﻿namespace Infrastructure.ConnectionProvider.Config
{
    public interface IStatementOptions
    {
        DataManipulation DataManipulation { get; set; }
        Queries Queries { get; set; }
    }
}
