﻿using System;
using Cassandra;

namespace Infrastructure.ConnectionProvider
{
    public class BoundStatementDecorator : BoundStatement, IStatement
    {
        //private readonly String _cqlQueryName, _cqlQuery;
        public PreparedStatementDecorator PreparedStatementDecorator { get; protected set; }
        public BoundStatement BoundStatement { get; protected set; }
        public BoundStatementDecorator(PreparedStatementDecorator preparedStatementDecorator, BoundStatement boundStatement)
            : base(preparedStatementDecorator.PreparedStatement)
        {
            PreparedStatementDecorator = preparedStatementDecorator;
            BoundStatement = boundStatement;
        }

        internal BoundStatementDecorator(BoundStatement boundStatement)
        {
            BoundStatement = boundStatement;
        }

        public BoundStatementDecorator(PreparedStatementDecorator preparedStatementDecorator)
        : base(preparedStatementDecorator.PreparedStatement)
        {
            PreparedStatementDecorator = preparedStatementDecorator;
            BoundStatement = new BoundStatement(preparedStatementDecorator.PreparedStatement);
        }

        public new bool AutoPage
        {
            get
            {
                return BoundStatement.AutoPage;
            }
        }

        public new ConsistencyLevel? ConsistencyLevel
        {
            get { return ConsistencyLevel; }
        }

        public new IStatement DisableTracing()
        {
            return BoundStatement.DisableTracing();
        }

        public new IStatement EnableTracing(bool enable = true)
        {
            return BoundStatement.EnableTracing(enable);
        }

        public new bool IsTracing
        {
            get { return BoundStatement.IsTracing; }
        }

        public new int PageSize
        {
            get { return BoundStatement.PageSize; }
        }

        public new byte[] PagingState
        {
            get { return BoundStatement.PagingState; }
        }

        public new object[] QueryValues
        {
            get
            {
                return BoundStatement.QueryValues;
            }
        }

        public new DateTimeOffset? Timestamp
        {
            get { return BoundStatement.Timestamp; }
        }

        public new IRetryPolicy RetryPolicy
        {
            get { return BoundStatement.RetryPolicy; }
        }

        public override RoutingKey RoutingKey
        {
            get { return BoundStatement.RoutingKey; }
        }

        public new ConsistencyLevel SerialConsistencyLevel
        {
            get { return BoundStatement.SerialConsistencyLevel; }
        }

        public new IStatement SetAutoPage(bool autoPage)
        {
            return BoundStatement.SetAutoPage(autoPage);
        }

        public new IStatement SetConsistencyLevel(ConsistencyLevel? consistency)
        {
            return BoundStatement.SetConsistencyLevel(consistency);
        }

        public new IStatement SetPageSize(int pageSize)
        {
            return BoundStatement.SetPageSize(pageSize);
        }

        public new IStatement SetPagingState(byte[] pagingState)
        {
            return BoundStatement.SetPagingState(pagingState);
        }

        public new IStatement SetRetryPolicy(IRetryPolicy policy)
        {
            return BoundStatement.SetRetryPolicy(policy);
        }

        public new IStatement SetSerialConsistencyLevel(ConsistencyLevel serialConsistency)
        {
            return BoundStatement.SetSerialConsistencyLevel(serialConsistency);
        }

        public new IStatement SetTimestamp(DateTimeOffset value)
        {
            return BoundStatement.SetTimestamp(value);
        }

        public new bool SkipMetadata
        {
            get { return BoundStatement.SkipMetadata; }
        }

        //from BoundStatement


        /// <summary>
        ///  Set the routing key for this query. This method allows to manually
        ///  provide a routing key for this BoundStatement. It is thus optional since the routing
        ///  key is only an hint for token aware load balancing policy but is never
        ///  mandatory.
        /// </summary>
        /// <param name="routingKeyComponents"> the raw (binary) values to compose the routing key.</param>
        public new BoundStatementDecorator SetRoutingKey(params RoutingKey[] routingKeyComponents)
        {
            BoundStatement.SetRoutingKey(routingKeyComponents);
            return this;
        }
    }
}
