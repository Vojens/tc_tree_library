﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ConnectionProvider.Util
{
    internal class Helper
    {
        internal static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }

        internal static string MakeErrorMessage(Dictionary<IPEndPoint, Exception> errors)
        {
            var aggregatedErrors = errors.Aggregate(new StringBuilder(),
                (sb, pair) => sb.AppendFormat("- {0}, {1}{2}", pair.Key.ToString(), FlattenException(pair.Value),Environment.NewLine),
                sb => sb.ToString());
            return string.Format("None of the hosts tried for query are available. Tried: {0}{1}", Environment.NewLine, aggregatedErrors);
        }
    }
}
