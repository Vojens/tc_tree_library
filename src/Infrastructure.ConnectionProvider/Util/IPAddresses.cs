﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace Infrastructure.ConnectionProvider.Util
{
    public class IpAddresses
    {
        public static bool GetResolvedConnecionIpAddress(string serverNameOrUrl, out IPAddress resolvedIpAddress)
        {
            var isResolved = false;
            IPAddress resolvIp = null;
            try
            {
                if (!IPAddress.TryParse(serverNameOrUrl, out resolvIp))
                {
                    var hostEntry = Dns.GetHostEntry(serverNameOrUrl);

                    if (hostEntry != null && hostEntry.AddressList != null && hostEntry.AddressList.Length > 0)
                    {
                        if (hostEntry.AddressList.Length == 1)
                        {
                            resolvIp = hostEntry.AddressList[0];
                            isResolved = true;
                        }
                        else
                        {
                            foreach (var var in hostEntry.AddressList.Where(var => var.AddressFamily == AddressFamily.InterNetwork))
                            {
                                resolvIp = var;
                                isResolved = true;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    isResolved = true;
                }
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                resolvedIpAddress = resolvIp;
            }

            return isResolved;
        }
    }
}
