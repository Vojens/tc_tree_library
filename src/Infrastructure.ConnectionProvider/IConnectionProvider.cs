﻿using System;
using Cassandra;
using Cassandra.Mapping;
using Infrastructure.ConnectionProvider.Config;

namespace Infrastructure.ConnectionProvider
{
    public interface IConnectionProvider : IDisposable
    {
        ISessionAdapter Session { get; }
        void Connect();

        void Reconnect();

        new void Dispose();

        Metadata Metadata { get; }
        
        IConnectionConfig ConnectionConfig { get; }

        CqlQueryOptions GenerateCqlQueryOptions(StatementOptions statementOptions);

        CqlQueryOptions GetCqlQueryOptions(StatementOptions statementOptions);

        Cluster Cluster { get; }
    }

    public enum StatementOptions
    {
        Upsert = 1,
        Delete = 2,
        Batch = 3,
        Select = 4,
        RangeScanSelect = 5
    }
}
