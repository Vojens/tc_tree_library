﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Cassandra;

namespace Infrastructure.ConnectionProvider.Exceptions
{
    public class AdvanceNoHostAvailableException : DriverException
    {
        public AdvanceNoHostAvailableException(string message) : base(message)
        {
        }

        public AdvanceNoHostAvailableException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public AdvanceNoHostAvailableException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
