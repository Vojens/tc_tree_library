﻿using System;
using System.Configuration;
using Infrastructure.ConnectionProvider.Config;

namespace Infrastructure.ConnectionProvider
{
    public class ConfigProvider
    {
        public static readonly string ConfigPathSuffix = "Config.xml";

        public static CassandraConfig GetCassandraConfig(string configType)
        {
            string basePath = ConfigurationManager.AppSettings["ConfigBasePath"];
            if (string.IsNullOrEmpty(basePath))
            {
                throw new NullReferenceException("ConfigBasePath is empty");
            }
            return CassandraConfig.CreateCassandraConfig(string.Format("{0}\\{1}{2}", basePath.TrimEnd('\\'), configType, ConfigPathSuffix));
        }
    }
}
