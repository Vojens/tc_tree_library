﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using Cassandra;
using Cassandra.Mapping;
using Infrastructure.ConnectionProvider.Config;
using Infrastructure.ConnectionProvider.Exceptions;
using Infrastructure.ConnectionProvider.Util;

namespace Infrastructure.ConnectionProvider
{
    public class CassandraConnectionProvider : IConnectionProvider
    {
        private static readonly Logger Logger = new Logger(typeof(CassandraConnectionProvider));
        public Cluster Cluster { get; private set; }

        protected CassandraConfig Config;

        public CassandraConnectionProvider(CassandraConfig config)
        {
            Config = config;
            ConnectionConfig = config;
            InitCluster();
        }

        public CassandraConnectionProvider(string configType)
        {
            Config = ConfigProvider.GetCassandraConfig(configType);
            ConnectionConfig = Config;
            InitCluster();
        }

        public ISessionAdapter Session
        {
            get; 
            protected set; 
        }

        public void Connect() {
            
            try
            {
                var session = Cluster.Connect();
                Session = new SessionAdapter(this, session);
            }
            catch (NoHostAvailableException e)
            {
                Console.WriteLine(Helper.MakeErrorMessage(e.Errors));
                Logger.Error(e);
                if (e.Errors == null)
                {
                    throw;
                }
                throw new AdvanceNoHostAvailableException(Helper.MakeErrorMessage(e.Errors), e);
            }
            catch (Exception e) {

                Logger.Error(e);
                throw;
            }
            
        }

        

        public void InitCluster()
        {
            var clusterConfig = Config.Cluster;
            if (clusterConfig.ContactPoints == null || clusterConfig.ContactPoints.Count < 1)
            {
                throw new ArgumentException("at least one server is required");
            }
            var builder = Cluster.Builder();
            var ipAddressList = new List<IPAddress>(clusterConfig.ContactPoints.Count);
            foreach (var contactPoint in clusterConfig.ContactPoints)
            {
                IPAddress ipAddress;
                if (IpAddresses.GetResolvedConnecionIpAddress(contactPoint, out ipAddress))
                {
                    ipAddressList.Add(ipAddress);
                }
                else
                {
                    throw new ArgumentException(contactPoint + " is not a valid IP Address");
                }
            }
            
            Contract.Assert(ipAddressList.Count > 0, "at least one valid ip address is required");
            Debug.Assert(clusterConfig.Port != null, "_config.Cluster.Port != null");
            builder.AddContactPoints(ipAddressList.ToArray()).WithPort(clusterConfig.Port.Value);

            if (clusterConfig.CompressionType != null)
            {
                builder.WithCompression(clusterConfig.CompressionType.Value);
            }
            if (clusterConfig.PoolingOptions != null)
            {
                builder.WithPoolingOptions(clusterConfig.PoolingOptions);
            }

            if (clusterConfig.QueryTimeout.HasValue)
            {
                builder.WithQueryTimeout(clusterConfig.QueryTimeout.Value);
            }

            if (clusterConfig.SocketOptions != null)
            {
                builder.WithSocketOptions(clusterConfig.SocketOptions);
            }

            if (clusterConfig.AuthProvider != null)
            {
                builder.WithAuthProvider(clusterConfig.AuthProvider);
            }

            if (clusterConfig.LoadBalancingPolicy != null)
            {
                builder.WithLoadBalancingPolicy(clusterConfig.LoadBalancingPolicy);
            }

            if (clusterConfig.ReconnectionPolicy != null)
            {
                builder.WithReconnectionPolicy(clusterConfig.ReconnectionPolicy);
            }

            if (clusterConfig.RetryPolicy != null)
            {
                builder.WithRetryPolicy(new LoggingRetryPolicy(DowngradingConsistencyRetryPolicy.Instance));
            }

            if (clusterConfig.ShouldSerializeSslEnabled() && clusterConfig.SslEnabled.Value)
            {
                if (clusterConfig.SslOptions == null)
                {
                    builder.WithSSL();
                }
                else
                {
                    builder.WithSSL(clusterConfig.SslOptions);
                }
            }
            if (clusterConfig.QueryOptions != null)
            {
                builder.WithQueryOptions(clusterConfig.QueryOptions);
            }
            if (clusterConfig.DisableRowSetBuffering.HasValue && clusterConfig.DisableRowSetBuffering.Value)
            {
                builder.WithoutRowSetBuffering();
            }
            if (Config.Cluster.Keyspace != null && Config.Cluster.Keyspace.Any())
            {
                builder.WithDefaultKeyspace(Config.Cluster.Keyspace);
            }

            Cluster = builder.Build();

            var statementOptions = Enum.GetValues(typeof(StatementOptions)).Cast<StatementOptions>();
            foreach (var statementOption in statementOptions)
            {
                if (!_statementOptionsDic.ContainsKey(statementOption))
                    _statementOptionsDic.Add(statementOption, GenerateCqlQueryOptions(statementOption));
            }
        }


        public void Reconnect() {
            Dispose();
            Connect();
        }


        public void Dispose() {
            if (Session != null) {
                try {
                    if (!Session.IsDisposed)
                    {
                        Session.Dispose();    
                    }
                    Session = null;
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            if (Cluster == null) return;
            try {
                Cluster.Shutdown();
                Cluster.Dispose();
                Cluster = null;
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public Metadata Metadata {
            get { return Cluster.Metadata; }
        }

        public IConnectionConfig ConnectionConfig { get; private set; }

        public CqlQueryOptions GenerateCqlQueryOptions(StatementOptions statementOptions)
        {
            var cqlQueryOptions = CqlQueryOptions.New();
            switch (statementOptions)
            {
                    case StatementOptions.Upsert:
                        var upsert = Config.StatementOptions.DataManipulation.Upsert;
                        if (upsert == null)
                        {
                            return cqlQueryOptions;
                        }
                    cqlQueryOptions.SetConsistencyLevel(upsert.ConsistencyLevel ?? ConnectionConfig.Cluster.QueryOptions.GetConsistencyLevel());

                    if (upsert.EnableTracing.HasValue)
                        {
                            if (upsert.EnableTracing.Value)
                            {
                                cqlQueryOptions.EnableTracing();
                            }
                            else
                            {
                                cqlQueryOptions.DisableTracing();
                            }
                        }
                        else
                        {
                            cqlQueryOptions.DisableTracing();
                        }
                    cqlQueryOptions.SetRetryPolicy(upsert.RetryPolicy ?? Cluster.Configuration.Policies.RetryPolicy);
                    break;

                    case StatementOptions.Delete:
                        var delete = Config.StatementOptions.DataManipulation.Delete;
                        if (delete == null)
                        {
                            return cqlQueryOptions;
                        }
                    cqlQueryOptions.SetConsistencyLevel(delete.ConsistencyLevel ?? ConnectionConfig.Cluster.QueryOptions.GetConsistencyLevel());

                    if (delete.EnableTracing.HasValue)
                        {
                            if (delete.EnableTracing.Value)
                            {
                                cqlQueryOptions.EnableTracing();
                            }
                            else
                            {
                                cqlQueryOptions.DisableTracing();
                            }
                        }
                        else
                        {
                            cqlQueryOptions.DisableTracing();
                        }
                    cqlQueryOptions.SetRetryPolicy(delete.RetryPolicy ?? Cluster.Configuration.Policies.RetryPolicy);
                    break;

                    case StatementOptions.Batch:
                    var batch = Config.StatementOptions.DataManipulation.Batch;
                    if (batch == null)
                    {
                        return cqlQueryOptions;
                    }
                    cqlQueryOptions.SetConsistencyLevel(batch.ConsistencyLevel ?? ConnectionConfig.Cluster.QueryOptions.GetConsistencyLevel());
                    if (batch.EnableTracing.HasValue)
                    {
                        if (batch.EnableTracing.Value)
                        {
                            cqlQueryOptions.EnableTracing();
                        }
                        else
                        {
                            cqlQueryOptions.DisableTracing();
                        }
                    }
                    else
                    {
                        cqlQueryOptions.DisableTracing();
                    }
                    cqlQueryOptions.SetRetryPolicy(batch.RetryPolicy ?? Cluster.Configuration.Policies.RetryPolicy);
//                    if (batch.BatchType.HasValue)
//                    {
//                        cqlQueryOptions.
//                    }

                    break;

                    case StatementOptions.Select:
                        var select = Config.StatementOptions.Queries.Select;
                        if (select == null)
                        {
                            return cqlQueryOptions;
                        }
                    cqlQueryOptions.SetConsistencyLevel(select.ConsistencyLevel ?? ConnectionConfig.Cluster.QueryOptions.GetConsistencyLevel());

                    if (select.EnableTracing.HasValue)
                        {
                            if (select.EnableTracing.Value)
                            {
                                cqlQueryOptions.EnableTracing();
                            }
                            else
                            {
                                cqlQueryOptions.DisableTracing();
                            }
                        }
                        else
                        {
                            cqlQueryOptions.DisableTracing();
                        }
                    cqlQueryOptions.SetRetryPolicy(select.RetryPolicyConfigValue != null ? select.RetryPolicy : Cluster.Configuration.Policies.RetryPolicy);
                    
                    if (select.SerialConsistencyLevel.HasValue)
                        {
                            cqlQueryOptions.SetSerialConsistencyLevel(select.SerialConsistencyLevel.Value);
                        }
                        else
                        {
                            cqlQueryOptions.SetConsistencyLevel(
                                Cluster.Configuration.QueryOptions.GetSerialConsistencyLevel());
                        }
                    cqlQueryOptions.SetPageSize(select.PageSize ?? Cluster.Configuration.QueryOptions.GetPageSize());
                    break;

                    case StatementOptions.RangeScanSelect:

                    var rangeScanSelect = Config.StatementOptions.Queries.RangeScanSelect;
                    if (rangeScanSelect == null)
                    {
                        return cqlQueryOptions;
                    }
                    cqlQueryOptions.SetConsistencyLevel(rangeScanSelect.ConsistencyLevel ?? ConnectionConfig.Cluster.QueryOptions.GetConsistencyLevel());
                    
                    if (rangeScanSelect.EnableTracing.HasValue)
                    {
                        if (rangeScanSelect.EnableTracing.Value)
                        {
                            cqlQueryOptions.EnableTracing();
                        }
                        else
                        {
                            cqlQueryOptions.DisableTracing();
                        }
                    }
                    else
                    {
                        cqlQueryOptions.DisableTracing();
                    }
                    cqlQueryOptions.SetRetryPolicy(rangeScanSelect.RetryPolicyConfigValue != null ? rangeScanSelect.RetryPolicy : Cluster.Configuration.Policies.RetryPolicy);
                    
                    if (rangeScanSelect.SerialConsistencyLevel.HasValue)
                    {
                        cqlQueryOptions.SetSerialConsistencyLevel(rangeScanSelect.SerialConsistencyLevel.Value);
                    }
                    else
                    {
                        cqlQueryOptions.SetConsistencyLevel(
                            Cluster.Configuration.QueryOptions.GetSerialConsistencyLevel());
                    }
                    cqlQueryOptions.SetPageSize(rangeScanSelect.PageSize ?? Cluster.Configuration.QueryOptions.GetPageSize());
                    break;
            }

            return cqlQueryOptions;
        }

        private readonly Dictionary<StatementOptions, CqlQueryOptions> _statementOptionsDic = new Dictionary<StatementOptions, CqlQueryOptions>();

        public CqlQueryOptions GetCqlQueryOptions(StatementOptions statementOptions)
        {
            return _statementOptionsDic[statementOptions];
        }

        public ISessionAdapter CreateSession()
        {
            var sessionAdaptee = Cluster.Connect();
            var session = new SessionAdapter(this, sessionAdaptee);

            if (Config.Cluster.Keyspace != null && Config.Cluster.Keyspace.Any())
            {
                session.ChangeKeyspace(Config.Cluster.Keyspace);
            }

            return session;
        }
    }
}
