﻿using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Cassandra;
using Cassandra.Data.Linq;

namespace Infrastructure.ConnectionProvider.Extensions
{
    public static class CqlQueryExtensions
    {
        public static Task<IEnumerable<T>> ConfigureAndExecuteAsync<T>(this CqlQuery<T> cqlQuery, IConnectionProvider connectionProvider)
        {
            var select = connectionProvider.ConnectionConfig.StatementOptions.Queries.Select;
            cqlQuery.SetConsistencyLevel(@select.ConsistencyLevel);
            return cqlQuery.ExecuteAsync();
        }

        public static Task<IEnumerable<T>> ConfigureAndExecuteAsync<T>(this CqlQuery<T> query, StatementOptions statementOptions, IConnectionProvider connectionProvider)
        {
            return query.Configure(statementOptions, connectionProvider).ExecuteAsync();
        }

        public static Task<IEnumerable<T>> ConfigureAndExecuteAsync<T>(this CqlQueryBase<T> query, StatementOptions statementOptions, IConnectionProvider connectionProvider)
        {
            return query.Configure(statementOptions,connectionProvider).ExecuteAsync();
        }

        public static Task<T> ConfigureAndExecuteAsync<T>(this CqlScalar<T> query, StatementOptions statementOptions, IConnectionProvider connectionProvider)
        {
            return query.Configure(statementOptions, connectionProvider).ExecuteAsync();
        }

        public static Task<T> ConfigureAndExecuteAsync<T>(this CqlQuerySingleElement<T> query, StatementOptions statementOptions, IConnectionProvider connectionProvider)
        {
            return query.Configure(statementOptions, connectionProvider).ExecuteAsync();
        }

        public static Task<RowSet> ConfigureAndExecuteAsync<T>(this CqlInsert<T> query, IConnectionProvider connectionProvider)
        {
            return query.Configure(StatementOptions.Upsert, connectionProvider).ExecuteAsync();
        }

        public static Task<T> ConfigureAndExecuteAsync<T>(this CqlQuerySingleElement<T> query, IConnectionProvider connectionProvider)
        {
            return query.Configure(StatementOptions.Select, connectionProvider).ExecuteAsync();
        }

        public static Task ConfigureAndExecuteAsync(this CqlDelete query, IConnectionProvider connectionProvider)
        {
            return query.Configure(StatementOptions.Delete, connectionProvider).ExecuteAsync();
        }
        
        public static T Configure<T>(this T statement, StatementOptions statementOptions, IConnectionProvider connectionProvider) where T : IStatement
        {
            switch (statementOptions)
            {
                case StatementOptions.Select:
                    var selectStatementOption = connectionProvider.ConnectionConfig.StatementOptions.Queries.Select;
                    if (selectStatementOption.ConsistencyLevel.HasValue)
                    {
                        statement.SetConsistencyLevel(selectStatementOption.ConsistencyLevel.Value);
                    }
                    if (selectStatementOption.SerialConsistencyLevel.HasValue)
                    {
                        statement.SetSerialConsistencyLevel(selectStatementOption.SerialConsistencyLevel.Value);
                    }
                    if (selectStatementOption.PageSize.HasValue)
                    {
                        statement.SetPageSize(selectStatementOption.PageSize.Value);
                    }
                    if (selectStatementOption.EnableTracing.HasValue)
                    {
                        statement.EnableTracing(selectStatementOption.EnableTracing.Value);
                    }
                    if (selectStatementOption.RetryPolicy != null)
                    {
                        statement.SetRetryPolicy(selectStatementOption.RetryPolicy);
                    }
                    if (selectStatementOption.DateTimeOffset.HasValue)
                    {
                        var dateTimeKind = selectStatementOption.DateTimeOffset.Value;
                        switch (dateTimeKind)
                        {
                            case DateTimeKind.Local:
                                statement.SetTimestamp(DateTimeOffset.Now);
                                break;
                            case DateTimeKind.Utc:
                                statement.SetTimestamp(DateTimeOffset.UtcNow);
                                break;
                        }
                    }
                    break;
                case StatementOptions.RangeScanSelect:
                    {
                        var rangeScanSelectStatementOption = connectionProvider.ConnectionConfig.StatementOptions.Queries.RangeScanSelect;
                        if (rangeScanSelectStatementOption.ConsistencyLevel.HasValue)
                        {
                            statement.SetConsistencyLevel(rangeScanSelectStatementOption.ConsistencyLevel.Value);
                        }
                        if (rangeScanSelectStatementOption.SerialConsistencyLevel.HasValue)
                        {
                            statement.SetSerialConsistencyLevel(rangeScanSelectStatementOption.SerialConsistencyLevel.Value);
                        }
                        if (rangeScanSelectStatementOption.PageSize.HasValue)
                        {
                            statement.SetPageSize(rangeScanSelectStatementOption.PageSize.Value);
                        }
                        if (rangeScanSelectStatementOption.EnableTracing.HasValue)
                        {
                            statement.EnableTracing(rangeScanSelectStatementOption.EnableTracing.Value);
                        }
                        if (rangeScanSelectStatementOption.RetryPolicy != null)
                        {
                            statement.SetRetryPolicy(rangeScanSelectStatementOption.RetryPolicy);
                        }
                        if (rangeScanSelectStatementOption.DateTimeOffset.HasValue)
                        {
                            var dateTimeKind = rangeScanSelectStatementOption.DateTimeOffset.Value;
                            switch (dateTimeKind)
                            {
                                case DateTimeKind.Local:
                                    statement.SetTimestamp(DateTimeOffset.Now);
                                    break;
                                case DateTimeKind.Utc:
                                    statement.SetTimestamp(DateTimeOffset.UtcNow);
                                    break;
                            }
                        }
                    }
                    break;
                case StatementOptions.Upsert:
                    {
                        var rangeScanSelectStatementOption = connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Upsert;
                        if (rangeScanSelectStatementOption.ConsistencyLevel.HasValue)
                        {
                            statement.SetConsistencyLevel(rangeScanSelectStatementOption.ConsistencyLevel.Value);
                        }

                        if (rangeScanSelectStatementOption.EnableTracing.HasValue)
                        {
                            statement.EnableTracing(rangeScanSelectStatementOption.EnableTracing.Value);
                        }
                        if (rangeScanSelectStatementOption.RetryPolicy != null)
                        {
                            statement.SetRetryPolicy(rangeScanSelectStatementOption.RetryPolicy);
                        }
                        if (rangeScanSelectStatementOption.DateTimeOffset.HasValue)
                        {
                            var dateTimeKind = rangeScanSelectStatementOption.DateTimeOffset.Value;
                            switch (dateTimeKind)
                            {
                                case DateTimeKind.Local:
                                    statement.SetTimestamp(DateTimeOffset.Now);
                                    break;
                                case DateTimeKind.Utc:
                                    statement.SetTimestamp(DateTimeOffset.UtcNow);
                                    break;
                            }
                        }
                    }
                    break;
                case StatementOptions.Delete:
                    var deleteStatementOption = connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Delete;
                    if (deleteStatementOption.ConsistencyLevel.HasValue)
                    {
                        statement.SetConsistencyLevel(deleteStatementOption.ConsistencyLevel.Value);
                    }

                    if (deleteStatementOption.EnableTracing.HasValue)
                    {
                        statement.EnableTracing(deleteStatementOption.EnableTracing.Value);
                    }
                    if (deleteStatementOption.RetryPolicy != null)
                    {
                        statement.SetRetryPolicy(deleteStatementOption.RetryPolicy);
                    }
                    if (deleteStatementOption.DateTimeOffset.HasValue)
                    {
                        var dateTimeKind = deleteStatementOption.DateTimeOffset.Value;
                        switch (dateTimeKind)
                        {
                            case DateTimeKind.Local:
                                statement.SetTimestamp(DateTimeOffset.Now);
                                break;
                            case DateTimeKind.Utc:
                                statement.SetTimestamp(DateTimeOffset.UtcNow);
                                break;
                        }
                    }
                    break;
                case StatementOptions.Batch:
                    var batchStatementOption = connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch;
                    if (batchStatementOption.ConsistencyLevel.HasValue)
                    {
                        statement.SetConsistencyLevel(batchStatementOption.ConsistencyLevel.Value);
                    }

                    if (batchStatementOption.EnableTracing.HasValue)
                    {
                        statement.EnableTracing(batchStatementOption.EnableTracing.Value);
                    }
                    if (batchStatementOption.RetryPolicy != null)
                    {
                        statement.SetRetryPolicy(batchStatementOption.RetryPolicy);
                    }
                    if (batchStatementOption.DateTimeOffset.HasValue)
                    {
                        var dateTimeKind = batchStatementOption.DateTimeOffset.Value;
                        switch (dateTimeKind)
                        {
                            case DateTimeKind.Local:
                                statement.SetTimestamp(DateTimeOffset.Now);
                                break;
                            case DateTimeKind.Utc:
                                statement.SetTimestamp(DateTimeOffset.UtcNow);
                                break;
                        }
                    }
                    if (batchStatementOption.BatchType.HasValue)
                    {
                        var batchStatement = statement as BatchStatement;
                        if (batchStatement != null)
                        {
                            batchStatement.SetBatchType(batchStatementOption.BatchType.Value);
                        }
                    }
                    break;
            }
            return statement;
        }

        public static IObservable<T> ReadLimit<T>(this CqlQuery<T> cqlQuery, IConnectionProvider connectionProvider, ConsistencyLevel consistencyLevel, int limit = 500)
        {
            return Observable.Create<T>(async o =>
            {
                try
                {
                    cqlQuery.Configure(StatementOptions.RangeScanSelect, connectionProvider);
                    cqlQuery.SetConsistencyLevel(consistencyLevel);
                    var cappedLimit = Math.Min(cqlQuery.PageSize, limit);
                    var enumerable = await cqlQuery.Take(cappedLimit).ExecuteAsync().ConfigureAwait(false);
                    foreach (var entry in enumerable)
                    {
                        o.OnNext(entry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public static IObservable<T> Read<T>(this CqlQuery<T> cqlQuery, IConnectionProvider connectionProvider)
        {
            return Observable.Create<T>(async o =>
            {
                try
                {
                    //cqlQuery.SetConsistencyLevel(ConsistencyLevel.LocalOne);
                    var enumerable = await cqlQuery.ExecutePagedAsync().ConfigureAwait(false);
                    
                    foreach (var entry in enumerable)
                    {
                        o.OnNext(entry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }

        public static IObservable<T> Read<T>(this CqlQuery<T> cqlQuery, IConnectionProvider connectionProvider, ConsistencyLevel consistencyLevel)
        {
            return Observable.Create<T>(async o =>
            {
                try
                {
                    cqlQuery.Configure(StatementOptions.RangeScanSelect, connectionProvider);
                    cqlQuery.SetConsistencyLevel(consistencyLevel);
                    var enumerable = await cqlQuery.ExecuteAsync().ConfigureAwait(false);
                    foreach (var entry in enumerable)
                    {
                        o.OnNext(entry);
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }
    }
}
