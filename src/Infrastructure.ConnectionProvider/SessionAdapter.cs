﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Disposables;
using System.Threading.Tasks;
using Cassandra;
using Infrastructure.ConnectionProvider.Extensions;
using Infrastructure.ConnectionProvider.Util;
using System.Reactive.Linq;
using Cassandra.Data.Linq;
using CqlQueryExtensions = Infrastructure.ConnectionProvider.Extensions.CqlQueryExtensions;

namespace Infrastructure.ConnectionProvider
{
    public interface ISessionAdapter
    {
        void Dispose();
        void ChangeKeyspace(string keyspaceName);
        void CreateKeyspace(string keyspaceName, Dictionary<string, string> replication = null, bool durableWrites = true);
        void CreateKeyspaceIfNotExists(string keyspaceName, Dictionary<string, string> replication = null, bool durableWrites = true);
        void DeleteKeyspace(string keyspaceName);
        void DeleteKeyspaceIfExists(string keyspaceName);
        Task ExecuteBatchAsync(ICollection<Statement> statements);
        Task ExecuteBatchAsync(RoutingKey[] routingKeys, ICollection<BoundStatement> statements, BatchStatement bs = null, int countMax = 100);
        Task ExecuteBatchAsync(RoutingKey[] routingKeys, ICollection<BoundStatementDecorator> statements, BatchStatementDecorator bs = null, int countMax = 100);
        Task ExecuteBatchAsync(ICollection<BoundStatement> statements, BatchStatement bs = null, int countMax = 1000);
        Task ExecuteBatchAsync(ICollection<Statement> statements, BatchStatement bs);
        Task ExecuteBatchAsync(ICollection<BoundStatementDecorator> statements, BatchStatement bs = null, int countMax = 1000);
        Task ExecuteBatchAsync(ICollection<Statement> statements, Func<BatchStatement, StatementOptions, IConnectionProvider, BatchStatement> batchConfiguratorFunc);
        Task<RowSet> ExecuteAsync(IStatement statement);
        Task<RowSet> ExecuteAsync(IStatement statement, StatementOptions statementOptions);

        RowSet Execute(IStatement statement, StatementOptions statementOptions);
        Task<PreparedStatement> PrepareAsync(string cqlQuery);
        Task<PreparedStatement> PrepareAsync(string cqlQueryName, string cqlQuery);
        int BinaryProtocolVersion { get; }
        ICluster Cluster { get; }
        bool IsDisposed { get; }
        string Keyspace { get; }
        UdtMappingDefinitions UserDefinedTypes { get; }
        ISession SessionAdaptee { get; }
        void InvalidateCache();

        Table<T> GetTable<T>();


        #region cassandara
        Task<RowSet> ExecuteNonQueryAsync(IStatement statement);
        Task<T> ExecuteScalarAsync<T>(IStatement statement);
        Task<T> ExecuteScalarAsync<T>(string cql, params object[] args);
        Task<List<T>> ExecuteListScalarAsync<T>(IStatement statement);
        IObservable<T> ExecuteScalarAsObservable<T>(IStatement statement);
        T ExecuteScalar<T>(IStatement statement);
        Task<List<T>> ExecuteListScalarAsync<T>(string cql, params object[] args);
        #endregion
    }

    public class SessionAdapter : ISessionAdapter
    {
        private readonly ISession _session;
        //private readonly CqlLogger _logger = new CqlLogger(typeof(SessionAdapter));
        private readonly IConnectionProvider _connectionProvider;
        private int? _batchSizeThreshold;
        private int? _batchCountThreshold;
        public readonly ConcurrentDictionary<string, PreparedStatement> PreparedStatementCache;
        public readonly ConcurrentDictionary<string, string> QueryCache;

        public SessionAdapter(IConnectionProvider connectionProvider, ISession sessionAdaptee)
        {
            _connectionProvider = connectionProvider;
            _session = sessionAdaptee;
            _batchSizeThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchSizeThreshold;
            _batchCountThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchCountThreshold;
            PreparedStatementCache = new ConcurrentDictionary<string, PreparedStatement>();
            QueryCache = new ConcurrentDictionary<string, string>();
        }

        public ISession SessionAdaptee
        {
            get
            {
                return _session;
            }
        }

        public void InvalidateCache()
        {
            PreparedStatementCache.Clear();
            QueryCache.Clear();
        }

        public void Dispose()
        {
            _session.Dispose();
        }

        public Table<T> GetTable<T>()
        {
            return _session.GetTable<T>();
        } 

        public void ChangeKeyspace(string keyspaceName)
        {
            _session.ChangeKeyspace(keyspaceName);
        }

        public void CreateKeyspace(string keyspaceName, Dictionary<string, string> replication = null, bool durableWrites = true)
        {
            throw new InvalidOperationException("creating a keyspace is not allowed");
        }

        public void CreateKeyspaceIfNotExists(string keyspaceName, Dictionary<string, string> replication = null, bool durableWrites = true)
        {
            throw new InvalidOperationException("creating a keyspace is not allowed");
        }

        public void DeleteKeyspace(string keyspaceName)
        {
            throw new InvalidOperationException("deleting a keyspace is not allowed");
        }

        public void DeleteKeyspaceIfExists(string keyspaceName)
        {
            throw new InvalidOperationException("deleting a keyspace is not allowed");
        }

        public Task ExecuteBatchAsync(ICollection<Statement> statements)
        {
            if (_connectionProvider != null)
            {
                _batchSizeThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchSizeThreshold;
                _batchCountThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchCountThreshold;
            }
            var size = 0;
            var count = 0;
            var bs = new BatchStatementDecorator();
            var tasks = new List<Task>();
            foreach (var statement in statements)
            {
                var bsd = statement as BoundStatementDecorator;
                
                IStatement iStatement = bsd != null ? bsd.BoundStatement : statement;
                var sizeofStat = GetStatementSize(iStatement);
                if (sizeofStat >= _batchSizeThreshold.Value)
                {
                    tasks.Add(ExecuteAsync(iStatement));
                }
                else
                {
                    bs.Add(bsd ?? statement);
                    size += sizeofStat;
                    if (!(size >= _batchSizeThreshold) && !(count >= _batchCountThreshold))
                        continue;
                    tasks.Add(ExecuteAsync(bs));
                    size = count = 0;
                    bs = new BatchStatementDecorator();
                }
                
            }
            if (!bs.IsEmpty)
            {
                tasks.Add(ExecuteAsync(bs));
            }
            return Task.WhenAll(tasks);
        }


        public Task ExecuteBatchAsync(RoutingKey[] routingKeys, ICollection<BoundStatement> statements, BatchStatement bs = null, int countMax = 1000)
        {
            var size = 0;
            var count = 0;
            if (bs == null)
            {
                bs = new BatchStatement();
            }
            bs.SetRoutingKey(routingKeys);
            var tasks = new List<Task>();
            foreach (var statement in statements)
            {
                var sizeofStat = GetStatementSize(statement);
                if (sizeofStat >= _batchSizeThreshold.Value)
                {
                    tasks.Add(ExecuteAsync(statement));
                }
                else
                {
                    bs.Add(statement);
                    //int sizeofStat = Marshal.SizeOf(statement.QueryValues);
                    size += sizeofStat;
                    if (!(size >= _batchSizeThreshold) && !(count >= _batchCountThreshold))
                        continue;
                    tasks.Add(ExecuteAsync(bs));
                    size = count = 0;
                    var tempBs = new BatchStatementDecorator();
                    tempBs.SetRoutingKey(routingKeys);
                    tempBs.SetBatchType(bs.BatchType);
                    tempBs.SetConsistencyLevel(bs.ConsistencyLevel);
                    tempBs.SetRetryPolicy(bs.RetryPolicy);
                    //tempBs.SetSerialConsistencyLevel(bs.SerialConsistencyLevel);  //DO NOT SET THE SERIAL CONSISTENCY LEVEL
                    bs = tempBs;
                }
                
            }
            if (!bs.IsEmpty)
            {
                tasks.Add(ExecuteAsync(bs));
            }
            return Task.WhenAll(tasks);
        }

        private int GetStatementSize(IStatement statement)
        {
            return statement == null ? 0 : statement.QueryValues.Where(queryValue => queryValue != null).Sum(queryValue => TypeHelper.SizeOf(queryValue));
        }

        public async Task ExecuteBatchAsync(ICollection<BoundStatement> statements, BatchStatement bs = null, int countMax = 1000)
        {
            var size = 0;
            var count = 0;
            if (bs == null)
            {
                bs = new BatchStatement();
            }
            var tasks = new List<Task>();
            foreach (var statement in statements)
            {
                bs.Add(statement);
                var sizeofStat = statement.QueryValues.Where(queryValue => queryValue != null).Sum(queryValue => TypeHelper.SizeOf(queryValue));
                //int sizeofStat = Marshal.SizeOf(statement.QueryValues);
                size += sizeofStat;
                if (!(size >= _batchSizeThreshold) && !(count >= _batchCountThreshold))
                    continue;
                tasks.Add(ExecuteAsync(bs));
                size = count = 0;
                var tempBs = new BatchStatementDecorator();
                tempBs.SetBatchType(bs.BatchType);
                tempBs.SetConsistencyLevel(bs.ConsistencyLevel);
                tempBs.SetRetryPolicy(bs.RetryPolicy);
                //tempBs.SetSerialConsistencyLevel(bs.SerialConsistencyLevel);  //DO NOT SET THE SERIAL CONSISTENCY LEVEL
                bs = tempBs;
            }
            if (!bs.IsEmpty)
            {
                tasks.Add(ExecuteAsync(bs));
            }
            await tasks.Await().ConfigureAwait(false);
        }

        public async Task ExecuteBatchAsync(ICollection<Statement> statements, BatchStatement bs)
        {
            var size = 0;
            var count = 0;
            if (bs == null)
            {
                bs = new BatchStatement();
            }
            var tasks = new List<Task>();
            foreach (var statement in statements)
            {
                bs.Add(statement);
                var sizeofStat = statement.QueryValues.Where(queryValue => queryValue != null).Sum(queryValue => TypeHelper.SizeOf(queryValue));
                //int sizeofStat = Marshal.SizeOf(statement.QueryValues);
                size += sizeofStat;
                if (!(size >= _batchSizeThreshold) && !(count >= _batchCountThreshold))
                    continue;
                tasks.Add(ExecuteAsync(bs));
                size = count = 0;
                var tempBs = new BatchStatementDecorator();
                tempBs.SetBatchType(bs.BatchType);
                tempBs.SetConsistencyLevel(bs.ConsistencyLevel);
                tempBs.SetRetryPolicy(bs.RetryPolicy);
                //tempBs.SetSerialConsistencyLevel(bs.SerialConsistencyLevel);  //DO NOT SET THE SERIAL CONSISTENCY LEVEL
                bs = tempBs;
            }
            if (!bs.IsEmpty)
            {
                tasks.Add(ExecuteAsync(bs));
            }
            await tasks.Await().ConfigureAwait(false);
        }

        public Task ExecuteBatchAsync(ICollection<BoundStatementDecorator> statements, BatchStatement bs = null, int countMax = 1000)
        {
            return ExecuteBatchAsync(statements.Select(x => x.BoundStatement).ToList(), bs, countMax);
        }

        public Task ExecuteBatchAsync(ICollection<Statement> statements, Func<BatchStatement, StatementOptions, IConnectionProvider, BatchStatement> batchConfiguratorFunc)
        {
            if (_connectionProvider != null)
            {
                _batchSizeThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchSizeThreshold;
                _batchCountThreshold = _connectionProvider.ConnectionConfig.StatementOptions.DataManipulation.Batch.BatchCountThreshold;
            }
            var size = 0;
            var count = 0;
            var bs = new BatchStatement();
            if (batchConfiguratorFunc == null)
            {
                batchConfiguratorFunc = CqlQueryExtensions.Configure;
            }
            //bs.Configure(StatementOptions.Batch);
            var tasks = new List<Task>();
            bs = batchConfiguratorFunc(bs, StatementOptions.Batch, _connectionProvider);
            foreach (var statement in statements)
            {

                var sizeofStat = statement.QueryValues.Where(queryValue => queryValue != null).Sum(queryValue => TypeHelper.SizeOf(queryValue));
                if (sizeofStat >= _batchSizeThreshold || (sizeofStat + size) >= _batchSizeThreshold)
                {
                    tasks.Add(ExecuteAsync(statement, StatementOptions.Batch));
                }
                else
                {
                    bs.Add(statement);
                    size += sizeofStat;
                    if (size >= _batchSizeThreshold || count >= _batchCountThreshold)
                    //5kb -- default warning when hitting 5k https://issues.apache.org/jira/browse/CASSANDRA-6487
                    {
                        tasks.Add(ExecuteAsync(bs));
                        size = count = 0;
                        bs = new BatchStatement();
                        bs = batchConfiguratorFunc(bs, StatementOptions.Batch, _connectionProvider);
                    }
                }

            }
            if (!bs.IsEmpty)
            {
                tasks.Add(ExecuteAsync(bs));
            }
            return tasks.Await();
        }

        public async Task ExecuteBatchAsync(RoutingKey[] routingKeys, ICollection<BoundStatementDecorator> statements, BatchStatementDecorator bs = null, int countMax = 1000)
        {
            var size = 0;
            var count = 0;
            if (bs == null)
            {
                bs = new BatchStatementDecorator();
            }
            if (routingKeys != null)
                bs.SetRoutingKey(routingKeys);
            var tasks = new List<Task>();
            foreach (var statement in statements)
            {
                bs.Add(statement.BoundStatement);
                var sizeofStat = statement.QueryValues.Where(queryValue => queryValue != null).Sum(queryValue => TypeHelper.SizeOf(queryValue));
                //int sizeofStat = Marshal.SizeOf(statement.QueryValues);
                size += sizeofStat;
                if (!(size >= _batchSizeThreshold) && !(count >= _batchCountThreshold))
                    continue;
                tasks.Add(ExecuteAsync(bs));
                size = count = 0;
                var tempBs = new BatchStatementDecorator();
                tempBs.SetRoutingKey(routingKeys);
                tempBs.SetBatchType(bs.BatchType);
                tempBs.SetConsistencyLevel(bs.ConsistencyLevel);
                tempBs.SetRetryPolicy(bs.RetryPolicy);
                //tempBs.SetSerialConsistencyLevel(bs.SerialConsistencyLevel);  //DO NOT SET THE SERIAL CONSISTENCY LEVEL
                bs = tempBs;
            }
            if (!bs.IsEmpty)
            {
                tasks.Add(ExecuteAsync(bs));
            }
            await Task.WhenAll(tasks).ConfigureAwait(false);
        }

        public async Task<RowSet> ExecuteAsync(IStatement statement)
        {
            //var isPass = false;
            RowSet rowSet;
            var sw = Stopwatch.StartNew();
            try
            {
                var bsd = statement as BoundStatementDecorator;
                if (bsd != null)
                {
                    rowSet = await _session.ExecuteAsync(bsd.BoundStatement).ConfigureAwait(false);
                }
                else
                {
                    rowSet = await _session.ExecuteAsync(statement).ConfigureAwait(false);
                }

                //isPass = true;
            }
            finally
            {
                sw.Stop();
                //_logger.DebugIStatement(statement, isPass, sw.Elapsed);
            }
            return rowSet;
        }

        public RowSet Execute(IStatement statement)
        {
            //var isPass = false;
            RowSet rowSet;
            var sw = Stopwatch.StartNew();
            try
            {
                var bsd = statement as BoundStatementDecorator;
                if (bsd != null)
                {
                    rowSet = _session.Execute(bsd.BoundStatement);
                }
                else
                {
                    rowSet = _session.Execute(statement);
                }

                //isPass = true;
            }
            finally
            {
                sw.Stop();
                //_logger.DebugIStatement(statement, isPass, sw.Elapsed);
            }
            return rowSet;
        }

        public Task<RowSet> ExecuteAsync(IStatement statement, StatementOptions statementOptions)
        {
            return ExecuteAsync(statement.Configure(statementOptions, _connectionProvider));
        }

        public RowSet Execute(IStatement statement, StatementOptions statementOptions)
        {
            return Execute(statement.Configure(statementOptions, _connectionProvider));
        }

        public Task<PreparedStatement> PrepareAsync(string cqlQuery)
        {
            return PrepareAsync(cqlQuery, cqlQuery);
        }

        public async Task<PreparedStatement> PrepareAsync(string cqlQueryName, string cqlQuery)
        {
            PreparedStatement preparedStatementDecorator;
            if (PreparedStatementCache.TryGetValue(cqlQueryName, out preparedStatementDecorator))
            {
                return preparedStatementDecorator;
            }
            var preparedStatement = await _session.PrepareAsync(cqlQuery).ConfigureAwait(false);
            QueryCache.TryAdd(cqlQueryName, cqlQuery);
            if (!PreparedStatementCache.TryAdd(cqlQueryName, preparedStatement))
            {
                //throw new DuplicateNameException(String.Format("cql query with name {0} is available more than once in the configuration", cqlQueryName)); //there could be a race condition that causes this
                return preparedStatement;
            }
            return preparedStatement;
        }

        public int BinaryProtocolVersion
        {
            get { return _session.BinaryProtocolVersion; }
        }

        public ICluster Cluster
        {
            get { return _session.Cluster; }
        }

        public bool IsDisposed
        {
            get { return _session.IsDisposed; }
        }

        public string Keyspace
        {
            get { return _session.Keyspace; }
        }

        public UdtMappingDefinitions UserDefinedTypes
        {
            get { return _session.UserDefinedTypes; }
        }
        
        public async Task<T> ExecuteScalarAsync<T>(IStatement statement)
        {
            T result = default(T);
            RowSet rs = await _session.ExecuteAsync(statement).ConfigureAwait(false);

            if (rs != null)
            {
                foreach (Row r in rs)
                {
                    if (!r.IsNull(0))
                        result = r.GetValue<T>(0);
                    return result;
                }
            }

            return result;
        }

        public T ExecuteScalar<T>(IStatement statement)
        {
            T result = default(T);
            RowSet rs = _session.Execute(statement);

            if (rs != null)
            {
                foreach (Row r in rs)
                {
                    if (!r.IsNull(0))
                        result = r.GetValue<T>(0);
                    return result;
                }
            }
            return result;
        }

        public async Task<T> ExecuteScalarAsync<T>(string cql, params object[] args)
        {
            T result = default(T);
            SimpleStatement statement = new SimpleStatement(cql, args);
            RowSet rs = await _session.ExecuteAsync(statement).ConfigureAwait(false);

            if (rs != null)
            {
                foreach (Row r in rs)
                {
                    result = r.GetValue<T>(0);
                    return result;
                }
            }
            return result;
        }

        public async Task<RowSet> ExecuteNonQueryAsync(IStatement statement)
        {
            RowSet rowset = await _session.ExecuteAsync(statement).ConfigureAwait(false);
            return rowset;
        }

        public async Task<List<T>> ExecuteListScalarAsync<T>(IStatement statement)
        {
            List<T> list = new List<T>();
            RowSet rs = await _session.ExecuteAsync(statement).ConfigureAwait(false);

            if (rs != null)
            {
                foreach (Row r in rs)
                {
                    if (!r.IsNull(0))
                        list.Add(r.GetValue<T>(0));
                }
            }
            return list;
        }

        public IObservable<T> ExecuteScalarAsObservable<T>(IStatement statement)
        {
            return Observable.Create<T>(async o =>
            {
                try
                {
                    var rowSet = await _session.ExecuteAsync(statement).ConfigureAwait(false);

                    if (rowSet != null)
                    {
                        foreach (var r in rowSet)
                        {
                            if (!r.IsNull(0))
                                o.OnNext(r.GetValue<T>(0));
                        }
                    }
                    o.OnCompleted();
                }
                catch (Exception e)
                {
                    o.OnError(e);
                }
                return Disposable.Empty;
            });
        }
        public async Task<List<T>> ExecuteListScalarAsync<T>(string cql, params object[] args)
        {
            List<T> list = new List<T>();
            SimpleStatement statement = new SimpleStatement(cql, args);
            RowSet rs = await _session.ExecuteAsync(statement).ConfigureAwait(false);
            if (rs != null)
            {
                foreach (Row r in rs)
                {
                    if (!r.IsNull(0))
                        list.Add(r.GetValue<T>(0));
                }
            }
            return list;

        }
    }
}
