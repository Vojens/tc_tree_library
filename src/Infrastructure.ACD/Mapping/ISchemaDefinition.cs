﻿using Cassandra.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Mapping
{
    public interface ISchemaDefinition
    {
        Dictionary<string, PropertyMap> PropertyMaps { get; }

        /// <summary>
        /// The Type of the POCO.
        /// </summary>
        Type PocoType { get; }

        /// <summary>
        /// The name of the table to map the POCO to.
        /// </summary>
        string GetTableName { get; }

        /// <summary>
        /// The name of the keyspace where the table is defined.
        /// When the keyspace name is not null, the table name for the query generated will be fully qualified (ie: keyspace.tablename)
        /// </summary>
        string GetKeyspaceName { get; }

        /// <summary>
        /// Whether or not this POCO should only have columns explicitly defined mapped.
        /// </summary>
        bool GetExplicitColumns { get; }

        /// <summary>
        /// Gets the partition key columns of the table.
        /// </summary>
        string[] GetPartitionKeys { get; }

        /// <summary>
        /// Gets the clustering key columns of the table.
        /// </summary>
        Tuple<string, SortOrder>[] GetClusteringKeys { get; }


        string[] GetPrimaryKeys { get; }
        /// <summary>
        /// Determines if the queries generated using this definition should be case-sensitive
        /// </summary>
        bool IsCaseSensitive { get; }

        /// <summary>
        /// Determines if the table is declared with COMPACT STORAGE
        /// </summary>
        bool IsCompactStorage { get; }

        /// <summary>
        /// Determines that all queries generated for this table can be made allowing server side filtering
        /// </summary>
        bool IsAllowFiltering { get; }

        /// <summary>
        /// Gets a column definition for the given field on the POCO.
        /// </summary>
        IPropertyDefinition GetColumnDefinition(FieldInfo field);

        /// <summary>
        /// Gets a column definition for the given property on the POCO.
        /// </summary>
        IPropertyDefinition GetColumnDefinition(PropertyInfo property);

        IEnumerable<object> GetPartitionValues<TProc>(TProc proc);
        void SetPartitionValues<TProc>(TProc proc, IEnumerable<object> values);
        void SetColumnValue(string columnName, object obj, object value);
        void GetColumnValue<T>(string columnName, object obj);

        //        Map<T> ToMap<T>();
        ITypeDefinition Map { get; }
    }
}
