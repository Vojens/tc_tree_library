﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.ACD.Repository;

namespace Infrastructure.ACD.Mapping
{
    abstract class SchemaMappings
    {
        internal SchemaContainer SchemaContainer;

        protected SchemaMappings(SchemaContainer schemaContainer)
        {
            SchemaContainer = schemaContainer;
        }

        public SchemaMap<TProc> For<TProc>()
        {
            var schemaMap = new SchemaMap<TProc>();
            return schemaMap;
        }
    }
}
