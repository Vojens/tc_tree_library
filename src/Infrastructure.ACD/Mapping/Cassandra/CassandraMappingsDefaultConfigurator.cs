﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.ACD.Repository;

namespace Infrastructure.ACD.Mapping.Cassandra
{
    public class CassandraMappingsDefaultConfigurator
    {
        public static void Configure(SchemaContainer schemaContainer)
        {
            new AcdEntryMappings(schemaContainer);
            new AcdSetEntryMappings(schemaContainer);
            new AcdWorkflowEntryMappings(schemaContainer);
            new ResourceEntryMappings(schemaContainer);
            new ResourceMetadataEntryMappings(schemaContainer);
            new AcdAuditEntryMappings(schemaContainer);
        }
    }
}
