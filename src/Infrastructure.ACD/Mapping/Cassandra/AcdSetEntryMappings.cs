﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;

namespace Infrastructure.ACD.Mapping.Cassandra
{
    class AcdSetEntryMappings : SchemaMappings
    {
        public AcdSetEntryMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<AcdSetEntry>()
                    .TableName("acd_set")
                    .ExplicitColumns()
                    .PartitionKey(e => e.OutId, e => e.InId)
                    .ClusteringKey(e => e.Permission)
                    .ClusteringKey(e => e.PrincipalId)
                    .ClusteringKey(e => e.AcdId)
                    .Column(e => e.OutId, e => e.WithName("outid"))
                    .Column(e => e.InId, e => e.WithName("inid"))
                    .Column(e => e.Permission, e => e.WithName("perm"))
                    .Column(e => e.PrincipalId, e => e.WithName("idenid"))
                    .Column(e => e.AcdId, e => e.WithName("acdid"))
                    .Column(e => e.SolrQuery, e => e.WithName("solr_query")));
            SchemaContainer.AddSchemaMap(
                For<AcdCreatePermission>()
                .TableName("acd_create")
                .ExplicitColumns()
                .PartitionKey(e => e.ResourceId)
                .ClusteringKey(e => e.PrincipalId)
                .ClusteringKey(e => e.ResourceType)
                .ClusteringKey(e => e.AcdId)
                .Column(e => e.ResourceId, e => e.WithName("resid"))
                .Column(e => e.PrincipalId, e => e.WithName("idenid"))
                .Column(e => e.ResourceType, e => e.WithName("typ"))
                .Column(e => e.AcdId, e => e.WithName("acdid")));
        }
    }
}
