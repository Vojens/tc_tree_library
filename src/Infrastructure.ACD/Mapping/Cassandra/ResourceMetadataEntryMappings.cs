﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;

namespace Infrastructure.ACD.Mapping.Cassandra
{
    class ResourceMetadataEntryMappings : SchemaMappings
    {
        public ResourceMetadataEntryMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<ResourceMetadataEntry>()
                    .TableName("resource_metadata")
                    .ExplicitColumns()
                    .PartitionKey(e => e.ParentId, e => e.ResourceId)
                    .Column(e => e.ResourceId, e => e.WithName("inid"))
                    .Column(e => e.ParentId, e => e.WithName("outid"))
                    .Column(e => e.CreatedUserId, e => e.WithName("cusrid"))
                    .Column(e => e.Description, e => e.WithName("descr"))
                    .Column(e => e.Name, e => e.WithName("nm"))
                    .Column(e => e.Uri, e => e.WithName("uri"))
                    .Column(e => e.Type, e => e.WithName("typ"))
                    .Column(e => e.ResourceSet, e => e.WithName("rs"))
                    .Column(e => e.Workflow, e => e.WithName("wfnm"))
                    .Column(e => e.WorkflowState, e => e.WithName("wfstat"))
                    .Column(e => e.SolrQuery, e => e.WithName("solr_query")));
        }
    }
}
