﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;

namespace Infrastructure.ACD.Mapping.Cassandra
{
    class AcdAuditEntryMappings : SchemaMappings
    {
        public AcdAuditEntryMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<AuditEntry>()
                    .TableName("acd_audits")
                    .ExplicitColumns()
                    .PartitionKey(e => e.InId, e => e.OutId)
                    .ClusteringKey(e => e.AuditId)
                    .Column(e => e.InId, e => e.WithName("outid"))
                    .Column(e => e.OutId, e => e.WithName("inid"))
                    .Column(e => e.AuditId, e => e.WithName("auditid"))
                    .Column(e => e.State, e => e.WithName("acdstate").WithDbType<short>())
                    .Column(e => e.Action, e => e.WithName("action").WithDbType<short>())
                    .Column(e => e.ResourceName, e => e.WithName("resname"))
                    .Column(e => e.ResourceType, e => e.WithName("restype"))
                    .Column(e => e.DisplayText, e => e.WithName("dtext"))
                    .Column(e => e.ModifiedUsername, e => e.WithName("updatedby"))
                    .Column(e => e.Timestamp, e => e.WithName("timestamp"))
                    .Column(e => e.SolrQuery, e => e.WithName("solr_query")));

            SchemaContainer.AddSchemaMap(
                For<AuditDataEntry>()
                    .TableName("acd_audit_data")
                    .ExplicitColumns()
                    .PartitionKey(e => e.AuditId)
                    .Column(e => e.AuditId, e => e.WithName("auditid"))
                    .Column(e => e.DataOld, e => e.WithName("old"))
                    .Column(e => e.DataNew, e => e.WithName("new")));
        }
    }
}
