﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;

namespace Infrastructure.ACD.Mapping.Cassandra
{
    class AcdEntryMappings : SchemaMappings
    {
        public AcdEntryMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<AcdEntry>()
                    .TableName("acds")
                    .ExplicitColumns()
                    .PartitionKey(e => e.ResourceId)
                    .ClusteringKey(e => e.AcdId)
                    .Column(e => e.ResourceId, e => e.WithName("resid"))
                    .Column(e => e.AcdId, e => e.WithName("acdid"))
                    .Column(e => e.PrincipalId, e => e.WithName("idenid"))
                    .Column(e => e.PrincipalName, e => e.WithName("idennm"))
                    .Column(e => e.PrincipalType, e => e.WithName("identyp").WithDbType<short>())
                    .Column(e => e.OnChildCreate, e => e.WithName("oncrtch").WithDbType<short>())
                    .Column(e => e.ForceOnChildCreate, e => e.WithName("focrch"))
                    .Column(e => e.InheritedFrom, e => e.WithName("inhtfrm"))
                    .Column(e => e.ApplicableWorkflow, e => e.WithName("wfnm"))
                    .Column(e => e.ApplicableWorkflowState, e => e.WithName("wfstat"))
                    .Column(e => e.CreatedUserId, e => e.WithName("crusrid"))
                    .Column(e => e.CreatedUsername, e => e.WithName("crusrnm"))
                    .Column(e => e.CreatedTimestamp, e => e.WithName("crtime"))
                    .Column(e => e.ResourceSet, e => e.WithName("rs"))
                    .Column(e => e.ResourceType, e => e.WithName("typ"))
                    .Column(e => e.Permissions, e => e.WithName("perms"))
                    .Column(e => e.SolrQuery, e => e.WithName("solr_query")));
        }
    }
}
