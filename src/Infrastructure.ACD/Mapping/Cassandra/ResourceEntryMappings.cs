﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;

namespace Infrastructure.ACD.Mapping.Cassandra
{
    class ResourceEntryMappings : SchemaMappings
    {
        public ResourceEntryMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<ResourceEntry>()
                    .TableName("resource_set")
                    .ExplicitColumns()
                    .PartitionKey(e => e.ParentId, e => e.ResourceId)
                    .Column(e => e.ParentId, e => e.WithName("outid"))
                    .Column(e => e.ResourceId, e => e.WithName("inid"))
                    .Column(e => e.ResourceUri, e => e.WithName("inuri"))
                    .Column(e => e.ResourceType, e => e.WithName("typ"))
                    .Column(e => e.ResourceName, e => e.WithName("nm"))
                    .Column(e => e.SolrQuery, e => e.WithName("solr_query")));
        }
    }
}
