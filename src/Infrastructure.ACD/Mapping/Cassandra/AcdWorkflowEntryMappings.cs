﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;

namespace Infrastructure.ACD.Mapping.Cassandra
{
    class AcdWorkflowEntryMappings : SchemaMappings
    {
        public AcdWorkflowEntryMappings(SchemaContainer schemaContainer)
            : base(schemaContainer)
        {
            SchemaContainer.AddSchemaMap(
                For<AcdWorkflowEntry>()
                    .TableName("acd_workflows")
                    .ExplicitColumns()
                    .PartitionKey(e => e.ResourceId)
                    .ClusteringKey(e => e.AcdWorkflow)
                    .ClusteringKey(e => e.AcdWorkflowState)
                    .ClusteringKey(e => e.CreatedUserId)
                    .ClusteringKey(e => e.Id)
                    .Column(e => e.ResourceId, e => e.WithName("resid"))
                    .Column(e => e.DisplayText, e => e.WithName("dtext"))
                    .Column(e => e.AcdWorkflow, e => e.WithName("acdwf"))
                    .Column(e => e.AcdWorkflowState, e => e.WithName("acdwfst"))
                    .Column(e => e.CreatedUserId, e => e.WithName("crusrid"))
                    .Column(e => e.Id, e => e.WithName("wfid"))
                    .Column(e => e.Data, e => e.WithName("data"))
                    .Column(e => e.CreatedUsername, e => e.WithName("crusrnm"))
                    .Column(e => e.CreatedTimestamp, e => e.WithName("crtime"))
                    .Column(e => e.SolrQuery, e => e.WithName("solr_query")));
        }
    }
}
