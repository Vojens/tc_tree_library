﻿using Cassandra.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Mapping
{
    public interface IPropertyDefinition
    {
        MemberInfo MemberInfo { get; }

        /// <summary>
        /// The Type of the property or field (i.e. FieldInfo.FieldType or PropertyInfo.PropertyType).
        /// </summary>
        Type MemberInfoType { get; }

        /// <summary>
        /// The name of the column in the database that this property/field maps to.
        /// </summary>
        string ColumnName { get; }

        /// <summary>
        /// The data type of the column in C* for inserting/updating data.
        /// </summary>
        Type ColumnType { get; }

        /// <summary>
        /// Whether the property/field should be ignored when mapping.
        /// </summary>
        bool IsIgnore { get; }

        /// <summary>
        /// Whether or not this column has been explicitly defined (for use when TypeDefinition.ExplicitColumns is true).
        /// </summary>
        bool IsExplicitlyDefined { get; }

        /// <summary>
        /// Determines if there is a secondary index defined for this column
        /// </summary>
        bool IsSecondaryIndex { get; }

        /// <summary>
        /// Determines if this column is a counter column
        /// </summary>
        bool IsCounter { get; }

        /// <summary>
        /// Determines if this column is a static column
        /// </summary>
        bool IsStatic { get; }

        bool IsDistance { get; }

        Action<ColumnMap> ToColumnMap();
    }
}
