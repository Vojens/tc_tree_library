﻿using Cassandra.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Mapping
{
    public class SchemaMap<TPoco> : ISchemaDefinition
    {
        private readonly Type _pocoType;

        private string _tableName;
        private bool _explicitColumns;
        private bool _caseSensitive;

        private string[] _partitionKeyColumns;
        private MemberInfo[] _partitionKeyColumnMembers;
        private readonly List<Tuple<string, SortOrder>> _clusteringKeyColumns = new List<Tuple<string, SortOrder>>(0);
        private readonly List<Tuple<MemberInfo, SortOrder>> _clusteringKeyColumnMembers = new List<Tuple<MemberInfo, SortOrder>>(0);
        private bool _compactStorage;
        private string _keyspaceName;
        private readonly Dictionary<string, PropertyMap> _propertyMaps;
        private readonly Map<TPoco> _map;

        public Dictionary<string, PropertyMap> PropertyMaps
        {
            get { return _propertyMaps; }
        }

        public Type PocoType
        {
            get { return _pocoType; }
        }

        public string GetTableName
        {
            get { return _tableName; }
        }

        public string GetKeyspaceName
        {
            get { return _keyspaceName; }
        }

        public bool GetExplicitColumns
        {
            get { return _explicitColumns; }
        }

        public string[] GetPrimaryKeys
        {
            get
            {
                return
                    GetPartitionKeys.Union(
                        GetClusteringKeys.OrderBy(x => x.Item2).Select(x => x.Item1)).ToArray();
            }
        }

        public bool IsCaseSensitive
        {
            get { return _caseSensitive; }
        }

        public bool IsAllowFiltering
        {
            get { return false; }
        }

        public bool IsCompactStorage
        {
            get { return _compactStorage; }
        }

        public string[] GetPartitionKeys
        {
            get
            {
                // Use string column names if configured
                if (_partitionKeyColumns != null)
                    return _partitionKeyColumns;

                // If no MemberInfos available either, just bail
                if (_partitionKeyColumnMembers == null)
                    return null;

                // Get the column names from the members
                var columnNames = new string[_partitionKeyColumnMembers.Length];
                for (var index = 0; index < _partitionKeyColumnMembers.Length; index++)
                {
                    var memberInfo = _partitionKeyColumnMembers[index];
                    columnNames[index] = GetColumnName(memberInfo);
                }

                return columnNames;
            }
        }

        public Tuple<string, SortOrder>[] GetClusteringKeys
        {
            get
            {
                //Need to concat the clustering keys by name
                //Plus the one defined by member
                return _clusteringKeyColumns
                    .Concat(_clusteringKeyColumnMembers.Select(i => Tuple.Create(GetColumnName(i.Item1), i.Item2)))
                    .ToArray();
            }
        }

        /// <summary>
        /// Creates a new fluent mapping definition for POCOs of Type TPoco.
        /// </summary>
        public SchemaMap()
        {
            _pocoType = typeof(TPoco);
            _propertyMaps = new Dictionary<string, PropertyMap>();
            _map = new Map<TPoco>();
        }

        /// <summary>
        /// Specifies what table to map the POCO to.
        /// </summary>
        public SchemaMap<TPoco> TableName(string tableName)
        {
            if (string.IsNullOrWhiteSpace(tableName)) throw new ArgumentNullException("tableName");

            _tableName = tableName;
            _map.TableName(tableName);
            return this;
        }

        /// <summary>
        /// Specifies the partition key column names for the table using the order provided.
        /// </summary>
        public SchemaMap<TPoco> PartitionKey(params string[] columnNames)
        {
            if (columnNames == null) throw new ArgumentNullException("columnNames");
            if (columnNames.Length == 0) throw new ArgumentOutOfRangeException("columnNames", "Must specify at least one partition key column.");
            if (_partitionKeyColumnMembers != null) throw new InvalidOperationException("Partition key columns were already specified.");
            _partitionKeyColumns = columnNames;
            _map.PartitionKey(columnNames);
            return this;
        }

        /// <summary>
        /// Specifies the properties/fields on the POCO whose column names are the partition key for the table.
        /// </summary>
        public SchemaMap<TPoco> PartitionKey(params Expression<Func<TPoco, object>>[] columns)
        {
            if (columns == null) throw new ArgumentNullException("columns");
            if (columns.Length == 0) throw new ArgumentOutOfRangeException("columns", "Must specify at least one partition key column.");
            if (_partitionKeyColumns != null) throw new InvalidOperationException("Partition key column names were already specified, define multiple using invoking this method with multiple expressions.");

            // Validate we got property/field expressions
            var partitionKeyMemberInfo = new MemberInfo[columns.Length];
            for (var index = 0; index < columns.Length; index++)
            {
                // If expression is good, add it to the array we're building (GetPropertyOrField should throw on invalid)
                var memberInfo = GetPropertyOrField(columns[index]);
                partitionKeyMemberInfo[index] = memberInfo;
            }

            // All expressions were good, so track accordingly
            _partitionKeyColumnMembers = partitionKeyMemberInfo;
            _map.PartitionKey(columns);
            return this;
        }

        /// <summary>
        /// Specifies the clustering key column names for the table using the order provided.
        /// </summary>
        public SchemaMap<TPoco> ClusteringKey(params string[] columnNames)
        {
            if (columnNames == null) throw new ArgumentNullException("columnNames");
            if (columnNames.Length == 0) return this;
            _clusteringKeyColumns.AddRange(columnNames.Select(name => Tuple.Create(name, SortOrder.Unspecified)));
            _map.ClusteringKey(columnNames);
            return this;
        }

        /// <summary>
        /// Specifies the Clustering keys with the corresponding clustering order
        /// </summary>
        public SchemaMap<TPoco> ClusteringKey(params Tuple<string, SortOrder>[] columnNames)
        {
            if (columnNames == null) throw new ArgumentNullException("columnNames");
            if (columnNames.Length == 0) return this;
            //Allow multiple calls to clustering key
            _clusteringKeyColumns.AddRange(columnNames);
            _map.ClusteringKey(columnNames);
            return this;
        }

        /// <summary>
        /// Specifies a Clustering key with its clustering order
        /// </summary>
        /// <param name="column">Expression to select the property or the field</param>
        /// <param name="order">Clustering order</param>
        public SchemaMap<TPoco> ClusteringKey(Expression<Func<TPoco, object>> column, SortOrder order)
        {
            if (column == null) throw new ArgumentNullException("column");
            var memberInfo = GetPropertyOrField(column);
            _clusteringKeyColumnMembers.Add(Tuple.Create(memberInfo, order));
            _map.ClusteringKey(column, order);
            return this;
        }

        /// <summary>
        /// Specifies a Clustering key with unspecified order
        /// </summary>
        /// <param name="column">Expression to select the property or the field</param>
        public SchemaMap<TPoco> ClusteringKey(Expression<Func<TPoco, object>> column)
        {
            //_map.ClusteringKey(column); //DON'T SET THIS HERE AS THIS WOULD RESULT IN DUPLICATIONS
            return ClusteringKey(column, SortOrder.Unspecified);
        }

        /// <summary>
        /// Specifies that when mapping, we should only map columns that are explicitly defined here.  Use the Column method
        /// to define columns.
        /// </summary>
        public SchemaMap<TPoco> ExplicitColumns()
        {
            _explicitColumns = true;
            _map.ExplicitColumns();
            return this;
        }

        /// <summary>
        /// Specifies that when generating queries, the table and column names identifiers must be quoted. Defaults to false.
        /// </summary>
        /// <returns></returns>
        public SchemaMap<TPoco> CaseSensitive()
        {
            _caseSensitive = true;
            _map.CaseSensitive();
            return this;
        }

        /// <summary>
        /// Defines options for mapping the column specified.
        /// </summary>
        public SchemaMap<TPoco> Column<TProp>(Expression<Func<TPoco, TProp>> column, Action<PropertyMap> columnConfig)
        {
            if (column == null) throw new ArgumentNullException("column");
            if (columnConfig == null) throw new ArgumentNullException("columnConfig");

            var memberInfo = GetPropertyOrField(column);

            // Create the PropertyMap for the member if we haven't already
            PropertyMap propertyMap;
            if (_propertyMaps.TryGetValue(memberInfo.Name, out propertyMap) == false)
            {
                var info = memberInfo as PropertyInfo;
                var memberInfoType = info != null
                                          ? info.PropertyType
                                          : ((FieldInfo)memberInfo).FieldType;

                propertyMap = new PropertyMap(memberInfo, memberInfoType, true);
                _propertyMaps[memberInfo.Name] = propertyMap;
            }

            // Run the configuration action on the column map
            columnConfig(propertyMap);
            //            var action = new Action<ColumnMap>(x => columnConfig(x))
            _map.Column(column, propertyMap.ToColumnMap());
            return this;
        }

        /// <summary>
        /// Specifies that when mapping, the table name should include the keyspace.
        /// Use only if the table you are mapping is in a different keyspace than the current <see cref="ISession"/>.
        /// </summary>
        public SchemaMap<TPoco> KeyspaceName(string name)
        {
            _keyspaceName = name;
            _map.KeyspaceName(_keyspaceName);
            return this;
        }

        /// <summary>
        /// Specifies that the table is defined as COMPACT STORAGE
        /// </summary>
        /// <returns></returns>
        public SchemaMap<TPoco> CompactStorage()
        {
            _compactStorage = true;
            _map.CompactStorage();
            return this;
        }

        /// <summary>
        /// Sets the mapping for the expression using the default options.
        /// </summary>
        public SchemaMap<TPoco> Column<TProp>(Expression<Func<TPoco, TProp>> column)
        {
            _map.Column(column);
            return Column(column, _ => { });
        }

        public IPropertyDefinition GetColumnDefinition(FieldInfo field)
        {
            // If a column map has been defined, return it, otherwise create an empty one
            PropertyMap propertyMap;
            return _propertyMaps.TryGetValue(field.Name, out propertyMap) ? propertyMap : new PropertyMap(field, field.FieldType, false);
        }

        public IPropertyDefinition GetColumnDefinition(PropertyInfo property)
        {
            // If a column map has been defined, return it, otherwise create an empty one
            PropertyMap propertyMap;
            return _propertyMaps.TryGetValue(property.Name, out propertyMap) ? propertyMap : new PropertyMap(property, property.PropertyType, false);
        }

        private string GetColumnName(MemberInfo memberInfo)
        {
            PropertyMap propertyMap;
            if (_propertyMaps.TryGetValue(memberInfo.Name, out propertyMap))
            {
                return ((IPropertyDefinition)propertyMap).ColumnName ?? memberInfo.Name;
            }
            return memberInfo.Name;
        }

        /// <summary>
        /// Gets the MemberInfo for the property or field that the expression provided refers to.  Will throw if the Expression does not refer
        /// to a valid property or field on TPoco.
        /// </summary>
        private MemberInfo GetPropertyOrField<TProp>(Expression<Func<TPoco, TProp>> expression)
        {
            // Take the body of the lambda expression
            var body = expression.Body;

            // We'll get a Convert node for the Func<TPoco, object> where the actual property expression is the operand being converted to object
            if (body.NodeType == ExpressionType.Convert)
                body = ((UnaryExpression)body).Operand;

            var memberExpression = body as MemberExpression;
            if (memberExpression == null || IsPropertyOrField(memberExpression.Member) == false)
                throw new ArgumentOutOfRangeException("expression", string.Format("Expression {0} is not a property or field.", expression));

            if (memberExpression.Member.ReflectedType != _pocoType && _pocoType.IsSubclassOf(memberExpression.Member.ReflectedType) == false)
            {
                throw new ArgumentOutOfRangeException("expression",
                                                      string.Format("Expression {0} refers to a property or field that is not from type {1}",
                                                                    expression, _pocoType));
            }

            return memberExpression.Member;
        }

        private static bool IsPropertyOrField(MemberInfo memberInfo)
        {
            return memberInfo.MemberType == MemberTypes.Field || memberInfo.MemberType == MemberTypes.Property;
        }

        public IEnumerable<object> GetPartitionValues<TProc>(TProc proc)
        {
            foreach (var clusteringKeyColumnMember in _clusteringKeyColumnMembers.Select(x => x.Item1))
            {
                if (clusteringKeyColumnMember.MemberType == MemberTypes.Field)
                {
                    yield return ((FieldInfo)clusteringKeyColumnMember).GetValue(proc);
                }
                else if (clusteringKeyColumnMember.MemberType == MemberTypes.Property)
                {
                    yield return ((PropertyInfo)clusteringKeyColumnMember).GetValue(proc, null);
                }
            }
        }

        public void SetPartitionValues<TProc>(TProc proc, IEnumerable<object> values)
        {
            var enumerator = values.GetEnumerator();

            foreach (var clusteringKeyColumnMember in _clusteringKeyColumnMembers.Select(x => x.Item1))
            {
                enumerator.MoveNext();
                if (clusteringKeyColumnMember.MemberType == MemberTypes.Field)
                {
                    ((FieldInfo)clusteringKeyColumnMember).SetValue(proc, enumerator.Current);
                }
                else if (clusteringKeyColumnMember.MemberType == MemberTypes.Property)
                {
                    ((PropertyInfo)clusteringKeyColumnMember).SetValue(proc, enumerator.Current);
                }
            }
        }

        public void SetColumnValue(string columnName, object obj, object value)
        {
            var propertyMap = _propertyMaps[columnName];

            if (propertyMap.MemberInfo.MemberType == MemberTypes.Field)
            {
                ((FieldInfo)propertyMap.MemberInfo).SetValue(obj, value);
            }
            else if (propertyMap.MemberInfo.MemberType == MemberTypes.Property)
            {
                ((PropertyInfo)propertyMap.MemberInfo).SetValue(obj, value);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public void GetColumnValue<T>(string columnName, object obj)
        {
            throw new NotImplementedException();
            //TODO find the goddamn fucking value of that object specified and return it
        }


        public ITypeDefinition Map
        {
            get
            {
                return _map;
            }
        }

        //        public ITypeDefinition ToMap()
        //        {
        //            Type mapType = typeof (Map<>);
        //            var makeGenericType = mapType.MakeGenericType(_pocoType);
        //            dynamic map = Activator.CreateInstance(makeGenericType);
        ////            var typeDefinition = (ITypeDefinition)Activator.CreateInstance(_pocoType);
        ////            var mapping = new Mappings();
        ////            var map = new Map<string>();
        //            map.TableName(_tableName);
        //            map.PartitionKey(_partitionKeyColumns);
        //            map.PartitionKey
        //        }
    }
}
