﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using System.Reactive.Disposables;
using Infrastructure.ACD.Entities;
using Infrastructure.Common;

namespace Infrastructure.ACD.Repository
{
    public interface IResourceRepository
    {
        Task<ResourceEntry> Get(Guid resourceId);
        ResourceEntry GetSync(Guid resourceId);
        Task<ResourceEntry> Get(string resourceUri);
        Task<ResourceMetadataEntry> GetMetadata(Guid resourceId);
        IObservable<ResourceEntry> Get(IEnumerable<Guid> resourceIds);
        IObservable<ResourceEntry> Get(IEnumerable<Guid> resourceIds, int skip, int take);
        IObservable<ResourceEntry> GetChildren(Guid resourceId);
        Task<ResourceEntry> GetParent(Guid resourceId);
        IObservable<ResourceEntry> GetParents(Guid resourceId);
        IObservable<ResourceEntry> GetAllChildren(Guid resourceId);
        List<Guid> GetResourceGraph(Guid resourceId);

        List<ResourceMetadataEntry> GetMetadata(List<Guid> resourceIds);
        List<Tuple<Guid, Guid>> GetParents(IList<Guid> List);
        IObservable<ResourceMetadataEntry> GetMetadata(IEnumerable<Guid> resourceIds, int skip, int take);

        ResourceMetadataEntry GetMetadataSync(Guid root);
    }
    public class ResourceRepository : IResourceRepository
    {
        IAclCqlRepository _cqlRepository;

        public ResourceRepository(IAclCqlRepository cqlRespository)
        {
            _cqlRepository = cqlRespository;
        }

        public Task<ResourceEntry> GetParent(Guid resourceId)
        {
            var repo = _cqlRepository.ReadResources();
            var solrQuery = new SolrQuery
            {
                Query = string.Format("outid:{0} AND cd:[-1 TO -1]", resourceId),
                Paging = Global.SolrPaging
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            return repo.Where(e => e.SolrQuery == solrQuery.ToString()).FirstOrDefault().ExecuteAsync();
        }

        public IObservable<ResourceEntry> GetParents(Guid resourceId)
        {
            return Observable.Create<ResourceEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadResources();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("outid:{0} AND cd:[-1 TO -1]", resourceId),
                        Paging = Global.SolrPaging
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data =
                        await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }
                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public Task<ResourceEntry> Get(Guid resourceId)
        {
            return _cqlRepository.DbMapper.FirstOrDefaultAsync<ResourceEntry>("WHERE outid = ? AND inid = ?", resourceId, resourceId);
        }

        public ResourceEntry GetSync(Guid resourceId)
        {
            return _cqlRepository.DbMapper.FirstOrDefault<ResourceEntry>("WHERE outid = ? AND inid = ?", resourceId, resourceId);
        }

        public Task<ResourceEntry> Get(string resourceUri)
        {
            var solrQuery = new SolrQuery { Query = string.Format("inuri:\"{0}\" AND outuri:\"{0}\"", resourceUri) };
            var repo = _cqlRepository.ReadResources();
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            return repo.Where(e => e.SolrQuery == solrQuery.ToString()).FirstOrDefault().ExecuteAsync();
        }

        public Task<ResourceMetadataEntry> GetMetadata(Guid resourceId)
        {
            return _cqlRepository.DbMapper.FirstOrDefaultAsync<ResourceMetadataEntry>("WHERE outid = ? AND inid = ?", resourceId, resourceId);
        }

        public IObservable<ResourceEntry> Get(IEnumerable<Guid> resourceIds)
        {
            return Observable.Create<ResourceEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadResources();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Join(" ", resourceIds.Select(e => string.Format("inid:{0} AND cd:0", e)).ToArray()),
                        Paging = Global.SolrPaging
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);

                    }
                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ResourceEntry> Get(IEnumerable<Guid> resourceIds, int skip, int take)
        {
            return Observable.Create<ResourceEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadResources();
                    var solrQuery = new SolrQuery
                    {
                        Query = "(" + string.Join(" ", resourceIds.Select(e => string.Format("inid:{0}", e)).ToArray()) +") AND cd:0",
                        Paging = Global.SolrPaging,
                        Start = skip
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).Take(take).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);

                    }
                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ResourceEntry> GetChildren(Guid resourceId)
        {
            return Observable.Create<ResourceEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadResources();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("+outid:{0} +cd:[1 TO 1]", resourceId),
                        Paging = Global.SolrPaging
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);

                    }
                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<ResourceEntry> GetAllChildren(Guid resourceId)
        {
            return Observable.Create<ResourceEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadResources();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("inid:{0} AND cd:[* TO 0]", resourceId),
                        Paging = Global.SolrPaging
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }
                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public List<Guid> GetResourceGraph(Guid resourceId)
        {
            var repo = _cqlRepository.ReadResources();
            var solrQuery = new SolrQuery
            {
                Query = string.Format("outid:({0}) AND cd:[0 TO *]", resourceId),
                Paging = Global.SolrPaging
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var rows = repo.Where(e => e.SolrQuery == solrQuery.ToString()).Execute();
            return rows.Select(e => e.ResourceId).ToList();
        }

        public List<ResourceMetadataEntry> GetMetadata(List<Guid> resourceIds)
        {
            var repo = _cqlRepository.ReadResourceMetadata();
            var solrQuery = new SolrQuery
            {
                Query = string.Join(" OR ", resourceIds.Select(e => "outid:(" + e + ")")),
                Paging = Global.SolrPaging,
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var rows = repo.Where(e => e.SolrQuery == solrQuery.ToString()).Execute();
            return rows.ToList();
        }

        public IObservable<ResourceMetadataEntry> GetMetadata(IEnumerable<Guid> resourceIds, int skip, int take)
        {
            return Observable.Create<ResourceMetadataEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadResourceMetadata();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Join(" OR ", resourceIds.Select(e => "inid:(" + e + ")")),
                        Start = skip,
                        Sort = "nm asc"
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).Take(take).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);

                    }
                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }
        
         public ResourceMetadataEntry GetMetadataSync(Guid root)
        {
            return _cqlRepository.DbMapper.FirstOrDefault<ResourceMetadataEntry>("WHERE outid = ? AND inid = ?", root, root);
        }

        public List<Tuple<Guid, Guid>> GetParents(IList<Guid> resourceIds)
        {
            var repo = _cqlRepository.ReadResources();
            var result = new List<Tuple<Guid, Guid>>(resourceIds.Count);
            var solrQuery = new SolrQuery
            {
                Query = string.Format("({0}) AND cd:1", string.Join(" OR ", resourceIds.Select(e => "inid:(" + e + ")"))),
                Paging = Global.SolrPaging,
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var rows = repo.Where(e => e.SolrQuery == solrQuery.ToString()).Execute();
            foreach (var resource in rows)
            {
                result.Add(Tuple.Create(resource.ResourceId, resource.ParentId));
            }
            return result;
        }

    }
}
