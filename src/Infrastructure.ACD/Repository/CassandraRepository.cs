﻿using Cassandra.Mapping;
using Infrastructure.ConnectionProvider;
using Infrastructure.Database.Cassandra.Manager.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Repository
{
    public abstract class CassandraRepository
    {
        protected IConnectionProvider ConnectionProvider { get; set; }
        private readonly IMapper _dbMapper;
        private SchemaContainer _schemaContainer;

        public IMapper DbMapper
        {
            get { return _dbMapper; }
        }

        protected internal SchemaContainer SchemaContainer
        {
            get { return _schemaContainer; }
        }
        protected CassandraRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
        {
            ConnectionProvider = connectionProvider;
            _schemaContainer = schemaContainer;
            _dbMapper = new MapperAdapter(ConnectionProvider, _schemaContainer.MappingConfiguration);

        }
    }
}
