﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Repository
{
    public class SolrQuery
    {
        [JsonProperty(PropertyName = "q")]
        public string Query { get; set; }
        [JsonProperty(PropertyName = "sort", NullValueHandling = NullValueHandling.Ignore)]
        public string Sort { get; set; }
        [JsonProperty(PropertyName = "start", NullValueHandling = NullValueHandling.Ignore)]
        public int Start { get; set; }
        [JsonProperty(PropertyName = "paging", NullValueHandling = NullValueHandling.Ignore)]
        public string Paging { get; set; }
        [JsonProperty(PropertyName = "fq", NullValueHandling = NullValueHandling.Ignore)]
        public string FilterQuery { get; set; }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
