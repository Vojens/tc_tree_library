﻿using Cassandra;
using Cassandra.Mapping;
using Infrastructure.ConnectionProvider;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Common.Extensions;
using Cassandra.Data.Linq;
using Infrastructure.ACD.Mapping;

namespace Infrastructure.ACD.Repository
{
    public class SchemaContainer
    {
        private IConnectionProvider _connectionProvider;
        private readonly Dictionary<Type, ISchemaDefinition> _schemaDefinitions;
        private readonly ConcurrentDictionary<Type, Statement> _tableLookUp;
        public MappingConfiguration MappingConfiguration { get; private set; }
        private IDictionary<Type, ITypeDefinition> _maps = new ConcurrentDictionary<Type, ITypeDefinition>();

        public SchemaContainer(IConnectionProvider connectionProvider)
            : this(connectionProvider, MappingConfiguration.Global)
        {
        }

        public SchemaContainer(IConnectionProvider connectionProvider, MappingConfiguration mappingConfiguration)
        {
            _connectionProvider = connectionProvider;
            _schemaDefinitions = new Dictionary<Type, ISchemaDefinition>();
            _tableLookUp = new ConcurrentDictionary<Type, Statement>();
            MappingConfiguration = mappingConfiguration;
        }

        internal static void EnsureHasEmptyConstructor(Type type)
        {
            var constructor = type.GetConstructors().FirstOrDefault(c => c.GetParameters().Length == 0);
            if (constructor.IsNullOrDefault())
            {
                throw new ArgumentException(
                    string.Format("class of type {0} does not have an empty constructor", type));
            }
        }

        public void AddSchemaMap<T>(ISchemaDefinition schemaDefinition)
        //where T : IModel
        {
            EnsureHasEmptyConstructor(typeof(T));
            MappingConfiguration.Define(schemaDefinition.Map); //it will already be defined below in the table
            var type = typeof(T);
            _schemaDefinitions.Add(type, schemaDefinition);
            var tableType = typeof(Table<>);
            var genericTableType = tableType.MakeGenericType(type);
            dynamic table = Activator.CreateInstance(genericTableType, _connectionProvider.Session.SessionAdaptee, MappingConfiguration, schemaDefinition.Map.TableName, schemaDefinition.Map.KeyspaceName);
            //table.CreateIfNotExists();
            _tableLookUp.TryAdd(type, table);
        }

        public void AddSchemaMap<T>(SchemaMap<T> schemaDefinition)
        {
            EnsureHasEmptyConstructor(typeof(T));
            MappingConfiguration.Define(schemaDefinition.Map); //it will already be defined below in the table
            var type = typeof(T);
            _schemaDefinitions.Add(type, schemaDefinition);
            var tableType = typeof(Table<>);
            var genericTableType = tableType.MakeGenericType(type);
            dynamic table = Activator.CreateInstance(genericTableType, _connectionProvider.Session.SessionAdaptee, MappingConfiguration, schemaDefinition.Map.TableName, schemaDefinition.Map.KeyspaceName);
            //table.CreateIfNotExists();
            _tableLookUp.TryAdd(type, table);
        }

        public void AddSchemaMap<T>(Map<T> map)
        //where T : IModel
        {
            EnsureHasEmptyConstructor(typeof(T));
            //MappingConfiguration.Define(map); //it will already be defined below in the table
            var type = typeof(T);
            var table = new Table<T>(_connectionProvider.Session.SessionAdaptee, MappingConfiguration);
            _tableLookUp.TryAdd(type, table);
        }

        public void AddMap<T>(Map<T> map)
        {
            _maps.Add(typeof(T), map);
        }

        public Table<T> GetTable<T>()
        {
            var type = typeof(T);
            Statement table;
            if (!_tableLookUp.TryGetValue(type, out table))
            {
                ITypeDefinition def;
                if (!_maps.TryGetValue(type, out def))
                {
                    throw new KeyNotFoundException(string.Format("Table of type {0} is not available in the table dictionary", type));
                }
                AddSchemaMap<T>((Map<T>)def);
                if (!_tableLookUp.TryGetValue(type, out table))
                {
                    throw new KeyNotFoundException(string.Format("Table of type {0} is not available in the table dictionary", type));
                }
            }
            return (Table<T>)table;
        }

        public virtual CqlQuery<TEntity> Query<TEntity>()
        {
            return CreateCqlQuery<TEntity>();
        }


        public CqlQuery<TSource> CreateCqlQuery<TSource>()
        {
            var type = typeof(TSource);
            var statement = _tableLookUp.GetOrAdd(type, stat =>
            {
                ISchemaDefinition schemaDefinition;
                if (!_schemaDefinitions.TryGetValue(type, out schemaDefinition))
                {
                    throw new KeyNotFoundException(string.Format("Schema Definition of type {0} is not found in the dictionary. Ensure that it is added as a main or a materialized view", type));
                }
                return (Statement)Activator.CreateInstance(type, _connectionProvider.Session.SessionAdaptee);
            });

            return (CqlQuery<TSource>)statement;
        }
    }
}
