﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using System.Reactive.Disposables;
using Cassandra.Mapping;
using Infrastructure.ACD.Entities;
using Infrastructure.ConnectionProvider;
using Infrastructure.Common;
using Infrastructure.Database.Cassandra.Manager.Mapping;

namespace Infrastructure.ACD.Repository
{
    public interface IAcdSetRepository
    {
        Task Put(AcdSetEntry entry);
        Task Put(IBatchHandler batcher, AcdSetEntry entry);
        Task Put(AcdCreatePermission entry);
        Task Put(IBatchHandler batcher, AcdCreatePermission entry);
        IObservable<AcdSetEntry> Get(Guid resourceId);
        IObservable<AcdSetEntry> Get(Guid resourceId, IEnumerable<Guid> identities);
        IObservable<AcdSetEntry> GetByAcdId(Guid acdId);
        IObservable<AcdCreatePermission> GetAcdCreateEntries(Guid resourceId, Guid principalId);
        IObservable<AcdSetEntry> Get(Guid id, Permissions permission);
        Task Delete(Guid resourceId);
        Task Delete(Guid resourceId, Permissions permission);
        Task Delete(Guid resourceId, Permissions permission, Guid principalId);
        Task Delete(Guid resourceId, Permissions permission, Guid principalId, Guid acdId);
        Task Delete(IBatchHandler batcher, Guid resourceId, Permissions permission, Guid principalId, Guid acdId);
        Task Delete(Guid resourceId, string resType, Guid principalId, Guid acdId); //for create permission
        Task Delete(IBatchHandler batcher, Guid resourceId, string resType, Guid principalId, Guid acdId); //for create permission
    }
    public class AcdSetRepository : IAcdSetRepository
    {
        IAclCqlRepository _cqlRepository;

        public AcdSetRepository(IAclCqlRepository cqlRespository)
        {
            _cqlRepository = cqlRespository;
        }

        public Task Put(AcdSetEntry entry)
        {
            return _cqlRepository.Add(entry).ExecuteAsync();
        }

        public Task Put(IBatchHandler batcher, AcdSetEntry entry)
        {
            return batcher.Append(_cqlRepository.Add(entry));
        }

        public Task Put(AcdCreatePermission entry)
        {
            return _cqlRepository.Add(entry).ExecuteAsync();
        }

        public Task Put(IBatchHandler batcher, AcdCreatePermission entry)
        {
            return batcher.Append(_cqlRepository.Add(entry));
        }

        public IObservable<AcdSetEntry> Get(Guid resourceId)
        {
            return Observable.Create<AcdSetEntry>(async o =>
            {
                try
                {
                    var data = await _cqlRepository.DbMapper.FetchAsync<AcdSetEntry>("WHERE outid = ? AND inid = ?", resourceId, resourceId).ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AcdSetEntry> Get(Guid resourceId, IEnumerable<Guid> identities)
        {
            return Observable.Create<AcdSetEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadSet();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("outid:{0} AND (", resourceId),
                        Paging = Global.SolrPaging
                    };
                    solrQuery.Query += string.Join(" OR ", identities.Select(e => string.Format("(idenid:{0})", e)).ToArray()) + ")";
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AcdSetEntry> GetByAcdId(Guid acdId)
        {
            return Observable.Create<AcdSetEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadSet();
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("acdid:{0}", acdId),
                        Paging = Global.SolrPaging
                    };
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AcdCreatePermission> GetAcdCreateEntries(Guid resourceId, Guid principalId)
        {
            return Observable.Create<AcdCreatePermission>(async o =>
            {
                try
                {
                    var data = await _cqlRepository.DbMapper.FetchAsync<AcdCreatePermission>("WHERE resid = ? AND idenid = ?", resourceId, principalId).ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public Task Delete(Guid resourceId)
        {
            var tasks = new Task[2];

            tasks[0] = _cqlRepository.DbMapper.DeleteAsync<AcdSetEntry>("WHERE outid = ? AND inid = ?", resourceId, resourceId);
            tasks[1] = _cqlRepository.DbMapper.DeleteAsync<AcdCreatePermission>("WHERE resid = ?", resourceId);

            return Task.WhenAll(tasks);
        }

        public Task Delete(Guid resourceId, Permissions permission)
        {
            return _cqlRepository.DbMapper.DeleteAsync<AcdSetEntry>("WHERE outid = ? AND inid = ? AND perm = ?", resourceId, resourceId, (short)permission);
        }

        public Task Delete(Guid resourceId, Permissions permission, Guid principalId)
        {
            return _cqlRepository.DbMapper.DeleteAsync<AcdSetEntry>("WHERE outid = ? AND inid = ? AND perm = ? AND idenid = ?", resourceId, resourceId, (short)permission, principalId);
        }

        public Task Delete(Guid resourceId, Permissions permission, Guid principalId, Guid acdId)
        {
            return _cqlRepository.DbMapper.DeleteAsync<AcdSetEntry>("WHERE outid = ? AND inid = ? AND perm = ? AND idenid = ? AND acdid = ?", resourceId, resourceId, (short)permission, principalId, acdId);
        }

        public Task Delete(IBatchHandler batcher, Guid resourceId, Permissions permission, Guid principalId, Guid acdId)
        {
            var perm = (short)permission;
            return batcher.Append(_cqlRepository.ReadSet().Where(e => e.InId == resourceId && e.OutId == resourceId && e.Permission == perm && e.PrincipalId == principalId && e.AcdId == acdId).Delete());
        }

        public IObservable<AcdSetEntry> Get(Guid id, Permissions permission)
        {
            var perm = (short)permission;
            return Observable.Create<AcdSetEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadSet();
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("outid:{0} AND perm:{1}", id, perm),
                        Paging = Global.SolrPaging
                    };
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public Task Delete(Guid resourceId, string resType, Guid principalId, Guid acdId)
        {
            return _cqlRepository.DbMapper.DeleteAsync<AcdCreatePermission>("WHERE resid = ? AND typ = ? AND idenid = ? AND acdid = ?", resourceId, resType, principalId, acdId);
        }

        public Task Delete(IBatchHandler batcher, Guid resourceId, string resType, Guid principalId, Guid acdId)
        {
            return batcher.Append(_cqlRepository.ReadCreateSet().Where(e => e.ResourceId == resourceId && e.ResourceType == resType && e.PrincipalId == principalId && e.AcdId == acdId).Delete());
        }
    }
}
