﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using Cassandra.Mapping;
using Infrastructure.ACD.Entities;
using Infrastructure.ConnectionProvider;
using Infrastructure.Database.Cassandra.Manager.Mapping;

namespace Infrastructure.ACD.Repository
{
    public interface IAcdAuditDataRepository
    {
        Task Put(AuditDataEntry entry);

        Task<AuditDataEntry> Get(Guid auditId);
    }

    public class AcdAuditDataRepository : IAcdAuditDataRepository
    {
        IAclCqlRepository _cqlRepository;

        public AcdAuditDataRepository(IAclCqlRepository cqlRepository)
        {
            _cqlRepository = cqlRepository;
        }
        public Task Put(AuditDataEntry entry)
        {
            return _cqlRepository.Add(entry).ExecuteAsync();
        }
        public Task<AuditDataEntry> Get(Guid auditId)
        {
            return _cqlRepository.DbMapper.FirstOrDefaultAsync<AuditDataEntry>("WHERE auditid = ?", auditId);
        }
    }
}
