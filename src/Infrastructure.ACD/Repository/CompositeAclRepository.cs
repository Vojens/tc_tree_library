﻿using Infrastructure.ConnectionProvider;

namespace Infrastructure.ACD.Repository
{
    public interface ICompositeAclRepository
    {
        IAcdAuditRepository AuditsRepository { get; }
        IAcdAuditDataRepository AuditDataRepository { get; }
        IAcdRepository AcdRepository { get; }
        IAcdSetRepository AcdSetRepository { get; }
        IAcdWorkflowRepository AcdWorkflowRepository { get; }
        IPermissionsRepository PermissionsRepository { get; }
        IResourceRepository ResourceRepository { get; }
        IConnectionProvider ConnectionProvider { get; }
    }

    public class CompositeAclRepository : ICompositeAclRepository
    {
        IConnectionProvider _connectionProvider;
        SchemaContainer _schemaContainer;
        IAclCqlRepository _cqlRepository;
        IAcdAuditRepository _auditRepository;
        IAcdAuditDataRepository _auditDataRepository;
        IAcdRepository _acdRepository;
        IAcdSetRepository _acdSetRespository;
        IAcdWorkflowRepository _acdWorkflowRepository;
        IPermissionsRepository _permissionRepository;
        IResourceRepository _resourceRepository;

        public CompositeAclRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
        {
            _connectionProvider = connectionProvider;
            _schemaContainer = schemaContainer;

            _cqlRepository = new AclCqlRepository(connectionProvider, schemaContainer);
            _auditRepository = new AcdAuditRepository(_cqlRepository);
            _auditDataRepository = new AcdAuditDataRepository(_cqlRepository);
            _acdRepository = new AcdRepository(_cqlRepository);
            _acdSetRespository = new AcdSetRepository(_cqlRepository);
            _acdWorkflowRepository = new AcdWorkflowRepository(_cqlRepository);
            _resourceRepository = new ResourceRepository(_cqlRepository);
            _permissionRepository = new PermissionsRepository(_cqlRepository, _resourceRepository);
        }

        public IAcdAuditRepository AuditsRepository { get { return _auditRepository; } }
        public IAcdAuditDataRepository AuditDataRepository { get { return _auditDataRepository; } }
        public IAcdRepository AcdRepository { get { return _acdRepository; } }
        public IAcdSetRepository AcdSetRepository { get { return _acdSetRespository; } }
        public IAcdWorkflowRepository AcdWorkflowRepository { get { return _acdWorkflowRepository; } }
        public IPermissionsRepository PermissionsRepository { get { return _permissionRepository; } }
        public IResourceRepository ResourceRepository { get { return _resourceRepository; } }
        public IConnectionProvider ConnectionProvider { get { return _connectionProvider; } }
    }
}
