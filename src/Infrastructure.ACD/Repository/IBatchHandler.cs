﻿using Cassandra;
using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Repository
{
    public interface IBatchHandler
    {
        Task Append(CqlCommand statement);
        Task Flush();
    }
}
