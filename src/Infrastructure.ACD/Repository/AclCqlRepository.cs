﻿using Cassandra.Data.Linq;
using Newtonsoft.Json;
using Infrastructure.ConnectionProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Cassandra.Mapping;
using Infrastructure.ACD.Entities;

namespace Infrastructure.ACD.Repository
{
    public interface IAclCqlRepository
    {
        CqlInsert<AcdEntry> Add(AcdEntry entry);
        CqlInsert<AcdSetEntry> Add(AcdSetEntry entry);
        CqlInsert<AcdCreatePermission> Add(AcdCreatePermission entry);
        CqlInsert<AuditEntry> Add(AuditEntry entry);
        CqlInsert<AcdWorkflowEntry> Add(AcdWorkflowEntry entry);
        CqlInsert<AuditDataEntry> Add(AuditDataEntry entry);
        CqlQuery<AcdEntry> ReadHeaders();
        CqlQuery<AcdWorkflowEntry> ReadWorkflows();
        CqlQuery<AcdSetEntry> ReadSet();
        CqlQuery<AcdCreatePermission> ReadCreateSet();
        CqlQuery<ResourceEntry> ReadResources();
        CqlQuery<ResourceMetadataEntry> ReadResourceMetadata();
        CqlQuery<AuditEntry> ReadAudits();
        CqlQuery<AuditDataEntry> ReadAuditData();
        IConnectionProvider ConnectionProvider { get; }
        IMapper DbMapper { get; }
    }

    public class AclCqlRepository : CassandraRepository, IAclCqlRepository
    {
        public AclCqlRepository(IConnectionProvider connectionProvider, SchemaContainer schemaContainer)
            : base(connectionProvider, schemaContainer)
        {

        }

        public CqlInsert<AcdEntry> Add(AcdEntry entry)
        {
            return SchemaContainer.GetTable<AcdEntry>().Insert(entry);
        }

        public CqlInsert<AcdSetEntry> Add(AcdSetEntry entry)
        {
            return SchemaContainer.GetTable<AcdSetEntry>().Insert(entry);
        }

        public CqlInsert<AcdCreatePermission> Add(AcdCreatePermission entry)
        {
            return SchemaContainer.GetTable<AcdCreatePermission>().Insert(entry);
        }

        public CqlInsert<AuditEntry> Add(AuditEntry entry)
        {
            return SchemaContainer.GetTable<AuditEntry>().Insert(entry);
        }
        public CqlInsert<AuditDataEntry> Add(AuditDataEntry entry)
        {
            return SchemaContainer.GetTable<AuditDataEntry>().Insert(entry);
        }

        public CqlInsert<AcdWorkflowEntry> Add(AcdWorkflowEntry entry)
        {
            return SchemaContainer.GetTable<AcdWorkflowEntry>().Insert(entry);
        }

        public CqlQuery<AcdEntry> ReadHeaders()
        {
            return SchemaContainer.GetTable<AcdEntry>();
        }

        public CqlQuery<AcdWorkflowEntry> ReadWorkflows()
        {
            return SchemaContainer.GetTable<AcdWorkflowEntry>();
        }

        public CqlQuery<AcdSetEntry> ReadSet()
        {
            return SchemaContainer.GetTable<AcdSetEntry>();
        }

        public CqlQuery<ResourceEntry> ReadResources()
        {
            return SchemaContainer.GetTable<ResourceEntry>();
        }

        public CqlQuery<ResourceMetadataEntry> ReadResourceMetadata()
        {
            return SchemaContainer.GetTable<ResourceMetadataEntry>();
        }

        public CqlQuery<AuditEntry> ReadAudits()
        {
            return SchemaContainer.GetTable<AuditEntry>();
        }
        public CqlQuery<AuditDataEntry> ReadAuditData()
        {
            return SchemaContainer.GetTable<AuditDataEntry>();
        }

        public CqlQuery<AcdCreatePermission> ReadCreateSet()
        {
            return SchemaContainer.GetTable<AcdCreatePermission>();
        }

        public new IConnectionProvider ConnectionProvider
        {
            get { return base.ConnectionProvider; }
        }
    }
}
