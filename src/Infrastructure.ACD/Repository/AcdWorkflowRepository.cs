﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using System.Reactive.Linq;
using Infrastructure.Common;
using System.Reactive.Disposables;
using Cassandra.Mapping;
using Infrastructure.ACD.Entities;
using Infrastructure.ConnectionProvider;
using Infrastructure.Common.Extensions;
using Infrastructure.Database.Cassandra.Manager.Mapping;

namespace Infrastructure.ACD.Repository
{
    public interface IAcdWorkflowRepository
    {
        Task<AcdWorkflowEntry> Get(Guid resourceId, string workflow);
        Task<AcdWorkflowEntry> Get(Guid workflowId);
        IObservable<AcdWorkflowEntry> GetAcdWorkflowsBy(Guid userId);
        IObservable<AcdWorkflowEntry> GetAcdWorkflowsBy(Guid userId, int skip, int take);
        Task<long> GetCountOfAcdWorkflowsBy(Guid userId);
        IObservable<AcdWorkflowEntry> GetAcdWorkflowsNotBy(Guid userId);
        IObservable<AcdWorkflowEntry> GetAcdWorkflowsNotBy(Guid userId, int skip, int take);
        Task<long> GetCountOfAcdWorkflowsNotBy(Guid userId);
        Task<bool> HasWorkflow(Guid resourceId, string workflow);
        Task<bool> HasWorkflow(Guid resourceId);
        Task<Guid> GetWorkflowUserId(Guid resourceId, string workflow);
        Task Put(AcdWorkflowEntry entry);
        Task Delete(Guid resIid);
        Task Delete(Guid resId, string workflow);
        Task Delete(AcdWorkflowEntry entry);
        Task Delete(Guid resourceId, string workflow, short workflowState, Guid createdUserId, Guid workflowId);
        Task<HashSet<Guid>> HasWorkflows(List<Guid> resourceIds, string workflow);
        Task<AcdWorkflowEntry> GetBuddiedAcd(Guid resourceId);
        Task<AcdWorkflowEntry> GetBuddiedAcd(Guid resourceId, string workflow);

        Task<Guid> GetWorkflowUserId(Guid resourceId);
        Task<string> GetWorkflowOption(Guid resourceId);


        List<Guid> GetAcdWorkflowsFor(List<Guid> resourceIds, string workflow);

        IObservable<AcdWorkflowEntry> GetAcdWorkflowsByDisplayText(string dtext);

        Task<long> GetAcdWorkflowsByDisplayTextCount(string dtext);
    }

    public class AcdWorkflowRepository : IAcdWorkflowRepository
    {
        IAclCqlRepository _cqlRepository;

        public AcdWorkflowRepository(IAclCqlRepository cqlRespository)
        {
            _cqlRepository = cqlRespository;
        }

        public Task<AcdWorkflowEntry> Get(Guid resourceId, string workflow)
        {
            return _cqlRepository.DbMapper.FirstOrDefaultAsync<AcdWorkflowEntry>("WHERE resid = ? AND acdwf = ?", resourceId, workflow);
        }

        public async Task<AcdWorkflowEntry> GetBuddiedAcd(Guid resourceId)
        {
            var repo = _cqlRepository.ReadWorkflows();
            var query = new SolrQuery
            {
                Query = string.Format("resid:{0} AND (acdwf:\"{1}\" OR acdwf:\"{2}\" OR acdwf:\"{3}\")", resourceId, WorkflowOptions.Buddy.ToString(), WorkflowOptions.ReplaceAcd.ToString(), WorkflowOptions.CopyAcl.ToString()),
                Paging = Global.SolrPaging,
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var data = await repo.Where(e => e.SolrQuery == query.ToString()).Take(1).ExecuteAsync().ConfigureAwait(false);
            return data.FirstOrDefault();
        }

        public async Task<AcdWorkflowEntry> GetBuddiedAcd(Guid resourceId, string workflow)
        {
            var repo = _cqlRepository.ReadWorkflows();
            var query = new SolrQuery
            {
                Query = string.Format("resid:{0} AND acdwf:\"{1}\"", resourceId, workflow),
                Paging = Global.SolrPaging,
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var data = await repo.Where(e => e.SolrQuery == query.ToString()).Take(1).ExecuteAsync().ConfigureAwait(false);
            return data.FirstOrDefault();
        }

        public async Task<AcdWorkflowEntry> Get(Guid workflowId)
        {
            // TODO : does this work without partition key ?

            var workflowEntry = await _cqlRepository.ReadWorkflows()
                .Where(e => e.Id == workflowId)
                .Take(1).ExecuteAsync().ConfigureAwait(false);
            return workflowEntry.FirstOrDefault();
        }

        public IObservable<AcdWorkflowEntry> GetAcdWorkflowsBy(Guid userId, int skip, int take)
        {
            return Observable.Create<AcdWorkflowEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadWorkflows();
                    var query = new SolrQuery
                    {
                        Query = string.Format("crusrid:{0}", userId),
                        Sort = "crtime asc",
                        Start = skip,
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == query.ToString()).Take(take).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }
                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AcdWorkflowEntry> GetAcdWorkflowsBy(Guid userId)
        {
            return Observable.Create<AcdWorkflowEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadWorkflows();
                    var query = new SolrQuery
                    {
                        Query = string.Format("crusrid:{0}", userId),
                        Sort = "crtime asc",
                        Paging = Global.SolrPaging
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == query.ToString()).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }
                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public Task<long> GetCountOfAcdWorkflowsBy(Guid userId)
        {
            var repo = _cqlRepository.ReadWorkflows();
            var query = new SolrQuery
            {
                Query = string.Format("crusrid:{0}", userId)
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            return repo.Where(e => e.SolrQuery == query.ToString()).Count().ExecuteAsync();
        }

        public IObservable<AcdWorkflowEntry> GetAcdWorkflowsNotBy(Guid userId, int skip, int take)
        {
            return Observable.Create<AcdWorkflowEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadWorkflows();
                    var query = new SolrQuery
                    {
                        Query = string.Format("-crusrid:{0}", userId),
                        Sort = "crtime asc",
                        Start = skip
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == query.ToString()).Take(take).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }
                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AcdWorkflowEntry> GetAcdWorkflowsNotBy(Guid userId)
        {
            return Observable.Create<AcdWorkflowEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadWorkflows();
                    var query = new SolrQuery
                    {
                        Query = string.Format("-crusrid:{0}", userId),
                        Sort = "crtime asc",
                        Paging = Global.SolrPaging
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == query.ToString()).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }
                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public Task<long> GetCountOfAcdWorkflowsNotBy(Guid userId)
        {
            try
            {
                var repo = _cqlRepository.ReadWorkflows();
                var query = new SolrQuery
                {
                    Query = string.Format("-crusrid:{0}", userId)
                };
                repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                return repo.Where(e => e.SolrQuery == query.ToString()).Count().ExecuteAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task Put(AcdWorkflowEntry entry)
        {
            return _cqlRepository.Add(entry).ExecuteAsync();
        }

        public async Task<bool> HasWorkflow(Guid resourceId, string workflow)
        {
            var anyOneItem = await _cqlRepository.DbMapper.FirstOrDefaultAsync<AcdWorkflowEntry>("WHERE resid = ? AND acdwf = ?", resourceId, workflow).ConfigureAwait(false);
            return !anyOneItem.IsNullOrDefault();
        }

        public async Task<bool> HasWorkflow(Guid resourceId)
        {
            var repo = _cqlRepository.ReadWorkflows();
            var query = new SolrQuery
            {
                Query = string.Format("resid:{0} AND (acdwf:\"{1}\" OR acdwf:\"{2}\" OR acdwf:\"{3}\")", resourceId, WorkflowOptions.Buddy.ToString(), WorkflowOptions.ReplaceAcd.ToString(), WorkflowOptions.CopyAcl.ToString()),
                Paging = Global.SolrPaging,
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var data = await repo.Where(e => e.SolrQuery == query.ToString()).Take(1).ExecuteAsync().ConfigureAwait(false);
            return data.Any();
        }

        public Task<Guid> GetWorkflowUserId(Guid resourceId, string workflow)
        {
            return _cqlRepository.DbMapper.FirstOrDefaultAsync<Guid>("SELECT crusrid FROM acd_workflows WHERE resid = ? AND acdwf = ?", resourceId, workflow);
        }

        public Task<Guid> GetWorkflowUserId(Guid resourceId)
        {
            var repo = _cqlRepository.ReadWorkflows();
            var query = new SolrQuery
            {
                Query = string.Format("resid:{0} AND (acdwf:\"{1}\" OR acdwf:\"{2}\" OR acdwf:\"{3}\")", resourceId, WorkflowOptions.Buddy.ToString(), WorkflowOptions.ReplaceAcd.ToString(), WorkflowOptions.CopyAcl.ToString()),
                Paging = Global.SolrPaging,
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            return repo.Where(e => e.SolrQuery == query.ToString()).Select(x => x.CreatedUserId).FirstOrDefault().ExecuteAsync();
        }

        public Task<string> GetWorkflowOption(Guid resourceId)
        {
            return _cqlRepository.DbMapper.FirstOrDefaultAsync<string>("SELECT acdwf FROM acd_workflows WHERE resid = ?", resourceId);
        }

        public Task Delete(Guid resIid)
        {
            return _cqlRepository.DbMapper.DeleteAsync<AcdWorkflowEntry>("WHERE resid = ?", resIid);
        }

        public Task Delete(AcdWorkflowEntry entry)
        {
            return Delete(entry.ResourceId, entry.AcdWorkflow, entry.AcdWorkflowState, entry.CreatedUserId, entry.Id);
        }

        public Task Delete(Guid resourceId, string workflow, short workflowState, Guid createdUserId, Guid workflowId)
        {
            return _cqlRepository.DbMapper.DeleteAsync<AcdWorkflowEntry>("WHERE resid = ? AND acdwf = ? AND acdwfst= ? AND crusrid = ? AND wfid = ?", resourceId, workflow, workflowState, createdUserId, workflowId);
        }

        public Task Delete(Guid resId, string workflow)
        {
            return _cqlRepository.DbMapper.DeleteAsync<AcdWorkflowEntry>("WHERE resid = ? AND acdwf = ?", resId, workflow);
        }

        public async Task<HashSet<Guid>> HasWorkflows(List<Guid> resourceIds, string workflow)
        {
            var repo = _cqlRepository.ReadWorkflows();
            var result = new HashSet<Guid>();
            var query = new SolrQuery
            {
                Query = string.Format("({0}) AND acdwf:\"{1}\"", string.Join(" OR ", resourceIds.Select(e => string.Format("resid:({0})", e)).ToArray()), workflow),
                Paging = Global.SolrPaging,
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var data = await repo.Where(e => e.SolrQuery == query.ToString()).ExecuteAsync().ConfigureAwait(false);
            foreach (var header in data)
            {
                result.Add(header.ResourceId);
            }
            return result;
        }


        public List<Guid> GetAcdWorkflowsFor(List<Guid> resourceIds, string workflow)
        {
            var repo = _cqlRepository.ReadWorkflows();
            var query = new SolrQuery
            {
                Query = string.Format("({0}) AND acdwf:\"{1}\"", string.Join(" OR ", resourceIds.Select(e => string.Format("resid:({0})", e)).ToArray()), workflow),
                Paging = Global.SolrPaging,
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var data = repo.Where(e => e.SolrQuery == query.ToString()).Execute();
            return data.Select(e => e.ResourceId).ToList();
        }


        public IObservable<AcdWorkflowEntry> GetAcdWorkflowsByDisplayText(string dtext)
        {
            return Observable.Create<AcdWorkflowEntry>(async o =>
            {
                try
                {

                    var repo = _cqlRepository.ReadWorkflows();
                    var query = new SolrQuery
                    {
                        Query = string.Format("dtext:\"{0}\"", dtext),
                        Paging = Global.SolrPaging,
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == query.ToString()).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }
                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }


        public Task<long> GetAcdWorkflowsByDisplayTextCount(string dtext)
        {
            var repo = _cqlRepository.ReadWorkflows();
            var query = new SolrQuery
            {
                Query = string.Format("dtext:\"{0}\"", dtext),
                Paging = Global.SolrPaging,
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            return repo.Where(e => e.SolrQuery == query.ToString()).Count().ExecuteAsync();
        }
    }
}
