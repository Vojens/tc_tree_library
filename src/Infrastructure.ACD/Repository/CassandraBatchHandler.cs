﻿using Cassandra;
using Cassandra.Data.Linq;
using Infrastructure.ConnectionProvider;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Repository
{
    public class CassandraBatchHandler : IBatchHandler
    {
        private int _batchSize;
        private int _counter;
        private object _locker;
        private IConnectionProvider _connectionProvider;
        private Batch _batch;

        public CassandraBatchHandler(IConnectionProvider connectionProvider, int batchSize)
        {
            _batchSize = batchSize;
            _counter = 0;
            _locker = new object();
            _connectionProvider = connectionProvider;
            _batch = null;
        }

        public Task Append(CqlCommand statement)
        {
            lock (_locker)
            {
                if (_batch == null)
                {
                    _batch = _connectionProvider.Session.SessionAdaptee.CreateBatch();
                }
                _batch.Append(statement);
                if (_counter < _batchSize)
                {
                    _counter++;
                    return Task.FromResult(0);
                }
                else
                {
                    var oldBatch = _batch;
                    _batch = null;
                    _counter = 0;
                    return Task.Run(() =>
                    {
                        oldBatch.Execute();
                    });
                }
            }
        }

        public Task Flush()
        {
            lock (_locker)
            {
                if (_batch != null)
                {
                    var oldBatch = _batch;
                    _batch = null;
                    _counter = 0;
                    return oldBatch.ExecuteAsync();
                }
                else
                {
                    return Task.FromResult(0);
                }
            }
        }
    }
}
