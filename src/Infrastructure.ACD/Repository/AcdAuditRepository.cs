﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using System.Reactive.Disposables;
using Cassandra.Mapping;
using Infrastructure.ACD.Entities;
using Infrastructure.ConnectionProvider;
using Infrastructure.Common;
using Infrastructure.Database.Cassandra.Manager.Mapping;

namespace Infrastructure.ACD.Repository
{
    public interface IAcdAuditRepository
    {
        Task Put(AuditEntry entry); 
        Task Put(AuditEntry entry, IBatchHandler batcher = null);
        IObservable<AuditEntry> Get(IEnumerable<Guid> identities, Guid? resourceId, AcdActions? action, string updatedBy, string resourceName, string displayText, DateTimeOffset? from, DateTimeOffset? to, int skip, int take, AuditsSortByOptions sortBy, SortDirection sortOrder, QueryOperator op, string path, string skey);
        Task<long> Count(IEnumerable<Guid> identities, Guid? resourceId, AcdActions? action, string updatedBy, string resourceName, string displayText, DateTimeOffset? from, DateTimeOffset? to, QueryOperator op, string path, string skey);
        Task<AuditEntry> Get(Guid resid, Guid auditId);
    }
    public class AcdAuditRepository : IAcdAuditRepository
    {
        IAclCqlRepository _cqlRepository;

        public AcdAuditRepository(IAclCqlRepository cqlRespository)
        {
            _cqlRepository = cqlRespository;
        }

        public Task Put(AuditEntry entry)
        {
            return _cqlRepository.Add(entry).ExecuteAsync();
        }

        public Task Put(AuditEntry entry, IBatchHandler batcher = null)
        {
            return batcher == null ? Put(entry) : batcher.Append(_cqlRepository.Add(entry));
        }

        public IObservable<AuditEntry> Get(IEnumerable<Guid> identities, Guid? resourceId, AcdActions? action, string updatedBy, string resourceName, string displayText, DateTimeOffset? from, DateTimeOffset? to, int skip, int take, AuditsSortByOptions sortBy, SortDirection sortOrder, QueryOperator op, string path, string skey)
        {
            return Observable.Create<AuditEntry>(async o =>
            {
                try
                {
                    var queryParts = GenerateQueryParts(resourceId, action, updatedBy, resourceName, displayText, from, to, op, path, skey);
                    var repo = _cqlRepository.ReadAudits();
                    var solrQuery = new SolrQuery
                    {
                        Query = queryParts.Any() ? string.Join(" ", queryParts) : "*:*",
                        Start = skip,
                        Sort = GetSortString(sortBy, sortOrder),
                        FilterQuery = string.Format("{{!type=join fromIndex={0}.acd_set}}(perm:6 AND ({1}))",
                        _cqlRepository.ConnectionProvider.ConnectionConfig.Cluster.Keyspace, string.Join(" OR ", identities.Select(e => string.Format("idenid:({0})", e))))
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).Take(take).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public Task<long> Count(IEnumerable<Guid> identities, Guid? resourceId, AcdActions? action, string updatedBy, string resourceName, string displayText, DateTimeOffset? from, DateTimeOffset? to, QueryOperator op, string path, string skey)
        {
            var queryParts = GenerateQueryParts(resourceId, action, updatedBy, resourceName, displayText, from, to, op, path, skey);
            var repo = _cqlRepository.ReadAudits();
            var solrQuery = new SolrQuery
            {
                Query = queryParts.Any() ? string.Join(" ", queryParts) : "*:*",
                Paging = Global.SolrPaging,
                Sort = "timestamp asc",
                FilterQuery = string.Format("{{!type=join fromIndex={0}.acd_set}}(perm:6 AND ({1}))",
                _cqlRepository.ConnectionProvider.ConnectionConfig.Cluster.Keyspace, string.Join(" OR ", identities.Select(e => string.Format("idenid:({0})", e))))
            };
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            return repo.Where(e => e.SolrQuery == solrQuery.ToString()).Count().ExecuteAsync();
        }

        public Task<AuditEntry> Get(Guid resid, Guid auditId)
        {
            return _cqlRepository.DbMapper.FirstOrDefaultAsync<AuditEntry>("WHERE outid = ? AND inid = ? AND auditid = ?", resid, resid, auditId);
        }

        private static string[] GenerateQueryParts(Guid? resourceId, AcdActions? action, string updatedBy, string resourceName, string displayText, DateTimeOffset? from, DateTimeOffset? to, QueryOperator op, string path = null, string skey = null)
        {
            var queryParts = new List<string>();

            var searchMultiple = false;

            queryParts.Add("-restype:(witsml_changelogs)");

            if (op == QueryOperator.OR && !string.IsNullOrWhiteSpace(updatedBy) || !string.IsNullOrWhiteSpace(resourceName))
            {
                searchMultiple = true;
            }

            if (resourceId.HasValue)
            {
                queryParts.Add(string.Format("+inid:{0}", searchMultiple ? resourceId.Value.ToString() + " +(" : resourceId.Value.ToString()));
            }
            else
            {
                queryParts.Add(string.Format("-restype:(channel){0}", searchMultiple ? " +(" : ""));
            }
            if (action.HasValue)
            {
                queryParts.Add(string.Format("+action:{0}", ((short)action.Value)));
            }
            if (from.HasValue && to.HasValue)
            {
                var utcTime = from.Value.ToUniversalTime();
                var utcTimeTo = to.Value.ToUniversalTime();
                queryParts.Add(string.Format("+timestamp:[{0} TO {1}]", utcTime.ToString("s") + "Z", utcTimeTo.ToString("s") + "Z"));
            }
            if (from.HasValue && !to.HasValue)
            {
                var utcTime = from.Value.ToUniversalTime();
                queryParts.Add(string.Format("+timestamp:[{0} TO *]", utcTime.ToString("s") + "Z"));
            }
            if (!from.HasValue && to.HasValue)
            {
                var utcTimeTo = to.Value.ToUniversalTime();
                queryParts.Add(string.Format("+timestamp:[* TO {0}]", utcTimeTo.ToString("s") + "Z"));
            }
            if (!string.IsNullOrWhiteSpace(updatedBy))
            {
                queryParts.Add(string.Format("{1}updatedby:{0}", updatedBy.ToLower(), op == QueryOperator.AND ? "+" : ""));
            }
            if (!string.IsNullOrWhiteSpace(resourceName))
            {
                queryParts.Add(string.Format("{1}resname:{0}", resourceName, op == QueryOperator.AND ? "+" : ""));
            }
            if (!string.IsNullOrWhiteSpace(displayText))
            {
                queryParts.Add(string.Format("{1}dtext:*{0}*", displayText, op == QueryOperator.AND ? "+" : ""));
            }
            if (!string.IsNullOrWhiteSpace(path))
            {
                queryParts.Add(string.Format("{1}dtext:" + "\"" + "{0}" + "\"", path.Replace(@"\", @"\\"), op == QueryOperator.AND ? "+" : ""));
            }
            if (!string.IsNullOrWhiteSpace(skey))
            {
                queryParts.Add(string.Format("{1}(resname:(*{0}*) OR updatedby:(*{0}*))", skey, op == QueryOperator.AND ? "+" : ""));
            }
            if(searchMultiple)
            {
                queryParts.Add(")");
            }
            return queryParts.ToArray();
        }

        private string GetSortString(AuditsSortByOptions sortBy, SortDirection sortOrder)
        {
            var orderBy = sortOrder == SortDirection.ASC ? "asc" : "desc";
            var sortByString = "timestamp";
            switch (sortBy)
            {
                case AuditsSortByOptions.DisplayText:
                    sortByString = "dtext";
                    break;
                case AuditsSortByOptions.User:
                    sortByString = "updatedby";
                    break;
                case AuditsSortByOptions.Name:
                    sortByString = "resname";
                    break;
                case AuditsSortByOptions.Action:
                    sortByString = "action";
                    break;
            }
            return string.Format("{0} {1}", sortByString, orderBy);
        }
    }
}
