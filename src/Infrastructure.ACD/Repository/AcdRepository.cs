﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using System.Reactive.Disposables;
using Cassandra.Mapping;
using Infrastructure.ACD.Entities;
using Infrastructure.ConnectionProvider;
using Infrastructure.Common;
using Infrastructure.Database.Cassandra.Manager.Mapping;

namespace Infrastructure.ACD.Repository
{
    public interface IAcdRepository
    {
        IObservable<AcdEntry> Get(Guid resourceId);
        IObservable<AcdEntry> Get(Guid resourceId, string resourceType, List<Guid> userIdentity);
        Task<AcdEntry> GetById(Guid acdId);
        IObservable<AcdEntry> Get(Guid resourceId, Guid principalId);
        IObservable<AcdEntry> Get(Guid resourceId, int skip, int take);
        IObservable<AcdEntry> Get(Guid resourceId, int skip, int take, AcdSortByOptions sortBy, SortDirection sortOrder, string searchKey);
        IObservable<AcdEntry> Get(Guid resourceId, string searchKey);
        IObservable<AcdEntry> Get(Guid resourceId, int skip, int take, string searchKey);
        IObservable<AcdEntry> GetInheritors(Guid acdId);
        IObservable<AcdEntry> GetWithOnCreate(Guid resourceId);
        Task<long> GetCount(Guid resourceId, string searchKey);
        Task<long> GetCount(Guid resourceId);
        Task Put(AcdEntry entry);
        Task Put(IBatchHandler batcher, AcdEntry entry);
        Task Delete(Guid resourceId, Guid acdId);
        Task Delete(IBatchHandler batcher, Guid resourceId, Guid acdId);
        Task DeleteAll(Guid resourceId);
        IObservable<AcdEntry> GetOriginallyAddedAcds(Guid sourceId);
        IObservable<AcdEntry> GetAcds(List<Guid> resids, Guid inheritedFrom);
        IObservable<AcdEntry> GetAcds(List<Guid> resids);
    }
    public class AcdRepository : IAcdRepository
    {
        IAclCqlRepository _cqlRepository;

        public AcdRepository(IAclCqlRepository cqlRespository)
        {
            _cqlRepository = cqlRespository;
        }

        public IObservable<AcdEntry> Get(Guid resourceId)
        {
            return Observable.Create<AcdEntry>(async o =>
            {
                try
                {
                    var data = await _cqlRepository.DbMapper.FetchAsync<AcdEntry>("WHERE resid = ?", resourceId).ConfigureAwait(false);

                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AcdEntry> Get(Guid resourceId, Guid principalId)
        {
            return Observable.Create<AcdEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadHeaders();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("resid:{0} AND idenid:{1}", resourceId, principalId),
                        Paging = Global.SolrPaging
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);

                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }


        public IObservable<AcdEntry> Get(Guid resourceId, string searchKey)
        {
            return Observable.Create<AcdEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadHeaders();

                    var skeyString = string.IsNullOrWhiteSpace(searchKey) ? "*" : "*" + searchKey + "*";
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("resid:{0} AND idennm:{1}", resourceId, skeyString),
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);

                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public async Task<AcdEntry> GetById(Guid acdId)
        {
            var repo = _cqlRepository.ReadHeaders();
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var solrQuery = new SolrQuery
            {
                Query = string.Format("acdid:{0}", acdId)
            };
            var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);
            return data.FirstOrDefault();
        }

        public IObservable<AcdEntry> Get(Guid resourceId, int skip, int take)
        {
            return Observable.Create<AcdEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadHeaders();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("resid:{0}", resourceId),
                        Sort = "crtime asc",
                        Start = skip
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).Take(take)
                            .ExecuteAsync().ConfigureAwait(false);

                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }


        public IObservable<AcdEntry> Get(Guid resourceId, string resourceType, List<Guid> userIdentities)
        {
            return Observable.Create<AcdEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadHeaders();
                    var solrQuery = new SolrQuery
                    {
                        Query = resourceType == string.Empty
                        ? string.Format("(resid:{0}) AND ({1})", resourceId,
                                  string.Join(" OR ", userIdentities.Select(e => string.Format("idenid:{0}", e))))
                        : string.Format("(resid:{0} AND typ:{1}) AND ({2})", resourceId, resourceType,
                                  string.Join(" OR ", userIdentities.Select(e => string.Format("idenid:{0}", e)))),
                        Sort = "crtime asc",
                        Start = 0,
                        Paging = Global.SolrPaging
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString())
                            .ExecuteAsync().ConfigureAwait(false);

                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AcdEntry> Get(Guid resourceId, int skip, int take, AcdSortByOptions sortBy, SortDirection sortOrder, string searchKey)
        {
            var sortByString = sortBy == AcdSortByOptions.PrincipalName ? "idennm" :
                sortBy == AcdSortByOptions.ResourceType ? "typ" :
                sortBy == AcdSortByOptions.IsInherited ? "inhtfrm" :
                "oncrtch";
            var skeyString = string.IsNullOrWhiteSpace(searchKey) ? "*" : "*" + searchKey + "*";
            return Observable.Create<AcdEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadHeaders();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("resid:{0} AND idennm:{1}", resourceId, skeyString),
                        Sort = sortByString == "idennm" ? sortByString + " " + sortOrder.ToString().ToLower() + ", typ " + sortOrder.ToString().ToLower() : sortByString + " " + sortOrder.ToString().ToLower(),
                        Start = skip
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).Take(take)
                            .ExecuteAsync().ConfigureAwait(false);

                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AcdEntry> Get(Guid resourceId, int skip, int take, string searchKey)
        {
            var skeyString = string.IsNullOrWhiteSpace(searchKey) ? "*" : "*" + searchKey + "*";
            return Observable.Create<AcdEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadHeaders();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("resid:{0} AND idennm:{1}", resourceId, skeyString),
                        Start = skip
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).Take(take)
                            .ExecuteAsync().ConfigureAwait(false);

                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AcdEntry> GetInheritors(Guid acdId)
        {
            return Observable.Create<AcdEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadHeaders();
                    var query = new SolrQuery
                    {
                        Query = string.Format("inhtfrm:{0}", acdId),
                        Sort = "crtime asc",
                        Paging = Global.SolrPaging
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == query.ToString()).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AcdEntry> GetWithOnCreate(Guid resourceId)
        {
            return Observable.Create<AcdEntry>(async o =>
            {
                try
                {
                    //var repo = _cqlRepository.ReadHeaders();
                    //var solrQuery = new SolrQuery
                    //{
                    //    Query = string.Format("+resid:{0} -oncrtch:{1}", resourceId, (int)OnChildCreateOptions.Never),
                    //    Sort = "crtime asc",
                    //    Paging = Global.SolrPaging
                    //};
                    //repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    //var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);

                    //var data = await repo.Where(e => e.ResourceId == resourceId).ExecuteAsync().ConfigureAwait(false);

                    var data = await _cqlRepository.DbMapper.FetchAsync<AcdEntry>("WHERE resid = ?", resourceId).ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        if (header.OnChildCreate == OnChildCreateOptions.Never)
                        {
                            continue;
                        }
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public async Task<long> GetCount(Guid resourceId, string searchKey)
        {
            var repo = _cqlRepository.ReadHeaders();
            var skeyString = string.IsNullOrWhiteSpace(searchKey) ? "*" : "*" + searchKey + "*";
            var solrQuery = new SolrQuery
            {
                Query = string.Format("resid:{0} AND idennm:{1}", resourceId, skeyString),
            };
            var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).Count().ExecuteAsync().ConfigureAwait(false);
            return data;
        }

        public Task<long> GetCount(Guid resourceId)
        {
            //var repo = _cqlRepository.ReadHeaders();

            //var data = await repo.Where(e => e.ResourceId == resourceId).Count()
            //          .ExecuteAsync().ConfigureAwait(false);

            return _cqlRepository.DbMapper.FirstOrDefaultAsync<long>("SELECT COUNT(*) FROM acds WHERE resid = ?", resourceId);
        }

        public Task Put(AcdEntry entry)
        {
            return _cqlRepository.Add(entry).ExecuteAsync();
        }

        public Task Put(IBatchHandler batcher, AcdEntry entry)
        {
            return batcher.Append(_cqlRepository.Add(entry));
        }

        public Task Delete(Guid resourceId, Guid acdId)
        {
            return _cqlRepository.DbMapper.DeleteAsync<AcdEntry>("WHERE resid = ? AND acdid = ?", resourceId, acdId);
        }

        public Task Delete(IBatchHandler batcher, Guid resourceId, Guid acdId)
        {
            return batcher.Append(_cqlRepository.ReadHeaders().Where(e => e.ResourceId == resourceId && e.AcdId == acdId).Delete());
        }

        public Task DeleteAll(Guid resourceId)
        {
            return _cqlRepository.DbMapper.DeleteAsync<AcdEntry>("WHERE resid = ?", resourceId);
        }

        public IObservable<AcdEntry> GetOriginallyAddedAcds(Guid sourceId)
        {
            return Observable.Create<AcdEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadHeaders();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("resid:{0} AND inhtfrm:({1})", sourceId, Guid.Empty),
                        Paging = Global.SolrPaging
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);

                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AcdEntry> GetAcds(List<Guid> resids, Guid inheritedFrom)
        {
            return Observable.Create<AcdEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadHeaders();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Format("(inhtfrm:{0}) AND ({1})", inheritedFrom, string.Join(" OR ", resids.Select(e => string.Format("resid:{0}", e)))),
                        Paging = Global.SolrPaging
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);

                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }

        public IObservable<AcdEntry> GetAcds(List<Guid> resids)
        {
            return Observable.Create<AcdEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadHeaders();
                    var solrQuery = new SolrQuery
                    {
                        Query = string.Join(" OR ", resids.Select(e => "resid:(" + e + ")")),
                        Paging = Global.SolrPaging
                    };
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);

                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }
    }
}
