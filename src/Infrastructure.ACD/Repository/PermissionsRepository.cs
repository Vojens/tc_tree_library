﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using Infrastructure.Common;
using System.Collections.Concurrent;
using System.Globalization;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Cassandra.Mapping;
using Infrastructure.ACD.Entities;
using Infrastructure.Common.Extensions;
using Infrastructure.Database.Cassandra.Manager.Mapping;
using Infrastructure.ConnectionProvider;
using Infrastructure.Common.Util;
using Infrastructure.Database.Cassandra.Manager;

namespace Infrastructure.ACD.Repository
{
    public interface IPermissionsRepository
    {
        Task<bool> HasAnyCreatePermission(Guid resourceId, IEnumerable<Guid> identities);
        Task<bool> HasCreatePermission(Guid resourceId, IEnumerable<Guid> identities, string resourceType);
        Task<bool> HasCreatePermission(List<Guid> resourceIds, IEnumerable<Guid> identities, string resourceType);
        Task<bool> Has(Guid resourceId, IEnumerable<Guid> identities, Permissions permission);
        Task<bool> Has(Guid resourceId, IEnumerable<Guid> identities, IEnumerable<Permissions> permissions);
        Task<bool> Has(string resourceUri, IEnumerable<Guid> identities, IEnumerable<Permissions> permissions);
        Task<Guid[]> Filter(IEnumerable<Guid> resourceIds, IEnumerable<Guid> identities, Permissions perm);
        Task<string[]> Filter(IEnumerable<string> resourceUris, IEnumerable<Guid> identities, Permissions perm);
        Task<Permissions[]> Get(Guid resourceId, IEnumerable<Guid> identities);

        Task<Dictionary<Guid, List<string>>> GetMultiplePermissions(List<Guid> resourceIds, List<Guid> identities,
            List<Permissions> permissions);
    }

    public class PermissionsRepository : IPermissionsRepository
    {
        private readonly IAclCqlRepository _cqlRepository;
        private readonly IResourceRepository _resourceRepository;

        public PermissionsRepository(IAclCqlRepository cqlRespository, IResourceRepository resourceRepository)
        {
            _cqlRepository = cqlRespository;
            _resourceRepository = resourceRepository;
        }

        public async Task<bool> Has(Guid resourceId, IEnumerable<Guid> identities, Permissions permission)
        {
            var perm = (short)permission;
            var identityList = identities.ToList();
            var identityCount = identityList.Count;
            if (identityCount == 0)
            {
                return false;
            }
            
            //fetch from the database with in-clause statement. If the number of identities grow, however,
            //this can increase the query size, so instead, fetch all
            
            const string selectCaluse = "SELECT outid FROM acd_set";
            const string partialWhenClause = "WHERE outid = ? AND inid = ? AND perm = ? AND idenid ";
            string whereClause = partialWhenClause;
            if (identityCount == 1)
            {
                whereClause += "= ?";
            }
            else if (identityCount > 1 && identityCount <= 20)
            {
                whereClause += string.Format("IN ({0})", string.Join(",", Enumerable.Repeat("?", identityCount)));
            }

            if (identityCount <= 20)
            {
                var @params = new List<object> {resourceId, resourceId, perm};
                identityList.ForEach(i => @params.Add(i));
                var cqlQuery = string.Format("{0} {1}", selectCaluse, whereClause);
                var outid = await _cqlRepository.DbMapper.FirstOrDefaultAsync<AcdSetEntry>(
                           cqlQuery, @params.ToArray())
                            .ConfigureAwait(false);
                return !outid.IsNullOrDefault();
            }
            else
            {
                //we fetch the complete list buy try to that in a smart way if we could
                //we try to use idenid >= ? and idenid <= ?, so we use uuid serializers
                //order the list, and get the first and the last
                var orderedIdentityList = identityList.OrderBy(iden => TypeCodec.EncodeUuid(iden)).ToList();

                var startIdenId = orderedIdentityList.First();
                var endIdenId = orderedIdentityList.Last();

                //there could be an additional identity ids that we don't want, so we pull the entire portion
                var fetchedIdentityIds = await
                    _cqlRepository.DbMapper.FetchAsync<AcdSetEntry>(
                        "SELECT idenid FROM acd_set WHERE inid = ? AND outid = ? AND perm = ? AND idenid => ? AND idenid <= ?", resourceId, resourceId, perm, startIdenId, endIdenId).ConfigureAwait(false);
                //then intersect to see if there is an overlap
                return identities.Intersect(fetchedIdentityIds.Select(x => x.PrincipalId)).Any();
            }
        }

        public async Task<bool> Has(Guid resourceId, IEnumerable<Guid> identities, IEnumerable<Permissions> permissions)
        {
            var tasks = new List<Task<AcdSetEntry>>();
            foreach (var permission in permissions)
            {
                var perm = (short)permission;
                tasks.Add(_cqlRepository.ReadSet().Where(e => e.InId == resourceId
                && e.OutId == resourceId && (short)e.Permission == perm
                && identities.Contains(e.PrincipalId)).FirstOrDefault().ExecuteAsync());
            }
            await Task.WhenAll(tasks.ToArray()).ConfigureAwait(false);
            return tasks.Select(e => e.Result).All(e => e != null);
        }

        public async Task<bool> Has(string resourceUri, IEnumerable<Guid> identities, IEnumerable<Permissions> permissions)
        {
            var resource = await _resourceRepository.Get(resourceUri).ConfigureAwait(false);
            return await Has(resource.ResourceId, identities, permissions).ConfigureAwait(false);
        }

        public async Task<Guid[]> Filter(IEnumerable<Guid> resourceIds, IEnumerable<Guid> identities, Permissions perm)
        {
            var tasks = new ConcurrentDictionary<Guid, Task<bool>>();
            foreach (var resourceId in resourceIds)
            {
                tasks.TryAdd(resourceId, Has(resourceId, identities, perm));
            }
            await Task.WhenAll(tasks.Values.ToArray()).ConfigureAwait(false);
            return tasks.Where(e => e.Value.Result).Select(e => e.Key).ToArray();
        }

        public async Task<string[]> Filter(IEnumerable<string> resourceUris, IEnumerable<Guid> identities, Permissions perm)
        {
            var data = new ConcurrentDictionary<string, Task<bool>>();
            var tasks = new List<Task>();
            foreach (var resourceUri in resourceUris)
            {
                var currentResourceUri = resourceUri;
                tasks.Add(Task.Run(async () =>
                {
                    var resource = await _resourceRepository.Get(currentResourceUri).ConfigureAwait(false);
                    data.TryAdd(currentResourceUri, Has(resource.ResourceId, identities, perm));
                }));
            }
            await Task.WhenAll(tasks.ToArray()).ConfigureAwait(false);
            await Task.WhenAll(data.Values.ToArray()).ConfigureAwait(false);
            return data.Where(e => e.Value.Result).Select(e => e.Key).ToArray();
        }

        public async Task<Permissions[]> Get(Guid resourceId, IEnumerable<Guid> identities)
        {
            var repo = _cqlRepository.ReadSet();
            repo.SetConsistencyLevel(Global.SolrConsistencyLevel);
            var solrQuery = new SolrQuery
            {
                Query = string.Format("(inid:{0}) AND ({1})",
                    resourceId,
                    string.Join(" ", identities.Select(e => string.Format("idenid:{0}", e)))),
                Paging = Global.SolrPaging
            };

            var dataResult = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);

            var data = dataResult.Select(e => e.Permission).Distinct().Cast<Permissions>().ToList();

            return data.ToArray();
        }

        public async Task<bool> HasCreatePermission(Guid resourceId, IEnumerable<Guid> identities, string resourceType)
        {
            var repo = _cqlRepository.ReadCreateSet();
            var types = new[] { "", resourceType.ToLower() };
            var result = await repo.Where(e => e.ResourceId == resourceId && identities.Contains(e.PrincipalId)
                && types.Contains(e.ResourceType)).FirstOrDefault().ExecuteAsync().ConfigureAwait(false);
            return result != null;
        }
        
        public async Task<bool> HasCreatePermission(List<Guid> resourceIds, IEnumerable<Guid> identities, string resourceType)
        {
            var repo = _cqlRepository.ReadCreateSet();
            var types = new[] { "", resourceType.ToLower() };

            var tasks = resourceIds.Select(resourceId => repo.Where(e => e.ResourceId == resourceId && identities.Contains(e.PrincipalId) &&
                                types.Contains(e.ResourceType)).FirstOrDefault().ExecuteAsync());
            await Task.WhenAll(tasks).ConfigureAwait(false);

            var hasPerm = true;
            foreach (var task in tasks)
            {
                var result = await task.ConfigureAwait(false);
                if (result == null)
                {
                    hasPerm = false;
                    break;
                }
            }
            return hasPerm;
        }

        public async Task<bool> HasAnyCreatePermission(Guid resourceId, IEnumerable<Guid> identities)
        {
            var repo = _cqlRepository.ReadCreateSet();
            var result = await repo.Where(e => e.ResourceId == resourceId && identities.Contains(e.PrincipalId)).FirstOrDefault().ExecuteAsync().ConfigureAwait(false);
            return result != null;
        }

        public async Task<Dictionary<Guid, List<string>>> GetMultiplePermissions(List<Guid> resourceIds, List<Guid> identities, List<Permissions> permissions)
        {
            var ignoredTypes = new[] { "channel", "mudlogchannelmap", "trajectorymap", "mudlogchannel", "trajectoryheader", "virtualLogChannel", "virtuallogsourcechannel" };
            var acds = await GetAcds(resourceIds, identities, permissions).AsAsyncList().ConfigureAwait(false);
            
            var createPerms = new List<IEnumerable<AcdCreatePermission>>();
            if (permissions.Contains(Permissions.Create))
            {
                var crPerms = await Task.WhenAll(HasCreatePermission(resourceIds, identities)).ConfigureAwait(false);
                createPerms = crPerms.ToList();
            }
            var result = new Dictionary<Guid, List<string>>();

            if (acds.Any())
            {
                foreach (var resPerm in acds.GroupBy(p => p.OutId))
                {
                    var perms = resPerm.Select(x => ((Permissions)x.Permission).ToString()).ToList();
                    foreach (var createPerm in createPerms)
                    {
                        var crPerms = createPerm.Where(x => x.ResourceId == resPerm.Key && !ignoredTypes.Contains(x.ResourceType)).ToList();
                        if (!crPerms.Any()) continue;
                        var crStr = crPerms.Aggregate(Permissions.Create + " (", (current, crPerm) => current +
                            (string.IsNullOrWhiteSpace(crPerm.ResourceType) ? "All" : crPerm.ResourceType) + ",").TrimEnd(',') + ")";
                        perms.Add(crStr);
                        break;
                    }
                    result.Add(resPerm.Key, perms);
                }
            }
            else
            {
                foreach (var createPerm in createPerms)
                {
                    var crPerms = createPerm.Where(x => !ignoredTypes.Contains(x.ResourceType)).ToList();
                    if (!crPerms.Any()) continue;
                    var crStr = crPerms.Aggregate(Permissions.Create + " (",
                                    (current, crPerm) => current + (string.IsNullOrWhiteSpace(crPerm.ResourceType)? "All"
                                                             : crPerm.ResourceType) +",").TrimEnd(',') + ")";
                    result.Add(crPerms.First().ResourceId, new List<string> {crStr});
                }
            }

            return result;
        }
        
        private IEnumerable<Task<IEnumerable<AcdCreatePermission>>> HasCreatePermission(List<Guid> resourceIds, IEnumerable<Guid> identities)
        {
            var repo = _cqlRepository.ReadCreateSet();
            var tasks = resourceIds.Select(resourceId => repo.Where(e => e.ResourceId == resourceId && identities.Contains(e.PrincipalId)).ExecuteAsync());
            return tasks;
        }

        private IObservable<AcdSetEntry> GetAcds(List<Guid> resourceIds, List<Guid> principalIds, List<Permissions> permissions)
        {

            return Observable.Create<AcdSetEntry>(async o =>
            {
                try
                {
                    var repo = _cqlRepository.ReadSet();
                    repo.SetConsistencyLevel(Global.SolrConsistencyLevel);

                    var solrQuery = new SolrQuery
                    {
                        Paging = Global.SolrPaging
                    };
                    solrQuery.Query += "(" + string.Join(" OR ", principalIds.Select(e => string.Format("idenid:{0}", e)).ToArray()) + ")";
                    solrQuery.Query += " AND (" + string.Join(" OR ", resourceIds.Select(e => string.Format("outid:{0}", e)).ToArray()) + ")";

                    if (permissions != null && permissions.Count > 0)
                    {
                        solrQuery.Query += " AND (" + string.Join(" OR ", permissions.Select(e => string.Format("perm:{0}", (short)e)).ToArray()) + ")";
                    }
                    var data = await repo.Where(e => e.SolrQuery == solrQuery.ToString()).ExecuteAsync().ConfigureAwait(false);
                    foreach (var header in data)
                    {
                        o.OnNext(header);
                    }

                    o.OnCompleted();
                }
                catch (Exception ex)
                {
                    o.OnError(ex);
                }
                return Disposable.Empty;
            });
        }
    }
}
