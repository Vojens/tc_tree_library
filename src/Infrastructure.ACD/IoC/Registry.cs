﻿using LightInject;
using Infrastructure.ConnectionProvider;
using Infrastructure.Common.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.IoC
{
    public class Registry : ICompositionRoot
    {
        private const string ConfigType = "Graph";

        public static void Init()
        {
            ComponentRegistry.RegisterFrom<Registry>();
        }

        public void Compose(IServiceRegistry serviceRegistry)
        {
            var connectionProvider = new CassandraConnectionProvider(ConfigType);
            connectionProvider.Connect();
            serviceRegistry.Register<IConnectionProvider>(factory => connectionProvider, ConfigType, new PerContainerLifetime());
        }
    }
}
