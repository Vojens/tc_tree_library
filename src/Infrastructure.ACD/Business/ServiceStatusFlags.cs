﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business
{
    public enum ServiceStatusFlags
    {
        NotStarted,
        Started,
        Completed,
        Errored
    }
}
