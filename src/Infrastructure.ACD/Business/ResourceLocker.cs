﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business
{
    public class ResourceLocker
    {
        static object _locker = new object();
        static Dictionary<Guid, HashSet<Guid>> _lockedResourcesPerJob = new Dictionary<Guid, HashSet<Guid>>();

        private static bool IsAlreadyLocked(HashSet<Guid> resourceIds)
        {
            var incoming = new HashSet<Guid>(resourceIds);
            var found = false;
            Parallel.ForEach(_lockedResourcesPerJob, e =>
            {
                if (e.Value.Intersect(incoming).Any())
                {
                    found = true;
                }
            });
            return found;
        }

        public static bool TryLock(Guid jobId, IEnumerable<Guid> resourceIds)
        {
            var incoming = new HashSet<Guid>(resourceIds);
            lock (_locker)
            {
                if (!IsAlreadyLocked(incoming))
                {
                    _lockedResourcesPerJob.Add(jobId, incoming);
                    return true;
                }
            }
            return false;
        }

        public static void Unlock(Guid jobId)
        {
            lock (_locker)
            {
                _lockedResourcesPerJob.Remove(jobId);
            }
        }
    }
}
