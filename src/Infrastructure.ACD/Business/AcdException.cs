﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business
{
    public class AcdException : Exception
    {
        public string Code { get; private set; }
        public string Description { get; private set; }
        public AcdException(string code, string description)
        {
            Code = code;
            Description = description;
        }
    }
}
