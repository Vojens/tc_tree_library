﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Cassandra.Mapping;
using Infrastructure.ACD.Business.CopyACL;
using Newtonsoft.Json;
using Infrastructure.ConnectionProvider;
using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Mapping.Cassandra;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.IoC;
using Infrastructure.Common.Model;
using RestSharp;
using Newtonsoft.Json.Linq;
using Infrastructure.Common.Extensions;

namespace Infrastructure.ACD.Business
{
    public interface ICompositeAclService
    {
        IObservable<AcdWorkflowEntry> GetWorkflowsByUserId(Guid userId, int skip, int take);

        Task<long> GetWorkflowsByUserIdCount(Guid userId);

        IObservable<AcdWorkflowEntry> GetWorkflowsNotByUserId(Guid userId, int skip, int take);

        Task<long> GetWorkflowsNotByUserIdCount(Guid userId);

        IObservable<AcdEntry> GetAclByResourceId(Guid resid, int skip, int take);

        Task<long> GetAclByResourceIdCount(Guid resid);

        Task<AcdWorkflowEntry> GetPendingAclByResourceId(Guid resid);

        Task<ResourceEntry> GetResource(Guid resid);

        Task<Permissions[]> GetEffectivePermissions(Guid resid);
        IObservable<AcdEntry> GetEffectivePermissions(Guid resid, string resourceType, List<Guid> userIdentities);

        Task ApplyAcl(IEnumerable<AcdEntry> acds, Guid statusId, bool approval = false, EventBus eventBusInstance = null);

        Task ApplyAclWithoutPermissionCheck(IEnumerable<AcdEntry> acds, Guid statusId, bool approval = false);

        Task ApplyAclIgnoringDuplicates(List<AcdEntry> data, Guid statusId, bool approval = false);

        bool IsBuddyUser(Guid userId);

        string GetPrincipalDetails(string principalName);

        Task CheckAndBuddyResource(Guid resid, IEnumerable<AcdEntry> acds);

        /// <summary>
        /// For copy acl
        /// </summary>
        Task CheckAndBuddyResource(Guid jobId, Guid source, Guid destination, Dictionary<Guid, Guid> maps);

        Task ApproveAcl(Guid resid, Guid statusId, EventBus eventBusInstance = null);

        Task RejectAcl(Guid resid);

        IObservable<AuditEntry> GetAudits(Guid? resid, int skip, int take, AcdActions? action, string updatedby, string resname, string displayText, DateTimeOffset? from, DateTimeOffset? to, AuditsSortByOptions sortBy, SortDirection sortOrder, QueryOperator op, string path, string skey);

        Task<long> GetAuditsCount(Guid? resid, AcdActions? action, string updatedby, string resname, string displayText, DateTimeOffset? from, DateTimeOffset? to, QueryOperator op, string path, string skey);

        IObservable<AcdEntry> SearchAcds(Guid resid, int skip, int take, AcdSortByOptions sortBy, SortDirection sortOrder, string searchKey);

        IObservable<AcdEntry> SearchAcds(Guid resid, string searchKey);

        IObservable<AcdEntry> SearchAcds(Guid resid, int skip, int take, string searchKey);

        Task<long> SearchAcdsCount(Guid resid, string searchKey);

        IObservable<AcdEntry> GetAcds(Guid resid);

        IObservable<AcdEntry> GetAcds(Guid resid, Guid userId);

        Task<Guid> BuddiedUserId(Guid resourceId);

        Task<AuditEntry> GetAudit(Guid resid, Guid id);
        Task<AuditDataEntry> GetAuditData(Guid resid, Guid id);

        Task DeleteAll(Guid resIid);

        Task<IEnumerable<AcdSetEntry>> GetAcdSets(Guid id, Permissions permission);

        IPermissionService PermissionService { get; }

        IResourceCreateService ResourceCreateService { get; }

        List<string> GetIdentities(Guid Id, string type);

        Task UpdatePrincipal(Guid resid, Guid old, Guid @new, PrincipalTypes principalType, string principalName);

        Task<ResourceMetadataEntry> GetResourceMetadata(Guid resid);

        IEnumerable<Guid> GetInheritedResources(Guid resid, int skip, int take, Guid acdId, bool isUp, bool isInheritedFromParent);

        IObservable<ResourceMetadataEntry> GetResourceMetadata(IEnumerable<Guid> resids, int skip, int take);

        Task CopyAcl(Guid jobId, Guid source, Guid destination, Dictionary<Guid, Guid> maps);

        Task ApproveCopyAcl(Guid job, Guid resid);

        Task RejectCopyAcl(Guid job, Guid resid);

        Task<List<AcdEntry>> ReviewCopyAcl(Guid resid);

        Task ApplyReplacePrincipal(Guid resid, Guid principalId, string principalName, PrincipalTypes principalType, IEnumerable<AcdEntry> acds, Guid statusId, bool approval = false);

        Task CheckAndBuddyReplacePrincipal(Guid resid, Guid principalId, string principalName, PrincipalTypes principalType, IEnumerable<AcdEntry> acds);

        Task ApproveReplacePrincipal(Guid resid, Guid statusId);

        Task<List<AcdEntry>> ReviewReplacePrincipal(Guid resid, Guid currentResid);

        Task RejectReplacePrincipal(Guid resid);

        Task<string> GetWorkflowOption(Guid resourceId);

        Task<List<ResourceMetadataEntry>> GetResourceDescendants(Guid resid, int skip, int take);

        Task<List<ResourceMetadataEntry>> GetResourceDescendantsForCopyAcl(Guid resid, int skip, int take);

        Task<Dictionary<Guid, List<string>>> GetMultiplePermissions(List<Guid> resourceIds, List<Guid> principalIds,
            List<Permissions> permissions = null);

        IObservable<AcdEntry> GetAcds(List<Guid> resids, Guid inheritedFrom);

        Task<AcdEntry> GetAcdById(Guid acdId);

        List<Principal> GetPrincipals(List<Guid> principalIds);
    }

    public class CompositeAclService : ICompositeAclService
    {
        private IGraphClientInfo _client;
        private IPermissionService _permissionService;
        private IResourceCreateService _resourceCreateService;
        private IPrincipalUpdateService _principalUpdateService;
        static IConnectionProvider _connectionProvider;
        static SchemaContainer _schemaContainer;
        static ICompositeAclRepository _repository;
        private readonly string _setAclUpdateExchange = ConfigurationManager.AppSettings["SetAclUpdateExchange"] ?? "SetAclUpdateExchange";
        private const string HashExchangeType = "x-consistent-hash";
        static CompositeAclService()
        {
            _connectionProvider = ComponentRegistry.Resolve<IConnectionProvider>();
            _schemaContainer = new SchemaContainer(_connectionProvider, new MappingConfiguration());
            CassandraMappingsDefaultConfigurator.Configure(_schemaContainer);
            _repository = new CompositeAclRepository(_connectionProvider, _schemaContainer);

        }

        public CompositeAclService(IGraphClientInfo client)
        {
            _client = client;
            _permissionService = new PermissionService(_repository);
            _resourceCreateService = new ResourceCreateService(_repository, client);
            _principalUpdateService = new PrincipalUpdateService(_repository, _permissionService, _client);
        }

        public IObservable<AcdWorkflowEntry> GetWorkflowsByUserId(Guid userId, int skip, int take)
        {
            var workflows = _repository.AcdWorkflowRepository.GetAcdWorkflowsBy(userId, skip, take).ToEnumerable();
            return GetCopyAclFilteredWorkflows(workflows.ToList());
        }

        public Task<long> GetWorkflowsByUserIdCount(Guid userId)
        {
            return _repository.AcdWorkflowRepository.GetCountOfAcdWorkflowsBy(userId);
        }

        public IObservable<AcdWorkflowEntry> GetWorkflowsNotByUserId(Guid userId, int skip, int take)
        {
            var workflows = _repository.AcdWorkflowRepository.GetAcdWorkflowsNotBy(userId, skip, take).ToEnumerable();
            return GetCopyAclFilteredWorkflows(workflows.ToList());

        }

        private IObservable<AcdWorkflowEntry> GetCopyAclFilteredWorkflows(List<AcdWorkflowEntry> workflows)
        {
            var copyAclWorkflows = workflows.Where(x => x.AcdWorkflow == WorkflowOptions.CopyAcl.ToString());
            workflows = workflows.Except(copyAclWorkflows).ToList();
            var deserilizedDataAndDetails = copyAclWorkflows.Select(x => new { ResourceId = x.ResourceId, Data = JsonConvert.DeserializeObject<NodeData>(x.Data) });
            var groupByRootDestination = deserilizedDataAndDetails.GroupBy(x => x.Data.RootDestination);

            foreach (var item in groupByRootDestination)
            {
                if (copyAclWorkflows.Any(x => x.ResourceId == item.Key))
                {
                    workflows.Add(copyAclWorkflows.Where(x => x.ResourceId == item.Key).FirstOrDefault());
                }
                else
                {
                    var currentWorkflow = copyAclWorkflows.Where(x => item.Select(y => y.ResourceId).Contains(x.ResourceId)).FirstOrDefault();
                    workflows.Add(currentWorkflow);
                }
            }

            return workflows.ToObservable();
        }

        public Task<long> GetWorkflowsNotByUserIdCount(Guid userId)
        {
            return _repository.AcdWorkflowRepository.GetCountOfAcdWorkflowsNotBy(userId);
        }

        public IObservable<AcdEntry> GetAclByResourceId(Guid resid, int skip, int take)
        {
            AssertPermission(resid, Permissions.SecurityRead).Wait();
            return _repository.AcdRepository.Get(resid, skip, take);
        }

        public Task<long> GetAclByResourceIdCount(Guid resid)
        {
            AssertPermission(resid, Permissions.SecurityRead).Wait();
            return _repository.AcdRepository.GetCount(resid);
        }

        public async Task<AcdWorkflowEntry> GetPendingAclByResourceId(Guid resid)
        {
            var isBuddyUser = IsBuddyUser(_client.UserId);
            var isResouceBuddied = await IsResourceBuddied(resid).ConfigureAwait(false);

            if (!isBuddyUser || !isResouceBuddied)
            {
                throw new AcdException("E_VAL", "Neither resource is buddied or user is buddy");
            }

            return await _repository.AcdWorkflowRepository.GetBuddiedAcd(resid).ConfigureAwait(false);
        }

        public Task<ResourceEntry> GetResource(Guid resid)
        {
            return _repository.ResourceRepository.Get(resid);
        }

        public async Task<Permissions[]> GetEffectivePermissions(Guid resid)
        {
            var permissions = (await _repository.PermissionsRepository.Get(resid, _client.UserIdentities).ConfigureAwait(false)).ToList();
            var hasCreatePermission = await _repository.PermissionsRepository.HasAnyCreatePermission(resid, _client.UserIdentities).ConfigureAwait(false);
            if (hasCreatePermission)
            {
                permissions.Add(Permissions.Create);
            }
            return permissions.ToArray();
        }

        public IObservable<AcdEntry> GetEffectivePermissions(Guid resid, string resourceType, List<Guid> userIdentities)
        {
            return _repository.AcdRepository.Get(resid, resourceType, userIdentities);
        }

        public async Task ApplyAcl(IEnumerable<AcdEntry> acds, Guid jobstatusId, bool approval = false, EventBus eventBusInstance = null)
        {
            var writeService = new AcdWriteService(_repository, _client, jobstatusId);
            var acdsList = acds.ToList();
            await writeService.Handle(acdsList, approval).ConfigureAwait(false);
            var resourceSet = await GetResourceSet(acdsList).ConfigureAwait(false);
            if (resourceSet.ToLower() == "sea")
            {
                await PublishSetAclUpdateEventForReplication(eventBusInstance, acdsList.First().ResourceId).ConfigureAwait(false);
            }

        }

        private async Task<string> GetResourceSet(List<AcdEntry> acdsList)
        {
            if (acdsList.First().ResourceSet != null)
                return acdsList.First().ResourceSet;

            var resMetadata = await GetResourceMetadata(acdsList.First().ResourceId).ConfigureAwait(false);
            return resMetadata == null ? string.Empty : resMetadata.ResourceSet;
        }

        private async Task PublishSetAclUpdateEventForReplication(EventBus eventBusInstance, Guid setId)
        {

            if (eventBusInstance != null)
                eventBusInstance.PublishRealtime(_setAclUpdateExchange, new SetAclUpdateEvent { ResourceId = setId, Timestamp = DateTimeOffset.UtcNow },
                    routingKey: Guid.NewGuid().ToString(), exchangeType: HashExchangeType);

            await PublishAclUpdateEventsForSubFolders(setId, eventBusInstance).ConfigureAwait(false);
        }

        private async Task PublishAclUpdateEventsForSubFolders(Guid setId, EventBus eventBusInstance)
        {
            var subFolders = (await _repository.ResourceRepository.GetChildren(setId).AsAsyncList().ConfigureAwait(false)).Where(x => x.ResourceType.Equals("folder", StringComparison.InvariantCultureIgnoreCase));
            foreach (var folder in subFolders)
            {
                await PublishSetAclUpdateEventForReplication(eventBusInstance, folder.ResourceId).ConfigureAwait(false);
            }
        }

        public Task ApplyAclWithoutPermissionCheck(IEnumerable<AcdEntry> acds, Guid statusId, bool approval = false)
        {
            var writeService = new AcdWriteService(_repository, _client, statusId);
            return writeService.HandleWithoutPermissionCheck(acds.ToList());
        }

        public Task ApplyAclIgnoringDuplicates(List<AcdEntry> acds, Guid statusId, bool approval = false)
        {
            var writeService = new AcdWriteService(_repository, _client, statusId);
            return writeService.HandleIgnoringDuplicates(acds.ToList());
        }

        public bool IsBuddyUser(Guid userId)
        {
            var restClient = new RestClient(ConfigurationManager.AppSettings["SecurityAPI"]);
            var request = new RestRequest("principals/users/buddied/" + userId);
            request.AddHeader("SamlToken", _client.Token.Replace("\r\n", ""));
            var response = restClient.Execute(request);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception(response.StatusDescription);
            }
            return bool.Parse(response.Content);
        }
        public string GetPrincipalDetails(string principalName)
        {
            var restClient = new RestClient(ConfigurationManager.AppSettings["SecurityAPI"]);
            var request = new RestRequest("principals/principal/" + principalName);
            request.AddHeader("SamlToken", _client.Token.Replace("\r\n", ""));
            var response = restClient.Execute(request);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception(response.StatusDescription);
            }

            return response.Content;
        }

        public List<Principal> GetPrincipals(List<Guid> principalIds)
        {
            var restClient = new RestClient(ConfigurationManager.AppSettings["SecurityAPI"]);
            var request = new RestRequest("principals/principalsByIds", Method.POST);
            request.AddJsonBody(principalIds);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("SamlToken", _client.Token.Replace("\r\n", ""));
            var response = restClient.Execute(request);
            var principals = JsonConvert.DeserializeObject<List<Principal>>(response.Content);
            return principals;
        }

        
        /// <summary>
        /// For copy acl
        /// </summary>
        public Task CheckAndBuddyResource(Guid jobId, Guid source, Guid destination, Dictionary<Guid, Guid> maps)
        {
            var copyService = new AclCopyService(_repository, _client, jobId);
            return copyService.Buddy(source, destination, maps.Select(e => Tuple.Create(e.Key, e.Value)).ToList());
        }

        public async Task CheckAndBuddyResource(Guid resid, IEnumerable<AcdEntry> acds)
        {
            await AssertPermission(resid, Permissions.SecurityWrite).ConfigureAwait(false);
            var isResourceBuddied = await IsResourceBuddied(resid).ConfigureAwait(false);

            if (isResourceBuddied)
            {
                throw new AcdException("E_VAL", "Resource is already buddied.");
            }

            var workflowEntry = new AcdWorkflowEntry
            {
                AcdWorkflow = WorkflowOptions.Buddy.ToString(),
                AcdWorkflowState = (short)BuddyAclWorkflows.Grant,
                CreatedTimestamp = DateTimeOffset.UtcNow,
                CreatedUserId = _client.UserId,
                CreatedUsername = _client.Username,
                Data = JsonConvert.SerializeObject(acds),
                Id = Guid.NewGuid(),
                ResourceId = resid
            };

            await _repository.AcdWorkflowRepository.Put(workflowEntry).ConfigureAwait(false);

            var currentAcds = _repository.AcdRepository.Get(resid).ToEnumerable().ToList();

            var resourceMetadata = await _repository.ResourceRepository.GetMetadata(resid).ConfigureAwait(false);

            var displayText = resourceMetadata.ResourceSet == "SEA"
                                        ? string.IsNullOrEmpty(resourceMetadata.Description) ? resourceMetadata.Uri : resourceMetadata.Description
                                        : resourceMetadata.Uri;

            var _auditId = Guid.NewGuid();
            var auditEntry = new AuditEntry
            {
                Action = AcdActions.Create,
                AuditId = _auditId,
                DisplayText = displayText,
                InId = resid,
                ModifiedUsername = _client.Username,
                OutId = resid,
                ResourceName = resourceMetadata.Name,
                ResourceType = resourceMetadata.Type,
                Timestamp = DateTimeOffset.UtcNow,
                State = AcdStates.Pending,
            };
            var auditDataEntry = new AuditDataEntry
            {
                AuditId = _auditId,
                DataNew = JsonConvert.SerializeObject(acds),
                DataOld = JsonConvert.SerializeObject(currentAcds)
            };

            await _repository.AuditsRepository.Put(auditEntry).ConfigureAwait(false);
            await _repository.AuditDataRepository.Put(auditDataEntry).ConfigureAwait(false);
        }

        public async Task ApproveAcl(Guid resid, Guid jobStatusId, EventBus eventBusInstance = null)
        {
            await AssertPermission(resid, Permissions.SecurityWrite).ConfigureAwait(false);

            var workflow = await ValidateAndGetWorkflow(resid).ConfigureAwait(false);

            if (string.IsNullOrEmpty(workflow.DisplayText))
            {
                var workflowData = JsonConvert.DeserializeObject<List<AcdEntry>>(workflow.Data);

                await ApplyAcl(workflowData, jobStatusId, true, eventBusInstance).ConfigureAwait(false);

                await _repository.AcdWorkflowRepository.Delete(workflow).ConfigureAwait(false);
            }
            else
            {
                //throw exception so that client can call a different api
                throw new AcdException("E_COPY_ACL", "Please call copy acl with approve flag");
            }
        }

        public async Task RejectAcl(Guid resid)
        {
            await AssertPermission(resid, Permissions.SecurityWrite).ConfigureAwait(false);

            var workflow = await ValidateAndGetWorkflow(resid, true).ConfigureAwait(false);

            if (!string.IsNullOrEmpty(workflow.DisplayText))
            {
                throw new AcdException("E_COPY_ACL", "Please call copy acl with reject flag");
            }

            var workflowData = JsonConvert.DeserializeObject<List<AcdEntry>>(workflow.Data);

            await _repository.AcdWorkflowRepository.Delete(workflow).ConfigureAwait(false);

            var currentAcds = _repository.AcdRepository.Get(resid).ToEnumerable().ToList();
            var resourceMetadata = await _repository.ResourceRepository.GetMetadata(resid).ConfigureAwait(false);

            var displayText = resourceMetadata.ResourceSet == "SEA"
                                        ? string.IsNullOrEmpty(resourceMetadata.Description) ? resourceMetadata.Uri : resourceMetadata.Description
                                        : resourceMetadata.Uri;
            var _auditId = Guid.NewGuid();
            var auditEntry = new AuditEntry
            {
                Action = AcdActions.RejectBuddy,
                AuditId = _auditId,                
                DisplayText = displayText,
                InId = resid,
                ModifiedUsername = _client.Username,
                OutId = resid,
                ResourceName = resourceMetadata.Name,
                ResourceType = resourceMetadata.Type,
                Timestamp = DateTimeOffset.UtcNow,
                State = AcdStates.Completed,
            };
            var auditDataEntry = new AuditDataEntry
            {
                AuditId = _auditId,
                DataNew = JsonConvert.SerializeObject(currentAcds),
                DataOld = JsonConvert.SerializeObject(workflowData)
            };

            await _repository.AuditsRepository.Put(auditEntry).ConfigureAwait(false);
            await _repository.AuditDataRepository.Put(auditDataEntry).ConfigureAwait(false);
        }

        public IObservable<AuditEntry> GetAudits(Guid? resid, int skip, int take, AcdActions? action, string updatedby, string resname, string displayText, DateTimeOffset? from, DateTimeOffset? to, AuditsSortByOptions sortBy, SortDirection sortOrder, QueryOperator op, string path, string skey)
        {
            var result = _repository.AuditsRepository.Get(_client.UserIdentities, resid, action, updatedby, resname, displayText, from, to, skip, take, sortBy, sortOrder, op, path, skey);
            return result.Where(x => resid != null || x.ResourceType != "channel");
        }

        public Task<long> GetAuditsCount(Guid? resid, AcdActions? action, string updatedby, string resname, string displayText, DateTimeOffset? from, DateTimeOffset? to, QueryOperator op, string path, string skey)
        {
            return _repository.AuditsRepository.Count(_client.UserIdentities, resid, action, updatedby, resname, displayText, from, to, op, path, skey);
        }

        public IObservable<AcdEntry> SearchAcds(Guid resid, int skip, int take, AcdSortByOptions sortBy, SortDirection sortOrder, string searchKey)
        {
            AssertPermission(resid, Permissions.SecurityRead).Wait();
            return _repository.AcdRepository.Get(resid, skip, take, sortBy, sortOrder, searchKey);
        }

        public IObservable<AcdEntry> SearchAcds(Guid resid, string searchKey)
        {
            AssertPermission(resid, Permissions.SecurityRead).Wait();
            return _repository.AcdRepository.Get(resid, searchKey);
        }

        public IObservable<AcdEntry> SearchAcds(Guid resid, int skip, int take, string searchKey)
        {
            AssertPermission(resid, Permissions.SecurityRead).Wait();
            return _repository.AcdRepository.Get(resid, skip, take, searchKey);
        }

        public Task<long> SearchAcdsCount(Guid resid, string searchKey)
        {
            AssertPermission(resid, Permissions.SecurityRead).Wait();
            return _repository.AcdRepository.GetCount(resid, searchKey);
        }

        public IObservable<AcdEntry> GetAcds(Guid resid)
        {
            return _repository.AcdRepository.Get(resid);
        }

        public IObservable<AcdEntry> GetAcds(Guid resid, Guid userId)
        {
            return _repository.AcdRepository.Get(resid, userId);
        }

        public Task<Guid> BuddiedUserId(Guid resourceId)
        {
            return _repository.AcdWorkflowRepository.GetWorkflowUserId(resourceId);
        }

        public Task<string> GetWorkflowOption(Guid resourceId)
        {
            return _repository.AcdWorkflowRepository.GetWorkflowOption(resourceId);
        }

        public Task<AuditEntry> GetAudit(Guid resid, Guid auditId)
        {
            return _repository.AuditsRepository.Get(resid, auditId);
        }

        public Task<AuditDataEntry> GetAuditData(Guid resid, Guid auditId)
        {
            return _repository.AuditDataRepository.Get(auditId);
        }

        public Task DeleteAll(Guid resIid)
        {
            var tasks = new List<Task>();
            tasks.Add(_repository.AcdRepository.DeleteAll(resIid));
            tasks.Add(_repository.AcdSetRepository.Delete(resIid));
            tasks.Add(_repository.AcdWorkflowRepository.Delete(resIid));
            return Task.WhenAll(tasks.ToArray());
        }

        public async Task<IEnumerable<AcdSetEntry>> GetAcdSets(Guid id, Permissions permission)
        {
            await AssertPermission(id, Permissions.SecurityRead).ConfigureAwait(false);
            return _repository.AcdSetRepository.Get(id, permission).ToEnumerable();
        }

        public IPermissionService PermissionService
        {
            get { return _permissionService; }
        }

        public IResourceCreateService ResourceCreateService
        {
            get { return _resourceCreateService; }
        }

        private async Task AssertPermission(Guid resid, Permissions permission)
        {
            var has = await _repository.PermissionsRepository.Has(resid, _client.UserIdentities, permission).ConfigureAwait(false);
            if (!has)
            {
                throw new AcdException("E_PERMISSION", "You have no permission to invoke that API.");
            }
        }

        private Task<bool> IsResourceBuddied(Guid resid)
        {
            return _repository.AcdWorkflowRepository.HasWorkflow(resid);
        }

        private async Task BreakInheritanceChain(Guid resid, IEnumerable<AcdEntry> newAcds, IEnumerable<AcdEntry> oldAcds)
        {
            var oldAcdDict = oldAcds.ToDictionary(e => e.AcdId, e => e);
            IDictionary<Guid, ResourceEntry> resourceChildren = null;
            foreach (var acd in newAcds)
            {
                if (oldAcdDict.ContainsKey(acd.AcdId) && oldAcdDict[acd.AcdId].InheritedFrom != Guid.Empty)
                {
                    var allInheritors = _repository.AcdRepository.GetInheritors(oldAcdDict[acd.AcdId].InheritedFrom).ToEnumerable();

                    if (resourceChildren == null)
                    {
                        resourceChildren = _repository.ResourceRepository.GetChildren(resid).ToEnumerable().ToDictionary(e => e.ResourceId, e => e);
                    }

                    foreach (var inheritor in allInheritors)
                    {
                        if (resourceChildren.ContainsKey(inheritor.ResourceId))
                        {
                            inheritor.InheritedFrom = acd.AcdId;
                            await _repository.AcdRepository.Put(inheritor).ConfigureAwait(false);
                        }
                    }
                }
            }
        }

        private async Task<AcdWorkflowEntry> ValidateAndGetWorkflow(Guid resid, bool isReject = false, bool isCopyAcl = false, bool isReplaceAcl = false)
        {
            var isBuddyUser = IsBuddyUser(_client.UserId);
            var isResouceBuddied = await IsResourceBuddied(resid).ConfigureAwait(false);

            if (!isBuddyUser || !isResouceBuddied)
            {
                throw new AcdException("E_VAL", "Neither resource is buddied or user is buddy");
            }

            var workflowOption = isCopyAcl
                                    ? WorkflowOptions.CopyAcl.ToString()
                                    : isReplaceAcl ? WorkflowOptions.ReplaceAcd.ToString() : WorkflowOptions.Buddy.ToString();

            var workflow = await _repository.AcdWorkflowRepository.GetBuddiedAcd(resid, workflowOption).ConfigureAwait(false);

            if (workflow != null && workflow.CreatedUserId == _client.UserId && !isReject)
            {
                throw new AcdException("E_VAL", "Buddy cannot approve himself");
            }

            return workflow;
        }

        private async Task<HashSet<Guid>> GetWorkflowForCopyAcl(List<Guid> resids)
        {
            var workflowOption = WorkflowOptions.Buddy.ToString();
            var workflow = await _repository.AcdWorkflowRepository.HasWorkflows(resids, workflowOption).ConfigureAwait(false);

            return workflow;
        }

        public List<string> GetIdentities(Guid Id, string type)
        {
            var restClient = new RestClient(ConfigurationManager.AppSettings["SecurityAPI"]);
            var request = type == "u" ? new RestRequest("principals/groups/" + Id) : new RestRequest("principals/users/" + Id);
            request.AddHeader("SamlToken", _client.Token.Replace("\r\n", ""));
            var response = restClient.Execute(request);

            JArray array = JArray.Parse(response.Content);

            var principalIds = array.Select(x => (x["Id"]).ToString()).ToList();
            principalIds.Add(Id.ToString());
            return principalIds;
        }

        public Task UpdatePrincipal(Guid resid, Guid old, Guid @new, PrincipalTypes principalType, string principalName)
        {
            return _principalUpdateService.Handle(resid, old, @new, principalType, principalName);
        }

        public Task<ResourceMetadataEntry> GetResourceMetadata(Guid resid)
        {
            return _repository.ResourceRepository.GetMetadata(resid);
        }

        public Task CopyAcl(Guid jobId, Guid source, Guid destination, Dictionary<Guid, Guid> maps)
        {
            var copyService = new AclCopyService(_repository, _client, jobId);
            return copyService.Apply(source, destination, maps.Select(e => Tuple.Create(e.Key, e.Value)).ToList());
        }

        public Task ApproveCopyAcl(Guid job, Guid resid)
        {
            var copyService = new AclCopyService(_repository, _client, job);
            return copyService.Approve(resid);
        }

        public Task RejectCopyAcl(Guid job, Guid resid)
        {
            var copyService = new AclCopyService(_repository, _client, job);
            return copyService.Reject(resid);
        }

        public async Task<List<AcdEntry>> ReviewCopyAcl(Guid resid)
        {
            var workflow = await ValidateAndGetWorkflow(resid, true, isCopyAcl: true).ConfigureAwait(false);

            var allInTheGroup = _repository.AcdWorkflowRepository.GetAcdWorkflowsByDisplayText(workflow.DisplayText).ToEnumerable();

            var workflowData = JsonConvert.DeserializeObject<NodeData>(workflow.Data);

            var oldAcds = workflowData.OldAcds.Where(x => x.InheritedFrom == Guid.Empty).ToList();

            var filteredAcds = workflowData.SourceAcds.Except(oldAcds).ToList();

            return filteredAcds;
        }

        public IEnumerable<Guid> GetInheritedResources(Guid resid, int skip, int take, Guid acdId, bool isUp = false, bool isInheritedFromParent = false)
        {
            var acd = _repository.AcdRepository.GetById(acdId).Result;

            var result = new List<Guid>();

            if (isUp)
            {
                GetParentAcds(resid, acdId, acd.InheritedFrom, result, isInheritedFromParent);
            }
            else
            {
                GetChildAcds(resid, acdId, acd.InheritedFrom, result, isInheritedFromParent);
            }
            return result;
        }

        public IObservable<ResourceMetadataEntry> GetResourceMetadata(IEnumerable<Guid> resids, int skip, int take)
        {
            return _repository.ResourceRepository.GetMetadata(resids, skip, take);
        }

        private List<Guid> GetParentAcds(Guid resid, Guid acdId, Guid? inheritedFromId, List<Guid> result, bool isInheritFromParent = false)
        {
            var parent = _repository.ResourceRepository.GetParent(resid);
            if (parent.Result == null)
            {
                return result;
            }
            var parentAcds = _repository.AcdRepository.Get(parent.Result.ResourceId).ToEnumerable().ToList();
            result.AddRange(parentAcds.Where(x => (!isInheritFromParent && (x.InheritedFrom == Guid.Empty ? x.AcdId == inheritedFromId : x.InheritedFrom == inheritedFromId)) || (x.OnChildCreate == OnChildCreateOptions.Never && (inheritedFromId != Guid.Empty && x.AcdId == inheritedFromId))).Select(x => x.ResourceId));
            if (parentAcds.Any(x => x.AcdId == inheritedFromId))
            {
                return result;
            }
            else
            {
                GetParentAcds(parent.Result.ResourceId, acdId, inheritedFromId, result, isInheritFromParent);
            }
            return result;
        }

        private List<Guid> GetChildAcds(Guid resid, Guid acdId, Guid? inheritedFromId, List<Guid> result, bool isInheritFromParent = false)
        {
            var children = _repository.ResourceRepository.GetChildren(resid).ToEnumerable().ToList();
            var childAcds = _repository.AcdRepository.GetAcds(children.Select(x => x.ResourceId).ToList()).ToEnumerable().ToList();


            foreach (var child in children)
            {
                var acds = childAcds.Where(x => x.ResourceId == child.ResourceId).ToList();
                result.AddRange(acds.Where(x => (!isInheritFromParent && (inheritedFromId == Guid.Empty ? x.InheritedFrom == acdId : x.InheritedFrom == inheritedFromId)) || (x.OnChildCreate == OnChildCreateOptions.Never && x.InheritedFrom == acdId)).Select(x => x.ResourceId));
            }
            return result.Distinct().ToList();
        }

        public async Task CheckAndBuddyReplacePrincipal(Guid resid, Guid principalId, string principalName, PrincipalTypes principalType, IEnumerable<AcdEntry> acds)
        {
            await AssertPermission(resid, Permissions.SecurityWrite).ConfigureAwait(false);
            var isResourceBuddied = await IsResourceBuddied(resid).ConfigureAwait(false);

            if (isResourceBuddied)
            {
                throw new AcdException("E_VAL", "Resource is already buddied.");
            }

            var replacePrincipalDetail = new ReplacePrincipalDetail
            {
                PrincipalId = principalId,
                PrincipalName = principalName,
                PrincipalType = principalType,
                ResourceId = resid
            };

            var workflowEntry = new AcdWorkflowEntry
            {
                AcdWorkflow = WorkflowOptions.ReplaceAcd.ToString(),
                AcdWorkflowState = (short)BuddyAclWorkflows.Grant,
                CreatedTimestamp = DateTimeOffset.UtcNow,
                CreatedUserId = _client.UserId,
                CreatedUsername = _client.Username,
                Data = JsonConvert.SerializeObject(acds),
                Id = Guid.NewGuid(),
                ResourceId = resid,
                DisplayText = JsonConvert.SerializeObject(replacePrincipalDetail)
            };

            await _repository.AcdWorkflowRepository.Put(workflowEntry).ConfigureAwait(false);

            var currentAcds = _repository.AcdRepository.Get(resid).ToEnumerable().ToList();

            var resourceMetadata = await _repository.ResourceRepository.GetMetadata(resid).ConfigureAwait(false);

            var displayText = resourceMetadata.ResourceSet == "SEA"
                                        ? string.IsNullOrEmpty(resourceMetadata.Description) ? resourceMetadata.Uri : resourceMetadata.Description
                                        : resourceMetadata.Uri;
            var _auditId = Guid.NewGuid();
            var auditEntry = new AuditEntry
            {
                Action = AcdActions.Create,
                AuditId = _auditId,
                DisplayText = displayText,
                InId = resid,
                ModifiedUsername = _client.Username,
                OutId = resid,
                ResourceName = resourceMetadata.Name,
                ResourceType = resourceMetadata.Type,
                Timestamp = DateTimeOffset.UtcNow,
                State = AcdStates.Pending,
            };

            var auditDataEntry = new AuditDataEntry
            {
                AuditId = _auditId,
                DataNew = JsonConvert.SerializeObject(acds),
                DataOld = JsonConvert.SerializeObject(currentAcds)
            };

            await _repository.AuditsRepository.Put(auditEntry).ConfigureAwait(false);
            await _repository.AuditDataRepository.Put(auditDataEntry).ConfigureAwait(false);
        }

        public async Task ApplyReplacePrincipal(Guid resid, Guid principalId, string principalName, PrincipalTypes principalType, IEnumerable<AcdEntry> acds, Guid statusId, bool approval = false)
        {
            var deletedAcds = new List<AcdEntry>();

            try
            {
                var result = new List<AcdEntry>();
                var copiedAcds = new List<AcdEntry>();
                var childResourceIds = new List<Guid>();

                result = _repository.AcdRepository.Get(resid).ToEnumerable().ToList();

                //create a same copy of acd 
                await CopyPrincipalForAcds(resid, principalId, principalName, principalType, acds, result, copiedAcds, childResourceIds).ConfigureAwait(false);

                DeleteAcdsofOldPrincipal(acds, deletedAcds, result);

                var groupedResources = copiedAcds.GroupBy(x => x.ResourceId).ToList();

                await ValidateCopiedAcds(groupedResources).ConfigureAwait(false);

                ServiceStatus.SetTotalOperations(statusId, Convert.ToInt32(copiedAcds.Count() + deletedAcds.Count()));
                ServiceStatus.IncrementResources(statusId, Convert.ToInt32(copiedAcds.Count() + deletedAcds.Count()));
                ServiceStatus.SetFlag(statusId, ServiceStatusFlags.Started);

                foreach (var resource in groupedResources)
                {
                    await ApplyAcl(resource.Select(x => x).ToList(), statusId, approval).ConfigureAwait(false);

                    ResourceLocker.Unlock(statusId);
                }
                ResourceLocker.Unlock(statusId);

                var groupedDeletedAcds = deletedAcds.GroupBy(x => x.ResourceId).ToList();

                foreach (var deletedAcd in groupedDeletedAcds)
                {
                    await ApplyAcl(deletedAcd, statusId, approval).ConfigureAwait(false);

                    ResourceLocker.Unlock(statusId);
                }

                await ProcessBuddyWorkflowForChildAcds(resid, childResourceIds, acds, principalId, principalName, principalType).ConfigureAwait(false);
            }
            finally
            {
                ResourceLocker.Unlock(statusId);
                Task.Run(async () =>
                {
                    await Task.Delay(1000).ConfigureAwait(false);
                    ServiceStatus.Remove(statusId);
                });
            }

        }

        private async Task ProcessBuddyWorkflowForChildAcds(Guid resid, List<Guid> childResourceIds, IEnumerable<AcdEntry> acds, Guid principalId, string principalName, PrincipalTypes principalType)
        {
            var resourceIds = await GetWorkflowForCopyAcl(childResourceIds).ConfigureAwait(false);

            foreach (var acd in acds)
            {
                foreach (var resourceId in resourceIds)
                {
                    var buddyWorkflow = await _repository.AcdWorkflowRepository.GetBuddiedAcd(resourceId).ConfigureAwait(false);
                    if (buddyWorkflow != null)
                    {
                        var childResourceAcds = _repository.AcdRepository.Get(resourceId).ToEnumerable().ToList();
                        var acdId = childResourceAcds.Where(x => x.PrincipalId == principalId && x.PrincipalName == principalName && x.PrincipalType == principalType && x.ResourceType == acd.ResourceType && x.OnChildCreate == acd.OnChildCreate).Select(x => x.AcdId).FirstOrDefault();

                        var workflowData = JsonConvert.DeserializeObject<List<AcdEntry>>(buddyWorkflow.Data).FirstOrDefault();
                        if (workflowData != null && (workflowData.PrincipalId == acd.PrincipalId && workflowData.PrincipalName == acd.PrincipalName && workflowData.PrincipalType == acd.PrincipalType))
                        {
                            workflowData.PrincipalId = principalId;
                            workflowData.PrincipalName = principalName;
                            workflowData.PrincipalType = principalType;
                            workflowData.AcdId = acdId;
                            var data = new List<AcdEntry> { workflowData };

                            var workflowEntry = new AcdWorkflowEntry
                            {
                                AcdWorkflow = WorkflowOptions.Buddy.ToString(),
                                AcdWorkflowState = (short)BuddyAclWorkflows.Grant,
                                CreatedTimestamp = DateTimeOffset.UtcNow,
                                CreatedUserId = buddyWorkflow.CreatedUserId,
                                CreatedUsername = buddyWorkflow.CreatedUsername,
                                Data = JsonConvert.SerializeObject(data),
                                Id = Guid.NewGuid(),
                                ResourceId = resourceId
                            };

                            await _repository.AcdWorkflowRepository.Put(workflowEntry).ConfigureAwait(false);
                        }
                        await _repository.AcdWorkflowRepository.Delete(buddyWorkflow).ConfigureAwait(false);
                    }
                }
            }
        }

        private async Task ValidateCopiedAcds(List<IGrouping<Guid, AcdEntry>> groupedResources)
        {
            foreach (var resource in groupedResources)
            {
                var existingAcds = _repository.AcdRepository.Get(resource.Key).ToEnumerable().ToList().Select(x => new { x.PrincipalId, x.ResourceType });
                var groupedSourceAcd = resource.Select(x => new { x.PrincipalId, x.ResourceType }).FirstOrDefault();
                if (existingAcds.Any(x => x.PrincipalId == groupedSourceAcd.PrincipalId && x.ResourceType == groupedSourceAcd.ResourceType))
                {
                    throw new AcdException("E_VAL", "Can't add Acds of same user and resource type");
                }
            }
        }

        public async Task<List<AcdEntry>> ReviewReplacePrincipal(Guid resid, Guid currentResid)
        {
            var workflow = await ValidateAndGetWorkflow(resid, true, isReplaceAcl: true).ConfigureAwait(false);

            var replacePrincipalDetail = JsonConvert.DeserializeObject<ReplacePrincipalDetail>(workflow.DisplayText);

            var workflowData = JsonConvert.DeserializeObject<List<AcdEntry>>(workflow.Data);

            return GetNewPrincipalForReview(resid, currentResid, replacePrincipalDetail, workflowData).ToList();
        }

        private List<AcdEntry> GetNewPrincipalForReview(Guid resid, Guid currentResid, ReplacePrincipalDetail replacePrincipalDetail, List<AcdEntry> workflowData)
        {
            var result = _repository.AcdRepository.Get(currentResid).ToEnumerable().ToList();
            var acdsToRemoved = new List<AcdEntry>();
            var copiedAcds = new List<AcdEntry>();

            foreach (var acd in workflowData)
            {
                var acdsToProcess = result.Where(x => x.PrincipalId == acd.PrincipalId && x.PrincipalType == acd.PrincipalType && x.PrincipalName == acd.PrincipalName).ToList();

                foreach (var acdToProcess in acdsToProcess)
                {
                    var copiedAcd = acdToProcess.Clone();
                    copiedAcd.AcdId = Guid.Empty;
                    copiedAcd.PrincipalId = replacePrincipalDetail.PrincipalId;
                    copiedAcd.PrincipalName = replacePrincipalDetail.PrincipalName;
                    copiedAcd.PrincipalType = replacePrincipalDetail.PrincipalType;
                    copiedAcd.ModifiedForReplacePrincipal = true;
                    copiedAcds.Add(copiedAcd);
                }
                acdsToRemoved.AddRange(acdsToProcess);
            }
            foreach (var acd in copiedAcds)
            {
                result.Add(acd);
            }
            foreach (var acd in acdsToRemoved)
            {
                result.Remove(acd);
            }
            return result;
        }

        public async Task ApproveReplacePrincipal(Guid resid, Guid statusId)
        {
            var workflow = await ValidateAndGetWorkflow(resid, isReplaceAcl: true).ConfigureAwait(false);

            var workflowData = JsonConvert.DeserializeObject<List<AcdEntry>>(workflow.Data);

            var replacePrincipalDetail = JsonConvert.DeserializeObject<ReplacePrincipalDetail>(workflow.DisplayText);

            await ApplyReplacePrincipal(resid, replacePrincipalDetail.PrincipalId, replacePrincipalDetail.PrincipalName, replacePrincipalDetail.PrincipalType, workflowData, statusId, true);

            await _repository.AcdWorkflowRepository.Delete(workflow).ConfigureAwait(false);
        }

        public async Task RejectReplacePrincipal(Guid resid)
        {
            await AssertPermission(resid, Permissions.SecurityWrite).ConfigureAwait(false);

            var workflow = await ValidateAndGetWorkflow(resid, true, isReplaceAcl: true).ConfigureAwait(false);
            var workflowData = JsonConvert.DeserializeObject<List<AcdEntry>>(workflow.Data);

            await _repository.AcdWorkflowRepository.Delete(workflow).ConfigureAwait(false);

            var currentAcds = _repository.AcdRepository.Get(resid).ToEnumerable().ToList();
            var resourceMetadata = await _repository.ResourceRepository.GetMetadata(resid).ConfigureAwait(false);

            var displayText = resourceMetadata.ResourceSet == "SEA"
                                        ? string.IsNullOrEmpty(resourceMetadata.Description) ? resourceMetadata.Uri : resourceMetadata.Description
                                        : resourceMetadata.Uri;

            var _auditId = Guid.NewGuid();
            var auditEntry = new AuditEntry
            {
                Action = AcdActions.RejectBuddy,
                AuditId = _auditId,
                DisplayText = displayText,
                InId = resid,
                ModifiedUsername = _client.Username,
                OutId = resid,
                ResourceName = resourceMetadata.Name,
                ResourceType = resourceMetadata.Type,
                Timestamp = DateTimeOffset.UtcNow,
                State = AcdStates.Completed,
            };

            var auditDataEntry = new AuditDataEntry
            {
                AuditId = _auditId,
                DataNew = JsonConvert.SerializeObject(currentAcds),
                DataOld = JsonConvert.SerializeObject(workflowData),
            };

            await _repository.AuditsRepository.Put(auditEntry).ConfigureAwait(false);
            await _repository.AuditDataRepository.Put(auditDataEntry).ConfigureAwait(false);
        }

        private async Task CopyPrincipalForAcds(Guid resid, Guid principalId, string principalName, PrincipalTypes principalType, IEnumerable<AcdEntry> acds, List<AcdEntry> result, List<AcdEntry> copiedAcds, List<Guid> childResourceIds = null)
        {
            foreach (var acd in acds)
            {
                await GetChildAcds(resid, result, childResourceIds).ConfigureAwait(false);

                //await ProcessBuddyWorkflowForChildAcds(resid, childResourceIds, acd, principalId, principalName, principalType).ConfigureAwait(false);

                var acdsToProcess = result.Where(x => x.PrincipalId == acd.PrincipalId && x.PrincipalType == acd.PrincipalType && x.PrincipalName == acd.PrincipalName).ToList();

                foreach (var acdToProcess in acdsToProcess)
                {
                    //also change principal name and principal type
                    var copiedAcd = acdToProcess.Clone();
                    copiedAcd.AcdId = Guid.Empty;
                    copiedAcd.PrincipalId = principalId;
                    copiedAcd.PrincipalName = principalName;
                    copiedAcd.PrincipalType = principalType;
                    copiedAcd.InheritedFrom = Guid.Empty;
                    copiedAcds.Add(copiedAcd);
                }
            }
        }

        private void DeleteAcdsofOldPrincipal(IEnumerable<AcdEntry> acds, List<AcdEntry> deletedAcds, List<AcdEntry> result)
        {
            foreach (var acd in acds)
            {
                var acdsToProcess = result.Where(x => x.PrincipalId == acd.PrincipalId && x.PrincipalType == acd.PrincipalType && x.PrincipalName == acd.PrincipalName).ToList();

                foreach (var acdToProcess in acdsToProcess)
                {
                    acdToProcess.Permissions.Remove(0);
                    acdToProcess.Permissions.Remove(1);
                    acdToProcess.Permissions.Remove(2);
                    acdToProcess.Permissions.Remove(3);
                    acdToProcess.Permissions.Remove(4);
                    acdToProcess.Permissions.Remove(5);
                    acdToProcess.Permissions.Remove(6);
                    acdToProcess.Permissions.Remove(7);
                    acdToProcess.Permissions.Remove(8);
                    deletedAcds.Add(acdToProcess);
                }
            }
        }

        private async Task<List<Guid>> GetChildAcds(Guid resid, List<AcdEntry> result, List<Guid> childIds)
        {
            var children = _repository.ResourceRepository.GetChildren(resid).ToEnumerable().ToList();

            foreach (var child in children)
            {
                var acds = _repository.AcdRepository.Get(child.ResourceId).ToEnumerable().ToList();
                childIds.Add(child.ResourceId);
                result.AddRange(acds.Where(x => x.InheritedFrom == Guid.Empty));
                await GetChildAcds(child.ResourceId, result, childIds).ConfigureAwait(false);
            }
            return childIds;
        }

        public async Task<List<ResourceMetadataEntry>> GetResourceDescendants(Guid resid, int skip, int take)
        {
            var workflow = await ValidateAndGetWorkflow(resid, true, isReplaceAcl: true).ConfigureAwait(false);

            var replacePrincipalDetail = JsonConvert.DeserializeObject<ReplacePrincipalDetail>(workflow.DisplayText);

            var workflowData = JsonConvert.DeserializeObject<List<AcdEntry>>(workflow.Data);

            var groupedResourceIds = new List<Guid>();

            var resource = _repository.ResourceRepository.GetMetadata(resid).Result;
            var types = resource.ResourceSet.ToLower() == "sea" ? GetSeaTypes() : GetWitsmlTypes();


            foreach (var acd in workflowData)
            {
                await GetChildrenWithValidAcds(resid, groupedResourceIds, acd, types).ConfigureAwait(false);
            }
            groupedResourceIds.Add(resid);

            return _repository.ResourceRepository.GetMetadata(groupedResourceIds, skip, take).ToEnumerable().ToList();
        }

        public async Task<List<ResourceMetadataEntry>> GetResourceDescendantsForCopyAcl(Guid resid, int skip, int take)
        {
            var workflow = await ValidateAndGetWorkflow(resid, true, isCopyAcl: true).ConfigureAwait(false);

            var allInTheGroup = _repository.AcdWorkflowRepository.GetAcdWorkflowsByDisplayText(workflow.DisplayText).ToEnumerable();

            var resourceIds = allInTheGroup.Select(x => x.ResourceId).ToList();

            return _repository.ResourceRepository.GetMetadata(resourceIds, skip, take).ToEnumerable().ToList();
        }

        private async Task<List<Guid>> GetChildrenWithValidAcds(Guid resid, List<Guid> result, AcdEntry acd, string[] types)
        {
            var children = _repository.ResourceRepository.GetChildren(resid).ToEnumerable().ToList();
            foreach (var child in children)
            {
                if (!types.Contains(child.ResourceType))
                {
                    continue;
                }
                var acds = _repository.AcdRepository.Get(child.ResourceId).ToEnumerable().ToList();
                var validAcds = acds.Where(x => x.PrincipalId == acd.PrincipalId && x.PrincipalName == acd.PrincipalName && x.PrincipalType == acd.PrincipalType);
                result.AddRange(validAcds.Select(x => x.ResourceId).Distinct().ToList());
                Parallel.Invoke(() => GetChildrenWithValidAcds(child.ResourceId, result, acd, types));
            }
            return result;
        }

        private static string[] GetSeaTypes()
        {
            var types = new[] { "Folder" };
            return types;
        }

        private static string[] GetWitsmlTypes()
        {
            var types = new[] {"well","wellLog","wellbore","log","mudlog","trajectoryheader","attachment","bhaRun","cementJob","objectGroup","convCore","drillReport","fluidsReport","formationMarker","opsReport","rig","risk","sidewallCore","stimJob","surveyProgram","target","tubular","wbGeometry","coordinateReferenceSystem","toolErrorModel","toolErrorTermSet","dtsInstalledSystem","dtsMeasurement","realtime","virtualLog"
            };

            return types;
        }


        public Task<Dictionary<Guid, List<string>>> GetMultiplePermissions(List<Guid> resourceIds, List<Guid> principalIds, List<Permissions> permissions = null)
        {
            return _repository.PermissionsRepository.GetMultiplePermissions(resourceIds, principalIds, permissions);
        }

        public IObservable<AcdEntry> GetAcds(List<Guid> resids, Guid inheritedFrom)
        {
            return _repository.AcdRepository.GetAcds(resids, inheritedFrom);
        }

        public Task<AcdEntry> GetAcdById(Guid acdId)
        {
            return _repository.AcdRepository.GetById(acdId);
        }
    }
}
