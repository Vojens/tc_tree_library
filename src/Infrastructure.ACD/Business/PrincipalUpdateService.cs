﻿using Infrastructure.ACD.Repository;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;
using Infrastructure.ACD.Entities;

namespace Infrastructure.ACD.Business
{
    public interface IPrincipalUpdateService
    {
        Task Handle(Guid resid, Guid old, Guid @new, PrincipalTypes principalType, string principalName);
    }

    public class PrincipalUpdateService : IPrincipalUpdateService
    {
        private ICompositeAclRepository _repository;
        private IPermissionService _permissionService;
        private IGraphClientInfo _client;

        const string _buddyWorkflow = "Buddy";

        public PrincipalUpdateService(ICompositeAclRepository repository, IPermissionService permissionService,
            IGraphClientInfo client)
        {
            _repository = repository;
            _permissionService = permissionService;
            _client = client;
        }

        public async Task Handle(Guid resourceId, Guid oldPrincipalId, Guid newPrincipalId, PrincipalTypes principalType, string principalName)
        {
            await Validate(resourceId, oldPrincipalId, newPrincipalId).ConfigureAwait(false);

            await ProcessResource(resourceId, oldPrincipalId, newPrincipalId, principalName, principalType).ConfigureAwait(false);

            await ProcessResourceTree(resourceId, oldPrincipalId, newPrincipalId, principalName, principalType).ConfigureAwait(false);
        }

        private async Task ProcessResourceTree(Guid resourceId, Guid oldPrincipalId, Guid newPrincipalId, string principalName, PrincipalTypes principalType)
        {
            var children = _repository.ResourceRepository.GetChildren(resourceId).ToEnumerable();

            foreach (var resource in children)
            {
                if (await _permissionService.HasPermission(resource.ResourceId, _client.UserIdentities, Permissions.SecurityWrite).ConfigureAwait(false))
                {
                    if (! await _repository.AcdWorkflowRepository.HasWorkflow(resource.ResourceId, _buddyWorkflow).ConfigureAwait(false))
                    {
                        await ProcessResource(resource.ResourceId, oldPrincipalId, newPrincipalId, principalName, principalType).ConfigureAwait(false);
                    }
                }

                await ProcessResourceTree(resource.ResourceId, oldPrincipalId, newPrincipalId, principalName, principalType).ConfigureAwait(false);
            }
        }

        private async Task ProcessResource(Guid resourceId, Guid oldPrincipalId, Guid newPrincipalId, string principalName, PrincipalTypes type)
        {
            var acdsOfOldPrincipalId = _repository.AcdRepository.Get(resourceId, oldPrincipalId).ToEnumerable();
            var acdCreatesOfOldPrincipalId = _repository.AcdSetRepository.GetAcdCreateEntries(resourceId, newPrincipalId).ToEnumerable();

            foreach (var acd in acdsOfOldPrincipalId)
            {
                var acdSetEntries = _repository.AcdSetRepository.GetByAcdId(acd.AcdId).ToEnumerable();

                foreach (var acdSetEntry in acdSetEntries)
                {
                    await _repository.AcdSetRepository.Delete(acdSetEntry.InId, (Permissions)acdSetEntry.Permission, acdSetEntry.PrincipalId, acdSetEntry.AcdId).ConfigureAwait(false);
                    acdSetEntry.PrincipalId = newPrincipalId;
                    await _repository.AcdSetRepository.Put(acdSetEntry).ConfigureAwait(false);
                }

                await _repository.AcdRepository.Delete(acd.ResourceId, acd.AcdId).ConfigureAwait(false);

                acd.PrincipalId = newPrincipalId;
                acd.PrincipalName = principalName;
                acd.PrincipalType = type;
                await _repository.AcdRepository.Put(acd).ConfigureAwait(false);
            }

            foreach (var acdCreateSet in acdCreatesOfOldPrincipalId)
            {
                await _repository.AcdSetRepository.Delete(acdCreateSet.ResourceId, acdCreateSet.ResourceType, acdCreateSet.PrincipalId, acdCreateSet.AcdId).ConfigureAwait(false);
                acdCreateSet.PrincipalId = newPrincipalId;
                await _repository.AcdSetRepository.Put(acdCreateSet).ConfigureAwait(false);
            }
        }

        private async Task Validate(Guid resourceId, Guid oldPrincipalId, Guid newPrincipalId)
        {
            Preconditions.CheckNeitherNullNorDefault(resourceId);
            Preconditions.CheckNeitherNullNorDefault(oldPrincipalId);
            Preconditions.CheckNeitherNullNorDefault(newPrincipalId);
            Preconditions.EnsureNotEquals(oldPrincipalId, newPrincipalId);

            if (!await _permissionService.HasPermission(resourceId, _client.UserIdentities, Permissions.SecurityWrite).ConfigureAwait(false))
            {
                throw new AcdException("E_PERM", "User has no write security permission");
            }

            if (!Utils.PrincipalExists(newPrincipalId, _client.Token))
            {
                throw new AcdException("E_VAL", "New principal does not exist");
            }

            if (await _repository.AcdWorkflowRepository.HasWorkflow(resourceId, _buddyWorkflow).ConfigureAwait(false))
            {
                throw new AcdException("E_VAL", "resource is already buddied");
            }
        }
    }
}
