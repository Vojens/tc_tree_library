﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.CopyACL
{
    public class ResourceGraph
    {
        public ResourceGraph(Node[] toplevelNodes, IDictionary<Guid, Node> pointers)
        {
            TopLevelNodes = toplevelNodes;
            Pointers = pointers;
        }
        public Node[] TopLevelNodes { get; set; }
        public IDictionary<Guid, Node> Pointers { get; set; }
    }
}
