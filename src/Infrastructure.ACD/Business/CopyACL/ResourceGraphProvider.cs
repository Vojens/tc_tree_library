﻿using Infrastructure.ACD.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;
using Infrastructure.ACD.Entities;

namespace Infrastructure.ACD.Business.CopyACL
{
    public interface IResourceGraphProvider
    {
        ResourceGraph GetWithBuddiedResources(Guid root);
        ResourceGraph GetWithoutBuddiedResources(Guid root);
    }

    public class ResourceGraphProvider : IResourceGraphProvider
    {
        private ICompositeAclRepository _repository;

        public ResourceGraphProvider(ICompositeAclRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get resources graph including buddied resources
        /// </summary>
        /// <param name="root">root resource id</param>
        /// <returns>A tuple containing top most children and a dictionary containing pointers to all nodes</returns>
        public ResourceGraph GetWithBuddiedResources(Guid root)
        {
            return GetTree(root, false);
        }

        /// <summary>
        /// Get resources graph without buddied resources and thus excluding their child level graph
        /// </summary>
        /// <param name="root">root resource id</param>
        /// <returns>A tuple containing top most children and a dictionary containing pointers to all nodes</returns>
        public ResourceGraph GetWithoutBuddiedResources(Guid root)
        {
            return GetTree(root, true);
        }

        private ResourceGraph GetTree(Guid root, bool excludeBuddied)
        {
            var allChidlren = _repository.ResourceRepository.GetAllChildren(root).ToEnumerable().ToList();
            var chunkedChildren = allChidlren.Select((i, idx) => new { i, idx }).GroupBy(e => e.idx / 500, e => e.i).ToList();
            var allMetadata = chunkedChildren.SelectMany(e => _repository.ResourceRepository.GetMetadata(e.Select(f => f.ParentId).ToList())).ToDictionary(e => e.ResourceId, e => e);
            var parents = chunkedChildren.SelectMany(e =>
                _repository.ResourceRepository.GetParents(e.Select(f => f.ParentId).ToList())).ToDictionary(e => e.Item1, e => e.Item2);
            var buddiedResources = excludeBuddied ? new HashSet<Guid>(chunkedChildren.SelectMany(e => _repository.AcdWorkflowRepository.GetAcdWorkflowsFor(e.Select(f => f.ResourceId).ToList(), "Buddy"))) :
                new HashSet<Guid>();
            var nodeGraph = GetNodeGraph(allMetadata, parents);
            var resourceGraph = GetResourceGraph(nodeGraph, buddiedResources, root);
            var rootResource = _repository.ResourceRepository.GetMetadataSync(root);
            var rootResourceNode = new Node(rootResource, resourceGraph.Item1);
            resourceGraph.Item2.Add(rootResource.ResourceId, rootResourceNode);
            var rootGraph = new ResourceGraph(
                new Node[] { rootResourceNode }, resourceGraph.Item2
                    );
            return rootGraph;
        }

        private Tuple<Node[], Dictionary<Guid, Node>> GetResourceGraph(Dictionary<Guid, List<Node>> nodeGraph, HashSet<Guid> buddiedResources, Guid input)
        {
            Func<Guid, Node[]> dataCollector = null;
            var pointers = new Dictionary<Guid, Node>();

            dataCollector = e =>
            {
                var childNodes = nodeGraph.ContainsKey(e) ? nodeGraph[e] : Enumerable.Empty<Node>();

                return childNodes.Where(f => !buddiedResources.Contains(f.Metadata.ResourceId)).Select(f =>
                {
                    var node = new Node(f.Metadata, dataCollector(f.Metadata.ResourceId));
                    pointers.Add(node.Metadata.ResourceId, node);
                    return node;

                }).ToArray();
            };

            var finaldata = dataCollector(input);

            return Tuple.Create(finaldata, pointers);
        }

        private Dictionary<Guid, List<Node>> GetNodeGraph(Dictionary<Guid, ResourceMetadataEntry> children, Dictionary<Guid, Guid> parents)
        {
            var nodeGraph = new Dictionary<Guid, List<Node>>();
            foreach (var pair in parents)
            {
                if (!nodeGraph.ContainsKey(pair.Value)) //pair.value is parent id
                {
                    nodeGraph.Add(pair.Value, new List<Node>());
                }

                if (children.ContainsKey(pair.Key))
                {
                    var child = children[pair.Key];
                    nodeGraph[pair.Value].Add(new Node(child, new Node[0]));
                }

            }
            return nodeGraph;
        }


    }
}
