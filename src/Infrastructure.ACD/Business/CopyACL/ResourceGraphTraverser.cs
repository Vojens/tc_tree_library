﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Extensions;
using Infrastructure.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.CopyACL
{
    public interface IResourceGraphTraverser
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="pointers"></param>
        /// <param name="mappings"></param>
        Task Traverse(Node[] graph, IDictionary<Guid, Guid> mappings, AcdEntry[] parentAcds);

        Task Commit();
    }

    public class ResourceGraphTraverser : IResourceGraphTraverser
    {
        private readonly ICompositeAclRepository _repository;
        private readonly INodeProcessor _processor;
        private readonly Stack<IList<AcdEntry>> _forced;
        private readonly IGraphClientInfo _client;
        private readonly int _count;
        private readonly Guid _jobId;
        private readonly bool _buddyingOperation;
        private readonly bool _approvalOperation;
        private Guid _destinationId;
        private Guid _sourceId;

        public ResourceGraphTraverser(ICompositeAclRepository repository, int count, 
            Guid jobId, IGraphClientInfo client,
            bool buddyingOperation, bool approvalOperation,
            Guid sourceId, Guid destinationId)
        {
            _repository = repository;
            _count = count;
            _jobId = jobId;
            _client = client;

            _buddyingOperation = buddyingOperation;
            _approvalOperation = approvalOperation;
            _sourceId = sourceId;
            _destinationId = destinationId;
            _processor = new NodeProcessor(repository, client, 
                jobId, buddyingOperation, approvalOperation,
                sourceId, destinationId);
        }

        public async Task Traverse(Node[] graph, IDictionary<Guid, Guid> mappings, AcdEntry[] parentAcds)
        {
            if (parentAcds == null)
            {
                parentAcds = new AcdEntry[0];
            }
            foreach (var node in graph)
            {
                if(!mappings.Select(x => x.Key).Contains(node.Metadata.ResourceId))
                {
                    ServiceStatus.IncrementProcessedOperations(_jobId);
                    await this.Traverse(node.Children, mappings, parentAcds).ConfigureAwait(false);
                    continue;
                }
                var nextParentAcds = await _processor.Process(node, mappings[node.Metadata.ResourceId], parentAcds)
                    .ConfigureAwait(false);
                ServiceStatus.IncrementProcessedOperations(_jobId);
                await this.Traverse(node.Children, mappings, nextParentAcds).ConfigureAwait(false);
            }
        }

        public Task Commit()
        {
            return _processor.PersistData();
        }
    }
}
