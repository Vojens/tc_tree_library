﻿using Infrastructure.ACD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.CopyACL
{
    public class Node
    {
        public Node(ResourceMetadataEntry metadata, Node[] children)
        {
            Metadata = metadata;
            Children = children;
        }
        public ResourceMetadataEntry Metadata { get; set; }
        public Node[] Children { get; set; }
    }
}
