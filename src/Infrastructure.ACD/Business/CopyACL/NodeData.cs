﻿using Infrastructure.ACD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.CopyACL
{
    public class NodeData
    {
        public NodeData(Guid sourceId, Guid rootSource, Guid rootDestination, Node node, AcdEntry[] oldAcds, AcdEntry[] newAcds, AcdEntry[] sourceAcds)
        {
            SourceId = sourceId;
            RootSource = rootSource;
            RootDestination = rootDestination;
            Node = node;
            OldAcds = oldAcds;
            NewAcds = newAcds;
            SourceAcds = sourceAcds;
        }

        public Guid RootSource { get; set; }
        public Guid RootDestination { get; set; }
        public Guid SourceId { get; set; }
        public Node Node { get; set; }
        public AcdEntry[] OldAcds { get; set; }
        public AcdEntry[] NewAcds { get; set; }
        public AcdEntry[] SourceAcds { get; set; }
    }
}
