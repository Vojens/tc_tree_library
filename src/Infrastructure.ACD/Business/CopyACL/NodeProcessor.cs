﻿using LightInject;
using Newtonsoft.Json;
using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.CopyACL
{
    public interface INodeProcessor
    {
        Task<AcdEntry[]> Process(Node node, Guid guid, AcdEntry[] parentAcds);
        Task PersistData();
    }
    public class NodeProcessor : INodeProcessor
    {
        private readonly ICompositeAclRepository _repository;
        private readonly IGraphClientInfo _client;
        private readonly IDictionary<Guid, NodeData> _data;

        const short _createPermission = (short)Permissions.Create;
        private readonly bool _buddyingOperation;
        private readonly bool _approvalOperation;
        private Guid _jobId;
        private Guid _sourceId;
        private Guid _destinationId;

        public NodeProcessor(ICompositeAclRepository repository, IGraphClientInfo client, Guid jobId,
            bool buddyingOperation, bool approvalOperation,
            Guid sourceId, Guid destinationId)
        {
            _repository = repository;
            _client = client;
            _jobId = jobId;
            _data = new Dictionary<Guid, NodeData>();

            _sourceId = sourceId;
            _destinationId = destinationId;

            _buddyingOperation = buddyingOperation;
            _approvalOperation = approvalOperation;

            if (_buddyingOperation && _approvalOperation)
            {
                throw new AcdException("E_VAL", "Cannot buddy and approve at the same time");
            }
        }

        public async Task<AcdEntry[]> Process(Node node, Guid sourceId, AcdEntry[] parentAcds)
        {
            await ValidateProcess(node, sourceId).ConfigureAwait(false);

            var sourceAcds = _approvalOperation ? await GetSourceAcdsFromWorkflow(node.Metadata.ResourceId).ConfigureAwait(false) :
                await GetSourceAcds(sourceId).ConfigureAwait(false);

            await ValidateSourceAcds(node.Metadata.ResourceId, sourceAcds).ConfigureAwait(false);

            return await Process(node, sourceId, sourceAcds.ToArray(), parentAcds).ConfigureAwait(false);
        }

        private async Task ValidateProcess(Node node, Guid sourceId)
        {
            var sourceResource = await _repository.ResourceRepository.GetMetadata(sourceId).ConfigureAwait(false);
            if (sourceResource.ResourceSet != node.Metadata.ResourceSet)
            {
                throw new AcdException("E_VAL", "Source and destination resource set does not match");
            }

            var allowedPermissions = await _repository.PermissionsRepository.Has(node.Metadata.ResourceId,
                _client.UserIdentities,
                new[] { Permissions.SecurityRead, Permissions.SecurityWrite }).ConfigureAwait(false);

            if (!allowedPermissions)
            {
                throw new AcdException("E_PERM", "Read and Edit permissions not present");
            }

            var isResourceBuddied = await _repository.AcdWorkflowRepository.HasWorkflow(node.Metadata.ResourceId, WorkflowOptions.CopyAcl.ToString()).ConfigureAwait(false);
            if (isResourceBuddied && _buddyingOperation)
            {
                throw new AcdException("E_VAL", "Resource is already buddied");
            }

            if (_approvalOperation && !isResourceBuddied)
            {
                throw new AcdException("E_VAL", "Resource is not buddied to approve");
            }
        }

        public async Task PersistData()
        {
            if(!_data.Any())
            {
                throw new AcdException("E_VAL", "There are no acds to copy");
            }
            foreach (var node in _data)
            {
                await PersistAudit(node.Value).ConfigureAwait(false);

                if (_buddyingOperation)
                {
                    await PersistAcdWorkflows(node.Value).ConfigureAwait(false);
                }
                else
                {
                    if (_approvalOperation)
                    {
                        await _repository.AcdWorkflowRepository.Delete(node.Value.Node.Metadata.ResourceId, WorkflowOptions.CopyAcl.ToString()).ConfigureAwait(false);
                    }
                    await PersistAcds(node.Value).ConfigureAwait(false);
                }
            }
        }

        private async Task PersistAcdWorkflows(NodeData node)
        {
            var nodeObject = node.Node;
            var newAcds = node.NewAcds;

            node.Node = null;
            node.NewAcds = null;

            var acdWorkflow = new AcdWorkflowEntry
            {
                AcdWorkflow = WorkflowOptions.CopyAcl.ToString(),
                CreatedTimestamp = DateTimeOffset.UtcNow,
                CreatedUserId = _client.UserId,
                CreatedUsername = _client.Username,
                Id = Guid.NewGuid(),
                ResourceId = nodeObject.Metadata.ResourceId,
                DisplayText = _jobId.ToString(),
                Data = JsonConvert.SerializeObject(node)
            };

            node.Node = nodeObject;
            node.NewAcds = newAcds;

            await _repository.AcdWorkflowRepository.Put(acdWorkflow).ConfigureAwait(false);
        }

        private async Task PersistAcds(NodeData node)
        {
            var children = PopulateResourceChildren(node.Node.Metadata.ResourceId, _jobId);
            var onelevelchild = _repository.ResourceRepository.GetChildren(node.Node.Metadata.ResourceId).ToEnumerable();
            var oneLevelMetadata = _repository.ResourceRepository.GetMetadata(onelevelchild.Select(x => x.ResourceId).ToList());

            var metadata = node.Node.Metadata;

            foreach (var acd in node.NewAcds)
            {
                // changes made for OCC of child on confirmation with Riza

                //if (acd.OnChildCreate != OnChildCreateOptions.Never)
                //{
                //    var isMemberOfIdentity = Utils.IsMemberOfIdentity(acd, metadata.CreatedUserId, _client.Token);

                //    switch (acd.OnChildCreate)
                //    {
                //        case OnChildCreateOptions.IfIdentity:
                //            if (!isMemberOfIdentity)
                //            {
                //                continue;
                //            }
                //            break;
                //        case OnChildCreateOptions.IfNotIdentity:
                //            if (isMemberOfIdentity)
                //            {
                //                continue;
                //            }
                //            break;
                //    }
                //}

                await PersistAcd(metadata, acd).ConfigureAwait(false);

                await PersistAcdSet(node, metadata, acd).ConfigureAwait(false);

                await PersistAcdCreateSet(metadata, acd).ConfigureAwait(false);

                if (acd.OnChildCreate != OnChildCreateOptions.Never && acd.OnChildCreate != OnChildCreateOptions.Always_1Level)
                {
                    foreach (var child in children)
                    {
                        var isIfIdentity = Utils.IsMemberOfIdentity(acd, child.CreatedUserId, _client.Token);

                        switch (acd.OnChildCreate)
                        {
                            case OnChildCreateOptions.IfIdentity:
                                if (!isIfIdentity)
                                {
                                    continue;
                                }
                                break;
                            case OnChildCreateOptions.IfNotIdentity:
                                if (isIfIdentity)
                                {
                                    continue;
                                }
                                break;
                        }
                        await PersistAcds(child, node, acd).ConfigureAwait(false);
                    }
                                      
                }
                if (acd.OnChildCreate == OnChildCreateOptions.Always_1Level)
                {
                    foreach (var child in oneLevelMetadata)
                    {
                        await PersistAcds(child, node, acd).ConfigureAwait(false);
                    }
                }
            }
        }

        private async Task PersistAcds(ResourceMetadataEntry child, NodeData node, AcdEntry parentAcd = null)
        {
            await PersistAcd(child, parentAcd, true).ConfigureAwait(false);

            await PersistAcdSet(node, child, parentAcd).ConfigureAwait(false);

            await PersistAcdCreateSet(child, parentAcd).ConfigureAwait(false);

        }

        private async Task PersistAcdCreateSet(ResourceMetadataEntry metadata, AcdEntry acd)
        {
            if (acd.Permissions.Contains(_createPermission))
            {
                var acdResourceType = acd.ResourceType ?? "";
                foreach (var type in ResourceTypeGroupRepository.GetInnerResourceTypes(acdResourceType))
                {
                    var acdCreateSet = new AcdCreatePermission
                    {
                        AcdId = acd.AcdId,
                        ResourceId = metadata.ResourceId,
                        ResourceType = type,
                        PrincipalId = acd.PrincipalId
                    };
                    await _repository.AcdSetRepository.Put(acdCreateSet).ConfigureAwait(false);
                }
            }
        }

        private async Task PersistAcdSet(NodeData node, ResourceMetadataEntry metadata, AcdEntry acd)
        {
            if (
                (acd.ResourceType == null ||
                ResourceTypeGroupRepository.GetInnerResourceTypes(acd.ResourceType.ToLower()).Contains(metadata.Type.ToLower()))
            && (acd.ApplicableWorkflow == null || acd.ApplicableWorkflow == node.Node.Metadata.Workflow)
            && (acd.ApplicableWorkflow == null || (acd.ApplicableWorkflow == metadata.Workflow && acd.ApplicableWorkflowState == metadata.WorkflowState)))
            {
                foreach (var permission in acd.Permissions.Where(e => e != _createPermission))
                {
                    var entry = new AcdSetEntry
                    {
                        AcdId = acd.AcdId,
                        InId = metadata.ResourceId,
                        OutId = metadata.ResourceId,
                        Permission = permission,
                        PrincipalId = acd.PrincipalId
                    };
                    await _repository.AcdSetRepository.Put(entry).ConfigureAwait(false);
                }
            }
        }

        private async Task PersistAcd(ResourceMetadataEntry metadata, AcdEntry acd, bool isChild = false)
        {
            var childAcd = isChild ? acd.Clone() : acd;
            childAcd.AcdId = Guid.NewGuid();
            childAcd.CreatedTimestamp = DateTimeOffset.UtcNow;
            childAcd.CreatedUserId = _client.UserId;
            childAcd.CreatedUsername = _client.Username;
            childAcd.ResourceId = metadata.ResourceId;
            childAcd.ResourceSet = metadata.ResourceSet;
            if (isChild)
                childAcd.InheritedFrom = acd.InheritedFrom != Guid.Empty ? acd.InheritedFrom : acd.AcdId;

            await _repository.AcdRepository.Put(childAcd).ConfigureAwait(false);
        }

        private async Task PersistAudit(NodeData data)
        {
            var displayText = data.Node.Metadata.ResourceSet == "SEA"
                                        ? string.IsNullOrEmpty(data.Node.Metadata.Description) ? data.Node.Metadata.Uri : data.Node.Metadata.Description
                                        : data.Node.Metadata.Uri;
            var _auditId = Guid.NewGuid();
            var audit = new AuditEntry
            {
                Action = AcdActions.Create,
                AuditId = _auditId,
                DisplayText = displayText,
                InId = data.Node.Metadata.ResourceId,
                OutId = data.Node.Metadata.ResourceId,
                ResourceName = data.Node.Metadata.Name,
                ResourceType = data.Node.Metadata.Type,
                State = _buddyingOperation ? AcdStates.Pending : AcdStates.Completed, //to be revisited
                Timestamp = DateTimeOffset.UtcNow,
                ModifiedUsername = _client.Username
            };

            var auditDataEntry = new AuditDataEntry
            {
                AuditId = _auditId,
                DataNew = JsonConvert.SerializeObject(data.NewAcds),
                DataOld = JsonConvert.SerializeObject(data.OldAcds)
            };

            await _repository.AuditsRepository.Put(audit).ConfigureAwait(false);
            await _repository.AuditDataRepository.Put(auditDataEntry).ConfigureAwait(false);
        }

        private async Task<AcdEntry[]> Process(Node node, Guid sourceId, AcdEntry[] sourceAcds, AcdEntry[] parentAcds)
        {
            var sourceOccAcds = sourceAcds.Where(e => e.OnChildCreate != OnChildCreateOptions.Never).ToArray();
            var resourceAcdsToBeAdded = sourceAcds.ToArray();
            var nextParentAcds = parentAcds.Concat(sourceOccAcds
                .Select(e =>
                {
                    var pushDown = e.Clone();
                    pushDown.InheritedFrom = pushDown.AcdId;
                    return pushDown;
                })).ToArray();
            var currentAcds = _repository.AcdRepository.Get(node.Metadata.ResourceId).ToEnumerable().ToArray();
            if (sourceAcds.Count() != 0)
                _data.Add(node.Metadata.ResourceId, new NodeData(sourceId, _sourceId, _destinationId, node, currentAcds, resourceAcdsToBeAdded, sourceAcds));

            return nextParentAcds;
        }

        private async Task<List<AcdEntry>> GetSourceAcds(Guid sourceId)
        {
            var resourceMetadata = await _repository.ResourceRepository.GetMetadata(sourceId)
                .ConfigureAwait(false);
            if (resourceMetadata == null)
            {
                throw new AcdException("E_VAL", "Source resource does not exist");
            }

            var hasPermission = await _repository.PermissionsRepository.Has(sourceId, _client.UserIdentities, Permissions.SecurityRead).ConfigureAwait(false);
            if (!hasPermission)
            {
                throw new AcdException("E_PERM", "Source resource permission denied");
            }

            var sourceAcds = _repository.AcdRepository.GetOriginallyAddedAcds(sourceId).ToEnumerable().ToList();

            return sourceAcds;
        }

        private async Task ValidateSourceAcds(Guid destinationId, List<AcdEntry> sourceAcds)
        {
            var existingAcds = _repository.AcdRepository.Get(destinationId).ToEnumerable().ToList();
            var skipAcds = new List<AcdEntry>();
            foreach (var acd in sourceAcds)
            {
                if (existingAcds.Any(x => x.PrincipalId == acd.PrincipalId && x.ResourceType == acd.ResourceType))
                {
                    skipAcds.Add(acd);
                }
            }
            foreach (var acd in skipAcds)
            {
                var acdToRemove = sourceAcds.Where(x => x.PrincipalId == acd.PrincipalId && x.ResourceType == acd.ResourceType).FirstOrDefault();
                sourceAcds.Remove(acdToRemove);
            }
         }

        private async Task<List<AcdEntry>> GetSourceAcdsFromWorkflow(Guid resourceId)
        {
            var workflow = await _repository.AcdWorkflowRepository.Get(resourceId, WorkflowOptions.CopyAcl.ToString()).ConfigureAwait(false);

            if (workflow == null)
            {
                return Enumerable.Empty<AcdEntry>().ToList();
            }

            if (string.IsNullOrEmpty(workflow.DisplayText))
            {
                throw new AcdException("E_VAL", "Resource is not part of copy acl group");
            }

            var nodeData = JsonConvert.DeserializeObject<NodeData>(workflow.Data);

            var filteredAcds = nodeData.SourceAcds.Where(x => x.InheritedFrom == Guid.Empty).ToList();

            return filteredAcds;
        }

        private List<ResourceMetadataEntry> PopulateResourceChildren(Guid resourceId, Guid jobStatusId)
        {
            var resourceGraph = _repository.ResourceRepository.GetResourceGraph(resourceId);
            resourceGraph.Remove(resourceId);
            var chunkedResourceGraph = resourceGraph.Select((i, idx) => new { i, idx }).GroupBy(e => e.idx / 500, e => e.i).ToList();
            var resourceMetadata = chunkedResourceGraph.SelectMany(e =>
            {
                var data = _repository.ResourceRepository.GetMetadata(e.ToList());
                ServiceStatus.IncrementResources(jobStatusId, e.Count());
                return data;
            }).ToList();
            return resourceMetadata;
        }
    }
}
