﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.CopyACL
{
    public class CopyAclWorkflow
    {
        public CopyAclWorkflow(Guid sourceId, Guid rootSource, Guid rootDestination, Guid groupId)
        {
            SourceId = sourceId;
            RootSource = rootSource;
            RootDestination = rootDestination;
            GroupId = groupId;
        }
        public Guid SourceId { get; set; }
        public Guid RootSource { get; set; }
        public Guid RootDestination { get; set; }
        public Guid GroupId { get; set; }
    }
}
