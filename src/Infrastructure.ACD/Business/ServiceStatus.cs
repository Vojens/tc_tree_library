﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business
{
    public class ServiceStatus
    {
        Guid _id;
        ServiceStatusFlags _currentStatus;
        int _totalOperations;
        int _processedOperations;
        int _resourcesCount;
        int _auditsCount;

        string _errorCode;
        string _errorDescription;

        private ServiceStatus()
        {
            _currentStatus = ServiceStatusFlags.NotStarted;
            _totalOperations = 0;
            _processedOperations = 0;
            _auditsCount = 0;
        }

        public Guid JobId { get { return _id; } }
        public ServiceStatusFlags CurrentStatus { get { return _currentStatus; } }
        public int TotalOperations { get { return _totalOperations; } }
        public int PorcessedOperations { get { return _processedOperations; } }
        public int ResourcesCount { get { return _resourcesCount; } }
        public int AuditsCount { get { return _auditsCount; } }
        public string ErrorCode { get { return _errorCode; } }
        public string ErrorDescription { get { return _errorDescription; } }

        static ConcurrentDictionary<Guid, ServiceStatus> jobs = new ConcurrentDictionary<Guid, ServiceStatus>();

        public static Guid Create()
        {
            var status = new ServiceStatus();
            var statusId = Guid.NewGuid();
            status._id = statusId;
            jobs.TryAdd(statusId, status);
            return statusId;
        }

        public static void SetFlag(Guid id, ServiceStatusFlags flag)
        {
            if (id == Guid.Empty)
            {
                return;
            }
            var status = jobs[id];
            status._currentStatus = flag;
        }

        public static void SetTotalOperations(Guid id, int totalOperations)
        {
            if (id == Guid.Empty)
            {
                return;
            }
            var status = jobs[id];
            status._totalOperations = totalOperations;
        }

        public static void SetErrorStatus(Guid id, string code, string desc)
        {
            if (id == Guid.Empty)
            {
                return;
            }
            var status = jobs[id];
            status._currentStatus = ServiceStatusFlags.Errored;
            status._errorCode = code;
            status._errorDescription = desc;
        }

        public static void IncrementProcessedOperations(Guid id)
        {
            if (id == Guid.Empty)
            {
                return;
            }
            var status = jobs[id];
            Interlocked.Increment(ref status._processedOperations);
        }

        public static void IncrementResources(Guid id)
        {
            if (id == Guid.Empty)
            {
                return;
            }
            var status = jobs[id];
            Interlocked.Increment(ref status._resourcesCount);
        }

        public static void IncrementAudits(Guid id)
        {
            if (id == Guid.Empty)
            {
                return;
            }
            var status = jobs[id];
            Interlocked.Increment(ref status._auditsCount);
        }

        public static void Remove(Guid id)
        {
            if (id == Guid.Empty)
            {
                return;
            }
            ServiceStatus removed;
            jobs.TryRemove(id, out removed);
        }

        public static ServiceStatus GetStatus(Guid id)
        {
            if (jobs.ContainsKey(id))
            {
                return jobs[id];
            }
            return null;
        }

        public static void IncrementResources(Guid jobStatusId, int countToAdd)
        {
            if (jobStatusId == Guid.Empty)
            {
                return;
            }
            var status = jobs[jobStatusId];
            Interlocked.Add(ref status._resourcesCount, countToAdd);
        }

        public static IEnumerable<ServiceStatus> GetAllJobs()
        {
            return jobs.Values;
        }
    }
}
