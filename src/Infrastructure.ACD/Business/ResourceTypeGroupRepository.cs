﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business
{
    public class ResourceTypeGroupRepository
    {
        public static string[] GetInnerResourceTypes(string resourceType)
        {
            resourceType = resourceType ?? "";
            switch (resourceType.ToLower())
            {
                case "witsmllogobject":
                    return new[] { "log", "channel" };
                case "witsmlmudlogobject":
                    return new[] { "mudlog", "channel", "mudlogchannelmap" };
                case "witsmltrajectoryobject":
                    return new[] { "trajectoryheader", "trajectory", "trajectorymap" };
                case "witsmlgenericobject":
                    return new[] { 
                        "log", "channel" ,
                        "mudlog", "channel", "mudlogchannelmap" ,
                        "trajectoryheader", "trajectory", "trajectorymap",
                        "virtuallog", "virtuallogchannel", "virtuallogsourcechannel",
                        "message", "attachment", "bharun", "cementjob", "objectgroup", "convcore", "drillreport", "fluidsreport", "formationmarker", "opsreport",
                        "rig", "risk", "sidewallcore", "stimjob", "surveyprogram", "target", "tubular", "wbgeometry", "coordinatereferencesystem",
                        "toolerrormodel", "toolerrortermset", "dtsinstalledsystem", "dtsmeasurement", "realtime" };
                case "witsmlvirtuallogobject":
                    return new[] { "virtuallog", "channel", "virtuallogsourcechannel" };
                case "seaelementobject":
                    return new[] { "record", "link", "calendar", "discussion", "realtimedisplay", "genericxml", "page", "witsmlobject", "attachment" };
                default:
                    return new[] { resourceType.ToLower() };
            }
        }
    }
}
