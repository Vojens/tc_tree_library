﻿using Newtonsoft.Json;
using Infrastructure.ACD.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business
{
    public static class Utils
    {
        public static bool IsMemberOfIdentity(AcdEntry acd, Guid createdUserId, string token)
        {
            if (acd.PrincipalType == PrincipalTypes.User)
            {
                return acd.PrincipalId == createdUserId;
            }

            var request = new RestRequest("principals/userIds/{groupId}", Method.GET);
            request.AddUrlSegment("groupId", acd.PrincipalId.ToString()); // replaces matching token in request.Resource

            var response = InvokeSecurityRestRequest(request, token);
            if (response.Content.Contains(createdUserId.ToString()))
            {
                return true;
            }
            return false;
        }

        internal static bool PrincipalExists(Guid newPrincipalId, string token)
        {
            var request = new RestRequest("principals/principalName/{id}", Method.GET);
            request.AddUrlSegment("id", newPrincipalId.ToString());
            var response = InvokeSecurityRestRequest(request, token);
            var data = JsonConvert.DeserializeObject<Dictionary<Guid, string>>(response.Content);
            return data.Any();
        }

        internal static string GetPrincipalName(Guid newPrincipalId, string token)
        {
            var request = new RestRequest("principals/principalName/{id}", Method.GET);
            request.AddUrlSegment("id", newPrincipalId.ToString());
            var response = InvokeSecurityRestRequest(request, token);
            var data = JsonConvert.DeserializeObject<Dictionary<Guid, string>>(response.Content);
            return data.First().Value;
        }

        private static IRestResponse InvokeSecurityRestRequest(IRestRequest request, string token)
        {
            request.AddHeader("SamlToken", token.Replace("\r\n", ""));
            var restClient = new RestClient(ConfigurationManager.AppSettings["SecurityAPI"]);
            var response = restClient.Execute(request);
            return response;
        }
    }
}
