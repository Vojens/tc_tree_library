﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reactive.Linq;
using Newtonsoft.Json;

namespace Infrastructure.ACD.Business
{
    public interface IResourceCreateService
    {
        Task Handle(IDictionary<string, object> parameters, bool isSingleParent = true);
    }
    public class ResourceCreateService : IResourceCreateService
    {
        private readonly ICompositeAclRepository _repository;
        private readonly IGraphClientInfo _client;

        private string _displayText;
        private Guid _resourceParentId;
        private List<Guid> _resourceParentIds;
        private Guid _resourceId;
        private string _resourceName;
        private string _resourceType;
        private string _resourceSet;
        private string _resourceWorkflow;
        private short _resourceWorkflowState;
        private Guid _resourceCreatedUserId;
        private bool _createOwnerReader;

        private readonly List<AcdEntry> _acds;
        private readonly ConcurrentBag<AcdSetEntry> _acdSets;
        private readonly ConcurrentBag<AcdCreatePermission> _acdCreateSet;
        private AuditEntry _audit;
        private AuditDataEntry _auditData;

        public ResourceCreateService(ICompositeAclRepository repository, IGraphClientInfo client)
        {
            _repository = repository;
            _client = client;
            _acds = new List<AcdEntry>();
            _acdSets = new ConcurrentBag<AcdSetEntry>();
            _acdCreateSet = new ConcurrentBag<AcdCreatePermission>();
        }

        public Task Handle(IDictionary<string, object> parameters, bool isSingleParent = true)
        {
            ExtractParameters(parameters, isSingleParent);
            if (isSingleParent)
            {
                var parentAcdsToBeForced = _repository.AcdRepository.GetWithOnCreate(_resourceParentId).ToEnumerable();
                foreach (var acd in parentAcdsToBeForced)
                {
                    GenerateAcd(acd);
                }
            }
            else
            {
                var parentAcdsToBeForced = new List<AcdEntry>();
                foreach (var parentId in _resourceParentIds)
                {
                    parentAcdsToBeForced.AddRange(_repository.AcdRepository.GetWithOnCreate(parentId).ToEnumerable());
                }
                foreach (var acd in parentAcdsToBeForced)
                {
                    GenerateAcd(acd);
                }
            }
            //GenerateOwnerAcd();
            GenerateAudit();
            return SaveEntities();
        }

        private Task SaveEntities()
        {
            var tasks = new List<Task>();
            foreach (var acd in _acds)
            {
                tasks.Add(_repository.AcdRepository.Put(acd));
            }
            foreach (var acdSet in _acdSets)
            {
                tasks.Add(_repository.AcdSetRepository.Put(acdSet));
            }
            foreach (var acdCreateSet in _acdCreateSet)
            {
                tasks.Add(_repository.AcdSetRepository.Put(acdCreateSet));
            }
            tasks.Add(_repository.AuditsRepository.Put(_audit));
            tasks.Add(_repository.AuditDataRepository.Put(_auditData));
            return Task.WhenAll(tasks.ToArray());
        }

        private void GenerateAudit()
        {
            var _auditId = Guid.NewGuid();
            _audit = new AuditEntry
            {
                Action = AcdActions.Create,
                DisplayText = _displayText,
                AuditId = _auditId,
                ModifiedUsername = _client.Username,
                InId = _resourceId,
                OutId = _resourceId,
                ResourceName = _resourceName,
                ResourceType = _resourceType,
                State = AcdStates.Completed,
                Timestamp = DateTimeOffset.UtcNow
            };
            _auditData = new AuditDataEntry
            {
                AuditId = _auditId,
                DataNew = JsonConvert.SerializeObject(_acds),
                DataOld = "[]"
            };
        }

        private void GenerateOwnerAcd()
        {
            if (!_createOwnerReader)
            {
                return;
            }
            var permissionOptions = new[] { (short)Permissions.Read };
            //var permissionOptions = Enum.GetValues(typeof(Permissions)).Cast<short>().ToArray();
            //var currentUserAcds = _acds.Where(e => e.PrincipalId == _client.UserId);
            //var isOwnerForced = currentUserAcds.Any();
            //var ownerPermissions = currentUserAcds.SelectMany(e => e.Permissions).ToArray();
            //if (!isOwnerForced || ownerPermissions.Length < permissionOptions.Length)
            //{
            //    var applicablePermissionOptions = !isOwnerForced ? permissionOptions : permissionOptions.Except(ownerPermissions);
                var ownerAcd = new AcdEntry
                {
                    AcdId = Guid.NewGuid(),
                    ApplicableWorkflow = null,
                    ApplicableWorkflowState = 0,
                    CreatedTimestamp = DateTimeOffset.UtcNow,
                    CreatedUserId = _client.UserId,
                    CreatedUsername = _client.Username,
                    ForceOnChildCreate = false,
                    InheritedFrom = Guid.Empty,
                    OnChildCreate = OnChildCreateOptions.Never,
                    Permissions = new SortedSet<short>(permissionOptions),
                    PrincipalId = _client.UserId,
                    PrincipalName = _client.Username,
                    PrincipalType = PrincipalTypes.User,
                    ResourceId = _resourceId,
                    ResourceSet = _resourceSet,
                    ResourceType = null
                };
                _acds.Add(ownerAcd);
                GenerateAcdSet(ownerAcd);
            //}
        }

        private void GenerateAcd(AcdEntry acd)
        {
            switch (acd.OnChildCreate)
            {
                case OnChildCreateOptions.IfIdentity:
                    if (!Utils.IsMemberOfIdentity(acd, _resourceCreatedUserId, _client.Token)) return;
                    break;
                case OnChildCreateOptions.IfNotIdentity:
                    if (Utils.IsMemberOfIdentity(acd, _resourceCreatedUserId, _client.Token)) return;
                    break;
            }
            var clone = acd.Clone();
            clone.AcdId = Guid.NewGuid();
            clone.ResourceId = _resourceId;
            clone.ResourceSet = _resourceSet;
            clone.CreatedTimestamp = DateTimeOffset.UtcNow;
            clone.InheritedFrom = acd.InheritedFrom.Equals(Guid.Empty) ? acd.AcdId : acd.InheritedFrom;
            _acds.Add(clone);
            GenerateAcdSet(clone);
        }

        private void GenerateAcdSet(AcdEntry acd)
        {
            var createPermission = (short)Permissions.Create;
            if ((acd.ResourceType == null || ResourceTypeGroupRepository.GetInnerResourceTypes(acd.ResourceType.ToLower()).Contains(_resourceType.ToLower()))
                   && (acd.ApplicableWorkflow == null || acd.ApplicableWorkflow == _resourceWorkflow)
                   && (acd.ApplicableWorkflow == null ||
                    (acd.ApplicableWorkflow == _resourceWorkflow && acd.ApplicableWorkflowState == _resourceWorkflowState)))
            {
                foreach (var permission in acd.Permissions.Where(e => e != createPermission))
                {
                    var acdSet = new AcdSetEntry
                    {
                        AcdId = acd.AcdId,
                        InId = _resourceId,
                        OutId = _resourceId,
                        Permission = permission,
                        PrincipalId = acd.PrincipalId
                    };
                    _acdSets.Add(acdSet);
                }
            }

            if (acd.Permissions.Contains(createPermission))
            {
                var acdResourceType = acd.ResourceType ?? "";
                foreach (var type in ResourceTypeGroupRepository.GetInnerResourceTypes(acdResourceType))
                {
                    var acdCreateSet = new AcdCreatePermission
                    {
                        AcdId = acd.AcdId,
                        ResourceId = _resourceId,
                        ResourceType = type,
                        PrincipalId = acd.PrincipalId
                    };
                    _acdCreateSet.Add(acdCreateSet);   
                }                
            }
        }

        private void ExtractParameters(IDictionary<string, object> parameters, bool isSingleParent)
        {
            if (isSingleParent)
                _resourceParentId = (Guid) parameters["ResourceParentId"];
            else
                _resourceParentIds = (List<Guid>)parameters["ResourceParentIds"];
            _displayText = (string)parameters["DisplayText"];
            _resourceId = (Guid)parameters["ResourceId"];
            _resourceName = (string)parameters["ResourceName"];
            _resourceType = (string)parameters["ResourceType"];
            _resourceSet = (string)parameters["ResourceSet"];
            _resourceWorkflow = (string)parameters["ResourceWorkflow"];
            _resourceWorkflowState = (short)parameters["ResourceWorkflowState"];
            _resourceCreatedUserId = (Guid)parameters["ResourceCreatedUserId"];
            _createOwnerReader = !parameters.ContainsKey("CreateOwnerReader") || (bool)parameters["CreateOwnerReader"];
        }
    }
}
