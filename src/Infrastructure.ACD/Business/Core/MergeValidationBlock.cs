﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.Core
{
    public class MergeValidationBlock : BaseCoreService
    {
        public MergeValidationBlock(ICompositeAclRepository repository)
            : base(repository)
        {

        }
        public override Task Handle(IDictionary<string, object> parameters)
        {
            var unmodifiedAcds = (IEnumerable<AcdEntry>)parameters["ExistingUnmodifiedAcds"];
            var newSimpleAcds = (IEnumerable<AcdEntry>)parameters["NewSimpleAcds"];
            var newInheritedAcds = (IEnumerable<AcdEntry>)parameters["NewInheritedAcds"];
            var modifiedAcds = (IEnumerable<AcdEntry>)parameters["ModifiedAcds"];
            var isMultiParent = (bool)parameters["IsMultiParent"];
            var ignoreDuplicates = parameters.ContainsKey("IgnoreDuplicates") ? (bool)parameters["IgnoreDuplicates"] : false;

            var merge1 = unmodifiedAcds.SelectMany(e => e.Permissions.Select(f => new { e.PrincipalId, e.ResourceType, f }));
            var merge2 = newSimpleAcds.Union(newInheritedAcds).Union(modifiedAcds).SelectMany(e => e.Permissions.Select(f => new { e.PrincipalId, e.ResourceType, f })).GroupBy(e => e).Select(e => e.Key);

            if (merge2.GroupBy(e => e).Count() != merge2.Count())
            {
                throw new AcdException("E_VAL", "Current modification has some duplicates");
            }

            if (ignoreDuplicates && merge2.Distinct().Intersect(merge1).Any())
            {
                var mergedCollection = merge2.Distinct().Intersect(merge1);

                foreach (var acd in mergedCollection)
                {
                    if (newSimpleAcds.Any(d => d.PrincipalId == acd.PrincipalId && d.ResourceType == acd.ResourceType))
                    {
                        var duplicateAcds = newSimpleAcds.Where(d => d.PrincipalId == acd.PrincipalId && d.ResourceType == acd.ResourceType).ToList();
                        newSimpleAcds = newSimpleAcds.Except(duplicateAcds);
                        parameters["NewSimpleAcds"] = newSimpleAcds;
                    }
                    if (newInheritedAcds.Any(d => d.PrincipalId == acd.PrincipalId && d.ResourceType == acd.ResourceType))
                    {
                        var duplicateAcds = newInheritedAcds.Where(d => d.PrincipalId == acd.PrincipalId && d.ResourceType == acd.ResourceType).ToList();
                        newInheritedAcds = newInheritedAcds.Except(duplicateAcds);
                        parameters["NewInheritedAcds"] = newInheritedAcds;
                    }
                    if (modifiedAcds.Any(d => d.PrincipalId == acd.PrincipalId && d.ResourceType == acd.ResourceType))
                    {
                        var duplicateAcds = modifiedAcds.Where(d => d.PrincipalId == acd.PrincipalId && d.ResourceType == acd.ResourceType).ToList();
                        modifiedAcds = modifiedAcds.Except(duplicateAcds);
                        parameters["ModifiedAcds"] = modifiedAcds;
                    }
                }
            }

            //if (!isMultiParent && !ignoreDuplicates)
            if (!ignoreDuplicates)
            {
                if (merge2.Distinct().Intersect(merge1).Any())
                {
                    throw new AcdException("E_VAL", "Current changes already exists in store");
                }
            }


            return Link(parameters);
        }
    }
}
