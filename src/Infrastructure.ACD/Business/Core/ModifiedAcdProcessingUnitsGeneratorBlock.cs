﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.Core
{
    public class ModifiedAcdProcessingUnitsGeneratorBlock : BaseCoreService
    {
        public ModifiedAcdProcessingUnitsGeneratorBlock(ICompositeAclRepository repository)
            : base(repository)
        {

        }
        public override Task Handle(IDictionary<string, object> parameters)
        {
            var modifiedSimpleAcds = GetModifiedSimpleAcds(parameters);
            var modifiedAcdsForDeletion = GetModifiedAcdsForDeletion(parameters);
            var modifiedComplexAcds = GetComplexModifiedAcds(parameters);

            parameters.Add("ModifiedSimpleAcds", modifiedSimpleAcds);
            parameters.Add("ModifiedSimpleAcdsCount", modifiedSimpleAcds.Count);

            parameters.Add("ModifiedAcdsForDeletion", modifiedAcdsForDeletion);
            parameters.Add("ModifiedAcdsForDeletionCount", modifiedAcdsForDeletion.Count);

            parameters.Add("ModifiedComplexAcds", modifiedComplexAcds);
            parameters.Add("ModifiedComplexAcdsCount", modifiedComplexAcds.Count);

            return Link(parameters);
        }

        private List<AcdEntry> GetComplexModifiedAcds(IDictionary<string, object> parameters)
        {
            var result = new List<AcdEntry>();
            var modifiedAcds = (IEnumerable<AcdEntry>)parameters["ModifiedAcds"];
            var existingAcds = (IEnumerable<AcdEntry>)parameters["ExistingAcds"];
            foreach (var acd in modifiedAcds)
            {
                var existingAcd = existingAcds.Single(e => e.AcdId == acd.AcdId);
                if (acd.ApplicableWorkflow != existingAcd.ApplicableWorkflow
                   || acd.ApplicableWorkflowState != existingAcd.ApplicableWorkflowState
                   || acd.ForceOnChildCreate != existingAcd.ForceOnChildCreate
                   || acd.OnChildCreate != existingAcd.OnChildCreate
                   || acd.ResourceType != existingAcd.ResourceType)
                {
                    result.Add(acd);
                }
            }
            return result;
        }

        private List<AcdEntry> GetModifiedAcdsForDeletion(IDictionary<string, object> parameters)
        {
            var result = new List<AcdEntry>();
            var modifiedAcds = (IEnumerable<AcdEntry>)parameters["ModifiedAcds"];
            foreach (var acd in modifiedAcds)
            {
                if (!acd.Permissions.Any())
                {
                    result.Add(acd);
                }
            }
            return result;
        }

        private List<AcdEntry> GetModifiedSimpleAcds(IDictionary<string, object> parameters)
        {
            var result = new List<AcdEntry>();
            var modifiedAcds = (IEnumerable<AcdEntry>)parameters["ModifiedAcds"];
            var existingAcds = (IEnumerable<AcdEntry>)parameters["ExistingAcds"];
            foreach (var acd in modifiedAcds)
            {
                var existingAcd = existingAcds.Single(e => e.AcdId == acd.AcdId);
                if (acd.ApplicableWorkflow == existingAcd.ApplicableWorkflow
                    && acd.ApplicableWorkflowState == existingAcd.ApplicableWorkflowState
                    && acd.ForceOnChildCreate == existingAcd.ForceOnChildCreate
                    && acd.OnChildCreate == existingAcd.OnChildCreate
                    && acd.ResourceType == existingAcd.ResourceType
                    && !acd.Permissions.SequenceEqual(existingAcd.Permissions)
                    && acd.Permissions.Any())
                {
                    result.Add(acd);
                }
            }
            return result;
        }
    }
}
