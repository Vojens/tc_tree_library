﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.Core
{
    public class NewSimpleAcdProcessingBlock : BaseCoreService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;

        public NewSimpleAcdProcessingBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _repository = repository;
            _client = client;
        }

        public override async Task Handle(IDictionary<string, object> parameters)
        {
            var data = (IEnumerable<AcdEntry>)parameters["NewSimpleAcds"];
            var jobId = (Guid)parameters["JobStatusId"];
            if (data.Any())
            {
                var resourceId = (Guid)parameters["ResourceId"];
                var resourceMetadata = (ResourceMetadataEntry)parameters["ResourceMetadata"];
                var auditsContainer = (IDictionary<Guid, ConcurrentBag<AcdEntry>>)parameters["AcdChangesForAudits"];
                var existingAcdContainerForAudit = (IDictionary<Guid, string>)parameters["ExistingAcdContainerForAudits"];

                EnsureAuditForResourceExists(auditsContainer, existingAcdContainerForAudit, resourceMetadata);
                await HandleEnumerable(data, auditsContainer, resourceMetadata, jobId).ConfigureAwait(false);
            }

            await Link(parameters).ConfigureAwait(false);
        }
    }

    public class NewSimpleRecursiveAcdProcessingBlock : BaseCoreService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;

        public NewSimpleRecursiveAcdProcessingBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _repository = repository;
            _client = client;
        }

        public override async Task Handle(IDictionary<string, object> parameters)
        {
            if (parameters.ContainsKey("NewSimpleRecursiveAcds"))
            {
                var data = (IEnumerable<AcdEntry>)parameters["NewSimpleRecursiveAcds"];
                var jobId = (Guid)parameters["JobStatusId"];
                if (data.Any())
                {
                    var resourceMetadata = (ResourceMetadataEntry)parameters["ResourceMetadata"];
                    var children = (IEnumerable<ResourceMetadataEntry>)parameters["ChildResourcesMetadata"];
                    var auditsContainer = (IDictionary<Guid, ConcurrentBag<AcdEntry>>)parameters["AcdChangesForAudits"];
                    var existingAcdContainerForAudit = (IDictionary<Guid, string>)parameters["ExistingAcdContainerForAudits"];
                    var oneLevelChild = (IEnumerable<ResourceEntry>)parameters["1LevelChildResource"];

                    await HandleRecursive(data, auditsContainer, existingAcdContainerForAudit, children, _client, jobId, oneLevelChild).ConfigureAwait(false);
                }
            }

            await Link(parameters).ConfigureAwait(false);
        }
    }

    public class NewInheritedAcdProcessingBlock : BaseCoreService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;

        public NewInheritedAcdProcessingBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _repository = repository;
            _client = client;
        }

        public override async Task Handle(IDictionary<string, object> parameters)
        {
            if (parameters.ContainsKey("NewInheritedAcds"))
            {
                var data = (IEnumerable<AcdEntry>)parameters["NewInheritedAcds"];
                var jobId = (Guid)parameters["JobStatusId"];
                if (data.Any())
                {
                    var resourceId = (Guid)parameters["ResourceId"];
                    var resourceMetadata = (ResourceMetadataEntry)parameters["ResourceMetadata"];
                    var auditsContainer = (IDictionary<Guid, ConcurrentBag<AcdEntry>>)parameters["AcdChangesForAudits"];
                    var existingAcdContainerForAudit = (IDictionary<Guid, string>)parameters["ExistingAcdContainerForAudits"];

                    EnsureAuditForResourceExists(auditsContainer, existingAcdContainerForAudit, resourceMetadata);
                    await HandleEnumerable(data, auditsContainer, resourceMetadata, jobId).ConfigureAwait(false);
                }
            }

            await Link(parameters).ConfigureAwait(false);
        }
    }

    public class NewInheritedRecursiveAcdProcessingBlock : BaseCoreService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;

        public NewInheritedRecursiveAcdProcessingBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _repository = repository;
            _client = client;
        }

        public override async Task Handle(IDictionary<string, object> parameters)
        {
            if (parameters.ContainsKey("NewRecursiveInheritedAcds"))
            {
                var data = (IEnumerable<AcdEntry>)parameters["NewRecursiveInheritedAcds"];
                var jobId = (Guid)parameters["JobStatusId"];
                var resourceMetadata = (ResourceMetadataEntry)parameters["ResourceMetadata"];
                var children = parameters.ContainsKey("ChildResourcesMetadata") ? (IEnumerable<ResourceMetadataEntry>)parameters["ChildResourcesMetadata"] : Enumerable.Empty<ResourceMetadataEntry>();
                var auditsContainer = (IDictionary<Guid, ConcurrentBag<AcdEntry>>)parameters["AcdChangesForAudits"];
                var existingAcdContainerForAudit = (IDictionary<Guid, string>)parameters["ExistingAcdContainerForAudits"];
                var oneLevelChild = parameters.ContainsKey("1LevelChildResource") ? (IEnumerable<ResourceEntry>)parameters["1LevelChildResource"] : Enumerable.Empty<ResourceEntry>();

                await HandleRecursive(data, auditsContainer, existingAcdContainerForAudit, children, _client, jobId, oneLevelChild).ConfigureAwait(false);
            }

            await Link(parameters).ConfigureAwait(false);
        }
    }
}
