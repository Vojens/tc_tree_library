﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.Core
{
    public class JobStatusInitiatorBlock : BaseCoreService
    {
        public JobStatusInitiatorBlock(ICompositeAclRepository repository)
            : base(repository)
        {

        }
        public override Task Handle(IDictionary<string, object> parameters)
        {
            var jobId = (Guid)parameters["JobStatusId"];
            ServiceStatus.SetFlag(jobId, ServiceStatusFlags.Started);
            ServiceStatus.SetTotalOperations(jobId, GetTotalOperations(parameters));
            return Link(parameters);
        }

        private int GetTotalOperations(IDictionary<string, object> parameters)
        {
            var childCount = 0;
            if (parameters.ContainsKey("ChildResourcesMetadataCount"))
            {
                childCount = (int)parameters["ChildResourcesMetadataCount"];
            }
            
            var newSimpleAcdCount = (int)parameters["NewSimpleAcdsCount"];
            var newSimpleRecursiveAcdsCount = (int)parameters["NewSimpleRecursiveAcdsCount"];
            var newInheritedAcdsCount = (int)parameters["NewInheritedAcdsCount"];
            var newRecursiveInheritedAcdsCount = parameters.Keys.Contains("NewRecursiveInheritedAcdsCount") ? (int)parameters["NewRecursiveInheritedAcdsCount"] : 0;

            var modifiedSimpleAcdsCount = (int)parameters["ModifiedSimpleAcdsCount"];
            var modifiedAcdsForDeletionCount = (int)parameters["ModifiedAcdsForDeletionCount"];
            var modifiedComplexAcdsCount = (int)parameters["ModifiedComplexAcdsCount"];

            return newSimpleAcdCount + (newSimpleRecursiveAcdsCount * childCount)
                + newInheritedAcdsCount + (newRecursiveInheritedAcdsCount * childCount)
                + modifiedSimpleAcdsCount + (modifiedSimpleAcdsCount * childCount)
                + modifiedAcdsForDeletionCount + (modifiedAcdsForDeletionCount * childCount)
                + 2 * (modifiedComplexAcdsCount + (modifiedComplexAcdsCount * childCount));
        }
    }
}
