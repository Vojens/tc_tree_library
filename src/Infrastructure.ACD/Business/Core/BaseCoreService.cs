﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;

namespace Infrastructure.ACD.Business.Core
{
    public abstract class BaseCoreService : ICoreService
    {
        public abstract Task Handle(IDictionary<string, object> parameters);
        protected ICompositeAclRepository Repository;

        public BaseCoreService(ICompositeAclRepository repository)
        {
            Repository = repository;
            _targetServices = new ConcurrentBag<ICoreService>();
        }

        ConcurrentBag<ICoreService> _targetServices;
        ICoreService _continueService;

        public ICoreService LinkAll(params ICoreService[] services)
        {
            foreach (var service in services)
            {
                _targetServices.Add(service);
            }
            return this;
        }

        public ICoreService LinkTo(ICoreService service)
        {
            _targetServices.Add(service);
            return service;
        }

        protected async Task Link(IDictionary<string, object> parameters)
        {
            if (_targetServices.Any())
            {
                var tasks = new Task[_targetServices.Count];
                for (int i = 0; i < _targetServices.Count; i++)
                {
                    tasks[i] = _targetServices.ElementAt(i).Handle(parameters);
                }
                await Task.WhenAll(tasks).ConfigureAwait(false);
            }
            if (_continueService != null)
            {
                await _continueService.Handle(parameters).ConfigureAwait(false);
            }
        }

        public ICoreService Join(ICoreService @continue)
        {
            _continueService = @continue;
            return @continue;
        }

        protected async Task HandleRecursive(IEnumerable<AcdEntry> data, IDictionary<Guid, ConcurrentBag<AcdEntry>> auditsContainer, IDictionary<Guid, string> existingAcdContainer, IEnumerable<ResourceMetadataEntry> metadata, IGraphClientInfo client, Guid jobId, IEnumerable<ResourceEntry> oneLevelChild = null)
        {
            var batchHandler = new CassandraBatchHandler(Repository.ConnectionProvider, 250);
            foreach (var child in metadata)
            {
                foreach (var acd in data)
                {
                    switch (acd.OnChildCreate)
                    {
                        case OnChildCreateOptions.Always_1Level:
                            var oneLevelChildResourceIds = oneLevelChild.Select(x => x.ResourceId).ToList();
                            if (oneLevelChildResourceIds.Contains(child.ResourceId))
                                await Handle(acd, child, auditsContainer, existingAcdContainer, client, batchHandler, always : true).ConfigureAwait(false);
                            break;
                        case OnChildCreateOptions.Always:
                            await Handle(acd, child, auditsContainer, existingAcdContainer, client, batchHandler, always: true).ConfigureAwait(false);
                            break;
                        case OnChildCreateOptions.IfIdentity:
                            await Handle(acd, child, auditsContainer, existingAcdContainer, client, batchHandler, ifIdentity: true).ConfigureAwait(false);
                            break;
                        case OnChildCreateOptions.IfNotIdentity:
                            await Handle(acd, child, auditsContainer, existingAcdContainer, client, batchHandler).ConfigureAwait(false);
                            break;
                    }
                    ServiceStatus.IncrementProcessedOperations(jobId);
                }
            }

            await batchHandler.Flush().ConfigureAwait(false);
        }

        private async Task Handle(AcdEntry acd, ResourceMetadataEntry child, IDictionary<Guid, ConcurrentBag<AcdEntry>> auditsContainer, IDictionary<Guid, string> existingAcdContainer, IGraphClientInfo client, IBatchHandler batcher, bool always = false, bool ifIdentity = false)
        {
            EnsureAuditForResourceExists(auditsContainer, existingAcdContainer, child);

            if (!always)
            {
                var isMemberOfIdentity = Utils.IsMemberOfIdentity(acd, child.CreatedUserId, client.Token);
                if (isMemberOfIdentity != ifIdentity) return;
            }
            var acdClone = acd.Clone();
            acdClone.AcdId = Guid.NewGuid();
            acdClone.ResourceId = child.ResourceId;
            acdClone.ResourceSet = child.ResourceSet;
            acdClone.InheritedFrom = acd.InheritedFrom != Guid.Empty ? acd.InheritedFrom : acd.AcdId;

            var saveTask = PutEntities(acdClone, child, batcher: batcher);

            auditsContainer[child.ResourceId].Add(acdClone);

            await saveTask.ConfigureAwait(false);
        }

        protected async Task HandleEnumerable(IEnumerable<AcdEntry> data, IDictionary<Guid, ConcurrentBag<AcdEntry>> auditsContainer, ResourceMetadataEntry metadata, Guid jobId)
        {
            var batchHandler = new CassandraBatchHandler(Repository.ConnectionProvider, 250);
            var tasks = new List<Task>();
            foreach (var acd in data)
            {
                tasks.Add(PutEntities(acd, metadata, batchHandler));
                auditsContainer[metadata.ResourceId].Add(acd);

                if (jobId != Guid.Empty) ServiceStatus.IncrementProcessedOperations(jobId);
            }

            await batchHandler.Flush().ConfigureAwait(false);
            await Task.WhenAll(tasks.ToArray()).ConfigureAwait(false);
        }

        protected Task PutEntities(AcdEntry acd, ResourceMetadataEntry metadata, IBatchHandler batcher = null)
        {
            var tasks = new List<Task>();
            var createPermission = (short)Permissions.Create;

            tasks.Add(batcher == null ? Repository.AcdRepository.Put(acd) : Repository.AcdRepository.Put(batcher, acd));

            if ((acd.ResourceType == null || ResourceTypeGroupRepository.GetInnerResourceTypes(acd.ResourceType.ToLower()).Contains(metadata.Type.ToLower()))
                    && (acd.ApplicableWorkflow == null || acd.ApplicableWorkflow == metadata.Workflow)
                    && (acd.ApplicableWorkflow == null || (acd.ApplicableWorkflow == metadata.Workflow && acd.ApplicableWorkflowState == metadata.WorkflowState)))
            {
                foreach (var permission in acd.Permissions.Where(e => e != createPermission))
                {
                    var entry = new AcdSetEntry
                    {
                        AcdId = acd.AcdId,
                        InId = metadata.ResourceId,
                        OutId = metadata.ResourceId,
                        Permission = permission,
                        PrincipalId = acd.PrincipalId
                    };
                    tasks.Add(batcher == null ? Repository.AcdSetRepository.Put(entry) : Repository.AcdSetRepository.Put(batcher, entry));
                }
            }

            if (acd.Permissions.Contains(createPermission))
            {
                var acdResourceType = acd.ResourceType ?? "";
                foreach (var type in ResourceTypeGroupRepository.GetInnerResourceTypes(acdResourceType))
                {
                    var acdCreateSet = new AcdCreatePermission
                    {
                        AcdId = acd.AcdId,
                        ResourceId = metadata.ResourceId,
                        ResourceType = type,
                        PrincipalId = acd.PrincipalId
                    };
                    tasks.Add(batcher == null ? Repository.AcdSetRepository.Put(acdCreateSet) : Repository.AcdSetRepository.Put(batcher, acdCreateSet));
                }
            }

            return Task.WhenAll(tasks.ToArray());
        }

        protected Task DeleteEntities(AcdEntry current, AcdEntry acdSet, AcdEntry acdCreate, IBatchHandler batchHandler)
        {
            var tasks = new List<Task>();
            var createPermission = (short)Permissions.Create;

            tasks.Add(Repository.AcdRepository.Delete(batchHandler, current.ResourceId, current.AcdId));

            foreach (var permission in acdSet.Permissions.Where(e => e != (short)Permissions.Create))
            {
                tasks.Add(Repository.AcdSetRepository.Delete(batchHandler, current.ResourceId, (Permissions)permission, current.PrincipalId, current.AcdId));
            }

            if (acdCreate.Permissions.Contains(createPermission))
            {
                foreach (var resType in ResourceTypeGroupRepository.GetInnerResourceTypes(acdCreate.ResourceType))
                {
                    tasks.Add(Repository.AcdSetRepository.Delete(batchHandler, current.ResourceId, resType, current.PrincipalId, current.AcdId));
                }
            }
            return Task.WhenAll(tasks.ToArray());
        }

        protected void EnsureAuditForResourceExists(IDictionary<Guid, ConcurrentBag<AcdEntry>> auditsContainer, IDictionary<Guid, string> existingAcdsForAudit, ResourceMetadataEntry metadata)
        {
            if (auditsContainer.ContainsKey(metadata.ResourceId) == false)
            {
                auditsContainer.Add(metadata.ResourceId, new ConcurrentBag<AcdEntry>());
            }

            if (existingAcdsForAudit.ContainsKey(metadata.ResourceId) == false)
            {
                var allCurrentAcds = Repository.AcdRepository.Get(metadata.ResourceId).ToEnumerable();
                existingAcdsForAudit.Add(metadata.ResourceId, Newtonsoft.Json.JsonConvert.SerializeObject(allCurrentAcds));
            }
        }
    }
}
