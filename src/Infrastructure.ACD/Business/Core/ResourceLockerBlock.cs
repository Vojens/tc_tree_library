﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.Core
{
    public class ResourceLockerBlock : BaseCoreService
    {

        public ResourceLockerBlock(ICompositeAclRepository repository)
            : base(repository)
        {
        }

        public override Task Handle(IDictionary<string, object> parameters)
        {
            var resourceId = (Guid)parameters["ResourceId"];
            var jobId = (Guid)parameters["JobStatusId"];
            var children = parameters.ContainsKey("ChildResourcesMetadata") ? (IEnumerable<ResourceMetadataEntry>)parameters["ChildResourcesMetadata"] : Enumerable.Empty<ResourceMetadataEntry>();
            var childIds = children.Select(e => e.ResourceId).ToList();
            if (childIds.Any() && !ResourceLocker.TryLock(jobId, childIds))
            {
                throw new ResourceLockedException();
            }
            return Link(parameters);
        }
    }
}
