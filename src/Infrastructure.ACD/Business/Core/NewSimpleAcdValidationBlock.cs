﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.Core
{
    public class NewSimpleAcdValidationBlock : BaseCoreService
    {
        public NewSimpleAcdValidationBlock(ICompositeAclRepository repository)
            : base(repository)
        {

        }
        public override Task Handle(IDictionary<string, object> parameters)
        {
            var data = (IEnumerable<AcdEntry>)parameters["NewSimpleAcds"];

            if (data.Any(e => e.Permissions.Any() == false))
            {
                throw new AcdException("E_VAL", "There should be at least one permission when adding a new");
            }

            return Link(parameters);
        }
    }
}
