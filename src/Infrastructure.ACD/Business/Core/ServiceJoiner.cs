﻿using Infrastructure.ACD.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.Core
{
    public class ServiceJoiner : BaseCoreService
    {
        public ServiceJoiner(ICompositeAclRepository repository)
            : base(repository)
        {

        }

        public override Task Handle(IDictionary<string, object> parameters)
        {
            return Link(parameters);
        }
    }
}
