﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Collections.Concurrent;

namespace Infrastructure.ACD.Business.Core
{
    public class PreProcessingBlock : BaseCoreService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;

        public PreProcessingBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _repository = repository;
            _client = client;
        }

        public override async Task Handle(IDictionary<string, object> parameters)
        {
            var data = (IEnumerable<AcdEntry>)parameters["Input"];
            var jobStatusId = (Guid)parameters["JobStatusId"];
            var resourceId = (Guid)parameters["ResourceId"];
            var newAcds = data.Where(e => e.AcdId.Equals(Guid.Empty));
            var newSimpleAcds = newAcds.Where(e => e.InheritedFrom.Equals(Guid.Empty)).ToList();
            var existingAcds = _repository.AcdRepository.Get(resourceId).ToEnumerable().ToList();   //TODO: ToList can crash
            var existingAcdIds = existingAcds.Select(e => e.AcdId);
            var modifiedAcds = data.Where(e => e.AcdId.Equals(Guid.Empty) == false);
            var modifiedAcdIds = modifiedAcds.Select(e => e.AcdId);
            var newInheritedAcds = newAcds.Where(e => e.InheritedFrom.Equals(Guid.Empty) == false).ToList();
            var newInheritedAcdsInheritedFromIds = newInheritedAcds.Select(e => e.InheritedFrom);
            var newSimpleRecursiveAcds = newSimpleAcds.Where(e => e.OnChildCreate != OnChildCreateOptions.Never).ToList();
            var existingAcdContainerForAudit = new ConcurrentDictionary<Guid, string>();
            existingAcdContainerForAudit.TryAdd(resourceId, Newtonsoft.Json.JsonConvert.SerializeObject(existingAcds));

            parameters.Add("NewAcds", newAcds.ToList());
            parameters.Add("ModifiedAcds", modifiedAcds.ToList());
            parameters.Add("ModifiedAcdIds", modifiedAcdIds.ToList());
            parameters.Add("ExistingAcds", existingAcds.ToList());
            parameters.Add("ExistingAcdIds", existingAcdIds.ToList());

            parameters.Add("NewSimpleAcds", newSimpleAcds);
            parameters.Add("NewSimpleAcdsCount", newSimpleAcds.Count);

            parameters.Add("NewSimpleRecursiveAcds", newSimpleRecursiveAcds);
            parameters.Add("NewSimpleRecursiveAcdsCount", newSimpleRecursiveAcds.Count);

            parameters.Add("NewInheritedAcds", newInheritedAcds);
            parameters.Add("NewInheritedAcdsCount", newInheritedAcds.Count);

            parameters.Add("ExistingForcedAcds", existingAcds.Where(e => e.InheritedFrom.Equals(Guid.Empty) == false && e.OnChildCreate != OnChildCreateOptions.Never && e.ForceOnChildCreate).ToList());
            parameters.Add("ExistingUnmodifiedAcds", existingAcds.Where(e => modifiedAcdIds.Contains(e.AcdId) == false).ToList());
            parameters.Add("ExistingAcdContainerForAudits", existingAcdContainerForAudit);

            if (newInheritedAcds.Any())
            {
                var parentResource = await _repository.ResourceRepository.GetParent(resourceId).ConfigureAwait(false);
                var parentAcds = _repository.AcdRepository.Get(parentResource.ResourceId).ToEnumerable().ToList();      //TODO: ToList can crash

                parameters.Add("ParentResource", parentResource);
                parameters.Add("ParentAcds", parentAcds.ToList());
            }

            if (newSimpleRecursiveAcds.Any() || modifiedAcds.Any())
            {
                var sw = System.Diagnostics.Stopwatch.StartNew();
                var onelevelchild = _repository.ResourceRepository.GetChildren(resourceId).ToEnumerable();

                var children = PopulateResourceChildren(resourceId, jobStatusId);
                var el = sw.ElapsedMilliseconds;
                parameters.Add("ChildResourcesMetadata", children);
                parameters.Add("ChildResourcesMetadataCount", children.Count);
                parameters.Add("1LevelChildResource", onelevelchild);
            }

            await Link(parameters).ConfigureAwait(false);
        }

        private List<ResourceMetadataEntry> PopulateResourceChildren(Guid resourceId, Guid jobStatusId)
        {
            var resourceGraph = _repository.ResourceRepository.GetResourceGraph(resourceId);
            resourceGraph.Remove(resourceId);
            var chunkedResourceGraph = resourceGraph.Select((i, idx) => new { i, idx }).GroupBy(e => e.idx / 500, e => e.i).ToList();
            var resourceMetadata = chunkedResourceGraph.SelectMany(e =>
            {
                var data = _repository.ResourceRepository.GetMetadata(e.ToList());
                ServiceStatus.IncrementResources(jobStatusId, e.Count());
                return data;
            }).ToList();
            return resourceMetadata;
        }

    }
}
