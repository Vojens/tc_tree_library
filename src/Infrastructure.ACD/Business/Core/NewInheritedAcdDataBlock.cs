﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.Core
{
    public class NewInheritedAcdDataBlock : BaseCoreService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;

        public NewInheritedAcdDataBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _repository = repository;
            _client = client;
        }

        public override Task Handle(IDictionary<string, object> parameters)
        {
            var data = (IEnumerable<AcdEntry>)parameters["NewInheritedAcds"];
            if (data.Any())
            {

                var inheritedFroms = data.Select(e => e.InheritedFrom);
                var existingAcds = (IEnumerable<AcdEntry>)parameters["ExistingAcds"];
                var existingInheritedFroms = existingAcds.Select(e => e.InheritedFrom);
                var parentAcds = (IEnumerable<AcdEntry>)parameters["ParentAcds"];
                var newRecursiveInheritedAcds = data.Where(e => e.OnChildCreate != OnChildCreateOptions.Never).ToList();

                foreach (var acd in data)
                {
                    var parentAcd = parentAcds.Single(e => e.AcdId == acd.InheritedFrom);

                    if (parentAcd.InheritedFrom.Equals(Guid.Empty) == false && acd.OnChildCreate != OnChildCreateOptions.Never)
                    {
                        acd.InheritedFrom = parentAcd.InheritedFrom;
                    }
                    acd.AcdId = Guid.NewGuid();
                    acd.ApplicableWorkflow = parentAcd.ApplicableWorkflow;
                    acd.ApplicableWorkflowState = parentAcd.ApplicableWorkflowState;
                    acd.ForceOnChildCreate = parentAcd.ForceOnChildCreate;
                    acd.OnChildCreate = parentAcd.OnChildCreate;
                    acd.Permissions = new SortedSet<short>(parentAcd.Permissions);
                    acd.PrincipalId = parentAcd.PrincipalId;
                    acd.PrincipalName = parentAcd.PrincipalName;
                    acd.PrincipalType = parentAcd.PrincipalType;
                    acd.ResourceType = parentAcd.ResourceType;
                }

                if (inheritedFroms.Intersect(existingInheritedFroms).Any())
                {
                    throw new AcdException("E_VAL", "All inherited ACDs must not already be inherited in the resource");
                }

                parameters.Add("NewRecursiveInheritedAcds", newRecursiveInheritedAcds);
                parameters.Add("NewRecursiveInheritedAcdsCount", newRecursiveInheritedAcds.Count);
            }

            return Link(parameters);
        }
    }
}
