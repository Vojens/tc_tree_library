﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.Core
{
    public class NewInheritedAcdPreValidationBlock : BaseCoreService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;

        public NewInheritedAcdPreValidationBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _repository = repository;
            _client = client;
        }

        public override async Task Handle(IDictionary<string, object> parameters)
        {
            var data = (IEnumerable<AcdEntry>)parameters["NewInheritedAcds"];

            if (data.Any())
            {
                var acdIds = data.Select(e => e.InheritedFrom);
                var parentResource = (ResourceEntry)parameters["ParentResource"];
                var parentAcds = (IEnumerable<AcdEntry>)parameters["ParentAcds"];
                var parentAcdIds = parentAcds.Select(e => e.AcdId);
                var hasPermission = await _repository.PermissionsRepository.Has(parentResource.ResourceId, _client.UserIdentities, Permissions.SecurityRead).ConfigureAwait(false);

                if (!hasPermission)
                {
                    throw new AcdException("E_VAL", "User has no security read permission on parent resource");
                }

                if (acdIds.Any(e => parentAcdIds.Contains(e) == false))
                {
                    throw new AcdException("E_VAL", "All inherited ACDs must exist in the parent");
                }

                if (acdIds.Distinct().Count() != acdIds.Count())
                {
                    throw new AcdException("E_VAL", "All inherited ACDs must be unique");
                }
            }

            await Link(parameters).ConfigureAwait(false);
        }
    }
}
