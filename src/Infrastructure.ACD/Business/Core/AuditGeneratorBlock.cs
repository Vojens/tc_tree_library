﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;
using Newtonsoft.Json;

namespace Infrastructure.ACD.Business.Core
{
    public class AuditGeneratorBlock : BaseCoreService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;

        public AuditGeneratorBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _repository = repository;
            _client = client;
        }

        public override async Task Handle(IDictionary<string, object> parameters)
        {
            var auditsContainer = (IDictionary<Guid, ConcurrentBag<AcdEntry>>)parameters["AcdChangesForAudits"];
            var resourceMetadata = (ResourceMetadataEntry)parameters["ResourceMetadata"];
            var existingAcdContainerForAudit = (IDictionary<Guid, string>)parameters["ExistingAcdContainerForAudits"];
            var isApproval = (bool)parameters["IsApproval"];
            var jobId = (Guid)parameters["JobStatusId"];

            if (auditsContainer.Any())
            {
                await HandleResource(resourceMetadata, auditsContainer[resourceMetadata.ResourceId], existingAcdContainerForAudit[resourceMetadata.ResourceId], isApproval).ConfigureAwait(false);

                if (parameters.ContainsKey("ChildResourcesMetadata"))
                {
                    var children = (IEnumerable<ResourceMetadataEntry>)parameters["ChildResourcesMetadata"];
                    foreach (var resource in children)
                    {
                        if (auditsContainer.ContainsKey(resource.ResourceId) && existingAcdContainerForAudit.ContainsKey(resource.ResourceId))
                        {
                            await HandleResource(resource, auditsContainer[resource.ResourceId], existingAcdContainerForAudit[resource.ResourceId]).ConfigureAwait(false);
                            ServiceStatus.IncrementAudits(jobId);
                        }
                    }
                }
            }

            ServiceStatus.SetFlag(jobId, ServiceStatusFlags.Completed);
            //remove the status after two minutes

            await Link(parameters).ConfigureAwait(false);
        }

        private async Task HandleResource(ResourceMetadataEntry metadata, ConcurrentBag<AcdEntry> newModifications, string old, bool approval = false)
        {
            var displayText = metadata.ResourceSet == "SEA"
                                        ? string.IsNullOrEmpty(metadata.Description) ? metadata.Uri : metadata.Description
                                        : metadata.Uri;
            var _auditId = Guid.NewGuid();

            var auditEntry = new AuditEntry
            {
                Action = approval ? AcdActions.ApproveBuddy : AcdActions.Create,
                AuditId = _auditId,
                DisplayText = displayText,
                InId = metadata.ResourceId,
                ModifiedUsername = _client.Username,
                OutId = metadata.ResourceId,
                ResourceName = metadata.Name,
                ResourceType = metadata.Type,
                Timestamp = DateTimeOffset.UtcNow,
                State = AcdStates.Completed,
            };

            var auditDataEntry = new AuditDataEntry
            {
                AuditId = _auditId,
                DataNew = JsonConvert.SerializeObject(newModifications),
                DataOld = old
            };

            await _repository.AuditsRepository.Put(auditEntry).ConfigureAwait(false);
            await _repository.AuditDataRepository.Put(auditDataEntry).ConfigureAwait(false);
        }
    }
}
