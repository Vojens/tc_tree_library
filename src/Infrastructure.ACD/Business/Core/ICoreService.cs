﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.Core
{
    public interface ICoreService
    {
        Task Handle(IDictionary<string, object> parameters);
        ICoreService LinkAll(params ICoreService[] services);
        ICoreService LinkTo(ICoreService service);
        ICoreService Join(ICoreService @continue);
    }
}
