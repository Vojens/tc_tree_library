﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.Core
{
    public class MetadataUpdateBlock : BaseCoreService
    {
        IGraphClientInfo _client;

        public MetadataUpdateBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _client = client;
        }

        public override Task Handle(IDictionary<string, object> parameters)
        {
            PopulateMetadata(parameters);
            InitializeContainers(parameters);
            return Link(parameters);
        }

        private void InitializeContainers(IDictionary<string, object> parameters)
        {
            //var acdsToDelete = new List<AcdEntry>();
            //var acdsToUpsert = new List<AcdEntry>();
            var acdChangesForAudits = new ConcurrentDictionary<Guid, ConcurrentBag<AcdEntry>>();
            //var acdSetsToDelete = new List<AcdSetEntry>();
            //var acdSetsToInsert = new List<AcdSetEntry>();

            //parameters.Add("AcdsToDelete", acdsToDelete);
            //parameters.Add("AcdsToUpsert", acdsToUpsert);
            parameters.Add("AcdChangesForAudits", acdChangesForAudits);
            //parameters.Add("AcdSetsToDelete", acdSetsToDelete);
            //parameters.Add("AcdSetsToInsert", acdSetsToInsert);
        }

        private void PopulateMetadata(IDictionary<string, object> parameters)
        {
            var timestamp = DateTimeOffset.UtcNow;
            var newSimpleAcds = (IEnumerable<AcdEntry>)parameters["NewSimpleAcds"];
            var resourceMetadata = (ResourceMetadataEntry)parameters["ResourceMetadata"];

            foreach (var acd in newSimpleAcds)
            {
                acd.AcdId = Guid.NewGuid();
                acd.CreatedTimestamp = timestamp;
                acd.CreatedUserId = _client.UserId;
                acd.CreatedUsername = _client.Username;
                acd.ResourceSet = resourceMetadata.ResourceSet;
            }

            var modifiedAcds = (IEnumerable<AcdEntry>)parameters["ModifiedAcds"];
            var existingAcds = (IEnumerable<AcdEntry>)parameters["ExistingAcds"];

            foreach (var acd in modifiedAcds)
            {
                var existingAcd = existingAcds.Single(e => e.AcdId == acd.AcdId);
                acd.CreatedTimestamp = existingAcd.CreatedTimestamp;
                acd.CreatedUserId = existingAcd.CreatedUserId;
                acd.CreatedUsername = existingAcd.CreatedUsername;
                acd.ResourceSet = existingAcd.ResourceSet;
                acd.InheritedFrom = existingAcd.InheritedFrom;
            }
        }
    }
}
