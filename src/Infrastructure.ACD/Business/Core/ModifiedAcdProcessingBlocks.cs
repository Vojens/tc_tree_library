﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;

namespace Infrastructure.ACD.Business.Core
{
    public class SimpleModifiedAcdProcessingBlock : BaseCoreService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;

        public SimpleModifiedAcdProcessingBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _repository = repository;
            _client = client;
        }

        public override async Task Handle(IDictionary<string, object> parameters)
        {
            var data = (IEnumerable<AcdEntry>)parameters["ModifiedSimpleAcds"];
            var jobId = (Guid)parameters["JobStatusId"];
            if (data.Any())
            {
                var existingAcds = (IEnumerable<AcdEntry>)parameters["ExistingAcds"];
                var resourceMetadata = (ResourceMetadataEntry)parameters["ResourceMetadata"];
                var auditsContainer = (IDictionary<Guid, ConcurrentBag<AcdEntry>>)parameters["AcdChangesForAudits"];
                var children = parameters.ContainsKey("ChildResourcesMetadata") ? (IEnumerable<ResourceMetadataEntry>)parameters["ChildResourcesMetadata"] : Enumerable.Empty<ResourceMetadataEntry>();
                var existingAcdContainerForAudit = (IDictionary<Guid, string>)parameters["ExistingAcdContainerForAudits"];


                EnsureAuditForResourceExists(auditsContainer, existingAcdContainerForAudit, resourceMetadata);
                await Process(data, existingAcds, resourceMetadata, auditsContainer, existingAcdContainerForAudit, children, jobId).ConfigureAwait(false);
            }

            await Link(parameters).ConfigureAwait(false);
        }

        private async Task Process(IEnumerable<AcdEntry> data, IEnumerable<AcdEntry> existingAcds, ResourceMetadataEntry metadata, IDictionary<Guid, ConcurrentBag<AcdEntry>> auditsContainer, IDictionary<Guid, string> existingAcdContainerForAudit, IEnumerable<ResourceMetadataEntry> children, Guid jobId)
        {
            foreach (var acd in data)
            {
                var inheritedFrom = acd.InheritedFrom;
                var existingAcd = existingAcds.Single(e => e.AcdId == acd.AcdId);

                if (inheritedFrom != Guid.Empty)
                {
                    acd.InheritedFrom = Guid.Empty;
                }

                await HandleAcd(acd, existingAcd, metadata).ConfigureAwait(false);
                auditsContainer[metadata.ResourceId].Add(acd);
                ServiceStatus.IncrementProcessedOperations(jobId);

                await HandleInheritors(acd, inheritedFrom, acd.Permissions, children, auditsContainer, existingAcdContainerForAudit, jobId).ConfigureAwait(false);
            }
        }

        private async Task HandleInheritors(AcdEntry acd, Guid inheritedFrom, SortedSet<short> permissions, IEnumerable<ResourceMetadataEntry> children, IDictionary<Guid, ConcurrentBag<AcdEntry>> auditsContainer, IDictionary<Guid, string> existingAcdContainerForAudit, Guid jobId)
        {
            var inheritors = _repository.AcdRepository.GetInheritors(inheritedFrom == Guid.Empty ? acd.AcdId : inheritedFrom).ToEnumerable().ToList();
            foreach (var resource in children)
            //foreach (var inheritor in inheritors)
            {
                //var resource = children.SingleOrDefault(e => e.ResourceId == inheritor.ResourceId);
                //if (resource == null)
                //{
                //    continue;
                //}

                ServiceStatus.IncrementProcessedOperations(jobId);

                var inheritor = inheritors.SingleOrDefault(e => e.ResourceId == resource.ResourceId);
                if (inheritor == null)
                {
                    continue;
                }
                EnsureAuditForResourceExists(auditsContainer, existingAcdContainerForAudit, resource);
                var unchangedPerms = inheritor.Permissions.Intersect(permissions);
                var removedPerms = inheritor.Permissions.Except(unchangedPerms);
                var addedPerms = permissions.Except(unchangedPerms);
                inheritor.InheritedFrom = acd.AcdId;
                inheritor.Permissions = permissions;
                await _repository.AcdRepository.Put(inheritor).ConfigureAwait(false);
                foreach (var permission in removedPerms)
                {
                    if (permission == (short)Permissions.Create)
                    {
                        foreach (var resType in ResourceTypeGroupRepository.GetInnerResourceTypes(inheritor.ResourceType))
                        {
                            await _repository.AcdSetRepository.Delete(inheritor.ResourceId, resType, inheritor.PrincipalId, inheritor.AcdId).ConfigureAwait(false);
                        }
                    }
                    else
                    {
                        await _repository.AcdSetRepository.Delete(inheritor.ResourceId, (Permissions)permission, inheritor.PrincipalId, inheritor.AcdId).ConfigureAwait(false);
                    }
                }
                if ((inheritor.ResourceType == null || ResourceTypeGroupRepository.GetInnerResourceTypes(inheritor.ResourceType.ToLower()).Contains(resource.Type.ToLower()))
                     && (inheritor.ApplicableWorkflow == null || inheritor.ApplicableWorkflow == resource.Workflow)
                     && (inheritor.ApplicableWorkflow == null || (inheritor.ApplicableWorkflow == resource.Workflow && acd.ApplicableWorkflowState == resource.WorkflowState)))
                {
                    foreach (var permission in addedPerms.Where(e => e != (short)Permissions.Create))
                    {
                        await _repository.AcdSetRepository.Put(new AcdSetEntry
                        {
                            AcdId = inheritor.AcdId,
                            InId = resource.ResourceId,
                            OutId = resource.ResourceId,
                            Permission = permission,
                            PrincipalId = inheritor.PrincipalId
                        }).ConfigureAwait(false);
                    }
                }

                if (addedPerms.Contains((short)Permissions.Create))
                {
                    var acdResourceType = inheritor.ResourceType ?? "";
                    foreach (var type in ResourceTypeGroupRepository.GetInnerResourceTypes(acdResourceType))
                    {
                        var acdCreateSet = new AcdCreatePermission
                        {
                            AcdId = inheritor.AcdId,
                            ResourceId = resource.ResourceId,
                            ResourceType = type,
                            PrincipalId = acd.PrincipalId
                        };
                        await _repository.AcdSetRepository.Put(acdCreateSet).ConfigureAwait(false);
                    }
                }

                auditsContainer[resource.ResourceId].Add(inheritor);
            }
        }

        private async Task HandleAcd(AcdEntry acd, AcdEntry existingAcd, ResourceMetadataEntry metadata)
        {
            var createPermission = (short)Permissions.Create;

            var unchangedPerms = acd.Permissions.Intersect(existingAcd.Permissions);
            var removedPerms = existingAcd.Permissions.Except(unchangedPerms);
            var addedPerms = acd.Permissions.Except(unchangedPerms);
            await _repository.AcdRepository.Put(acd).ConfigureAwait(false);

            foreach (var permission in removedPerms)
            {
                if (permission != createPermission)
                {
                    await _repository.AcdSetRepository.Delete(acd.ResourceId, (Permissions)permission, acd.PrincipalId, acd.AcdId).ConfigureAwait(false);
                }
                else
                {
                    foreach (var type in ResourceTypeGroupRepository.GetInnerResourceTypes(acd.ResourceType))
                    {
                        await _repository.AcdSetRepository.Delete(acd.ResourceId, type, acd.PrincipalId, acd.AcdId).ConfigureAwait(false);
                    }
                }
            }

            foreach (var permission in addedPerms)
            {
                if ((acd.ResourceType == null || ResourceTypeGroupRepository.GetInnerResourceTypes(acd.ResourceType.ToLower()).Contains(metadata.Type.ToLower()))
                    && (acd.ApplicableWorkflow == null || acd.ApplicableWorkflow == metadata.Workflow)
                    && (acd.ApplicableWorkflow == null || (acd.ApplicableWorkflow == metadata.Workflow && acd.ApplicableWorkflowState == metadata.WorkflowState)))
                {

                    if (permission != createPermission)
                    {
                        await _repository.AcdSetRepository.Put(new AcdSetEntry
                        {
                            AcdId = acd.AcdId,
                            InId = acd.ResourceId,
                            OutId = acd.ResourceId,
                            Permission = permission,
                            PrincipalId = acd.PrincipalId
                        }).ConfigureAwait(false);
                    }
                }

                if (permission == createPermission)
                {
                    var acdResourceType = acd.ResourceType ?? "";
                    foreach (var type in ResourceTypeGroupRepository.GetInnerResourceTypes(acdResourceType))
                    {

                        await _repository.AcdSetRepository.Put(new AcdCreatePermission
                        {
                            AcdId = acd.AcdId,
                            ResourceId = metadata.ResourceId,
                            ResourceType = type,
                            PrincipalId = acd.PrincipalId
                        }).ConfigureAwait(false);
                    }
                }
            }
        }
    }

    public class AcdDeleteProcessingBlock : BaseCoreService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;

        public AcdDeleteProcessingBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _repository = repository;
            _client = client;
        }

        public override async Task Handle(IDictionary<string, object> parameters)
        {
            var data = (IEnumerable<AcdEntry>)parameters["ModifiedAcdsForDeletion"];
            var jobId = (Guid)parameters["JobStatusId"];
            if (data.Any())
            {
                var existingAcds = (IEnumerable<AcdEntry>)parameters["ExistingAcds"];
                var resourceMetadata = (ResourceMetadataEntry)parameters["ResourceMetadata"];
                var auditsContainer = (IDictionary<Guid, ConcurrentBag<AcdEntry>>)parameters["AcdChangesForAudits"];
                var children = parameters.ContainsKey("ChildResourcesMetadata") ? (IEnumerable<ResourceMetadataEntry>)parameters["ChildResourcesMetadata"] : Enumerable.Empty<ResourceMetadataEntry>();
                var existingAcdContainerForAudit = (IDictionary<Guid, string>)parameters["ExistingAcdContainerForAudits"];

                EnsureAuditForResourceExists(auditsContainer, existingAcdContainerForAudit, resourceMetadata);

                await Process(data, existingAcds, resourceMetadata, auditsContainer, existingAcdContainerForAudit, children, jobId).ConfigureAwait(false);
            }

            await Link(parameters).ConfigureAwait(false);
        }

        private async Task Process(IEnumerable<AcdEntry> data, IEnumerable<AcdEntry> existingAcds, ResourceMetadataEntry metadata, IDictionary<Guid, ConcurrentBag<AcdEntry>> auditsContainer, IDictionary<Guid, string> existingAcdContainerForAudit, IEnumerable<ResourceMetadataEntry> children, Guid jobId)
        {
            var batchHandler = new CassandraBatchHandler(Repository.ConnectionProvider, 250);

            var inheritors = new Dictionary<Guid, List<AcdEntry>>();

            foreach (var acd in data)
            {
                var existing = existingAcds.Single(e => e.AcdId == acd.AcdId);

                var inheritedFrom = acd.InheritedFrom;

                var deleteTask = DeleteEntities(acd, existing, acd, batchHandler);

                auditsContainer[metadata.ResourceId].Add(acd);

                await deleteTask.ConfigureAwait(false);

                auditsContainer[metadata.ResourceId].Add(acd);

                ServiceStatus.IncrementProcessedOperations(jobId);

                inheritors.Add(acd.AcdId, _repository.AcdRepository.GetInheritors(inheritedFrom == Guid.Empty ? acd.AcdId : inheritedFrom).ToEnumerable().ToList());
            }

            await HandleInheritors(data, inheritors, children, auditsContainer, existingAcdContainerForAudit, jobId, batchHandler).ConfigureAwait(false);
            
            await batchHandler.Flush().ConfigureAwait(false);
        }

        private async Task HandleInheritors(IEnumerable<AcdEntry> acds, Dictionary<Guid, List<AcdEntry>> inheritors, IEnumerable<ResourceMetadataEntry> children, IDictionary<Guid, ConcurrentBag<AcdEntry>> auditsContainer, IDictionary<Guid, string> existingAcdContainerForAudit, Guid jobId, IBatchHandler batchHandler)
        {

            foreach (var resource in children)
            {
                foreach (var acd in acds)
                {
                    ServiceStatus.IncrementProcessedOperations(jobId);

                    var inheritor = inheritors[acd.AcdId].SingleOrDefault(e => e.ResourceId == resource.ResourceId);

                    if (inheritor == null)
                    {
                        continue;
                    }
                    EnsureAuditForResourceExists(auditsContainer, existingAcdContainerForAudit, resource);

                    var deleteTask = DeleteEntities(inheritor, inheritor, acd, batchHandler);

                    auditsContainer[resource.ResourceId].Add(inheritor);

                    await deleteTask.ConfigureAwait(false);
                }
            }
        }
    }

    public class ComplexModifiedAcdProcessingBlock : BaseCoreService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;

        public ComplexModifiedAcdProcessingBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _repository = repository;
            _client = client;
        }

        public override async Task Handle(IDictionary<string, object> parameters)
        {
            var data = (IEnumerable<AcdEntry>)parameters["ModifiedComplexAcds"];
            var jobId = (Guid)parameters["JobStatusId"];
            if (data.Any())
            {
                var resourceMetadata = (ResourceMetadataEntry)parameters["ResourceMetadata"];
                var children = parameters.ContainsKey("ChildResourcesMetadata") ? (IEnumerable<ResourceMetadataEntry>)parameters["ChildResourcesMetadata"] : Enumerable.Empty<ResourceMetadataEntry>();
                var auditsContainer = (IDictionary<Guid, ConcurrentBag<AcdEntry>>)parameters["AcdChangesForAudits"];
                var existingAcdContainerForAudit = (IDictionary<Guid, string>)parameters["ExistingAcdContainerForAudits"];
                var existingAcds = (IEnumerable<AcdEntry>)parameters["ExistingAcds"];
                var oneLevelChild = (IEnumerable<ResourceEntry>)parameters["1LevelChildResource"];

                EnsureAuditForResourceExists(auditsContainer, existingAcdContainerForAudit, resourceMetadata);
                await DeleteCurrent(data, children, auditsContainer, existingAcdContainerForAudit, jobId, existingAcds).ConfigureAwait(false);
                await HandleEnumerable(data, auditsContainer, resourceMetadata, jobId).ConfigureAwait(false);
                await HandleRecursive(data, auditsContainer, existingAcdContainerForAudit, children, _client, jobId, oneLevelChild).ConfigureAwait(false);
            }

            await Link(parameters).ConfigureAwait(false);
        }

        private async Task DeleteCurrent(IEnumerable<AcdEntry> data, IEnumerable<ResourceMetadataEntry> children, IDictionary<Guid, ConcurrentBag<AcdEntry>> auditsContainer, IDictionary<Guid, string> existingAcdContainerForAudit, Guid jobId, IEnumerable<AcdEntry> existingAcds)
        {
            var batchHandler = new CassandraBatchHandler(Repository.ConnectionProvider, 250);

            foreach (var acd in data)
            {
                var inheritedFrom = acd.InheritedFrom;

                var existingAcd = existingAcds.Single(x => x.AcdId == acd.AcdId);

                var deleteTask = DeleteEntities(acd, existingAcd, existingAcd, batchHandler);

                await deleteTask.ConfigureAwait(false);

                var deletedInheritedResourceIds = await DeleteInheritors(acd, inheritedFrom, children, auditsContainer, existingAcdContainerForAudit, jobId, batchHandler).ConfigureAwait(false);

                //await AddInheritedAcds(children, auditsContainer, acd, deletedInheritedResourceIds).ConfigureAwait(false);

                var cloneForAudit = existingAcd.Clone();
                cloneForAudit.Permissions.Clear();
                auditsContainer[acd.ResourceId].Add(cloneForAudit);

                acd.InheritedFrom = Guid.Empty;
                acd.AcdId = Guid.NewGuid();

                ServiceStatus.IncrementProcessedOperations(jobId);
            }
            await batchHandler.Flush().ConfigureAwait(false);
        }

        //private async Task AddInheritedAcds(IEnumerable<ResourceMetadataEntry> children, IDictionary<Guid, ConcurrentBag<AcdEntry>> auditsContainer, AcdEntry acd, Dictionary<Guid, Guid> deletedInheritedResourceIds)
        //{
        //    if (deletedInheritedResourceIds.Any())
        //    {
        //        //non forced
        //        foreach (var resourceId in deletedInheritedResourceIds)
        //        {
        //            var resourceMetadata = children.Single(e => e.ResourceId == resourceId.Key);
        //            var clone = acd.Clone();
        //            clone.ResourceId = resourceId.Key;
        //            clone.ResourceSet = resourceMetadata.ResourceSet;
        //            clone.AcdId = resourceId.Value;
        //            clone.InheritedFrom = acd.AcdId;
        //            await HandleEnumerable(new List<AcdEntry> { clone }, auditsContainer, resourceMetadata, Guid.Empty).ConfigureAwait(false);
        //        }
        //    }
        //}

        private async Task<Dictionary<Guid, Guid>> DeleteInheritors(AcdEntry acd, Guid inheritedFrom, IEnumerable<ResourceMetadataEntry> children, IDictionary<Guid, ConcurrentBag<AcdEntry>> auditsContainer, IDictionary<Guid, string> existingAcdContainerForAudit, Guid jobId, IBatchHandler batchHandler)
        {
            var inheritors = _repository.AcdRepository.GetInheritors(inheritedFrom == Guid.Empty ? acd.AcdId : inheritedFrom).ToEnumerable().ToList();

            var dataResult = new Dictionary<Guid, Guid>();

            foreach (var resourceMetadata in children)
            //foreach (var inheritor in inheritors)
            {
                //var resourceMetadata = children.SingleOrDefault(e => e.ResourceId == inheritor.ResourceId);

                //if (resourceMetadata == null)
                //{
                //    continue;
                //}

                ServiceStatus.IncrementProcessedOperations(jobId);

                var inheritor = inheritors.SingleOrDefault(e => e.ResourceId == resourceMetadata.ResourceId);
                if (inheritor == null)
                {
                    continue;
                }

                EnsureAuditForResourceExists(auditsContainer, existingAcdContainerForAudit, resourceMetadata);

                var deleteTask = DeleteEntities(inheritor, inheritor, inheritor, batchHandler);

                var cloneForAudit = inheritor.Clone();
                cloneForAudit.Permissions.Clear();
                auditsContainer[inheritor.ResourceId].Add(cloneForAudit);

                //if (inheritor.OnChildCreate == OnChildCreateOptions.Never)
                //{
                //    dataResult.Add(inheritor.ResourceId, inheritor.AcdId);
                //}
                await deleteTask.ConfigureAwait(false);
            }

            return dataResult;
        }
    }

}
