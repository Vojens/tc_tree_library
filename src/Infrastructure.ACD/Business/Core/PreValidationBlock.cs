﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Infrastructure.Common.Extensions;

namespace Infrastructure.ACD.Business.Core
{
    public class PreValidationBlock : BaseCoreService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;

        public PreValidationBlock(ICompositeAclRepository repository, IGraphClientInfo client)
            : base(repository)
        {
            _repository = repository;
            _client = client;
        }

        public override async Task Handle(IDictionary<string, object> parameters)
        {
            var data = (IList<AcdEntry>)parameters["Input"];
            var skipPermissionCheck = parameters.ContainsKey("SkipPermissionCheck") ? (bool)parameters["SkipPermissionCheck"] : false;

            if (data.Any() == false)
            {
                throw new AcdException("E_PRE_VAL", "There should be at least one ACD entry in the input");
            }

            if (data.GroupBy(e => e.ResourceId).Count() != 1)
            {
                throw new AcdException("E_PRE_VAL", "All ACDs must have the same ResourceId");
            }

            var hasCountGreater = data.GroupBy(x => new { x.PrincipalId, x.ResourceType }).Any(x => x.Count() > 1);

            if (hasCountGreater)
            {
                throw new AcdException("E_PRE_VAL", "Can't add Acds of same user and resource type");
            }

            var resourceId = data[0].ResourceId;
            parameters.Add("ResourceId", resourceId);

            var resourceMetadata = await _repository.ResourceRepository.GetMetadata(resourceId).ConfigureAwait(false);
            if (resourceMetadata == null)
            {
                throw new AcdException("E_PRE_VAL", "Given ResourceId has no Resource in store");
            }
            parameters.Add("ResourceMetadata", resourceMetadata);
            
            var parents = await _repository.ResourceRepository.GetParents(resourceId).AsAsyncList().ConfigureAwait(false);
            parameters.Add("IsMultiParent", parents.Count > 1);

            var hasPermission = await _repository.PermissionsRepository.Has(resourceId, _client.UserIdentities, Permissions.SecurityWrite).ConfigureAwait(false);

            if (!skipPermissionCheck && !hasPermission)
            {
                throw new AcdException("E_PRE_VAL", "User has no security write permission on the resource");
            }

            var hasEmptyPrincipalIds = data.Any(e => e.PrincipalId.Equals(Guid.Empty));

            if (hasEmptyPrincipalIds)
            {
                throw new AcdException("E_PRE_VAL", "All ACD entries must have non-empty PrincipalId");
            }

            var hasInvalidPrincipalName = data.Any(e => string.IsNullOrEmpty(e.PrincipalName));

            if (hasInvalidPrincipalName)
            {
                throw new AcdException("E_PRE_VAL", "All ACD entries must have non-empty and non-null PrincipalName");
            }

            var hasInvalidForce = data.Any(e => e.OnChildCreate == OnChildCreateOptions.Never && e.ForceOnChildCreate);

            if (hasInvalidForce)
            {
                throw new AcdException("E_PRE_VAL", "All ACD entries with OnChildCreate as Never should have their ForceOnChildCreate as false");
            }

            await Link(parameters).ConfigureAwait(false);
        }
    }
}
