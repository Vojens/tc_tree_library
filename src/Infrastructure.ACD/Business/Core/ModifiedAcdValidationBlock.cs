﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business.Core
{
    public class ModifiedAcdValidationBlock : BaseCoreService
    {
        public ModifiedAcdValidationBlock(ICompositeAclRepository repository)
            : base(repository)
        {

        }
        public override Task Handle(IDictionary<string, object> parameters)
        {
            var modifiedAcds = (IEnumerable<AcdEntry>)parameters["ModifiedAcds"];
            var modifiedAcdIds = (IEnumerable<Guid>)parameters["ModifiedAcdIds"];
            var existingAcds = (IEnumerable<AcdEntry>)parameters["ExistingAcds"];
            var existingAcdIds = (IEnumerable<Guid>)parameters["ExistingAcdIds"];
            var existingForcedAcds = (IEnumerable<AcdEntry>)parameters["ExistingForcedAcds"];
            var existingForcedAcdIds = existingForcedAcds.Select(e => e.AcdId);

            if (modifiedAcdIds.Distinct().Count() != modifiedAcdIds.Count())
            {
                throw new AcdException("E_VAL", "No two modified ACD entries can have the same AcdId");
            }

            if (modifiedAcdIds.Any(e => existingAcdIds.Contains(e) == false))
            {
                throw new AcdException("E_VAL", "All modified ACDs must be existing in the store");
            }

            if (modifiedAcds.Any(e => { var existing = existingAcds.Single(f => f.AcdId == e.AcdId); return existing.PrincipalId != e.PrincipalId; }))
            {
                throw new AcdException("E_VAL", "All modified ACDs' PrincipalId cannot be changed");
            }

            if (modifiedAcdIds.Any(e => existingForcedAcdIds.Contains(e)))
            {
                throw new AcdException("E_VAL", "All modified ACDs must not be forced");
            }

            if (modifiedAcds.Any(e => e.ValuesEqual(existingAcds.Single(f => f.AcdId == e.AcdId))))
            {
                throw new AcdException("E_VAL", "All modified ACDs must have some changes from existing ACDs");
            }

            return Link(parameters);
        }
    }
}
