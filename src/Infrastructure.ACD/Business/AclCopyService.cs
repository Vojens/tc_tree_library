﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;
using Infrastructure.ACD.Business.CopyACL;
using Newtonsoft.Json;

namespace Infrastructure.ACD.Business
{
    public interface IAclCopyService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="maps">Tuple containing source-destination mapping</param>
        Task Apply(Guid source, Guid destination, IList<Tuple<Guid, Guid>> maps);
        Task Buddy(Guid source, Guid destination, IList<Tuple<Guid, Guid>> maps);
        Task Approve(Guid resource);
        Task Reject(Guid resource);
    }

    public class AclCopyService : IAclCopyService
    {
        private readonly Guid _jobId;
        private readonly IDictionary<Guid, AcdEntry> _acds;
        private readonly ICompositeAclRepository _repository;
        private readonly IGraphClientInfo _client;
        private readonly IResourceGraphProvider _graphProvider;

        public AclCopyService(ICompositeAclRepository repository, IGraphClientInfo client, Guid jobId)
        {
            _jobId = jobId;
            _repository = repository;
            _client = client;

            _acds = new Dictionary<Guid, AcdEntry>();
            _graphProvider = new ResourceGraphProvider(repository);
        }

        public Task Apply(Guid source, Guid destination, IList<Tuple<Guid, Guid>> maps)
        {
            return Apply(source, destination, maps, false, false);
        }

        public Task Buddy(Guid source, Guid destination, IList<Tuple<Guid, Guid>> maps)
        {
            return Apply(source, destination, maps, true, false);
        }

        public async Task Approve(Guid resource)
        {
            var workflow = await _repository.AcdWorkflowRepository.Get(resource, WorkflowOptions.CopyAcl.ToString()).ConfigureAwait(false);

            if (string.IsNullOrEmpty(workflow.DisplayText))
            {
                throw new AcdException("E_VAL", "Resource is not part of a copy acl group");
            }

            if (workflow.CreatedUserId == _client.UserId)
            {
                throw new AcdException("E_VAL", "Same user cannot approve");
            }

            var count = await _repository.AcdWorkflowRepository.GetAcdWorkflowsByDisplayTextCount(workflow.DisplayText).ConfigureAwait(false);

            ServiceStatus.SetTotalOperations(_jobId, Convert.ToInt32(count));
            ServiceStatus.IncrementResources(_jobId, Convert.ToInt32(count));
            ServiceStatus.SetFlag(_jobId, ServiceStatusFlags.Started);

            var allInTheGroup = _repository.AcdWorkflowRepository.GetAcdWorkflowsByDisplayText(workflow.DisplayText).ToEnumerable();

            var workflowData = JsonConvert.DeserializeObject<NodeData>(workflow.Data);

            var mappings = new Dictionary<Guid, Guid>();

            foreach (var grp in allInTheGroup)
            {
                var data = JsonConvert.DeserializeObject<NodeData>(grp.Data);
                mappings.Add(data.SourceId, grp.ResourceId);
            }

            await Apply(workflowData.RootSource, workflowData.RootDestination, mappings.Select(e => Tuple.Create(e.Key, e.Value)).ToList(), false, true).ConfigureAwait(false);
        }

        public async Task Reject(Guid resource)
        {
            var workflow = await _repository.AcdWorkflowRepository.Get(resource, WorkflowOptions.CopyAcl.ToString()).ConfigureAwait(false);

            if (string.IsNullOrEmpty(workflow.DisplayText))
            {
                throw new AcdException("E_VAL", "Resource is not part of a copy acl group");
            }

            var count = await _repository.AcdWorkflowRepository.GetAcdWorkflowsByDisplayTextCount(workflow.DisplayText).ConfigureAwait(false);

            ServiceStatus.SetTotalOperations(_jobId, Convert.ToInt32(count));
            ServiceStatus.IncrementResources(_jobId, Convert.ToInt32(count));
            ServiceStatus.SetFlag(_jobId, ServiceStatusFlags.Started);

            var allInTheGroup = _repository.AcdWorkflowRepository.GetAcdWorkflowsByDisplayText(workflow.DisplayText).ToEnumerable();

            foreach (var node in allInTheGroup)
            {
                var resourceId = node.ResourceId;
                var currentAcds = _repository.AcdRepository.Get(resourceId).ToEnumerable().ToList();
                var resourceMetadata = await _repository.ResourceRepository.GetMetadata(resourceId).ConfigureAwait(false);
                var displayText = resourceMetadata.ResourceSet == "SEA"
                        ? string.IsNullOrEmpty(resourceMetadata.Description) ? resourceMetadata.Uri : resourceMetadata.Description
                        : resourceMetadata.Uri;
                var workflowData = JsonConvert.DeserializeObject<NodeData>(node.Data);
                var _auditId = Guid.NewGuid();
                var auditEntry = new AuditEntry
                {
                    Action = AcdActions.RejectBuddy,
                    AuditId = _auditId,
                    DisplayText = displayText,
                    InId = resourceId,
                    ModifiedUsername = _client.Username,
                    OutId = resourceId,
                    ResourceName = resourceMetadata.Name,
                    ResourceType = resourceMetadata.Type,
                    Timestamp = DateTimeOffset.UtcNow,
                    State = AcdStates.Completed,
                };

                var auditDataEntry = new AuditDataEntry
                {
                    AuditId = _auditId,
                    DataNew = JsonConvert.SerializeObject(currentAcds),
                    DataOld = JsonConvert.SerializeObject(workflowData.SourceAcds)
                };

                await _repository.AuditsRepository.Put(auditEntry).ConfigureAwait(false);
                await _repository.AuditDataRepository.Put(auditDataEntry).ConfigureAwait(false);

                await _repository.AcdWorkflowRepository.Delete(node).ConfigureAwait(false);

                ServiceStatus.IncrementProcessedOperations(_jobId);
                ServiceStatus.IncrementAudits(_jobId);
            }
        }

        private async Task Apply(Guid source, Guid destination, IList<Tuple<Guid, Guid>> maps, bool isBuddy = false, bool isApprove = false)
        {
            var destinationGraph = _graphProvider.GetWithoutBuddiedResources(destination);
            ServiceStatus.SetTotalOperations(_jobId, destinationGraph.Pointers.Count);
            var mappingDictionary = maps.ToDictionary(e => e.Item2, e => e.Item1);

            var traverser = new ResourceGraphTraverser(_repository,
                destinationGraph.Pointers.Count,
                _jobId, _client,
                isBuddy, isApprove,
                source, destination);

            await traverser.Traverse(destinationGraph.TopLevelNodes, mappingDictionary, null).ConfigureAwait(false);
            await traverser.Commit().ConfigureAwait(false);
        }
    }
}
