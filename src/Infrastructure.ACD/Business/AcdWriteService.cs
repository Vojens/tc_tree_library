﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using Infrastructure.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Infrastructure.ACD.Business.Core;

namespace Infrastructure.ACD.Business
{
    public interface IAcdWriteService
    {
        Task Handle(IList<AcdEntry> input, bool isApproval);
        Task HandleWithoutPermissionCheck(IList<AcdEntry> input);
    }
    public class AcdWriteService : IAcdWriteService
    {
        ICompositeAclRepository _repository;
        IGraphClientInfo _client;
        Guid _jobStatusId;

        public AcdWriteService(ICompositeAclRepository repository, IGraphClientInfo client, Guid jobStatusId)
        {
            _repository = repository;
            _client = client;
            _jobStatusId = jobStatusId;

            PreValidationBlock = new PreValidationBlock(_repository, _client);
            PreProcessingBlock = new PreProcessingBlock(_repository, _client);
            NewSimpleAcdValidationBlock = new NewSimpleAcdValidationBlock(_repository);
            ModifiedAcdValidationBlock = new ModifiedAcdValidationBlock(_repository);
            NewInheritedAcdValidationBlock = new NewInheritedAcdPreValidationBlock(_repository, _client);
            NewInheritedAcdDataBlock = new NewInheritedAcdDataBlock(_repository, _client);
            MergeValidationBlock = new MergeValidationBlock(_repository);
            MetadataUpdateBlock = new MetadataUpdateBlock(_repository, _client);
            NewSimpleAcdProcessingBlock = new NewSimpleAcdProcessingBlock(_repository, _client);
            NewSimpleRecursiveAcdProcessingBlock = new NewSimpleRecursiveAcdProcessingBlock(_repository, _client);
            NewInheritedAcdProcessingBlock = new NewInheritedAcdProcessingBlock(_repository, _client);
            NewInheritedRecursiveAcdProcessingBlock = new NewInheritedRecursiveAcdProcessingBlock(_repository, _client);
            ModifiedAcdProcessingUnitsGeneratorBlock = new ModifiedAcdProcessingUnitsGeneratorBlock(_repository);
            SimpleModifiedAcdProcessingBlock = new SimpleModifiedAcdProcessingBlock(_repository, _client);
            AcdDeleteProcessingBlock = new AcdDeleteProcessingBlock(_repository, _client);
            ComplexModifiedAcdProcessingBlock = new ComplexModifiedAcdProcessingBlock(_repository, _client);
            AuditGeneratorBlock = new AuditGeneratorBlock(_repository, _client);
            JobStatusInitiatorBlock = new JobStatusInitiatorBlock(_repository);
            ResourceLockerBlock = new ResourceLockerBlock(_repository);

            PreValidationBlock.LinkTo(PreProcessingBlock).LinkTo(ResourceLockerBlock)
                .LinkTo(NewInheritedAcdDataBlock).LinkTo(ModifiedAcdProcessingUnitsGeneratorBlock)
                .LinkTo(JobStatusInitiatorBlock)
                .LinkTo(NewSimpleAcdValidationBlock)
                .LinkTo(ModifiedAcdValidationBlock).LinkTo(NewInheritedAcdValidationBlock)
                .LinkTo(MergeValidationBlock)
                .LinkTo(MetadataUpdateBlock).LinkAll(NewSimpleAcdProcessingBlock, NewSimpleRecursiveAcdProcessingBlock, NewInheritedAcdProcessingBlock, NewInheritedRecursiveAcdProcessingBlock)
                .Join(new ServiceJoiner(_repository))
                .LinkAll(SimpleModifiedAcdProcessingBlock, AcdDeleteProcessingBlock, ComplexModifiedAcdProcessingBlock)
                .Join(AuditGeneratorBlock);
        }



        public Task Handle(IList<AcdEntry> input, bool isApproval)
        {
            var data = new Dictionary<string, object> {
                { "Input", input }, 
                { "IsApproval", isApproval }, 
                { "JobStatusId", _jobStatusId }
            };
            return PreValidationBlock.Handle(data);
        }

        public Task HandleWithoutPermissionCheck(IList<AcdEntry> input)
        {
            var data = new Dictionary<string, object> {
                { "Input", input }, 
                { "IsApproval", false }, 
                { "SkipPermissionCheck", true},
                { "JobStatusId", _jobStatusId }
            };
            return PreValidationBlock.Handle(data);
        }

        public Task HandleIgnoringDuplicates(IList<AcdEntry> input)
        {
            var data = new Dictionary<string, object> {
                { "Input", input }, 
                { "IsApproval", false }, 
                { "IgnoreDuplicates", true},
                { "JobStatusId", _jobStatusId }
            };
            return PreValidationBlock.Handle(data);
        }


        ICoreService PreValidationBlock;
        ICoreService PreProcessingBlock;
        ICoreService JobStatusInitiatorBlock;
        ICoreService NewSimpleAcdValidationBlock;
        ICoreService ModifiedAcdValidationBlock;
        ICoreService NewInheritedAcdValidationBlock;
        ICoreService NewInheritedAcdDataBlock;
        ICoreService MergeValidationBlock;
        ICoreService MetadataUpdateBlock;
        ICoreService NewSimpleAcdProcessingBlock;
        ICoreService NewSimpleRecursiveAcdProcessingBlock;
        ICoreService NewInheritedAcdProcessingBlock;
        ICoreService NewInheritedRecursiveAcdProcessingBlock;
        ICoreService ModifiedAcdProcessingUnitsGeneratorBlock;
        ICoreService SimpleModifiedAcdProcessingBlock;
        ICoreService AcdDeleteProcessingBlock;
        ICoreService ComplexModifiedAcdProcessingBlock;
        ICoreService AuditGeneratorBlock;
        ICoreService ResourceLockerBlock;
    }
}
