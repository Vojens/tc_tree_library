﻿using Infrastructure.ACD.Entities;
using Infrastructure.ACD.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Business
{
    public interface IPermissionService
    {
        Task<bool> HasCreatePermission(Guid resourceId, List<Guid> useridentities, string resourceType);
        Task<bool> HasCreatePermission(List<Guid> resourceIds, List<Guid> useridentities, string resourceType);
        Task<bool> HasPermission(Guid resourceid, List<Guid> useridentities, Permissions perm);
        Task<List<Guid>> HasPermission(List<Guid> reousrceIds, List<Guid> userIdentities, Permissions perm);
        Task<bool> HasPermission(string resourceuri, List<Guid> useridentities, Permissions perm);
        Task<List<string>> HasPermission(List<string> resourceuri, List<Guid> userIdentities, Permissions perm);
        //Task<bool> HasPermission(string resourceuri, List<Guid> useridentities, Permissions perm);
        //Task<bool> HasPermission(Guid resourceId, List<Guid> useridentities, Permissions perm);
        Task<bool> HasPermission(Guid resourceid, List<Guid> useridentities, List<Permissions> perm);
        Task<bool> HasPermission(string resourceuri, List<Guid> useridentities, List<Permissions> perm);
    }

    public class PermissionService : IPermissionService
    {
        const int _resourcePageSize = 100;
        static Guid[] _whitelistedPrincipalIds;
        ICompositeAclRepository _repository;

        static PermissionService()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "whitelist.json");
            if (!File.Exists(path))
                path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin", "whitelist.json");
            _whitelistedPrincipalIds = Newtonsoft.Json.JsonConvert.DeserializeObject<Guid[]>(File.ReadAllText(path));
        }

        public static bool IsWhitelisted(List<Guid> useridentities)
        {
            return _whitelistedPrincipalIds.Intersect(useridentities).Any();
        }

        public PermissionService(ICompositeAclRepository repository)
        {
            _repository = repository;
        }

        public async Task<bool> HasCreatePermission(Guid resourceId, List<Guid> useridentities, string resourceType)
        {
            if (_whitelistedPrincipalIds.Intersect(useridentities).Any())
            {
                return true;
            }
            return await _repository.PermissionsRepository.HasCreatePermission(resourceId, useridentities, resourceType).ConfigureAwait(false);
        }

        public async Task<bool> HasCreatePermission(List<Guid> resourceIds, List<Guid> useridentities, string resourceType)
        {
            if (_whitelistedPrincipalIds.Intersect(useridentities).Any())
            {
                return true;
            }
            return await _repository.PermissionsRepository.HasCreatePermission(resourceIds, useridentities, resourceType).ConfigureAwait(false);
        }

        public async Task<bool> HasPermission(Guid resourceid, List<Guid> useridentities, Permissions perm)
        {
            AssertNotCreatePermission(perm);
            if (_whitelistedPrincipalIds.Intersect(useridentities).Any())
            {
                return true;
            }
            return (await HasPermission(new List<Guid> { resourceid }, useridentities, perm).ConfigureAwait(false)).Any();
        }

        public async Task<List<Guid>> HasPermission(List<Guid> reousrceIds, List<Guid> userIdentities, Permissions perm)
        {
            AssertNotCreatePermission(perm);
            if (_whitelistedPrincipalIds.Intersect(userIdentities).Any())
            {
                return userIdentities;
            }
            var count = 0;
            var result = new List<Guid>();
            do
            {
                var page = reousrceIds.Skip(count).Take(_resourcePageSize);
                count += _resourcePageSize;
                result.AddRange(await _repository.PermissionsRepository.Filter(page.ToList(), userIdentities, perm).ConfigureAwait(false));
            } while (count < reousrceIds.Count);
            return result;
        }

        public async Task<bool> HasPermission(string resourceuri, List<Guid> useridentities, Permissions perm)
        {
            AssertNotCreatePermission(perm);
            if (_whitelistedPrincipalIds.Intersect(useridentities).Any())
            {
                return true;
            }
            return (await HasPermission(new List<string> { resourceuri }, useridentities, perm).ConfigureAwait(false)).Any();
        }

        public async Task<List<string>> HasPermission(List<string> resourceuri, List<Guid> userIdentities, Permissions perm)
        {
            AssertNotCreatePermission(perm);
            if (_whitelistedPrincipalIds.Intersect(userIdentities).Any())
            {
                return resourceuri;
            }
            var count = 0;
            var result = new List<string>();
            do
            {
                var page = resourceuri.Skip(count).Take(_resourcePageSize);
                count += _resourcePageSize;
                result.AddRange(await _repository.PermissionsRepository.Filter(page.ToList(), userIdentities, perm).ConfigureAwait(false));
            } while (count < resourceuri.Count);
            return result;
        }

        //public async Task<bool> HasPermission(Guid resourceId, List<Guid> useridentities, Entities.Permissions perm)
        //{
        //    AssertNotCreatePermission(perm);
        //    if (_whitelistedPrincipalIds.Intersect(useridentities).Any())
        //    {
        //        return true;
        //    }
        //    return await HasPermission(resourceId, useridentities, new List<Entities.Permissions> { perm }).ConfigureAwait(false);
        //}

        public async Task<bool> HasPermission(Guid resourceid, List<Guid> useridentities, List<Permissions> perm)
        {
            AssertNotCreatePermission(perm);
            if (_whitelistedPrincipalIds.Intersect(useridentities).Any())
            {
                return true;
            }
            return await _repository.PermissionsRepository.Has(resourceid, useridentities, perm).ConfigureAwait(false);
        }

        //public async Task<bool> HasPermission(string resourceuri, List<Guid> useridentities, Entities.Permissions perm)
        //{
        //    AssertNotCreatePermission(perm);
        //    if (_whitelistedPrincipalIds.Intersect(useridentities).Any())
        //    {
        //        return true;
        //    }
        //    return await HasPermission(resourceuri, useridentities, new List<Entities.Permissions> { perm }).ConfigureAwait(false);
        //}

        public async Task<bool> HasPermission(string resourceuri, List<Guid> useridentities, List<Permissions> perm)
        {
            AssertNotCreatePermission(perm);
            if (_whitelistedPrincipalIds.Intersect(useridentities).Any())
            {
                return true;
            }
            return await _repository.PermissionsRepository.Has(resourceuri, useridentities, perm).ConfigureAwait(false);
        }

        private static void AssertNotCreatePermission(Permissions perm)
        {
            if (perm == Permissions.Create)
            {
                throw new Exception("Cannot check create permission via this api");
            }
        }

        private void AssertNotCreatePermission(List<Permissions> perm)
        {
            var hasCreatePermission = perm.Any(e => e == Permissions.Create);
            if (hasCreatePermission)
            {
                AssertNotCreatePermission(Permissions.Create);
            }
        }
    }
}
