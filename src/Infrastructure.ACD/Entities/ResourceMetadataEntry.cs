﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Entities
{
    public class ResourceMetadataEntry
    {
        public Guid ParentId { get; set; }
        public Guid ResourceId { get; set; }
        public Guid CreatedUserId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Uri { get; set; }
        public string Type { get; set; }
        public string ResourceSet { get; set; }
        public string Workflow { get; set; }
        public short WorkflowState { get; set; }
        public string SolrQuery { get; set; }
    }
}
