﻿using System;

namespace Infrastructure.ACD.Entities
{
    public class AcdSetEntry
    {
        public Guid OutId { get; set; }
        public Guid InId { get; set; }
        public short Permission { get; set; }
        public Guid PrincipalId { get; set; }
        public Guid AcdId { get; set; }
        public string SolrQuery { get; set; }
    }
}
