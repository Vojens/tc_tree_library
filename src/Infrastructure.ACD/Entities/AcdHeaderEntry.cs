﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Entities
{
    public class AcdEntry
    {
        public Guid ResourceId { get; set; }
        public Guid AcdId { get; set; }
        public string ResourceType { get; set; }
        public Guid PrincipalId { get; set; }
        public string PrincipalName { get; set; }
        public PrincipalTypes PrincipalType { get; set; }
        public OnChildCreateOptions OnChildCreate { get; set; }
        public Boolean ForceOnChildCreate { get; set; }
        public Guid InheritedFrom { get; set; }
        public string ApplicableWorkflow { get; set; }
        public short ApplicableWorkflowState { get; set; }
        public Guid CreatedUserId { get; set; }
        public string CreatedUsername { get; set; }
        public DateTimeOffset CreatedTimestamp { get; set; }
        public string ResourceSet { get; set; }
        public SortedSet<short> Permissions { get; set; }
        public string SolrQuery { get; set; }
        public Boolean ModifiedForReplacePrincipal { get; set; }
    }
}
