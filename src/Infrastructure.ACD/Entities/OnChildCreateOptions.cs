﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.ACD.Entities
{
    public enum OnChildCreateOptions
    {
        Never,
        Always,
        IfIdentity,
        IfNotIdentity,
        Always_1Level
    }
}
