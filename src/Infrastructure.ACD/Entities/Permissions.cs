﻿namespace Infrastructure.ACD.Entities
{
    public enum Permissions : short
    {
        Read,
        Create,
        Edit,
        Delete,
        Browse,
        Link,
        SecurityRead,
        SecurityWrite,
        TransitionWorkflow
    }
}
