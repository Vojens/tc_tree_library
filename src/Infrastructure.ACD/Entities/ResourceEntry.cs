﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Entities
{
    public class ResourceEntry
    {
        public Guid ParentId { get; set; }
        public Guid ResourceId { get; set; }
        public string ResourceName { get; set; }
        public string ResourceType { get; set; }
        public string ResourceUri { get; set; }
        public string SolrQuery { get; set; }
    }
}
