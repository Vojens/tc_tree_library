﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Entities
{
    public class AuditDataEntry
    {
        public Guid AuditId { get; set; }
        public string DataOld { get; set; }
        public string DataNew { get; set; }
    }
}
