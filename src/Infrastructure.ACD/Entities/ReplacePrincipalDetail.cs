﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infrastructure.ACD.Entities
{
    public class ReplacePrincipalDetail
    {
        public Guid ResourceId { get; set; }
        public string PrincipalName { get; set; }
        public Guid PrincipalId { get; set; }
        public PrincipalTypes PrincipalType { get; set; }
        public List<AcdEntry> Acds { get; set; }
    }
}