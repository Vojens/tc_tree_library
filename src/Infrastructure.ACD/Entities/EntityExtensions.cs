﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Entities
{
    public static class EntityExtensions
    {
        public static AcdEntry Clone(this AcdEntry entry)
        {
            var newEntry = new AcdEntry
            {
                AcdId = entry.AcdId,
                ApplicableWorkflow = entry.ApplicableWorkflow,
                ApplicableWorkflowState = entry.ApplicableWorkflowState,
                CreatedTimestamp = entry.CreatedTimestamp,
                CreatedUserId = entry.CreatedUserId,
                CreatedUsername = entry.CreatedUsername,
                ForceOnChildCreate = entry.ForceOnChildCreate,
                InheritedFrom = entry.InheritedFrom,
                OnChildCreate = entry.OnChildCreate,
                Permissions = new SortedSet<short>(entry.Permissions),
                PrincipalId = entry.PrincipalId,
                PrincipalName = entry.PrincipalName,
                PrincipalType = entry.PrincipalType,
                ResourceId = entry.ResourceId,
                ResourceSet = entry.ResourceSet,
                ResourceType = entry.ResourceType                
            };
            return newEntry;
        }

        public static bool ValuesEqual(this AcdEntry entry, AcdEntry compareTo)
        {
            return entry.AcdId == compareTo.AcdId && entry.ApplicableWorkflow == compareTo.ApplicableWorkflow &&
                entry.ApplicableWorkflowState == compareTo.ApplicableWorkflowState && entry.ForceOnChildCreate == compareTo.ForceOnChildCreate &&
                entry.OnChildCreate == compareTo.OnChildCreate && entry.Permissions.SequenceEqual(compareTo.Permissions) &&
                entry.PrincipalId == compareTo.PrincipalId && entry.ResourceType == compareTo.ResourceType;
        }
    }
}
