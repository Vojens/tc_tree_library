﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Entities
{
    public class AcdWorkflowEntry
    {
        public Guid ResourceId { get; set; }
        public string DisplayText { get; set; }
        public string AcdWorkflow { get; set; }
        public short AcdWorkflowState { get; set; }
        public Guid CreatedUserId { get; set; }
        public Guid Id { get; set; }
        public string Data { get; set; }
        public string CreatedUsername { get; set; }
        public DateTimeOffset CreatedTimestamp { get; set; }
        public string SolrQuery { get; set; }
    }
}
