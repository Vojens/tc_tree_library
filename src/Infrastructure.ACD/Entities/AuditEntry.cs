﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ACD.Entities
{
    public class AuditEntry
    {
        public Guid InId { get; set; }
        public Guid OutId { get; set; }
        public Guid AuditId { get; set; }
        public AcdStates State { get; set; }
        public AcdActions Action { get; set; }
        public string ResourceName { get; set; }
        public string ResourceType { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string DisplayText { get; set; }
        public string ModifiedUsername { get; set; }
        public string SolrQuery { get; set; }
    }
}
