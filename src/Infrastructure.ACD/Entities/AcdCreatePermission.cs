﻿using System;

namespace Infrastructure.ACD.Entities
{
    public class AcdCreatePermission
    {
        public Guid ResourceId { get; set; }
        public Guid PrincipalId { get; set; }
        public string ResourceType { get; set; }
        public Guid AcdId { get; set; }
    }
}
