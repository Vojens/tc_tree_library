﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infrastructure.ACD.Entities
{
    public enum WorkflowOptions
    {
        Buddy,
        ReplaceAcd,
        CopyAcl
    }
}